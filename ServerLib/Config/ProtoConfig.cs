﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using ProtoBuf;

namespace ServerLib
{
    /// <summary>
    /// 服务器条目
    /// </summary>
    [ProtoContract]
    public class ServerItem
    {
        [ProtoMember(1)]
        public string key = "";

        [ProtoMember(2)]
        public string zoneid = "";

        [ProtoMember(3)]
        public string type = "";

        [ProtoMember(4)]
        public string desc = "";

        [ProtoMember(5)]
        public string number = "";

        [ProtoMember(6)]
        public string ip = "";

        [ProtoMember(7)]
        public string port = "";
    }

    [ProtoContract]
    public class ServerItems
    {
        [ProtoMember(1)]
        public Dictionary<UInt32, ServerItem> serverMap = new Dictionary<UInt32, ServerItem>();

        //获取实际地址
        public IPEndPoint GetEndPoint(UInt32 serverid)
        {
            if (serverMap.ContainsKey(serverid))
            {
                ServerItem item = serverMap[serverid];
                return new IPEndPoint(IPAddress.Parse(item.ip), int.Parse(item.port));
            }

            return null;
        }

        public UInt32 GetServerId(IPEndPoint endpoint)
        {
            foreach(var item in serverMap)
            {
                if(endpoint.Address.ToString()==item.Value.ip && endpoint.Port.ToString()==item.Value.port)
                {
                    return item.Key;
                }
            }

            return 0;
        }

        public List<IPEndPoint> GetConnectEndPoints(List<string> serverTypes)
        {
            List<IPEndPoint> lstPoints = new List<IPEndPoint>();

            foreach (var peer in serverTypes)
            {
                foreach (var item in serverMap)
                {
                    UInt32 serverid = item.Key;
                    VirtualAddress addr = GetVirtualAddress(serverid);
                    if (peer == addr.StrServiceType)
                    {
                        IPEndPoint endpoint = GetEndPoint(serverid);
                        if(null != endpoint)
                        {
                            lstPoints.Add(endpoint);
                        }
                    }
                }
            }

            return lstPoints;
        }

        public static VirtualAddress GetVirtualAddress(UInt32 serverid)
        {
            return serverid;
        }
    }

    [ProtoContract]
    public class RoleActorIds
    {

    }
}