﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerLib.Bus;

namespace ServerLib
{
    /// <summary>
    ///虚拟地址
    /// </summary>
    public class VirtualAddress
    {
        string addr;
        UInt32 addrNum;

        /// <summary>
        /// string style address
        /// </summary>
        public string Addr
        {
            get
            {
                return addr;
            }
        }


        private VirtualAddress()
        {
        }

        /// <summary>
        /// ctor , parse input string
        /// </summary>
        /// <param name="addr"></param>
        public VirtualAddress(string addr)
        {
            this.addr = addr;
            addrNum = 0;

            parse();
        }

        public VirtualAddress(string zoneid, string type, string number)
        {
            this.addr = "0." + zoneid + "." + ServiceType.getServiceType(type) + "." + number;
            addrNum = 0;

            parse();
        }

        /// <summary>
        /// ctor with numeric input, generate string style address.
        /// </summary>
        /// <param name="addr"></param>
        public VirtualAddress(UInt32 addr)
        {
            this.addr = string.Format("{0}.{1}.{2}.{3}",(byte)(addr>>24), (byte)((addr >> 16) & 0xFF),(byte)((addr >> 8) & 0xFF),(byte)(addr & 0xFF));
            addrNum = addr;
        }

        /// <summary>
        ///  Friendly output
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return addr;
        }

        /// <summary>
        /// from string to VirtualAddress
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        public static implicit operator VirtualAddress(string addr)
        {
            return new VirtualAddress(addr);
        }

        /// <summary>
        /// from VirtualAddress to UInt32
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        public static implicit operator UInt32(VirtualAddress addr)
        {
            return addr.addrNum;
        }

        /// <summary>
        /// from UInt32 to VirtualAddress
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        public static implicit operator VirtualAddress(UInt32 addr)
        {
            return new VirtualAddress(addr);
        }

        void parse()
        {
            string[] Result = addr.Split('.');
            addrNum = (UInt32)(byte.Parse(Result[0]) << 24 | byte.Parse(Result[1]) << 16 | byte.Parse(Result[2]) << 8 | byte.Parse(Result[3]));
        }

        /// <summary>
        /// ==
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static bool operator==(VirtualAddress lhs, VirtualAddress rhs)
        {
            if ((lhs as object) == null)
            {
                if ((rhs as object) != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if ((rhs as object) == null)
            {
                return false;
            }

            if (lhs.addr == rhs.addr)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        ///  !=
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static bool operator!=(VirtualAddress lhs, VirtualAddress rhs)
        {
            return !(lhs == rhs);
        }

        /// <summary>
        /// equal by memory address
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is VirtualAddress)
            {
                VirtualAddress addr = (VirtualAddress)obj;
                return addr == this;
            }

            return false;
        }

        /// <summary>
        /// hash code
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return addr.GetHashCode();
        }

        /// <summary>
        /// judge whether address is a router
        /// </summary>
        /// <returns></returns>
        public bool IsMonitor()
        {
            return GetServiceType() == ServerLib.Bus.ServiceType.SERVICE_MONITOR;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsLobby()
        {
            return GetServiceType() == ServerLib.Bus.ServiceType.SERVICE_LOBBY;
        }

        public bool IsBattle()
        {
            return GetServiceType() == ServerLib.Bus.ServiceType.SERVICE_BATTLE;
        }

        /// <summary>
        /// judge whether address is a gate server
        /// </summary>
        /// <returns></returns>
        public bool IsGate()
        {
            return GetServiceType() == ServerLib.Bus.ServiceType.SERVICE_GATE;
        }

        public string StrServiceType
        {
            get
            {
                return ServerLib.Bus.ServiceType.toString(GetServiceType());
            }
        }

        /// <summary>
        /// judge whether address is null
        /// </summary>
        /// <returns></returns>
        public bool IsZero()
        {
            return addrNum == 0;
        }

        /// <summary>
        /// 获取服务类型
        /// </summary>
        /// <returns></returns>
        public byte GetServiceType()
        {
            return (byte)((addrNum & 0x0000FF00) >> 8);
        }

        /// <summary>
        /// 获取GameId
        /// </summary>
        /// <returns></returns>
        public byte GetGameId()
        {
            return (byte)((addrNum & 0xFF000000) >> 24);
        }

        /// <summary>
        /// 获取ZoneId
        /// </summary>
        /// <returns></returns>
        public byte GetZoneId()
        {
            return (byte)((addrNum & 0x00FF0000) >> 16);
        }

        /// <summary>
        /// Preset address stands for client.
        /// </summary>
        public static readonly VirtualAddress CLIENT_ADDRESS = new VirtualAddress("255.255.255.255");

        /// <summary>
        /// Preset address stands for NULL, proto targeted with this address will send to router.
        /// </summary>
        public static readonly VirtualAddress NULL_ADDRESS = new VirtualAddress("0.0.0.0");

        /// <summary>
        /// Preset address for broadcasting
        /// </summary>
        public static readonly VirtualAddress BROADCAST_ADDRESS = new VirtualAddress("0.0.255.255");
    }
}
