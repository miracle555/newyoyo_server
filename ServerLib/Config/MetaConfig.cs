using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using ServerLib;

namespace Config
{
    public class ServerConfig : IConfig
    {
        public LogConfig LogCfg { get; set; }
        
        public BusConfig BusCfg { get; set; }

        public string ConfigPath { get; set; }

        public StaticDataConfig StaticDataCfg { get; set; }

        public DumpConfig DumpCfg { get; set; }

        /// <summary>
        /// ������Ϣ1����
        /// </summary>
        public int heartMillisecond = 1000;

        public ServerConfig()
        {
            LogCfg = new LogConfig();
            BusCfg = new BusConfig();
            StaticDataCfg = new StaticDataConfig();
            DumpCfg = new DumpConfig();
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "LogCfg")
                {
                    LogCfg.Load((XmlElement)elementNode);
                }
                if (elementNode.Name == "BusCfg")
                {
                    BusCfg.Load((XmlElement)elementNode);
                }
                if (elementNode.Name == "StaticDataCfg")
                {
                    StaticDataCfg.Load((XmlElement)elementNode);
                }
                if (elementNode.Name == "DumpCfg")
                {
                    DumpCfg.Load((XmlElement)elementNode);
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }

    public class LocalAddressConfig : IConfig
    {
        public string ZoneId
        {
            get;
            set;
        }

        public string ServerType
        {
            get;
            set;
        }

        public string ServerNumber 
        {
            get;
            set;
        }

        public LocalAddressConfig()
        {
            ZoneId = "";
            ServerType = "";
            ServerNumber = "";
        }

        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "ZoneId")
                {
                    ZoneId = elementNode.InnerText;
                }
                if (elementNode.Name == "ServerType")
                {
                    ServerType = elementNode.InnerText;
                }
                if (elementNode.Name == "ServerNumber")
                {
                    ServerNumber = elementNode.InnerText;
                }
            }
        }

        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }

    public class BusConfig : IConfig
    {
        public List<string> Peer
        {
            get;
            set;
        }

        public LocalAddressConfig LocalAddressCfg
        {
            get;
            set;
        }

        public BusConfig()
        {
            Peer = new List<string>();
            LocalAddressCfg = new LocalAddressConfig();
        }

        VirtualAddress localAddress;
        public VirtualAddress LocalAddress
        {
            get
            {
                if(null == localAddress)
                {
                    localAddress = new VirtualAddress(LocalAddressCfg.ZoneId, LocalAddressCfg.ServerType, LocalAddressCfg.ServerNumber);
                }

                return localAddress;
            }
        }

        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "LocalAddressCfg")
                {
                    LocalAddressCfg.Load((XmlElement)elementNode);
                }
                if (elementNode.Name == "Peers")
                {
                    XmlNodeList childNodes = elementNode.ChildNodes;
                    foreach (XmlNode childNode in childNodes)
                    {
                        Peer.Add(childNode.InnerText);
                    }
                }
            }
        }

        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }

    public class DumpConfig : IConfig
    {
        public bool DumpSwitch
        {
            get;
            set;
        }

        public uint DumpSetting
        {
            get;
            set;
        }



        public DumpConfig()
        {
            DumpSwitch = false;
            DumpSetting = 0x00000000;
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "DumpSwitch")
                {
                    DumpSwitch = bool.Parse(elementNode.InnerText);
                }
                if (elementNode.Name == "DumpSetting")
                {
                    DumpSetting = uint.Parse(elementNode.InnerText);
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


}


