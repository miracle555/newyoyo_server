﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module
{
    /// <summary>
    ///预定义的Module id,使用byte存储,不要超过0xFF
    /// </summary>
    public class ModuleID
    {
        /// <summary>
        /// reserved begin
        /// </summary>
        public const UInt32 MODULE_RESERVEID_BEGIN = 1;

        /// <summary>

        /// <summary>
        /// reserved end
        /// </summary>
        public const UInt32 MODULE_RESERVEID_END = 1000;
    }
}