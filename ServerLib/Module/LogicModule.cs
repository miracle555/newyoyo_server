﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using ServerLib;
using ServerLib.Bus;
using System.Net;

using Event;
using Log;

namespace Module
{
    public abstract class LogicModule : ILogicModule
    {
        public LogicModule(ServerEnvironment env)
        {
            this.env = env;
        }

        public virtual UInt32 Id
        {
            get
            {
                return 0xff;
            }
        }

        public bool Init()
        {
            return OnInit();
        }

        public bool PostInit()
        {
            return OnPostInit();
        }

        public void PreDestroy()
        {
            OnPreDestroy();
        }

        public void Destroy()
        {
            OnDestroy();
        }

        public void Tick(int step)
        {
            OnTick(step);
        }

        protected virtual bool OnInit()
        {
            return true;
        }

        protected virtual bool OnPostInit()
        {
            return true;
        }

        protected virtual void OnPreDestroy()
        {
        }

        protected virtual void OnDestroy()
        {
        }

        protected virtual void OnTick(int step)
        {
        }

        public int RegisterMsgHandler<T>(ProtoID protoID, T msgHandler)
        {
            return env.BusService.RegisterMsgHandler(protoID, msgHandler);
        }

        //////////////////////////////////////////////////////////////////////////
        // Data Members
        protected ServerEnvironment env;
    }
}
