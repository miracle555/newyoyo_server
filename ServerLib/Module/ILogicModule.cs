﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerLib.Bus;

namespace Module
{
    /// <summary>
    ///Module接口
    /// </summary>
    public interface ILogicModule 
    {
        /// <summary>
        /// module init
        /// </summary>
        bool Init();

        /// <summary>
        /// module PostInit
        /// </summary>
        bool PostInit();

        /// <summary>
        /// module PreDestroy
        /// </summary>
        void PreDestroy();

        /// <summary>
        /// module destroy
        /// </summary>
        void Destroy();

        /// <summary>
        /// update
        /// </summary>
        /// <param name="step"></param>
        void Tick();
    }

}
