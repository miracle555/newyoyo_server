﻿/* ============================================================
* Author:       liubingsheng
* Time:         2014/11/27 11:18:27
* FileName:     ModuleManager
* Purpose:      Module管理器
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Event;
using Log;
using Utility.Debugger;
using ServerLib;

using System.Reflection;

namespace Module
{
    //ModuleManager实现
    internal class ModuleManager : IModuleManager
    {
        protected HashSet<ILogicModule> setModules = new HashSet<ILogicModule>();

        public ModuleManager()
        {
        }

        public void RegisterModule(ILogicModule module)
        {
            setModules.Add(module);
        }

        public bool Init()
        {
            foreach (var module in setModules)
            {
                try
                {
                    if(!module.Init())
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    LogSys.Warn("[Module]Module {0} Init Exception: {1}", module.GetType().Name, ex.Message);
                    return false;
                }
            }

            foreach (var module in setModules)
            {
                try
                {
                    if (!module.PostInit())
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    LogSys.Warn("[Module]Module {0} PostInit Exception: {1}", module.GetType().Name, ex.Message);

                    return false;
                }
            }

            return true;
        }

        public void Destroy()
        {
            foreach (var module in setModules)
            {
                try
                {
                    module.PreDestroy();
                }
                catch (Exception ex)
                {
                    LogSys.Warn("[Module]Module {0} PreDestroy Exception: {1}", module.GetType().Name, ex.Message);
                }
            }
            
            foreach (var module in setModules)
            {
                try
                {
                    module.Destroy();
                }
                catch (Exception ex)
                {
                    LogSys.Warn("[Module]Module {0} Destroy Exception: {1}", module.GetType().Name, ex.Message);
                }
            }

            setModules.Clear();
        }

        public void Tick()
        {
            foreach (var module in setModules)
            {
                DateTime startTime = DateTime.Now;

                module.Tick();

                TimeSpan span = DateTime.Now - startTime;
                if (span.TotalMilliseconds > 50.0f)
                {
                    if (span.TotalMilliseconds > 2000.0f)
                    {
                        LogSys.Warn("[ModuleUpdate]delta = {0} module = {1}", span.TotalMilliseconds, module.GetType().Name);
                    }
                    else
                    {
                        LogSys.Info("[ModuleUpdate]delta = {0} module = {1}", span.TotalMilliseconds, module.GetType().Name);
                    }
                }
            }
        }
    }
}
