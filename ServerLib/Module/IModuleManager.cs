﻿/* ============================================================
* Author:       liubingsheng
* Time:         2014/11/27 11:18:27
* FileName:     IModuleManager
* Purpose:      Module管理器接口
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module
{
    //ModuleManager接口
    public interface IModuleManager
    {
        bool Init();

        void Destroy();

        void Tick();

        /// <summary>
        /// 注册需要加载的module
        /// </summary>
        /// <param name="id">module id</param>
        /// <param name="moduleType">module type</param>
        void RegisterModule(ILogicModule module);
    }
}
