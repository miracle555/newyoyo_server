﻿/* ============================================================
* Author:       liubingsheng
* Time:         2014/12/8 12:03:32
* FileName:     Session
* Purpose:      连接会话
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Protocols;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Concurrent;
using Config;
using Log;
using Event;

namespace ServerLib.Session
{
    public class SessionConfig
    {
        public int ProtoBodyDelayParse
        {
            get;
            set;
        }

        public int DelaySend
        {
            get;
            set;
        }
        public SessionConfig()
        {
            ProtoBodyDelayParse = 0;
            DelaySend = 1;
        }
    }

    //连接会话
    internal class Session
    {
        const int BUFFER_LEN = 65536;
        const int STREAM_LEN_MAX = 65536;

        enum SessionState
        {
            State_Connected = 1,        //已连接，开始收发包
            State_Disconnect = 2,       //检测到断开连接
        }

        public UInt32 sessionId
        {
            get;
            private set;
        }

        Socket sock;

        //消息缓冲
        byte[] buffer = new byte[BUFFER_LEN];
        MemoryStream bufferStream = new MemoryStream();
        List<Protocol> protos = new List<Protocol>();
        SessionState state = SessionState.State_Connected;
        SessionConfig config;
        bool isServerSession;

        SessionManager mgr;

        public Session(SessionConfig config, UInt32 id, Socket sock, bool isServerSession, SessionManager mgr)
        {
            sessionId = id;
            this.sock = sock;
            this.mgr = mgr;
            this.config = config;
            this.isServerSession = isServerSession;
        }

        public void Init()
        {
            RecvMessage();
        }

        async void RecvMessage()
        {
            if (state != SessionState.State_Connected)
            {
                return;
            }

            try
            {
                int bytesRead = await sock.RecvAsync(buffer, 0, BUFFER_LEN, SocketFlags.None);
                if (bytesRead > 0)
                {
                    long pos = bufferStream.Position;
                    bufferStream.Position = bufferStream.Length;
                    bufferStream.Write(buffer, 0, bytesRead);
                    bufferStream.Position = pos;

                    while (true)
                    {
                        pos = bufferStream.Position;
                        Protocol proto = ProtocolSeg(bufferStream, isServerSession, config.ProtoBodyDelayParse == 1);

                        if (proto == null)
                        {
                            break;
                        }

                        protos.Add(proto);
                    }

                    if (bufferStream.Length == bufferStream.Position)
                    {
                        bufferStream.Position = 0;
                        bufferStream.SetLength(0);
                    }
                    else if (bufferStream.Position >= STREAM_LEN_MAX)
                    {
                        int len = (int)(bufferStream.Length - bufferStream.Position);
                        byte[] temp = new byte[len];
                        bufferStream.Read(temp, 0, len);
                        bufferStream.SetLength(0);
                        bufferStream.Write(temp, 0, len);
                        bufferStream.Position = 0;
                    }

                    RecvMessage();
                }
                else
                {
                    OnSocketDisconnect(DisconnReason.Passive);
                }
            }
            catch (SocketException sEx)
            {
                LogSys.Info("[Session.HandleReceive]SocketException : {0} ErrorCode: {1}, 远程强行关闭socket", sEx.Message, sEx.ErrorCode);
                if (sEx.ErrorCode == 10054)
                {
                    OnSocketDisconnect(DisconnReason.Passive);
                }
                else
                {
                    OnSocketDisconnect(DisconnReason.Exception);
                }
            }
            catch(System.ObjectDisposedException)
            {
                OnSocketDisconnect(DisconnReason.Exception);
            }
            catch (Exception ex)
            {
                LogSys.Warn("[Session.HandleReceive]Exception : {0} {1}", ex.Message, ex.HResult);
                OnSocketDisconnect(DisconnReason.Exception);
            }
        }

        public async void SendMessage(Stream stream, Action callback = null)
        {
            if (state != SessionState.State_Connected)
            {
                return;
            }

            try
            {
                byte[] buf = new byte[stream.Length];
                stream.Position = 0;
                stream.Read(buf, 0, (int)stream.Length);
                int sendSize = await sock.SendAsync(buf, 0, buf.Length, SocketFlags.None);
                if(null != callback)
                {
                    callback();
                }
                //LogSys.Info("[Session.SendMessage]SendMessage : sendSzie: {0}", sendSize);
            }
            catch (SocketException sEx)
            {
                LogSys.Info("[Session.SendMessage]SocketException : {0} ErrorCode: {1}", sEx.Message, sEx.ErrorCode);
                if (sEx.ErrorCode == 10054)
                {
                    OnSocketDisconnect(DisconnReason.Passive);
                }
                else
                {
                    OnSocketDisconnect(DisconnReason.Exception);
                }
            }
            catch (Exception ex)
            {
                LogSys.Info("[Session.SendMessage]Exception : {0},{1}", ex.HResult, ex.Message);
                OnSocketDisconnect(DisconnReason.Exception);
            }
        }

        /// <summary>
        /// 主动断开连接
        /// </summary>
        public void Disconnect(DisconnReason reason = DisconnReason.Active)
        {
            //SendAction();

            OnSocketDisconnect(reason);
        }

        void OnSocketDisconnect(DisconnReason reason)
        {
            try
            {
                if(state != SessionState.State_Disconnect)
                {
                    state = SessionState.State_Disconnect;

                    mgr.HandleDisconnected(sessionId, reason);

                    sock.Shutdown(SocketShutdown.Both);
                    sock.Close();
                }
            }
            catch(Exception)
            {

            }
        }

        public void Update()
        {
            if (state != SessionState.State_Connected)
            {
                return;
            }

            foreach(var proto in protos)
            {
                mgr.HandleMessage(sessionId, proto);
            }
            protos.Clear();

            //SendAction();
        }

        //断包器,先使用New实现.性能优化时可以考虑使用static或者ObjPool
        public static Protocol ProtocolSeg(System.IO.Stream stream, bool isServerSession, bool isDelayParse)
        {
            Protocol protocol = new Protocol();
            if (isDelayParse)
            {
                if (protocol.DeserializeHead(stream, isServerSession) < 0)
                    return null;
            }
            else
            {
                if (protocol.Deserialize(stream, isServerSession) < 0)
                    return null;
            }

            return protocol;
        }

    }
}
