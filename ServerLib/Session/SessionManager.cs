﻿/* ============================================================
* Author:       liubingsheng
* Time:         2014/12/8 11:58:53
* FileName:     SessionManager
* Purpose:      Session管理器
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config;
using Protocols;
using Utility.Common;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Log;
using Event;
using System.Collections.Concurrent;

namespace ServerLib.Session
{
    public enum DisconnReason
    {
        None,
        Active,             //服务器主动断开
        Passive,            //被动断开
        Exception,          //异常断开
    }

    /// <summary>
    /// 建立连接
    /// </summary>
    /// <param name="remoteEndPoint"></param>
    /// <param name="sessionId"></param>
    public delegate void HandleConnected(IPEndPoint remoteEndPoint, UInt32 sessionId);

    /// <summary>
    /// 建立连接失败
    /// </summary>
    /// <param name="remoteEndPoint"></param>
    public delegate void HandleConnectFailed(IPEndPoint remoteEndPoint);

    /// <summary>
    /// 消息回调
    /// </summary>
    /// <param name="sessionId"></param>
    /// <param name="proto"></param>
    /// <returns></returns>
    public delegate void HandleMessage(UInt32 sessionId, Protocol proto);

    /// <summary>
    /// 断开连接
    /// </summary>
    /// <param name="sessionId"></param>
    /// <param name="reason"></param>
    public delegate void HandleDisconnected(UInt32 sessionId, DisconnReason reason);

    //Session管理器
    public class SessionManager
    {
        SessionConfig config;
        LinkedList<IPEndPoint> connectingList = new LinkedList<IPEndPoint>();
        Dictionary<UInt32, Session> sessionMap = new Dictionary<UInt32, Session>();
        IdGenerator idGen = new IdGenerator();
        Socket acceptSock;
        bool isServerSession = false;

        /// <summary>
        /// 连接成功
        /// </summary>
        public event HandleConnected OnConnected;

        /// <summary>
        /// 连接失败
        /// </summary>
        public event HandleConnectFailed OnConnectFailed;

        /// <summary>
        /// 消息到来
        /// </summary>
        public event HandleMessage OnMessage;

        /// <summary>
        /// 连接断开
        /// </summary>
        public event HandleDisconnected OnDisconnected;

        public SessionManager(bool isServerSession)
        {
            this.isServerSession = isServerSession;
        }

        public void Init(SessionConfig config, IPEndPoint listenEndPoint = null, List<IPEndPoint> lstConnectEndPoints = null)
        {
            this.config = config;
            if (listenEndPoint != null)
            {
                if (listenEndPoint.Port != 0)
                {
                    LogSys.Info("[SessionManager.Start]......listening {0}", listenEndPoint.ToString());

                    acceptSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    acceptSock.Bind(listenEndPoint);
                    acceptSock.Listen(128 * 8);
                    StartListen(acceptSock);
                }
            }
            if (lstConnectEndPoints != null)
            {
                foreach (var item in lstConnectEndPoints)
                {
                    Connect(item);
                }
            }
        }

        public void Stop()
        {
            if (acceptSock != null)
            {
                acceptSock.Shutdown(SocketShutdown.Both);
                acceptSock.Close();
            }
        }

        //发送协议
        public void SendMessage(UInt32 sessionId, ProtoBody msg)
        {
            Protocol proto = new Protocol(msg);
            SendMessage(sessionId, proto);
        }

        public void SendMessage(UInt32 sessionId, Protocol proto, Action callback = null)
        {
            if (proto.MsgId != MSGID.HeartbeatReq && proto.MsgId != MSGID.HeartbeatRsp)
            {
                if (proto.MsgId == 0)
                {
                    int a = 1;
                    a++;
                }
                LogSys.Debug("sessionId:{0} send {1}", sessionId, proto.MsgId);
            }
                
            using (MemoryStream stream = new MemoryStream())
            {
                proto.Serialize(stream, isServerSession);

                if (!sessionMap.ContainsKey(sessionId))
                {
                    LogSys.Info("[SESSIONMGR] can not find session {0}, proto {1} send failed!", sessionId, proto.MsgId);
                    return;
                }

                sessionMap[sessionId].SendMessage(stream, callback);
            }
        }

        public void Update()
        {
            List<UInt32> lstKeys = new List<UInt32>();
            lstKeys.AddRange(sessionMap.Keys);

            foreach (var key in lstKeys)
            {
                if (sessionMap.ContainsKey(key))
                {
                    sessionMap[key].Update();
                }
            }
        }

        /// <summary>
        /// 开始监听
        /// </summary>
        /// <param name="acceptSock"></param>
        async void StartListen(Socket acceptSock)
        {
            try
            {
                Socket sock = await acceptSock.AcceptAsync();

                LogSys.Info("[SessionManager][HandleAccept]有新连接到来");

                if (sock.Connected)
                {
                    UInt32 sessionId = (UInt32)idGen.GenerateSerialId();
                    Session session = new Session(config, sessionId, sock, isServerSession, this);
                    sessionMap.Add(sessionId, session);
                    OnConnected((IPEndPoint)sock.RemoteEndPoint, sessionId);
                    session.Init();
                }
                else
                {
                    LogSys.Warn("[SessionManager][HandleAccept] 悲剧");
                }
            }
            catch (SocketException ex)
            {
                LogSys.Warn("[SessionManager][HandleAccept]SocketException: {0} ErrorCode: {1}", ex.Message, ex.ErrorCode);
            }
            catch (System.ObjectDisposedException ex)
            {
                LogSys.Warn("[SessionManager][HandleAccept]ObjectDisposedException: {0}", ex.Message);
            }
            catch (Exception ex)
            {
                LogSys.Warn("[SessionManager][HandleAccept]Exception: {0}", ex.Message);
            }
            finally
            {
                LogSys.Info("[SessionManager.HandleAccept]继续监听下一个连接");
                StartListen(acceptSock);
            }
        }

        /// <summary>
        /// 主动连接
        /// </summary>
        /// <param name="endpoint">endpoint</param>
        async void Connect(IPEndPoint endpoint)
        {
            if (connectingList.Contains(endpoint))
            {
                return;
            }
            connectingList.AddLast(endpoint);

            LogSys.Info("[SessionManager.Connect]endpoint = {0}", endpoint.ToString());

            try
            {
                Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                await sock.ConnectAsync(endpoint);

                if (sock.Connected)
                {
                    UInt32 sessionId = (UInt32)idGen.GenerateSerialId();
                    Session session = new Session(config, sessionId, sock, isServerSession, this);
                    sessionMap.Add(sessionId, session);
                    OnConnected((IPEndPoint)sock.RemoteEndPoint, sessionId);
                    session.Init();
                }
                else
                {
                    OnConnectFailed(endpoint);
                }

                if (connectingList.Contains(endpoint))
                {
                    connectingList.Remove(endpoint);
                }
            }
            catch (SocketException sEx)
            {
                LogSys.Warn("[SessionManager][Connect]SocketException: {0} ErrorCode: {1}", sEx.Message, sEx.ErrorCode);
            }
            catch (System.ObjectDisposedException ex)
            {
                LogSys.Warn("[SessionManager][Connect]ObjectDisposedException: {0}", ex.Message);
            }
            catch (Exception ex)
            {
                LogSys.Warn("[SessionManager][Connect]Exception: {0}", ex.Message);
            }
        }

        ConcurrentQueue<Socket> newClientSockets = new ConcurrentQueue<Socket>();

        struct stSocket
        {
            public stSocket(Socket s, IPEndPoint end)
            {
                sock = s;
                endpoint = end;
            }

            public Socket sock;
            public IPEndPoint endpoint;
        }

        public void HandleDisconnected(UInt32 sessionId, DisconnReason reason)
        {
            OnDisconnected(sessionId, reason);
        }

        public void HandleMessage(UInt32 sessionId, Protocol proto)
        {
            if(proto.MsgId != MSGID.HeartbeatReq && proto.MsgId != MSGID.HeartbeatRsp)
            {
                LogSys.Debug("sessionId:{0} recv {1}", sessionId, proto.MsgId);
            }

            OnMessage(sessionId, proto);
        }

        /// <summary>
        /// 主动断开连接
        /// </summary>
        /// <param name="sessionId">session id</param>
        /// <param name="reason">reason</param>
        public void Disconnect(UInt32 sessionId, DisconnReason reason = DisconnReason.Active)
        {
            if (sessionMap.ContainsKey(sessionId))
            {
                sessionMap[sessionId].Disconnect(reason);
            }
        }
    }
}
