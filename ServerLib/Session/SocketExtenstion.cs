﻿/* ============================================================
* Author:       刘冰生
* Time:         2017/1/22 12:03:32
* FileName:     SocketExtension.cs
* Purpose:      socket扩展
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.CompilerServices;

namespace ServerLib.Session
{
    public sealed class SocketAwaitable : INotifyCompletion
    { 
        private readonly static Action SENTINEL = () => { };
        internal bool m_wasCompleted; 
        internal Action m_continuation; 
        internal SocketAsyncEventArgs m_eventArgs;
        public SocketAwaitable(SocketAsyncEventArgs eventArgs) 
        { 
            if (eventArgs == null) throw new ArgumentNullException("eventArgs"); 
            m_eventArgs = eventArgs; 
            eventArgs.Completed += delegate 
            { 
                var prev = m_continuation ?? Interlocked.CompareExchange(ref m_continuation, SENTINEL, null);
                if (prev != SENTINEL) prev(); 
            }; 
        }
        internal void Reset() 
        { 
            m_wasCompleted = false; 
            m_continuation = null; 
        }
        public SocketAwaitable GetAwaiter() { return this; }
        public bool IsCompleted { get { return m_wasCompleted; } }
        public void OnCompleted(Action continuation) 
        { 
            if (m_continuation == SENTINEL || Interlocked.CompareExchange(ref m_continuation, continuation, null) == SENTINEL) 
            { 
                Task.Run(continuation); 
            } 
        }
        public void GetResult() 
        { 
            if (m_eventArgs.SocketError != SocketError.Success) 
                throw new SocketException((int)m_eventArgs.SocketError); 
        } 
    }

    public static class SocketExtensions
    {
        public static SocketAwaitable ReceiveAsync(this Socket socket, SocketAwaitable awaitable)
        {
            awaitable.Reset();
            if (!socket.ReceiveAsync(awaitable.m_eventArgs))
                awaitable.m_wasCompleted = true;
            return awaitable;
        }
        public static SocketAwaitable SendAsync(this Socket socket, SocketAwaitable awaitable)
        {
            awaitable.Reset();
            if (!socket.SendAsync(awaitable.m_eventArgs))
                awaitable.m_wasCompleted = true;
            return awaitable;
        }

        public static Task<int> RecvAsync(this Socket socket, byte[] buffer, int offset, int size, SocketFlags socketFlags)
        {
            var tcs = new TaskCompletionSource<int>(socket);
            socket.BeginReceive(buffer, offset, size, socketFlags, iar =>
            {
                var t = (TaskCompletionSource<int>)iar.AsyncState;
                var s = (Socket)t.Task.AsyncState;
                try { t.TrySetResult(s.EndReceive(iar)); }
                catch (Exception exc) { t.TrySetException(exc); }
            }, tcs);
            return tcs.Task;
        }

        public static Task ConnectAsync(this Socket socket, EndPoint remoteEP)
        {
            return Task.Factory.FromAsync(socket.BeginConnect, socket.EndConnect, remoteEP, null);
        }

        public static Task<Socket> AcceptAsync(this Socket socket)
        {
            return Task<Socket>.Factory.FromAsync(socket.BeginAccept, socket.EndAccept, null);
        }

        public static Task<int> SendAsync(this Socket socket, byte[] buffer, int offset, int size, SocketFlags socketFlags)
        {
            var tcs = new TaskCompletionSource<int>(socket);
            socket.BeginSend(buffer, offset, size, socketFlags, iar =>
            {
                var t = (TaskCompletionSource<int>)iar.AsyncState;
                var s = (Socket)t.Task.AsyncState;
                try { t.TrySetResult(s.EndSend(iar)); }
                catch (Exception exc) { t.TrySetException(exc); }
            }, tcs);
            return tcs.Task;
        }
    }
}
