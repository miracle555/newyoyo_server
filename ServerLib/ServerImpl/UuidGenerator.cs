﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config;
using Utility.Timer;
using Utility.Coroutines;
using Utility.Common;

namespace ServerLib
{
    /// <summary>
    /// uuid生成器
    /// </summary>
    public sealed class UuidGenerator
    {
        public UInt64 lobbyid = 0; // 4位 9999
        public UInt64 second = 0; // 9位 30年
        public UInt64 increment = 0; // 4位 9999

        public DateTime lastTime = DateTime.Now;

        public void Update()
        {
            TimeSpan span = DateTime.Now - DateTime.Parse("2015/1/1 0:00");
            UInt64 intervalSecond = (UInt64)span.Seconds;
            if(intervalSecond != second)
            {
                second = intervalSecond;
                increment = 0;
            }
        }

        public UInt64 GenerateUuid()
        {
            increment++;
            UInt64 uuid = lobbyid << 13;
            uuid += second << 4;
            uuid += increment;
            return uuid;
        }
    }
}
