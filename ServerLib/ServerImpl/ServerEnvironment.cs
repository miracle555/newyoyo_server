﻿/* ============================================================
* Author:       liubingsheng
* Time:         2014/11/26 15:45:52
* FileName:     env
* Purpose:      env存放全局数据
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config;
using Utility.Timer;
using Event;
using Module;
using ServerLib.Bus;
using Protocols;
using Log;
using Utility.Coroutines;
using Utility.Common;
using Redis;
using StaticData;
using System.Diagnostics;
using System.Threading;
using System.Collections.Concurrent;

namespace ServerLib
{
    /// <summary>
    /// 把await后代码切回主线程调用，就不需要考虑线程同步了
    /// </summary>
    public sealed class SingleThreadSynchronizationContext : SynchronizationContext
    {
        private readonly ConcurrentQueue<KeyValuePair<SendOrPostCallback, object>> m_queue = new ConcurrentQueue<KeyValuePair<SendOrPostCallback, object>>();

        public override void Post(SendOrPostCallback d, object state)
        {
            //LogSys.Info("[RunOnCurrentThread] Post *************************************************************");

            m_queue.Enqueue(new KeyValuePair<SendOrPostCallback, object>(d, state));
        }

        public override void Send(SendOrPostCallback d, object state)
        {
            //LogSys.Info("[RunOnCurrentThread] Send $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

            base.Send(d, state);
        }

        public void RunOnCurrentThread()
        {
            //LogSys.Info("[RunOnCurrentThread] Start, =================================================");

            KeyValuePair<SendOrPostCallback, object> workItem;
            List<KeyValuePair<SendOrPostCallback, object>> lstItems = new List<KeyValuePair<SendOrPostCallback, object>>();

            for (int i = 0; i < m_queue.Count; i++)
            {
                if (m_queue.TryDequeue(out workItem))
                {
                    lstItems.Add(workItem);
                }
                else
                {
                    Debug.Assert(false);
                }
            }

            foreach (var item in lstItems)
            {
                item.Key(item.Value);
            }

            //LogSys.Info("[RunOnCurrentThread] Finish, lstItems={0}, =================================================", lstItems.Count);
        }
    }

    //存放Server的全局引用的类
    public class env
    {
        public static  ServerConfig Config { get; private set; }

        public static  IModuleManager ModuleManager { get; private set; }
 
        public static IBusService BusService { get; private set; }

        public static ITimerManager Timer { get; private set; }

        public static IEventSystem eventSystem { get; private set; }
   
        public static  RedisSystem Redis { get; private set; }
 
        public static UuidGenerator IdGen { get; private set; }

        /// <summary>
        /// 静态数据管理器
        /// </summary>
        public static StaticDataManager sdata { get; private set; }

        public static void Init(ServerConfig config)
        {
            Console.Title = Process.GetCurrentProcess().ProcessName;

            Timer = new TimerManager();
            IdGen = new UuidGenerator();
            Config = config;
            eventSystem = new EventSystem();
            BusService = new BusService();
            Redis = new RedisSystem();
            ModuleManager = new ModuleManager();
            sdata = new StaticDataManager();
        }

        public static bool IsAlive = true;

        public static bool Start()
        {
            ServerConfig config = Config;

            if (!Init())
            {
                Destroy();
                return false;
            }

            while (IsAlive)
            {
                //LogSys.Debug("Env.Start1");

                DateTime startTime = DateTime.Now;
                try
                {
                    Update();
                }
                catch (Exception ex)
                {
                    if (Config.DumpCfg.DumpSwitch)
                    {
                        Utility.ExceptionHandle.StandardExceptionHandler.HandleException(ex, (Utility.ExceptionHandle.MiniDumper.Typ)Config.DumpCfg.DumpSetting);
                    }
                    LogSys.ErrorModule("Exception", "Exception: {0}. Stack {1}", ex.Message, ex.StackTrace);
                    if (null != ex.InnerException)
                    {
                        LogSys.ErrorModule("Exception", "InnerException: {0}. Stack {1}", ex.InnerException.Message, ex.InnerException.StackTrace);
                    }
                    continue;
                }

                //LogSys.Debug("Env.Start2");

                TimeSpan span = DateTime.Now - startTime;
                if (span.TotalMilliseconds > 500.0f)
                {
                    LogSys.Warn("[ServerUpdate]delta = {0}", span.TotalMilliseconds);
                }

                if (span.TotalMilliseconds < 50.0f)
                {
                    Thread.Sleep(10);
                }

                //LogSys.Debug("Env.Start3");
                //LogSys.Debug("==========================================================");
            }

            LogSys.Warn("Env.Start, Destroy");
            Destroy();

            return true;
        }

        public static bool Init()
        {
            // 设置同步上下文环境
            SingleThreadSynchronizationContext context = new SingleThreadSynchronizationContext();
            SynchronizationContext.SetSynchronizationContext(context);

            LogSys.Init(Config.LogCfg);
            LogSys.RegisterLogImpl(new FileLog(Config.LogCfg, Config.BusCfg.LocalAddress.ToString()));

            Redis.Init("127.0.0.1", 6379);
            BusService.Init(Config.BusCfg);

            Redis.getObject("staticdata", sdata);

            if(!ModuleManager.Init())
            {
                return false;
            }

            return true;
        }

        public static void Update()
        {
            // 执行await后的回调函数
            SingleThreadSynchronizationContext context = SynchronizationContext.Current as SingleThreadSynchronizationContext;
            context.RunOnCurrentThread();

            BusService.Update();
            //EventSystem.Update();
            ModuleManager.Tick();
            Timer.Update();
            IdGen.Update();
        }

        public static void Destroy()
        {
            BusService.Stop();
            ModuleManager.Destroy();
        }
    }
}
