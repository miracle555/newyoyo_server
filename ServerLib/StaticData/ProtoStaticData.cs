﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;
using Redis;
using System.IO;

namespace StaticData
{
    [ProtoContract]
    public class SpecificCfgItem
    {
        [ProtoMember(1)]
        public UInt32 m_uniqueid = 0;

        [ProtoMember(2)]
        public UInt32 m_effect_id = 0;

        /// <summary>
        /// 等级
        /// </summary>
        [ProtoMember(3)]
        public UInt32 m_level = 0;

        /// <summary>
        /// 名称
        /// </summary>
        [ProtoMember(4)]
        public string m_name = "";

        /// <summary>
        /// 模型
        /// </summary>
        [ProtoMember(5)]
        public string m_model = "";

        /// <summary>
        /// 眩晕时间
        /// </summary>
        [ProtoMember(6)]
        public float m_vertigo_time = 0.0f;

        /// <summary>
        /// 眩晕概率
        /// </summary>
        [ProtoMember(7)]
        public float m_vertigo_rate = 0.0f;

        /// <summary>
        /// 定身时间
        /// </summary>
        [ProtoMember(8)]
        public float m_fixed_boddy_time = 0.0f;

        /// <summary>
        /// 定身概率
        /// </summary>
        [ProtoMember(9)]
        public float m_fixed_boddy_rate = 0.0f;

        /// <summary>
        /// 暴击概率，暴击后双倍伤害
        /// </summary>
        [ProtoMember(10)]
        public float m_crit_rate = 0.0f;

        /// <summary>
        /// 闪避概率，闪避躲避全部伤害
        /// </summary>
        [ProtoMember(11)]
        public float m_dodge_rate = 0.0f;

        /// <summary>
        /// 护盾生命
        /// </summary>
        [ProtoMember(12)]
        public UInt32 m_shield_life = 0;

        /// <summary>
        /// 每隔一段时间产生一个护盾，单位：秒
        /// </summary>
        [ProtoMember(13)]
        public float m_shield_rate = 0.0f;
    }

    [ProtoContract]
    public class SpecificConfig
    {
        [ProtoMember(1)]
        protected Dictionary<UInt32, SpecificCfgItem> mapItems = new Dictionary<UInt32, SpecificCfgItem>();

        public void AddItem(SpecificCfgItem item)
        {
            mapItems.Add(item.m_uniqueid, item);
        }

        public bool HasItem(UInt32 uniqueId)
        {
            return mapItems.ContainsKey(uniqueId);
        }
    }

    [ProtoContract]
    public class SArrowUnit
    {
        [ProtoMember(1)]
        public UInt32 m_arrowid = 0;

        [ProtoMember(2)]
        public UInt32 m_arrow_level = 0;
    };

    [ProtoContract]
    public class SSpecialUnit
    {
        [ProtoMember(1)]
        public UInt32 m_effect_id;

        [ProtoMember(2)]
        public UInt32 m_effect_level;
    };

    [ProtoContract]
    public class UnitConfigItem
    {
        [ProtoMember(1)]
        public UInt32 m_spiritid = 0;

        [ProtoMember(2)]
        public string m_spirit_name = "";

        [ProtoMember(3)]
        public string m_model = "";

        [ProtoMember(4)]
        public float m_range = 0.0f;//	碰撞判定半径

        [ProtoMember(5)]
        public float m_move_speed = 0.0f;//	移动速度

        [ProtoMember(6)]
        public float m_turn_around_speed = 0.0f;//	转身速度

        [ProtoMember(7)]
        public UInt32 m_life_addition = 0;// 	生命加成

        [ProtoMember(8)]
        public SArrowUnit m_basic_arrow = new SArrowUnit(); //	基础箭只

        [ProtoMember(9)]
        public SArrowUnit m_default_grow_arrow = new SArrowUnit();//	默认蓄力箭只，格式：蓄力击id*等级

        [ProtoMember(10)]
        public List<SArrowUnit> m_buff_grow_arrows = new List<SArrowUnit>();//	buff蓄力箭只，格式：蓄力击id*等级，蓄力击id*等级…

        [ProtoMember(11)]
        public List<SSpecialUnit> m_specific_effects = new List<SSpecialUnit>();//	特殊效果加成，格式：特殊效果id*等级，特殊效果id*等级…

        [ProtoMember(12)]
        public UInt32 m_arrow_upper_limit = 0;//	箭只容纳上限，单位：支

        [ProtoMember(13)]
        public UInt32 m_arrow_recovery_second = 0;//	箭只恢复速度，单位：秒/支

        public UInt32 getArrowLevel(UInt32 arrowid)
        {
            foreach (var item in m_buff_grow_arrows)
            {
                if (item.m_arrowid == arrowid)
                {
                    return item.m_arrow_level;
                }
            }

            return 1;
        }
    }

    /// <summary>
    /// 精灵表
    /// </summary>
    [ProtoContract]
    public class UnitConfig
    {
        [ProtoMember(1)]
        protected Dictionary<UInt32, UnitConfigItem> mapItems = new Dictionary<uint, UnitConfigItem>();

        public UnitConfigItem GetItem(UInt32 spiritid)
        {
            if (mapItems.ContainsKey(spiritid))
            {
                return mapItems[spiritid];
            }

            return null;
        }

        public void AddItem(UnitConfigItem item)
        {
            if(HasItem(item.m_spiritid))
            {
                return;
            }

            mapItems.Add(item.m_spiritid, item);
        }

        public bool HasItem(UInt32 spiritid)
        {
            return mapItems.ContainsKey(spiritid);
        }
    }

    [ProtoContract]
    public class ArrowConfigItem
    {
        [ProtoMember(1)]
        public UInt32 m_id = 0;

        [ProtoMember(2)]
        public UInt32 m_arrow_id = 0;

        [ProtoMember(3)]
        public UInt32 m_level = 0;//	等级

        [ProtoMember(4)]
        public string m_name = "";//	名称

        [ProtoMember(5)]
        public string m_model = "";//	模型

        [ProtoMember(6)]
        public float m_move_speed = 0.0f;//	移动速度

        [ProtoMember(7)]
        public UInt32 m_damage = 0;//	初始击中伤害

        [ProtoMember(8)]
        public UInt32 m_spurting_damage = 0;//	溅射伤害，闪电、火焰、穿透

        [ProtoMember(9)]
        public UInt32 m_penetrate = 0;//	穿透敌人数量

        [ProtoMember(10)]
        public UInt32 m_range = 0;//	散射数量、闪电链数、火焰溅射范围

        [ProtoMember(11)]
        public string m_judge = "";//	特殊判定条件。当闪电时格式：数字 = 连接两个单位间的最大距离，超过此距离判定失败

        [ProtoMember(12)]
        public float m_lighting_distance = 0.0f;

    }

    [ProtoContract]
    public class ArrowConfig
    {
        [ProtoMember(1)]
        protected Dictionary<UInt32, ArrowConfigItem> mapItems = new Dictionary<uint, ArrowConfigItem>();

        public ArrowConfigItem GetItem(UInt32 arrowid, UInt32 level)
        {
            UInt32 id = arrowid * 1000 + level;

            if (mapItems.ContainsKey(id))
            {
                return mapItems[id];
            }

            return null;
        }

        public void AddItem(ArrowConfigItem item)
        {
            if(HasItem(item.m_id))
            {
                return;
            }

            mapItems.Add(item.m_id, item);
        }

        public bool HasItem(UInt32 arrowid, UInt32 level)
        {
            UInt32 id = arrowid * 1000 + level;
            return mapItems.ContainsKey(id);
        }

        public bool HasItem(UInt32 id)
        {
            return mapItems.ContainsKey(id);
        }
    }

    [ProtoContract]
    public class BuffConfigItem
    {
        [ProtoMember(1)]
        public UInt32 m_buff_id = 0;

        [ProtoMember(2)]
        public string m_name = "";//	名称

        [ProtoMember(2)]
        public UInt32 m_arrow_id = 0;//	触发的箭只

        [ProtoMember(2)]
        public float m_duration_second = 0.0f;// buff持续时间
    }

    [ProtoContract]
    public class BuffConfig
    {
        [ProtoMember(1)]
        protected Dictionary<UInt32, BuffConfigItem> mapItems = new Dictionary<uint, BuffConfigItem>();

        public BuffConfigItem GetItem(UInt32 itemid)
        {
            if (mapItems.ContainsKey(itemid))
            {
                return mapItems[itemid];
            }

            return null;
        }

        public void AddItem(BuffConfigItem item)
        {
            if(!HasItem(item.m_buff_id))
            {
                return;
            }

            mapItems.Add(item.m_buff_id, item);
        }

        public bool HasItem(UInt32 itemid)
        {
            return mapItems.ContainsKey(itemid);
        }
    }

    [ProtoContract]
    public class GodConfig
    {
        /// <summary>
        /// 战斗中英灵基础生命
        /// </summary>
        [ProtoMember(1)]
        public UInt32 initSpiritLife { get; set; }

        /// <summary>
        /// 混战模式单局战斗时间，单位：秒
        /// </summary>
        [ProtoMember(2)]
        public float battleLastSecond { get; set; }

        /// <summary>
        /// 混战模式单局人数上限
        /// </summary>
        [ProtoMember(3)]
        public UInt32 battleRoleLimit { get; set; }

        /// <summary>
        /// 混战模式游戏启动后仍可进入的时间，单位：秒
        /// </summary>
        [ProtoMember(4)]
        public float roomOpenSecond { get; set; }

        /// <summary>
        /// 混战模式单次击杀积分
        /// </summary>
        [ProtoMember(5)]
        public UInt32 killScore { get; set; }

        /// <summary>
        /// 初始英灵ID
        /// </summary>
        [ProtoMember(6)]
        public UInt32 initSpiritId { get; set; } 

        /// <summary>
        /// 玩家进入房间后保护时间
        /// </summary>
        [ProtoMember(7)]
        public float protectSecond { get; set; }

        /// <summary>
        /// 死亡自动复活时间
        /// </summary>
        [ProtoMember(8)]
        public float autoReviveSecond { get; set; } 

        /// <summary>
        /// buff生存时间
        /// </summary>
        [ProtoMember(9)]
        public float buffLifeTime { get; set; } 
    }

    /// <summary> 
    /// 静态数据管理器
    /// </summary>
    [ProtoContract]
    public class StaticDataManager
    {
        [ProtoMember(1)]
        public UnitConfig unit = new UnitConfig();

        [ProtoMember(2)]
        public ArrowConfig arrow = new ArrowConfig();

        [ProtoMember(3)]
        public SpecificConfig special = new SpecificConfig();

        [ProtoMember(4)]
        public GodConfig god = new GodConfig();

        [ProtoMember(5)]
        public BuffConfig buff = new BuffConfig();
    }
}