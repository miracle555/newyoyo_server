﻿/* ============================================================
* Author:       liubingsheng
* Time:         2014/11/11 9:56:15
* FileName:     BusService
* Purpose:      服务器间通信服务
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config;
using System.Net;
using ServerLib.Bus;
using Protocols;
using Log;
using System.IO;
using Utility.Debugger;
using ServerLib.Session;
using Event;

using System.Reflection;

namespace ServerLib.Bus
{
    public class BusService : IBusService
    {
        //连接信息
        class PeerState
        {
            public UInt32 serverid;
            public UInt32 sessionId;

            public PeerState(UInt32 serverid, UInt32 sessionId)
            {
                this.serverid = serverid;
                this.sessionId = sessionId;
            }
        }

        //主动连接信息
        class ConnectState : PeerState
        {
            public IPEndPoint endpoint;
            public bool connected;

            public ConnectState(UInt32 serverid, IPEndPoint endpoint)
                : base(serverid, 0)
            {
                this.endpoint = endpoint;
                connected = false;
            }
        }

        //所有连接列表
        List<PeerState> peerStateList = new List<PeerState>();

        private Dictionary<MSGID, Delegate> msgHandlerMap = new Dictionary<MSGID, Delegate>();

        Func<stServerBody, ProtoBody, bool> spyFunction = NullFunction;

        static bool NullFunction(stServerBody serverBody, ProtoBody body)
        {
            return false;
        }

        UInt32 GetServerId(UInt32 sessionId)
        {
            foreach(var item in peerStateList)
            {
                if(item.sessionId == sessionId)
                {
                    return item.serverid;
                }
            }

            return 0;
        }

        public void RegisterMsgHandler<T>(MSGID protoID, MsgHandler<T> msgHandler) where T : ProtoBody
        {
            MsgHandlerAll<T> handler = (protoBody, from, serverBody) =>
            {
                msgHandler(from, serverBody.accountId, protoBody);
            };

            RegisterHandlerInternal(protoID, handler);
        }

        public void RegisterMsgHandler<T>(MsgHandlerEx<T> msgHandler) where T : ProtoBody
        {
            MsgHandlerAll<T> handler = delegate(T protoBody, UInt32 from, stServerBody serverBody)
            {
                msgHandler(protoBody, from, serverBody.sessionId);
            };

            MSGID protoID = (MSGID)ProtoID.GetId(typeof(T));
            RegisterHandlerInternal(protoID, handler);
        }

        public void RegisterSpyFunction(Func<stServerBody, ProtoBody, bool> function)
        {
            spyFunction = function;
        }

        public void RegisterMsgHandler<T>(MSGID protoID, MsgHandlerAll<T> msgHandler) where T : ProtoBody
        {
            RegisterHandlerInternal(protoID, msgHandler);
        }

        void RegisterHandlerInternal<T>(MSGID protoID, MsgHandlerAll<T> msgHandler) where T : ProtoBody
        {
            MsgHandlerAll<ProtoBody> common_handler = delegate(ProtoBody protoBody, UInt32 from, stServerBody serverBody)
            {
                T body = protoBody as T;

                if(null != body)
                {
                    msgHandler(body, from, serverBody);
                }
            };

            msgHandlerMap.Add(protoID, common_handler);
        }

        public virtual void Init(BusConfig config)
        {
            this.config = config;

            ServerItems serverItems = new ServerItems();
            env.Redis.getObject("iptable", serverItems);

            this.serverItems = serverItems;

            sessionMgr.OnConnected += this.HandleConnected;
            sessionMgr.OnMessage += this.HandleMessage;
            sessionMgr.OnDisconnected += this.HandleDisconnected;

            SessionConfig sessionConfig = new SessionConfig();
            IPEndPoint listenEndPoint = null;
            List<IPEndPoint> lstConnectEndPoints = serverItems.GetConnectEndPoints(config.Peer);

            if(config.LocalAddress.IsGate())
            {
                sessionConfig.ProtoBodyDelayParse = 1;
            }
            else
            {
                listenEndPoint = serverItems.GetEndPoint(config.LocalAddress);
            }

            sessionMgr.Init(sessionConfig, listenEndPoint, lstConnectEndPoints);
        }

        public void Stop()
        {
            sessionMgr.Stop();
        }

        //////////////////////////////////////////////////////////////////////////
        // 消息发送接口实现
        public void SendMessageToServer(UInt32 serverid, ProtoBody msg)
        {
            PeerState state = FindState(serverid);
            if (null == state)
            {
                LogSys.Warn(string.Format("[BUS] can not find state for server {0}", serverid.ToString()));
                return;
            }

            sessionMgr.SendMessage(state.sessionId, msg);
        }

        public void SendMessageToServer(UInt32 serverid, Protocol proto)
        {
            PeerState state = FindState(serverid);
            if (null == state)
            {
                LogSys.Warn(string.Format("[BUS] can not find state for server {0}", serverid.ToString()));
                return;
            }

            sessionMgr.SendMessage(state.sessionId, proto);
        }

        public void SendMessage(UInt32 sessionId, ProtoBody msg, Action callback = null)
        {
            sessionMgr.SendMessage(sessionId, msg, callback);
        }

        public void SendMessage(UInt32 sessionId, Protocol proto, Action callback = null)
        {
            sessionMgr.SendMessage(sessionId, proto, callback);
        }
        public void SendMessage(string servertype, ProtoBody msg)
        {
            foreach(var item in peerStateList)
            {
                VirtualAddress address = ServerItems.GetVirtualAddress(item.serverid);
                if(address.StrServiceType == servertype)
                {
                    sessionMgr.SendMessage(item.sessionId, msg);
                }
            }
        }

        public void SendMessage(string servertype, Protocol proto)
        {
            foreach (var item in peerStateList)
            {
                VirtualAddress address = ServerItems.GetVirtualAddress(item.serverid);
                if (address.StrServiceType== servertype)
                {
                    sessionMgr.SendMessage(item.sessionId, proto);
                }
            }
        }

        public void Update()
        {
            sessionMgr.Update();
        }

        //连接建立回调
        public void HandleConnected(IPEndPoint remoteEndPoint, UInt32 sessionId)
        {
            LogSys.Info("[BusService.HandleConnected] peer = {0} sessionId = {1}",  remoteEndPoint.ToString(), sessionId);
            UInt32 serverid = serverItems.GetServerId(remoteEndPoint);
            if (0 != serverid)
            {
                AddRemoteService(sessionId, serverid);
  
                ServerRegisterSelfReq self = new ServerRegisterSelfReq();
                self.serverID = config.LocalAddress;

                LogSys.Info("[BusService.HandleConnected] peer = {0} sessionId = {1}, Send ServerRegisterSelfReq", remoteEndPoint.ToString(), sessionId);
                SendMessage(sessionId, self);
            }
        }

        void OnRegisterSelf(ServerRegisterSelfReq protoBody, UInt32 sessionId)
        {
            VirtualAddress from = new VirtualAddress(protoBody.serverID);

            LogSys.Info("[BusService.OnRegisterSelf] peer = {0} sessionId = {1}, Send ServerRegisterSelfReq", from.ToString(), sessionId);

            AddRemoteService(sessionId, protoBody.serverID);
        }

        public void HandleMessage(UInt32 sessionId, Protocol proto)
        {
            if (MSGID.ServerRegisterSelfReq == proto.MsgId)
            {
                OnRegisterSelf(proto.Body as ServerRegisterSelfReq, sessionId);
                return;
            }

            //[12/16/2014 刘冰生] 回调通知所有模块 
            try
            {
                UInt32 serverid = GetServerId(sessionId);
                if (msgHandlerMap.ContainsKey(proto.MsgId))
                {
                    MsgHandlerAll<ProtoBody> handler = msgHandlerMap[proto.MsgId] as MsgHandlerAll<ProtoBody>;
                    stServerBody serverBody = new stServerBody(proto.RoleId, proto.SessionId, proto.uuid);

                    if (!spyFunction(serverBody, proto.Body))
                    {
                        handler(proto.Body, serverid, serverBody);
                    }
                }
            }
            catch (System.Exception ex)
            {
                string errMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                string trace = ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace;

                LogSys.ErrorModule("Exception", "{0}:{1} {2}",sessionId, proto, errMsg);
                LogSys.ErrorModule("Trace", "{0}:{1} {2}",sessionId, proto, trace);
            }

            // 接收消息事件
            EventMsgReceived ev = new EventMsgReceived();
            ev.proto = proto;
            env.eventSystem.SendEvent(ev);
        }

        int FindPeerState(UInt32 serverid)
        {
            for (int i = 0; i < peerStateList.Count; i++)
            {
                if (peerStateList[i].serverid == serverid)
                {
                    return i;
                }
            }
            return -1;
        }

        private PeerState FindState(UInt32 serverid)
        {
            int index = FindPeerState(serverid);
            if (-1 == index)
            {
                return null;
            }

            return peerStateList[index];
        }

        public int AddRemoteService(UInt32 sessionId, UInt32 serverid)
        {
            if (FindPeerState(serverid) != -1)
            {
                LogSys.Warn(string.Format("Remote Service {0} already exist!", serverid));
                return -1;
            }

            PeerState state = new PeerState(serverid, sessionId);
            peerStateList.Add(state);

            //通知服务器连接上了
            EventServiceStart ev = new EventServiceStart();
            ev.serverid = state.serverid;
            env.eventSystem.SendEvent(ev);

            return 0;
        }

        public void HandleDisconnected(UInt32 sessionId, DisconnReason reason)
        {
        }

        /// <summary>
        /// 建立连接失败
        /// </summary>
        /// <param name="remoteEndPoint">endpoint</param>
        /// <param name="sessionId">sessionId</param>
        /// <param name="bActive">bActive</param>
        /// <param name="serverID">serverID</param>
        public void HandleConnectFailed(IPEndPoint remoteEndPoint)
        {

        }

        //////////////////////////////////////////////////////////////////////////
        // Data member 

        protected BusConfig config;

        protected ServerItems serverItems = new ServerItems();

        SessionManager sessionMgr = new SessionManager(true);
    }
}
