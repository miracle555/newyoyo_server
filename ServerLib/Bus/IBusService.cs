﻿/* ============================================================
* Author:       liubingsheng
* Time:         2014/11/11 9:56:15
* FileName:     IBusService
* Purpose:      Bus接口
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config;
using System.Net;
using ServerLib.Bus;
using Protocols;
using System.IO;
using ProtoBuf;

namespace ServerLib.Bus
{
    [ProtoContract]
    public class stServerBody
    {
        public stServerBody(UInt64 accountId, UInt32 sessionId, UInt64 uuid)
        {
            this.accountId = accountId;
            this.sessionId = sessionId;
            this.uuid = uuid;
        }

        [ProtoMember(1)]
        public UInt64 accountId = 0;

        [ProtoMember(2)]
        public UInt32 sessionId = 0;

        [ProtoMember(3)]
        public UInt64 uuid = 0;
    }

    /// <summary>
    /// 增强的报文处理接口样式
    /// </summary>
    /// <param name="protoBody"></param>
    /// <param name="from"></param>
    /// <param name="sessionId"></param>
    public delegate void MsgHandlerEx<T>(T protoBody, UInt32 from, UInt32 sessionId);

    /// <summary>
    /// 玩家消息
    /// </summary>
    /// <param name="protoBody"></param>
    /// <param name="from"></param>
    /// <param name="accountId"></param>
    public delegate void MsgHandler<T>(UInt32 from, UInt64 accountId, T protoBody);


    public delegate void MsgHandlerAll<T>(T protoBody, UInt32 from, stServerBody serverBody);

    /// <summary>
    ///BusService接口
    /// </summary>
    public interface IBusService
    {
        /// <summary>
        /// initialize with config
        /// </summary>
        /// <param name="config"></param>
        /// <param name="ipTables"></param>
        /// <param name="env"></param>
        void Init(BusConfig config);

        /// <summary>
        /// stop bus service
        /// </summary>
        void Stop();

        #region Message Send Interface
        /// <summary>
        /// 将消息发送到目标地址,会根据地址情况自动填充协议头声明
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        void SendMessageToServer(UInt32 serverid, ProtoBody msg);

        /// <summary>
        /// 将消息发送到目标地址,不会变更协议头声明
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="proto"></param>
        /// <returns></returns>
        void SendMessageToServer(UInt32 serverid, Protocol proto);

        void SendMessage(UInt32 sessionId, ProtoBody msg, Action callback = null);

        void SendMessage(UInt32 sessionId, Protocol proto, Action callback = null);

        void SendMessage(string servertype, ProtoBody msg);

        void SendMessage(string servertype, Protocol proto);

        #endregion

        /// <summary>
        /// update
        /// </summary>
        /// <param name="step"></param>
        void Update();

        void RegisterMsgHandler<T>(MsgHandlerEx<T> msgHandler) where T : ProtoBody;

        void RegisterMsgHandler<T>(MSGID protoID, MsgHandler<T> msgHandler) where T : ProtoBody;

        void RegisterMsgHandler<T>(MSGID protoID, MsgHandlerAll<T> msgHandler) where T : ProtoBody;

        void RegisterSpyFunction(Func<stServerBody, ProtoBody, bool> function);
    }
}
