﻿/********************************************************************
	created:	2014/12/02
	created:	2:12:2014   19:00
	file base:	ServiceType
	author:		刘冰生
	
	purpose:	预定义Service类型,此类型与虚拟地址的第三段对应
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLib.Bus
{
    /// <summary>
    ///预定义的Service类型
    /// </summary>
    public class ServiceType
    {
        /// <summary>
        /// reserved begin
        /// </summary>
        public const byte SERVICE_RESERVETYPE_BEGIN = 0;

        /// <summary>
        /// monitor
        /// </summary>
        public const byte SERVICE_MONITOR = 1;

        /// <summary>
        /// lobby
        /// </summary>
        public const byte SERVICE_LOBBY = 2;

        /// <summary>
        /// battle
        /// </summary>
        public const byte SERVICE_BATTLE = 3;

        /// <summary>
        /// gate
        /// </summary>
        public const byte SERVICE_GATE = 4;
        /// <summary>
        /// reserved end
        /// </summary>
        public const byte SERVICE_RESERVETYPE_END = 100;

        public static byte getServiceType(string type)
        {
            if(type == "monitor")
            {
                return SERVICE_MONITOR;
            }

            if (type == "lobby")
            {
                return SERVICE_LOBBY;
            }

            if (type == "battle")
            {
                return SERVICE_BATTLE;
            }

            if (type == "gate")
            {
                return SERVICE_GATE;
            }

            return 0;
        }

        public static string toString(byte type)
        {
            if (type == SERVICE_MONITOR)
            {
                return "monitor";
            }

            if (type == SERVICE_LOBBY)
            {
                return "lobby";
            }

            if (type == SERVICE_BATTLE)
            {
                return "battle";
            }

            if (type == SERVICE_GATE)
            {
                return "gate";
            }

            return "def";
        }
    }
}
