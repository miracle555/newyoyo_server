﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Protocols;

using Config;

namespace Event
{
	/// <summary>
    /// event id
    /// </summary>
	public class EventId
	{
		public static int EVENTID_BEGIN = 0;

		public static int EventNetIOThroughput = 1;
		public static int EventAddThroughputData = 2;
		public static int EventIsOpenNetIOThroughput = 3;
		public static int EventLogServerConnected = 4;
		public static int EventStaticDataTableChanged = 5;
		public static int EventNewDay = 6;
		public static int EventClock = 7;
		public static int EventConfigChanged = 8;
		public static int EventServiceStart = 9;
		public static int EventMsgReceived = 10;

		public static int EVENTID_END = EVENTID_BEGIN + 1000;

        public static int Event_PlayerLogin = 1001;
        public static int Event_PlayerLogout = 1002;
        public static int Event_CreateRole = 1003;
	}

	/// <summary>
	/// 获取NetIOThroughput
	/// </summary>
	public class EventNetIOThroughput : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventNetIOThroughput;
            }
        }
		
		//IsServer
		public bool IsServer
		{
			get;
			set;
		}
		
		//IsSend
		public bool IsSend
		{
			get;
			set;
		}
		
		//ProtoID
		public UInt16 ProtoID
		{
			get;
			set;
		}
		
		//ProtoName
		public string ProtoName
		{
			get;
			set;
		}
		
		//SendByteCount
		public int SendByteCount
		{
			get;
			set;
		}
		
		//ReceiveByteCount
		public int ReceiveByteCount
		{
			get;
			set;
		}

		public EventNetIOThroughput()
		{
		}
	}
	/// <summary>
	/// AddThroughputData
	/// </summary>
	public class EventAddThroughputData : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventAddThroughputData;
            }
        }
		
		//DataName
		public string DataName
		{
			get;
			set;
		}
		
		//Value
		public long Value
		{
			get;
			set;
		}

		public EventAddThroughputData()
		{
		}
	}
	/// <summary>
	/// 开启或关闭网络性能统计
	/// </summary>
	public class EventIsOpenNetIOThroughput : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventIsOpenNetIOThroughput;
            }
        }
		
		//是否开启
		public bool IsOpened
		{
			get;
			set;
		}

		public EventIsOpenNetIOThroughput()
		{
		}
	}
	/// <summary>
	/// 和LogServer连上
	/// </summary>
	public class EventLogServerConnected : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventLogServerConnected;
            }
        }
		
		//是否连接上
		public bool IsConnected
		{
			get;
			set;
		}

		public EventLogServerConnected()
		{
		}
	}
	/// <summary>
	/// 静态数据表变化
	/// </summary>
	public class EventStaticDataTableChanged : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventStaticDataTableChanged;
            }
        }
		
		//静态数据表的文件名
		public string StaticDataTableFileName
		{
			get;
			set;
		}

		public EventStaticDataTableChanged()
		{
		}
	}
	/// <summary>
	/// 触发跨天事件
	/// </summary>
	public class EventNewDay : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventNewDay;
            }
        }
		
		//触发日期
		public string Date
		{
			get;
			set;
		}

		public EventNewDay()
		{
		}
	}
	/// <summary>
	/// 触发整点事件
	/// </summary>
	public class EventClock : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventClock;
            }
        }
		
		//
		public DateTime Now
		{
			get;
			set;
		}
		
		//
		public DateTime Last
		{
			get;
			set;
		}

		public EventClock()
		{
		}
	}
	/// <summary>
	/// 配置文件发生改变
	/// </summary>
	public class EventConfigChanged : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventConfigChanged;
            }
        }
		
		//配置文件全名（包含尾缀，不包含路径）
		public string FullConfigFileName
		{
			get;
			set;
		}
		
		//变更后的配置对象
		public ServerConfig ChangedConfigObj
		{
			get;
			set;
		}

		public EventConfigChanged()
		{
		}
	}
	/// <summary>
	/// 服务器连接成功
	/// </summary>
	public class EventServiceStart : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventServiceStart;
            }
        }
		
		//远端服务器id
		public UInt32 serverid
		{
			get;
			set;
		}

		public EventServiceStart()
		{
		}
	}
	/// <summary>
	/// 接受到网络消息
	/// </summary>
	public class EventMsgReceived : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.EventMsgReceived;
            }
        }
		
		//远端服务器虚拟地址
		public UInt32 from
		{
			get;
			set;
		}
		
		//消息协议
		public Protocol proto
		{
			get;
			set;
		}

		public EventMsgReceived()
		{
		}
	}
}
