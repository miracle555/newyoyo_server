﻿/* ============================================================
* Author:       liubingsheng
* Time:         2016/11/29 14:52:25
* FileName:     RedisSystem
* Purpose:      Redis
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Config;
using System.Data;
using Log;
using Utility.Common;
using StackExchange.Redis;
using System.IO;
using ProtoBuf;

namespace Redis
{
    public class RedisSystem
    {
        //////////////////////////////////////////////////////////////////////////
        public RedisSystem()
        {
        }

        /// <summary>
        /// 内部初始化
        /// </summary>
        public void Init(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
            string server = ip + ":" + port.ToString() + ",allowAdmin=true";
            redis = ConnectionMultiplexer.Connect(server);
        }

        string ip = "";
        int port = 0;

        protected ConnectionMultiplexer redis;

        public string MergeKey(string table, string id)
        {
            string key = table + "_" + id;
            return key;
        }

        public bool getObject<T>(string key, T obj)
        {
            IDatabase db = redis.GetDatabase();
            RedisValue value = db.StringGet(key);
            if(value.IsNullOrEmpty)
            {
                return false;
            }

            if(!obj.SerializeFromBinary(value))
            {
                return false;
            }

            return true;
        }

        public bool setObject<T>(string key, T obj)
        {
            byte[] data;

            if (!obj.SerializeToBinary(out data))
            {
                return false;
            }

            return db.StringSet(key, data);
        }

        /// <summary>
        /// 往集合添加元素
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetAdd(string key, UInt64 value)
        {
            db.SetAdd(key, value, CommandFlags.FireAndForget);
            return true;
        }

        public bool SetRemove(string key, UInt64 value)
        {
            db.SetRemove(key, value, CommandFlags.FireAndForget);
            return true;
        }

        public IDatabase db
        {
            get
            {
                IDatabase db = redis.GetDatabase();
                return db;
            }
        }

        public IServer server
        {
            get
            {
                IServer server = redis.GetServer(this.ip, this.port);
                return server;
            }
        }

        public LuaScript Script
        {
            get;
            set;
        }
    }
}
