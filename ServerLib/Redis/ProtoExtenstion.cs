﻿/* ============================================================
* Author:       liubingsheng
* Time:         2016/11/29 14:52:25
* FileName:     RedisSystem
* Purpose:      Redis
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Config;
using System.Data;
using Log;
using Utility.Common;
using StackExchange.Redis;
using System.IO;
using ProtoBuf;

namespace Redis
{
    /// <summary>
    /// 数据访问扩展方法类
    /// </summary>
    public static class ProtoExtensions
    {
        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="buffer">数据字节数组</param>
        /// <param name="protoObject">需要填充数据的对象</param>        
        public static bool SerializeFromBinary(this object protoObject, byte[] buffer)
        {
            using (MemoryStream stream = new MemoryStream(buffer))
            {
                try
                {
                    Serializer.NonGeneric.Merge(stream, protoObject);
                }
                catch (ProtoBuf.ProtoException e)
                {
                    LogSys.Error("[ProtoExtensions.SerializeFromBinary] ProtoBuf error : {0}, protoObject type is {1}", e.Message, protoObject.GetType().ToString());
                    return false;
                }
                catch (Exception e)
                {
                    LogSys.Error("[ProtoExtensions.SerializeFromBinary] Other error : {0}, protoObject type is {1}", e.Message, protoObject.GetType().ToString());
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// 序列化Protobuf列对象
        /// </summary>
        /// <param name="protoColData">Proto列对象</param>
        /// <returns>返回流字节数组，若错误则返回null</returns>
        public static bool SerializeToBinary(this object protoObject, out byte[] data)
        {
            data = defaultBytes;
            using (MemoryStream stream = new MemoryStream())
            {
                try
                {
                    Serializer.NonGeneric.Serialize(stream, protoObject);
                }
                catch (ProtoBuf.ProtoException e)
                {
                    LogSys.Error("[ProtoExtensions.SerializeToBinary] ProtoBuf error : {0}, protoObject type is {1}", e.Message, protoObject.GetType().ToString());
                    return false;
                }
                catch (Exception e)
                {
                    LogSys.Error("[ProtoExtensions.SerializeToBinary] Other error : {0}, protoObject type is {1}", e.Message, protoObject.GetType().ToString());
                    return false;
                }

                data = stream.ToArray();
                return true;
            }
        }

        public static byte[] defaultBytes = new byte[4];
    }
}
