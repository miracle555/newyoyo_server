﻿namespace AndroidAutobuilder
{
    partial class AndroidAutobuilder
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxOutputPath = new System.Windows.Forms.TextBox();
            this.buttonOutputPath = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxTfsAccount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxProduct = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBundleVersionCode = new System.Windows.Forms.TextBox();
            this.textBoxBundleVersion = new System.Windows.Forms.TextBox();
            this.buttonAddRes = new System.Windows.Forms.Button();
            this.listViewRes = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxVersion = new System.Windows.Forms.TextBox();
            this.checkBoxPackDev = new System.Windows.Forms.CheckBox();
            this.checkBoxRevert = new System.Windows.Forms.CheckBox();
            this.checkBoxPackArts = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdate = new System.Windows.Forms.CheckBox();
            this.buttonBuild = new System.Windows.Forms.Button();
            this.textBoxProjectPath = new System.Windows.Forms.TextBox();
            this.textBoxTfsPath = new System.Windows.Forms.TextBox();
            this.textBoxRarPath = new System.Windows.Forms.TextBox();
            this.textBoxUnityPath = new System.Windows.Forms.TextBox();
            this.buttonProjectPath = new System.Windows.Forms.Button();
            this.buttonTfsPath = new System.Windows.Forms.Button();
            this.buttonRarPath = new System.Windows.Forms.Button();
            this.buttonUnityPath = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.textBoxSdkPath = new System.Windows.Forms.TextBox();
            this.buttonSdkPath = new System.Windows.Forms.Button();
            this.comboBoxPlatform = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.comboBoxPlatform);
            this.panel1.Controls.Add(this.buttonSdkPath);
            this.panel1.Controls.Add(this.textBoxSdkPath);
            this.panel1.Controls.Add(this.textBoxOutputPath);
            this.panel1.Controls.Add(this.buttonOutputPath);
            this.panel1.Controls.Add(this.textBoxPassword);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxTfsAccount);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxProduct);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxBundleVersionCode);
            this.panel1.Controls.Add(this.textBoxBundleVersion);
            this.panel1.Controls.Add(this.buttonAddRes);
            this.panel1.Controls.Add(this.listViewRes);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxVersion);
            this.panel1.Controls.Add(this.checkBoxPackDev);
            this.panel1.Controls.Add(this.checkBoxRevert);
            this.panel1.Controls.Add(this.checkBoxPackArts);
            this.panel1.Controls.Add(this.checkBoxUpdate);
            this.panel1.Controls.Add(this.buttonBuild);
            this.panel1.Controls.Add(this.textBoxProjectPath);
            this.panel1.Controls.Add(this.textBoxTfsPath);
            this.panel1.Controls.Add(this.textBoxRarPath);
            this.panel1.Controls.Add(this.textBoxUnityPath);
            this.panel1.Controls.Add(this.buttonProjectPath);
            this.panel1.Controls.Add(this.buttonTfsPath);
            this.panel1.Controls.Add(this.buttonRarPath);
            this.panel1.Controls.Add(this.buttonUnityPath);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(577, 538);
            this.panel1.TabIndex = 0;
            // 
            // textBoxOutputPath
            // 
            this.textBoxOutputPath.Enabled = false;
            this.textBoxOutputPath.Location = new System.Drawing.Point(84, 150);
            this.textBoxOutputPath.Name = "textBoxOutputPath";
            this.textBoxOutputPath.Size = new System.Drawing.Size(490, 21);
            this.textBoxOutputPath.TabIndex = 34;
            this.textBoxOutputPath.TabStop = false;
            // 
            // buttonOutputPath
            // 
            this.buttonOutputPath.Location = new System.Drawing.Point(3, 148);
            this.buttonOutputPath.Name = "buttonOutputPath";
            this.buttonOutputPath.Size = new System.Drawing.Size(75, 23);
            this.buttonOutputPath.TabIndex = 33;
            this.buttonOutputPath.Text = "输出路径";
            this.buttonOutputPath.UseVisualStyleBackColor = true;
            this.buttonOutputPath.Click += new System.EventHandler(this.buttonOutputPath_Click);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(86, 339);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(160, 21);
            this.textBoxPassword.TabIndex = 32;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 342);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "Password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 12);
            this.label5.TabIndex = 30;
            this.label5.Text = "Tfs Account";
            // 
            // textBoxTfsAccount
            // 
            this.textBoxTfsAccount.Location = new System.Drawing.Point(86, 312);
            this.textBoxTfsAccount.Name = "textBoxTfsAccount";
            this.textBoxTfsAccount.Size = new System.Drawing.Size(160, 21);
            this.textBoxTfsAccount.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 369);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 28;
            this.label4.Text = "Product Name";
            // 
            // textBoxProduct
            // 
            this.textBoxProduct.Location = new System.Drawing.Point(86, 366);
            this.textBoxProduct.Name = "textBoxProduct";
            this.textBoxProduct.Size = new System.Drawing.Size(160, 21);
            this.textBoxProduct.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(289, 369);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 12);
            this.label3.TabIndex = 26;
            this.label3.Text = "Bundle Version Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(319, 342);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "Bundle Version";
            // 
            // textBoxBundleVersionCode
            // 
            this.textBoxBundleVersionCode.Location = new System.Drawing.Point(414, 366);
            this.textBoxBundleVersionCode.Name = "textBoxBundleVersionCode";
            this.textBoxBundleVersionCode.Size = new System.Drawing.Size(160, 21);
            this.textBoxBundleVersionCode.TabIndex = 24;
            // 
            // textBoxBundleVersion
            // 
            this.textBoxBundleVersion.Location = new System.Drawing.Point(414, 339);
            this.textBoxBundleVersion.Name = "textBoxBundleVersion";
            this.textBoxBundleVersion.Size = new System.Drawing.Size(160, 21);
            this.textBoxBundleVersion.TabIndex = 23;
            // 
            // buttonAddRes
            // 
            this.buttonAddRes.Location = new System.Drawing.Point(3, 245);
            this.buttonAddRes.Name = "buttonAddRes";
            this.buttonAddRes.Size = new System.Drawing.Size(571, 23);
            this.buttonAddRes.TabIndex = 22;
            this.buttonAddRes.Text = "添加资源目录";
            this.buttonAddRes.UseVisualStyleBackColor = true;
            this.buttonAddRes.Click += new System.EventHandler(this.buttonAddRes_Click);
            // 
            // listViewRes
            // 
            this.listViewRes.FullRowSelect = true;
            this.listViewRes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewRes.Location = new System.Drawing.Point(3, 177);
            this.listViewRes.Name = "listViewRes";
            this.listViewRes.Size = new System.Drawing.Size(571, 62);
            this.listViewRes.TabIndex = 21;
            this.listViewRes.UseCompatibleStateImageBehavior = false;
            this.listViewRes.View = System.Windows.Forms.View.Details;
            this.listViewRes.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listViewRes_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 396);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 20;
            this.label1.Text = "Res Version";
            // 
            // textBoxVersion
            // 
            this.textBoxVersion.Location = new System.Drawing.Point(414, 393);
            this.textBoxVersion.Name = "textBoxVersion";
            this.textBoxVersion.Size = new System.Drawing.Size(160, 21);
            this.textBoxVersion.TabIndex = 8;
            // 
            // checkBoxPackDev
            // 
            this.checkBoxPackDev.AutoSize = true;
            this.checkBoxPackDev.Location = new System.Drawing.Point(195, 274);
            this.checkBoxPackDev.Name = "checkBoxPackDev";
            this.checkBoxPackDev.Size = new System.Drawing.Size(96, 16);
            this.checkBoxPackDev.TabIndex = 12;
            this.checkBoxPackDev.Text = "编译程序资源";
            this.checkBoxPackDev.UseVisualStyleBackColor = true;
            // 
            // checkBoxRevert
            // 
            this.checkBoxRevert.AutoSize = true;
            this.checkBoxRevert.Location = new System.Drawing.Point(3, 274);
            this.checkBoxRevert.Name = "checkBoxRevert";
            this.checkBoxRevert.Size = new System.Drawing.Size(108, 16);
            this.checkBoxRevert.TabIndex = 9;
            this.checkBoxRevert.Text = "撤销挂起的更改";
            this.checkBoxRevert.UseVisualStyleBackColor = true;
            // 
            // checkBoxPackArts
            // 
            this.checkBoxPackArts.AutoSize = true;
            this.checkBoxPackArts.Location = new System.Drawing.Point(297, 274);
            this.checkBoxPackArts.Name = "checkBoxPackArts";
            this.checkBoxPackArts.Size = new System.Drawing.Size(96, 16);
            this.checkBoxPackArts.TabIndex = 11;
            this.checkBoxPackArts.Text = "编译美术资源";
            this.checkBoxPackArts.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdate
            // 
            this.checkBoxUpdate.AutoSize = true;
            this.checkBoxUpdate.Location = new System.Drawing.Point(117, 274);
            this.checkBoxUpdate.Name = "checkBoxUpdate";
            this.checkBoxUpdate.Size = new System.Drawing.Size(72, 16);
            this.checkBoxUpdate.TabIndex = 10;
            this.checkBoxUpdate.Text = "自动更新";
            this.checkBoxUpdate.UseVisualStyleBackColor = true;
            // 
            // buttonBuild
            // 
            this.buttonBuild.Location = new System.Drawing.Point(3, 476);
            this.buttonBuild.Name = "buttonBuild";
            this.buttonBuild.Size = new System.Drawing.Size(571, 59);
            this.buttonBuild.TabIndex = 13;
            this.buttonBuild.Text = "开始打包";
            this.buttonBuild.UseVisualStyleBackColor = true;
            this.buttonBuild.Click += new System.EventHandler(this.buttonBuild_Click);
            // 
            // textBoxProjectPath
            // 
            this.textBoxProjectPath.Enabled = false;
            this.textBoxProjectPath.Location = new System.Drawing.Point(84, 92);
            this.textBoxProjectPath.Name = "textBoxProjectPath";
            this.textBoxProjectPath.Size = new System.Drawing.Size(490, 21);
            this.textBoxProjectPath.TabIndex = 11;
            this.textBoxProjectPath.TabStop = false;
            // 
            // textBoxTfsPath
            // 
            this.textBoxTfsPath.Enabled = false;
            this.textBoxTfsPath.Location = new System.Drawing.Point(84, 63);
            this.textBoxTfsPath.Name = "textBoxTfsPath";
            this.textBoxTfsPath.Size = new System.Drawing.Size(490, 21);
            this.textBoxTfsPath.TabIndex = 10;
            this.textBoxTfsPath.TabStop = false;
            // 
            // textBoxRarPath
            // 
            this.textBoxRarPath.Enabled = false;
            this.textBoxRarPath.Location = new System.Drawing.Point(84, 34);
            this.textBoxRarPath.Name = "textBoxRarPath";
            this.textBoxRarPath.Size = new System.Drawing.Size(490, 21);
            this.textBoxRarPath.TabIndex = 9;
            this.textBoxRarPath.TabStop = false;
            // 
            // textBoxUnityPath
            // 
            this.textBoxUnityPath.Enabled = false;
            this.textBoxUnityPath.Location = new System.Drawing.Point(84, 5);
            this.textBoxUnityPath.Name = "textBoxUnityPath";
            this.textBoxUnityPath.Size = new System.Drawing.Size(490, 21);
            this.textBoxUnityPath.TabIndex = 8;
            this.textBoxUnityPath.TabStop = false;
            // 
            // buttonProjectPath
            // 
            this.buttonProjectPath.Location = new System.Drawing.Point(3, 90);
            this.buttonProjectPath.Name = "buttonProjectPath";
            this.buttonProjectPath.Size = new System.Drawing.Size(75, 23);
            this.buttonProjectPath.TabIndex = 7;
            this.buttonProjectPath.Text = "项目路径";
            this.buttonProjectPath.UseVisualStyleBackColor = true;
            this.buttonProjectPath.Click += new System.EventHandler(this.buttonProjectPath_Click);
            // 
            // buttonTfsPath
            // 
            this.buttonTfsPath.Location = new System.Drawing.Point(3, 61);
            this.buttonTfsPath.Name = "buttonTfsPath";
            this.buttonTfsPath.Size = new System.Drawing.Size(75, 23);
            this.buttonTfsPath.TabIndex = 6;
            this.buttonTfsPath.Text = "tfs路径";
            this.buttonTfsPath.UseVisualStyleBackColor = true;
            this.buttonTfsPath.Click += new System.EventHandler(this.buttonTfsPath_Click);
            // 
            // buttonRarPath
            // 
            this.buttonRarPath.Location = new System.Drawing.Point(3, 32);
            this.buttonRarPath.Name = "buttonRarPath";
            this.buttonRarPath.Size = new System.Drawing.Size(75, 23);
            this.buttonRarPath.TabIndex = 5;
            this.buttonRarPath.Text = "winrar路径";
            this.buttonRarPath.UseVisualStyleBackColor = true;
            this.buttonRarPath.Click += new System.EventHandler(this.buttonRarPath_Click);
            // 
            // buttonUnityPath
            // 
            this.buttonUnityPath.Location = new System.Drawing.Point(3, 3);
            this.buttonUnityPath.Name = "buttonUnityPath";
            this.buttonUnityPath.Size = new System.Drawing.Size(75, 23);
            this.buttonUnityPath.TabIndex = 4;
            this.buttonUnityPath.Text = "unity路径";
            this.buttonUnityPath.UseVisualStyleBackColor = true;
            this.buttonUnityPath.Click += new System.EventHandler(this.buttonUnityPath_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBoxOutput);
            this.panel2.Location = new System.Drawing.Point(595, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(577, 538);
            this.panel2.TabIndex = 1;
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Location = new System.Drawing.Point(3, 3);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOutput.Size = new System.Drawing.Size(571, 532);
            this.textBoxOutput.TabIndex = 3;
            // 
            // textBoxSdkPath
            // 
            this.textBoxSdkPath.Enabled = false;
            this.textBoxSdkPath.Location = new System.Drawing.Point(84, 121);
            this.textBoxSdkPath.Name = "textBoxSdkPath";
            this.textBoxSdkPath.Size = new System.Drawing.Size(490, 21);
            this.textBoxSdkPath.TabIndex = 35;
            this.textBoxSdkPath.TabStop = false;
            // 
            // buttonSdkPath
            // 
            this.buttonSdkPath.Location = new System.Drawing.Point(3, 119);
            this.buttonSdkPath.Name = "buttonSdkPath";
            this.buttonSdkPath.Size = new System.Drawing.Size(75, 23);
            this.buttonSdkPath.TabIndex = 36;
            this.buttonSdkPath.Text = "sdk路径";
            this.buttonSdkPath.UseVisualStyleBackColor = true;
            this.buttonSdkPath.Click += new System.EventHandler(this.buttonSdkPath_Click);
            // 
            // comboBoxPlatform
            // 
            this.comboBoxPlatform.FormattingEnabled = true;
            this.comboBoxPlatform.Location = new System.Drawing.Point(414, 312);
            this.comboBoxPlatform.Name = "comboBoxPlatform";
            this.comboBoxPlatform.Size = new System.Drawing.Size(160, 20);
            this.comboBoxPlatform.TabIndex = 37;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(355, 315);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 38;
            this.label7.Text = "Platform";
            // 
            // AndroidAutobuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 562);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "AndroidAutobuilder";
            this.Text = "AndroidAutobuilder";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonProjectPath;
        private System.Windows.Forms.Button buttonTfsPath;
        private System.Windows.Forms.Button buttonRarPath;
        private System.Windows.Forms.Button buttonUnityPath;
        private System.Windows.Forms.TextBox textBoxProjectPath;
        private System.Windows.Forms.TextBox textBoxTfsPath;
        private System.Windows.Forms.TextBox textBoxRarPath;
        private System.Windows.Forms.TextBox textBoxUnityPath;
        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.CheckBox checkBoxPackArts;
        private System.Windows.Forms.CheckBox checkBoxUpdate;
        private System.Windows.Forms.CheckBox checkBoxRevert;
        private System.Windows.Forms.CheckBox checkBoxPackDev;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxVersion;
        private System.Windows.Forms.Button buttonAddRes;
        private System.Windows.Forms.ListView listViewRes;
        private System.Windows.Forms.TextBox textBoxBundleVersionCode;
        private System.Windows.Forms.TextBox textBoxBundleVersion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxProduct;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxTfsAccount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxOutputPath;
        private System.Windows.Forms.Button buttonOutputPath;
        private System.Windows.Forms.Button buttonSdkPath;
        private System.Windows.Forms.TextBox textBoxSdkPath;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxPlatform;
    }
}

