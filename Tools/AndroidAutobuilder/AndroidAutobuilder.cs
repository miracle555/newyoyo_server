﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AndroidAutobuilder
{
    public partial class AndroidAutobuilder : Form
    {
        //////////////////////////////////////////////////////////////////////////
        List<string> parters = new List<string>();

        //////////////////////////////////////////////////////////////////////////
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string section, string key, string defVal, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        //////////////////////////////////////////////////////////////////////////
        public AndroidAutobuilder()
        {
            InitializeComponent();

            loadIni();

            listViewRes.Columns.Add("Path", "Path", 400);
        }

        //////////////////////////////////////////////////////////////////////////
        void loadIni()
        {
            string iniPath = System.Environment.CurrentDirectory + @"\AndroidAutobuilder.ini";
            if (!File.Exists(iniPath))
            {
                var file = File.Create(iniPath);
                file.Close();
            }
            else
            {
                StringBuilder sb = new StringBuilder(1024);
                GetPrivateProfileString("Option", "TfsAccount", "", sb, 1024, iniPath);
                textBoxTfsAccount.Text = sb.ToString();
                GetPrivateProfileString("Option", "TfsPassword", "", sb, 1024, iniPath);
                textBoxPassword.Text = sb.ToString();
                GetPrivateProfileString("Option", "UnityPath", "", sb, 1024, iniPath);
                textBoxUnityPath.Text = sb.ToString();
                GetPrivateProfileString("Option", "RarPath", "", sb, 1024, iniPath);
                textBoxRarPath.Text = sb.ToString();
                GetPrivateProfileString("Option", "TfsPath", "", sb, 1024, iniPath);
                textBoxTfsPath.Text = sb.ToString();
                GetPrivateProfileString("Option", "ProjectPath", "", sb, 1024, iniPath);
                textBoxProjectPath.Text = sb.ToString();
                GetPrivateProfileString("Option", "OutputPath", "", sb, 1024, iniPath);
                textBoxOutputPath.Text = sb.ToString();
                GetPrivateProfileString("Option", "SdkPath", "", sb, 1024, iniPath);
                textBoxSdkPath.Text = sb.ToString();
                GetPrivateProfileString("Option", "Update", "0", sb, 1024, iniPath);
                checkBoxUpdate.Checked = (int.Parse(sb.ToString()) == 1);
                GetPrivateProfileString("Option", "PackArts", "0", sb, 1024, iniPath);
                checkBoxPackArts.Checked = (int.Parse(sb.ToString()) == 1);
                GetPrivateProfileString("Option", "PackDev", "0", sb, 1024, iniPath);
                checkBoxPackDev.Checked = (int.Parse(sb.ToString()) == 1);
                GetPrivateProfileString("Option", "Revert", "0", sb, 1024, iniPath);
                checkBoxRevert.Checked = (int.Parse(sb.ToString()) == 1);
                GetPrivateProfileString("Option", "Version", "", sb, 1024, iniPath);
                textBoxVersion.Text = sb.ToString();
                GetPrivateProfileString("Option", "BundleVersion", "", sb, 1024, iniPath);
                textBoxBundleVersion.Text = sb.ToString();
                GetPrivateProfileString("Option", "BundleVersionCode", "", sb, 1024, iniPath);
                textBoxBundleVersionCode.Text = sb.ToString();
                GetPrivateProfileString("Option", "ProductName", "", sb, 1024, iniPath);
                textBoxProduct.Text = sb.ToString();

                GetPrivateProfileString("Option", "Res", "", sb, 1024, iniPath);
                string res = sb.ToString();
                if (!string.IsNullOrEmpty(res))
                {
                    string[] paths = res.Split(';');
                    foreach (string path in paths)
                    {
                        ListViewItem lvi = new ListViewItem();
                        lvi.Text = path;
                        listViewRes.Items.Add(lvi);
                    }
                }

                iniPath = System.Environment.CurrentDirectory + @"\Parters.ini";
                GetPrivateProfileString("Parters", "Count", "", sb, 1024, iniPath);
                int count = int.Parse(sb.ToString());
                for (int i = 0; i < count; i++)
                {
                    string key = string.Format("Parter{0}", i + 1);
                    GetPrivateProfileString("Parters", key, "", sb, 1024, iniPath);
                    string parter = sb.ToString();
                    comboBoxPlatform.Items.Add(parter);
                    parters.Add(parter);
                }
                comboBoxPlatform.Items.Add("ALL");
            }
        }

        void saveIni()
        {
            output("save config");

            string iniPath = System.Environment.CurrentDirectory + @"\AndroidAutobuilder.ini";
            if (!File.Exists(iniPath))
            {
                var file = File.Create(iniPath);
                file.Close();
            }

            WritePrivateProfileString("Option", "TfsAccount", textBoxTfsAccount.Text, iniPath);
            WritePrivateProfileString("Option", "TfsPassword", textBoxPassword.Text, iniPath);
            WritePrivateProfileString("Option", "UnityPath", textBoxUnityPath.Text, iniPath);
            WritePrivateProfileString("Option", "RarPath", textBoxRarPath.Text, iniPath);
            WritePrivateProfileString("Option", "TfsPath", textBoxTfsPath.Text, iniPath);
            WritePrivateProfileString("Option", "ProjectPath", textBoxProjectPath.Text, iniPath);
            WritePrivateProfileString("Option", "OutputPath", textBoxOutputPath.Text, iniPath);
            WritePrivateProfileString("Option", "SdkPath", textBoxSdkPath.Text, iniPath);
            WritePrivateProfileString("Option", "Update", checkBoxUpdate.Checked ? "1" : "0", iniPath);
            WritePrivateProfileString("Option", "PackArts", checkBoxPackArts.Checked ? "1" : "0", iniPath);
            WritePrivateProfileString("Option", "PackDev", checkBoxPackDev.Checked ? "1" : "0", iniPath);
            WritePrivateProfileString("Option", "Revert", checkBoxRevert.Checked ? "1" : "0", iniPath);
            WritePrivateProfileString("Option", "Version", textBoxVersion.Text, iniPath);
            WritePrivateProfileString("Option", "BundleVersion", textBoxBundleVersion.Text, iniPath);
            WritePrivateProfileString("Option", "BundleVersionCode", textBoxBundleVersionCode.Text, iniPath);
            WritePrivateProfileString("Option", "ProductName", textBoxProduct.Text, iniPath);

            if (listViewRes.Items.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < listViewRes.Items.Count; i++)
                {
                    ListViewItem item = listViewRes.Items[i];
                    string path = item.SubItems[0].Text;
                    if (i == 0)
                    {
                        sb.Append(path);
                    }
                    else
                    {
                        sb.AppendFormat(";{0}", path);
                    }
                }
                WritePrivateProfileString("Option", "Res", sb.ToString(), iniPath);
            }
        }

        void output(string line, bool includeTime = true)
        {
            if (includeTime)
            {
                string msg = string.Format("[{0}]{1}", DateTime.Now.ToString(), line) + Environment.NewLine;
                textBoxOutput.AppendText(msg);
            }
            else
            {
                string msg = line + Environment.NewLine;
                textBoxOutput.AppendText(msg);
            }
        }

        void error(string line)
        {
            output(line);
            MessageBox.Show(line, "Error");
        }

        void executeCmd(string exe, string cmd, string path = "")
        {
            output(string.Format("[ExecCmd]{0} {1}", exe, cmd));

            System.Diagnostics.Process sysProcess = new System.Diagnostics.Process();
            sysProcess.StartInfo.FileName = exe;
            sysProcess.StartInfo.Arguments = cmd;
            if (path != "")
            {
                sysProcess.StartInfo.WorkingDirectory = path;
            }
            sysProcess.Start();
            sysProcess.WaitForExit();
            sysProcess.Close();
        }

        void update(int changelist = 0)
        {
            StringBuilder sb = new StringBuilder();
            if (checkBoxRevert.Checked)
            {
                output("revert......");
                sb.AppendFormat("undo {0} /recursive /noprompt /login:{1},{2}",
                textBoxProjectPath.Text, textBoxTfsAccount.Text, textBoxPassword.Text);
                executeCmd(textBoxTfsPath.Text, sb.ToString());
                sb.Clear();
            }

            if (checkBoxUpdate.Checked)
            {
                output("update......");
                sb.AppendFormat("get {0} /recursive /noprompt /login:{1},{2}",
                textBoxProjectPath.Text, textBoxTfsAccount.Text, textBoxPassword.Text);
                if (changelist > 0)
                {
                    sb.AppendFormat(" /v:{0}", changelist);
                }
                executeCmd(textBoxTfsPath.Text, sb.ToString());
            }
        }

        void compile()
        {
            convert();
            packArts();
            packDev();
        }

        void convert()
        {
            output("convert csv......");

            StringBuilder sb = new StringBuilder();
            sb.Append("-quit -batchmode -executeMethod AndroidBuilder.ConvertCsv");
            sb.AppendFormat(" -projectPath \"{0}\"", textBoxProjectPath.Text);
            sb.AppendFormat(" -logFile \"{0}\\ConvertCsv.log\"", logPath);
            executeCmd(textBoxUnityPath.Text, sb.ToString());
        }

        void packArts()
        {
            if (!checkBoxPackArts.Checked)
                return;

            output("pack arts......");

            StringBuilder sb = new StringBuilder();
            sb.Append("-quit -batchmode -executeMethod AndroidBuilder.PackArts");
            sb.AppendFormat(" -projectPath \"{0}\"", textBoxProjectPath.Text);
            sb.AppendFormat(" -logFile \"{0}\\PackArts.log\"", logPath);
            executeCmd(textBoxUnityPath.Text, sb.ToString());
        }

        void packDev()
        {
            if (!checkBoxPackDev.Checked)
                return;

            output("pack dev......");

            StringBuilder sb = new StringBuilder();
            sb.Append("-quit -batchmode -executeMethod AndroidBuilder.PackDev");
            sb.AppendFormat(" -projectPath \"{0}\"", textBoxProjectPath.Text);
            sb.AppendFormat(" -logFile \"{0}\\PackDev.log\"", logPath);
            executeCmd(textBoxUnityPath.Text, sb.ToString());
        }

        void copyRes()
        {
            output("copy res......");
            string assetbundle = string.Format(@"{0}\AssetBundles", dataPath);
            if (!Directory.Exists(assetbundle))
            {
                Directory.CreateDirectory(assetbundle);
            }
            string assetPath = string.Format(@"{0}\AssetBundles-Android", textBoxProjectPath.Text);
            copyDirectory(assetPath, assetbundle);

            if (listViewRes.Items.Count > 0)
            {
                for (int i = 0; i < listViewRes.Items.Count; i++)
                {
                    ListViewItem item = listViewRes.Items[i];
                    string path = item.SubItems[0].Text;
                    if (path.Contains("AssetBundles"))
                        continue;

                    string resPath = path.Replace(textBoxProjectPath.Text, dataPath);
                    if (!Directory.Exists(resPath))
                    {
                        Directory.CreateDirectory(resPath);
                    }
                    copyDirectory(path, resPath);
                }
            }
        }

        bool makeZip()
        {
            output("make zip......");
            string zipPath = string.Format(@"{0}\all.zip", dataPath);
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(" a -r -afzip -ep1 \"{0}\"", zipPath);
            sb.AppendFormat(" \"{0}\\*\"", dataPath);
            executeCmd(textBoxRarPath.Text, sb.ToString());

            if (!File.Exists(zipPath))
            {
                error("make zip failed");
                return false;
            }
            return true;
        }

        bool copyZip()
        {
            string zipPath = string.Format(@"{0}\all.zip", dataPath);
            if (!File.Exists(zipPath))
                return false;

            string dstPath = textBoxProjectPath.Text + @"\Assets\Plugins\Android\assets\lwts";
            if (!Directory.Exists(dstPath))
            {
                Directory.CreateDirectory(dstPath);
            }

            string dst = dstPath + @"\all.zip";
            File.Copy(zipPath, dst, true);
            return true;
        }

        void genVersion()
        {
            string path = dataPath + @"\config";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string file = string.Format(@"{0}\version.txt", path);
            FileStream fs = new FileStream(file, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(textBoxVersion.Text);
            sw.Close();
            fs.Close();
        }

        void copyDirectory(string srcdir, string desdir)
        {
            string folderName = srcdir.Substring(srcdir.LastIndexOf("\\") + 1);
            string desfolderdir = desdir;
            if (desdir.LastIndexOf("\\") == (desdir.Length - 1))
            {
                desfolderdir = desdir + folderName;
            }
            string[] filenames = Directory.GetFileSystemEntries(srcdir);

            foreach (string file in filenames)
            {
                if (Directory.Exists(file))
                {
                    string currentdir = desfolderdir + "\\" + file.Substring(file.LastIndexOf("\\") + 1);
                    if (!Directory.Exists(currentdir))
                    {
                        Directory.CreateDirectory(currentdir);
                    }
                    copyDirectory(file, currentdir);
                }
                else
                {
                    string srcfileName = file.Substring(file.LastIndexOf("\\") + 1);
                    srcfileName = desfolderdir + "\\" + srcfileName;
                    if (!Directory.Exists(desfolderdir))
                    {
                        Directory.CreateDirectory(desfolderdir);
                    }
                    File.Copy(file, srcfileName);
                }
            }
        }

        bool generate()
        {
            output("generate......");

            DateTime now = DateTime.Now;
            List<string> platforms = new List<string>();
            if (comboBoxPlatform.Text == "ALL")
            {
                platforms = parters;
            }
            else
            {
                platforms.Add(comboBoxPlatform.Text);
            }

            foreach (string parter in platforms)
            {
                output(string.Format("=================================build {0} begin=================================", parter), false);

                string apk = textBoxProjectPath.Text + @"\otome.apk";
                if (File.Exists(apk))
                {
                    File.Delete(apk);
                }

                string iniPath = System.Environment.CurrentDirectory + @"\Parters.ini";
                StringBuilder sb = new StringBuilder();
                GetPrivateProfileString(parter, "Bundle_identifier", "", sb, 1024, iniPath);
                string bundle_identifier = sb.ToString();
                GetPrivateProfileString(parter, "PARTER_ID", "", sb, 1024, iniPath);
                string define_symbols = sb.ToString();
                GetPrivateProfileString(parter, "Sdk", "", sb, 1024, iniPath);
                string sdk = sb.ToString();
                GetPrivateProfileString(parter, "Apk", "", sb, 1024, iniPath);
                string apkName = sb.ToString();
                if (string.IsNullOrEmpty(apkName))
                {
                    apkName = parter;
                }
                GetPrivateProfileString(parter, "Title", "", sb, 1024, iniPath);
                string title = sb.ToString();
                if (string.IsNullOrEmpty(title))
                {
                    title = textBoxProduct.Text;
                }
                string sdkPath = string.Format(@"{0}\{1}", textBoxSdkPath.Text, sdk);
                if (!Directory.Exists(sdkPath))
                {
                    error(string.Format("parter {0} sdk path not exist", parter));
                    return false;
                }
                string sdkDstPath = string.Format(@"{0}\Assets\Plugins\Android", textBoxProjectPath.Text);
                if (Directory.Exists(sdkDstPath))
                {
                    Directory.Delete(sdkDstPath, true);
                    Thread.Sleep(3000);
                }
                DirectoryInfo info = Directory.CreateDirectory(sdkDstPath);
                if (!info.Exists)
                {
                    error(string.Format("parter {0} create sdk path failed", parter));
                    return false;
                }
                copyDirectory(sdkPath, sdkDstPath);
                if (!copyZip())
                {
                    error(string.Format("parter {0} copy zip failed", parter));
                    return false;
                }

                sb.Clear();
                sb.Append("-quit -batchmode -executeMethod AndroidBuilder.GenerateApk");
                sb.AppendFormat(" -projectPath \"{0}\"", textBoxProjectPath.Text);
                sb.AppendFormat(" -logFile \"{0}\\GenerateApk_{1}.log\"", logPath, apkName);
                sb.AppendFormat(" -PRODUCTNAME \"{0}\"", title);
                sb.AppendFormat(" -BUNDLEVERSION \"{0}\"", textBoxBundleVersion.Text);
                sb.AppendFormat(" -BUNDLEVERSIONCODE \"{0}\"", textBoxBundleVersionCode.Text);
                if (!string.IsNullOrEmpty(bundle_identifier))
                {
                    sb.AppendFormat(" -BUNDLEIDENTIFIER \"{0}\"", bundle_identifier);
                }
                if (!string.IsNullOrEmpty(define_symbols))
                {
                    sb.AppendFormat(" -SCRIPTINGDEFINESYMBOLS \"{0}\"", define_symbols);
                }
                executeCmd(textBoxUnityPath.Text, sb.ToString());

                string src = textBoxProjectPath.Text + @"\otome.apk";
                string dst = string.Format(@"{0}\{1}_{2}.apk", binPath, textBoxBundleVersion.Text, apkName);
                if (File.Exists(src))
                {
                    File.Copy(src, dst, true);
                    if (Directory.Exists(textBoxOutputPath.Text))
                    {
                        string platformPath = string.Format(@"{0}\{1}", textBoxOutputPath.Text, parter);
                        string datePath = string.Format(@"{0}\{1}{2:00}", platformPath, now.Year, now.Month);
                        if (!Directory.Exists(platformPath))
                        {
                            Directory.CreateDirectory(platformPath);
                        }
                        if (!Directory.Exists(datePath))
                        {
                            Directory.CreateDirectory(datePath);
                        }

                        dst = string.Format(@"{0}\{1}_{2}.apk", datePath, textBoxBundleVersion.Text, apkName);
                        File.Copy(src, dst, true);
                    }
                    File.Delete(src);
                }
                else
                {
                    error(string.Format("parter {0} generate failed", parter));
                    return false;
                }

                reportStatisticsInfo(apkName);

                output(string.Format("==================================build {0} end==================================", parter), false);
                output("", false);
            }

            output("generate complete");
            return true;
        }

        bool check()
        {
            if (string.IsNullOrEmpty(textBoxTfsAccount.Text))
            {
                error("tfs account invalid");
                return false;
            }
            if (string.IsNullOrEmpty(textBoxPassword.Text))
            {
                error("tfs password invalid");
                return false;
            }
            if (!File.Exists(textBoxUnityPath.Text))
            {
                error("unity path not exist");
                return false;
            }
            if (!File.Exists(textBoxRarPath.Text))
            {
                error("winrar path not exist");
                return false;
            }
            if (!File.Exists(textBoxTfsPath.Text))
            {
                error("tfs path not exist");
                return false;
            }
            if (!Directory.Exists(textBoxProjectPath.Text))
            {
                error("project path not exist");
                return false;
            }
            if (!Directory.Exists(textBoxSdkPath.Text))
            {
                error("sdk path not exist");
                return false;
            }
            if (string.IsNullOrEmpty(textBoxBundleVersion.Text))
            {
                error("bundle version invalid");
                return false;
            }
            if (string.IsNullOrEmpty(textBoxProduct.Text))
            {
                error("product name invalid");
                return false;
            }

            int version = 0;
            if (!int.TryParse(textBoxVersion.Text, out version))
            {
                error("res version invalid");
                return false;
            }

            if (!int.TryParse(textBoxBundleVersionCode.Text, out version))
            {
                error("bundle version code invalid");
                return false;
            }

            if (string.IsNullOrEmpty(comboBoxPlatform.Text))
            {
                error("platform not select");
                return false;
            }

            Process[] process;
            process = Process.GetProcesses();
            foreach (Process proces in process)
            {
                string name = proces.ProcessName.ToLower();
                if (name == "unity")
                {
                    error("please close unity");
                    return false;
                }
            }

            return true;
        }

        void createDirectory()
        {
            DateTime now = DateTime.Now;
            binPath = string.Format(@"{0}\{1}{2:00}{3:00}-{4:00}{5:00}{6:00}{7:000}", System.Environment.CurrentDirectory,
                now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, now.Millisecond);
            Directory.CreateDirectory(binPath);
            logPath = binPath + @"\log";
            Directory.CreateDirectory(logPath);
            dataPath = binPath + @"\data";
            Directory.CreateDirectory(dataPath);
        }

        void reportStatisticsInfo(string apkName)
        {
            output("-----------------------------------report-----------------------------------", false);
            string path = string.Format(@"{0}\GenerateApk_{1}.log", logPath, apkName);
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string line = sr.ReadLine();
            int count = 50;
            while (line != null && count > 0)
            {
                if (line.Contains('%') && (line.Contains("mb") || line.Contains("kb")))
                {
                    count--;
                    output(line, false);
                    if (line.Contains("Complete size"))
                    {
                        output("", false);
                        output("Used Assets, sorted by uncompressed size:", false);
                    }
                }

                line = sr.ReadLine();
            }
            sr.Close();
            fs.Close();
            output("----------------------------------------------------------------------------", false);
        }

        //////////////////////////////////////////////////////////////////////////
        string binPath;
        string logPath;
        string dataPath;

        //////////////////////////////////////////////////////////////////////////
        private void buttonUnityPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                textBoxUnityPath.Text = fileBrowser.FileName;
                saveIni();
            }
        }

        private void buttonRarPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                textBoxRarPath.Text = fileBrowser.FileName;
                saveIni();
            }
        }

        private void buttonTfsPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                textBoxTfsPath.Text = fileBrowser.FileName;
                saveIni();
            }
        }

        private void buttonProjectPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                textBoxProjectPath.Text = folderBrowser.SelectedPath;
                saveIni();
            }
        }

        private void buttonBuild_Click(object sender, EventArgs e)
        {
            buttonBuild.Enabled = false;

            try
            {
                saveIni();
                createDirectory();

                output("build......");
                DateTime now = DateTime.Now;
                if (!check())
                {
                    output("build failed");
                    buttonBuild.Enabled = true;
                    return;
                }

                update();
                compile();
                copyRes();
                genVersion();

                if (!makeZip())
                {
                    output("build failed");
                    buttonBuild.Enabled = true;
                    return;
                }

                if (generate())
                {
                    TimeSpan span = DateTime.Now - now;
                    output("build success");
                }
            }
            catch (Exception ex)
            {
                error(string.Format("Exception: {0}", ex.Message));
            }
            
            buttonBuild.Enabled = true;
        }

        private void buttonAddRes_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                string path = folderBrowser.SelectedPath;
                if (!path.Contains(textBoxProjectPath.Text))
                {
                    error("path invalid");
                    return;
                }

                ListViewItem lvi = new ListViewItem();
                lvi.Text = path;
                listViewRes.Items.Add(lvi);
                saveIni();
            }
        }

        private void listViewRes_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                for (int i = 0; i < listViewRes.SelectedItems.Count; i++)
                {
                    ListViewItem item = listViewRes.SelectedItems[i];
                    listViewRes.Items.Remove(item);
                }
                saveIni();
            }
        }

        private void buttonOutputPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                textBoxOutputPath.Text = folderBrowser.SelectedPath;
                saveIni();
            }
        }

        private void buttonSdkPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                textBoxSdkPath.Text = folderBrowser.SelectedPath;
                saveIni();
            }
        }
    }
}
