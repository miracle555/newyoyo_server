using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Protocols;
using Log;

namespace StaticData
{
    public class ArrowConfigItemImp
    {
        public ArrowConfigItem item = new ArrowConfigItem();

        public bool Load(XmlElement elementNode)
        {
            if (!XmlRead.ReadAttribute(elementNode, "f_id", out item.m_arrow_id))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_level", out item.m_level))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_name", out item.m_name))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_model", out item.m_model))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_move_speed", out item.m_move_speed))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_damage", out item.m_damage))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_spurting_damage", out item.m_spurting_damage))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_penetrate", out item.m_penetrate))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_range", out item.m_range))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_judge", out item.m_judge))
            {
                return false;
            }

            item.m_id = item.m_arrow_id * 1000 + item.m_level;

            return true;
        }
    };

    public class ArrowConfigImp
    {
        public ArrowConfig config = new ArrowConfig();

        public bool Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.GetElementsByTagName("item");
            foreach (XmlNode elementNode in elementNodes)
            {
                ArrowConfigItemImp item = new ArrowConfigItemImp();
                
                if(!item.Load((XmlElement)elementNode))
                {
                    LogSys.Error("ArrowConfig load failed, item.m_id:{0}", item.item.m_id);
                    return false;
                }

                config.AddItem(item.item);
            }

            return true;
        }

        public bool Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            return Load(rootElem);
        }
    }
}


