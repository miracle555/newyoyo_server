using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Protocols;
using ServerLib;

namespace Config
{
    public class ServerIPTable
    {
        public ServerItems serverItems = new ServerItems();

        public void Load(XmlElement rootElem)
        {
            serverItems.serverMap.Clear();
            XmlNodeList elementNodes = rootElem.GetElementsByTagName("entry");
            foreach (XmlNode elementNode in elementNodes)
            {
                string zoneid = ((XmlElement)elementNode).GetAttribute("zoneid");
                string type = ((XmlElement)elementNode).GetAttribute("type");
                string desc = ((XmlElement)elementNode).GetAttribute("desc");
                string number = ((XmlElement)elementNode).GetAttribute("number");
                string ip = ((XmlElement)elementNode).GetAttribute("ip");
                string port = ((XmlElement)elementNode).HasAttribute("port") ? ((XmlElement)elementNode).GetAttribute("port") : "0";

                string key = type + number;
                VirtualAddress address = new VirtualAddress(zoneid, type, number);

                ServerItem item = new ServerItem();
                item.key = key;
                item.zoneid = zoneid;
                item.type = type;
                item.desc = desc;
                item.number = number;
                item.ip = ip;
                item.port = port;

                serverItems.serverMap.Add(address, item);
            }
        }

        public void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }
}


