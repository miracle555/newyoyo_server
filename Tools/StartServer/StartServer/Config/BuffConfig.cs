using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Protocols;
using Log;

namespace StaticData
{
    public class BuffConfigItemImp
    {
        public BuffConfigItem item = new BuffConfigItem();

        public bool Load(XmlElement elementNode)
        {
            if (!XmlRead.ReadAttribute(elementNode,"f_id", out item.m_buff_id))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_name", out item.m_name))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_arrow", out item.m_arrow_id))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_duration", out item.m_duration_second))
            {
                return false;
            }

            return true;
        }
    };

    public class BuffConfigImp
    {
        public BuffConfig config = new BuffConfig();

        public bool Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.GetElementsByTagName("item");
            foreach (XmlNode elementNode in elementNodes)
            {
                BuffConfigItemImp item = new BuffConfigItemImp();

                if (!item.Load((XmlElement)elementNode))
                {
                    LogSys.Error("BuffConfig load failed, item.m_buff_id:{0}", item.item.m_buff_id);
                    return false;
                }

                config.AddItem(item.item);
            }

            return true;
        }

        public bool Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            return Load(rootElem);
        }
    }
}


