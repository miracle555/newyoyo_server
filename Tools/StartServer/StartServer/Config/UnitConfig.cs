using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Log;
using ProtoBuf;

namespace StaticData
{
    public class UnitConfigItemImp
    {
        public UnitConfigItem item = new UnitConfigItem();

        public bool Load(XmlElement elementNode)
        {
            if (!XmlRead.ReadAttribute(elementNode, "f_id", out item.m_spiritid))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_name", out item.m_spirit_name))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_model", out item.m_model))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_range", out item.m_range))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_move_speed", out item.m_move_speed))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_turn_around_speed", out item.m_turn_around_speed))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_life_addition", out item.m_life_addition))
            {
                return false;
            }


	        {
		        string strItems = "";
		        if (!XmlRead.ReadAttribute(elementNode,"f_basic_arrow", out strItems))
                {
                    return false;
                }

                string[] arrCounts = strItems.Split('*');
                if(arrCounts.Length != 2)
                {
                    return false;
                }

                if (!UInt32.TryParse(arrCounts[0], out item.m_basic_arrow.m_arrowid))
                {
                    return false;
                }

                if (!UInt32.TryParse(arrCounts[1], out item.m_basic_arrow.m_arrow_level))
                {
                    return false;
                }
	        }

	        {
		        string strItems = "";
		        if (!XmlRead.ReadAttribute(elementNode,"f_default_grow_arrow", out strItems))
                {
                    return false;
                }

                string[] arrCounts = strItems.Split('*');
                if(arrCounts.Length != 2)
                {
                    return false;
                }

                if (!UInt32.TryParse(arrCounts[0], out item.m_default_grow_arrow.m_arrowid))
                {
                    return false;
                }

                if (!UInt32.TryParse(arrCounts[1], out item.m_default_grow_arrow.m_arrow_level))
                {
                    return false;
                }
	        }

	        {
		        string strItems = "";
		        if (!XmlRead.ReadAttribute(elementNode,"f_buff_grow_arrow", out strItems))
                {
                    return false;
                }

                string[] arrItems = strItems.Split(',');
                foreach(var strItem in arrItems)
                {
                    string[] arrCounts = strItem.Split('*');
                    if(arrCounts.Length != 2)
                    {
                        return false;
                    }

                    SArrowUnit arrow = new SArrowUnit();
                    if (!UInt32.TryParse(arrCounts[0], out arrow.m_arrowid))
                    {
                        return false;
                    }

                    if (!UInt32.TryParse(arrCounts[1], out arrow.m_arrow_level))
                    {
                        return false;
                    }

                    item.m_buff_grow_arrows.Add(arrow);
                }
	        }

	        {
                string strItems = "";
		        if (!XmlRead.ReadAttribute(elementNode,"f_specific", out strItems))
                {
                    return false;
                }

                if(strItems.Length != 0)
                {
                    string[] arrItems = strItems.Split(',');
                    foreach (var strItem in arrItems)
                    {
                        string[] arrCounts = strItem.Split('*');
                        if (arrCounts.Length != 2)
                        {
                            return false;
                        }

                        SSpecialUnit special = new SSpecialUnit();
                        if (!UInt32.TryParse(arrCounts[0], out special.m_effect_id))
                        {
                            return false;
                        }

                        if (!UInt32.TryParse(arrCounts[1], out special.m_effect_level))
                        {
                            return false;
                        }

                        item.m_specific_effects.Add(special);
                    }
                }
	        }

	        if (!XmlRead.ReadAttribute(elementNode,"f_arrow_upper_limit", out item.m_arrow_upper_limit))
            {
                return false;
            }

	        if (!XmlRead.ReadAttribute(elementNode,"f_arrow_recovery", out item.m_arrow_recovery_second))
            {
                return false;
            }


            return true;
        }
    };

    public class UnitConfigImp
    {
        public UnitConfig config = new UnitConfig();

        public bool Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.GetElementsByTagName("item");
            foreach (XmlNode elementNode in elementNodes)
            {
                UnitConfigItemImp item = new UnitConfigItemImp();

                if (!item.Load((XmlElement)elementNode))
                {
                    LogSys.Error("UnitConfig load failed, item.m_spiritid:{0}", item.item.m_spiritid);
                    return false;
                }

                config.AddItem(item.item);
            }

            return true;
        }

        public bool Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            return Load(rootElem);
        }
    }
}


