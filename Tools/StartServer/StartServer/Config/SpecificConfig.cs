using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Log;

namespace StaticData
{
    public class SpecificCfgItemImp
    {
        public SpecificCfgItem item = new SpecificCfgItem();

        public bool Load(XmlElement elementNode)
        {
            if (!XmlRead.ReadAttribute(elementNode, "f_id", out item.m_effect_id))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_level", out item.m_level))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_name", out item.m_name))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_model", out item.m_model))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_vertigo_time", out item.m_vertigo_time))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_vertigo_rate", out item.m_vertigo_rate))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_fixed_boddy_time", out item.m_fixed_boddy_time))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_fixed_boddy_rate", out item.m_fixed_boddy_rate))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_crit_rate", out item.m_crit_rate))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_dodge_rate", out item.m_dodge_rate))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_shield_life", out item.m_shield_life))
            {
                return false;
            }

            if (!XmlRead.ReadAttribute(elementNode, "f_shield_rate", out item.m_shield_rate))
            {
                return false;
            }

            item.m_uniqueid = item.m_effect_id * 100 + item.m_level;

            return true;
        }
    };

    public class SpecificConfigImp
    {
        public SpecificConfig config = new SpecificConfig();

        public bool Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.GetElementsByTagName("item");
            foreach (XmlNode elementNode in elementNodes)
            {
                SpecificCfgItemImp item = new SpecificCfgItemImp();

                if (!item.Load((XmlElement)elementNode))
                {
                    LogSys.Error("SpecificConfig load failed, item.m_uniqueid:{0}", item.item.m_uniqueid);
                    return false;
                }

                config.AddItem(item.item);
            }

            return true;
        }

        public bool Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            return Load(rootElem);
        }
    }
}


