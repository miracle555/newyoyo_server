using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using ProtoBuf;

namespace StaticData
{
    public class GodConfigImp
    {
        public GodConfig config = new GodConfig();

        public bool Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.GetElementsByTagName("item");
            foreach (XmlNode node in elementNodes)
            {
                XmlElement elementNode = (XmlElement)node;

                UInt32 id = 0;
                if (!XmlRead.ReadAttribute(elementNode, "f_id", out id))
                {
                    return false;
                }

                if (id == 1)
                {
                    //战斗中英灵基础生命
                    UInt32 life = 0;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out life))
                    {
                        return false;
                    }
                    config.initSpiritLife = life;
                }
                else if (id == 2)
                {
                    //混战模式单局战斗时间
                    float lastSecond = 0.0f;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out lastSecond))
                    {
                        return false;
                    }
                    config.battleLastSecond = lastSecond;
                }
                else if (id == 3)
                {
                    //混战模式单局人数上限
                    UInt32 limit = 0;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out limit))
                    {
                        return false;
                    }
                    config.battleRoleLimit = limit;
                }
                else if (id == 4)
                {
                    //混战模式游戏启动后仍可进入的时间
                    float opensecond = 0.0f;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out opensecond))
                    {
                        return false;
                    }
                    config.roomOpenSecond = opensecond;
                }
                else if (id == 5)
                {
                    //混战模式单次击杀积分
                    UInt32 killScore = 0;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out killScore))
                    {
                        return false;
                    }
                    config.killScore = killScore;
                }
                else if (id == 6)
                {
                    //初始英灵
                    UInt32 spiritid = 0;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out spiritid))
                    {
                        return false;
                    }
                    config.initSpiritId = spiritid;
                }
                else if (id == 7)
                {
                    //玩家进入房间后保护时间
                    float protectSecond = 0.0f;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out protectSecond))
                    {
                        return false;
                    }
                    config.protectSecond = protectSecond;
                }
                else if (id == 8)
                {
                    float second = 0.0f;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out second))
                    {
                        return false;
                    }
                    config.autoReviveSecond = second;
                }
                else if (id == 9)
                {
                    float second = 0.0f;
                    if (!XmlRead.ReadAttribute(elementNode, "f_value", out second))
                    {
                        return false;
                    }
                    config.buffLifeTime = second;
                }
            }

            return true;
        }

        public bool Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            return Load(rootElem);
        }
    }
}


