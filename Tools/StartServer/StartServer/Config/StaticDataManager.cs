﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Module;
using Protocols;
using System.Xml;
using ServerLib;
using Log;
using ProtoBuf;
using Redis;

namespace StaticData
{
    public static class XmlRead
    {
        public static bool ReadAttribute(XmlElement elementNode, string attrName, out UInt32 value)
        {
            value = 0;

            if (!elementNode.HasAttribute(attrName))
            {
                return false;
            }

            string strValue =  elementNode.GetAttribute(attrName);

            if(!UInt32.TryParse(strValue, out value))
            {
                return false;
            }

            return true;
        }

        public static bool ReadAttribute(XmlElement elementNode, string attrName, out float value)
        {
            value = 0.0f;

            if (!elementNode.HasAttribute(attrName))
            {
                return false;
            }

            string strValue = elementNode.GetAttribute(attrName);

            if (!float.TryParse(strValue, out value))
            {
                return false;
            }

            return true;
        }

        public static bool ReadAttribute(XmlElement elementNode, string attrName, out string value)
        {
            value = "";

            if(!elementNode.HasAttribute(attrName))
            {
                return false;
            }

            value = elementNode.GetAttribute(attrName);
            return true;
        }
    };

	/// <summary>
    /// 静态数据管理器
    /// </summary>
    [ProtoContract]
    public class StaticDataManagerImp
    {
        public StaticDataManager staticData = new StaticDataManager();

        public bool Init(string path)
        {
            this.path = path;

            if (!LoadUnityConfig())
            {
                return false;
            }

            if(!LoadArrowConfig())
            {
                return false;
            }

            //if(!SpecialConfig.Load(RelativePath + "\\" + "t_specific_config.xml"))
            //{
            //    LogSys.Error("StaticDataManager load failed, file:t_specific_config.xml");
            //    return false;
            //}

            if(!LoadGodConfig())
            {
                return false;
            }

            if(!LoadBuffConfig())
            {
                return false;
            }

            return true;
        }

        public byte[] testProtobuf()
        {
            byte[] data;
            this.SerializeToBinary(out data);
            return data;
        }

        bool LoadUnityConfig()
        {
            UnitConfigImp config = new UnitConfigImp();
            if (!config.Load(path + "\\" + "t_unit_config.xml"))
            {
                LogSys.Error("StaticDataManager load failed, file:t_unit_config.xml");
                return false;
            }
            staticData.unit = config.config;
            return true;
        }

        bool LoadArrowConfig()
        {
            ArrowConfigImp config = new ArrowConfigImp();
            if (!config.Load(path + "\\" + "t_arrow_config.xml"))
            {
                LogSys.Error("StaticDataManager load failed, file:t_arrow_config.xml");
                return false;
            }

            staticData.arrow = config.config;
            return true;
        }

        bool LoadGodConfig()
        {
            GodConfigImp config = new GodConfigImp();
            if (!config.Load(path + "\\" + "t_god_config.xml"))
            {
                LogSys.Error("StaticDataManager load failed, file:t_god_config.xml");
                return false;
            }

            staticData.god = config.config;
            return true;
        }

        bool LoadBuffConfig()
        {
            BuffConfigImp config = new BuffConfigImp();
            if (!config.Load(path + "\\" + "t_buff_config.xml"))
            {
                LogSys.Error("StaticDataManager load failed, file:t_buff_config.xml");
                return false;
            }
     
            staticData.buff = config.config;
            return true;
        }

        /// <summary>
        /// 相对路径，相当于配置文件
        /// </summary>
        protected string path = "";
    }
}
