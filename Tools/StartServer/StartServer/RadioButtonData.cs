﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StartServer
{
    public class ServerControlData
    {
        public string serverName = string.Empty;
        public RadioButton rBtnDebug = new RadioButton();
        public RadioButton rBtnDebugStep = new RadioButton();
        public RadioButton rBtnRelease = new RadioButton();
        public RadioButton rBtnNothing = new RadioButton();
        public PictureBox pBox = new PictureBox();
    }
}
