﻿namespace StartServer
{
    partial class FormStartServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartAll = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnRedis = new System.Windows.Forms.Button();
            this.btnClient = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnGm = new System.Windows.Forms.Button();
            this.btnConfigServerIP = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStartAll
            // 
            this.btnStartAll.Location = new System.Drawing.Point(459, 376);
            this.btnStartAll.Name = "btnStartAll";
            this.btnStartAll.Size = new System.Drawing.Size(75, 23);
            this.btnStartAll.TabIndex = 1;
            this.btnStartAll.Text = "启动全部";
            this.btnStartAll.UseVisualStyleBackColor = true;
            this.btnStartAll.Click += new System.EventHandler(this.btnStartAll_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(540, 376);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "终止全部";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnRedis
            // 
            this.btnRedis.Location = new System.Drawing.Point(257, 375);
            this.btnRedis.Name = "btnRedis";
            this.btnRedis.Size = new System.Drawing.Size(79, 23);
            this.btnRedis.TabIndex = 3;
            this.btnRedis.Text = "启动redis";
            this.btnRedis.UseVisualStyleBackColor = true;
            this.btnRedis.Click += new System.EventHandler(this.btnStartRedis_Click);
            // 
            // btnClient
            // 
            this.btnClient.Enabled = false;
            this.btnClient.Location = new System.Drawing.Point(92, 376);
            this.btnClient.Name = "btnClient";
            this.btnClient.Size = new System.Drawing.Size(99, 23);
            this.btnClient.TabIndex = 4;
            this.btnClient.Text = "启动模拟客户端";
            this.btnClient.UseVisualStyleBackColor = true;
            this.btnClient.Click += new System.EventHandler(this.btnClient_Click);
            // 
            // btnTest
            // 
            this.btnTest.Enabled = false;
            this.btnTest.Location = new System.Drawing.Point(12, 376);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(74, 23);
            this.btnTest.TabIndex = 5;
            this.btnTest.Text = "协议测试";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnGm
            // 
            this.btnGm.Enabled = false;
            this.btnGm.Location = new System.Drawing.Point(197, 376);
            this.btnGm.Name = "btnGm";
            this.btnGm.Size = new System.Drawing.Size(53, 23);
            this.btnGm.TabIndex = 6;
            this.btnGm.Text = "GM工具";
            this.btnGm.UseVisualStyleBackColor = true;
            this.btnGm.Click += new System.EventHandler(this.btnGm_Click);
            // 
            // btnConfigServerIP
            // 
            this.btnConfigServerIP.Location = new System.Drawing.Point(342, 375);
            this.btnConfigServerIP.Name = "btnConfigServerIP";
            this.btnConfigServerIP.Size = new System.Drawing.Size(107, 23);
            this.btnConfigServerIP.TabIndex = 7;
            this.btnConfigServerIP.Text = "配置服务器IP";
            this.btnConfigServerIP.UseVisualStyleBackColor = true;
            this.btnConfigServerIP.Click += new System.EventHandler(this.btnConfigServerIp_Click);
            // 
            // FormStartServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 408);
            this.Controls.Add(this.btnConfigServerIP);
            this.Controls.Add(this.btnGm);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.btnClient);
            this.Controls.Add(this.btnRedis);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStartAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormStartServer";
            this.Text = "StartServer";
            this.Load += new System.EventHandler(this.FormStartServer_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStartAll;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnRedis;
        private System.Windows.Forms.Button btnClient;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnGm;
        private System.Windows.Forms.Button btnConfigServerIP;
    }
}

