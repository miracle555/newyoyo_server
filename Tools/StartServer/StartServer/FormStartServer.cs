﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Config;
using Redis;
using StaticData;
using ProtoBuf;

namespace StartServer
{
    public partial class FormStartServer : Form
    {
        public FormStartServer()
        {
            InitializeComponent();
        }

        System.Threading.Timer m_timer;
        
        private void FormStartServer_Load(object sender, EventArgs e)
        {
            bool isExists = File.Exists("Server.ini");
            if (!isExists)
            {
                CreateIniFile();
            }

            m_timer = new System.Threading.Timer(this.ShowStartServerColor, null, 0, 1000);

            string content = File.ReadAllText("Server.ini");
            string[] contentLines = content.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            //this.Size = new Size(643, contentLines.Length * 50);
            //btnTest.Location = new Point(12, contentLines.Length * 42);
            //btnClient.Location = new Point(92, contentLines.Length * 42);
            //btnMonitor.Location = new Point(197, contentLines.Length * 42);
            //btnGm.Location = new Point(282, contentLines.Length * 42);
            //btnStatistics.Location = new Point(342, contentLines.Length * 42);
            //btnStartAll.Location = new Point(459, contentLines.Length * 42);
            //btnStop.Location = new Point(540, contentLines.Length * 42);
            int height = contentLines.Length * 40 + 90;
            this.Size = new Size(643, height);

            //btnTest.Location = new Point(12, height - 72);
            //btnClient.Location = new Point(92, height - 72);
            //btnGm.Location = new Point(282, height - 72);
            //btnRedis.Location = new Point(197, height - 72);
            //btnConfigServerIP.Location = new Point(342, height - 72);
            //btnStartAll.Location = new Point(459, height - 72);
            //btnStop.Location = new Point(540, height - 72);

            btnTest.Location = new Point(btnTest.Location.X, height - 72);
            btnClient.Location = new Point(btnClient.Location.X, height - 72);
            btnGm.Location = new Point(btnGm.Location.X, height - 72);
            btnRedis.Location = new Point(btnRedis.Location.X, height - 72);
            btnConfigServerIP.Location = new Point(btnConfigServerIP.Location.X, height - 72);
            btnStartAll.Location = new Point(btnStartAll.Location.X, height - 72);
            btnStop.Location = new Point(btnStop.Location.X, height - 72);

            for (int i = 0;i < contentLines.Length;i++)
            {
                string[] serverInfo = contentLines[i].Split(' ');
                GroupBox gBox = new GroupBox();
                gBox.Name = "gBox" + i.ToString();
                gBox.Location = new Point(27, 40*i);
                gBox.Size = new Size(576, 50);

                PictureBox pbox = new PictureBox();
                pbox.Size = new System.Drawing.Size(26, 23);
                pbox.Location = new Point(7, 17);
                //pbox.Image = global::StartServer.Properties.Resources._1069976;
                pbox.Name = "pic" + i.ToString();
                gBox.Controls.Add(pbox);

                Label lbl = new Label();
                lbl.Name = "lbl" + i.ToString();
                lbl.Text = serverInfo[0] + ":";
                lbl.Location = new Point(37, 17);
                lbl.AutoSize = true;
                gBox.Controls.Add(lbl);

                RadioButton rBtnDebug = new RadioButton();
                gBox.Controls.Add(rBtnDebug);
                rBtnDebug.Name = "RunDebug" + i.ToString();
                rBtnDebug.Location = new Point(137, 17);
                rBtnDebug.Text = "Debug";
                rBtnDebug.Size = new Size(80, 16);

                RadioButton rBtnDebugStep = new RadioButton();
                gBox.Controls.Add(rBtnDebugStep);
                rBtnDebugStep.Name = "RunDebugStep" + i.ToString();
                rBtnDebugStep.Location = new Point(217, 17);
                rBtnDebugStep.Text = "Step";
                rBtnDebugStep.Size = new Size(100, 16);

                RadioButton rBtnRelease = new RadioButton();
                gBox.Controls.Add(rBtnRelease);
                rBtnRelease.Name = "RunRelease" + i.ToString();
                rBtnRelease.Location = new Point(317, 17);
                rBtnRelease.Text = "Release";
                rBtnRelease.Size = new Size(90, 16);

                RadioButton rBtnNothing = new RadioButton();
                gBox.Controls.Add(rBtnNothing);
                rBtnNothing.Name = "NotRun" + i.ToString();
                rBtnNothing.Location = new Point(407, 17);
                rBtnNothing.Text = "Ignore";
                rBtnNothing.Size = new Size(70, 16);

                Button btn = new Button();
                btn.Name = "btn" + i.ToString();
                btn.Text = "启动";
                btn.Location = new Point(487, 11);
                gBox.Controls.Add(btn);

                btn.Click += new EventHandler(Button_Click);

                ServerControlData rbuttonData = new ServerControlData();
                rbuttonData.rBtnDebug = rBtnDebug;
                rbuttonData.rBtnDebugStep = rBtnDebugStep;
                rbuttonData.rBtnRelease = rBtnRelease;
                rbuttonData.rBtnNothing = rBtnNothing;
                rbuttonData.serverName = serverInfo[0];
                rbuttonData.pBox = pbox;
                m_radioMap.Add(btn.Name, rbuttonData);

                switch (serverInfo[1])
                {
                    case "1":
                        rBtnDebug.Checked = true;
                        break;
                    case "2":
                        rBtnDebugStep.Checked = true;
                        break;
                    case "3":
                        rBtnRelease.Checked = true;
                        break;
                    case "4":
                        rBtnNothing.Checked = true;
                        break;
                }

                this.Controls.Add(gBox);
            }
        }

        private void ShowStartServerColor(object o)
        {
            try
            {
                foreach (KeyValuePair<string, ServerControlData> kvp in m_radioMap)
                {
                    bool isExists = GetProcess(kvp.Value.serverName);
                    if (isExists)
                    {
                        kvp.Value.pBox.Image = global::StartServer.Properties.Resources._1069976;
                    }
                    else
                    {
                        kvp.Value.pBox.Image = null;
                    }
                }
            }
            catch(Exception e)
            {
                e.ToString();
            }
        }

        private void CreateIniFile()
        {
            string content = string.Empty;
            content = "MonitorServer 3" + Environment.NewLine;
            content += "BattleServer 3" + Environment.NewLine;
            content += "GateServer 3" + Environment.NewLine;
            content += "LobbyServer 3" + Environment.NewLine;
            File.WriteAllText("Server.ini", content);
        }

        Dictionary<string, ServerControlData> m_radioMap = new Dictionary<string, ServerControlData>();

        private void Button_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string btnName = btn.Name;
            StartProcess(btnName);
            Save();
        }

        private bool GetProcess(string processName)
        {
            bool isExists = false;
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcesses();
            for (int i=0;i<processes.Length - 1;i++)
            {
                if (processes[i].ProcessName == processName)
                {
                    isExists = true;
                    break;
                }
            }
            return isExists;
        }

        

        private void StartProcess(string btnName)
        {
            ServerControlData rbuttonData = new ServerControlData();
            if (m_radioMap.TryGetValue(btnName, out rbuttonData))
            {
                bool isExists = GetProcess(rbuttonData.serverName);
                if (isExists)
                    return;

                string cfg = rbuttonData.serverName.Substring(0, rbuttonData.serverName.Length - 6);
                cfg = "..\\cfg\\" + cfg + "SvrCfg1.xml";
                string serverName = rbuttonData.serverName + ".exe";

                string path = string.Empty;
                if (rbuttonData.rBtnDebug.Checked)
                {
                    path = "..\\Debug\\";
                    System.Diagnostics.Process.Start(path + serverName, cfg);
                }
                else if (rbuttonData.rBtnDebugStep.Checked)
                {
                    path = "..\\Debug\\";
                    System.Diagnostics.Process.Start(path + serverName, cfg + " step");
                }
                else if (rbuttonData.rBtnRelease.Checked)
                {
                    path = "..\\Release\\";
                    System.Diagnostics.Process.Start(path + serverName, cfg);
                }
                else if (rbuttonData.rBtnNothing.Checked)
                {
                    //不启动
                }

            }
            //ServerControlData rbuttonData = new ServerControlData();
            //if (m_radioMap.TryGetValue(btnName, out rbuttonData))
            //{
            //    bool isExists = GetProcess(rbuttonData.serverName);
            //    if (isExists)
            //        return;

            //    string cfg = rbuttonData.serverName.Substring(0, rbuttonData.serverName.Length - 6);
            //    cfg = ".\\cfg\\" + cfg + "SvrCfg1.xml";
            //    string serverName = rbuttonData.serverName + ".exe";

            //    string path = string.Empty;
            //    if (rbuttonData.rBtnDebug.Checked)
            //    {
            //        path = ".\\Debug\\";
            //        System.Diagnostics.Process.Start(path + serverName, cfg);
            //    }
            //    else if (rbuttonData.rBtnDebugStep.Checked)
            //    {
            //        path = ".\\Debug\\";
            //        System.Diagnostics.Process.Start(path + serverName, cfg + " step");
            //    }
            //    else if (rbuttonData.rBtnRelease.Checked)
            //    {
            //        path = ".\\Release\\";
            //        System.Diagnostics.Process.Start(path + serverName, cfg);
            //    }
            //    else if (rbuttonData.rBtnNothing.Checked)
            //    {
            //        //不启动
            //    }

            //}
        }

        private void Save()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            foreach (KeyValuePair<string, ServerControlData> kvp in m_radioMap)
            {
                string saveRadio = string.Empty;
                ServerControlData rbuttonData = kvp.Value;
                if (rbuttonData.rBtnDebug.Checked)
                {
                    saveRadio = "1";
                }
                else if (rbuttonData.rBtnDebugStep.Checked)
                {
                    saveRadio = "2";
                }
                else if (rbuttonData.rBtnRelease.Checked)
                {
                    saveRadio = "3";
                }
                else if (rbuttonData.rBtnNothing.Checked)
                {
                    saveRadio = "4";
                }
                dic.Add(rbuttonData.serverName, saveRadio);
            }

            string content = string.Empty;
            foreach(KeyValuePair<string, string> kvp in dic)
            {
                content += kvp.Key + " " + kvp.Value + Environment.NewLine;
            }
            File.WriteAllText("Server.ini", content);
        }

        private void btnStartAll_Click(object sender, EventArgs e)
        {
            foreach (KeyValuePair<string, ServerControlData> kvp in m_radioMap)
            {
                StartProcess(kvp.Key);
            }
            Save();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process[] processes = System.Diagnostics.Process.GetProcesses();
            for (int i = 0; i < processes.Length - 1; i++)
            {
                foreach (KeyValuePair<string, ServerControlData> kvp in m_radioMap)
                {
                    string processName = kvp.Value.serverName;
                    if (processes[i].ProcessName == processName)
                    {
                        processes[i].Kill();
                    }
                }
            }

            Save();
        }

        private void btnStartRedis_Click(object sender, EventArgs e)
        {
            bool isExists = GetProcess("redis-server");
            if (isExists)
                return;

            Environment.CurrentDirectory = "..\\Redis-x64-3.2.100\\";
            string serverName = "redis-server.exe";
            string cfg = "redis.conf";
            
            System.Diagnostics.Process.Start(serverName, cfg);

            Environment.CurrentDirectory = "..\\Debug\\";
        }

        private void btnClient_Click(object sender, EventArgs e)
        {
            bool isExists = GetProcess("SimClientDriver");
            if (isExists)
                return;

            string serverName = "SimClientDriver.exe";
            //string path = ".\\Debug\\";
            Environment.CurrentDirectory = ".\\Debug";
            System.Diagnostics.Process.Start(serverName, "..\\ClientScript" + " " + "..\\ClientScript\\Config.xml");
            Environment.CurrentDirectory = "..\\";
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            bool isExists = GetProcess("ProtocolTest");
            if (isExists)
                return;

            string serverName = "ProtocolTest.exe";
            //string path = ".\\Debug\\";
            Environment.CurrentDirectory = ".\\Debug";
            System.Diagnostics.Process.Start(serverName);
            Environment.CurrentDirectory = "..\\";
        }

        private void btnGm_Click(object sender, EventArgs e)
        {
            bool isExists = GetProcess("GmClient");
            if (isExists)
                return;

            string serverName = "GmClient.exe";
            //string path = ".\\Debug\\";
            Environment.CurrentDirectory = ".\\Debug";
            System.Diagnostics.Process.Start(serverName);
            Environment.CurrentDirectory = "..\\";
        }

        ///// <summary>
        ///// 启动经分客户端
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void btnStatistics_Click(object sender, EventArgs e)
        //{
        //    bool isExists = GetProcess("StatisticsClient");
        //    if (isExists)
        //        return;

        //    string serverName = "StatisticsClient.exe";
        //    Environment.CurrentDirectory = ".\\Debug";
        //    System.Diagnostics.Process.Start(serverName);
        //    Environment.CurrentDirectory = "..\\";
        //}

        ServerIPTable serverIpTable = new ServerIPTable();
        RedisSystem redis = new RedisSystem();
        StaticDataManagerImp staticDataManager = new StaticDataManagerImp();

        /// <summary>
        /// 配置服务器IP
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfigServerIp_Click(object sender, EventArgs e)
        {
            redis.Init("127.0.0.1", 6379);

            LoadServerIpConfig();
            LoadStaticData();
        }

        void LoadServerIpConfig()
        {
            string ipFile = "ServerIPTable.xml";
            serverIpTable.Load(ipFile);
            
            redis.setObject("iptable", serverIpTable.serverItems);
        }

        void LoadStaticData()
        {
            string strPath = "..\\StaticData";
            if(!staticDataManager.Init(strPath))
            {
                MessageBox.Show("策划数据表配置错误，请检查");
                return;
            }

            StaticDataManager staticData = staticDataManager.staticData;

            redis.setObject("staticdata", staticData);
        }
    }
}
