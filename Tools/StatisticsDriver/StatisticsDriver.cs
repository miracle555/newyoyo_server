﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Config;
using System.IO;
using ServerLib;
using System.Runtime.InteropServices;
using DB;
using Utility.DAL;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Reflection;

namespace StatisticsDriver
{
    public partial class StatisticsDriver : Form
    {
        //////////////////////////////////////////////////////////////////////////
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string section, string key, string defVal, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        //////////////////////////////////////////////////////////////////////////
        ServerConfig config = new ServerConfig();
        DBLocalConfig dbConfig = new DBLocalConfig();
        const string ini = ".\\StatisticsDriver.ini";
        DBSystem db = new DBSystem();
        Timer timer = new Timer();

        //////////////////////////////////////////////////////////////////////////
        public StatisticsDriver()
        {
            InitializeComponent();
            Init();
        }

        //////////////////////////////////////////////////////////////////////////
        void Init()
        {
            LoadIni();

            config.DBLocalMode = 2;
            dbConfig.DatabaseType = 1;
            dbConfig.AutoCreate = true;
            dbConfig.LogicTypeList = "1;2;3;4;5";
            dbConfig.DALProviderType = 1;
            config.DBLocalCfg = new List<DBLocalConfig>();
            config.DBLocalCfg.Add(dbConfig);
        }

        void LoadIni()
        {
            if (!File.Exists(ini))
            {
                var file = File.Create(ini);
                file.Close();
            }
            else
            {
                StringBuilder sb = new StringBuilder(255);
                GetPrivateProfileString("Database", "host", "", sb, 255, ini);
                textBoxHost.Text = sb.ToString();
                GetPrivateProfileString("Database", "port", "", sb, 255, ini);
                textBoxPort.Text = sb.ToString();
                GetPrivateProfileString("Database", "account", "", sb, 255, ini);
                textBoxAccount.Text = sb.ToString();
                GetPrivateProfileString("Database", "password", "", sb, 255, ini);
                textBoxPassword.Text = sb.ToString();
                GetPrivateProfileString("Database", "database", "", sb, 255, ini);
                textBoxDatabase.Text = sb.ToString();
            }
        }

        void SaveIni()
        {
            if (!File.Exists(ini))
            {
                var file = File.Create(ini);
                file.Close();
            }

            WritePrivateProfileString("Database", "host", textBoxHost.Text, ini);
            WritePrivateProfileString("Database", "port", textBoxPort.Text, ini);
            WritePrivateProfileString("Database", "account", textBoxAccount.Text, ini);
            WritePrivateProfileString("Database", "password", textBoxPassword.Text, ini);
            WritePrivateProfileString("Database", "database", textBoxDatabase.Text, ini);
        }

        void Tick(object sender, EventArgs e)
        {
            db.Update(200);
        }

        void output(string line, bool includeTime = true)
        {
            if (includeTime)
            {
                string msg = string.Format("[{0}]{1}", DateTime.Now.ToString(), line) + Environment.NewLine;
                textBoxOutput.AppendText(msg);
            }
            else
            {
                string msg = line + Environment.NewLine;
                textBoxOutput.AppendText(msg);
            }
        }

        void error(string line)
        {
            output(line);
            MessageBox.Show(line, "Error");
        }

        //////////////////////////////////////////////////////////////////////////
        private void buttonConnect_Click(object sender, EventArgs e)
        {
            dbConfig.HostIp = textBoxHost.Text;
            dbConfig.Port = string.IsNullOrEmpty(textBoxPort.Text) ? 0 : int.Parse(textBoxPort.Text);
            dbConfig.Username = textBoxAccount.Text;
            dbConfig.Password = textBoxPassword.Text;
            dbConfig.Database = textBoxDatabase.Text;

            textBoxHost.Enabled = false;
            textBoxPort.Enabled = false;
            textBoxAccount.Enabled = false;
            textBoxPassword.Enabled = false;
            textBoxDatabase.Enabled = false;
            buttonConnect.Enabled = false;
            SaveIni();

            DBSystem.CollectDbSchema();
            db.Init(config);

            timer.Interval = 200;
            timer.Tick += Tick;
            timer.Start();

            output("Database connected");
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxActor.Text))
            {
                error("Item is null");
                return;
            }

            if (string.IsNullOrEmpty(textBoxScript.Text))
            {
                error("Script is null");
                return;
            }

            CompilerResults results;
            using (var provider = new CSharpCodeProvider())
            {
                CompilerParameters options = new CompilerParameters();
                options.GenerateInMemory = true;
                options.ReferencedAssemblies.Add("System.dll");
                options.ReferencedAssemblies.Add("System.Core.dll");
                options.ReferencedAssemblies.Add("System.Data.dll");
                options.ReferencedAssemblies.Add("System.Data.DataSetExtensions.dll");
                options.ReferencedAssemblies.Add("System.Xml.dll");
                options.ReferencedAssemblies.Add("System.Xml.Linq.dll");
                options.ReferencedAssemblies.Add("Utility.dll");
                options.ReferencedAssemblies.Add("ServerLib.dll");
                options.ReferencedAssemblies.Add("log4net.dll");
                options.ReferencedAssemblies.Add("protobuf-net.dll");
                options.ReferencedAssemblies.Add("CommonLib.dll");
                options.ReferencedAssemblies.Add("ItemComponent.dll");
                options.ReferencedAssemblies.Add("ScriptContext.dll");
                options.ReferencedAssemblies.Add("Protocols.dll");
                options.ReferencedAssemblies.Add("LogicLib.dll");

                string[] scripts = { textBoxScript.Text };
                results = provider.CompileAssemblyFromFile(options, scripts);
                if (results.Errors.HasErrors)
                {
                    foreach (CompilerError err in results.Errors)
                    {
                        string msg = string.Format("{0} {1}", err.Line, err.ErrorText);
                        output(msg);
                    }
                    error("Compile failed");
                    return;
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT * FROM role");

            DALIdentity identity = new DALIdentity(0, 0);
            DataSet rs = db.ExecuteDataSetSync(identity, DALIdentity.DefaultSplitCatalog, sb.ToString());

            if (rs.HasErrors)
            {
                error("Load item failed");
                return;
            }

            List<UInt64> ids = new List<ulong>();
            List<string> names = new List<string>();
            List<byte[]> items = new List<byte[]>();
            foreach (DataRow row in rs.Tables[0].Rows)
            {
                UInt64 uid = UInt64.Parse(row["id"].ToString());
                string name = row["name"].ToString();
                byte[] item = row[textBoxActor.Text] == DBNull.Value ? null : (byte[])row[textBoxActor.Text];

                ids.Add(uid);
                names.Add(name);
                items.Add(item);
            }

            String script = System.IO.Path.GetFileNameWithoutExtension(textBoxScript.Text);
            string totalClassName = "Script." + script;
            Type type = results.CompiledAssembly.GetType(totalClassName);
            MethodInfo method = type.GetMethod("Run");
            if (method != null)
            {
                string reportDir = @".\Report";
                if (!Directory.Exists(reportDir))
                {
                    Directory.CreateDirectory(reportDir);
                }

                DateTime now = DateTime.Now;
                string report = string.Format(@"{0}\report_{1}{2:00}{3:00}-{4:00}{5:00}{6:00}{7:000}.log",
                    reportDir, now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, now.Millisecond);
                FileStream fs = new FileStream(report, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);

                try
                {
                    object[] objects = { sw, ids, names, items };
                    method.Invoke(null, objects);
                    sw.Flush();
                    fs.Position = 0;
                    StreamReader sr = new StreamReader(fs);
                    string line = sr.ReadLine();
                    while (line != null)
                    {
                        output(line);
                        line = sr.ReadLine();
                    }
                }
                catch (Exception ex)
                {
                    error(string.Format("Script exception: {0}", ex.Message));
                }
                finally
                {
                    fs.Close();
                }
            }
            output("completed");
        }

        private void buttonScript_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                textBoxScript.Text = fileBrowser.FileName;
            }
        }
    }
}
