﻿/* ============================================================
* Author:       xieqin
* Time:         2015/7/9 14:08:13
* FileName:     ProtoConnect
* Purpose:      ProtoConnect
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Protocols
{
    public class ServerConnected : ProtoBody
    {
        public IPEndPoint endpoint;
        public bool success;
    }

    public class ServerDisconnected : ProtoBody
    {
    }

    public class ProtoIDReserve : ProtoID
    {
        protected ProtoIDReserve(UInt16 protoIDIn, Type bodyTypeIn, string nameIn)
            : base(protoIDIn, bodyTypeIn, nameIn, false, true)
        {
        }

        public static ProtoIDReserve CMD_SERVER_CONNECTED = new ProtoIDReserve(0xff01, typeof(ServerConnected), "CMD_SERVER_CONNECTED");
        public static ProtoIDReserve CMD_SERVER_DISCONNECTED = new ProtoIDReserve(0xff02, typeof(ServerDisconnected), "CMD_SERVER_DISCONNECTED");
    }
}
