﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protocols;
using System.Runtime.InteropServices;
using System.IO;
using System.Net;
using System.Reflection;
using ProtoBuf;
using Protocols.Error;
using Utility.Common;

namespace ProtocolTest
{
    public partial class ProtocolTest : Form
    {
        //////////////////////////////////////////////////////////////////////////
        Timer timer = new Timer();
        SessionManager sessionManager = new SessionManager();
        List<Type> protoTypes = new List<Type>();
        List<Type> protoToTest = new List<Type>();
        int protoIndex = -1;
        Random ra = new Random();
        Dictionary<int, ListViewItem> items = new Dictionary<int, ListViewItem>();
        static char[] constant = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@', '#', '$', '%', '^', '&', '*', '|', '+', '(', ')' };
        int testing;
        int frequency;

        //////////////////////////////////////////////////////////////////////////
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string section, string key, string defVal, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        //////////////////////////////////////////////////////////////////////////
        public ProtocolTest()
        {
            InitializeComponent();
            InitData();
            InitForm();
        }

        //////////////////////////////////////////////////////////////////////////
        void LoadData()
        {
            if (!File.Exists(".\\ProtocolTest.ini"))
            {
                var file = File.Create(".\\ProtocolTest.ini");
                file.Close();
            }
            else
            {
                StringBuilder sb = new StringBuilder(255);
                GetPrivateProfileString("Login", "ip", "", sb, 255, ".\\ProtocolTest.ini");
                TBServerIp.Text = sb.ToString();
                GetPrivateProfileString("Login", "account", "", sb, 255, ".\\ProtocolTest.ini");
                TBAccount.Text = sb.ToString();
                GetPrivateProfileString("Login", "password", "", sb, 255, ".\\ProtocolTest.ini");
                TBPassword.Text = sb.ToString();

                int res = GetPrivateProfileString("Data", "frequency", "", sb, 255, ".\\ProtocolTest.ini");
                frequency = res == 0 ? 0 : int.Parse(sb.ToString());
                res = GetPrivateProfileString("Data", "playerCount", "", sb, 255, ".\\ProtocolTest.ini");
                int playerCount = res == 0 ? 1 : int.Parse(sb.ToString());
                TBPlayerCount.Text = playerCount.ToString();
            }
        }

        void SaveData()
        {
            if (!File.Exists(".\\ProtocolTest.ini"))
            {
                var file = File.Create(".\\ProtocolTest.ini");
                file.Close();
            }

            WritePrivateProfileString("Login", "ip", TBServerIp.Text, ".\\ProtocolTest.ini");
            WritePrivateProfileString("Login", "account", TBAccount.Text, ".\\ProtocolTest.ini");
            WritePrivateProfileString("Login", "password", TBPassword.Text, ".\\ProtocolTest.ini");
            WritePrivateProfileString("Data", "frequency", frequency.ToString(), ".\\ProtocolTest.ini");
            WritePrivateProfileString("Data", "playerCount", TBPlayerCount.Text, ".\\ProtocolTest.ini");
        }

        void InitData()
        {
            LoadData();

            Assembly assembly = Assembly.GetAssembly(typeof(ProtoBody));
            foreach (Type type in assembly.GetTypes())
            {
                if (!type.IsAbstract && type.IsSubclassOf(typeof(ProtoBody)))
                {
                    Type[] types = new Type[] { };
                    object[] objs = new object[] { };
                    ConstructorInfo constructor = type.GetConstructor(types);
                    object body = constructor.Invoke(objs);
                    protoTypes.Add(type);
                }
            }

            testing = 0;
            timer.Interval = 200;
            timer.Tick += Tick;
            timer.Start();
        }

        void InitForm()
        {
            listViewProto.Columns.Add("Proto", "协议", 180);

            BtnConnect.Enabled = false;
            BtnRegister.Enabled = false;
            BtnLogin.Enabled = false;
            BtnLogout.Enabled = false;
            BtnProtocolTest.Enabled = false;
            BtnStreamTest.Enabled = false;
            BtnStopTest.Enabled = false;
            buttonAddProto.Enabled = false;

            listViewProto.FullRowSelect = true;
        }

        void Tick(object sender, EventArgs e)
        {
            int sessionId = 0;
            Protocol proto = sessionManager.GetMessage(out sessionId);
            while (proto != null)
            {
                if (proto.GetID() == ProtoIDReserve.CMD_SERVER_CONNECTED)
                {
                    ServerConnected body = (ServerConnected)proto.GetBody();
                    HandleConnect(sessionId, body.success);
                }
                else if (proto.GetID() == ProtoIDReserve.CMD_SERVER_DISCONNECTED)
                {
                    HandleDisconnect(sessionId);
                }
                else
                {
                    HandleMessage(sessionId, proto);
                }

                proto = sessionManager.GetMessage(out sessionId);
            }

            if (testing == 1)
            {
                RandomSendProto();
            }
            else if (testing == 2)
            {
                RandomSendStream();
            }
        }

        void HandleConnect(int sessionId, bool success)
        {
            if (success)
            {
                items[sessionId].SubItems[2].Text = "已连接";
            }
            else
            {
                items[sessionId].SubItems[2].Text = "连接失败";
            }
        }

        void HandleDisconnect(int sessionId)
        {
            items[sessionId].SubItems[1].Text = "";
            items[sessionId].SubItems[2].Text = "离线";
            items.Remove(sessionId);
        }

        void HandleMessage(int sessionId, Protocol proto)
        {
            if (proto.GetID() == ProtoID.CMD_LOGIN_USERLOGIN_RSP)
            {
                UserLoginRsp rsp = (UserLoginRsp)proto.GetBody();
                if (rsp.errorCode != ErrorID.Success)
                {
                    if (testing == 0)
                        items[sessionId].SubItems[2].Text = "登录失败";
                }
                else
                {
                    items[sessionId].SubItems[1].Text = rsp.roleName;
                    items[sessionId].SubItems[2].Text = "在线";
                }
            }
            else if (proto.GetID() == ProtoID.CMD_LOGIN_CREATE_ROLE_RSP)
            {
                CreateRoleRsp roleBody = (CreateRoleRsp)proto.GetBody();
                if (roleBody.errorCode == ErrorID.Success)
                {
                    if (testing == 0)
                        items[sessionId].SubItems[2].Text = "已注册";
                }
            }
            else if (proto.GetID() == ProtoID.CMD_LOGIN_CREATE_ACCOUNT_RSP)
            {
                CreateAccountRsp body = (CreateAccountRsp)proto.GetBody();
                if (body.errorCode == ErrorID.Error_Create_Duplicated_Account)
                {
                    if (testing == 0)
                        items[sessionId].SubItems[2].Text = "已注册";
                }
            }
            else if (proto.GetID() == ProtoID.CMD_CHANGE_NAME_RSP)
            {
                ChangeNameRsp rsp = (ChangeNameRsp)proto.GetBody();
                if (rsp.errorCode == ErrorID.Success)
                {
                    items[sessionId].SubItems[1].Text = rsp.req.name;
                }
            }
        }

        void SendMessage(int sessionId, ProtoBody body)
        {
            Protocol proto = new Protocol(body);
            sessionManager.SendMessage(sessionId, body);

            OutputDetail(string.Format("[{0}]Server <------ {1}", sessionId, proto.GetID().ToString()));
            OutputDetail(ObjectDumper.Dump(body, true));
        }

        void SendMessage(int sessionId, Protocol proto)
        {
            sessionManager.SendMessage(sessionId, proto);

            OutputDetail(string.Format("[{0}]Server <------ {1}", sessionId, proto.GetID().ToString()));
            OutputDetail(ObjectDumper.Dump(proto.GetBody(), true));
        }

        void SendStream(int sessionId, Stream stream)
        {
            sessionManager.SendMessage(sessionId, stream);

            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            string text = reader.ReadToEnd();
            OutputDetail(string.Format("[{0}]Server <------ Stream", sessionId));
            OutputDetail(string.Format("Stream: {0}", text));
        }

        void OutputDetail(string line)
        {
            line += "\n";
            TBOutputDetail.AppendText(line);
        }

        void RandomSendProto()
        {
            if (frequency == 0)
                frequency = 1;

            if (protoIndex == -1)
            {
                foreach (var item in items)
                {
                    int count = protoTypes.Count;
                    int index = ra.Next(0, count);
                    Type type = protoTypes[index];
                    for (int i = 0; i < frequency; i++)
                    {
                        object body = RandomFillData(type);
                        Protocol msg = new Protocol((ProtoBody)body);
                        SendMessage(item.Key, msg);
                    }
                }
            }
            else
            {
                for (int i = 0; i < frequency; i++)
                {
                    foreach (var item in items)
                    {
                        Type type = protoToTest[protoIndex];
                        object body = RandomFillData(type);
                        Protocol msg = new Protocol((ProtoBody)body);
                        SendMessage(item.Key, msg);
                    }

                    protoIndex++;
                    if (protoIndex >= protoToTest.Count)
                        protoIndex = 0;
                }
            }
        }

        void RandomSendStream()
        {
            if (frequency == 0)
                frequency = 1;

            foreach (var item in items)
            {
                for (int i = 0; i < frequency; i++)
                {
                    int len = ra.Next(1024);
                    byte[] buffer = new byte[len];
                    ra.NextBytes(buffer);

                    using (MemoryStream stream = new MemoryStream(buffer))
                    {
                        SendStream(item.Key, stream);
                    }
                }
            }
        }

        object RandomFillData(Type type)
        {
            Type[] types = new Type[] { };
            object[] objs = new object[] { };
            ConstructorInfo constructor = type.GetConstructor(types);
            if (constructor == null)
                return null;

            object obj = constructor.Invoke(objs);
            foreach (var field in type.GetFields())
            {
                var attr = field.GetCustomAttribute<ProtoMemberAttribute>();
                if (attr == null)
                    continue;

                Type fieldType = field.FieldType;
                if (fieldType.IsPrimitive)
                {
                    if (fieldType == typeof(Int16))
                    {
                        object[] vals = { (Int16)0, (Int16)(-1), (Int16)1, (Int16)(-100), (Int16)100, (Int16)(-1000), (Int16)1000, Int16.MinValue, Int16.MaxValue, (Int16)ra.Next(Int16.MinValue, Int16.MaxValue) };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                    else if (fieldType == typeof(UInt16))
                    {
                        object[] vals = { (UInt16)0, (UInt16)1, (UInt16)100, (UInt16)1000, UInt16.MinValue, UInt16.MaxValue, (UInt16)ra.Next(UInt16.MinValue, UInt16.MaxValue) };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                    else if (fieldType == typeof(Int32))
                    {
                        object[] vals = { 0, -1, 1, -100, 100, -1000, 1000, Int32.MinValue, Int32.MaxValue, ra.Next(Int32.MinValue, Int32.MaxValue) };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                    else if (fieldType == typeof(UInt32))
                    {
                        object[] vals = { (UInt32)0, (UInt32)1, (UInt32)100, (UInt32)1000, UInt32.MinValue, UInt32.MaxValue, (UInt32)ra.Next(Int32.MaxValue) };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                    else if (fieldType == typeof(Int64))
                    {
                        object[] vals = { (Int64)0, (Int64)(-1), (Int64)1, (Int64)(-100), (Int64)(100), (Int64)(-1000), (Int64)1000, Int64.MinValue, Int64.MaxValue, (Int64)ra.Next(Int32.MinValue, Int32.MaxValue) };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                    else if (fieldType == typeof(UInt64))
                    {
                        object[] vals = { (UInt64)0, (UInt64)1, (UInt64)100, (UInt64)1000, UInt64.MinValue, UInt64.MaxValue, (UInt64)ra.Next(Int32.MaxValue) };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                    else if (fieldType == typeof(float))
                    {
                        object[] vals = { 0.0f, -1.0f, 1.0f, -100.0f, 100.0f, -1000.0f, 1000.0f, float.MinValue, float.MaxValue, (float)ra.NextDouble() };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                    else if (fieldType == typeof(double))
                    {
                        object[] vals = { 0.0d, -1.0d, 1.0d, -100.0d, 100.0d, -1000.0d, 1000.0d, double.MinValue, double.MaxValue, ra.NextDouble() };
                        field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                    }
                }
                else if (fieldType == typeof(string))
                {
                    string[] vals = { null, "", GenerateRandomString(ra.Next(1024)) };
                    field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                }
                else
                {
                    object[] vals = { null, RandomFillData(field.FieldType) };
                    field.SetValue(obj, vals[ra.Next(0, vals.Length)]);
                }
            }

            return obj;
        }

        string GenerateRandomString(int Length)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < Length; i++)
            {
                sb.Append(constant[ra.Next(constant.Length)]);
            }
            return sb.ToString();
        }

        //////////////////////////////////////////////////////////////////////////
        private void BtnLogout_Click(object sender, EventArgs e)
        {
            testing = 0;
            foreach (var item in items)
            {
                sessionManager.Disconnect(item.Key);
            }
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            UserLoginReq req = new UserLoginReq();
            req.platformType = 0;
            req.password = TBPassword.Text;

            foreach (var item in items)
            {
                req.account = item.Value.SubItems[0].Text;
                SendMessage(item.Key, req);
            }
        }

        private void BtnProtocolTest_Click(object sender, EventArgs e)
        {
            protoToTest.Clear();
            protoIndex = -1;

            if (listViewProto.Items.Count > 0)
            {
                foreach (ListViewItem item in listViewProto.Items)
                {
                    int index = listViewProto.Columns["Proto"].Index;
                    string proto = item.SubItems[index].Text;
                    Type type = protoTypes.Find(x => x.Name == proto);
                    if (type != null)
                    {
                        protoToTest.Add(type);
                    }
                }
                protoIndex = 0;
            }

            testing = 1;
        }

        private void BtnStopTest_Click(object sender, EventArgs e)
        {
            testing = 0;
        }

        private void BtnStreamTest_Click(object sender, EventArgs e)
        {
            testing = 2;
        }

        private void BtnRegister_Click(object sender, EventArgs e)
        {
            CreateAccountReq req = new CreateAccountReq();
            req.platformType = 0;
            req.password = TBPassword.Text;

            foreach (var item in items)
            {
                req.account = item.Value.SubItems[0].Text;
                SendMessage(item.Key, req);
            }
        }

        private void BtnSetup_Click(object sender, EventArgs e)
        {
            int playerCount = int.Parse(TBPlayerCount.Text);

            SaveData();

            LVState.BeginUpdate();
            for (int i = 0; i < playerCount; i++)
            {
                string account = string.Format("{0}{1}", TBAccount.Text, i + 1);
                ListViewItem lvi = new ListViewItem();
                lvi.ImageIndex = i;
                lvi.Text = account;
                lvi.SubItems.Add("");
                lvi.SubItems.Add("离线");
                LVState.Items.Add(lvi);
            }
            LVState.EndUpdate();

            TBAccount.Enabled = false;
            TBServerIp.Enabled = false;
            TBPassword.Enabled = false;
            TBPlayerCount.Enabled = false;
            BtnSetup.Enabled = false;
            BtnConnect.Enabled = true;
            BtnRegister.Enabled = true;
            BtnLogin.Enabled = true;
            BtnLogout.Enabled = true;
            BtnProtocolTest.Enabled = true;
            BtnStreamTest.Enabled = true;
            BtnStopTest.Enabled = true;
            buttonAddProto.Enabled = true;
        }

        private void BtnConnect_Click(object sender, EventArgs e)
        {
            string[] ips = TBServerIp.Text.Split(':');
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(ips[0]), int.Parse(ips[1]));
            int playerCount = int.Parse(TBPlayerCount.Text);
            for (int i = 0; i < playerCount; i++)
            {
                int sessionId = sessionManager.Connect(endpoint);
                items[sessionId] = LVState.Items[i];
            }
        }

        private void listViewProto_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (listViewProto.SelectedItems.Count > 0)
                {
                    foreach (ListViewItem item in listViewProto.SelectedItems)
                    {
                        listViewProto.Items.Remove(item);
                    }
                }
            }
        }

        private void buttonAddProto_Click(object sender, EventArgs e)
        {
            ProtoAdd form = new ProtoAdd(protoTypes);
            form.StartPosition = FormStartPosition.CenterParent;
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK
                && !string.IsNullOrEmpty(form.Proto))
            {
                int index = -1;
                if (listViewProto.SelectedItems.Count > 0)
                {
                    index = listViewProto.SelectedItems[0].Index;
                }

                ListViewItem item = new ListViewItem();
                item.Text = form.Proto;

                if (index == -1)
                {
                    listViewProto.Items.Add(item);
                }
                else
                {
                    listViewProto.Items.Insert(index, item);
                }
            }
        }
    }
}
