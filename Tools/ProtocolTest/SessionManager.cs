﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace ProtocolTest
{
    //Session管理器
    public class SessionManager
    {
        Dictionary<int, Session> sessionMap = new Dictionary<int, Session>();
        object thisLock = new object();
        int id;

        public SessionManager()
        {
            id = 1;
        }

        //发送协议
        public int SendMessage(int sessionId, ProtoBody msg)
        {
            Protocol proto = new Protocol(msg);
            return SendMessage(sessionId, proto);
        }

        public int SendMessage(int sessionId, Protocol proto)
        {
            lock (thisLock)
            {
                if (sessionMap.ContainsKey(sessionId))
                {
                    return sessionMap[sessionId].SendMessage(proto);
                }
            }
            return 1;
        }

        public int SendMessage(int sessionId, Stream stream)
        {
            lock (thisLock)
            {
                if (sessionMap.ContainsKey(sessionId))
                {
                    return sessionMap[sessionId].SendMessage(stream);
                }
            }
            return 1;
        }

        public Protocol GetMessage(out int sessionId)
        {
            LinkedList<int> removeList = new LinkedList<int>();
            Protocol proto = null;
            sessionId = 0;
            lock (thisLock)
            {
                foreach (KeyValuePair<int, Session> pair in sessionMap)
                {
                    proto = pair.Value.GetMessage();
                    if (proto == null)
                        continue;

                    sessionId = pair.Key;
                    if (proto.GetID() == ProtoIDReserve.CMD_SERVER_DISCONNECTED)
                    {
                        sessionMap.Remove(pair.Key);
                    }
                    break;
                }
            }

            return proto;
        }

        /// <summary>
        /// 主动连接
        /// </summary>
        /// <param name="endpoint">endpoint</param>
        public int Connect(IPEndPoint endpoint)
        {
            int sessionId = 0;
            lock (thisLock)
            {
                sessionId = id++;
                Session session = new Session();
                sessionMap.Add(sessionId, session);
                session.Connect(endpoint);
            }
            return sessionId;
        }

        /// <summary>
        /// 主动断开连接
        /// </summary>
        /// <param name="sessionId">session id</param>
        public void Disconnect(int sessionId)
        {
            lock (thisLock)
            {
                if (sessionMap.ContainsKey(sessionId))
                {
                    sessionMap[sessionId].Disconnect();
                }
            }
        }
    }
}
