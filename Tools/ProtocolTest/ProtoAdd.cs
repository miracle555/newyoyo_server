﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProtocolTest
{
    public partial class ProtoAdd : Form
    {
        public ProtoAdd(List<Type> protos)
        {
            InitializeComponent();

            cbProto.Items.Add("");
            foreach (var proto in protos)
            {
                cbProto.Items.Add(proto.Name);
            }

            btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public string Proto
        {
            get
            {
                string proto = (string)cbProto.SelectedItem;
                if (!string.IsNullOrEmpty(proto))
                {
                    return proto;
                }
                return null;
            }
        }
    }
}
