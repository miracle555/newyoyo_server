﻿/* ============================================================
* Author:       xieqin
* Time:         2015/7/9 14:01:07
* FileName:     Session
* Purpose:      Session
* =============================================================*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Net;

namespace ProtocolTest
{
    internal class Session
    {
        const int BUFFER_LEN = 65536;
        const int STREAM_LEN_MAX = 65536;

        Socket sock;

        //消息缓冲
        byte[] buffer = new byte[BUFFER_LEN];
        MemoryStream bufferStream;
        LinkedList<Protocol> protoList;
        object thisLock = new object();
        bool connecting;

        public bool IsConnected
        {
            get
            {
                if (sock != null && sock.Connected)
                {
                    return true;
                }
                return false;
            }
        }

        public Session()
        {
            connecting = false;

            sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sock.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);

            bufferStream = new MemoryStream();
            protoList = new LinkedList<Protocol>();
        }

        public void Connect(IPEndPoint endpoint)
        {
            if (connecting)
            {
                return;
            }
            else
            {
                connecting = true;
            }

            try
            {
                if (IsConnected)
                {
                    InitSocket();
                }

                object[] param = { sock, endpoint };
                sock.BeginConnect(endpoint, new AsyncCallback(HandleConnect), param);
            }
            catch (Exception ex)
            {
                OutputException(ex);
            }
        }

        /// <summary>
        /// 主动断开连接
        /// </summary>
        public void Disconnect()
        {
            if (IsConnected)
            {
                InitSocket();
                PostDisconnectProto();
            }
        }

        //发送协议
        public int SendMessage(ProtoBody body)
        {
            Protocol proto = new Protocol(body);
            return SendMessage(proto);
        }

        public int SendMessage(Protocol proto)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                proto.Serialize(stream, true);
                return SendMessage(stream);
            }
        }

        public Protocol GetMessage()
        {
            Protocol proto = null;
            lock (thisLock)
            {
                if (protoList.Count > 0)
                {
                    proto = protoList.First.Value;
                    protoList.RemoveFirst();
                }
            }
            return proto;
        }

        void InitSocket()
        {
            try
            {
                sock.Close();
                sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sock.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
            }
            catch (Exception ex)
            {
                OutputException(ex);
            }
        }

        void PostDisconnectProto()
        {
            ServerDisconnected body = new ServerDisconnected();
            Protocol proto = new Protocol(body);
            PostProto(proto);
        }

        void PostProto(Protocol proto)
        {
            lock (thisLock)
            {
                protoList.AddLast(proto);
            }
        }

        void BeginReceive()
        {
            sock.BeginReceive(buffer, 0, BUFFER_LEN, 0, new AsyncCallback(HandleReceive), null);
        }

        public int SendMessage(Stream stream)
        {
            if (!IsConnected)
            {
                return 1;
            }

            try
            {
                byte[] buf = new byte[stream.Length];
                stream.Position = 0;
                stream.Read(buf, 0, (int)stream.Length);
                sock.BeginSend(buf, 0, buf.Length, 0, new AsyncCallback(HandleSend), null);
            }
            catch (Exception ex)
            {
                OutputException(ex);

                InitSocket();
                PostDisconnectProto();
            }

            return 0;
        }

        void HandleReceive(IAsyncResult result)
        {
            try
            {
                if (!IsConnected)
                    return;

                int bytesRead = sock.EndReceive(result);
                if (bytesRead > 0)
                {
                    long pos = bufferStream.Position;
                    bufferStream.Position = bufferStream.Length;
                    bufferStream.Write(buffer, 0, bytesRead);
                    bufferStream.Position = pos;

                    while (true)
                    {
                        Protocol proto = ProtoHelper.ProtocolSeg(bufferStream, true, false);
                        if (proto == null)
                        {
                            break;
                        }

                        PostProto(proto);
                    }

                    if (bufferStream.Length == bufferStream.Position)
                    {
                        bufferStream.Position = 0;
                        bufferStream.SetLength(0);
                    }
                    else if (bufferStream.Position >= STREAM_LEN_MAX)
                    {
                        int len = (int)(bufferStream.Length - bufferStream.Position);
                        byte[] temp = new byte[len];
                        bufferStream.Read(temp, 0, len);
                        bufferStream.SetLength(0);
                        bufferStream.Write(temp, 0, len);
                        bufferStream.Position = 0;
                    }

                    BeginReceive();
                }
                else
                {
                    InitSocket();
                    PostDisconnectProto();
                }
            }
            catch (Exception ex)
            {
                OutputException(ex);

                InitSocket();
                PostDisconnectProto();
            }
        }

        void HandleSend(IAsyncResult result)
        {
            try
            {
                sock.EndSend(result);
            }
            catch (Exception ex)
            {
                OutputException(ex);

                InitSocket();
                PostDisconnectProto();
            }
        }

        void HandleConnect(IAsyncResult result)
        {
            object[] param = (object[])result.AsyncState;
            Socket sock = (Socket)param[0];
            IPEndPoint endpoint = (IPEndPoint)param[1];

            ServerConnected body = new ServerConnected();
            body.success = false;

            try
            {
                sock.EndConnect(result);

                if (sock.Connected)
                {
                    body.endpoint = (IPEndPoint)sock.RemoteEndPoint;
                    body.success = true;
                    BeginReceive();
                }
            }
            catch (Exception ex)
            {
                OutputException(ex);

                InitSocket();
            }
            finally
            {
                connecting = false;

                Protocol proto = new Protocol(body);
                PostProto(proto);
            }
        }

        void OutputException(Exception ex)
        {
        }
    }
}
