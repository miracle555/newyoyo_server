﻿namespace ProtocolTest
{
    partial class ProtocolTest
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnConnect = new System.Windows.Forms.Button();
            this.BtnSetup = new System.Windows.Forms.Button();
            this.TBPlayerCount = new System.Windows.Forms.TextBox();
            this.LVState = new System.Windows.Forms.ListView();
            this.chAccount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chRole = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LBPlayerCount = new System.Windows.Forms.Label();
            this.BtnRegister = new System.Windows.Forms.Button();
            this.BtnLogout = new System.Windows.Forms.Button();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.TBPassword = new System.Windows.Forms.TextBox();
            this.LBPassword = new System.Windows.Forms.Label();
            this.LBAccount = new System.Windows.Forms.Label();
            this.LBServerIp = new System.Windows.Forms.Label();
            this.TBAccount = new System.Windows.Forms.TextBox();
            this.TBServerIp = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BtnStreamTest = new System.Windows.Forms.Button();
            this.BtnStopTest = new System.Windows.Forms.Button();
            this.TBOutputDetail = new System.Windows.Forms.TextBox();
            this.BtnProtocolTest = new System.Windows.Forms.Button();
            this.buttonAddProto = new System.Windows.Forms.Button();
            this.listViewProto = new System.Windows.Forms.ListView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnConnect);
            this.panel1.Controls.Add(this.BtnSetup);
            this.panel1.Controls.Add(this.TBPlayerCount);
            this.panel1.Controls.Add(this.LVState);
            this.panel1.Controls.Add(this.LBPlayerCount);
            this.panel1.Controls.Add(this.BtnRegister);
            this.panel1.Controls.Add(this.BtnLogout);
            this.panel1.Controls.Add(this.BtnLogin);
            this.panel1.Controls.Add(this.TBPassword);
            this.panel1.Controls.Add(this.LBPassword);
            this.panel1.Controls.Add(this.LBAccount);
            this.panel1.Controls.Add(this.LBServerIp);
            this.panel1.Controls.Add(this.TBAccount);
            this.panel1.Controls.Add(this.TBServerIp);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1326, 300);
            this.panel1.TabIndex = 2;
            // 
            // BtnConnect
            // 
            this.BtnConnect.Location = new System.Drawing.Point(5, 140);
            this.BtnConnect.Name = "BtnConnect";
            this.BtnConnect.Size = new System.Drawing.Size(177, 23);
            this.BtnConnect.TabIndex = 14;
            this.BtnConnect.Text = "连接服务器";
            this.BtnConnect.UseVisualStyleBackColor = true;
            this.BtnConnect.Click += new System.EventHandler(this.BtnConnect_Click);
            // 
            // BtnSetup
            // 
            this.BtnSetup.Location = new System.Drawing.Point(5, 111);
            this.BtnSetup.Name = "BtnSetup";
            this.BtnSetup.Size = new System.Drawing.Size(177, 23);
            this.BtnSetup.TabIndex = 13;
            this.BtnSetup.Text = "设置";
            this.BtnSetup.UseVisualStyleBackColor = true;
            this.BtnSetup.Click += new System.EventHandler(this.BtnSetup_Click);
            // 
            // TBPlayerCount
            // 
            this.TBPlayerCount.Location = new System.Drawing.Point(62, 84);
            this.TBPlayerCount.Name = "TBPlayerCount";
            this.TBPlayerCount.Size = new System.Drawing.Size(120, 21);
            this.TBPlayerCount.TabIndex = 12;
            // 
            // LVState
            // 
            this.LVState.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chAccount,
            this.chRole,
            this.chState});
            this.LVState.Location = new System.Drawing.Point(188, 3);
            this.LVState.Name = "LVState";
            this.LVState.Size = new System.Drawing.Size(1135, 294);
            this.LVState.TabIndex = 11;
            this.LVState.UseCompatibleStateImageBehavior = false;
            this.LVState.View = System.Windows.Forms.View.Details;
            // 
            // chAccount
            // 
            this.chAccount.Text = "帐号";
            this.chAccount.Width = 160;
            // 
            // chRole
            // 
            this.chRole.Text = "角色";
            this.chRole.Width = 640;
            // 
            // chState
            // 
            this.chState.Text = "状态";
            this.chState.Width = 160;
            // 
            // LBPlayerCount
            // 
            this.LBPlayerCount.AutoSize = true;
            this.LBPlayerCount.Location = new System.Drawing.Point(3, 87);
            this.LBPlayerCount.Name = "LBPlayerCount";
            this.LBPlayerCount.Size = new System.Drawing.Size(53, 12);
            this.LBPlayerCount.TabIndex = 9;
            this.LBPlayerCount.Text = "玩家数量";
            // 
            // BtnRegister
            // 
            this.BtnRegister.Location = new System.Drawing.Point(5, 169);
            this.BtnRegister.Name = "BtnRegister";
            this.BtnRegister.Size = new System.Drawing.Size(177, 23);
            this.BtnRegister.TabIndex = 8;
            this.BtnRegister.Text = "注册";
            this.BtnRegister.UseVisualStyleBackColor = true;
            this.BtnRegister.Click += new System.EventHandler(this.BtnRegister_Click);
            // 
            // BtnLogout
            // 
            this.BtnLogout.Location = new System.Drawing.Point(5, 227);
            this.BtnLogout.Name = "BtnLogout";
            this.BtnLogout.Size = new System.Drawing.Size(177, 23);
            this.BtnLogout.TabIndex = 7;
            this.BtnLogout.Text = "断开连接";
            this.BtnLogout.UseVisualStyleBackColor = true;
            this.BtnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
            // 
            // BtnLogin
            // 
            this.BtnLogin.Location = new System.Drawing.Point(5, 198);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(177, 23);
            this.BtnLogin.TabIndex = 6;
            this.BtnLogin.Text = "登录";
            this.BtnLogin.UseVisualStyleBackColor = true;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // TBPassword
            // 
            this.TBPassword.Location = new System.Drawing.Point(62, 57);
            this.TBPassword.Name = "TBPassword";
            this.TBPassword.Size = new System.Drawing.Size(120, 21);
            this.TBPassword.TabIndex = 5;
            this.TBPassword.UseSystemPasswordChar = true;
            // 
            // LBPassword
            // 
            this.LBPassword.AutoSize = true;
            this.LBPassword.Location = new System.Drawing.Point(3, 60);
            this.LBPassword.Name = "LBPassword";
            this.LBPassword.Size = new System.Drawing.Size(29, 12);
            this.LBPassword.TabIndex = 4;
            this.LBPassword.Text = "密码";
            // 
            // LBAccount
            // 
            this.LBAccount.AutoSize = true;
            this.LBAccount.Location = new System.Drawing.Point(3, 33);
            this.LBAccount.Name = "LBAccount";
            this.LBAccount.Size = new System.Drawing.Size(53, 12);
            this.LBAccount.TabIndex = 3;
            this.LBAccount.Text = "帐号前缀";
            // 
            // LBServerIp
            // 
            this.LBServerIp.AutoSize = true;
            this.LBServerIp.Location = new System.Drawing.Point(3, 6);
            this.LBServerIp.Name = "LBServerIp";
            this.LBServerIp.Size = new System.Drawing.Size(41, 12);
            this.LBServerIp.TabIndex = 2;
            this.LBServerIp.Text = "服务器";
            // 
            // TBAccount
            // 
            this.TBAccount.Location = new System.Drawing.Point(62, 30);
            this.TBAccount.Name = "TBAccount";
            this.TBAccount.Size = new System.Drawing.Size(120, 21);
            this.TBAccount.TabIndex = 1;
            // 
            // TBServerIp
            // 
            this.TBServerIp.Location = new System.Drawing.Point(62, 3);
            this.TBServerIp.Name = "TBServerIp";
            this.TBServerIp.Size = new System.Drawing.Size(120, 21);
            this.TBServerIp.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.listViewProto);
            this.panel2.Controls.Add(this.buttonAddProto);
            this.panel2.Controls.Add(this.BtnStreamTest);
            this.panel2.Controls.Add(this.BtnStopTest);
            this.panel2.Controls.Add(this.TBOutputDetail);
            this.panel2.Controls.Add(this.BtnProtocolTest);
            this.panel2.Location = new System.Drawing.Point(12, 318);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1326, 400);
            this.panel2.TabIndex = 3;
            // 
            // BtnStreamTest
            // 
            this.BtnStreamTest.Location = new System.Drawing.Point(5, 345);
            this.BtnStreamTest.Name = "BtnStreamTest";
            this.BtnStreamTest.Size = new System.Drawing.Size(177, 23);
            this.BtnStreamTest.TabIndex = 5;
            this.BtnStreamTest.Text = "随机字节流测试";
            this.BtnStreamTest.UseVisualStyleBackColor = true;
            this.BtnStreamTest.Click += new System.EventHandler(this.BtnStreamTest_Click);
            // 
            // BtnStopTest
            // 
            this.BtnStopTest.Location = new System.Drawing.Point(5, 374);
            this.BtnStopTest.Name = "BtnStopTest";
            this.BtnStopTest.Size = new System.Drawing.Size(177, 23);
            this.BtnStopTest.TabIndex = 4;
            this.BtnStopTest.Text = "停止测试";
            this.BtnStopTest.UseVisualStyleBackColor = true;
            this.BtnStopTest.Click += new System.EventHandler(this.BtnStopTest_Click);
            // 
            // TBOutputDetail
            // 
            this.TBOutputDetail.Location = new System.Drawing.Point(188, 5);
            this.TBOutputDetail.Multiline = true;
            this.TBOutputDetail.Name = "TBOutputDetail";
            this.TBOutputDetail.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TBOutputDetail.Size = new System.Drawing.Size(1135, 392);
            this.TBOutputDetail.TabIndex = 3;
            // 
            // BtnProtocolTest
            // 
            this.BtnProtocolTest.Location = new System.Drawing.Point(5, 316);
            this.BtnProtocolTest.Name = "BtnProtocolTest";
            this.BtnProtocolTest.Size = new System.Drawing.Size(177, 23);
            this.BtnProtocolTest.TabIndex = 1;
            this.BtnProtocolTest.Text = "开始协议测试";
            this.BtnProtocolTest.UseVisualStyleBackColor = true;
            this.BtnProtocolTest.Click += new System.EventHandler(this.BtnProtocolTest_Click);
            // 
            // buttonAddProto
            // 
            this.buttonAddProto.Location = new System.Drawing.Point(5, 287);
            this.buttonAddProto.Name = "buttonAddProto";
            this.buttonAddProto.Size = new System.Drawing.Size(177, 23);
            this.buttonAddProto.TabIndex = 7;
            this.buttonAddProto.Text = "增加协议";
            this.buttonAddProto.UseVisualStyleBackColor = true;
            this.buttonAddProto.Click += new System.EventHandler(this.buttonAddProto_Click);
            // 
            // listViewProto
            // 
            this.listViewProto.Location = new System.Drawing.Point(5, 5);
            this.listViewProto.Name = "listViewProto";
            this.listViewProto.Size = new System.Drawing.Size(177, 276);
            this.listViewProto.TabIndex = 51;
            this.listViewProto.UseCompatibleStateImageBehavior = false;
            this.listViewProto.View = System.Windows.Forms.View.Details;
            // 
            // ProtocolTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ProtocolTest";
            this.Text = "ProtocolTest";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnLogout;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.TextBox TBPassword;
        private System.Windows.Forms.Label LBPassword;
        private System.Windows.Forms.Label LBAccount;
        private System.Windows.Forms.Label LBServerIp;
        private System.Windows.Forms.TextBox TBAccount;
        private System.Windows.Forms.TextBox TBServerIp;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button BtnProtocolTest;
        private System.Windows.Forms.TextBox TBOutputDetail;
        private System.Windows.Forms.Button BtnStopTest;
        private System.Windows.Forms.Button BtnStreamTest;
        private System.Windows.Forms.Button BtnRegister;
        private System.Windows.Forms.Label LBPlayerCount;
        private System.Windows.Forms.ListView LVState;
        private System.Windows.Forms.ColumnHeader chAccount;
        private System.Windows.Forms.ColumnHeader chRole;
        private System.Windows.Forms.ColumnHeader chState;
        private System.Windows.Forms.TextBox TBPlayerCount;
        private System.Windows.Forms.Button BtnSetup;
        private System.Windows.Forms.Button BtnConnect;
        private System.Windows.Forms.Button buttonAddProto;
        private System.Windows.Forms.ListView listViewProto;
    }
}

