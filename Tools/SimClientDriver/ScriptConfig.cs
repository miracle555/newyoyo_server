﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/10 10:13:04
* FileName:     ScriptConfig
* Purpose:      
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Script;

namespace SimClientDriver
{
    public class ScriptConfig
    {
        public class ScriptData
        {
            public string script;
            public string param;
        }

        public class Pipeline
        {
            public List<Context> ctxs = new List<Context>();
            public List<ScriptData> scripts = new List<ScriptData>();
        }

        public List<Pipeline> Pipelines = new List<Pipeline>();

        public void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            XmlNodeList entryNodes = rootElem.GetElementsByTagName("entry");
            foreach (XmlNode entryNode in entryNodes)
            {
                string ip = ((XmlElement)entryNode).GetAttribute("ip");
                string port = ((XmlElement)entryNode).GetAttribute("port");

                Pipeline pipeline = new Pipeline();
                
                XmlNodeList accountNodes = ((XmlElement)entryNode).GetElementsByTagName("account");
                foreach (XmlNode accountNode in accountNodes)
                {
                    string account = ((XmlElement)accountNode).GetAttribute("account");
                    string password = ((XmlElement)accountNode).GetAttribute("password");
                    string role = ((XmlElement)accountNode).GetAttribute("role");

                    Context ctx = new Context(Console.Out, Console.Out);
                    ctx.SaveData("ip", ip);
                    ctx.SaveData("port", int.Parse(port));
                    ctx.SaveData("account", account);
                    ctx.SaveData("password", password);
                    ctx.SaveData("role", role);
                    ctx.Init();
                    pipeline.ctxs.Add(ctx);
                }

                XmlNodeList scriptNodes = ((XmlElement)entryNode).GetElementsByTagName("script");
                foreach (XmlNode scriptNode in scriptNodes)
                {
                    ScriptData data = new ScriptData();
                    data.script = scriptNode.InnerText;
                    if (((XmlElement)scriptNode).HasAttribute("param"))
                    {
                        data.param = ((XmlElement)scriptNode).GetAttribute("param");
                    }
                    else
                    {
                        data.param = null;
                    }
                    pipeline.scripts.Add(data);
                }

                Pipelines.Add(pipeline);
            }
        }
    }
}
