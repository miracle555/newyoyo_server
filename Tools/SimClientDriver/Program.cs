﻿/* ============================================================
* Author:       xieqin
* Time:         2014/11/28 11:18:27
* FileName:     Program
* Purpose:      模拟客户端驱动
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SimClient;
using System.IO;
using Config;
using Script;

namespace SimClientDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("press Enter to start......");
            Console.ReadLine();

            while (true)
            {
                string errorMsg;
                ScriptDriver driver = new ScriptDriver();
                driver.LoadScript(args[0]);
                if (!driver.CompileScript(out errorMsg))
                {
                    Console.WriteLine("Compile failed......error = {0}", errorMsg);
                }
                else
                {
                    Console.WriteLine("Compile succeed\n");

                    ScriptConfig config = new ScriptConfig();
                    config.Load(args[1]);

                    int index = 0;
                    Thread[] threads = new Thread[config.Pipelines.Count];
                    ManualResetEvent[] events = new ManualResetEvent[config.Pipelines.Count];
                    foreach (ScriptConfig.Pipeline pipeline in config.Pipelines)
                    {
                        ScriptConfig.ScriptData[] datas = new ScriptConfig.ScriptData[pipeline.scripts.Count];
                        for (int i = 0; i < pipeline.scripts.Count; i++)
                        {
                            datas[i] = pipeline.scripts[i];
                        }
                        foreach (Context ctx in pipeline.ctxs)
                        {
                            driver.RegisterScripts(ctx);
                        }
                        ManualResetEvent mrEv = new ManualResetEvent(false);
                        events[index] = mrEv;
                        string[] scripts = new string[pipeline.scripts.Count];
                        string[] param = new string[pipeline.scripts.Count];
                        for (int i = 0; i < pipeline.scripts.Count; i++)
                        {
                            scripts[i] = pipeline.scripts[i].script;
                            param[i] = pipeline.scripts[i].param;
                        }
                        foreach (Context ctx in pipeline.ctxs)
                        {
                            driver.InitScripts(ctx);
                        }
                        Context[] ctxs = new Context[pipeline.ctxs.Count];
                        for (int i = 0; i < pipeline.ctxs.Count; i++)
                        {
                            ctxs[i] = pipeline.ctxs[i];
                        }
                        threads[index] = new Thread(new ThreadStart(() => driver.RunScript(false, scripts, ctxs, param, mrEv)));
                        index++;
                    }

                    foreach (Thread thread in threads)
                    {
                        thread.Start();

                        Thread.Sleep(100);
                    }

                    //2015.3.2金刚修改
                    if (events.Length <= 64)
                    {
                        WaitHandle.WaitAll(events);
                    }
                    else
                    {
                        int eventsArrayCount = events.Length / 64 + 1;
                        for (int i = 0; i < eventsArrayCount; i++)
                        {
                            int count = 0;
                            if (eventsArrayCount == i + 1)
                            {
                                count = events.Length % 64;
                            }
                            else
                            {
                                count = 64;
                            }
                            ManualResetEvent[] newEvents = new ManualResetEvent[count];
                            for (int j = 0; j < count; j++)
                            {
                                newEvents[j] = events[i * 64 + j];
                            }
                            WaitHandle.WaitAll(newEvents);
                        }
                    }

                    //WaitHandle.WaitAll(events);
                }

                Thread.Sleep(1000);

                Console.WriteLine("\r\nStop!");
                Console.WriteLine("press Enter to restart......");
                Console.ReadLine();
            }
        }
    }
}
