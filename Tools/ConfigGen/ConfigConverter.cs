﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace ConfigGen
{
    //Config生成
    public class ConfigConverter
    {
        public static void Convert(string configFile)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(configFile);
                XmlElement rootElem = doc.DocumentElement;

                Convert(rootElem);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred: {0}", ex.Message);
            }
        }

        public static void Convert(XmlElement documentElement)
        {
            XmlNodeList ConfigNodes = documentElement.GetElementsByTagName("Config");
            if (ConfigNodes.Count == 0)
            {
                throw new ApplicationException("No Config Element!");
            }

            foreach (XmlNode ConfigNode in ConfigNodes)
            {
                GenerateConfigFile(ConfigNode);
            }
        }

        static void GenerateConfigFile(XmlNode ConfigNode)
        {
            string configName = ((XmlElement)ConfigNode).GetAttribute("name");
            string templateName = ((XmlElement)ConfigNode).GetAttribute("template");

            try
            {
                configName += ".xml";
                templateName += "_tmpl.xml";

                File.Copy(templateName, configName, true);

                XmlDocument cfgDoc = new XmlDocument();
                cfgDoc.Load(configName);
                XmlElement rootElem = cfgDoc.DocumentElement;

                XmlNodeList elementNodes = ConfigNode.ChildNodes;
                foreach (XmlNode elementNode in elementNodes)
                {
                    HandleElementNode(elementNode, rootElem);
                }

                cfgDoc.Save(configName);

                Console.WriteLine("......Generate {0}", configName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred: {0}", ex.Message);
                Console.WriteLine("Failed to generate {0}", configName);
            }
        }

        private static void HandleElementNode(XmlNode elementNode, XmlNode tmplNode)
        {
            bool found = false;
            XmlNodeList tmplNodes = tmplNode.ChildNodes;
            foreach (XmlNode node in tmplNodes)
            {
                if (node.Name == elementNode.Name)
                {
                    found = true;
                    if (((XmlElement)elementNode).HasAttribute("ref"))
                    {
                        tmplNode.RemoveChild(node);
                        found = false;
                    }
                    else if (!elementNode.HasChildNodes || (elementNode.ChildNodes.Count == 1 && elementNode.FirstChild.NodeType == XmlNodeType.Text))
                    {
                        if (elementNode.NodeType == XmlNodeType.Element
                            && ((XmlElement)elementNode).HasAttribute("index")
                            && ((XmlElement)elementNode).GetAttribute("index") != ((XmlElement)node).GetAttribute("index"))
                        {
                            found = false;
                        }
                        else
                        {
                            node.InnerText = elementNode.InnerText;
                        }
                    }
                    else
                    {
                        XmlNodeList childNodes = elementNode.ChildNodes;
                        foreach (XmlNode childNode in childNodes)
                        {
                            HandleElementNode(childNode, node);
                        }
                    }
                }
            }

            if (!found)
            {
                tmplNode.AppendChild(tmplNode.OwnerDocument.ImportNode(elementNode, true));
            }
        }
    }
}
