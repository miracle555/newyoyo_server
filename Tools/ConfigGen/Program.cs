﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigGen
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Argument error!");
            }
            else
            {
                GenerateConfig(args[0]);
            }

            Console.ReadLine();
        }

        static void GenerateConfig(string configFile)
        {
            Console.WriteLine("Start generate...");

            ConfigConverter.Convert(configFile);

            Console.WriteLine("Generate complete!");
        }
    }
}
