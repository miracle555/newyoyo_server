﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/3 11:18:27
* FileName:     ServerProxy
* Purpose:      脚本使用的服务器通信接口
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using Protocols;
using System.Runtime.InteropServices;
using System.Threading;

namespace Script
{
    public delegate void HandleMessage(Protocol proto, object obj);

    /// <summary>
    /// 脚本服务器通信
    /// </summary>
    public class ServerProxy
    {
        class HandlerState
        {
            public HandleMessage handler;
            public object obj;

            public HandlerState(HandleMessage handler, object obj)
            {
                this.handler = handler;
                this.obj = obj;
            }
        }

        TextWriter output;
        TextWriter error;

        IPEndPoint endpoint;
        Socket sock;
        NetworkStream inputStream;
        MemoryStream bufferStream = new MemoryStream();
        byte[] buffer = new byte[BUFFER_LEN];
        const int BUFFER_LEN = 1024 * 30;
        const int STREAM_LEN_MAX = 65536;
        List<HandlerState> handlerList = new List<HandlerState>();
        object theLock = new object();

        internal ServerProxy(string ip, int port, TextWriter output, TextWriter error)
        {
            this.output = output;
            this.error = output;

            string[] ipRes = ip.Split('.');
            byte[] ipAddress = { byte.Parse(ipRes[0]), byte.Parse(ipRes[1]), byte.Parse(ipRes[2]), byte.Parse(ipRes[3]) };
            IPAddress ipAddr = new IPAddress(ipAddress);
            endpoint = new IPEndPoint(ipAddr, port);
        }

        /// <summary>
        /// 连接服务器
        /// </summary>
        internal void Connect()
        {
            output.WriteLine("[{0}][ServerProxy.Connect]endpoint = {1}", Thread.CurrentThread.ManagedThreadId, endpoint.ToString());

            //try
            //{
                lock (theLock)
                {
                    sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sock.Connect(endpoint);
                    inputStream = new NetworkStream(sock);
                    uint dummy = 0;
                    byte[] inOptionValues = new byte[Marshal.SizeOf(dummy) * 3];
                    BitConverter.GetBytes((uint)0).CopyTo(inOptionValues, 0);
                    BitConverter.GetBytes((uint)600000).CopyTo(inOptionValues, Marshal.SizeOf(dummy));
                    BitConverter.GetBytes((uint)600000).CopyTo(inOptionValues, Marshal.SizeOf(dummy) * 2);
                    sock.IOControl(IOControlCode.KeepAliveValues, inOptionValues, null);
                    inputStream.BeginRead(buffer, 0, BUFFER_LEN, HandleReceive, null);
                }
            //}
            //catch (Exception ex)
            //{
            //    error.WriteLine("[{0}][ServerProxy.Connect]Exception: {1}", Thread.CurrentThread.ManagedThreadId, ex.Message);
            //}
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        internal void Disconnect()
        {
            if (sock.Connected)
            {
                output.WriteLine("[{0}][ServerProxy.Disconnect]endpoint = {1}", Thread.CurrentThread.ManagedThreadId, endpoint.ToString());

                try
                {
                    sock.Close();
                    sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    sock.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
                }
                catch (Exception ex)
                {
                    error.WriteLine("[{0}][ServerProxy.Disconnect]Exception: {1}", Thread.CurrentThread.ManagedThreadId, ex.Message);
                }
            }
        }

        /// <summary>
        /// 发送协议体
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal int SendMessage(ProtoBody msg, UInt64 roleID = 0)
        {
            if (!sock.Connected)
                return 1;

            Protocol proto = new Protocol(msg);
            proto.SetRoleID(roleID);

            return SendMessage(proto);
        }

        internal int SendMessage(Protocol proto)
        {
            if (!sock.Connected)
                return 1;

            output.WriteLine(string.Format("[{0}]{1} <--- {2}", Thread.CurrentThread.ManagedThreadId, endpoint.ToString(), proto.GetID()));

            MemoryStream stream = new MemoryStream();
            proto.Serialize(stream, true);

            return Send(stream);
        }

        /// <summary>
        /// 注册消息处理器
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="obj"></param>
        internal void RegisterMessageHandler(HandleMessage handler, object obj)
        {
            lock (theLock)
            {
                if (handlerList.FindIndex(x => (x.handler == handler)) == -1)
                {
                    HandlerState state = new HandlerState(handler, obj);
                    handlerList.Add(state);
                }
            }
        }

        int Send(Stream stream)
        {
            try
            {
                if (sock.Connected)
                {
                    byte[] buffer = new byte[stream.Length];
                    stream.Position = 0;
                    stream.Read(buffer, 0, (int)stream.Length);
                    sock.Send(buffer);
                }
            }
            catch (Exception ex)
            {
                error.WriteLine("[{0}][ServerProxy.Send]Exception: {1}", Thread.CurrentThread.ManagedThreadId, ex.Message);
                Disconnect();
            }

            return 0;
        }

        void HandleReceive(IAsyncResult result)
        {
            if (!sock.Connected)
            {
                return;
            }

            try
            {
                lock (theLock)
                {
                    int bytesRead = inputStream.EndRead(result);
                    if (bytesRead > 0)
                    {
                        long pos = bufferStream.Position;
                        bufferStream.Position = bufferStream.Length;
                        bufferStream.Write(buffer, 0, bytesRead);
                        bufferStream.Position = pos;

                        while (true)
                        {
                            Protocol pkg = ProtoHelper.ProtocolSeg(bufferStream, true, false);
                            if (pkg == null)
                            {
                                break;
                            }

                            output.WriteLine(string.Format("[{0}]{1} ---> {2}", Thread.CurrentThread.ManagedThreadId, endpoint.ToString(), pkg.GetID()));

                            foreach (HandlerState handler in handlerList)
                            {
                                try
                                {
                                    handler.handler(pkg, handler.obj);
                                }
                                catch (Exception ex)
                                {
                                    output.WriteLine(string.Format("[{0}]MessageHandler Exception: {1}",
                                        Thread.CurrentThread.ManagedThreadId, ex.Message));
                                }
                            }
                        }

                        if (bufferStream.Length == bufferStream.Position)
                        {
                            bufferStream.Position = 0;
                            bufferStream.SetLength(0);
                        }
                        else if (bufferStream.Position >= STREAM_LEN_MAX)
                        {
                            int len = (int)(bufferStream.Length - bufferStream.Position);
                            byte[] temp = new byte[len];
                            bufferStream.Read(temp, 0, len);
                            bufferStream.SetLength(0);
                            bufferStream.Write(temp, 0, len);
                            bufferStream.Position = 0;
                        }

                        inputStream.BeginRead(buffer, 0, BUFFER_LEN, HandleReceive, null);
                    }
                    else
                    {
                        Disconnect();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[{0}][ServerProxy.HandleReceive]Exception : {1}", Thread.CurrentThread.ManagedThreadId, ex.Message);
                Disconnect();
            }
        }
    }
}
