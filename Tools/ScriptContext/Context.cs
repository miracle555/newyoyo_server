﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/3 11:18:27
* FileName:     Context
* Purpose:      脚本使用的上下文数据
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;
using Protocols;
using System.Threading;

namespace Script
{
    public class ScriptData
    {
        public int Index;
        public string Script;
        public string Param;
    }

    public class Context
    {
        public ServerProxy Server
        {
            get;
            private set;
        }

        public ScriptProxy Script
        {
            get;
            private set;
        }

        public TextWriter Output
        {
            get;
            private set;
        }

        public TextWriter Error
        {
            get;
            private set;
        }

        object eventLock = new object();
        object dataLock = new object();
        object stateLock = new object();

        Dictionary<string, object> userData = new Dictionary<string, object>();
        Dictionary<string, int> events = new Dictionary<string, int>();
        LinkedList<ScriptData> scripts = new LinkedList<ScriptData>();
        int runningScript;
        List<int> states = new List<int>();
        TimeSpan spanMax = new TimeSpan();
        TimeSpan spans = new TimeSpan();
        int spanCount = 0;

        public Context(TextWriter output, TextWriter error)
        {
            Output = output;
            Error = error;
        }

        public void Init()
        {
            object ip = new object();
            object port = new object();
            LoadData("ip", out ip);
            LoadData("port", out port);
            Server = new ServerProxy((string)ip, (int)port, Output, Error);
            Script = new ScriptProxy();
        }

        public void Reset()
        {
            scripts.Clear();
            states.Clear();
            spanMax = new TimeSpan();
            spans = new TimeSpan();
            runningScript = 0;
            spanCount = 0;
        }

        /// <summary>
        /// 注册脚本
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="method"></param>
        public void RegisterScript(string name, Type type, MethodInfo method)
        {
            Script.RegisterScript(name, type, method);
        }

        public void OnScriptComplete(bool success)
        {
            lock (stateLock)
            {
                states[runningScript] = success ? 1 : -1;
            }
        }

        public void OnResponse(TimeSpan span)
        {
            spanCount++;
            spans += span;
            if (span > spanMax)
            {
                spanMax = span;
            }
        }

        public TimeSpan GetSpanMax()
        {
            lock (stateLock)
            {
                return spanMax;
            }
        }

        public int GetSpans(out TimeSpan spans)
        {
            lock (stateLock)
            {
                spans = this.spans;
                return spanCount;
            }
        }

        public void SetRunningScript(int index)
        {
            lock (stateLock)
            {
                runningScript = index;
            }
        }

        public int GetRunningScript()
        {
            lock (stateLock)
            {
                return runningScript;
            }
        }

        public bool GetScriptState(int index, out bool success)
        {
            lock (stateLock)
            {
                if (states[index] == 1)
                {
                    success = true;
                    return true;
                }
                else if (states[index] == -1)
                {
                    success = false;
                    return true;
                }
                success = false;
                return false;
            }
        }

        public void AddState(int state)
        {
            lock (stateLock)
            {
                states.Add(state);
            }
        }

        /// <summary>
        /// 连接服务器
        /// </summary>
        public void Connect()
        {
            Server.Connect();
        }

        /// <summary>
        /// 与服务器断开连接
        /// </summary>
        public void Disconnect()
        {
            Server.Disconnect();
        }

        /// <summary>
        /// 发送协议
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public int SendMessage(ProtoBody msg, UInt64 roleID = 0)
        {
            return Server.SendMessage(msg, roleID);
        }

        public int SendMessage(Protocol proto)
        {
            return Server.SendMessage(proto);
        }

        /// <summary>
        /// 注册消息处理器
        /// </summary>
        /// <param name="handler"></param>
        public void RegisterMessageHandler(HandleMessage handler)
        {
            Server.RegisterMessageHandler(handler, this);
        }

        /// <summary>
        /// 保存自定义数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        public void SaveData(string key, object obj)
        {
            lock (dataLock)
            {
                userData[key] = obj;
            }
        }

        /// <summary>
        /// 获取自定义数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool LoadData(string key, out object obj)
        {
            lock (dataLock)
            {
                if (userData.ContainsKey(key))
                {
                    obj = userData[key];
                    return true;
                }
            }
            obj = null;
            return false;
        }

        /// <summary>
        /// 在事件上阻塞
        /// </summary>
        /// <param name="ev">事件</param>
        /// <param name="timeout">超时</param>
        public void Wait(string ev, int timeout = -1)
        {
            lock (eventLock)
            {
                if (!events.ContainsKey(ev))
                {
                    events[ev] = 1;
                }
            }
        }

        /// <summary>
        /// 发出事件
        /// </summary>
        /// <param name="ev">事件</param>
        public void Signal(string ev)
        {
            lock (eventLock)
            {
                if (events.ContainsKey(ev))
                {
                    events.Remove(ev);
                }
                else
                {
                    throw new ArgumentException(string.Format("[Context.Signal]no event {0}", ev));
                }
            }
        }

        public bool Enable
        {
            get
            {
                lock (eventLock)
                {
                    if (events.Count > 0)
                    {
                        return false;
                    }
                    return true;
                }
            }
        }

        public LinkedList<ScriptData> Scripts
        {
            get
            {
                return scripts;
            }
        }
    }
}
