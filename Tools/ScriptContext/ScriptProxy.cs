﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/11 10:47:07
* FileName:     ScriptProxy
* Purpose:      
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;

namespace Script
{
    public class ScriptProxy
    {
        class ScriptInfo
        {
            public Type type;
            public MethodInfo method;

            public ScriptInfo(Type type, MethodInfo method)
            {
                this.type = type;
                this.method = method;
            }
        }

        Dictionary<string, ScriptInfo> scripts = new Dictionary<string, ScriptInfo>();

        internal void RegisterScript(string name, Type type, MethodInfo method)
        {
            ScriptInfo script = new ScriptInfo(type, method);
            scripts[name] = script;
        }

        internal void Run(string script, object obj)
        {
            if (scripts.ContainsKey(script))
            {
                MethodInfo method = scripts[script].method;
                object[] objects = new object[1];
                objects[0] = obj;
                method.Invoke(null, objects);
            }
        }
    }
}
