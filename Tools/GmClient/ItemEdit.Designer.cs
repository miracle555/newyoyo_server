﻿namespace GmClient
{
    partial class ItemEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnItemCancel = new System.Windows.Forms.Button();
            this.btnItemOk = new System.Windows.Forms.Button();
            this.btnMinusItem = new System.Windows.Forms.Button();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.tbItemCount = new System.Windows.Forms.TextBox();
            this.tbItemName = new System.Windows.Forms.TextBox();
            this.tbItemId = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "道具";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "数量";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnItemCancel);
            this.panel1.Controls.Add(this.btnItemOk);
            this.panel1.Controls.Add(this.btnMinusItem);
            this.panel1.Controls.Add(this.btnAddItem);
            this.panel1.Controls.Add(this.tbItemCount);
            this.panel1.Controls.Add(this.tbItemName);
            this.panel1.Controls.Add(this.tbItemId);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(316, 134);
            this.panel1.TabIndex = 2;
            // 
            // btnItemCancel
            // 
            this.btnItemCancel.Location = new System.Drawing.Point(155, 81);
            this.btnItemCancel.Name = "btnItemCancel";
            this.btnItemCancel.Size = new System.Drawing.Size(75, 23);
            this.btnItemCancel.TabIndex = 8;
            this.btnItemCancel.Text = "取消";
            this.btnItemCancel.UseVisualStyleBackColor = true;
            // 
            // btnItemOk
            // 
            this.btnItemOk.Location = new System.Drawing.Point(74, 81);
            this.btnItemOk.Name = "btnItemOk";
            this.btnItemOk.Size = new System.Drawing.Size(75, 23);
            this.btnItemOk.TabIndex = 7;
            this.btnItemOk.Text = "确定";
            this.btnItemOk.UseVisualStyleBackColor = true;
            // 
            // btnMinusItem
            // 
            this.btnMinusItem.Location = new System.Drawing.Point(67, 52);
            this.btnMinusItem.Name = "btnMinusItem";
            this.btnMinusItem.Size = new System.Drawing.Size(24, 23);
            this.btnMinusItem.TabIndex = 6;
            this.btnMinusItem.Text = "-";
            this.btnMinusItem.UseVisualStyleBackColor = true;
            this.btnMinusItem.Click += new System.EventHandler(this.btnMinusItem_Click);
            // 
            // btnAddItem
            // 
            this.btnAddItem.Location = new System.Drawing.Point(249, 52);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(24, 23);
            this.btnAddItem.TabIndex = 5;
            this.btnAddItem.Text = "+";
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // tbItemCount
            // 
            this.tbItemCount.Location = new System.Drawing.Point(97, 54);
            this.tbItemCount.Name = "tbItemCount";
            this.tbItemCount.Size = new System.Drawing.Size(146, 21);
            this.tbItemCount.TabIndex = 4;
            // 
            // tbItemName
            // 
            this.tbItemName.Enabled = false;
            this.tbItemName.Location = new System.Drawing.Point(173, 27);
            this.tbItemName.Name = "tbItemName";
            this.tbItemName.Size = new System.Drawing.Size(100, 21);
            this.tbItemName.TabIndex = 3;
            // 
            // tbItemId
            // 
            this.tbItemId.Enabled = false;
            this.tbItemId.Location = new System.Drawing.Point(67, 27);
            this.tbItemId.Name = "tbItemId";
            this.tbItemId.Size = new System.Drawing.Size(100, 21);
            this.tbItemId.TabIndex = 2;
            // 
            // ItemEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 158);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ItemEdit";
            this.Text = "ItemEdit";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnMinusItem;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.TextBox tbItemCount;
        private System.Windows.Forms.TextBox tbItemName;
        private System.Windows.Forms.TextBox tbItemId;
        private System.Windows.Forms.Button btnItemCancel;
        private System.Windows.Forms.Button btnItemOk;
    }
}