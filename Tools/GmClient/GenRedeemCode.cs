﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GmClient
{
    public partial class GenRedeemCode : Form
    {
        public GenRedeemCode()
        {
            InitializeComponent();

            DateTime now = DateTime.Now;
            textBoxBegin.Text = now.ToString();
            textBoxEnd.Text = now.ToString();

            buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public int Count
        {
            get
            {
                int count = 0;
                int.TryParse(textBoxCount.Text, out count);
                return count;
            }
        }

        public int Type
        {
            get
            {
                int type = 0;
                int.TryParse(textBoxType.Text, out type);
                return type;
            }
        }

        public int Bonus
        {
            get
            {
                int bonus = 0;
                int.TryParse(textBoxBonus.Text, out bonus);
                return bonus;
            }
        }

        public DateTime TimeBegin
        {
            get
            {
                DateTime begin = DateTime.MinValue;
                if (!string.IsNullOrEmpty(textBoxBegin.Text))
                {
                    DateTime.TryParse(textBoxBegin.Text, out begin);
                }
                return begin;
            }
        }

        public DateTime TimeEnd
        {
            get
            {
                DateTime end = DateTime.MaxValue;
                if (!string.IsNullOrEmpty(textBoxEnd.Text))
                {
                    DateTime.TryParse(textBoxEnd.Text, out end);
                }
                return end;
            }
        }
    }
}
