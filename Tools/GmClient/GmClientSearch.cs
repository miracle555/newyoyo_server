﻿using Protocols;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protocols.Error;
using System.Net;

namespace GmClient
{
    partial class GmClientSearch : Form
    {
        Timer timer = new Timer();

        public Session _Session;


        public string RoleName
        {
            get;
            set;
        }

        public UInt64 RoleId
        {
            get;
            set;
        }

        public GmClientSearch(UInt64 roleId, string name, Session session)
        {
            InitializeComponent();

            RoleId = roleId;
            RoleName = name;
            _Session = session;

            InitData();
        }

        private void InitData()
        {
            InitForm();

            GmGetTaskNameReq req = new GmGetTaskNameReq();
            SendMessage(req);

            GmGetAchieveNameReq achiReq = new GmGetAchieveNameReq();
            SendMessage(achiReq);

            GmGetMakeDataReq makeReq = new GmGetMakeDataReq();
            SendMessage(makeReq);
        }

        private void InitForm()
        {
            txtBoxEndTime.Text = DateTime.Now.ToString();
            txtBoxRoleId.Text = RoleId.ToString();
        }

        private void GmClientSearch_Load(object sender, EventArgs e)
        {
            lvTokenLog.FullRowSelect = true;
            lvVipGiftDetail.FullRowSelect = true;
            lvRealTime.FullRowSelect = true;
        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="body"></param>
        private void SendMessage(ProtoBody body)
        {
            Protocol proto = new Protocol(body);
            _Session.SendMessage(proto);
        }

        /// <summary>
        /// 解析金币产生途径
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetGoldProductWay(int index)
        {
            string goldProductWay = string.Empty;

            switch (index)
            {
                case 1:
                    goldProductWay = "剧情过关奖励";
                    break;
                case 2:
                    goldProductWay = "PK过关奖励";
                    break;
                case 3:
                    goldProductWay = "PK排名奖励";
                    break;
                case 4:
                    goldProductWay = "签到奖励";
                    break;
                case 5:
                    goldProductWay = "钻石购买获得";
                    break;
                case 6:
                    goldProductWay = "成就奖励";
                    break;
                case 7:
                    goldProductWay = "累计消费奖励";
                    break;
                case 8:
                    goldProductWay = "累计充值奖励";
                    break;
                case 9:
                    goldProductWay = "PK称号奖励";
                    break;
                case 10:
                    goldProductWay = "首冲礼包获得";
                    break;
                case 11:
                    goldProductWay = "等级礼包获得";
                    break;
                case 12:
                    goldProductWay = "过关有礼获得";
                    break;
                case 13:
                    goldProductWay = "VIP等级礼包获得";
                    break;
                case 14:
                    goldProductWay = "新手引导奖励";
                    break;
                case 15:
                    goldProductWay = "每日任务奖励";
                    break;
                case 16:
                    goldProductWay = "协会关卡奖励";
                    break;
                case 17:
                    goldProductWay = "协会章节奖励";
                    break;
                case 18:
                    goldProductWay = "协会任务奖励";
                    break;
                case 19:
                    goldProductWay = "协会每日排行榜奖励";
                    break;
                case 20:
                    goldProductWay = "协会每日排行榜前三名奖励";
                    break;
                case 21:
                    goldProductWay = "协会雕像升级奖励";
                    break;
                case 22:
                    goldProductWay = "协会商会升级奖励";
                    break;
                case 23:
                    goldProductWay = "协会活跃排行奖励";
                    break;
                case 24:
                    goldProductWay = "套装集齐奖励";
                    break;
                case 25:
                    goldProductWay = "搭配二选一活动奖励";
                    break;
                case 26:
                    goldProductWay = "邮件赠送";
                    break;
                default:
                    break;
            }

            return goldProductWay;
        }

        /// <summary>
        /// 解析钻石产生途径
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetDiamondProductWay(int index)
        {
            string diamondProductWay = string.Empty;

            switch (index)
            {
                case 1:
                    diamondProductWay = "PK过关奖励";
                    break;
                case 2:
                    diamondProductWay = "PK排名奖励";
                    break;
                case 3:
                    diamondProductWay = "PK称号奖励";
                    break;
                case 4:
                    diamondProductWay = "签到奖励";
                    break;
                case 5:
                    diamondProductWay = "每日任务奖励";
                    break;
                case 6:
                    diamondProductWay = "成就奖励";
                    break;
                case 7:
                    diamondProductWay = "累计消费奖励";
                    break;
                case 8:
                    diamondProductWay = "累计充值奖励";
                    break;
                case 9:
                    diamondProductWay = "首冲双倍获得";
                    break;
                case 10:
                    diamondProductWay = "首冲礼包获得";
                    break;
                case 11:
                    diamondProductWay = "等级礼包获得";
                    break;
                case 12:
                    diamondProductWay = "过关有礼获得";
                    break;
                case 13:
                    diamondProductWay = "月卡奖励";
                    break;
                case 14:
                    diamondProductWay = "殴打数值君获得";
                    break;
                case 15:
                    diamondProductWay = "红包活动获得";
                    break;
                case 16:
                    diamondProductWay = "VIP等级礼包获得";
                    break;
                case 17:
                    diamondProductWay = "新手引导奖励";
                    break;
                case 18:
                    diamondProductWay = "协会关卡奖励";
                    break;
                case 19:
                    diamondProductWay = "协会章节奖励";
                    break;
                case 20:
                    diamondProductWay = "协会任务奖励";
                    break;
                case 21:
                    diamondProductWay = "协会每日排行榜奖励";
                    break;
                case 22:
                    diamondProductWay = "协会每日排行榜前三名奖励";
                    break;
                case 23:
                    diamondProductWay = "协会红包获得";
                    break;
                case 24:
                    diamondProductWay = "协会雕像升级奖励";
                    break;
                case 25:
                    diamondProductWay = "协会商会升级奖励";
                    break;
                case 26:
                    diamondProductWay = "协会活跃排行奖励";
                    break;
                case 27:
                    diamondProductWay = "套装集齐奖励";
                    break;
                case 28:
                    diamondProductWay = "充值获得";
                    break;
                case 29:
                    diamondProductWay = "搭配二选一活动奖励";
                    break;
                case 30:
                    diamondProductWay = "邮件赠送";
                    break;
                default:
                    break;
            }

            return diamondProductWay;
        }

        /// <summary>
        /// 解析金币消耗途径
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetGoldConsumeWay(int index)
        {
            string goldConsumeWay = string.Empty;

            switch (index)
            {
                case 1:
                    goldConsumeWay = "商城服饰购买消耗";
                    break;
                case 2:
                    goldConsumeWay = "商城道具购买消耗";
                    break;
                case 3:
                    goldConsumeWay = "商城材料购买消耗";
                    break;
                case 4:
                    goldConsumeWay = "购买换装提示消耗";
                    break;
                case 5:
                    goldConsumeWay = "服装制作消耗";
                    break;
                case 6:
                    goldConsumeWay = "服装染色消耗";
                    break;
                case 7:
                    goldConsumeWay = "服装进化消耗";
                    break;
                case 8:
                    goldConsumeWay = "许愿池抽奖1次消耗";
                    break;
                case 9:
                    goldConsumeWay = "许愿池抽奖10次消耗";
                    break;
                case 10:
                    goldConsumeWay = "创建协会消耗";
                    break;
                case 11:
                    goldConsumeWay = "捐献女神雕像消耗";
                    break;
                case 12:
                    goldConsumeWay = "协会祈祷消耗";
                    break;
                case 13:
                    goldConsumeWay = "神秘商店购买消耗";
                    break;
                default:
                    break;
            }

            return goldConsumeWay;
        }

        /// <summary>
        /// 解析钻石消耗途径
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetDiamondConsumeWay(int index)
        {
            string diamondConsumeWay = string.Empty;

            switch (index)
            {
                case 1:
                    diamondConsumeWay = "商城服饰购买消耗";
                    break;
                case 2:
                    diamondConsumeWay = "商城道具购买消耗";
                    break;
                case 3:
                    diamondConsumeWay = "商城材料购买消耗";
                    break;
                case 4:
                    diamondConsumeWay = "购买中级换装提示消耗";
                    break;
                case 5:
                    diamondConsumeWay = "购买高级换装提示消耗";
                    break;
                case 6:
                    diamondConsumeWay = "购买PK次数消耗";
                    break;
                case 7:
                    diamondConsumeWay = "购买公主过关次数消耗";
                    break;
                case 8:
                    diamondConsumeWay = "购买金币消耗";
                    break;
                case 9:
                    diamondConsumeWay = "购买体力消耗";
                    break;
                case 10:
                    diamondConsumeWay = "许愿星1次抽奖消耗";
                    break;
                case 11:
                    diamondConsumeWay = "许愿星10次抽奖消耗";
                    break;
                case 12:
                    diamondConsumeWay = "公主梦1次抽奖消耗";
                    break;
                case 13:
                    diamondConsumeWay = "公主梦10次抽奖消耗";
                    break;
                case 14:
                    diamondConsumeWay = "购买搭配二选一选择次数消耗";
                    break;
                case 15:
                    diamondConsumeWay = "刷新淘乐乐商城列表消耗";
                    break;
                case 16:
                    diamondConsumeWay = "购买协会关卡过关次数消耗";
                    break;
                case 17:
                    diamondConsumeWay = "刷新协会任务列表消耗";
                    break;
                case 18:
                    diamondConsumeWay = "创建协会消耗";
                    break;
                case 19:
                    diamondConsumeWay = "捐献女神雕像消耗";
                    break;
                case 20:
                    diamondConsumeWay = "协会祈祷消耗";
                    break;
                case 21:
                    diamondConsumeWay = "协会红包消耗";
                    break;
                case 22:
                    diamondConsumeWay = "刷新神秘商店消耗";
                    break;
                case 23:
                    diamondConsumeWay = "神秘商店购买消耗";
                    break;
                case 24:
                    diamondConsumeWay = "补签消耗";
                    break;
                default:
                    break;
            }

            return diamondConsumeWay;
        }

        /// <summary>
        /// 解析物品产生/消耗途径
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetItemWay(int index)
        {
            string itemGetWay = string.Empty;

            switch (index)
            {
                case 0:
                    itemGetWay = "商城购买";
                    break;
                case 1:
                    itemGetWay = "剧情关卡掉落";
                    break;
                case 2:
                    itemGetWay = "协会关卡奖励";
                    break;
                case 3:
                    itemGetWay = "PK奖励";
                    break;
                case 4:
                    itemGetWay = "PK商城兑换";
                    break;
                case 5:
                    itemGetWay = "抽宝箱奖励";
                    break;
                case 6:
                    itemGetWay = "抽宝箱商城兑换";
                    break;
                case 7:
                    itemGetWay = "服饰制作";
                    break;
                case 8:
                    itemGetWay = "服饰染色";
                    break;
                case 9:
                    itemGetWay = "服饰进化";
                    break;
                case 10:
                    itemGetWay = "服饰分解";
                    break;
                case 11:
                    itemGetWay = "每日任务奖励";
                    break;
                case 12:
                    itemGetWay = "成就奖励";
                    break;
                case 13:
                    itemGetWay = "签到奖励";
                    break;
                case 14:
                    itemGetWay = "累计登录奖励";
                    break;
                case 15:
                    itemGetWay = "累计消耗活动奖励";
                    break;
                case 16:
                    itemGetWay = "累计充值活动奖励";
                    break;
                case 17:
                    itemGetWay = "关卡掉落活动兑换";
                    break;
                case 18:
                    itemGetWay = "过关有礼活动奖励";
                    break;
                case 19:
                    itemGetWay = "搭配二选一兑换";
                    break;
                case 20:
                    itemGetWay = "搭配二选一活动奖励";
                    break;
                case 21:
                    itemGetWay = "VIP等级礼包";
                    break;
                case 22:
                    itemGetWay = "淘淘乐商城购买";
                    break;
                case 23:
                    itemGetWay = "珍宝阁购买";
                    break;
                case 24:
                    itemGetWay = "协会关卡兑换";
                    break;
                case 25:
                    itemGetWay = "协会任务奖励";
                    break;
                case 26:
                    itemGetWay = "协会活跃排行奖励";
                    break;
                case 27:
                    itemGetWay = "协会雕像升级奖励";
                    break;
                case 28:
                    itemGetWay = "协会商会升级奖励";
                    break;
                case 29:
                    itemGetWay = "套装集齐奖励";
                    break;
                case 30:
                    itemGetWay = "神秘商店购买";
                    break;
                case 31:
                    itemGetWay = "邮件";
                    break;
                case 32:
                    itemGetWay = "新手引导";
                    break;
                default:
                    break;
            }

            return itemGetWay;
        }

        /// <summary>
        /// 金钱流水查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTokenLogSearch_Click(object sender, EventArgs e)
        {
            GmGetRoleTokenLogReq req = new GmGetRoleTokenLogReq();

            if (!GetSearchTime(out req.StartTime, out req.EndTime))
            {
                return;
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// 获取查询时间
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        private bool GetSearchTime(out DateTime startTime, out DateTime endTime)
        {
            bool isTrue = true;
            startTime = DateTime.MinValue;
            endTime = DateTime.MaxValue;

            if (!string.IsNullOrEmpty(txtBoxStartTime.Text))
            {
                if (!DateTime.TryParse(txtBoxStartTime.Text, out startTime))
                {
                    MessageBox.Show("输入的起始时间格式不正确");
                    isTrue = false;
                }
            }

            if (!string.IsNullOrEmpty(txtBoxEndTime.Text))
            {
                if (!DateTime.TryParse(txtBoxEndTime.Text, out endTime))
                {
                    MessageBox.Show("输入的截止时间格式不正确");
                    isTrue = false;
                }
            }

            return isTrue;
        }

        private void txtBoxEndTime_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtBoxEndTime.Text))
            {
                txtBoxEndTime.Text = DateTime.Now.ToString();
            }
        }

        private void txtBoxStartTime_MouseClick(object sender, MouseEventArgs e)
        {
            if ("1900-01-01 0:00:00" == txtBoxStartTime.Text)
            {
                txtBoxStartTime.Clear();
            }
        }

        private void txtBoxStartTime_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtBoxStartTime.Text))
            {
                txtBoxStartTime.Text = "1900-01-01 0:00:00";
            }
        }


        private void lvTokenLog_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (lvTokenLog.SelectedItems.Count > 0)
                {
                    ListViewItem item = lvTokenLog.SelectedItems[0];
                    lvTokenLog.Items.Remove(item);
                }
            }
        }

        /// <summary>
        /// 物品流水查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnItemLog_Click(object sender, EventArgs e)
        {
            GmGetRoleItemLogReq req = new GmGetRoleItemLogReq();
            if (!GetSearchTime(out req.StartTime, out req.EndTime))
            {
                return;
            }

            if (!string.IsNullOrEmpty(txtBoxItemId.Text))
            {
                req.ItemId = Int32.Parse(txtBoxItemId.Text);
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        private UInt64 GetRoleId()
        {
            UInt64 roleId = 0;

            if (!string.IsNullOrEmpty(txtBoxRoleId.Text))
            {
                roleId = UInt64.Parse(txtBoxRoleId.Text);
            }

            return roleId;
        }

        private void txtBoxRoleId_MouseClick(object sender, MouseEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBoxRoleId.Text))
            {
                txtBoxRoleId.Clear();
                txtBoxRoleId.Text = "0";
            }
        }

        private void txtBoxRoleId_Leave(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(txtBoxRoleId.Text))
            //{
            //    txtBoxRoleId.Text = RoleId.ToString();
            //}
        }

        /// <summary>
        /// 签到日志查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSignin_Click(object sender, EventArgs e)
        {
            GmGetRoleSigninLogReq req = new GmGetRoleSigninLogReq();
            if (!GetSearchTime(out req.StartTime, out req.EndTime))
            {
                return;
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// 查询每日任务日志
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTask_Click(object sender, EventArgs e)
        {
            GmGetRoleTaskLogReq req = new GmGetRoleTaskLogReq();

            if (!string.IsNullOrEmpty(txtBoxTaskId.Text))
            {
                req.Id = Int32.Parse(txtBoxTaskId.Text);
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// 成就日志查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAchieve_Click(object sender, EventArgs e)
        {
            GmGetRoleAchieveLogReq req = new GmGetRoleAchieveLogReq();

            if (!string.IsNullOrEmpty(txtBoxAchiId.Text))
            {
                req.Id = Int32.Parse(txtBoxAchiId.Text);
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// pk日志查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPk_Click(object sender, EventArgs e)
        {
            GmGetRoleCompetitionLogReq req = new GmGetRoleCompetitionLogReq();
            req.RoleId = GetRoleId();

            if (0 == req.RoleId)
            {
                MessageBox.Show("请输入您要查找的玩家ID");
            }

            SendMessage(req);
        }

        /// <summary>
        /// 升级信息查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLevelGrade_Click(object sender, EventArgs e)
        {
            GmGetRoleGradeExpLogReq req = new GmGetRoleGradeExpLogReq();
            if (!GetSearchTime(out req.StartTime, out req.EndTime))
            {
                return;
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// VIP信息查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVipGrade_Click(object sender, EventArgs e)
        {
            GmGetRoleVipGradeExpLogReq req = new GmGetRoleVipGradeExpLogReq();
            req.RoleId = GetRoleId();

            if (0 == req.RoleId)
            {
                MessageBox.Show("请输入您要查找的玩家ID");
            }

            SendMessage(req);
        }

        /// <summary>
        /// 登录日志查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            GmGetRoleLoginDetailLogReq req = new GmGetRoleLoginDetailLogReq();
            if (!GetSearchTime(out req.StartTime, out req.EndTime))
            {
                return;
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// 服饰制作日志查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMake_Click(object sender, EventArgs e)
        {
            GmGetMakeDetailLogReq req = new GmGetMakeDetailLogReq();

            if (!string.IsNullOrEmpty(txtPictureId.Text))
            {
                req.Id = Int32.Parse(txtPictureId.Text);
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// 实时登录数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRealTmeLogin_Click(object sender, EventArgs e)
        {
            GmGetRealTimeLoginReq req = new GmGetRealTimeLoginReq();
            SendMessage(req);
        }

        /// <summary>
        /// 实时充值消费数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRealTimeRC_Click(object sender, EventArgs e)
        {
            GmGetRealTimeRCInfoReq req = new GmGetRealTimeRCInfoReq();
            SendMessage(req);
        }

        /// <summary>
        /// 染色日志查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEvolve_Click(object sender, EventArgs e)
        {
            GmGetDyeDetailLogReq req = new GmGetDyeDetailLogReq();
            if (!string.IsNullOrEmpty(txtBoxItemId.Text))
            {
                req.Id = Int32.Parse(txtBoxItemId.Text);
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// 兑换活动兑换明细
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnActivityExchange_Click(object sender, EventArgs e)
        {
            GmGetActivityExchangeDetailReq req = new GmGetActivityExchangeDetailReq();
            req.RoleId = GetRoleId();
            SendMessage(req);
        }

        /// <summary>
        /// 商城购买
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMallPurchase_Click(object sender, EventArgs e)
        {
            GmGetMallConsumeRecordReq req = new GmGetMallConsumeRecordReq();
            if (!GetSearchTime(out req.StartTime, out req.EndTime))
            {
                return;
            }

            if (!string.IsNullOrEmpty(txtBoxItemId.Text))
            {
                req.ItemId = Int32.Parse(txtBoxItemId.Text);
            }

            req.RoleId = GetRoleId();
            SendMessage(req);
        }

    }
}
