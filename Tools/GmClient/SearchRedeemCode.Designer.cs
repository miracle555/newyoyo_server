﻿namespace GmClient
{
    partial class SearchRedeemCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonSearchRole = new System.Windows.Forms.Button();
            this.buttonSearchBonus = new System.Windows.Forms.Button();
            this.textBoxKey = new System.Windows.Forms.TextBox();
            this.buttonSearchCode = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSearchType = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonSearchType);
            this.panel1.Controls.Add(this.buttonSearchRole);
            this.panel1.Controls.Add(this.buttonSearchBonus);
            this.panel1.Controls.Add(this.textBoxKey);
            this.panel1.Controls.Add(this.buttonSearchCode);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(316, 184);
            this.panel1.TabIndex = 0;
            // 
            // buttonSearchRole
            // 
            this.buttonSearchRole.Location = new System.Drawing.Point(3, 88);
            this.buttonSearchRole.Name = "buttonSearchRole";
            this.buttonSearchRole.Size = new System.Drawing.Size(310, 23);
            this.buttonSearchRole.TabIndex = 25;
            this.buttonSearchRole.Text = "查询角色id";
            this.buttonSearchRole.UseVisualStyleBackColor = true;
            this.buttonSearchRole.Click += new System.EventHandler(this.buttonSearchRole_Click);
            // 
            // buttonSearchBonus
            // 
            this.buttonSearchBonus.Location = new System.Drawing.Point(3, 59);
            this.buttonSearchBonus.Name = "buttonSearchBonus";
            this.buttonSearchBonus.Size = new System.Drawing.Size(310, 23);
            this.buttonSearchBonus.TabIndex = 24;
            this.buttonSearchBonus.Text = "查询礼包id";
            this.buttonSearchBonus.UseVisualStyleBackColor = true;
            this.buttonSearchBonus.Click += new System.EventHandler(this.buttonSearchBonus_Click);
            // 
            // textBoxKey
            // 
            this.textBoxKey.Location = new System.Drawing.Point(3, 3);
            this.textBoxKey.Name = "textBoxKey";
            this.textBoxKey.Size = new System.Drawing.Size(310, 21);
            this.textBoxKey.TabIndex = 23;
            // 
            // buttonSearchCode
            // 
            this.buttonSearchCode.Location = new System.Drawing.Point(3, 30);
            this.buttonSearchCode.Name = "buttonSearchCode";
            this.buttonSearchCode.Size = new System.Drawing.Size(310, 23);
            this.buttonSearchCode.TabIndex = 20;
            this.buttonSearchCode.Text = "查询兑换码";
            this.buttonSearchCode.UseVisualStyleBackColor = true;
            this.buttonSearchCode.Click += new System.EventHandler(this.buttonSearchCode_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(3, 146);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(310, 23);
            this.buttonCancel.TabIndex = 21;
            this.buttonCancel.Text = "取消";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonSearchType
            // 
            this.buttonSearchType.Location = new System.Drawing.Point(3, 117);
            this.buttonSearchType.Name = "buttonSearchType";
            this.buttonSearchType.Size = new System.Drawing.Size(310, 23);
            this.buttonSearchType.TabIndex = 26;
            this.buttonSearchType.Text = "查询兑换码类型";
            this.buttonSearchType.UseVisualStyleBackColor = true;
            this.buttonSearchType.Click += new System.EventHandler(this.buttonSearchType_Click);
            // 
            // SearchRedeemCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 208);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "SearchRedeemCode";
            this.Text = "SearchRedeemCode";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxKey;
        private System.Windows.Forms.Button buttonSearchCode;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSearchRole;
        private System.Windows.Forms.Button buttonSearchBonus;
        private System.Windows.Forms.Button buttonSearchType;
    }
}