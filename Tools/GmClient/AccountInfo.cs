﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GmClient
{
    public partial class AccountInfo : Form
    {
        //////////////////////////////////////////////////////////////////////////
        public AccountInfo(string account)
        {
            InitializeComponent();

            if (!string.IsNullOrEmpty(account))
            {
                textBoxAccount.ReadOnly = true;
            }
            else
            {
                textBoxAccount.ReadOnly = false;
            }

            textBoxAccount.Text = account;
            tbGmLevel.Text = "1";

            btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public string Account
        {
            get
            {
                return textBoxAccount.Text;
            }
        }

        public string Password
        {
            get
            {
                return textBoxPassword.Text;
            }
        }

        public int GmLevel
        {
            get
            {
                return int.Parse(tbGmLevel.Text);
            }
        }

        //////////////////////////////////////////////////////////////////////////
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int level = int.Parse(tbGmLevel.Text);
            level++;
            tbGmLevel.Text = level.ToString();
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            int level = int.Parse(tbGmLevel.Text);
            if (level > 1)
            {
                level--;
                tbGmLevel.Text = level.ToString();
            }
        }
    }
}
