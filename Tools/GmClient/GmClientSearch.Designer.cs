﻿namespace GmClient
{
    partial class GmClientSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTokenLogSearch = new System.Windows.Forms.Button();
            this.txtBoxStartTime = new System.Windows.Forms.TextBox();
            this.txtBoxEndTime = new System.Windows.Forms.TextBox();
            this.lvTokenLog = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxRoleId = new System.Windows.Forms.TextBox();
            this.btnItemLog = new System.Windows.Forms.Button();
            this.btnSignin = new System.Windows.Forms.Button();
            this.btnTask = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxTaskId = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxAchiId = new System.Windows.Forms.TextBox();
            this.btnAchieve = new System.Windows.Forms.Button();
            this.btnPk = new System.Windows.Forms.Button();
            this.btnLevelGrade = new System.Windows.Forms.Button();
            this.btnVipGrade = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBoxItemId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPictureId = new System.Windows.Forms.TextBox();
            this.btnMake = new System.Windows.Forms.Button();
            this.btnRealTmeLogin = new System.Windows.Forms.Button();
            this.btnRealTimeRC = new System.Windows.Forms.Button();
            this.lvRealTime = new System.Windows.Forms.ListView();
            this.btnEvolve = new System.Windows.Forms.Button();
            this.btnActivityExchange = new System.Windows.Forms.Button();
            this.lvVipGiftDetail = new System.Windows.Forms.ListView();
            this.btnMallPurchase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTokenLogSearch
            // 
            this.btnTokenLogSearch.Location = new System.Drawing.Point(11, 86);
            this.btnTokenLogSearch.Name = "btnTokenLogSearch";
            this.btnTokenLogSearch.Size = new System.Drawing.Size(93, 23);
            this.btnTokenLogSearch.TabIndex = 0;
            this.btnTokenLogSearch.Text = "金钱流水查询";
            this.btnTokenLogSearch.UseVisualStyleBackColor = true;
            this.btnTokenLogSearch.Click += new System.EventHandler(this.btnTokenLogSearch_Click);
            // 
            // txtBoxStartTime
            // 
            this.txtBoxStartTime.Location = new System.Drawing.Point(77, 46);
            this.txtBoxStartTime.Name = "txtBoxStartTime";
            this.txtBoxStartTime.Size = new System.Drawing.Size(121, 21);
            this.txtBoxStartTime.TabIndex = 1;
            this.txtBoxStartTime.Text = "1900-01-01 0:00:00";
            this.txtBoxStartTime.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtBoxStartTime_MouseClick);
            this.txtBoxStartTime.Leave += new System.EventHandler(this.txtBoxStartTime_Leave);
            // 
            // txtBoxEndTime
            // 
            this.txtBoxEndTime.Location = new System.Drawing.Point(276, 46);
            this.txtBoxEndTime.Name = "txtBoxEndTime";
            this.txtBoxEndTime.Size = new System.Drawing.Size(163, 21);
            this.txtBoxEndTime.TabIndex = 2;
            this.txtBoxEndTime.Leave += new System.EventHandler(this.txtBoxEndTime_Leave);
            // 
            // lvTokenLog
            // 
            this.lvTokenLog.Location = new System.Drawing.Point(11, 145);
            this.lvTokenLog.Name = "lvTokenLog";
            this.lvTokenLog.Size = new System.Drawing.Size(1050, 649);
            this.lvTokenLog.TabIndex = 3;
            this.lvTokenLog.UseCompatibleStateImageBehavior = false;
            this.lvTokenLog.View = System.Windows.Forms.View.Details;
            this.lvTokenLog.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvTokenLog_KeyUp);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "起始时间：";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(216, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "截止时间：";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "角色id：";
            // 
            // txtBoxRoleId
            // 
            this.txtBoxRoleId.Location = new System.Drawing.Point(77, 19);
            this.txtBoxRoleId.Name = "txtBoxRoleId";
            this.txtBoxRoleId.Size = new System.Drawing.Size(121, 21);
            this.txtBoxRoleId.TabIndex = 8;
            this.txtBoxRoleId.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtBoxRoleId_MouseClick);
            this.txtBoxRoleId.Leave += new System.EventHandler(this.txtBoxRoleId_Leave);
            // 
            // btnItemLog
            // 
            this.btnItemLog.Location = new System.Drawing.Point(447, 85);
            this.btnItemLog.Name = "btnItemLog";
            this.btnItemLog.Size = new System.Drawing.Size(116, 23);
            this.btnItemLog.TabIndex = 10;
            this.btnItemLog.Text = "物品流水查询";
            this.btnItemLog.UseVisualStyleBackColor = true;
            this.btnItemLog.Click += new System.EventHandler(this.btnItemLog_Click);
            // 
            // btnSignin
            // 
            this.btnSignin.Location = new System.Drawing.Point(110, 86);
            this.btnSignin.Name = "btnSignin";
            this.btnSignin.Size = new System.Drawing.Size(93, 23);
            this.btnSignin.TabIndex = 11;
            this.btnSignin.Text = "签到日志查询";
            this.btnSignin.UseVisualStyleBackColor = true;
            this.btnSignin.Click += new System.EventHandler(this.btnSignin_Click);
            // 
            // btnTask
            // 
            this.btnTask.Location = new System.Drawing.Point(209, 86);
            this.btnTask.Name = "btnTask";
            this.btnTask.Size = new System.Drawing.Size(116, 23);
            this.btnTask.TabIndex = 12;
            this.btnTask.Text = "每日任务日志查询";
            this.btnTask.UseVisualStyleBackColor = true;
            this.btnTask.Click += new System.EventHandler(this.btnTask_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(218, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "任务Id：";
            // 
            // txtBoxTaskId
            // 
            this.txtBoxTaskId.Location = new System.Drawing.Point(276, 19);
            this.txtBoxTaskId.Name = "txtBoxTaskId";
            this.txtBoxTaskId.Size = new System.Drawing.Size(49, 21);
            this.txtBoxTaskId.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(331, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 21);
            this.label5.TabIndex = 15;
            this.label5.Text = "成就id：";
            // 
            // txtBoxAchiId
            // 
            this.txtBoxAchiId.Location = new System.Drawing.Point(382, 19);
            this.txtBoxAchiId.Name = "txtBoxAchiId";
            this.txtBoxAchiId.Size = new System.Drawing.Size(57, 21);
            this.txtBoxAchiId.TabIndex = 16;
            // 
            // btnAchieve
            // 
            this.btnAchieve.Location = new System.Drawing.Point(332, 85);
            this.btnAchieve.Name = "btnAchieve";
            this.btnAchieve.Size = new System.Drawing.Size(104, 23);
            this.btnAchieve.TabIndex = 17;
            this.btnAchieve.Text = "成就日志查询";
            this.btnAchieve.UseVisualStyleBackColor = true;
            this.btnAchieve.Click += new System.EventHandler(this.btnAchieve_Click);
            // 
            // btnPk
            // 
            this.btnPk.Location = new System.Drawing.Point(11, 116);
            this.btnPk.Name = "btnPk";
            this.btnPk.Size = new System.Drawing.Size(93, 23);
            this.btnPk.TabIndex = 18;
            this.btnPk.Text = "PK日志查询";
            this.btnPk.UseVisualStyleBackColor = true;
            this.btnPk.Click += new System.EventHandler(this.btnPk_Click);
            // 
            // btnLevelGrade
            // 
            this.btnLevelGrade.Location = new System.Drawing.Point(111, 115);
            this.btnLevelGrade.Name = "btnLevelGrade";
            this.btnLevelGrade.Size = new System.Drawing.Size(92, 23);
            this.btnLevelGrade.TabIndex = 19;
            this.btnLevelGrade.Text = "升级日志查询";
            this.btnLevelGrade.UseVisualStyleBackColor = true;
            this.btnLevelGrade.Click += new System.EventHandler(this.btnLevelGrade_Click);
            // 
            // btnVipGrade
            // 
            this.btnVipGrade.Location = new System.Drawing.Point(210, 114);
            this.btnVipGrade.Name = "btnVipGrade";
            this.btnVipGrade.Size = new System.Drawing.Size(112, 23);
            this.btnVipGrade.TabIndex = 20;
            this.btnVipGrade.Text = "VIP升级日志查询";
            this.btnVipGrade.UseVisualStyleBackColor = true;
            this.btnVipGrade.Click += new System.EventHandler(this.btnVipGrade_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(332, 116);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(104, 23);
            this.btnLogin.TabIndex = 21;
            this.btnLogin.Text = "登录日志查询";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(445, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 18);
            this.label6.TabIndex = 22;
            this.label6.Text = "物品ID：";
            // 
            // txtBoxItemId
            // 
            this.txtBoxItemId.Location = new System.Drawing.Point(494, 19);
            this.txtBoxItemId.Name = "txtBoxItemId";
            this.txtBoxItemId.Size = new System.Drawing.Size(69, 21);
            this.txtBoxItemId.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(445, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 21);
            this.label7.TabIndex = 24;
            this.label7.Text = "图纸ID：";
            // 
            // txtPictureId
            // 
            this.txtPictureId.Location = new System.Drawing.Point(494, 46);
            this.txtPictureId.Name = "txtPictureId";
            this.txtPictureId.Size = new System.Drawing.Size(69, 21);
            this.txtPictureId.TabIndex = 25;
            // 
            // btnMake
            // 
            this.btnMake.Location = new System.Drawing.Point(447, 116);
            this.btnMake.Name = "btnMake";
            this.btnMake.Size = new System.Drawing.Size(116, 23);
            this.btnMake.TabIndex = 26;
            this.btnMake.Text = "服装制作日志查询";
            this.btnMake.UseVisualStyleBackColor = true;
            this.btnMake.Click += new System.EventHandler(this.btnMake_Click);
            // 
            // btnRealTmeLogin
            // 
            this.btnRealTmeLogin.Location = new System.Drawing.Point(583, 17);
            this.btnRealTmeLogin.Name = "btnRealTmeLogin";
            this.btnRealTmeLogin.Size = new System.Drawing.Size(110, 23);
            this.btnRealTmeLogin.TabIndex = 27;
            this.btnRealTmeLogin.Text = "实时登录数据";
            this.btnRealTmeLogin.UseVisualStyleBackColor = true;
            this.btnRealTmeLogin.Click += new System.EventHandler(this.btnRealTmeLogin_Click);
            // 
            // btnRealTimeRC
            // 
            this.btnRealTimeRC.Location = new System.Drawing.Point(583, 44);
            this.btnRealTimeRC.Name = "btnRealTimeRC";
            this.btnRealTimeRC.Size = new System.Drawing.Size(110, 23);
            this.btnRealTimeRC.TabIndex = 28;
            this.btnRealTimeRC.Text = "实时充值消费数据";
            this.btnRealTimeRC.UseVisualStyleBackColor = true;
            this.btnRealTimeRC.Click += new System.EventHandler(this.btnRealTimeRC_Click);
            // 
            // lvRealTime
            // 
            this.lvRealTime.Location = new System.Drawing.Point(699, 7);
            this.lvRealTime.Name = "lvRealTime";
            this.lvRealTime.Size = new System.Drawing.Size(678, 69);
            this.lvRealTime.TabIndex = 29;
            this.lvRealTime.UseCompatibleStateImageBehavior = false;
            this.lvRealTime.View = System.Windows.Forms.View.Details;
            // 
            // btnEvolve
            // 
            this.btnEvolve.Location = new System.Drawing.Point(583, 86);
            this.btnEvolve.Name = "btnEvolve";
            this.btnEvolve.Size = new System.Drawing.Size(110, 23);
            this.btnEvolve.TabIndex = 31;
            this.btnEvolve.Text = "染色日志查询";
            this.btnEvolve.UseVisualStyleBackColor = true;
            this.btnEvolve.Click += new System.EventHandler(this.btnEvolve_Click);
            // 
            // btnActivityExchange
            // 
            this.btnActivityExchange.Location = new System.Drawing.Point(583, 116);
            this.btnActivityExchange.Name = "btnActivityExchange";
            this.btnActivityExchange.Size = new System.Drawing.Size(159, 23);
            this.btnActivityExchange.TabIndex = 32;
            this.btnActivityExchange.Text = "兑换活动物品兑换日志查询";
            this.btnActivityExchange.UseVisualStyleBackColor = true;
            this.btnActivityExchange.Click += new System.EventHandler(this.btnActivityExchange_Click);
            // 
            // lvVipGiftDetail
            // 
            this.lvVipGiftDetail.Location = new System.Drawing.Point(1067, 145);
            this.lvVipGiftDetail.Name = "lvVipGiftDetail";
            this.lvVipGiftDetail.Size = new System.Drawing.Size(310, 649);
            this.lvVipGiftDetail.TabIndex = 33;
            this.lvVipGiftDetail.UseCompatibleStateImageBehavior = false;
            this.lvVipGiftDetail.View = System.Windows.Forms.View.Details;
            // 
            // btnMallPurchase
            // 
            this.btnMallPurchase.Location = new System.Drawing.Point(748, 116);
            this.btnMallPurchase.Name = "btnMallPurchase";
            this.btnMallPurchase.Size = new System.Drawing.Size(119, 23);
            this.btnMallPurchase.TabIndex = 34;
            this.btnMallPurchase.Text = "角色消费日志查询";
            this.btnMallPurchase.UseVisualStyleBackColor = true;
            this.btnMallPurchase.Click += new System.EventHandler(this.btnMallPurchase_Click);
            // 
            // GmClientSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1389, 806);
            this.Controls.Add(this.btnMallPurchase);
            this.Controls.Add(this.lvVipGiftDetail);
            this.Controls.Add(this.btnActivityExchange);
            this.Controls.Add(this.btnEvolve);
            this.Controls.Add(this.lvRealTime);
            this.Controls.Add(this.btnRealTimeRC);
            this.Controls.Add(this.btnRealTmeLogin);
            this.Controls.Add(this.btnMake);
            this.Controls.Add(this.txtPictureId);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBoxItemId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.btnVipGrade);
            this.Controls.Add(this.btnLevelGrade);
            this.Controls.Add(this.btnPk);
            this.Controls.Add(this.btnAchieve);
            this.Controls.Add(this.txtBoxAchiId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBoxTaskId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnTask);
            this.Controls.Add(this.btnSignin);
            this.Controls.Add(this.btnItemLog);
            this.Controls.Add(this.txtBoxRoleId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvTokenLog);
            this.Controls.Add(this.txtBoxEndTime);
            this.Controls.Add(this.txtBoxStartTime);
            this.Controls.Add(this.btnTokenLogSearch);
            this.Name = "GmClientSearch";
            this.Text = "GmClientSearch";
            this.Load += new System.EventHandler(this.GmClientSearch_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTokenLogSearch;
        private System.Windows.Forms.TextBox txtBoxEndTime;
        public System.Windows.Forms.ListView lvTokenLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxStartTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxRoleId;
        private System.Windows.Forms.Button btnItemLog;
        private System.Windows.Forms.Button btnSignin;
        private System.Windows.Forms.Button btnTask;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxTaskId;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxAchiId;
        private System.Windows.Forms.Button btnAchieve;
        private System.Windows.Forms.Button btnPk;
        private System.Windows.Forms.Button btnLevelGrade;
        private System.Windows.Forms.Button btnVipGrade;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBoxItemId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPictureId;
        private System.Windows.Forms.Button btnMake;
        private System.Windows.Forms.Button btnRealTmeLogin;
        private System.Windows.Forms.Button btnRealTimeRC;
        private System.Windows.Forms.Button btnEvolve;
        private System.Windows.Forms.Button btnActivityExchange;
        public System.Windows.Forms.ListView lvRealTime;
        public System.Windows.Forms.ListView lvVipGiftDetail;
        private System.Windows.Forms.Button btnMallPurchase;
    }
}