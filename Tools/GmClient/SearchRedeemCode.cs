﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GmClient
{
    public partial class SearchRedeemCode : Form
    {
        public SearchRedeemCode()
        {
            InitializeComponent();

            buttonSearchCode.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonSearchBonus.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonSearchRole.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonSearchType.DialogResult = System.Windows.Forms.DialogResult.OK;
            buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public string Key
        {
            get
            {
                return textBoxKey.Text;
            }
        }

        public string Index
        {
            get;
            private set;
        }

        private void buttonSearchCode_Click(object sender, EventArgs e)
        {
            Index = "Code";
        }

        private void buttonSearchBonus_Click(object sender, EventArgs e)
        {
            Index = "Bonus";
        }

        private void buttonSearchRole_Click(object sender, EventArgs e)
        {
            Index = "Role";
        }

        private void buttonSearchType_Click(object sender, EventArgs e)
        {
            Index = "Type";
        }
    }
}
