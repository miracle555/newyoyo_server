﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protocols;
using System.Net;
using System.Runtime.InteropServices;
using System.IO;
using Protocols.Error;
using OfficeOpenXml;

namespace GmClient
{
    public partial class GmClient : Form
    {
        //////////////////////////////////////////////////////////////////////////
        class MailTag
        {
            public Int64 MailId;
            public Int64 BodyId;

            public MailTag(Int64 mId, Int64 bId)
            {
                MailId = mId;
                BodyId = bId;
            }

            public override bool Equals(object obj)
            {
                MailTag other = obj as MailTag;
                if (other != null)
                {
                    if (other.MailId == MailId && other.BodyId == BodyId)
                        return true;
                    return false;
                }

                return base.Equals(obj);
            }

            public override int GetHashCode()
            {
                return string.Format("{0}::{1}", MailId, BodyId).GetHashCode();
            }
        }

        //[2/29/2016 马天] 内容限制长度
        private const int MailTitleMaxLength = 12;
        private const int MailContentMaxLength = 200;
        private const int SystemPostMaxLength = 45;

        //////////////////////////////////////////////////////////////////////////
        Timer timer = new Timer();
        Session session = new Session();
        List<SystemPostItem> posts = new List<SystemPostItem>();
        Dictionary<MailTag, MailInfo> mails = new Dictionary<MailTag, MailInfo>();
        public static Dictionary<int, string> ItemDatas = new Dictionary<int, string>();
        public static Dictionary<string, string> AttrDatas = new Dictionary<string, string>();

        private List<int> equips = new List<int>();
        public UInt64 roleId = 0;
        public string name = string.Empty;
        GmClientSearch gmSearch;
        public static Dictionary<int, string> QuestDatas = new Dictionary<int, string>();
        public static Dictionary<int, string> AchieveDatas = new Dictionary<int, string>();
        public static Dictionary<int, ManufactureMakeData> MakeDatas = new Dictionary<int, ManufactureMakeData>();
        /// <summary>
        /// 弹幕信息
        /// </summary>
        Dictionary<int, SubtitleInfo> subtitles = new Dictionary<int, SubtitleInfo>();

        Random ra = new Random();

        //////////////////////////////////////////////////////////////////////////
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string section, string key, string defVal, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        //////////////////////////////////////////////////////////////////////////
        public GmClient()
        {
            InitializeComponent();

            InitData();
            InitForm();
        }

        //////////////////////////////////////////////////////////////////////////
        void LoadData()
        {
            if (!File.Exists(".\\GmClient.ini"))
            {
                var file = File.Create(".\\GmClient.ini");
                file.Close();
            }
            else
            {
                StringBuilder sb = new StringBuilder(255);
                GetPrivateProfileString("Server", "Count", "", sb, 255, ".\\GmClient.ini");

                try
                {
                    int count = int.Parse(sb.ToString());
                    for (int i = 1; i <= count; i++)
                    {
                        string key = string.Format("Server{0}", i);
                        GetPrivateProfileString("Server", key, "", sb, 255, ".\\GmClient.ini");
                        comboBoxServer.Items.Add(sb.ToString());
                    }
                }
                catch (Exception)
                {
                }

                GetPrivateProfileString("Login", "account", "", sb, 255, ".\\GmClient.ini");
                TBAccount.Text = sb.ToString();
                GetPrivateProfileString("Login", "password", "", sb, 255, ".\\GmClient.ini");
                TBPassword.Text = sb.ToString();
            }
        }

        void SaveData()
        {
            if (!File.Exists(".\\GmClient.ini"))
            {
                var file = File.Create(".\\GmClient.ini");
                file.Close();
            }

            WritePrivateProfileString("Login", "account", TBAccount.Text, ".\\GmClient.ini");
            WritePrivateProfileString("Login", "password", TBPassword.Text, ".\\GmClient.ini");
        }

        public void InitData()
        {
            LoadData();

            timer.Interval = 200;
            timer.Tick += Tick;
            timer.Start();

            session.Init();
        }

        void InitForm()
        {
            BtnLogout.Enabled = false;

            DateTime now = DateTime.Now;
            tbSysPostStartTime.Text = now.ToString();
            tbSysPostEndTime.Text = now.ToString();
            tbDisableDendline.Text = DateTime.MaxValue.ToString();
            tbLogStart.Text = DateTime.MinValue.ToString();
            tbLogEnd.Text = DateTime.MaxValue.ToString();
            textBoxCheckReloadTime.Text = now.ToString();

            lvPlayers.Columns.Add("RoleId", "角色id", 120);
            lvPlayers.Columns.Add("Role", "角色", 120);
            lvPlayers.Columns.Add("Level", "等级");
            lvPlayers.Columns.Add("VipGrade", "Vip等级");
            lvPlayers.Columns.Add("Stamina", "体力");
            lvPlayers.Columns.Add("Gold", "金币");
            lvPlayers.Columns.Add("Diamond", "钻石");
            lvPlayers.Columns.Add("PkPoint", "代币");
            lvPlayers.Columns.Add("GuildId", "所在协会Id", 120);
            lvPlayers.Columns.Add("PassLevelId", "最后玩的关卡", 120);

            lvAccessories.Columns.Add("ItemId", "道具id", 50);
            lvAccessories.Columns.Add("ItemName", "道具", 100);
            lvAccessories.Columns.Add("Attr", "属性", 80);
            lvAccessories.Columns.Add("Count", "数量", 40);

            lvMailReceiver.Columns.Add("RoleId", "角色id", 120);
            lvMailReceiver.Columns.Add("PlatformId", "平台id", 120);

            lvAccounts.FullRowSelect = true;
            lvPlayers.FullRowSelect = true;
            lvDetails.FullRowSelect = true;
            lvAccessories.FullRowSelect = true;
            lvMailReceiver.FullRowSelect = true;
            lvSbutitles.FullRowSelect = true;
        }

        public void Tick(object sender, EventArgs e)
        {
            Protocol proto = session.GetMessage();
            while (proto != null)
            {
                if (proto.GetID() == ProtoIDReserve.CMD_SERVER_CONNECTED)
                {
                    ServerConnected body = (ServerConnected)proto.GetBody();
                    HandleConnect(body.success);
                }
                else if (proto.GetID() == ProtoIDReserve.CMD_SERVER_DISCONNECTED)
                {
                    HandleDisconnect();
                }
                else
                {
                    HandleMessage(proto);
                }

                proto = session.GetMessage();
            }
        }

        void HandleConnect(bool success)
        {
            Output("Server <-----> Client");

            UserLoginReq req = new UserLoginReq();
            req.platformType = 0;
            req.account = TBAccount.Text;
            req.password = TBPassword.Text;
            SendMessage(req);
        }

        void HandleDisconnect()
        {
            Output("Server <--x--> Client");
            Error(ErrorID.Success, "登出");

            BtnLogin.Enabled = true;
            BtnLogout.Enabled = false;
        }

        void HandleMessage(Protocol proto)
        {
            Output(string.Format("Server ------> {0}", proto.GetID().ToString()));

            if (proto.GetID() == ProtoID.CMD_LOGIN_USERLOGIN_RSP)
            {
                UserLoginRsp rsp = (UserLoginRsp)proto.GetBody();
                if (rsp.errorCode != ErrorID.Success)
                {
                    Error(rsp.errorCode, string.Format("登录失败: {0}", rsp.errorCode.ToString()));

                    BtnLogin.Enabled = true;
                }
                else
                {
                    Error(ErrorID.Success, "登录成功");

                    BtnLogout.Enabled = true;

                    GmItemDataReq itemReq = new GmItemDataReq();
                    SendMessage(itemReq);
                    GmAttrDataReq attrReq = new GmAttrDataReq();
                    SendMessage(attrReq);
                }
            }
            else if (proto.GetID() == ProtoID.CMD_SYSTEMPOST_LIST_RSP)
            {
                SystemPostListRsp rsp = (SystemPostListRsp)proto.GetBody();
                posts = rsp.posts;
                lvAccounts.BeginUpdate();
                lvAccounts.Columns.Clear();
                lvAccounts.Items.Clear();

                lvAccounts.Columns.Add("Begin", "起始时间", 120);
                lvAccounts.Columns.Add("End", "结束时间", 120);
                lvAccounts.Columns.Add("Platform", "渠道平台", 40);
                lvAccounts.Columns.Add("Content", "内容", 360);
                lvAccounts.Columns.Add("State", "状态", 120);
                lvAccounts.Columns.Add("Guid", "Guid");

                foreach (var post in posts)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = post.post.timeStart.ToString();
                    lvi.SubItems.Add(post.post.timeEnd.ToString());
                    lvi.SubItems.Add(post.platform.ToString());
                    lvi.SubItems.Add(post.post.contents[0].message);
                    if (post.isStart)
                    {
                        lvi.SubItems.Add("正在发送");
                    }
                    else
                    {
                        lvi.SubItems.Add("等待发送");
                    }
                    lvi.SubItems.Add(post.guid.ToString());
                    lvAccounts.Items.Add(lvi);
                }
                lvAccounts.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_DB_QUERY_ACCOUNTINFO_RSP)
            {
                DBQueryAccountInfoRsp rsp = (DBQueryAccountInfoRsp)proto.GetBody();
                lvAccounts.BeginUpdate();
                lvAccounts.Columns.Clear();
                lvAccounts.Items.Clear();

                lvAccounts.Columns.Add("PlatformId", "平台id");
                lvAccounts.Columns.Add("Puid", "PUID");
                lvAccounts.Columns.Add("AccountId", "帐号id");
                lvAccounts.Columns.Add("Account", "帐号", 120);
                lvAccounts.Columns.Add("RoleId", "角色id", 120);
                lvAccounts.Columns.Add("Role", "角色", 120);
                lvAccounts.Columns.Add("GmLevel", "gm等级");
                lvAccounts.Columns.Add("CreateTime", "创建时间", 150);
                lvAccounts.Columns.Add("LastLoginTime", "最后登录时间", 150);
                lvAccounts.Columns.Add("Disable", "封停");
                lvAccounts.Columns.Add("DisableDendline", "封停期限", 150);
                lvAccounts.Columns.Add("Online", "状态");

                foreach (var account in rsp.accounts)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = account.Value.platform.ToString();
                    lvi.SubItems.Add(account.Value.puid.ToString());
                    lvi.SubItems.Add(account.Value.accountId.ToString());
                    lvi.SubItems.Add(account.Value.account);
                    lvi.SubItems.Add(account.Value.roleId.ToString());
                    lvi.SubItems.Add(account.Value.role);
                    lvi.SubItems.Add(account.Value.gmLevel.ToString());
                    lvi.SubItems.Add(account.Value.createTime);
                    lvi.SubItems.Add(account.Value.lastLoginTime);
                    lvi.SubItems.Add(account.Value.disable == 0 ? "" : "封停");
                    lvi.SubItems.Add(account.Value.disable == 0 ? "" : account.Value.disableDendline);
                    lvi.SubItems.Add(account.Value.isOnline == 0 ? "离线" : "在线");
                    lvAccounts.Items.Add(lvi);
                }
                lvAccounts.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_GM_GETPLAYERINFO_RSP)
            {
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                GmGetPlayerInfoRsp rsp = (GmGetPlayerInfoRsp)proto.GetBody();
                lvPlayers.BeginUpdate();
                lvPlayers.Items.Clear();

                ListViewItem lvi = new ListViewItem();
                lvi.Text = rsp.roleId.ToString();
                lvi.SubItems.Add(rsp.name);
                lvi.SubItems.Add(rsp.level.ToString());
                lvi.SubItems.Add(rsp.VipGrade.ToString());
                lvi.SubItems.Add(rsp.stamina.ToString());
                lvi.SubItems.Add(rsp.gold.ToString());
                lvi.SubItems.Add(rsp.diamond.ToString());
                lvi.SubItems.Add(rsp.pkPoint.ToString());
                lvi.SubItems.Add(rsp.GuildId.ToString());
                lvi.SubItems.Add(rsp.LastLevelId.ToString());
                lvPlayers.Items.Add(lvi);
                lvPlayers.EndUpdate();

                roleId = rsp.roleId;
                name = rsp.name;
                equips = rsp.Equips;
            }
            else if (proto.GetID() == ProtoID.CMD_GM_ITEMLIST_RSP)
            {
                GmItemListRsp rsp = (GmItemListRsp)proto.GetBody();
                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("RoleId", "角色id", 120);
                lvDetails.Columns.Add("ItemId", "道具id");
                lvDetails.Columns.Add("ItemName", "道具", 120);
                lvDetails.Columns.Add("ItemCount", "数量");
                lvDetails.Columns.Add("IsEquip", "是否装备");

                foreach (var item in rsp.items)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = rsp.roleId.ToString();
                    lvi.SubItems.Add(item.id.ToString());
                    if (ItemDatas.ContainsKey(item.id))
                    {
                        lvi.SubItems.Add(ItemDatas[item.id]);
                    }
                    else
                    {
                        lvi.SubItems.Add("");
                    }
                    lvi.SubItems.Add(item.count.ToString());
                    lvi.SubItems.Add(equips.Contains(item.id) ? "是" : "否");

                    lvDetails.Items.Add(lvi);
                }
                lvDetails.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_GM_CHANGENAME_RSP)
            {
                GmChangeNameRsp rsp = (GmChangeNameRsp)proto.GetBody();
                if (rsp.error == ErrorID.Success)
                {
                    Error(ErrorID.Success, "角色改名成功");
                }
                else
                {
                    Error(rsp.error, "角色改名失败：重名了");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_GM_ITEMEDIT_RSP)
            {
                GmItemEditRsp rsp = (GmItemEditRsp)proto.GetBody();
                if (rsp.error == ErrorID.Success)
                {
                    Error(ErrorID.Success, "物品编辑成功");
                }
                else
                {
                    Error(rsp.error, string.Format("物品编辑失败: {0}", rsp.error.ToString()));
                }
            }
            else if (proto.GetID() == ProtoID.CMD_GM_ITEMDATA_RSP)
            {
                ItemDatas.Clear();
                GmItemDataRsp rsp = (GmItemDataRsp)proto.GetBody();
                ItemDatas = rsp.datas;
                Error(ErrorID.Success, "获得物品数据");
            }
            else if (proto.GetID() == ProtoID.CMD_GM_ATTRDATA_RSP)
            {
                StringBuilder sb = new StringBuilder(255);

                AttrDatas.Clear();
                GmAttrDataRsp rsp = (GmAttrDataRsp)proto.GetBody();
                foreach (string attr in rsp.attrs)
                {
                    int size = GetPrivateProfileString("Attr", attr, "", sb, 255, ".\\GmClient.ini");
                    if (size > 0)
                    {
                        AttrDatas[sb.ToString()] = attr;
                    }
                }

                Error(ErrorID.Success, "获得属性数据");
            }
            else if (proto.GetID() == ProtoID.CMD_MAIL_SENDMAIL_RSP)
            {
                SendMailRsp rsp = (SendMailRsp)proto.GetBody();
                if (rsp.error == ErrorID.Success)
                {
                    Error(ErrorID.Success, "邮件发送成功");
                }
                else
                {
                    Error(rsp.error, string.Format("邮件发送失败: {0}", rsp.error.ToString()));
                }
            }
            else if (proto.GetID() == ProtoID.CMD_MAIL_QUERY_MAILLIST_RSP)
            {
                QueryMailListRsp rsp = (QueryMailListRsp)proto.GetBody();
                mails.Clear();
                foreach (var mail in rsp.mails)
                {
                    MailTag tag = new MailTag(mail.mailItemId, mail.mailBodyId);
                    mails[tag] = mail;
                }

                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("RoleId", "角色id", 120);
                lvDetails.Columns.Add("MailId", "mail id");
                lvDetails.Columns.Add("BodyId", "body id");
                lvDetails.Columns.Add("MailTitle", "邮件标题", 180);
                lvDetails.Columns.Add("SendName", "发件人昵称");
                lvDetails.Columns.Add("SenderId", "发件人Id");
                lvDetails.Columns.Add("SendTime", "发件时间", 150);
                lvDetails.Columns.Add("AttacherItem", "附件物品", 240);
                lvDetails.Columns.Add("AttacherAttr", "附件属性", 240);
                lvDetails.Columns.Add("DrawAttachedItems", "附件领取");
                lvDetails.Columns.Add("DrawAttachTime", "领取时间", 240);
                lvDetails.Columns.Add("Expired", "过期时间", 240);
                lvDetails.Columns.Add("Deleted", "删除");

                foreach (var mail in mails)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = rsp.roleId.ToString();
                    lvi.SubItems.Add(mail.Key.MailId.ToString());
                    lvi.SubItems.Add(mail.Key.BodyId.ToString());
                    lvi.SubItems.Add(mail.Value.title);
                    lvi.SubItems.Add(mail.Value.senderName);
                    lvi.SubItems.Add(mail.Value.sendId.ToString());
                    lvi.SubItems.Add(mail.Value.sentTime.ToString());
                    lvi.SubItems.Add(GetMailItemAttach(mail.Value.attacher));
                    lvi.SubItems.Add(GetMailAttrAttach(mail.Value.attacher));
                    lvi.SubItems.Add(mail.Value.IsDrawAttachedItems ? "已领取" : "未领取");
                    lvi.SubItems.Add(mail.Value.drawAttachTime.ToString());
                    lvi.SubItems.Add(mail.Value.expiredTime.ToString());
                    lvi.SubItems.Add(mail.Value.isDeleted ? "已删除" : "未删除");
                    lvDetails.Items.Add(lvi);
                }
                lvDetails.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_MAIL_QUERY_PRESETSMAILLIST_RSP)
            {
                QueryPreSetsMailListRsp rsp = (QueryPreSetsMailListRsp)proto.GetBody();

                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("BodyId", "邮件编号");
                lvDetails.Columns.Add("MailTitle", "邮件标题", 180);
                lvDetails.Columns.Add("SendName", "发件人昵称");
                lvDetails.Columns.Add("SenderId", "发件人Id", 120);
                lvDetails.Columns.Add("SendTime", "发件时间", 150);
                lvDetails.Columns.Add("AttacherItem", "附件物品", 240);
                lvDetails.Columns.Add("AttacherAttr", "附件属性", 240);
                lvDetails.Columns.Add("Expired", "过期时间", 240);

                foreach (var mail in rsp.mails)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = mail.mailBodyId.ToString();
                    lvi.SubItems.Add(mail.title);
                    lvi.SubItems.Add(mail.senderName);
                    lvi.SubItems.Add(mail.sendId.ToString());
                    lvi.SubItems.Add(mail.sentTime.ToString());
                    lvi.SubItems.Add(GetMailItemAttach(mail.attacher));
                    lvi.SubItems.Add(GetMailAttrAttach(mail.attacher));
                    lvi.SubItems.Add(mail.expiredTime.ToString());
                    lvDetails.Items.Add(lvi);
                }

                lvDetails.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_MAIL_DELETE_PRESETSMAILLIST_RSP)
            {
                DeletePreSetsMailListRsp rsp = (DeletePreSetsMailListRsp)proto.GetBody();

                if (ErrorID.Success == rsp.errCode)
                {
                    if (lvDetails.SelectedItems.Count > 0)
                    {
                        lvDetails.Items.Remove(lvDetails.SelectedItems[0]);
                    }
                    else if (lvPlayers.Items.Count == 1)
                    {
                        lvDetails.Items.RemoveAt(0);
                    }
                }
                else
                {
                    MessageBox.Show("删除邮件出错了");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_STATISTICS_LOGLIST_RSP)
            {
                StatisticsLogListRsp rsp = (StatisticsLogListRsp)proto.GetBody();
                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("RoleId", "角色id", 120);
                lvDetails.Columns.Add("LogKey", "Key");
                lvDetails.Columns.Add("Content", "Content", 480);
                lvDetails.Columns.Add("LogTime", "Time", 150);

                foreach (var log in rsp.logs)
                {
                    string key = log.key;
                    string content = log.content;
                    ConvertLog(ref key, ref content);

                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = rsp.req.roleId.ToString();
                    lvi.SubItems.Add(key);
                    lvi.SubItems.Add(content);
                    lvi.SubItems.Add(log.time.ToString());
                    lvDetails.Items.Add(lvi);
                }
                lvDetails.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_SUBTITLE_QUERY_RSP)
            {
                QuerySubtitleRsp rsp = (QuerySubtitleRsp)proto.GetBody();
                subtitles.Clear();
                foreach (int key in rsp.SubtitleInfos.Keys)
                {
                    subtitles.Add(key, rsp.SubtitleInfos[key]);
                }

                ShowSubtitleInfo(subtitles, lvSbutitles);
            }
            else if (proto.GetID() == ProtoID.CMD_SUBTITLE_DELETE_RSP)
            {
                DeleteSubtitleRsp rsp = (DeleteSubtitleRsp)proto.GetBody();

                if (ErrorID.Success == rsp.ErrCode)
                {
                    ShowSubtitleInfo(subtitles, lvSbutitles);
                    MessageBox.Show("删除弹幕成功");
                }
                else
                {
                    MessageBox.Show("删除弹幕失败");
                }

            }
            else if (proto.GetID() == ProtoID.CMD_PUSH_RSP)
            {
                PushRsp rsp = (PushRsp)proto.GetBody();

                if (ErrorID.Success == rsp.ErrCode)
                {
                    MessageBox.Show("立即推送成功");
                }
                else
                {
                    MessageBox.Show("推送失败");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_PUSH_LOSEPLAYER_RSP)
            {
                PushToLoseUserRsp rsp = (PushToLoseUserRsp)proto.GetBody();

                if (ErrorID.Success == rsp.ErrCode)
                {
                    MessageBox.Show("推送流失玩家成功");
                }
                else
                {
                    MessageBox.Show("推送失败");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_GM_LISTACCOUNT_RSP)
            {
                GmListAccountRsp rsp = (GmListAccountRsp)proto.GetBody();
                lvAccounts.BeginUpdate();
                lvAccounts.Columns.Clear();
                lvAccounts.Items.Clear();

                lvAccounts.Columns.Add("AccountId", "帐号id");
                //lvAccounts.Columns.Add("Account", "帐号");
                lvAccounts.Columns.Add("RoleId", "角色id", 120);
                lvAccounts.Columns.Add("Role", "角色", 120);
                lvAccounts.Columns.Add("GmLevel", "gm等级");
                //lvAccounts.Columns.Add("CreateTime", "创建时间", 150);
                //lvAccounts.Columns.Add("LastLoginTime", "最后登录时间", 150);
                //lvAccounts.Columns.Add("Disable", "封停");
                //lvAccounts.Columns.Add("DisableDendline", "封停期限", 150);
                //lvAccounts.Columns.Add("Online", "状态");

                foreach (var account in rsp.accounts)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = account.Value.accountId.ToString();
                    //lvi.SubItems.Add("");
                    lvi.SubItems.Add(account.Value.roleId.ToString());
                    lvi.SubItems.Add(account.Value.role);
                    lvi.SubItems.Add(account.Value.gmLevel.ToString());
                    //lvi.SubItems.Add("");
                    //lvi.SubItems.Add("");
                    //lvi.SubItems.Add("");
                    //lvi.SubItems.Add("");
                    //lvi.SubItems.Add("");
                    lvAccounts.Items.Add(lvi);
                }
                lvAccounts.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_GM_CONFIGACCOUNT_RSP)
            {
                GmAccountConfigRsp rsp = (GmAccountConfigRsp)proto.GetBody();
                if (rsp.error == ErrorID.Success)
                {
                    MessageBox.Show("gm帐号配置成功");
                }
                else
                {
                    Error(rsp.error, string.Format("gm帐号配置失败: {0}", rsp.error.ToString()));
                }
            }
            else if (proto.GetID() == ProtoID.CMD_GM_REGISTERACCOUNT_RSP)
            {
                GmRegisterAccountRsp rsp = (GmRegisterAccountRsp)proto.GetBody();
                if (rsp.error == ErrorID.Success)
                {
                    MessageBox.Show("gm帐号注册成功");
                }
                else
                {
                    Error(rsp.error, string.Format("gm帐号注册失败: {0}", rsp.error.ToString()));
                }
            }
            else if (proto.GetID() == ProtoID.CMD_GM_REDEEMCODEROUNDLIST_RSP)
            {
                GmRedeemCodeRoundListRsp rsp = (GmRedeemCodeRoundListRsp)proto.GetBody();
                GenRedeemCode form = new GenRedeemCode();
                form.StartPosition = FormStartPosition.CenterParent;
                if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    int count = form.Count;
                    int bonus = form.Bonus;
                    int type = form.Type;
                    DateTime timeBegin = form.TimeBegin;
                    DateTime timeEnd = form.TimeEnd;
                    if (count <= 0 || bonus <= 0)
                    {
                        MessageBox.Show("输入参数不合法");
                        return;
                    }

                    int round = 0;
                    while (true)
                    {
                        round = ra.Next(100000, 999999);
                        if (!rsp.rounds.Contains(round))
                            break;
                    }

                    Dictionary<string, int> codes = new Dictionary<string, int>();
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < count; i++)
                    {
                        while (true)
                        {
                            sb.Clear();
                            sb.AppendFormat("{0:000000}", round);
                            for (int j = 0; j < 14; j++)
                            {
                                int num = ra.Next(0, 10);
                                sb.Append(num);
                            }
                            string code = sb.ToString();
                            if (!codes.ContainsKey(code))
                            {
                                codes[code] = 1;
                                break;
                            }
                        }
                    }

                    StringBuilder result = new StringBuilder();
                    DateTime now = DateTime.Now;
                    string report = string.Format(@"{0}\code-{1}{2:00}{3:00}-{4:00}{5:00}{6:00}{7:000}.txt",
                        System.Environment.CurrentDirectory,
                        now.Year, now.Month, now.Day,
                        now.Hour, now.Minute, now.Second, now.Millisecond);

                    {
                        FileStream fs = new FileStream(report, FileMode.Create);
                        StreamWriter sw = new StreamWriter(fs);
                        foreach (var code in codes)
                        {
                            sb.Clear();
                            sb.AppendFormat("{0}", code.Key);
                            sw.WriteLine(sb.ToString());
                        }
                        sw.Flush();
                        fs.Close();
                        result.AppendFormat("兑换码 {0} 已生成, ", report);
                    }

                    report = string.Format(@"{0}\code-{1}{2:00}{3:00}-{4:00}{5:00}{6:00}{7:000}.sql",
                        System.Environment.CurrentDirectory,
                        now.Year, now.Month, now.Day,
                        now.Hour, now.Minute, now.Second, now.Millisecond);

                    {
                        FileStream fs = new FileStream(report, FileMode.Create);
                        StreamWriter sw = new StreamWriter(fs);
                        sw.WriteLine("use otomeglobal;");
                        sw.WriteLine(string.Format("INSERT INTO redeem_code_round (round) VALUES ({0});", round));
                        foreach (var code in codes)
                        {
                            sb.Clear();
                            sb.Append("INSERT INTO redeem_code (code, bonus, type, begintime, endtime) VALUES (")
                                .AppendFormat("'{0}', ", code.Key)
                                .Append(bonus).Append(", ")
                                .Append(type).Append(", ")
                                .AppendFormat("'{0}', ", timeBegin)
                                .AppendFormat("'{0}');", timeEnd);
                            sw.WriteLine(sb.ToString());
                        }
                        sw.Flush();
                        fs.Close();
                        result.AppendFormat("请使用 {0} 导入", report);
                    }

                    Output(result.ToString());
                    MessageBox.Show(result.ToString());
                }
            }
            else if (proto.GetID() == ProtoID.CMD_GM_REDEEMCODEBONUSINFO_RSP)
            {
                GmRedeemCodeBonusInfoRsp rsp = (GmRedeemCodeBonusInfoRsp)proto.GetBody();
                int drawCnt = 0;
                int undrawCnt = 0;
                int timeoutCnt = 0;
                DateTime now = DateTime.Now;
                if (rsp.infos.Count > 0)
                {
                    foreach (var info in rsp.infos)
                    {
                        if (info.Value.isdraw == 1)
                        {
                            drawCnt++;
                        }
                        else
                        {
                            undrawCnt++;
                        }
                        if (now > info.Value.endtime)
                        {
                            timeoutCnt++;
                        }
                    }

                    MessageBox.Show(string.Format("兑换码总量{0}, 已兑换数量{1}, 未兑换数量{2}, 已过期数量{3}",
                        drawCnt + undrawCnt, drawCnt, undrawCnt, timeoutCnt));
                }
                else
                {
                    MessageBox.Show("未找到该礼品信息");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_GM_REDEEMCODEINFO_RSP)
            {
                GmRedeemCodeInfoRsp rsp = (GmRedeemCodeInfoRsp)proto.GetBody();
                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("Code", "兑换码", 160);
                lvDetails.Columns.Add("Bonus", "礼包id");
                lvDetails.Columns.Add("IsDraw", "已兑换");
                lvDetails.Columns.Add("RoleId", "兑换角色id", 120);
                lvDetails.Columns.Add("DrawTime", "兑换时间", 120);

                if (!string.IsNullOrEmpty(rsp.info.code))
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = rsp.info.code;
                    lvi.SubItems.Add(rsp.info.bonus.ToString());
                    lvi.SubItems.Add(rsp.info.isdraw.ToString());
                    if (rsp.info.isdraw == 1)
                    {
                        lvi.SubItems.Add(rsp.info.roleId.ToString());
                        lvi.SubItems.Add(rsp.info.drawtime.ToString());
                    }
                    else
                    {
                        lvi.SubItems.Add("");
                        lvi.SubItems.Add("");
                    }

                    lvDetails.Items.Add(lvi);
                }
                else
                {
                    MessageBox.Show("未找到该兑换码");
                }

                lvDetails.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_GM_REDEEMCODEROLEINFO_RSP)
            {
                GmRedeemCodeRoleInfoRsp rsp = (GmRedeemCodeRoleInfoRsp)proto.GetBody();
                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("Code", "兑换码", 160);
                lvDetails.Columns.Add("Bonus", "礼包id");
                lvDetails.Columns.Add("IsDraw", "已兑换");
                lvDetails.Columns.Add("RoleId", "兑换角色id", 120);
                lvDetails.Columns.Add("DrawTime", "兑换时间", 120);

                if (rsp.infos.Count > 0)
                {
                    foreach (var info in rsp.infos)
                    {
                        ListViewItem lvi = new ListViewItem();
                        lvi.Text = info.Value.code;
                        lvi.SubItems.Add(info.Value.bonus.ToString());
                        lvi.SubItems.Add(info.Value.isdraw.ToString());
                        if (info.Value.isdraw == 1)
                        {
                            lvi.SubItems.Add(info.Value.roleId.ToString());
                            lvi.SubItems.Add(info.Value.drawtime.ToString());
                        }
                        else
                        {
                            lvi.SubItems.Add("");
                            lvi.SubItems.Add("");
                        }

                        lvDetails.Items.Add(lvi);
                    }
                }
                else
                {
                    MessageBox.Show("未找到该角色的兑换信息");
                }

                lvDetails.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_GM_REDEEMCODETYPELIST_RSP)
            {
                GmRedeemCodeTypeListRsp rsp = (GmRedeemCodeTypeListRsp)proto.GetBody();
                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("Type", "兑换码类型", 160);

                foreach (int type in rsp.types)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = type.ToString();
                    lvDetails.Items.Add(lvi);
                }

                lvDetails.EndUpdate();
            }
            else if (proto.GetID() == ProtoID.CMD_GM_GETRECHARGEINFO_RSP)
            {
                GmGetRechargeInfoRsp rsp = (GmGetRechargeInfoRsp)proto.GetBody();

                if (rsp.ErrCode != ErrorID.Success)
                {
                    MessageBox.Show("查询失败");
                    return;
                }

                if (rsp.RechargeInfos.Count == 0)
                {
                    MessageBox.Show("未找到该角色的充值信息");
                    return;
                }

                lvDetails.BeginUpdate();
                lvDetails.Columns.Clear();
                lvDetails.Items.Clear();

                lvDetails.Columns.Add("OrderId", "订单号", 120);
                lvDetails.Columns.Add("Money", "人民币");
                lvDetails.Columns.Add("TradeTime", "充值时间", 120);

                foreach (var item in rsp.RechargeInfos)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = item.OrderId.ToString();
                    lvi.SubItems.Add(item.Money.ToString());
                    lvi.SubItems.Add(item.RechargeTime.ToString());

                    lvDetails.Items.Add(lvi);
                }

                lvDetails.EndUpdate();
            }
            else if (ProtoID.CMD_GET_TOKENDETAILS_RSP == proto.GetID())
            {
                GetRoleTokenDetailsRsp rsp = proto.GetBody() as GetRoleTokenDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询金钱流水出错了^_^");
                    return;
                }

                int curDiamondCount = 100;
                int curGoldCount = 10000;
                string gold = "金币";
                string diamond = "钻石";
                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                gmSearch.lvTokenLog.Columns.Add("ChangeValue", "改变值");
                gmSearch.lvTokenLog.Columns.Add("ChangeWay", "改变原因");
                gmSearch.lvTokenLog.Columns.Add("NowValue", "当前值");
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 120);

                foreach (var token in rsp.TokenDetails)
                {
                    StringBuilder sb = new StringBuilder();
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = token.RoleId.ToString();
                    lvItem.SubItems.Add(token.Count.ToString());

                    if (1 == token.TokenType)
                    {
                        if (1 == token.OpType)
                        {
                            curGoldCount += token.Count;
                            sb.AppendFormat("{0}{1}", gmSearch.GetGoldProductWay(token.Way), gold);
                        }
                        else if (2 == token.OpType)
                        {
                            curGoldCount -= token.Count;
                            sb.AppendFormat("{0}{1}", gmSearch.GetGoldConsumeWay(token.Way), gold);
                        }

                        lvItem.SubItems.Add(sb.ToString());
                        lvItem.SubItems.Add(curGoldCount.ToString());
                    }
                    else if (2 == token.TokenType)
                    {
                        if (1 == token.OpType)
                        {
                            curDiamondCount += token.Count;
                            sb.AppendFormat("{0}{1}", gmSearch.GetDiamondProductWay(token.Way), diamond);
                        }
                        else if (2 == token.OpType)
                        {
                            curDiamondCount -= token.Count;
                            sb.AppendFormat("{0}{1}", gmSearch.GetDiamondConsumeWay(token.Way), diamond);
                        }

                        lvItem.SubItems.Add(sb.ToString());
                        lvItem.SubItems.Add(curDiamondCount.ToString());
                    }

                    lvItem.SubItems.Add(token.Time.ToString());
                    gmSearch.lvTokenLog.Items.Add(lvItem);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_ITEMDETAILS_RSP == proto.GetID())
            {
                GetRoleItemDetailsRsp rsp = proto.GetBody() as GetRoleItemDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询物品流水出错了^_^");
                    return;
                }

                List<int> hasItems = new List<int>() { 72066, 69075, 70075, 68066, 67066 };
                Dictionary<int, bool> hasCalItems = new Dictionary<int, bool>();
                foreach (var item in hasItems)
                {
                    hasCalItems[item] = false;
                }

                int itemOldCount = 0;
                int itemNowCount = 0;
                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("Name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("ItemGetWay", "动作", 200);
                gmSearch.lvTokenLog.Columns.Add("ItemId", "物品id", 40);
                gmSearch.lvTokenLog.Columns.Add("ItemName", "物品名称", 40);
                gmSearch.lvTokenLog.Columns.Add("ItemOldCount", "旧物品数量", 50);
                gmSearch.lvTokenLog.Columns.Add("ItemNowCount", "新物品数量", 50);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 180);

                foreach (var item in rsp.ItemDetails)
                {
                    if (hasCalItems.ContainsKey(item.ItemId))
                    {
                        if (!hasCalItems[item.ItemId])
                        {
                            itemNowCount = 1;
                            hasCalItems[item.ItemId] = true;
                        }
                    }

                    itemOldCount = itemNowCount;
                    itemNowCount = itemOldCount + item.Count;

                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = item.RoleId.ToString();

                    //if (item.RoleId == roleId)
                    //{
                    //    lvItem.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvItem.SubItems.Add("-");
                    //}

                    lvItem.SubItems.Add(gmSearch.GetItemWay(item.Way));
                    lvItem.SubItems.Add(item.ItemId.ToString());
                    lvItem.SubItems.Add(ItemDatas[item.ItemId]);
                    lvItem.SubItems.Add(itemOldCount.ToString());
                    lvItem.SubItems.Add(itemNowCount.ToString());
                    lvItem.SubItems.Add(item.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvItem);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_LEVELGRADEDETAILS_RSP == proto.GetID())
            {
                GetRoleLevelGradeDetailsRsp rsp = proto.GetBody() as GetRoleLevelGradeDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询经验等级出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("LevelGrade", "角色等级");
                gmSearch.lvTokenLog.Columns.Add("ChangeValue", "经验变化值");
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 240);

                foreach (var levelGrade in rsp.LevelGradeDetails)
                {
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = levelGrade.RoleId.ToString();

                    //if (levelGrade.RoleId == roleId)
                    //{
                    //    lvItem.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvItem.SubItems.Add("-");
                    //}

                    lvItem.SubItems.Add(levelGrade.Grade.ToString());
                    lvItem.SubItems.Add(levelGrade.GradeExp.ToString());
                    lvItem.SubItems.Add(levelGrade.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvItem);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_VIPGRADEDETAILS_RSP == proto.GetID())
            {
                GetRoleVipGradeDetailsRsp rsp = proto.GetBody() as GetRoleVipGradeDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询VIP经验等级出错了^_^");
                    return;
                }

                string way = "充值";
                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("VipGrade", "VIP等级", 60);
                gmSearch.lvTokenLog.Columns.Add("VipGradeExp", "VIP等级经验", 80);
                gmSearch.lvTokenLog.Columns.Add("ChangeValue", "经验变化值", 80);
                gmSearch.lvTokenLog.Columns.Add("Way", "动作", 60);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 180);

                foreach (var vipGrade in rsp.VipGradeDetails)
                {
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = rsp.RoleId.ToString();
                    lvItem.SubItems.Add(name);
                    lvItem.SubItems.Add(vipGrade.Grade.ToString());
                    lvItem.SubItems.Add(vipGrade.GradeExp.ToString());
                    lvItem.SubItems.Add(vipGrade.ChangeExpCount.ToString());
                    lvItem.SubItems.Add(way);
                    lvItem.SubItems.Add(vipGrade.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvItem);
                }

                gmSearch.lvTokenLog.EndUpdate();

                gmSearch.lvVipGiftDetail.BeginUpdate();
                gmSearch.lvVipGiftDetail.Columns.Clear();
                gmSearch.lvVipGiftDetail.Items.Clear();

                gmSearch.lvVipGiftDetail.Columns.Add("VipGrade", "礼包等级", 60);
                gmSearch.lvVipGiftDetail.Columns.Add("IsDraw", "是否领取", 60);
                gmSearch.lvVipGiftDetail.Columns.Add("Time", "领取时间", 180);

                for (int i = 1; i < 11; i++)
                {
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = string.Format("VIP{0}", i);

                    if (rsp.GradeGifts.ContainsKey(i))
                    {
                        lvItem.SubItems.Add("是");
                        lvItem.SubItems.Add(rsp.GradeGifts[i].ToString());
                    }
                    else
                    {
                        lvItem.SubItems.Add("否");
                        lvItem.SubItems.Add("-");
                    }

                    gmSearch.lvVipGiftDetail.Items.Add(lvItem);
                }

                gmSearch.lvVipGiftDetail.EndUpdate();
            }
            else if (ProtoID.CMD_GET_SIGNINDETAILS_RSP == proto.GetID())
            {
                GetRoleSigninDetailsRsp rsp = proto.GetBody() as GetRoleSigninDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询签到记录出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("Dayth", "签到ID", 90);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 240);

                foreach (var signin in rsp.SigninDetails)
                {
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = signin.RoleId.ToString();
                    //if (signin.RoleId == roleId)
                    //{
                    //    lvItem.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvItem.SubItems.Add("-");
                    //}
                    lvItem.SubItems.Add(signin.Id.ToString());
                    lvItem.SubItems.Add(signin.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvItem);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_ACHIEVEDETAILS_RSP == proto.GetID())
            {
                GetRoleAchieveDetailsRsp rsp = proto.GetBody() as GetRoleAchieveDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询成就记录出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("AchiId", "成就id", 20);
                gmSearch.lvTokenLog.Columns.Add("AchiName", "成就名称", 200);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 240);

                foreach (var achieve in rsp.AchieveDetails)
                {
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = achieve.RoleId.ToString();
                    //if (achieve.RoleId == roleId)
                    //{
                    //    lvItem.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvItem.SubItems.Add("-");
                    //}
                    lvItem.SubItems.Add(achieve.Id.ToString());
                    lvItem.SubItems.Add(GetAchieveName(achieve.Id));
                    lvItem.SubItems.Add(achieve.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvItem);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_TASKDETAILS_RSP == proto.GetID())
            {
                GetRoleTaskDetailsRsp rsp = proto.GetBody() as GetRoleTaskDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询任务记录出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("TaskId", "任务id", 20);
                gmSearch.lvTokenLog.Columns.Add("TaskName", "任务名称", 200);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 240);

                foreach (var task in rsp.TaskDetails)
                {
                    ListViewItem lvItem = new ListViewItem();
                    lvItem.Text = task.RoleId.ToString();
                    //if (task.RoleId == roleId)
                    //{
                    //    lvItem.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvItem.SubItems.Add("-");
                    //}
                    lvItem.SubItems.Add(task.Id.ToString());
                    lvItem.SubItems.Add(GetTaskName(task.Id));
                    lvItem.SubItems.Add(task.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvItem);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_COMPETITIONDETAILS_RSP == proto.GetID())
            {
                GetRoleCompetitionDetailsRsp rsp = proto.GetBody() as GetRoleCompetitionDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询PK记录出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("Rank", "排名", 40);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 200);
                gmSearch.lvTokenLog.Columns.Add("Result", "胜负", 40);
                gmSearch.lvTokenLog.Columns.Add("OppoRoleId", "匹配玩家id", 120);
                gmSearch.lvTokenLog.Columns.Add("OppoRoleName", "匹配玩家名称", 200);

                foreach (var pk in rsp.PkDetails)
                {
                    string result = 1 == pk.Result ? "胜" : "负";

                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = rsp.RoleId.ToString();
                    lvi.SubItems.Add(name);
                    lvi.SubItems.Add(pk.Rank.ToString());
                    lvi.SubItems.Add(pk.Time.ToString());
                    lvi.SubItems.Add(result);
                    lvi.SubItems.Add(pk.OpponentId.ToString());
                    lvi.SubItems.Add(pk.OpponontName.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvi);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_MAKEDETAIL_RSP == proto.GetID())
            {
                GetRoleMakeDetailsRsp rsp = proto.GetBody() as GetRoleMakeDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询制作记录出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("ProductName", "成品名称", 40);
                gmSearch.lvTokenLog.Columns.Add("PictureId", "图纸id", 40);
                gmSearch.lvTokenLog.Columns.Add("Consume", "消耗材料明细", 200);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 240);

                foreach (var make in rsp.makeDetails)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = make.RoleId.ToString();
                    //if (make.RoleId == roleId)
                    //{
                    //    lvi.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvi.SubItems.Add("-");
                    //}
                    lvi.SubItems.Add(ItemDatas[make.ProductId]);
                    lvi.SubItems.Add(GetMakePicture(make.ProductId).ToString());
                    lvi.SubItems.Add(GetMakeConsume(make.ProductId));
                    lvi.SubItems.Add(make.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvi);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_DYEDETAIL_RSP == proto.GetID())
            {
                GetRoleDyeDetailsRsp rsp = proto.GetBody() as GetRoleDyeDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询染色记录出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("name", "角色名", 200);
                gmSearch.lvTokenLog.Columns.Add("ProductId", "获得服饰Id");
                gmSearch.lvTokenLog.Columns.Add("ProductName", "获得服饰名称");
                gmSearch.lvTokenLog.Columns.Add("RawDressId", "消耗服饰Id");
                gmSearch.lvTokenLog.Columns.Add("RawDressName", "消耗服饰名称");
                gmSearch.lvTokenLog.Columns.Add("DyeId", "消耗染料Id");
                gmSearch.lvTokenLog.Columns.Add("DyeCount", "消耗染料数量");
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 240);

                foreach (var dye in rsp.dyeDetails)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = dye.RoleId.ToString();
                    //if (dye.RoleId == roleId)
                    //{
                    //    lvi.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvi.SubItems.Add("-");
                    //}
                    lvi.SubItems.Add(dye.ProductId.ToString());
                    lvi.SubItems.Add(ItemDatas[dye.ProductId]);
                    lvi.SubItems.Add(dye.RawItemId.ToString());
                    lvi.SubItems.Add(ItemDatas[dye.RawItemId]);
                    lvi.SubItems.Add(dye.DyeId.ToString());
                    lvi.SubItems.Add(dye.DyeCount.ToString());
                    lvi.SubItems.Add(dye.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvi);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_LOGINDETAIL_RSP == proto.GetID())
            {
                GetRoleLoginDetailsRsp rsp = proto.GetBody() as GetRoleLoginDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询登录记录出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                gmSearch.lvTokenLog.Columns.Add("OpType", "操作", 40);
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 200);
                gmSearch.lvTokenLog.Columns.Add("Gold", "金币数量");
                gmSearch.lvTokenLog.Columns.Add("Diamond", "钻石数量");
                gmSearch.lvTokenLog.Columns.Add("Stamina", "体力数量");

                foreach (var login in rsp.LoginDetails)
                {
                    string op = 10 == login.OpType ? "登录" : "登出";

                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = login.RoleId.ToString();
                    lvi.SubItems.Add(op);
                    lvi.SubItems.Add(login.Time.ToString());
                    lvi.SubItems.Add(login.GoldCount.ToString());
                    lvi.SubItems.Add(login.DiamondCount.ToString());
                    lvi.SubItems.Add(login.StaminaCount.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvi);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GET_REALTIMELOGIN_RSP == proto.GetID())
            {
                GetRealTimeLoginCountRsp rsp = proto.GetBody() as GetRealTimeLoginCountRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询实时登录数据出错了^_^");
                    return;
                }

                gmSearch.lvRealTime.BeginUpdate();
                gmSearch.lvRealTime.Columns.Clear();
                gmSearch.lvRealTime.Items.Clear();

                gmSearch.lvRealTime.Columns.Add("AddUserCount", "实时新增人数", 80);
                gmSearch.lvRealTime.Columns.Add("OnlineUserCount", "实时活跃人数", 80);

                ListViewItem lvi = new ListViewItem();
                lvi.Text = rsp.AddCount.ToString();
                lvi.SubItems.Add(rsp.LoginCount.ToString());

                gmSearch.lvRealTime.Items.Add(lvi);
                gmSearch.lvRealTime.EndUpdate();
            }
            else if (ProtoID.CMD_GET_REALTIMERCINFO_RSP == proto.GetID())
            {
                GetRealTimeRCInfoRsp rsp = proto.GetBody() as GetRealTimeRCInfoRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询实时充值消费数据出错了^_^");
                    return;
                }

                gmSearch.lvRealTime.BeginUpdate();
                gmSearch.lvRealTime.Columns.Clear();
                gmSearch.lvRealTime.Items.Clear();

                gmSearch.lvRealTime.Columns.Add("NewPayUserCount", "实时新增充值人数", 100);
                gmSearch.lvRealTime.Columns.Add("Amount", "实时充值总额", 80);
                gmSearch.lvRealTime.Columns.Add("RealAmount", "实时实际充值额", 80);
                gmSearch.lvRealTime.Columns.Add("ConsumeUserCount", "实时消费人数", 80);
                gmSearch.lvRealTime.Columns.Add("ConsumeAmount", "实时消费额", 80);

                ListViewItem lvi = new ListViewItem();
                lvi.Text = rsp.AddRechargeUser.ToString();
                lvi.SubItems.Add(rsp.TotalRechargeCount.ToString());
                lvi.SubItems.Add(rsp.RechargeCount.ToString());
                lvi.SubItems.Add(rsp.ConsumeUser.ToString());
                lvi.SubItems.Add(rsp.ConsumeCount.ToString());

                gmSearch.lvRealTime.Items.Add(lvi);
                gmSearch.lvRealTime.EndUpdate();
            }
            else if (ProtoID.CMD_GET_ROLEACTIVITYEXCHANGE_RSP == proto.GetID())
            {
                GetRoleActivityExchangeDetailsRsp rsp = proto.GetBody() as GetRoleActivityExchangeDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询兑换活动兑换明细出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                gmSearch.lvTokenLog.Columns.Add("name", "活动类型", 200);
                gmSearch.lvTokenLog.Columns.Add("ItemId", "兑换的物品Id");
                gmSearch.lvTokenLog.Columns.Add("ItemName", "兑换的物品名称");
                gmSearch.lvTokenLog.Columns.Add("ItemCount", "兑换的物品数量");
                gmSearch.lvTokenLog.Columns.Add("Time", "时间", 240);

                foreach (var exchange in rsp.ExchangeDetails)
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = exchange.RoleId.ToString();
                  
                    if (EGetItemWay.LevelFallExchange == exchange.Way)
                    {
                        lvi.SubItems.Add("关卡掉落");
                    }
                    else if (EGetItemWay.SelectClothesExchange == exchange.Way)
                    {
                        lvi.SubItems.Add("搭配二选一");
                    }

                    lvi.SubItems.Add(exchange.ProductId.ToString());
                    lvi.SubItems.Add(ItemDatas[exchange.ProductId]);
                    lvi.SubItems.Add(exchange.ItemCount.ToString());
                    lvi.SubItems.Add(exchange.Time.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvi);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
            else if (ProtoID.CMD_GM_GETTASKNAME_RSP == proto.GetID())
            {
                GmGetTaskNameRsp rsp = proto.GetBody() as GmGetTaskNameRsp;
                QuestDatas = rsp.Id4Names;
            }
            else if (ProtoID.CMD_GM_GETACHIEVENAME_RSP == proto.GetID())
            {
                GmGetAchieveNameRsp rsp = proto.GetBody() as GmGetAchieveNameRsp;
                AchieveDatas = rsp.Id4Names;
            }
            else if (ProtoID.CMD_GM_GETMAKEDATA_RSP == proto.GetID())
            {
                GmGetMakeDataRsp rsp = proto.GetBody() as GmGetMakeDataRsp;
                MakeDatas = rsp.MakeDatas;
            }
            else if (ProtoID.CMD_GET_MALLPURCHASEDETAIL_RSP == proto.GetID())
            {
                GetMallConsumeDetailsRsp rsp = proto.GetBody() as GetMallConsumeDetailsRsp;

                if (ErrorID.Success != rsp.ErrCode)
                {
                    MessageBox.Show("查询兑换活动兑换明细出错了^_^");
                    return;
                }

                gmSearch.lvTokenLog.BeginUpdate();
                gmSearch.lvTokenLog.Columns.Clear();
                gmSearch.lvTokenLog.Items.Clear();

                gmSearch.lvTokenLog.Columns.Add("OrderId", "订单号", 80);
                gmSearch.lvTokenLog.Columns.Add("RoleId", "角色id", 120);
                //gmSearch.lvTokenLog.Columns.Add("Name", "角色名", 120);
                gmSearch.lvTokenLog.Columns.Add("TokenType", "消费类别", 60);
                gmSearch.lvTokenLog.Columns.Add("TokenCount", "消费数量", 60);
                gmSearch.lvTokenLog.Columns.Add("Time", "消费时间", 160);
                gmSearch.lvTokenLog.Columns.Add("Way", "消费途径", 160);
                gmSearch.lvTokenLog.Columns.Add("ItemId", "获得物品Id", 60);
                gmSearch.lvTokenLog.Columns.Add("ItemName", "获得物品名称", 60);
                gmSearch.lvTokenLog.Columns.Add("ItemCount", "获得物品数量", 60);

                foreach (var purchase in rsp.PurchaseRecords)
                {
                    string tokenType = 1 == purchase.TokenType ? "金币" : "钻石";
                    string itemType = string.Empty;
                   
                    if (1==purchase.ItemType)
                    {
                        itemType = "物品";
                    }
                    else if (2 == purchase.ItemType)
                    {
                        itemType = "材料";
                    }
                    else if (3 == purchase.ItemType)
                    {
                        itemType = "道具";
                    }

                    string way = string.Format("商城购买{0}消耗{1}", itemType, tokenType);

                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = purchase.OrderId.ToString();
                    lvi.SubItems.Add(purchase.RoleId.ToString());
                    //if (purchase.RoleId == roleId)
                    //{
                    //    lvi.SubItems.Add(name);
                    //}
                    //else
                    //{
                    //    lvi.SubItems.Add("-");
                    //}
                    lvi.SubItems.Add(tokenType);
                    lvi.SubItems.Add(purchase.TokenCount.ToString());
                    lvi.SubItems.Add(purchase.Time.ToString());
                    lvi.SubItems.Add(way);
                    lvi.SubItems.Add(purchase.ItemId.ToString());
                    lvi.SubItems.Add(ItemDatas[purchase.ItemId]);
                    lvi.SubItems.Add(purchase.Count.ToString());

                    gmSearch.lvTokenLog.Items.Add(lvi);
                }

                gmSearch.lvTokenLog.EndUpdate();
            }
        }


        /// <summary>
        /// 获取制作消耗
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public string GetMakeConsume(int productId)
        {
            StringBuilder sb = new StringBuilder();

            ManufactureMakeData makeData;
            MakeDatas.TryGetValue(productId, out makeData);

            if (null != makeData)
            {
                if (makeData.RawDressId1 > 0)
                {
                    sb.AppendFormat("({0},{1})", makeData.RawDressId1, makeData.Count1 - 1);
                }

                if (makeData.RawDressId2 > 0)
                {
                    sb.AppendFormat("({0},{1})", makeData.RawDressId2, makeData.Count2 - 1);
                }

                if (makeData.RawDressId3 > 0)
                {
                    sb.AppendFormat("({0},{1})", makeData.RawDressId3, makeData.Count3 - 1);
                }

                if (makeData.RawDressId4 > 0)
                {
                    sb.AppendFormat("({0},{1})", makeData.RawDressId4, makeData.Count4 - 1);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// 获取图纸id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public int GetMakePicture(int productId)
        {
            int pictureId = 0;

            if (MakeDatas.ContainsKey(productId))
            {
                pictureId = MakeDatas[productId].DrawingPaperId;
            }

            return pictureId;
        }

        /// <summary>
        /// 获取任务名称
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public string GetTaskName(int taskId)
        {
            string name = string.Empty;

            if (QuestDatas.ContainsKey(taskId))
            {
                name = QuestDatas[taskId];
            }

            return name;
        }

        /// <summary>
        /// 获取成就名称
        /// </summary>
        /// <param name="achieveId"></param>
        /// <returns></returns>
        public string GetAchieveName(int achieveId)
        {
            string name = string.Empty;

            if (AchieveDatas.ContainsKey(achieveId))
            {
                name = AchieveDatas[achieveId];
            }

            return name;
        }

        void ConvertLog(ref string key, ref string content)
        {
            if (key == "Login")
            {
                key = "帐号系统";
                string[] contents = content.Split('|');
                if (contents[0].Contains("Login"))
                {
                    content = "上线";
                }
                else if (contents[0].Contains("Logout"))
                {
                    content = "下线";
                }
                else if (contents[0].Contains("AddUser"))
                {
                    content = "创建角色";
                }
            }
            else if (key == "Register")
            {
                key = "帐号系统";
                content = "注册帐号";
            }
        }

        /// <summary>
        /// 获取邮件物品附件
        /// </summary>
        /// <param name="attacher"></param>
        /// <returns></returns>
        private string GetMailItemAttach(MailAttacherBody attacher)
        {
            StringBuilder itemAttacher = new StringBuilder();

            if (null != attacher.gameItems)
            {
                foreach (var item in attacher.gameItems)
                {
                    itemAttacher.AppendFormat("({0},{1})", item.gameItemId, item.amount);
                }
            }

            return itemAttacher.ToString();
        }

        /// <summary>
        /// 获取邮件属性附件
        /// </summary>
        /// <param name="attacher"></param>
        /// <returns></returns>
        private string GetMailAttrAttach(MailAttacherBody attacher)
        {
            StringBuilder attrAttacher = new StringBuilder();

            if (null != attacher.resources)
            {
                foreach (var attr in attacher.resources)
                {
                    attrAttacher.AppendFormat("({0},{1})", attr.type, attr.amount);
                }
            }

            return attrAttacher.ToString();
        }

        /// <summary>
        /// 显示弹幕信息
        /// </summary>
        /// <param name="subtitles"></param>
        /// <param name="lvSbutitles"></param>
        void ShowSubtitleInfo(Dictionary<int, SubtitleInfo> subtitles, ListView lvSbutitles)
        {
            lvSbutitles.BeginUpdate();
            lvSbutitles.Columns.Clear();
            lvSbutitles.Items.Clear();
            lvSbutitles.Columns.Add("ID", "编号");
            lvSbutitles.Columns.Add("ScenarioID", "章");
            lvSbutitles.Columns.Add("PartID", "节");
            lvSbutitles.Columns.Add("PassageID", "段");
            lvSbutitles.Columns.Add("Content", "弹幕内容", 360);
            lvSbutitles.Columns.Add("FontSize", "字体大小");
            lvSbutitles.Columns.Add("Color", "颜色");
            lvSbutitles.Columns.Add("RoleId", "角色id", 120);

            foreach (int key in subtitles.Keys)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = key.ToString();
                lvi.SubItems.Add(subtitles[key].ScenarioID.ToString());
                lvi.SubItems.Add(subtitles[key].PartID.ToString());
                lvi.SubItems.Add(subtitles[key].PassageID.ToString());
                lvi.SubItems.Add(subtitles[key].Subtitle);
                lvi.SubItems.Add(FontSizeToString(subtitles[key].FontSize));
                lvi.SubItems.Add(ColorToString(subtitles[key].Color));
                lvi.SubItems.Add(subtitles[key].RoleID.ToString());

                lvSbutitles.Items.Add(lvi);
            }

            lvSbutitles.EndUpdate();
        }

        /// <summary>
        /// 字体枚举转换成汉字
        /// </summary>
        /// <param name="fontsize"></param>
        /// <returns></returns>
        string FontSizeToString(SubtitleFontSize fontsize)
        {
            string ret = "";
            if (SubtitleFontSize.small == fontsize)
            {
                ret = "小";
            }
            else if (SubtitleFontSize.middle == fontsize)
            {
                ret = "中";
            }
            else if (SubtitleFontSize.big == fontsize)
            {
                ret = "大";
            }
            return ret;
        }

        /// <summary>
        /// 颜色枚举转换成汉字
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        string ColorToString(SubtitleColor color)
        {
            string ret = "";
            if (SubtitleColor.red == color)
            {
                ret = "红色";
            }
            else if (SubtitleColor.yellow == color)
            {
                ret = "黄色";
            }
            else if (SubtitleColor.green == color)
            {
                ret = "绿色";
            }
            else if (SubtitleColor.blue == color)
            {
                ret = "蓝色";
            }
            else if (SubtitleColor.purple == color)
            {
                ret = "紫色";
            }
            else if (SubtitleColor.white == color)
            {
                ret = "白色";
            }
            else if (SubtitleColor.blue == color)
            {
                ret = "黑色";
            }

            return ret;
        }

        void SendMessage(ProtoBody body)
        {
            Protocol proto = new Protocol(body);
            Output(string.Format("Server <------ {0}", proto.GetID().ToString()));
            session.SendMessage(proto);
        }

        void SendMessage(Protocol proto)
        {
            Output(string.Format("Server <------ {0}", proto.GetID().ToString()));
            session.SendMessage(proto);
        }

        void Output(string line)
        {
            line += "\n";
            TBOutput.AppendText(line);
        }

        void Error(ErrorID error, string line)
        {
            line += "\n";
            tbError.AppendText(line);

            if (error != ErrorID.Success)
            {
                MessageBox.Show(line, "Error");
            }
        }

        void SelectedIndexChanged(ListView lv)
        {
            foreach (ListViewItem item in lv.Items)
            {
                item.BackColor = SystemColors.Window;
                item.ForeColor = Color.Black;
            }

            foreach (ListViewItem item in lv.SelectedItems)
            {
                item.BackColor = SystemColors.MenuHighlight;
                item.ForeColor = Color.White;
            }
        }

        void CallItemEditForm()
        {
            if (lvDetails.SelectedItems.Count > 0)
            {
                ListViewItem item = lvDetails.SelectedItems[0];
                if (lvDetails.Columns.ContainsKey("ItemId")
                    && lvDetails.Columns.ContainsKey("ItemName")
                    && lvDetails.Columns.ContainsKey("ItemCount"))
                {
                    int index = lvDetails.Columns["ItemId"].Index;
                    int itemId = int.Parse(item.SubItems[index].Text);
                    index = lvDetails.Columns["ItemName"].Index;
                    string itemName = item.SubItems[index].Text;
                    index = lvDetails.Columns["ItemCount"].Index;
                    int itemCount = int.Parse(item.SubItems[index].Text);

                    ItemEdit form = new ItemEdit(itemId, itemName, itemCount);
                    form.StartPosition = FormStartPosition.CenterParent;
                    if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        item.SubItems[index].Text = form.ItemCount.ToString();
                    }
                }
            }
        }

        void DisplayMailInfo()
        {
            if (lvDetails.SelectedItems.Count > 0)
            {
                ListViewItem item = lvDetails.SelectedItems[0];
                if (lvDetails.Columns.ContainsKey("MailId") && lvDetails.Columns.ContainsKey("BodyId"))
                {
                    int index = lvDetails.Columns["MailId"].Index;
                    long mailId = long.Parse(item.SubItems[index].Text);
                    index = lvDetails.Columns["BodyId"].Index;
                    long bodyId = long.Parse(item.SubItems[index].Text);
                    MailTag tag = new MailTag(mailId, bodyId);
                    if (mails.ContainsKey(tag))
                    {
                        lvAccessories.Items.Clear();
                        var mail = mails[tag];
                        tbMailTitle.Text = mail.title;
                        tbMailContent.Text = mail.content;
                        if (mail.hasAttacher && mail.attacher != null)
                        {
                            foreach (var it in mail.attacher.gameItems)
                            {
                                ListViewItem lvi = new ListViewItem();
                                lvi.Text = it.gameItemId.ToString();
                                lvi.SubItems.Add(ItemDatas[it.gameItemId]);
                                lvi.SubItems.Add("");
                                lvi.SubItems.Add(it.amount.ToString());
                                lvAccessories.Items.Add(lvi);
                            }

                            foreach (var it in mail.attacher.resources)
                            {
                                ListViewItem lvi = new ListViewItem();
                                lvi.Text = "";
                                lvi.SubItems.Add("");
                                lvi.SubItems.Add(it.type);
                                lvi.SubItems.Add(it.amount.ToString());
                                lvAccessories.Items.Add(lvi);
                            }
                        }
                    }
                }
            }
        }

        void CallItemAddForm(UInt64 roleId)
        {
            ItemAdd form = new ItemAdd();
            form.StartPosition = FormStartPosition.CenterParent;
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK
                && form.ItemId > 0 && form.Count > 0)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = roleId.ToString();
                lvi.SubItems.Add(form.ItemId.ToString());
                lvi.SubItems.Add(ItemDatas[form.ItemId]);
                lvi.SubItems.Add(form.Count.ToString());
                lvDetails.Items.Add(lvi);
            }
        }

        void CallAccessoryEditForm(bool addNew)
        {
            if (addNew)
            {
                AccessoryEdit form = new AccessoryEdit(0, "", 0);
                form.StartPosition = FormStartPosition.CenterParent;
                if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    if (form.Count <= 0)
                    {
                        MessageBox.Show("数量必须大于零", "Error");
                        return;
                    }

                    int itemId = form.ItemId;
                    if (itemId > 0)
                    {
                        ListViewItem lvi = new ListViewItem();
                        lvi.Text = itemId.ToString();
                        lvi.SubItems.Add(ItemDatas[itemId]);
                        lvi.SubItems.Add("");
                        lvi.SubItems.Add(form.Count.ToString());
                        lvAccessories.Items.Add(lvi);
                    }
                    else
                    {
                        string attr = form.Attr;
                        if (!string.IsNullOrEmpty(attr))
                        {
                            ListViewItem lvi = new ListViewItem();
                            lvi.Text = "";
                            lvi.SubItems.Add("");
                            lvi.SubItems.Add(attr);
                            lvi.SubItems.Add(form.Count.ToString());
                            lvAccessories.Items.Add(lvi);
                        }
                    }
                }
            }
            else
            {
                if (lvAccessories.SelectedItems.Count > 0)
                {
                    ListViewItem item = lvAccessories.SelectedItems[0];
                    int index = lvAccessories.Columns["ItemId"].Index;
                    int itemId = string.IsNullOrEmpty(item.SubItems[index].Text) ? 0 : int.Parse(item.SubItems[index].Text);
                    index = lvAccessories.Columns["Attr"].Index;
                    string attr = item.SubItems[index].Text;
                    index = lvAccessories.Columns["Count"].Index;
                    int count = int.Parse(item.SubItems[index].Text);

                    AccessoryEdit form = new AccessoryEdit(itemId, attr, count);
                    form.StartPosition = FormStartPosition.CenterParent;
                    if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        if (form.Count < 0)
                        {
                            MessageBox.Show("数量不能小于零", "Error");
                            return;
                        }

                        if (form.Count == 0)
                        {
                            lvAccessories.Items.Remove(item);
                        }
                        else
                        {
                            item.SubItems[lvAccessories.Columns["ItemId"].Index].Text = form.ItemId == 0 ? "" : form.ItemId.ToString();
                            item.SubItems[lvAccessories.Columns["ItemName"].Index].Text = form.ItemId == 0 ? "" : ItemDatas[form.ItemId];
                            item.SubItems[lvAccessories.Columns["Attr"].Index].Text = form.Attr;
                            item.SubItems[lvAccessories.Columns["Count"].Index].Text = form.Count.ToString();
                        }
                    }
                }
            }
        }

        void ListRoleInfo()
        {
            ListViewItem item = null;
            if (lvAccounts.SelectedItems.Count > 0)
            {
                item = lvAccounts.SelectedItems[0];
            }
            else if (lvAccounts.Items.Count == 1)
            {
                item = lvAccounts.Items[0];
            }

            if (item != null)
            {
                if (lvAccounts.Columns.ContainsKey("RoleId"))
                {
                    int index = lvAccounts.Columns["RoleId"].Index;
                    GmGetPlayerInfoReq req = new GmGetPlayerInfoReq();
                    req.roleId = UInt64.Parse(item.SubItems[index].Text);
                    SendMessage(req);
                }
            }
        }


        void SendMail(List<UInt64> ids, int broadcastType)
        {
            if (string.IsNullOrEmpty(tbMailTitle.Text) || tbMailTitle.Text == "邮件标题"
                || string.IsNullOrEmpty(tbMailContent.Text) || tbMailContent.Text == "邮件内容")
                return;

            //限制长度
            if (tbMailTitle.Text.Length > MailTitleMaxLength)
            {
                MessageBox.Show(string.Format("邮件标题应小于{0}个字", MailTitleMaxLength), "Error");
                return;
            }

            if (tbMailContent.Text.Length > MailContentMaxLength)
            {
                MessageBox.Show(string.Format("邮件内容应小于{0}个字", MailContentMaxLength), "Error");
                return;
            }

            GmSendSystemMailReq req = new GmSendSystemMailReq();
            req.senderName = "谢尼";
            req.title = tbMailTitle.Text;
            req.contents = tbMailContent.Text;
            req.broadcastType = broadcastType;

            if (lvAccessories.Items.Count > 0)
            {
                List<MailAttacherGameItemBody> items = new List<MailAttacherGameItemBody>();
                List<MailAttacherResourceBody> resources = new List<MailAttacherResourceBody>();
                foreach (ListViewItem item in lvAccessories.Items)
                {
                    int index = lvAccessories.Columns["ItemId"].Index;
                    if (!string.IsNullOrEmpty(item.SubItems[index].Text))
                    {
                        MailAttacherGameItemBody itemBody = new MailAttacherGameItemBody();
                        itemBody.gameItemId = int.Parse(item.SubItems[index].Text);
                        index = lvAccessories.Columns["ItemName"].Index;
                        itemBody.gameItemName = item.SubItems[index].Text;
                        index = lvAccessories.Columns["Count"].Index;
                        itemBody.amount = int.Parse(item.SubItems[index].Text);
                        items.Add(itemBody);
                    }
                    else
                    {
                        index = lvAccessories.Columns["Attr"].Index;
                        if (!string.IsNullOrEmpty(item.SubItems[index].Text))
                        {
                            MailAttacherResourceBody resource = new MailAttacherResourceBody();
                            resource.type = item.SubItems[index].Text;
                            index = lvAccessories.Columns["Count"].Index;
                            resource.amount = int.Parse(item.SubItems[index].Text);
                            resources.Add(resource);
                        }
                    }
                }

                MailAttacherBody attacher = new MailAttacherBody();
                attacher.gameItems = items.ToArray();
                attacher.resources = resources.ToArray();
                req.attacher = attacher;
            }

            if (ids != null)
            {
                req.mailItemType = 2;
                req.receiverIds = ids.ToArray();
            }
            else
            {
                req.mailItemType = 100;
            }

            req.sendTime = DateTime.Now;
            if (!string.IsNullOrEmpty(txtMailSendTime.Text) && "定时发送时间 0001-01-01 0:00:00" != txtMailSendTime.Text)
            {
                DateTime temp;
                if (DateTime.TryParse(txtMailSendTime.Text, out temp))
                {
                    req.sendTime = temp;
                }
            }

            if (!string.IsNullOrEmpty(txtBoxMailExpri.Text) && "邮件过期时间 0001-01-01 0:00:00" != txtBoxMailExpri.Text)
            {
                DateTime temp;
                if (DateTime.TryParse(txtBoxMailExpri.Text, out temp))
                {
                    req.expirationTime = temp;
                }
            }

            SendMessage(req);

            MessageBox.Show("邮件发送成功");
        }

        //////////////////////////////////////////////////////////////////////////
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            BtnLogin.Enabled = false;

            if (!string.IsNullOrEmpty(comboBoxServer.Text))
            {
                string key = comboBoxServer.Text;
                StringBuilder sb = new StringBuilder(255);
                GetPrivateProfileString("Server", key, "", sb, 255, ".\\GmClient.ini");
                string serverIp = sb.ToString();
                string[] ips = serverIp.Split(':');
                IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(ips[0]), int.Parse(ips[1]));
                session.Connect(endpoint);
            }

            SaveData();
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            session.Disconnect();
        }

        private void GmClient_Load(object sender, EventArgs e)
        {

        }

        private void BtnSystemPost_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TBSystemPost.Text) || TBSystemPost.Text == "公告内容")
                return;

            if (TBSystemPost.Text.Length > SystemPostMaxLength)
            {
                MessageBox.Show(string.Format("系统公告应小于{0}个字", SystemPostMaxLength), "Error");
                return;
            }

            if (MessageBox.Show("是否发送系统公告？", "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                GmSystemPostReq req = new GmSystemPostReq();
                req.roleId = 0;
                req.post.type = MessageType.System;
                if (!string.IsNullOrEmpty(textBoxPlatform.Text))
                {
                    req.platform = int.Parse(textBoxPlatform.Text);
                }
                if (string.IsNullOrEmpty(tbSysPostStartTime.Text))
                {
                    req.post.timeStart = DateTime.MinValue;
                }
                else
                {
                    req.post.timeStart = DateTime.Parse(tbSysPostStartTime.Text);
                }
                if (string.IsNullOrEmpty(tbSysPostEndTime.Text))
                {
                    req.post.timeEnd = DateTime.MinValue;
                }
                else
                {
                    req.post.timeEnd = DateTime.Parse(tbSysPostEndTime.Text);
                }
                if (!string.IsNullOrEmpty(tbInterval.Text))
                {
                    req.post.interval = 1000 * int.Parse(tbInterval.Text);
                }
                MessageInfo content = new MessageInfo();
                content.size = 26;
                content.message = TBSystemPost.Text;
                req.post.contents.Add(content);
                SendMessage(req);
            }
        }

        private void btnRevert_Click(object sender, EventArgs e)
        {
            if (lvAccounts.SelectedItems.Count > 0)
            {
                if (lvAccounts.Columns.ContainsKey("Guid"))
                {
                    if (MessageBox.Show("是否撤销该公告？", "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        int index = lvAccounts.Columns["Guid"].Index;
                        Guid guid = new Guid(lvAccounts.SelectedItems[0].SubItems[index].Text);
                        GmSystemPostRevertReq req = new GmSystemPostRevertReq();
                        req.guid = guid;
                        SendMessage(req);
                    }
                }
            }
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            GmSystemPostListReq req = new GmSystemPostListReq();
            SendMessage(req);
        }

        private void tbAccountKey_MouseClick(object sender, MouseEventArgs e)
        {
            if (tbAccountKey.Text == "帐号")
            {
                tbAccountKey.Clear();
            }
        }

        private void tbPlayerKey_MouseClick(object sender, MouseEventArgs e)
        {
            if (tbPlayerKey.Text == "角色")
            {
                tbPlayerKey.Clear();
            }
        }

        private void tbActIdKey_MouseClick(object sender, MouseEventArgs e)
        {
            if (tbActIdKey.Text == "帐号id")
            {
                tbActIdKey.Clear();
            }
        }

        private void tbPlayerIdKey_MouseClick(object sender, MouseEventArgs e)
        {
            if (tbPlayerIdKey.Text == "角色id")
            {
                tbPlayerIdKey.Clear();
            }
        }

        private void btnSearchAccount_Click(object sender, EventArgs e)
        {
            GmGetAccountInfoReq req = new GmGetAccountInfoReq();
            if (!string.IsNullOrEmpty(tbAccountKey.Text) && tbAccountKey.Text != "帐号")
            {
                req.account = tbAccountKey.Text;
            }
            if (!string.IsNullOrEmpty(tbPlayerKey.Text) && tbPlayerKey.Text != "角色")
            {
                req.role = tbPlayerKey.Text;
            }
            if (!string.IsNullOrEmpty(tbActIdKey.Text) && tbActIdKey.Text != "帐号id")
            {
                req.accountId = UInt64.Parse(tbActIdKey.Text);
            }
            if (!string.IsNullOrEmpty(tbPlayerIdKey.Text) && tbPlayerIdKey.Text != "角色id")
            {
                req.roleId = UInt64.Parse(tbPlayerIdKey.Text);
            }
            SendMessage(req);
        }

        private void btnSearchPlayer_Click(object sender, EventArgs e)
        {
            ListRoleInfo();
        }

        private void tbAccountKey_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbAccountKey.Text))
            {
                tbAccountKey.Text = "帐号";
            }
        }

        private void tbPlayerKey_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbPlayerKey.Text))
            {
                tbPlayerKey.Text = "角色";
            }
        }

        private void tbActIdKey_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbActIdKey.Text))
            {
                tbActIdKey.Text = "帐号id";
            }
        }

        private void tbPlayerIdKey_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbPlayerIdKey.Text))
            {
                tbPlayerIdKey.Text = "角色id";
            }
        }

        private void btnItemList_Click(object sender, EventArgs e)
        {
            ListViewItem item = null;
            if (lvPlayers.SelectedItems.Count > 0)
            {
                item = lvPlayers.SelectedItems[0];
            }
            else if (lvPlayers.Items.Count == 1)
            {
                item = lvPlayers.Items[0];
            }

            if (item != null)
            {
                if (lvPlayers.Columns.ContainsKey("RoleId"))
                {
                    int index = lvPlayers.Columns["RoleId"].Index;
                    GmItemListReq req = new GmItemListReq();
                    req.roleId = UInt64.Parse(item.SubItems[index].Text);
                    SendMessage(req);
                }
            }
        }

        private void btnMailList_Click(object sender, EventArgs e)
        {
            ListViewItem item = null;
            if (lvPlayers.SelectedItems.Count > 0)
            {
                item = lvPlayers.SelectedItems[0];
            }
            else if (lvPlayers.Items.Count == 1)
            {
                item = lvPlayers.Items[0];
            }

            if (item != null)
            {
                if (lvPlayers.Columns.ContainsKey("RoleId"))
                {
                    int index = lvPlayers.Columns["RoleId"].Index;
                    GmMailListReq req = new GmMailListReq();
                    req.roleId = UInt64.Parse(item.SubItems[index].Text);
                    SendMessage(req);
                }
            }
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            if (lvAccounts.SelectedItems.Count > 0)
            {
                ListViewItem item = lvAccounts.SelectedItems[0];
                if (lvAccounts.Columns.ContainsKey("AccountId"))
                {
                    if (MessageBox.Show(string.Format("是否将帐号: {0} id: {1} 封停？",
                        item.SubItems[lvAccounts.Columns["Account"].Index].Text,
                        item.SubItems[lvAccounts.Columns["AccountId"].Index].Text),
                        "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        int index = lvAccounts.Columns["AccountId"].Index;
                        GmLockAccountReq req = new GmLockAccountReq();
                        req.accountId = UInt64.Parse(item.SubItems[index].Text);
                        if (string.IsNullOrEmpty(tbDisableDendline.Text))
                        {
                            req.dendline = DateTime.MaxValue;
                        }
                        else
                        {
                            req.dendline = DateTime.Parse(tbDisableDendline.Text);
                        }
                        SendMessage(req);
                    }
                }
            }
        }

        private void btnUnlock_Click(object sender, EventArgs e)
        {
            if (lvAccounts.SelectedItems.Count > 0)
            {
                ListViewItem item = lvAccounts.SelectedItems[0];
                if (lvAccounts.Columns.ContainsKey("AccountId"))
                {
                    if (MessageBox.Show(string.Format("是否将帐号: {0} id: {1} 解封？",
                        item.SubItems[lvAccounts.Columns["Account"].Index].Text,
                        item.SubItems[lvAccounts.Columns["AccountId"].Index].Text),
                        "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        int index = lvAccounts.Columns["AccountId"].Index;
                        GmUnlockAccountReq req = new GmUnlockAccountReq();
                        req.accountId = UInt64.Parse(item.SubItems[index].Text);
                        SendMessage(req);
                    }
                }
            }
        }

        private void btnKick_Click(object sender, EventArgs e)
        {
            ListViewItem item = null;
            ListView lv = null;
            if (lvAccounts.SelectedItems.Count > 0)
            {
                item = lvAccounts.SelectedItems[0];
                lv = lvAccounts;
            }
            else if (lvPlayers.SelectedItems.Count > 0)
            {
                item = lvPlayers.SelectedItems[0];
                lv = lvPlayers;
            }

            if (item != null && lv != null)
            {
                if (lv.Columns.ContainsKey("RoleId"))
                {
                    if (MessageBox.Show(string.Format("是否将角色: {0} id: {1} 踢下线？",
                        item.SubItems[lv.Columns["Role"].Index].Text,
                        item.SubItems[lv.Columns["RoleId"].Index].Text),
                        "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        int index = lv.Columns["RoleId"].Index;
                        GmKickRoleReq req = new GmKickRoleReq();
                        req.roleId = UInt64.Parse(item.SubItems[index].Text);
                        SendMessage(req);
                    }
                }
            }
        }

        private void btnChangeName_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbChangeName.Text))
                return;

            ListViewItem item = null;
            ListView lv = null;
            if (lvAccounts.SelectedItems.Count > 0)
            {
                item = lvAccounts.SelectedItems[0];
                lv = lvAccounts;
            }
            else if (lvPlayers.SelectedItems.Count > 0)
            {
                item = lvPlayers.SelectedItems[0];
                lv = lvPlayers;
            }

            if (item != null && lv != null)
            {
                if (lv.Columns.ContainsKey("RoleId"))
                {
                    if (MessageBox.Show(string.Format("是否将角色: {0} id: {1} 改名为 {2}？",
                        item.SubItems[lv.Columns["Role"].Index].Text,
                        item.SubItems[lv.Columns["RoleId"].Index].Text,
                        tbChangeName.Text), "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        int index = lv.Columns["RoleId"].Index;
                        GmChangeNameReq req = new GmChangeNameReq();
                        req.roleId = UInt64.Parse(item.SubItems[index].Text);
                        req.name = tbChangeName.Text;
                        SendMessage(req);
                    }
                }
            }
        }

        private void lvAccounts_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedIndexChanged(lvAccounts);
        }

        private void lvPlayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedIndexChanged(lvPlayers);
        }

        private void lvDetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedIndexChanged(lvDetails);
        }

        private void btnItemEdit_Click(object sender, EventArgs e)
        {
            CallItemEditForm();
        }

        private void lvDetails_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lvDetails.Columns.ContainsKey("ItemId")
                && lvDetails.Columns.ContainsKey("ItemName")
                && lvDetails.Columns.ContainsKey("ItemCount"))
            {
                CallItemEditForm();
            }
            else if (lvDetails.Columns.ContainsKey("MailId"))
            {
                DisplayMailInfo();
            }
        }

        private void btnItemSubmit_Click(object sender, EventArgs e)
        {
            if (lvDetails.Items.Count > 0 && lvDetails.Columns.ContainsKey("RoleId")
                && lvDetails.Columns.ContainsKey("ItemId") && lvDetails.Columns.ContainsKey("ItemCount"))
            {
                if (MessageBox.Show(string.Format("是否将角色id: {0} 编辑的道具提交？",
                    lvDetails.Items[0].SubItems[lvDetails.Columns["RoleId"].Index].Text),
                    "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    GmItemEditReq req = new GmItemEditReq();
                    foreach (ListViewItem it in lvDetails.Items)
                    {
                        ItemInfo item = new ItemInfo();
                        int index = lvDetails.Columns["RoleId"].Index;
                        req.roleId = UInt64.Parse(it.SubItems[index].Text);
                        index = lvDetails.Columns["ItemId"].Index;
                        item.id = int.Parse(it.SubItems[index].Text);
                        index = lvDetails.Columns["ItemCount"].Index;
                        item.count = int.Parse(it.SubItems[index].Text);
                        req.items.Add(item);
                    }
                    SendMessage(req);
                }
            }
        }

        private void TBSystemPost_MouseClick(object sender, MouseEventArgs e)
        {
            if (TBSystemPost.Text == "公告内容")
            {
                TBSystemPost.Clear();
            }
        }

        private void TBSystemPost_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TBSystemPost.Text))
            {
                TBSystemPost.Text = "公告内容";
            }
        }

        private void tbMailTitle_MouseClick(object sender, MouseEventArgs e)
        {
            if (tbMailTitle.Text == "邮件标题")
            {
                tbMailTitle.Clear();
            }
        }

        private void tbMailTitle_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbMailTitle.Text))
            {
                tbMailTitle.Text = "邮件标题";
            }
        }

        private void tbMailContent_MouseClick(object sender, MouseEventArgs e)
        {
            if (tbMailContent.Text == "邮件内容")
            {
                tbMailContent.Clear();
            }
        }

        private void tbMailContent_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbMailContent.Text))
            {
                tbMailContent.Text = "邮件内容";
            }
        }

        private void lvAccounts_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lvAccounts.Columns.ContainsKey("RoleId"))
            {
                ListRoleInfo();
            }
        }

        private void btnSendMail_Click(object sender, EventArgs e)
        {
            if (lvMailReceiver.Items.Count > 0)
            {
                if (lvMailReceiver.Columns.ContainsKey("RoleId"))
                {
                    if (MessageBox.Show("是否发送系统邮件？", "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        int index = lvMailReceiver.Columns["RoleId"].Index;
                        List<UInt64> ids = new List<ulong>();
                        foreach (ListViewItem item in lvMailReceiver.Items)
                        {
                            UInt64 roleId = UInt64.Parse(item.SubItems[index].Text);
                            ids.Add(roleId);
                        }
                        SendMail(ids, 0);
                    }
                }
            }
        }

        private void btnBroadcastMail_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否发送系统邮件？", "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                SendMail(null, 0);
            }
        }

        private void btnAccessory_Click(object sender, EventArgs e)
        {
            CallAccessoryEditForm(true);
        }

        private void lvAccessories_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            CallAccessoryEditForm(false);
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            ListViewItem item = null;
            if (lvPlayers.SelectedItems.Count > 0)
            {
                item = lvPlayers.SelectedItems[0];
            }
            else if (lvPlayers.Items.Count == 1)
            {
                item = lvPlayers.Items[0];
            }

            if (item != null)
            {
                if (lvPlayers.Columns.ContainsKey("RoleId"))
                {
                    int index = lvPlayers.Columns["RoleId"].Index;
                    GmLogListReq req = new GmLogListReq();
                    req.roleId = UInt64.Parse(item.SubItems[index].Text);
                    req.begin = DateTime.Parse(tbLogStart.Text);
                    req.end = DateTime.Parse(tbLogEnd.Text);
                    SendMessage(req);
                }
            }
        }

        private void btnReceiver_Click(object sender, EventArgs e)
        {
            if (lvPlayers.SelectedItems.Count > 0)
            {
                ListViewItem item = lvPlayers.SelectedItems[0];
                if (lvPlayers.Columns.ContainsKey("RoleId") && lvPlayers.Columns.ContainsKey("Role"))
                {
                    int index = lvPlayers.Columns["RoleId"].Index;
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = item.SubItems[index].Text;
                    //index = lvPlayers.Columns["Role"].Index;
                    //lvi.SubItems.Add(item.SubItems[index].Text);
                    lvi.SubItems.Add("0");
                    lvMailReceiver.Items.Add(lvi);
                }
            }
        }

        private void lvAccessories_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (lvAccessories.SelectedItems.Count > 0)
                {
                    ListViewItem item = lvAccessories.SelectedItems[0];
                    lvAccessories.Items.Remove(item);
                }
            }
        }

        private void btnItemAdd_Click(object sender, EventArgs e)
        {
            ListViewItem item = null;
            if (lvPlayers.SelectedItems.Count > 0)
            {
                item = lvPlayers.SelectedItems[0];
            }
            else if (lvPlayers.Items.Count == 1)
            {
                item = lvPlayers.Items[0];
            }

            if (item != null)
            {
                if (lvPlayers.Columns.ContainsKey("RoleId"))
                {
                    int index = lvPlayers.Columns["RoleId"].Index;
                    CallItemAddForm(UInt64.Parse(item.SubItems[index].Text));
                }
            }
        }

        /// <summary>
        /// 查询弹幕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuerySub_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSubScenarioId.Text) || "剧情-章" == TxtSubScenarioId.Text ||
                string.IsNullOrEmpty(TxtSubPartId.Text) || "剧情-节" == TxtSubPartId.Text ||
                string.IsNullOrEmpty(TxtSubPassageId.Text) || "剧情-段" == TxtSubPassageId.Text)
            {
                return;
            }

            GMQuerySubtitleReq req = new GMQuerySubtitleReq();
            req.ScenarioID = Int32.Parse(TxtSubScenarioId.Text);
            req.PartID = UInt16.Parse(TxtSubPartId.Text);
            req.PassageID = UInt16.Parse(TxtSubPassageId.Text);
            SendMessage(req);
        }

        /// <summary>
        /// 删除弹幕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteSub_Click(object sender, EventArgs e)
        {
            GMDeleteSubtitleReq req = new GMDeleteSubtitleReq();
            if (lvSbutitles.SelectedItems.Count > 0)
            {
                ListViewItem item = lvSbutitles.SelectedItems[0];
                if (lvSbutitles.Columns.ContainsKey("ID"))
                {
                    int index = lvSbutitles.Columns["ID"].Index;
                    int id = int.Parse(item.SubItems[index].Text);
                    req.ID = id;
                    SendMessage(req);
                    subtitles.Remove(id);
                }
            }
        }

        private void TxtSubScenarioId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSubScenarioId.Text))
            {
                TxtSubScenarioId.Text = "剧情-章";
            }
        }

        private void TxtSubScenarioId_MouseClick(object sender, MouseEventArgs e)
        {
            if (TxtSubScenarioId.Text == "剧情-章")
            {
                TxtSubScenarioId.Clear();
            }
        }

        private void TxtSubPartId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSubPartId.Text))
            {
                TxtSubPartId.Text = "剧情-节";
            }
        }

        private void TxtSubPartId_MouseClick(object sender, MouseEventArgs e)
        {
            if (TxtSubPartId.Text == "剧情-节")
            {
                TxtSubPartId.Clear();
            }
        }

        private void TxtSubPassageId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSubPassageId.Text))
            {
                TxtSubPassageId.Text = "剧情-段";
            }
        }

        private void TxtSubPassageId_MouseClick(object sender, MouseEventArgs e)
        {
            if (TxtSubPassageId.Text == "剧情-段")
            {
                TxtSubPassageId.Clear();
            }
        }

        /// <summary>
        /// 开启定时推送
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenTimingPush_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtGetStamina.Text) || "领取体力推送内容" == txtGetStamina.Text)
            {
                return;
            }

            if (string.IsNullOrEmpty(txtRecoverStamina.Text) || "体力恢复已满推送内容" == txtRecoverStamina.Text)
            {
                return;
            }

            if (string.IsNullOrEmpty(txtTimingPushTime.Text) || "定时推送时间，格式如：13：00，多个时间用逗号隔开" == txtTimingPushTime.Text)
            {
                return;
            }

            GMSwitchTimingPushReq req = new GMSwitchTimingPushReq();
            if (string.IsNullOrEmpty(txtStaminaRecoverTitle.Text) || "体力恢复已满推送标题" == txtStaminaRecoverTitle.Text)
            {
                req.GetStaminaTitle = "";
            }
            else
            {
                req.GetStaminaTitle = txtGetStaminaTitle.Text;
            }

            if (string.IsNullOrEmpty(txtGetStaminaTitle.Text) || "领取体力推送标题" == txtGetStaminaTitle.Text)
            {
                req.GetStaminaTitle = "";
            }
            else
            {
                req.RecoverStaminaTitle = txtStaminaRecoverTitle.Text;
            }

            string[] times = txtTimingPushTime.Text.Split(',');

            req.IsOpen = true;
            req.Time1 = times[0];
            if (times.Length > 1)
            {
                req.Time2 = times[1];
            }
            req.GetStaminaText = txtGetStamina.Text;
            req.RecoverStaminaText = txtRecoverStamina.Text;
            SendMessage(req);
        }

        /// <summary>
        /// 关闭定时推送
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCloseTimingPush_Click(object sender, EventArgs e)
        {
            GMSwitchTimingPushReq req = new GMSwitchTimingPushReq();
            req.IsOpen = false;
            SendMessage(req);
        }

        private void txtGetStamina_MouseClick(object sender, MouseEventArgs e)
        {
            if ("领取体力推送内容" == txtGetStamina.Text)
            {
                txtGetStamina.Clear();
            }
        }

        private void txtGetStamina_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtGetStamina.Text))
            {
                txtGetStamina.Text = "领取体力推送内容";
            }
        }

        private void txtRecoverStamina_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRecoverStamina.Text))
            {
                txtRecoverStamina.Text = "体力恢复已满推送内容";
            }
        }

        private void txtRecoverStamina_MouseClick(object sender, MouseEventArgs e)
        {
            if ("体力恢复已满推送内容" == txtRecoverStamina.Text)
            {
                txtRecoverStamina.Clear();
            }
        }

        private void txtTimingPushTime_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTimingPushTime.Text))
            {
                txtTimingPushTime.Text = "定时推送时间，格式如：13：00，多个时间用逗号隔开";
            }
        }

        private void txtTimingPushTime_MouseClick(object sender, MouseEventArgs e)
        {
            if ("定时推送时间，格式如：13：00，多个时间用逗号隔开" == txtTimingPushTime.Text)
            {
                txtTimingPushTime.Clear();
            }
        }

        #region
        ///// <summary>
        ///// 流失玩家推送
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void btnLosePush_Click(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrEmpty(txtLostPush.Text) || "流失玩家推送内容" == txtLostPush.Text)
        //    {
        //        return;
        //    }

        //    if (string.IsNullOrEmpty(txtLoseDays.Text) || "流失天数" == txtLoseDays.Text)
        //    {
        //        return;
        //    }

        //    int loseDays = 0;
        //    if (Int32.TryParse(txtLoseDays.Text,out loseDays))
        //    {
        //        GMPushToLosePlayerReq req = new GMPushToLosePlayerReq();
        //        req.LoseDays = loseDays;
        //        req.Content = txtLostPush.Text;
        //        SendMessage(req);
        //    }
        //}

        //private void txtLoseDays_Leave(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrEmpty(txtLoseDays.Text) )
        //    {
        //        txtLoseDays.Text = "流失天数";
        //    }
        //}

        //private void txtLoseDays_MouseClick(object sender, MouseEventArgs e)
        //{
        //    if ("流失天数" == txtLoseDays.Text)
        //    {
        //        txtLoseDays.Clear();
        //    }
        //}

        //private void txtLostPush_Leave(object sender, EventArgs e)
        //{
        //    if (string.IsNullOrEmpty(txtLostPush.Text))
        //    {
        //        txtLostPush.Text = "流失玩家推送内容";
        //    }
        //}

        //private void txtLostPush_MouseClick(object sender, MouseEventArgs e)
        //{
        //    if ( "流失玩家推送内容" == txtLostPush.Text)
        //    {
        //        txtLostPush.Clear();
        //    }
        //}
        #endregion

        private void txtStaminaRecoverTitle_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtStaminaRecoverTitle.Text))
            {
                txtStaminaRecoverTitle.Text = "体力恢复已满推送标题";
            }
        }

        private void txtStaminaRecoverTitle_MouseClick(object sender, MouseEventArgs e)
        {
            if ("体力恢复已满推送标题" == txtStaminaRecoverTitle.Text)
            {
                txtStaminaRecoverTitle.Clear();
            }
        }

        private void txtGetStaminaTitle_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtGetStaminaTitle.Text))
            {
                txtGetStaminaTitle.Text = "领取体力推送标题";
            }
        }

        private void txtGetStaminaTitle_MouseClick(object sender, MouseEventArgs e)
        {
            if ("领取体力推送标题" == txtGetStaminaTitle.Text)
            {
                txtGetStaminaTitle.Clear();
            }
        }

        private void buttonBroadcast_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("是否发送广播邮件？", "Confirm", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                SendMail(null, 1);
            }
        }

        private void buttonCheckReload_Click(object sender, EventArgs e)
        {
            GMStaticDataCheckReload msg = new GMStaticDataCheckReload();
            msg.t = DateTime.Parse(textBoxCheckReloadTime.Text);
            SendMessage(msg);
        }

        private void buttonGmList_Click(object sender, EventArgs e)
        {
            GmListAccountReq req = new GmListAccountReq();
            SendMessage(req);
        }

        private void buttonGmRegister_Click(object sender, EventArgs e)
        {
            AccountInfo form = new AccountInfo("");
            form.StartPosition = FormStartPosition.CenterParent;
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                GmRegisterAccountReq req = new GmRegisterAccountReq();
                req.account = form.Account;
                req.password = form.Password;
                req.gmLevel = form.GmLevel;
                SendMessage(req);
            }
        }

        private void buttonGmConfig_Click(object sender, EventArgs e)
        {
            ListViewItem item = null;
            if (lvAccounts.SelectedItems.Count > 0)
            {
                item = lvAccounts.SelectedItems[0];
            }
            else if (lvAccounts.Items.Count == 1)
            {
                item = lvAccounts.Items[0];
            }

            if (item != null)
            {
                if (lvAccounts.Columns.ContainsKey("AccountId"))
                {
                    int index = lvAccounts.Columns["Role"].Index;
                    string account = item.SubItems[index].Text;
                    index = lvAccounts.Columns["AccountId"].Index;
                    UInt64 acctid = UInt64.Parse(item.SubItems[index].Text);
                    AccountInfo form = new AccountInfo(account);
                    form.StartPosition = FormStartPosition.CenterParent;
                    if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                    {
                        GmAccountConfigReq req = new GmAccountConfigReq();
                        req.accountId = acctid;
                        req.password = form.Password;
                        req.gmLevel = form.GmLevel;
                        SendMessage(req);
                    }
                }
            }
        }

        /// <summary>
        /// 推送流失用户
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPushLoseUser_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLoseDays.Text) || "流失天数" == txtLoseDays.Text)
            {
                return;
            }

            if (string.IsNullOrEmpty(txtLosePushTitle.Text) || string.IsNullOrEmpty(txtLosePushContent.Text))
            {
                return;
            }

            int loseDays;

            if (Int32.TryParse(txtLoseDays.Text, out loseDays))
            {
                GMPushToLosePlayerReq req = new GMPushToLosePlayerReq();
                req.Title = txtLosePushTitle.Text;
                req.Content = txtLosePushContent.Text;
                req.LoseDays = loseDays;
                SendMessage(req);
            }
        }

        private void txtLoseDays_MouseClick(object sender, MouseEventArgs e)
        {
            if ("流失天数" == txtLoseDays.Text)
            {
                txtLoseDays.Clear();
            }
        }

        private void txtLoseDays_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLoseDays.Text))
            {
                txtLoseDays.Text = "流失天数";
            }
        }

        private void txtLosePushTitle_MouseClick(object sender, MouseEventArgs e)
        {
            if ("流失用户推送标题" == txtLosePushTitle.Text)
            {
                txtLosePushTitle.Clear();
            }
        }

        private void txtLosePushTitle_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLosePushTitle.Text))
            {
                txtLosePushTitle.Text = "流失用户推送标题";
            }
        }

        private void txtLosePushContent_MouseClick(object sender, MouseEventArgs e)
        {
            if ("流失用户推送内容" == txtLosePushContent.Text)
            {
                txtLosePushContent.Clear();
            }
        }

        private void txtLosePushContent_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLosePushContent.Text))
            {
                txtLosePushContent.Text = "流失用户推送内容";
            }
        }

        private void buttonRedeemCode_Click(object sender, EventArgs e)
        {
            GmRedeemCodeRoundListReq req = new GmRedeemCodeRoundListReq();
            SendMessage(req);
        }

        private void buttonRedeemCodeSearch_Click(object sender, EventArgs e)
        {
            SearchRedeemCode form = new SearchRedeemCode();
            form.StartPosition = FormStartPosition.CenterParent;
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                if (!string.IsNullOrEmpty(form.Key))
                {
                    if (form.Index == "Code")
                    {
                        GmRedeemCodeInfoReq req = new GmRedeemCodeInfoReq();
                        req.code = form.Key;
                        SendMessage(req);
                    }
                    else if (form.Index == "Bonus")
                    {
                        GmRedeemCodeBonusInfoReq req = new GmRedeemCodeBonusInfoReq();
                        if (int.TryParse(form.Key, out req.bonus))
                        {
                            SendMessage(req);
                        }
                        else
                        {
                            MessageBox.Show("参数无效");
                        }
                    }
                    else if (form.Index == "Role")
                    {
                        GmRedeemCodeRoleInfoReq req = new GmRedeemCodeRoleInfoReq();
                        if (UInt64.TryParse(form.Key, out req.roleId))
                        {
                            SendMessage(req);
                        }
                        else
                        {
                            MessageBox.Show("参数无效");
                        }
                    }
                }

                if (form.Index == "Type")
                {
                    GmRedeemCodeTypeListReq req = new GmRedeemCodeTypeListReq();
                    SendMessage(req);
                }
            }
        }

        private void buttonUploadLog_Click(object sender, EventArgs e)
        {
            ListViewItem item = null;
            if (lvPlayers.SelectedItems.Count > 0)
            {
                item = lvPlayers.SelectedItems[0];
            }
            else if (lvPlayers.Items.Count == 1)
            {
                item = lvPlayers.Items[0];
            }

            if (item != null)
            {
                if (lvPlayers.Columns.ContainsKey("RoleId"))
                {
                    int index = lvPlayers.Columns["RoleId"].Index;
                    GmUploadLogReq req = new GmUploadLogReq();
                    req.roleId = UInt64.Parse(item.SubItems[index].Text);
                    SendMessage(req);
                }
            }
        }

        /// <summary>
        /// 立即推送
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPushInst_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPushTitle.Text) || string.IsNullOrEmpty(txtPushContent.Text))
            {
                return;
            }

            if (string.IsNullOrEmpty(txtPusnTimes.Text) || string.IsNullOrEmpty(txtPushInterval.Text))
            {
                return;
            }

            int times;
            int interval;
            GMPushReq req = new GMPushReq();

            if (Int32.TryParse(txtPusnTimes.Text, out times) && Int32.TryParse(txtPushInterval.Text, out interval))
            {
                req.PushTimes = times;
                req.IntervalMinute = interval;
            }

            req.Title = txtPushTitle.Text;
            req.Content = txtPushContent.Text;
            SendMessage(req);
        }

        private void txtPushTitle_MouseClick(object sender, MouseEventArgs e)
        {
            if ("立即推送标题" == txtPushTitle.Text)
            {
                txtPushTitle.Clear();
            }
        }

        private void txtPushTitle_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPushTitle.Text))
            {
                txtPushTitle.Text = "立即推送标题";
            }
        }

        private void txtPushContent_MouseClick(object sender, MouseEventArgs e)
        {
            if ("立即推送内容" == txtPushContent.Text)
            {
                txtPushContent.Clear();
            }
        }

        private void txtPushContent_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPushContent.Text))
            {
                txtPushContent.Text = "立即推送内容";
            }
        }

        private void txtPusnTimes_Click(object sender, EventArgs e)
        {
            if ("立即推送次数" == txtPusnTimes.Text)
            {
                txtPusnTimes.Clear();
            }
        }

        private void txtPusnTimes_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPusnTimes.Text))
            {
                txtPusnTimes.Text = "立即推送次数";
            }
        }

        private void txtPushInterval_Click(object sender, EventArgs e)
        {
            if ("立即推送时间间隔，分钟" == txtPushInterval.Text)
            {
                txtPushInterval.Clear();
            }
        }

        private void txtPushInterval_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPushInterval.Text))
            {
                txtPushInterval.Text = "立即推送时间间隔，分钟";
            }
        }

        private void btnImportRecip_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileBrowser = new OpenFileDialog();
            if (fileBrowser.ShowDialog() == DialogResult.OK)
            {
                string extension = System.IO.Path.GetExtension(fileBrowser.FileName);

                if (".xlsx" == extension)
                {
                    LoadExcelData(fileBrowser.FileName);
                }

                #region //导入csv
                //using (StreamReader sr = new StreamReader(fileBrowser.FileName))
                //{
                //    while (sr.Peek() >= 0)
                //    {
                //        string info = sr.ReadLine();
                //        string[] infos = info.Split('|');

                //        if (2 != infos.Length)
                //        {
                //            continue;
                //        }

                //        UInt64 roleId;
                //        if (!UInt64.TryParse(infos[0], out roleId))
                //        {
                //            continue;
                //        }

                //        ListViewItem lvi = new ListViewItem();
                //        lvi.Text = roleId.ToString();
                //        lvi.SubItems.Add(infos[1]);
                //        lvMailReceiver.Items.Add(lvi);
                //    }
                //}
                #endregion
            }
        }

        private void lvMailReceiver_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (lvMailReceiver.SelectedItems.Count > 0)
                {
                    ListViewItem item = lvMailReceiver.SelectedItems[0];
                    lvMailReceiver.Items.Remove(item);
                }
            }
        }

        private void txtMailSendTime_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMailSendTime.Text))
            {
                txtMailSendTime.Text = "定时发送时间 0001-01-01 0:00:00";
            }
        }

        private void txtMailSendTime_MouseClick(object sender, MouseEventArgs e)
        {
            if ("定时发送时间 0001-01-01 0:00:00" == txtMailSendTime.Text)
            {
                txtMailSendTime.Clear();
            }
        }

        private void LoadExcelData(string fullFileName)
        {
            string sheetName = "sheet1";
            ExcelWorksheet workSheet = null;
            int maxColumnNum = 0;//最大列
            int minColumnNum = 0;//最小列
            int maxRowNum = 0;//最小行
            int minRowNum = 0;//最大行

            try
            {
                FileInfo file = new FileInfo(fullFileName);
                ExcelPackage package = new ExcelPackage(file);
                int sheetCount = package.Workbook.Worksheets.Count;
                workSheet = package.Workbook.Worksheets[sheetName];

                if (workSheet == null)
                {
                    return;
                }

                maxColumnNum = workSheet.Dimension.End.Column;//最大列
                minColumnNum = workSheet.Dimension.Start.Column;//最小列

                maxRowNum = workSheet.Dimension.End.Row;//最小行
                minRowNum = workSheet.Dimension.Start.Row;//最大行

                if (maxColumnNum == 0)
                {
                    return;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }

            for (int n = 1; n <= maxRowNum; n++)
            {
                if (n == 1)
                {
                    continue;
                }

                string[] infos = new string[maxColumnNum];
                for (int m = 1; m <= maxColumnNum; m++)
                {
                    infos[m - 1] = workSheet.Cells[n, m].Value.ToString();
                }

                UInt64 roleId;
                if (UInt64.TryParse(infos[0], out roleId))
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = roleId.ToString();
                    lvi.SubItems.Add(infos[1]);
                    lvMailReceiver.Items.Add(lvi);
                }
            }
        }

        private void btnOrderRecord_Click(object sender, EventArgs e)
        {
            if (lvPlayers.SelectedItems.Count > 0)
            {
                ListViewItem item = lvPlayers.SelectedItems[0];
                if (lvPlayers.Columns.ContainsKey("RoleId") && lvPlayers.Columns.ContainsKey("Role"))
                {
                    int index = lvPlayers.Columns["RoleId"].Index;

                    UInt64 roleId;
                    if (UInt64.TryParse(item.SubItems[index].Text, out roleId))
                    {
                        GmGetRechargeInfoReq req = new GmGetRechargeInfoReq();
                        req.roleId = roleId;
                        SendMessage(req);
                    }
                }
            }
        }


        public void CallGmSearchForm()
        {
            gmSearch = new GmClientSearch(roleId, name, session);
            //this.Hide();
            gmSearch.Show();


        }

        public void BtnGmSearch_Click_1(object sender, EventArgs e)
        {
            CallGmSearchForm();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        /// <summary>
        /// 查询预设置邮件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPreMailSearch_Click(object sender, EventArgs e)
        {
            GmQueryPreSetsMailListReq req = new GmQueryPreSetsMailListReq();
            SendMessage(req);
        }

        /// <summary>
        /// 删除预设置邮件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPreMailDelete_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem();

            if (lvDetails.SelectedItems.Count > 0)
            {
                lvi = lvDetails.SelectedItems[0];
            }
            else if (lvPlayers.Items.Count == 1)
            {
                lvi = lvDetails.Items[0];
            }

            if (lvi != null)
            {
                if (lvDetails.Columns.ContainsKey("BodyId"))
                {
                    int index = lvDetails.Columns["BodyId"].Index;
                    GmDeletePreSetsMailListReq req = new GmDeletePreSetsMailListReq();
                    req.BodyItemId = Int64.Parse(lvi.SubItems[index].Text);
                    SendMessage(req);
                }
            }
            else
            {
                MessageBox.Show("您没有选中任何邮件");
            }
        }

        private void txtBoxMailExpri_MouseClick(object sender, MouseEventArgs e)
        {
            if ("邮件过期时间 0001-01-01 0:00:00" == txtBoxMailExpri.Text)
            {
                txtBoxMailExpri.Clear();
            }
        }

        private void txtBoxMailExpri_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtBoxMailExpri.Text))
            {
                txtBoxMailExpri.Text = "邮件过期时间 0001-01-01 0:00:00";
            }
        }
    }
}
