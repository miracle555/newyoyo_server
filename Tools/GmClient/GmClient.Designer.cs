﻿namespace GmClient
{
    partial class GmClient
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnGmSearch = new System.Windows.Forms.Button();
            this.comboBoxServer = new System.Windows.Forms.ComboBox();
            this.buttonUploadLog = new System.Windows.Forms.Button();
            this.buttonRedeemCodeSearch = new System.Windows.Forms.Button();
            this.buttonRedeemCode = new System.Windows.Forms.Button();
            this.buttonGmConfig = new System.Windows.Forms.Button();
            this.buttonGmRegister = new System.Windows.Forms.Button();
            this.buttonGmList = new System.Windows.Forms.Button();
            this.tbError = new System.Windows.Forms.TextBox();
            this.TBOutput = new System.Windows.Forms.TextBox();
            this.BtnLogout = new System.Windows.Forms.Button();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.TBPassword = new System.Windows.Forms.TextBox();
            this.LBPassword = new System.Windows.Forms.Label();
            this.LBAccount = new System.Windows.Forms.Label();
            this.LBServerIp = new System.Windows.Forms.Label();
            this.TBAccount = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtBoxMailExpri = new System.Windows.Forms.TextBox();
            this.btnPreMailDelete = new System.Windows.Forms.Button();
            this.btnPreMailSearch = new System.Windows.Forms.Button();
            this.btnOrderRecord = new System.Windows.Forms.Button();
            this.txtMailSendTime = new System.Windows.Forms.TextBox();
            this.btnImportRecip = new System.Windows.Forms.Button();
            this.buttonCheckReload = new System.Windows.Forms.Button();
            this.buttonBroadcast = new System.Windows.Forms.Button();
            this.textBoxCheckReloadTime = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPlatform = new System.Windows.Forms.TextBox();
            this.btnItemAdd = new System.Windows.Forms.Button();
            this.btnReceiver = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbLogEnd = new System.Windows.Forms.TextBox();
            this.tbLogStart = new System.Windows.Forms.TextBox();
            this.btnLog = new System.Windows.Forms.Button();
            this.btnAccessory = new System.Windows.Forms.Button();
            this.lvAccessories = new System.Windows.Forms.ListView();
            this.btnBroadcastMail = new System.Windows.Forms.Button();
            this.btnSendMail = new System.Windows.Forms.Button();
            this.tbMailTitle = new System.Windows.Forms.TextBox();
            this.btnMailList = new System.Windows.Forms.Button();
            this.btnList = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tbMailContent = new System.Windows.Forms.TextBox();
            this.btnRevert = new System.Windows.Forms.Button();
            this.tbInterval = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnItemSubmit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnItemEdit = new System.Windows.Forms.Button();
            this.tbSysPostEndTime = new System.Windows.Forms.TextBox();
            this.tbSysPostStartTime = new System.Windows.Forms.TextBox();
            this.tbChangeName = new System.Windows.Forms.TextBox();
            this.TBSystemPost = new System.Windows.Forms.TextBox();
            this.tbPlayerKey = new System.Windows.Forms.TextBox();
            this.BtnSystemPost = new System.Windows.Forms.Button();
            this.btnChangeName = new System.Windows.Forms.Button();
            this.tbDisableDendline = new System.Windows.Forms.TextBox();
            this.tbPlayerIdKey = new System.Windows.Forms.TextBox();
            this.btnSearchAccount = new System.Windows.Forms.Button();
            this.btnUnlock = new System.Windows.Forms.Button();
            this.btnKick = new System.Windows.Forms.Button();
            this.btnItemList = new System.Windows.Forms.Button();
            this.tbAccountKey = new System.Windows.Forms.TextBox();
            this.btnSearchPlayer = new System.Windows.Forms.Button();
            this.btnLock = new System.Windows.Forms.Button();
            this.tbActIdKey = new System.Windows.Forms.TextBox();
            this.lvAccounts = new System.Windows.Forms.ListView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lvMailReceiver = new System.Windows.Forms.ListView();
            this.lvSbutitles = new System.Windows.Forms.ListView();
            this.lvDetails = new System.Windows.Forms.ListView();
            this.lvPlayers = new System.Windows.Forms.ListView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnDeleteSub = new System.Windows.Forms.Button();
            this.btnQuerySub = new System.Windows.Forms.Button();
            this.TxtSubPassageId = new System.Windows.Forms.TextBox();
            this.TxtSubPartId = new System.Windows.Forms.TextBox();
            this.TxtSubScenarioId = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtPushInterval = new System.Windows.Forms.TextBox();
            this.txtPusnTimes = new System.Windows.Forms.TextBox();
            this.btnPushInst = new System.Windows.Forms.Button();
            this.txtPushContent = new System.Windows.Forms.TextBox();
            this.txtPushTitle = new System.Windows.Forms.TextBox();
            this.txtLoseDays = new System.Windows.Forms.TextBox();
            this.txtLosePushContent = new System.Windows.Forms.TextBox();
            this.txtLosePushTitle = new System.Windows.Forms.TextBox();
            this.btnPushLoseUser = new System.Windows.Forms.Button();
            this.txtStaminaRecoverTitle = new System.Windows.Forms.TextBox();
            this.txtGetStaminaTitle = new System.Windows.Forms.TextBox();
            this.txtTimingPushTime = new System.Windows.Forms.TextBox();
            this.txtRecoverStamina = new System.Windows.Forms.TextBox();
            this.txtGetStamina = new System.Windows.Forms.TextBox();
            this.btnCloseTimingPush = new System.Windows.Forms.Button();
            this.btnOpenTimingPush = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnGmSearch);
            this.panel1.Controls.Add(this.comboBoxServer);
            this.panel1.Controls.Add(this.buttonUploadLog);
            this.panel1.Controls.Add(this.buttonRedeemCodeSearch);
            this.panel1.Controls.Add(this.buttonRedeemCode);
            this.panel1.Controls.Add(this.buttonGmConfig);
            this.panel1.Controls.Add(this.buttonGmRegister);
            this.panel1.Controls.Add(this.buttonGmList);
            this.panel1.Controls.Add(this.tbError);
            this.panel1.Controls.Add(this.TBOutput);
            this.panel1.Controls.Add(this.BtnLogout);
            this.panel1.Controls.Add(this.BtnLogin);
            this.panel1.Controls.Add(this.TBPassword);
            this.panel1.Controls.Add(this.LBPassword);
            this.panel1.Controls.Add(this.LBAccount);
            this.panel1.Controls.Add(this.LBServerIp);
            this.panel1.Controls.Add(this.TBAccount);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1322, 135);
            this.panel1.TabIndex = 1;
            // 
            // BtnGmSearch
            // 
            this.BtnGmSearch.Location = new System.Drawing.Point(340, 33);
            this.BtnGmSearch.Name = "BtnGmSearch";
            this.BtnGmSearch.Size = new System.Drawing.Size(143, 23);
            this.BtnGmSearch.TabIndex = 29;
            this.BtnGmSearch.Text = "日志查询";
            this.BtnGmSearch.UseVisualStyleBackColor = true;
            this.BtnGmSearch.Click += new System.EventHandler(this.BtnGmSearch_Click_1);
            // 
            // comboBoxServer
            // 
            this.comboBoxServer.FormattingEnabled = true;
            this.comboBoxServer.Location = new System.Drawing.Point(50, 3);
            this.comboBoxServer.Name = "comboBoxServer";
            this.comboBoxServer.Size = new System.Drawing.Size(131, 20);
            this.comboBoxServer.TabIndex = 28;
            // 
            // buttonUploadLog
            // 
            this.buttonUploadLog.Location = new System.Drawing.Point(340, 109);
            this.buttonUploadLog.Name = "buttonUploadLog";
            this.buttonUploadLog.Size = new System.Drawing.Size(146, 23);
            this.buttonUploadLog.TabIndex = 27;
            this.buttonUploadLog.Text = "要求玩家上传日志";
            this.buttonUploadLog.UseVisualStyleBackColor = true;
            this.buttonUploadLog.Click += new System.EventHandler(this.buttonUploadLog_Click);
            // 
            // buttonRedeemCodeSearch
            // 
            this.buttonRedeemCodeSearch.Location = new System.Drawing.Point(188, 109);
            this.buttonRedeemCodeSearch.Name = "buttonRedeemCodeSearch";
            this.buttonRedeemCodeSearch.Size = new System.Drawing.Size(146, 23);
            this.buttonRedeemCodeSearch.TabIndex = 26;
            this.buttonRedeemCodeSearch.Text = "兑换码查询";
            this.buttonRedeemCodeSearch.UseVisualStyleBackColor = true;
            this.buttonRedeemCodeSearch.Click += new System.EventHandler(this.buttonRedeemCodeSearch_Click);
            // 
            // buttonRedeemCode
            // 
            this.buttonRedeemCode.Location = new System.Drawing.Point(188, 80);
            this.buttonRedeemCode.Name = "buttonRedeemCode";
            this.buttonRedeemCode.Size = new System.Drawing.Size(146, 23);
            this.buttonRedeemCode.TabIndex = 25;
            this.buttonRedeemCode.Text = "生成兑换码";
            this.buttonRedeemCode.UseVisualStyleBackColor = true;
            this.buttonRedeemCode.Click += new System.EventHandler(this.buttonRedeemCode_Click);
            // 
            // buttonGmConfig
            // 
            this.buttonGmConfig.Location = new System.Drawing.Point(188, 32);
            this.buttonGmConfig.Name = "buttonGmConfig";
            this.buttonGmConfig.Size = new System.Drawing.Size(146, 23);
            this.buttonGmConfig.TabIndex = 24;
            this.buttonGmConfig.Text = "gm帐号配置";
            this.buttonGmConfig.UseVisualStyleBackColor = true;
            this.buttonGmConfig.Click += new System.EventHandler(this.buttonGmConfig_Click);
            // 
            // buttonGmRegister
            // 
            this.buttonGmRegister.Location = new System.Drawing.Point(340, 3);
            this.buttonGmRegister.Name = "buttonGmRegister";
            this.buttonGmRegister.Size = new System.Drawing.Size(146, 23);
            this.buttonGmRegister.TabIndex = 23;
            this.buttonGmRegister.Text = "gm帐号注册";
            this.buttonGmRegister.UseVisualStyleBackColor = true;
            this.buttonGmRegister.Click += new System.EventHandler(this.buttonGmRegister_Click);
            // 
            // buttonGmList
            // 
            this.buttonGmList.Location = new System.Drawing.Point(188, 3);
            this.buttonGmList.Name = "buttonGmList";
            this.buttonGmList.Size = new System.Drawing.Size(146, 23);
            this.buttonGmList.TabIndex = 22;
            this.buttonGmList.Text = "gm帐号列表";
            this.buttonGmList.UseVisualStyleBackColor = true;
            this.buttonGmList.Click += new System.EventHandler(this.buttonGmList_Click);
            // 
            // tbError
            // 
            this.tbError.Enabled = false;
            this.tbError.Location = new System.Drawing.Point(492, 0);
            this.tbError.Multiline = true;
            this.tbError.Name = "tbError";
            this.tbError.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbError.Size = new System.Drawing.Size(295, 129);
            this.tbError.TabIndex = 21;
            // 
            // TBOutput
            // 
            this.TBOutput.Enabled = false;
            this.TBOutput.Location = new System.Drawing.Point(793, 3);
            this.TBOutput.Multiline = true;
            this.TBOutput.Name = "TBOutput";
            this.TBOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TBOutput.Size = new System.Drawing.Size(526, 129);
            this.TBOutput.TabIndex = 2;
            // 
            // BtnLogout
            // 
            this.BtnLogout.Location = new System.Drawing.Point(3, 109);
            this.BtnLogout.Name = "BtnLogout";
            this.BtnLogout.Size = new System.Drawing.Size(179, 23);
            this.BtnLogout.TabIndex = 7;
            this.BtnLogout.Text = "登出";
            this.BtnLogout.UseVisualStyleBackColor = true;
            this.BtnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
            // 
            // BtnLogin
            // 
            this.BtnLogin.Location = new System.Drawing.Point(3, 80);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(179, 23);
            this.BtnLogin.TabIndex = 6;
            this.BtnLogin.Text = "登录";
            this.BtnLogin.UseVisualStyleBackColor = true;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // TBPassword
            // 
            this.TBPassword.Location = new System.Drawing.Point(50, 51);
            this.TBPassword.Name = "TBPassword";
            this.TBPassword.Size = new System.Drawing.Size(132, 21);
            this.TBPassword.TabIndex = 5;
            this.TBPassword.UseSystemPasswordChar = true;
            // 
            // LBPassword
            // 
            this.LBPassword.AutoSize = true;
            this.LBPassword.Location = new System.Drawing.Point(3, 54);
            this.LBPassword.Name = "LBPassword";
            this.LBPassword.Size = new System.Drawing.Size(29, 12);
            this.LBPassword.TabIndex = 4;
            this.LBPassword.Text = "密码";
            // 
            // LBAccount
            // 
            this.LBAccount.AutoSize = true;
            this.LBAccount.Location = new System.Drawing.Point(3, 30);
            this.LBAccount.Name = "LBAccount";
            this.LBAccount.Size = new System.Drawing.Size(29, 12);
            this.LBAccount.TabIndex = 3;
            this.LBAccount.Text = "帐号";
            // 
            // LBServerIp
            // 
            this.LBServerIp.AutoSize = true;
            this.LBServerIp.Location = new System.Drawing.Point(3, 6);
            this.LBServerIp.Name = "LBServerIp";
            this.LBServerIp.Size = new System.Drawing.Size(41, 12);
            this.LBServerIp.TabIndex = 2;
            this.LBServerIp.Text = "服务器";
            // 
            // TBAccount
            // 
            this.TBAccount.Location = new System.Drawing.Point(50, 27);
            this.TBAccount.Name = "TBAccount";
            this.TBAccount.Size = new System.Drawing.Size(132, 21);
            this.TBAccount.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtBoxMailExpri);
            this.panel2.Controls.Add(this.btnPreMailDelete);
            this.panel2.Controls.Add(this.btnPreMailSearch);
            this.panel2.Controls.Add(this.btnOrderRecord);
            this.panel2.Controls.Add(this.txtMailSendTime);
            this.panel2.Controls.Add(this.btnImportRecip);
            this.panel2.Controls.Add(this.buttonCheckReload);
            this.panel2.Controls.Add(this.buttonBroadcast);
            this.panel2.Controls.Add(this.textBoxCheckReloadTime);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.textBoxPlatform);
            this.panel2.Controls.Add(this.btnItemAdd);
            this.panel2.Controls.Add(this.btnReceiver);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.tbLogEnd);
            this.panel2.Controls.Add(this.tbLogStart);
            this.panel2.Controls.Add(this.btnLog);
            this.panel2.Controls.Add(this.btnAccessory);
            this.panel2.Controls.Add(this.lvAccessories);
            this.panel2.Controls.Add(this.btnBroadcastMail);
            this.panel2.Controls.Add(this.btnSendMail);
            this.panel2.Controls.Add(this.tbMailTitle);
            this.panel2.Controls.Add(this.btnMailList);
            this.panel2.Controls.Add(this.btnList);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.tbMailContent);
            this.panel2.Controls.Add(this.btnRevert);
            this.panel2.Controls.Add(this.tbInterval);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.btnItemSubmit);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnItemEdit);
            this.panel2.Controls.Add(this.tbSysPostEndTime);
            this.panel2.Controls.Add(this.tbSysPostStartTime);
            this.panel2.Controls.Add(this.tbChangeName);
            this.panel2.Controls.Add(this.TBSystemPost);
            this.panel2.Controls.Add(this.tbPlayerKey);
            this.panel2.Controls.Add(this.BtnSystemPost);
            this.panel2.Controls.Add(this.btnChangeName);
            this.panel2.Controls.Add(this.tbDisableDendline);
            this.panel2.Controls.Add(this.tbPlayerIdKey);
            this.panel2.Controls.Add(this.btnSearchAccount);
            this.panel2.Controls.Add(this.btnUnlock);
            this.panel2.Controls.Add(this.btnKick);
            this.panel2.Controls.Add(this.btnItemList);
            this.panel2.Controls.Add(this.tbAccountKey);
            this.panel2.Controls.Add(this.btnSearchPlayer);
            this.panel2.Controls.Add(this.btnLock);
            this.panel2.Controls.Add(this.tbActIdKey);
            this.panel2.Location = new System.Drawing.Point(12, 153);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(486, 525);
            this.panel2.TabIndex = 2;
            // 
            // txtBoxMailExpri
            // 
            this.txtBoxMailExpri.Location = new System.Drawing.Point(274, 495);
            this.txtBoxMailExpri.Name = "txtBoxMailExpri";
            this.txtBoxMailExpri.Size = new System.Drawing.Size(209, 21);
            this.txtBoxMailExpri.TabIndex = 61;
            this.txtBoxMailExpri.Text = "邮件过期时间 0001-01-01 0:00:00";
            this.txtBoxMailExpri.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtBoxMailExpri_MouseClick);
            this.txtBoxMailExpri.Leave += new System.EventHandler(this.txtBoxMailExpri_Leave);
            // 
            // btnPreMailDelete
            // 
            this.btnPreMailDelete.Location = new System.Drawing.Point(131, 494);
            this.btnPreMailDelete.Name = "btnPreMailDelete";
            this.btnPreMailDelete.Size = new System.Drawing.Size(135, 23);
            this.btnPreMailDelete.TabIndex = 60;
            this.btnPreMailDelete.Text = "删除预设置邮件";
            this.btnPreMailDelete.UseVisualStyleBackColor = true;
            this.btnPreMailDelete.Click += new System.EventHandler(this.btnPreMailDelete_Click);
            // 
            // btnPreMailSearch
            // 
            this.btnPreMailSearch.Location = new System.Drawing.Point(5, 494);
            this.btnPreMailSearch.Name = "btnPreMailSearch";
            this.btnPreMailSearch.Size = new System.Drawing.Size(116, 23);
            this.btnPreMailSearch.TabIndex = 59;
            this.btnPreMailSearch.Text = "查询预设置邮件";
            this.btnPreMailSearch.UseVisualStyleBackColor = true;
            this.btnPreMailSearch.Click += new System.EventHandler(this.btnPreMailSearch_Click);
            // 
            // btnOrderRecord
            // 
            this.btnOrderRecord.Location = new System.Drawing.Point(363, 263);
            this.btnOrderRecord.Name = "btnOrderRecord";
            this.btnOrderRecord.Size = new System.Drawing.Size(120, 23);
            this.btnOrderRecord.TabIndex = 58;
            this.btnOrderRecord.Text = "查询充值记录";
            this.btnOrderRecord.UseVisualStyleBackColor = true;
            this.btnOrderRecord.Click += new System.EventHandler(this.btnOrderRecord_Click);
            // 
            // txtMailSendTime
            // 
            this.txtMailSendTime.Location = new System.Drawing.Point(274, 467);
            this.txtMailSendTime.Name = "txtMailSendTime";
            this.txtMailSendTime.Size = new System.Drawing.Size(209, 21);
            this.txtMailSendTime.TabIndex = 57;
            this.txtMailSendTime.Text = "定时发送时间 0001-01-01 0:00:00";
            this.txtMailSendTime.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtMailSendTime_MouseClick);
            this.txtMailSendTime.Leave += new System.EventHandler(this.txtMailSendTime_Leave);
            // 
            // btnImportRecip
            // 
            this.btnImportRecip.Location = new System.Drawing.Point(274, 437);
            this.btnImportRecip.Name = "btnImportRecip";
            this.btnImportRecip.Size = new System.Drawing.Size(209, 23);
            this.btnImportRecip.TabIndex = 56;
            this.btnImportRecip.Text = "导入收件人";
            this.btnImportRecip.UseVisualStyleBackColor = true;
            this.btnImportRecip.Click += new System.EventHandler(this.btnImportRecip_Click);
            // 
            // buttonCheckReload
            // 
            this.buttonCheckReload.Location = new System.Drawing.Point(363, 236);
            this.buttonCheckReload.Name = "buttonCheckReload";
            this.buttonCheckReload.Size = new System.Drawing.Size(120, 23);
            this.buttonCheckReload.TabIndex = 46;
            this.buttonCheckReload.Text = "重新加载静态数据";
            this.buttonCheckReload.UseVisualStyleBackColor = true;
            this.buttonCheckReload.Click += new System.EventHandler(this.buttonCheckReload_Click);
            // 
            // buttonBroadcast
            // 
            this.buttonBroadcast.Location = new System.Drawing.Point(89, 464);
            this.buttonBroadcast.Name = "buttonBroadcast";
            this.buttonBroadcast.Size = new System.Drawing.Size(81, 23);
            this.buttonBroadcast.TabIndex = 55;
            this.buttonBroadcast.Text = "广播邮件";
            this.buttonBroadcast.UseVisualStyleBackColor = true;
            this.buttonBroadcast.Click += new System.EventHandler(this.buttonBroadcast_Click);
            // 
            // textBoxCheckReloadTime
            // 
            this.textBoxCheckReloadTime.Location = new System.Drawing.Point(363, 209);
            this.textBoxCheckReloadTime.Name = "textBoxCheckReloadTime";
            this.textBoxCheckReloadTime.Size = new System.Drawing.Size(120, 21);
            this.textBoxCheckReloadTime.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(376, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 54;
            this.label6.Text = "渠道平台";
            // 
            // textBoxPlatform
            // 
            this.textBoxPlatform.Location = new System.Drawing.Point(435, 66);
            this.textBoxPlatform.Name = "textBoxPlatform";
            this.textBoxPlatform.Size = new System.Drawing.Size(48, 21);
            this.textBoxPlatform.TabIndex = 53;
            // 
            // btnItemAdd
            // 
            this.btnItemAdd.Location = new System.Drawing.Point(165, 236);
            this.btnItemAdd.Name = "btnItemAdd";
            this.btnItemAdd.Size = new System.Drawing.Size(75, 23);
            this.btnItemAdd.TabIndex = 51;
            this.btnItemAdd.Text = "添加道具";
            this.btnItemAdd.UseVisualStyleBackColor = true;
            this.btnItemAdd.Click += new System.EventHandler(this.btnItemAdd_Click);
            // 
            // btnReceiver
            // 
            this.btnReceiver.Location = new System.Drawing.Point(176, 437);
            this.btnReceiver.Name = "btnReceiver";
            this.btnReceiver.Size = new System.Drawing.Size(90, 23);
            this.btnReceiver.TabIndex = 50;
            this.btnReceiver.Text = "添加收件人";
            this.btnReceiver.UseVisualStyleBackColor = true;
            this.btnReceiver.Click += new System.EventHandler(this.btnReceiver_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(129, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 48;
            this.label4.Text = "-";
            // 
            // tbLogEnd
            // 
            this.tbLogEnd.Location = new System.Drawing.Point(146, 265);
            this.tbLogEnd.Name = "tbLogEnd";
            this.tbLogEnd.Size = new System.Drawing.Size(120, 21);
            this.tbLogEnd.TabIndex = 47;
            // 
            // tbLogStart
            // 
            this.tbLogStart.Location = new System.Drawing.Point(3, 265);
            this.tbLogStart.Name = "tbLogStart";
            this.tbLogStart.Size = new System.Drawing.Size(120, 21);
            this.tbLogStart.TabIndex = 46;
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(274, 263);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(75, 23);
            this.btnLog.TabIndex = 45;
            this.btnLog.Text = "查询日志";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // btnAccessory
            // 
            this.btnAccessory.Location = new System.Drawing.Point(89, 436);
            this.btnAccessory.Name = "btnAccessory";
            this.btnAccessory.Size = new System.Drawing.Size(81, 23);
            this.btnAccessory.TabIndex = 44;
            this.btnAccessory.Text = "添加附件";
            this.btnAccessory.UseVisualStyleBackColor = true;
            this.btnAccessory.Click += new System.EventHandler(this.btnAccessory_Click);
            // 
            // lvAccessories
            // 
            this.lvAccessories.Location = new System.Drawing.Point(3, 364);
            this.lvAccessories.Name = "lvAccessories";
            this.lvAccessories.Size = new System.Drawing.Size(480, 67);
            this.lvAccessories.TabIndex = 43;
            this.lvAccessories.UseCompatibleStateImageBehavior = false;
            this.lvAccessories.View = System.Windows.Forms.View.Details;
            this.lvAccessories.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvAccessories_KeyUp);
            this.lvAccessories.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvAccessories_MouseDoubleClick);
            // 
            // btnBroadcastMail
            // 
            this.btnBroadcastMail.Location = new System.Drawing.Point(3, 464);
            this.btnBroadcastMail.Name = "btnBroadcastMail";
            this.btnBroadcastMail.Size = new System.Drawing.Size(80, 23);
            this.btnBroadcastMail.TabIndex = 42;
            this.btnBroadcastMail.Text = "系统邮件";
            this.btnBroadcastMail.UseVisualStyleBackColor = true;
            this.btnBroadcastMail.Click += new System.EventHandler(this.btnBroadcastMail_Click);
            // 
            // btnSendMail
            // 
            this.btnSendMail.Location = new System.Drawing.Point(176, 464);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(90, 23);
            this.btnSendMail.TabIndex = 41;
            this.btnSendMail.Text = "发送邮件";
            this.btnSendMail.UseVisualStyleBackColor = true;
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // tbMailTitle
            // 
            this.tbMailTitle.Location = new System.Drawing.Point(3, 292);
            this.tbMailTitle.Name = "tbMailTitle";
            this.tbMailTitle.Size = new System.Drawing.Size(480, 21);
            this.tbMailTitle.TabIndex = 40;
            this.tbMailTitle.Text = "邮件标题";
            this.tbMailTitle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbMailTitle_MouseClick);
            this.tbMailTitle.Leave += new System.EventHandler(this.tbMailTitle_Leave);
            // 
            // btnMailList
            // 
            this.btnMailList.Location = new System.Drawing.Point(3, 436);
            this.btnMailList.Name = "btnMailList";
            this.btnMailList.Size = new System.Drawing.Size(80, 23);
            this.btnMailList.TabIndex = 33;
            this.btnMailList.Text = "查询邮件";
            this.btnMailList.UseVisualStyleBackColor = true;
            this.btnMailList.Click += new System.EventHandler(this.btnMailList_Click);
            // 
            // btnList
            // 
            this.btnList.Location = new System.Drawing.Point(3, 93);
            this.btnList.Name = "btnList";
            this.btnList.Size = new System.Drawing.Size(237, 23);
            this.btnList.TabIndex = 20;
            this.btnList.Text = "获取公告";
            this.btnList.UseVisualStyleBackColor = true;
            this.btnList.Click += new System.EventHandler(this.btnList_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(353, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 18;
            this.label3.Text = "秒";
            // 
            // tbMailContent
            // 
            this.tbMailContent.Location = new System.Drawing.Point(3, 319);
            this.tbMailContent.Multiline = true;
            this.tbMailContent.Name = "tbMailContent";
            this.tbMailContent.Size = new System.Drawing.Size(480, 39);
            this.tbMailContent.TabIndex = 39;
            this.tbMailContent.Text = "邮件内容";
            this.tbMailContent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbMailContent_MouseClick);
            this.tbMailContent.Leave += new System.EventHandler(this.tbMailContent_Leave);
            // 
            // btnRevert
            // 
            this.btnRevert.Location = new System.Drawing.Point(246, 93);
            this.btnRevert.Name = "btnRevert";
            this.btnRevert.Size = new System.Drawing.Size(237, 23);
            this.btnRevert.TabIndex = 13;
            this.btnRevert.Text = "撤销公告";
            this.btnRevert.UseVisualStyleBackColor = true;
            this.btnRevert.Click += new System.EventHandler(this.btnRevert_Click);
            // 
            // tbInterval
            // 
            this.tbInterval.Location = new System.Drawing.Point(307, 66);
            this.tbInterval.Name = "tbInterval";
            this.tbInterval.Size = new System.Drawing.Size(40, 21);
            this.tbInterval.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(272, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "间隔";
            // 
            // btnItemSubmit
            // 
            this.btnItemSubmit.Location = new System.Drawing.Point(246, 236);
            this.btnItemSubmit.Name = "btnItemSubmit";
            this.btnItemSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnItemSubmit.TabIndex = 38;
            this.btnItemSubmit.Text = "道具提交";
            this.btnItemSubmit.UseVisualStyleBackColor = true;
            this.btnItemSubmit.Click += new System.EventHandler(this.btnItemSubmit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(129, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "-";
            // 
            // btnItemEdit
            // 
            this.btnItemEdit.Location = new System.Drawing.Point(84, 236);
            this.btnItemEdit.Name = "btnItemEdit";
            this.btnItemEdit.Size = new System.Drawing.Size(75, 23);
            this.btnItemEdit.TabIndex = 37;
            this.btnItemEdit.Text = "编辑道具";
            this.btnItemEdit.UseVisualStyleBackColor = true;
            this.btnItemEdit.Click += new System.EventHandler(this.btnItemEdit_Click);
            // 
            // tbSysPostEndTime
            // 
            this.tbSysPostEndTime.Location = new System.Drawing.Point(146, 66);
            this.tbSysPostEndTime.Name = "tbSysPostEndTime";
            this.tbSysPostEndTime.Size = new System.Drawing.Size(120, 21);
            this.tbSysPostEndTime.TabIndex = 14;
            // 
            // tbSysPostStartTime
            // 
            this.tbSysPostStartTime.Location = new System.Drawing.Point(3, 66);
            this.tbSysPostStartTime.Name = "tbSysPostStartTime";
            this.tbSysPostStartTime.Size = new System.Drawing.Size(120, 21);
            this.tbSysPostStartTime.TabIndex = 13;
            // 
            // tbChangeName
            // 
            this.tbChangeName.Location = new System.Drawing.Point(165, 209);
            this.tbChangeName.Name = "tbChangeName";
            this.tbChangeName.Size = new System.Drawing.Size(156, 21);
            this.tbChangeName.TabIndex = 36;
            // 
            // TBSystemPost
            // 
            this.TBSystemPost.Location = new System.Drawing.Point(3, 3);
            this.TBSystemPost.Multiline = true;
            this.TBSystemPost.Name = "TBSystemPost";
            this.TBSystemPost.Size = new System.Drawing.Size(480, 59);
            this.TBSystemPost.TabIndex = 11;
            this.TBSystemPost.Text = "公告内容";
            this.TBSystemPost.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TBSystemPost_MouseClick);
            this.TBSystemPost.Leave += new System.EventHandler(this.TBSystemPost_Leave);
            // 
            // tbPlayerKey
            // 
            this.tbPlayerKey.Location = new System.Drawing.Point(109, 151);
            this.tbPlayerKey.Name = "tbPlayerKey";
            this.tbPlayerKey.Size = new System.Drawing.Size(100, 21);
            this.tbPlayerKey.TabIndex = 23;
            this.tbPlayerKey.Text = "角色";
            this.tbPlayerKey.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbPlayerKey_MouseClick);
            this.tbPlayerKey.Leave += new System.EventHandler(this.tbPlayerKey_Leave);
            // 
            // BtnSystemPost
            // 
            this.BtnSystemPost.Location = new System.Drawing.Point(3, 122);
            this.BtnSystemPost.Name = "BtnSystemPost";
            this.BtnSystemPost.Size = new System.Drawing.Size(480, 23);
            this.BtnSystemPost.TabIndex = 12;
            this.BtnSystemPost.Text = "发送系统公告(45字以内)";
            this.BtnSystemPost.UseVisualStyleBackColor = true;
            this.BtnSystemPost.Click += new System.EventHandler(this.BtnSystemPost_Click);
            // 
            // btnChangeName
            // 
            this.btnChangeName.Location = new System.Drawing.Point(84, 207);
            this.btnChangeName.Name = "btnChangeName";
            this.btnChangeName.Size = new System.Drawing.Size(75, 23);
            this.btnChangeName.TabIndex = 35;
            this.btnChangeName.Text = "角色改名";
            this.btnChangeName.UseVisualStyleBackColor = true;
            this.btnChangeName.Click += new System.EventHandler(this.btnChangeName_Click);
            // 
            // tbDisableDendline
            // 
            this.tbDisableDendline.Location = new System.Drawing.Point(165, 180);
            this.tbDisableDendline.Name = "tbDisableDendline";
            this.tbDisableDendline.Size = new System.Drawing.Size(156, 21);
            this.tbDisableDendline.TabIndex = 34;
            // 
            // tbPlayerIdKey
            // 
            this.tbPlayerIdKey.Location = new System.Drawing.Point(321, 151);
            this.tbPlayerIdKey.Name = "tbPlayerIdKey";
            this.tbPlayerIdKey.Size = new System.Drawing.Size(100, 21);
            this.tbPlayerIdKey.TabIndex = 24;
            this.tbPlayerIdKey.Text = "角色id";
            this.tbPlayerIdKey.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbPlayerIdKey_MouseClick);
            this.tbPlayerIdKey.Leave += new System.EventHandler(this.tbPlayerIdKey_Leave);
            // 
            // btnSearchAccount
            // 
            this.btnSearchAccount.Location = new System.Drawing.Point(3, 178);
            this.btnSearchAccount.Name = "btnSearchAccount";
            this.btnSearchAccount.Size = new System.Drawing.Size(75, 23);
            this.btnSearchAccount.TabIndex = 26;
            this.btnSearchAccount.Text = "查询帐号";
            this.btnSearchAccount.UseVisualStyleBackColor = true;
            this.btnSearchAccount.Click += new System.EventHandler(this.btnSearchAccount_Click);
            // 
            // btnUnlock
            // 
            this.btnUnlock.Location = new System.Drawing.Point(327, 178);
            this.btnUnlock.Name = "btnUnlock";
            this.btnUnlock.Size = new System.Drawing.Size(75, 23);
            this.btnUnlock.TabIndex = 29;
            this.btnUnlock.Text = "解封";
            this.btnUnlock.UseVisualStyleBackColor = true;
            this.btnUnlock.Click += new System.EventHandler(this.btnUnlock_Click);
            // 
            // btnKick
            // 
            this.btnKick.Location = new System.Drawing.Point(408, 178);
            this.btnKick.Name = "btnKick";
            this.btnKick.Size = new System.Drawing.Size(75, 23);
            this.btnKick.TabIndex = 30;
            this.btnKick.Text = "踢线";
            this.btnKick.UseVisualStyleBackColor = true;
            this.btnKick.Click += new System.EventHandler(this.btnKick_Click);
            // 
            // btnItemList
            // 
            this.btnItemList.Location = new System.Drawing.Point(3, 236);
            this.btnItemList.Name = "btnItemList";
            this.btnItemList.Size = new System.Drawing.Size(75, 23);
            this.btnItemList.TabIndex = 32;
            this.btnItemList.Text = "查询道具";
            this.btnItemList.UseVisualStyleBackColor = true;
            this.btnItemList.Click += new System.EventHandler(this.btnItemList_Click);
            // 
            // tbAccountKey
            // 
            this.tbAccountKey.Location = new System.Drawing.Point(3, 151);
            this.tbAccountKey.Name = "tbAccountKey";
            this.tbAccountKey.Size = new System.Drawing.Size(100, 21);
            this.tbAccountKey.TabIndex = 22;
            this.tbAccountKey.Text = "帐号";
            this.tbAccountKey.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbAccountKey_MouseClick);
            this.tbAccountKey.Leave += new System.EventHandler(this.tbAccountKey_Leave);
            // 
            // btnSearchPlayer
            // 
            this.btnSearchPlayer.Location = new System.Drawing.Point(3, 207);
            this.btnSearchPlayer.Name = "btnSearchPlayer";
            this.btnSearchPlayer.Size = new System.Drawing.Size(75, 23);
            this.btnSearchPlayer.TabIndex = 27;
            this.btnSearchPlayer.Text = "角色信息";
            this.btnSearchPlayer.UseVisualStyleBackColor = true;
            this.btnSearchPlayer.Click += new System.EventHandler(this.btnSearchPlayer_Click);
            // 
            // btnLock
            // 
            this.btnLock.Location = new System.Drawing.Point(84, 178);
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(75, 23);
            this.btnLock.TabIndex = 28;
            this.btnLock.Text = "封停";
            this.btnLock.UseVisualStyleBackColor = true;
            this.btnLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // tbActIdKey
            // 
            this.tbActIdKey.Location = new System.Drawing.Point(215, 151);
            this.tbActIdKey.Name = "tbActIdKey";
            this.tbActIdKey.Size = new System.Drawing.Size(100, 21);
            this.tbActIdKey.TabIndex = 25;
            this.tbActIdKey.Text = "帐号id";
            this.tbActIdKey.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbActIdKey_MouseClick);
            this.tbActIdKey.Leave += new System.EventHandler(this.tbActIdKey_Leave);
            // 
            // lvAccounts
            // 
            this.lvAccounts.Location = new System.Drawing.Point(0, 3);
            this.lvAccounts.Name = "lvAccounts";
            this.lvAccounts.Size = new System.Drawing.Size(824, 146);
            this.lvAccounts.TabIndex = 20;
            this.lvAccounts.UseCompatibleStateImageBehavior = false;
            this.lvAccounts.View = System.Windows.Forms.View.Details;
            this.lvAccounts.SelectedIndexChanged += new System.EventHandler(this.lvAccounts_SelectedIndexChanged);
            this.lvAccounts.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvAccounts_MouseDoubleClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lvMailReceiver);
            this.panel4.Controls.Add(this.lvSbutitles);
            this.panel4.Controls.Add(this.lvDetails);
            this.panel4.Controls.Add(this.lvPlayers);
            this.panel4.Controls.Add(this.lvAccounts);
            this.panel4.Location = new System.Drawing.Point(504, 153);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(830, 734);
            this.panel4.TabIndex = 35;
            // 
            // lvMailReceiver
            // 
            this.lvMailReceiver.Location = new System.Drawing.Point(3, 501);
            this.lvMailReceiver.Name = "lvMailReceiver";
            this.lvMailReceiver.Size = new System.Drawing.Size(251, 228);
            this.lvMailReceiver.TabIndex = 24;
            this.lvMailReceiver.UseCompatibleStateImageBehavior = false;
            this.lvMailReceiver.View = System.Windows.Forms.View.Details;
            this.lvMailReceiver.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvMailReceiver_KeyUp);
            // 
            // lvSbutitles
            // 
            this.lvSbutitles.Location = new System.Drawing.Point(260, 501);
            this.lvSbutitles.Name = "lvSbutitles";
            this.lvSbutitles.Size = new System.Drawing.Size(564, 228);
            this.lvSbutitles.TabIndex = 23;
            this.lvSbutitles.UseCompatibleStateImageBehavior = false;
            this.lvSbutitles.View = System.Windows.Forms.View.Details;
            // 
            // lvDetails
            // 
            this.lvDetails.Location = new System.Drawing.Point(0, 327);
            this.lvDetails.Name = "lvDetails";
            this.lvDetails.Size = new System.Drawing.Size(824, 168);
            this.lvDetails.TabIndex = 22;
            this.lvDetails.UseCompatibleStateImageBehavior = false;
            this.lvDetails.View = System.Windows.Forms.View.Details;
            this.lvDetails.SelectedIndexChanged += new System.EventHandler(this.lvDetails_SelectedIndexChanged);
            this.lvDetails.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvDetails_MouseDoubleClick);
            // 
            // lvPlayers
            // 
            this.lvPlayers.Location = new System.Drawing.Point(0, 155);
            this.lvPlayers.Name = "lvPlayers";
            this.lvPlayers.Size = new System.Drawing.Size(824, 166);
            this.lvPlayers.TabIndex = 21;
            this.lvPlayers.UseCompatibleStateImageBehavior = false;
            this.lvPlayers.View = System.Windows.Forms.View.Details;
            this.lvPlayers.SelectedIndexChanged += new System.EventHandler(this.lvPlayers_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnDeleteSub);
            this.panel3.Controls.Add(this.btnQuerySub);
            this.panel3.Controls.Add(this.TxtSubPassageId);
            this.panel3.Controls.Add(this.TxtSubPartId);
            this.panel3.Controls.Add(this.TxtSubScenarioId);
            this.panel3.Location = new System.Drawing.Point(12, 684);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(486, 27);
            this.panel3.TabIndex = 36;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // btnDeleteSub
            // 
            this.btnDeleteSub.Location = new System.Drawing.Point(389, 3);
            this.btnDeleteSub.Name = "btnDeleteSub";
            this.btnDeleteSub.Size = new System.Drawing.Size(94, 23);
            this.btnDeleteSub.TabIndex = 4;
            this.btnDeleteSub.Text = "删除弹幕";
            this.btnDeleteSub.UseVisualStyleBackColor = true;
            this.btnDeleteSub.Click += new System.EventHandler(this.btnDeleteSub_Click);
            // 
            // btnQuerySub
            // 
            this.btnQuerySub.Location = new System.Drawing.Point(289, 3);
            this.btnQuerySub.Name = "btnQuerySub";
            this.btnQuerySub.Size = new System.Drawing.Size(94, 23);
            this.btnQuerySub.TabIndex = 3;
            this.btnQuerySub.Text = "查询弹幕";
            this.btnQuerySub.UseVisualStyleBackColor = true;
            this.btnQuerySub.Click += new System.EventHandler(this.btnQuerySub_Click);
            // 
            // TxtSubPassageId
            // 
            this.TxtSubPassageId.Location = new System.Drawing.Point(188, 3);
            this.TxtSubPassageId.Name = "TxtSubPassageId";
            this.TxtSubPassageId.Size = new System.Drawing.Size(85, 21);
            this.TxtSubPassageId.TabIndex = 2;
            this.TxtSubPassageId.Text = "剧情-段";
            this.TxtSubPassageId.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TxtSubPassageId_MouseClick);
            this.TxtSubPassageId.Leave += new System.EventHandler(this.TxtSubPassageId_Leave);
            // 
            // TxtSubPartId
            // 
            this.TxtSubPartId.Location = new System.Drawing.Point(96, 3);
            this.TxtSubPartId.Name = "TxtSubPartId";
            this.TxtSubPartId.Size = new System.Drawing.Size(85, 21);
            this.TxtSubPartId.TabIndex = 1;
            this.TxtSubPartId.Text = "剧情-节";
            this.TxtSubPartId.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TxtSubPartId_MouseClick);
            this.TxtSubPartId.Leave += new System.EventHandler(this.TxtSubPartId_Leave);
            // 
            // TxtSubScenarioId
            // 
            this.TxtSubScenarioId.Location = new System.Drawing.Point(5, 3);
            this.TxtSubScenarioId.Name = "TxtSubScenarioId";
            this.TxtSubScenarioId.Size = new System.Drawing.Size(85, 21);
            this.TxtSubScenarioId.TabIndex = 0;
            this.TxtSubScenarioId.Text = "剧情-章";
            this.TxtSubScenarioId.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TxtSubScenarioId_MouseClick);
            this.TxtSubScenarioId.Leave += new System.EventHandler(this.TxtSubScenarioId_Leave);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txtPushInterval);
            this.panel5.Controls.Add(this.txtPusnTimes);
            this.panel5.Controls.Add(this.btnPushInst);
            this.panel5.Controls.Add(this.txtPushContent);
            this.panel5.Controls.Add(this.txtPushTitle);
            this.panel5.Controls.Add(this.txtLoseDays);
            this.panel5.Controls.Add(this.txtLosePushContent);
            this.panel5.Controls.Add(this.txtLosePushTitle);
            this.panel5.Controls.Add(this.btnPushLoseUser);
            this.panel5.Controls.Add(this.txtStaminaRecoverTitle);
            this.panel5.Controls.Add(this.txtGetStaminaTitle);
            this.panel5.Controls.Add(this.txtTimingPushTime);
            this.panel5.Controls.Add(this.txtRecoverStamina);
            this.panel5.Controls.Add(this.txtGetStamina);
            this.panel5.Controls.Add(this.btnCloseTimingPush);
            this.panel5.Controls.Add(this.btnOpenTimingPush);
            this.panel5.Location = new System.Drawing.Point(12, 717);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(486, 170);
            this.panel5.TabIndex = 37;
            // 
            // txtPushInterval
            // 
            this.txtPushInterval.Location = new System.Drawing.Point(129, 115);
            this.txtPushInterval.Name = "txtPushInterval";
            this.txtPushInterval.Size = new System.Drawing.Size(154, 21);
            this.txtPushInterval.TabIndex = 18;
            this.txtPushInterval.Text = "立即推送时间间隔，分钟";
            this.txtPushInterval.Click += new System.EventHandler(this.txtPushInterval_Click);
            this.txtPushInterval.Leave += new System.EventHandler(this.txtPushInterval_Leave);
            // 
            // txtPusnTimes
            // 
            this.txtPusnTimes.Location = new System.Drawing.Point(5, 115);
            this.txtPusnTimes.Name = "txtPusnTimes";
            this.txtPusnTimes.Size = new System.Drawing.Size(118, 21);
            this.txtPusnTimes.TabIndex = 17;
            this.txtPusnTimes.Text = "立即推送次数";
            this.txtPusnTimes.Click += new System.EventHandler(this.txtPusnTimes_Click);
            this.txtPusnTimes.Leave += new System.EventHandler(this.txtPusnTimes_Leave);
            // 
            // btnPushInst
            // 
            this.btnPushInst.Location = new System.Drawing.Point(289, 114);
            this.btnPushInst.Name = "btnPushInst";
            this.btnPushInst.Size = new System.Drawing.Size(81, 23);
            this.btnPushInst.TabIndex = 16;
            this.btnPushInst.Text = "立即推送";
            this.btnPushInst.UseVisualStyleBackColor = true;
            this.btnPushInst.Click += new System.EventHandler(this.btnPushInst_Click);
            // 
            // txtPushContent
            // 
            this.txtPushContent.Location = new System.Drawing.Point(129, 87);
            this.txtPushContent.Name = "txtPushContent";
            this.txtPushContent.Size = new System.Drawing.Size(342, 21);
            this.txtPushContent.TabIndex = 15;
            this.txtPushContent.Text = "立即推送内容";
            this.txtPushContent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtPushContent_MouseClick);
            this.txtPushContent.Leave += new System.EventHandler(this.txtPushContent_Leave);
            // 
            // txtPushTitle
            // 
            this.txtPushTitle.Location = new System.Drawing.Point(5, 87);
            this.txtPushTitle.Name = "txtPushTitle";
            this.txtPushTitle.Size = new System.Drawing.Size(118, 21);
            this.txtPushTitle.TabIndex = 14;
            this.txtPushTitle.Text = "立即推送标题";
            this.txtPushTitle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtPushTitle_MouseClick);
            this.txtPushTitle.Leave += new System.EventHandler(this.txtPushTitle_Leave);
            // 
            // txtLoseDays
            // 
            this.txtLoseDays.Location = new System.Drawing.Point(376, 115);
            this.txtLoseDays.Name = "txtLoseDays";
            this.txtLoseDays.Size = new System.Drawing.Size(95, 21);
            this.txtLoseDays.TabIndex = 13;
            this.txtLoseDays.Text = "流失天数";
            this.txtLoseDays.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtLoseDays_MouseClick);
            this.txtLoseDays.Leave += new System.EventHandler(this.txtLoseDays_Leave);
            // 
            // txtLosePushContent
            // 
            this.txtLosePushContent.Location = new System.Drawing.Point(129, 142);
            this.txtLosePushContent.Name = "txtLosePushContent";
            this.txtLosePushContent.Size = new System.Drawing.Size(241, 21);
            this.txtLosePushContent.TabIndex = 12;
            this.txtLosePushContent.Text = "流失用户推送内容";
            this.txtLosePushContent.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtLosePushContent_MouseClick);
            this.txtLosePushContent.Leave += new System.EventHandler(this.txtLosePushContent_Leave);
            // 
            // txtLosePushTitle
            // 
            this.txtLosePushTitle.Location = new System.Drawing.Point(3, 142);
            this.txtLosePushTitle.Name = "txtLosePushTitle";
            this.txtLosePushTitle.Size = new System.Drawing.Size(118, 21);
            this.txtLosePushTitle.TabIndex = 11;
            this.txtLosePushTitle.Text = "流失用户推送标题";
            this.txtLosePushTitle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtLosePushTitle_MouseClick);
            this.txtLosePushTitle.Leave += new System.EventHandler(this.txtLosePushTitle_Leave);
            // 
            // btnPushLoseUser
            // 
            this.btnPushLoseUser.Location = new System.Drawing.Point(376, 142);
            this.btnPushLoseUser.Name = "btnPushLoseUser";
            this.btnPushLoseUser.Size = new System.Drawing.Size(95, 23);
            this.btnPushLoseUser.TabIndex = 10;
            this.btnPushLoseUser.Text = "流失用户推送";
            this.btnPushLoseUser.UseVisualStyleBackColor = true;
            this.btnPushLoseUser.Click += new System.EventHandler(this.btnPushLoseUser_Click);
            // 
            // txtStaminaRecoverTitle
            // 
            this.txtStaminaRecoverTitle.Location = new System.Drawing.Point(5, 32);
            this.txtStaminaRecoverTitle.Name = "txtStaminaRecoverTitle";
            this.txtStaminaRecoverTitle.Size = new System.Drawing.Size(118, 21);
            this.txtStaminaRecoverTitle.TabIndex = 9;
            this.txtStaminaRecoverTitle.Text = "体力恢复推送标题";
            this.txtStaminaRecoverTitle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtStaminaRecoverTitle_MouseClick);
            this.txtStaminaRecoverTitle.Leave += new System.EventHandler(this.txtStaminaRecoverTitle_Leave);
            // 
            // txtGetStaminaTitle
            // 
            this.txtGetStaminaTitle.Location = new System.Drawing.Point(5, 4);
            this.txtGetStaminaTitle.Name = "txtGetStaminaTitle";
            this.txtGetStaminaTitle.Size = new System.Drawing.Size(118, 21);
            this.txtGetStaminaTitle.TabIndex = 8;
            this.txtGetStaminaTitle.Text = "领取体力推送标题";
            this.txtGetStaminaTitle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtGetStaminaTitle_MouseClick);
            this.txtGetStaminaTitle.Leave += new System.EventHandler(this.txtGetStaminaTitle_Leave);
            // 
            // txtTimingPushTime
            // 
            this.txtTimingPushTime.Location = new System.Drawing.Point(5, 60);
            this.txtTimingPushTime.Name = "txtTimingPushTime";
            this.txtTimingPushTime.Size = new System.Drawing.Size(365, 21);
            this.txtTimingPushTime.TabIndex = 5;
            this.txtTimingPushTime.Text = "定时推送时间，格式如：13：00，多个时间用逗号隔开";
            this.txtTimingPushTime.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtTimingPushTime_MouseClick);
            this.txtTimingPushTime.Leave += new System.EventHandler(this.txtTimingPushTime_Leave);
            // 
            // txtRecoverStamina
            // 
            this.txtRecoverStamina.Location = new System.Drawing.Point(129, 31);
            this.txtRecoverStamina.Name = "txtRecoverStamina";
            this.txtRecoverStamina.Size = new System.Drawing.Size(241, 21);
            this.txtRecoverStamina.TabIndex = 4;
            this.txtRecoverStamina.Text = "体力恢复已满推送内容";
            this.txtRecoverStamina.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtRecoverStamina_MouseClick);
            this.txtRecoverStamina.Leave += new System.EventHandler(this.txtRecoverStamina_Leave);
            // 
            // txtGetStamina
            // 
            this.txtGetStamina.Location = new System.Drawing.Point(129, 4);
            this.txtGetStamina.Name = "txtGetStamina";
            this.txtGetStamina.Size = new System.Drawing.Size(342, 21);
            this.txtGetStamina.TabIndex = 3;
            this.txtGetStamina.Text = "领取体力推送内容";
            this.txtGetStamina.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtGetStamina_MouseClick);
            this.txtGetStamina.Leave += new System.EventHandler(this.txtGetStamina_Leave);
            // 
            // btnCloseTimingPush
            // 
            this.btnCloseTimingPush.Location = new System.Drawing.Point(376, 56);
            this.btnCloseTimingPush.Name = "btnCloseTimingPush";
            this.btnCloseTimingPush.Size = new System.Drawing.Size(95, 23);
            this.btnCloseTimingPush.TabIndex = 1;
            this.btnCloseTimingPush.Text = "关闭定时推送";
            this.btnCloseTimingPush.UseVisualStyleBackColor = true;
            this.btnCloseTimingPush.Click += new System.EventHandler(this.btnCloseTimingPush_Click);
            // 
            // btnOpenTimingPush
            // 
            this.btnOpenTimingPush.Location = new System.Drawing.Point(376, 27);
            this.btnOpenTimingPush.Name = "btnOpenTimingPush";
            this.btnOpenTimingPush.Size = new System.Drawing.Size(95, 23);
            this.btnOpenTimingPush.TabIndex = 0;
            this.btnOpenTimingPush.Text = "开启定时推送";
            this.btnOpenTimingPush.UseVisualStyleBackColor = true;
            this.btnOpenTimingPush.Click += new System.EventHandler(this.btnOpenTimingPush_Click);
            // 
            // GmClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1346, 899);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "GmClient";
            this.Text = "GmClient";
            this.Load += new System.EventHandler(this.GmClient_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox TBPassword;
        private System.Windows.Forms.Label LBPassword;
        private System.Windows.Forms.Label LBAccount;
        private System.Windows.Forms.Label LBServerIp;
        private System.Windows.Forms.TextBox TBAccount;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Button BtnLogout;
        private System.Windows.Forms.TextBox TBOutput;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView lvAccounts;
        private System.Windows.Forms.Button btnSearchAccount;
        private System.Windows.Forms.TextBox tbActIdKey;
        private System.Windows.Forms.TextBox tbPlayerIdKey;
        private System.Windows.Forms.TextBox tbPlayerKey;
        private System.Windows.Forms.TextBox tbAccountKey;
        private System.Windows.Forms.Button btnSearchPlayer;
        private System.Windows.Forms.Button btnKick;
        private System.Windows.Forms.Button btnUnlock;
        private System.Windows.Forms.Button btnLock;
        private System.Windows.Forms.Button btnMailList;
        private System.Windows.Forms.Button btnItemList;
        private System.Windows.Forms.Button btnList;
        private System.Windows.Forms.Button btnRevert;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbInterval;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSysPostEndTime;
        private System.Windows.Forms.TextBox tbSysPostStartTime;
        private System.Windows.Forms.TextBox TBSystemPost;
        private System.Windows.Forms.Button BtnSystemPost;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tbError;
        private System.Windows.Forms.ListView lvPlayers;
        private System.Windows.Forms.ListView lvDetails;
        private System.Windows.Forms.TextBox tbDisableDendline;
        private System.Windows.Forms.Button btnChangeName;
        private System.Windows.Forms.TextBox tbChangeName;
        private System.Windows.Forms.Button btnItemSubmit;
        private System.Windows.Forms.Button btnItemEdit;
        private System.Windows.Forms.TextBox tbMailTitle;
        private System.Windows.Forms.TextBox tbMailContent;
        private System.Windows.Forms.Button btnSendMail;
        private System.Windows.Forms.Button btnBroadcastMail;
        private System.Windows.Forms.ListView lvAccessories;
        private System.Windows.Forms.Button btnAccessory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbLogEnd;
        private System.Windows.Forms.TextBox tbLogStart;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Button btnReceiver;
        private System.Windows.Forms.Button btnItemAdd;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox TxtSubScenarioId;
        private System.Windows.Forms.TextBox TxtSubPartId;
        private System.Windows.Forms.TextBox TxtSubPassageId;
        private System.Windows.Forms.Button btnQuerySub;
        private System.Windows.Forms.Button btnDeleteSub;
        private System.Windows.Forms.ListView lvSbutitles;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPlatform;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnOpenTimingPush;
        private System.Windows.Forms.Button btnCloseTimingPush;
        private System.Windows.Forms.TextBox txtGetStamina;
        private System.Windows.Forms.TextBox txtRecoverStamina;
        private System.Windows.Forms.TextBox txtTimingPushTime;
        private System.Windows.Forms.TextBox txtStaminaRecoverTitle;
        private System.Windows.Forms.TextBox txtGetStaminaTitle;
        private System.Windows.Forms.Button buttonBroadcast;
        private System.Windows.Forms.Button buttonCheckReload;
        private System.Windows.Forms.TextBox textBoxCheckReloadTime;
        private System.Windows.Forms.Button buttonGmList;
        private System.Windows.Forms.Button buttonGmRegister;
        private System.Windows.Forms.Button buttonGmConfig;
        private System.Windows.Forms.TextBox txtLoseDays;
        private System.Windows.Forms.TextBox txtLosePushContent;
        private System.Windows.Forms.TextBox txtLosePushTitle;
        private System.Windows.Forms.Button btnPushLoseUser;
        private System.Windows.Forms.Button buttonRedeemCode;
        private System.Windows.Forms.Button buttonRedeemCodeSearch;
        private System.Windows.Forms.Button buttonUploadLog;
        private System.Windows.Forms.TextBox txtPushTitle;
        private System.Windows.Forms.TextBox txtPushContent;
        private System.Windows.Forms.Button btnPushInst;
        private System.Windows.Forms.TextBox txtPusnTimes;
        private System.Windows.Forms.TextBox txtPushInterval;
        private System.Windows.Forms.Button btnImportRecip;
        private System.Windows.Forms.ListView lvMailReceiver;
        private System.Windows.Forms.TextBox txtMailSendTime;
        private System.Windows.Forms.Button btnOrderRecord;
        private System.Windows.Forms.ComboBox comboBoxServer;
        private System.Windows.Forms.Button BtnGmSearch;
        private System.Windows.Forms.TextBox txtBoxMailExpri;
        private System.Windows.Forms.Button btnPreMailDelete;
        private System.Windows.Forms.Button btnPreMailSearch;

    }
}

