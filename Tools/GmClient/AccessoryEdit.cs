﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GmClient
{
    public partial class AccessoryEdit : Form
    {
        //////////////////////////////////////////////////////////////////////////
        public AccessoryEdit(int itemId, string attr, int count)
        {
            InitializeComponent();

            cbItem.Items.Add("");
            foreach (var val in GmClient.ItemDatas)
            {
                string text = string.Format("{0} {1}", val.Key, val.Value);
                cbItem.Items.Add(text);
                if (itemId == val.Key)
                {
                    cbItem.SelectedItem = text;
                }
            }

            cbAttr.Items.Add("");
            foreach (var val in GmClient.AttrDatas)
            {
                string text = string.Format("{0} {1}", val.Key, val.Value);
                cbAttr.Items.Add(text);
                if (attr == val.Key)
                {
                    cbAttr.SelectedItem = text;
                }
            }

            tbCount.Text = count.ToString();

            btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public int ItemId
        {
            get
            {
                string item = (string)cbItem.SelectedItem;
                if (!string.IsNullOrEmpty(item))
                {
                    string[] s = item.Split(' ');
                    return int.Parse(s[0]);
                }
                return 0;
            }
        }

        public string Attr
        {
            get
            {
                string attr = (string)cbAttr.SelectedItem;
                if (!string.IsNullOrEmpty(attr))
                {
                    string[] s = attr.Split(' ');
                    return s[1];
                }
                return "";
            }
        }

        public int Count
        {
            get
            {
                return int.Parse(tbCount.Text);
            }
        }

        //////////////////////////////////////////////////////////////////////////
        private void btnMinus_Click(object sender, EventArgs e)
        {
            int count = int.Parse(tbCount.Text);
            if (count > 0)
            {
                count--;
                tbCount.Text = count.ToString();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int count = int.Parse(tbCount.Text);
            count++;
            tbCount.Text = count.ToString();
        }

        private void cbItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            string item = (string)cbItem.SelectedItem;
            if (!string.IsNullOrEmpty(item))
            {
                cbAttr.SelectedItem = "";
            }
        }

        private void cbAttr_SelectedIndexChanged(object sender, EventArgs e)
        {
            string attr = (string)cbAttr.SelectedItem;
            if (!string.IsNullOrEmpty(attr))
            {
                cbItem.SelectedItem = "";
            }
        }
    }
}
