﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GmClient
{
    public partial class ItemEdit : Form
    {
        //////////////////////////////////////////////////////////////////////////
        public ItemEdit(int itemId, string itemName, int itemCount)
        {
            InitializeComponent();

            tbItemId.Text = itemId.ToString();
            tbItemName.Text = itemName;
            tbItemCount.Text = itemCount.ToString();

            btnItemOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnItemCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public int ItemCount
        {
            get
            {
                return int.Parse(tbItemCount.Text);
            }
        }

        //////////////////////////////////////////////////////////////////////////
        private void btnMinusItem_Click(object sender, EventArgs e)
        {
            int itemCount = int.Parse(tbItemCount.Text);
            if (itemCount > 0)
            {
                itemCount--;
                tbItemCount.Text = itemCount.ToString();
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            int itemCount = int.Parse(tbItemCount.Text);
            itemCount++;
            tbItemCount.Text = itemCount.ToString();
        }
    }
}
