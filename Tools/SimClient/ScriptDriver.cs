﻿/* ============================================================
* Author:       xieqin
* Time:         2014/11/28 11:18:27
* FileName:     ScriptDriver
* Purpose:      脚本解释器
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;
using System.IO;
using Script;

namespace SimClient
{
    /// <summary>
    /// 脚本解释器
    /// </summary>
    public class ScriptDriver
    {
        CompilerResults results = null;
        List<string> scriptList = new List<string>();
        string path;

        /// <summary>
        /// 加载脚本
        /// </summary>
        /// <param name="scriptPath"></param>
        public void LoadScript(string scriptPath)
        {
            path = scriptPath;

            DirectoryInfo folder = new DirectoryInfo(path);
            loadDirectory(folder);
        }

        void loadDirectory(DirectoryInfo folder)
        {
            foreach (FileInfo file in folder.GetFiles())
            {
                if (file.Extension == ".cs")
                {
                    scriptList.Add(file.FullName);
                }
            }

            foreach (DirectoryInfo directory in folder.GetDirectories())
            {
                loadDirectory(directory);
            }
        }

        /// <summary>
        /// 编译脚本
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public bool CompileScript(out string errorMsg)
        {
            using (var provider = new CSharpCodeProvider())
            {
                CompilerParameters options = new CompilerParameters();
                options.GenerateInMemory = true;
                options.ReferencedAssemblies.Add("System.dll");
                options.ReferencedAssemblies.Add("System.Core.dll");
                options.ReferencedAssemblies.Add("System.Data.dll");
                options.ReferencedAssemblies.Add("System.Data.DataSetExtensions.dll");
                options.ReferencedAssemblies.Add("System.Xml.dll");
                options.ReferencedAssemblies.Add("System.Xml.Linq.dll");
                options.ReferencedAssemblies.Add("Utility.dll");
                options.ReferencedAssemblies.Add("ServerLib.dll");
                options.ReferencedAssemblies.Add("log4net.dll");
                options.ReferencedAssemblies.Add("protobuf-net.dll");
                options.ReferencedAssemblies.Add("CommonLib.dll");
                options.ReferencedAssemblies.Add("ItemComponent.dll");
                options.ReferencedAssemblies.Add("ScriptContext.dll");
                options.ReferencedAssemblies.Add("Protocols.dll");
                options.ReferencedAssemblies.Add("LogicLib.dll");

                StringBuilder errMsg = new StringBuilder();

                string[] scripts = new string[scriptList.Count];
                for (int i = 0; i < scriptList.Count; i++)
                {
                    scripts[i] = scriptList[i];
                }
                results = provider.CompileAssemblyFromFile(options, scripts);
                if (results.Errors.HasErrors)
                {
                    foreach (CompilerError error in results.Errors)
                    {
                        errMsg.AppendFormat("{0} {1}", error.Line, error.ErrorText);
                    }
                    errorMsg = errMsg.ToString();
                    return false;
                }
                errorMsg = "No error!";
                return true;
            }
        }

        /// <summary>
        /// 注册脚本信息
        /// </summary>
        /// <param name="ctx"></param>
        public void RegisterScripts(Context ctx, List<string> scripts = null)
        {
            if (scripts == null)
            {
                foreach (Type type in results.CompiledAssembly.GetTypes())
                {
                    MethodInfo method = type.GetMethod("Run");
                    if (method != null)
                    {
                        ctx.RegisterScript(type.Name, type, method);
                    }
                }
            }
            else
            {
                foreach (string script in scripts)
                {
                    string name = string.Format("Script.{0}", script);
                    Type type = results.CompiledAssembly.GetType(name);
                    if (type != null)
                    {
                        MethodInfo method = type.GetMethod("Run");
                        if (method != null)
                        {
                            ctx.RegisterScript(type.Name, type, method);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 初始化脚本
        /// </summary>
        /// <param name="ctx"></param>
        public void InitScripts(Context ctx, List<string> scripts = null)
        {
            if (scripts == null)
            {
                foreach (Type type in results.CompiledAssembly.GetTypes())
                {
                    MethodInfo method = type.GetMethod("Init");
                    if (method != null)
                    {
                        object[] objects = { ctx };
                        method.Invoke(null, objects);
                    }
                }
            }
            else
            {
                foreach (string script in scripts)
                {
                    string name = string.Format("Script.{0}", script);
                    Type type = results.CompiledAssembly.GetType(name);
                    if (type != null)
                    {
                        MethodInfo method = type.GetMethod("Init");
                        if (method != null)
                        {
                            object[] objects = { ctx };
                            method.Invoke(null, objects);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 运行脚本
        /// </summary>
        /// <param name="scripts"></param>
        /// <param name="ctx"></param>
        public void RunScript(string[] scripts, Context ctx, string[] param, ManualResetEvent mrEv = null)
        {
            if (results != null)
            {
                for (int i = 0; i < scripts.Length; i++)
                {
                    string script = scripts[i];
                    string totalClassName = "Script." + script;
                    Type type = results.CompiledAssembly.GetType(totalClassName);
                    MethodInfo method = type.GetMethod("Run");
                    if (method != null)
                    {
                        try
                        {
                            object[] objects = { ctx, param[i] };
                            method.Invoke(null, objects);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("[ScriptDriver.RunScript]script: {0} Exception: {1}", script, ex.Message);
                        }
                    }
                }

                if (mrEv != null)
                {
                    mrEv.Set();
                }
            }
        }

        public void RunScript(bool nonstop, string[] scripts, Context[] ctxs, string[] param, ManualResetEvent mrEv = null)
        {
            if (results != null)
            {
                foreach (Context ctx in ctxs)
                {
                    for (int i = 0; i < scripts.Length; i++)
                    {
                        ScriptData data = new ScriptData();
                        data.Index = i;
                        data.Script = scripts[i];
                        data.Param = param[i];
                        ctx.Scripts.AddLast(data);
                        ctx.AddState(0);
                    }
                }

                while (true)
                {
                    bool finish = true;
                    foreach (Context ctx in ctxs)
                    {
                        if (ctx.Enable)
                        {
                            if (ctx.Scripts.Count > 0)
                            {
                                finish = false;

                                ScriptData data = ctx.Scripts.First.Value;
                                ctx.Scripts.RemoveFirst();

                                if (nonstop && ctx.Scripts.Count == 0)
                                {
                                    for (int i = 0; i < scripts.Length; i++)
                                    {
                                        ScriptData scp = new ScriptData();
                                        scp.Index = i;
                                        scp.Script = scripts[i];
                                        scp.Param = param[i];
                                        ctx.Scripts.AddLast(data);
                                        ctx.AddState(0);
                                    }
                                }

                                string totalClassName = "Script." + data.Script;
                                //[10/22/2015 史耀力] TODO 查找不到脚本要正确处理 
                                Type type = results.CompiledAssembly.GetType(totalClassName);
                                MethodInfo method = type.GetMethod("Run");
                                if (method != null)
                                {
                                    try
                                    {
                                        ctx.SetRunningScript(data.Index);
                                        object[] objects = { ctx, data.Param };
                                        method.Invoke(null, objects);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("[ScriptDriver.RunScript]script: {0} Exception: {1}", data.Script, ex.Message);
                                    }
                                }
                            }
                            else
                            {
                                ctx.SetRunningScript(-1);
                            }
                        }
                        else
                        {
                            finish = false;
                        }
                    }

                    Thread.Sleep(200);

                    if (finish)
                        break;
                }

                if (mrEv != null)
                {
                    mrEv.Set();
                }
            }
        }
    }
}
