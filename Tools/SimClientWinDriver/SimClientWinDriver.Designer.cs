﻿namespace SimClientWinDriver
{
    partial class SimClientWinDriver
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonConfig = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.buttonExit = new System.Windows.Forms.Button();
            this.listViewScript = new System.Windows.Forms.ListView();
            this.buttonAddScript = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.textBoxAccountCount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxThreadCount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxPlayer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAccount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.listViewReport = new System.Windows.Forms.ListView();
            this.buttonRunTimes = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonRunTimes);
            this.panel1.Controls.Add(this.buttonConfig);
            this.panel1.Controls.Add(this.buttonLogout);
            this.panel1.Controls.Add(this.buttonLogin);
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.textBoxPort);
            this.panel1.Controls.Add(this.buttonExit);
            this.panel1.Controls.Add(this.listViewScript);
            this.panel1.Controls.Add(this.buttonAddScript);
            this.panel1.Controls.Add(this.buttonRun);
            this.panel1.Controls.Add(this.buttonLoad);
            this.panel1.Controls.Add(this.textBoxAccountCount);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.textBoxThreadCount);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textBoxPassword);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.textBoxId);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.textBoxPlayer);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.textBoxAccount);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBoxServer);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(243, 574);
            this.panel1.TabIndex = 0;
            // 
            // buttonConfig
            // 
            this.buttonConfig.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonConfig.Location = new System.Drawing.Point(3, 219);
            this.buttonConfig.Name = "buttonConfig";
            this.buttonConfig.Size = new System.Drawing.Size(237, 23);
            this.buttonConfig.TabIndex = 58;
            this.buttonConfig.Text = "配置";
            this.buttonConfig.UseVisualStyleBackColor = true;
            this.buttonConfig.Click += new System.EventHandler(this.buttonConfig_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonLogout.Location = new System.Drawing.Point(3, 277);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(237, 23);
            this.buttonLogout.TabIndex = 57;
            this.buttonLogout.Text = "登出";
            this.buttonLogout.UseVisualStyleBackColor = true;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // buttonLogin
            // 
            this.buttonLogin.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonLogin.Location = new System.Drawing.Point(3, 248);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(237, 23);
            this.buttonLogin.TabIndex = 56;
            this.buttonLogin.Text = "登录";
            this.buttonLogin.UseVisualStyleBackColor = true;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLogin_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(3, 461);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(237, 23);
            this.buttonSave.TabIndex = 55;
            this.buttonSave.Text = "保存配置";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(45, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 54;
            this.label8.Text = "port";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(80, 30);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(160, 21);
            this.textBoxPort.TabIndex = 53;
            this.textBoxPort.Text = "10000";
            // 
            // buttonExit
            // 
            this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonExit.Location = new System.Drawing.Point(3, 548);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(237, 23);
            this.buttonExit.TabIndex = 52;
            this.buttonExit.Text = "退出";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // listViewScript
            // 
            this.listViewScript.FullRowSelect = true;
            this.listViewScript.Location = new System.Drawing.Point(3, 306);
            this.listViewScript.Name = "listViewScript";
            this.listViewScript.Size = new System.Drawing.Size(237, 91);
            this.listViewScript.TabIndex = 50;
            this.listViewScript.UseCompatibleStateImageBehavior = false;
            this.listViewScript.View = System.Windows.Forms.View.Details;
            this.listViewScript.KeyUp += new System.Windows.Forms.KeyEventHandler(this.listViewScript_KeyUp);
            this.listViewScript.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewScript_MouseDoubleClick);
            // 
            // buttonAddScript
            // 
            this.buttonAddScript.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonAddScript.Location = new System.Drawing.Point(3, 403);
            this.buttonAddScript.Name = "buttonAddScript";
            this.buttonAddScript.Size = new System.Drawing.Size(237, 23);
            this.buttonAddScript.TabIndex = 18;
            this.buttonAddScript.Text = "添加脚本";
            this.buttonAddScript.UseVisualStyleBackColor = true;
            this.buttonAddScript.Click += new System.EventHandler(this.buttonAddScript_Click);
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(3, 490);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(237, 23);
            this.buttonRun.TabIndex = 17;
            this.buttonRun.Text = "运行";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(3, 432);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(237, 23);
            this.buttonLoad.TabIndex = 15;
            this.buttonLoad.Text = "读取配置";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // textBoxAccountCount
            // 
            this.textBoxAccountCount.Location = new System.Drawing.Point(80, 192);
            this.textBoxAccountCount.Name = "textBoxAccountCount";
            this.textBoxAccountCount.Size = new System.Drawing.Size(160, 21);
            this.textBoxAccountCount.TabIndex = 13;
            this.textBoxAccountCount.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 195);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "帐号数量";
            // 
            // textBoxThreadCount
            // 
            this.textBoxThreadCount.Location = new System.Drawing.Point(80, 165);
            this.textBoxThreadCount.Name = "textBoxThreadCount";
            this.textBoxThreadCount.Size = new System.Drawing.Size(160, 21);
            this.textBoxThreadCount.TabIndex = 11;
            this.textBoxThreadCount.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 10;
            this.label7.Text = "线程数量";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(80, 84);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(160, 21);
            this.textBoxPassword.TabIndex = 9;
            this.textBoxPassword.Text = "password";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "密码";
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(80, 138);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.Size = new System.Drawing.Size(160, 21);
            this.textBoxId.TabIndex = 7;
            this.textBoxId.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "Id起始";
            // 
            // textBoxPlayer
            // 
            this.textBoxPlayer.Location = new System.Drawing.Point(80, 111);
            this.textBoxPlayer.Name = "textBoxPlayer";
            this.textBoxPlayer.Size = new System.Drawing.Size(160, 21);
            this.textBoxPlayer.TabIndex = 5;
            this.textBoxPlayer.Text = "avatar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "角色前缀";
            // 
            // textBoxAccount
            // 
            this.textBoxAccount.Location = new System.Drawing.Point(80, 57);
            this.textBoxAccount.Name = "textBoxAccount";
            this.textBoxAccount.Size = new System.Drawing.Size(160, 21);
            this.textBoxAccount.TabIndex = 3;
            this.textBoxAccount.Text = "account";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "帐号前缀";
            // 
            // textBoxServer
            // 
            this.textBoxServer.Location = new System.Drawing.Point(80, 3);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(160, 21);
            this.textBoxServer.TabIndex = 1;
            this.textBoxServer.Text = "127.0.0.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务器ip";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBoxOutput);
            this.panel2.Controls.Add(this.listViewReport);
            this.panel2.Location = new System.Drawing.Point(261, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(907, 574);
            this.panel2.TabIndex = 1;
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Location = new System.Drawing.Point(3, 290);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.Size = new System.Drawing.Size(901, 281);
            this.textBoxOutput.TabIndex = 52;
            // 
            // listViewReport
            // 
            this.listViewReport.FullRowSelect = true;
            this.listViewReport.Location = new System.Drawing.Point(3, 3);
            this.listViewReport.Name = "listViewReport";
            this.listViewReport.Size = new System.Drawing.Size(901, 281);
            this.listViewReport.TabIndex = 51;
            this.listViewReport.UseCompatibleStateImageBehavior = false;
            this.listViewReport.View = System.Windows.Forms.View.Details;
            // 
            // buttonRunTimes
            // 
            this.buttonRunTimes.Location = new System.Drawing.Point(3, 519);
            this.buttonRunTimes.Name = "buttonRunTimes";
            this.buttonRunTimes.Size = new System.Drawing.Size(237, 23);
            this.buttonRunTimes.TabIndex = 59;
            this.buttonRunTimes.Text = "持续运行";
            this.buttonRunTimes.UseVisualStyleBackColor = true;
            this.buttonRunTimes.Click += new System.EventHandler(this.buttonRunTimes_Click);
            // 
            // SimClientWinDriver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 598);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "SimClientWinDriver";
            this.Text = "SimClientWinDriver";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPlayer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAccount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxId;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAccountCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxThreadCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonAddScript;
        private System.Windows.Forms.ListView listViewScript;
        private System.Windows.Forms.ListView listViewReport;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.Button buttonLogout;
        private System.Windows.Forms.Button buttonConfig;
        private System.Windows.Forms.Button buttonRunTimes;
    }
}

