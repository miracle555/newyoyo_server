﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimClientWinDriver
{
    public partial class ScriptEdit : Form
    {
        public ScriptEdit(string script = "", string param = "")
        {
            InitializeComponent();

            textBoxScript.Text = script;
            textBoxParam.Text = param;

            btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public string Script
        {
            get
            {
                return textBoxScript.Text;
            }
        }

        public string Param
        {
            get
            {
                return textBoxParam.Text;
            }
        }
    }
}
