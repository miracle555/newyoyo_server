﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Protocols;
using Protocols.Error;
using Script;
using SimClient;
using SimClientDriver;

namespace SimClientWinDriver
{
    public partial class SimClientWinDriver : Form
    {
        /// <summary>
        /// 启动控制台
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern bool AllocConsole();
        /// <summary>
        /// 释放控制台
        /// </summary>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern bool FreeConsole();

        long OnlinePlayers;
        long OfflinePlayers;
        long OnlinePlayersShow;
        long OfflinePlayersShow;
        ScriptConfig config;
        ScriptDriver driver;
        List<Context> contexts = new List<Context>();
        List<Thread> threads = new List<Thread>();
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public SimClientWinDriver()
        {
            InitializeComponent();

            InitForm();
        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams _createParams = base.CreateParams;
                _createParams.ClassStyle = _createParams.ClassStyle | CP_NOCLOSE_BUTTON;
                return _createParams;
            }
        }

        void InitForm()
        {
            listViewScript.Columns.Add("Script", "Script", 160);
            listViewScript.Columns.Add("Param", "Param", 60);

            listViewReport.Columns.Add("Script", "Script", 160);
            listViewReport.Columns.Add("Running", "Running");
            listViewReport.Columns.Add("Success", "Success");
            listViewReport.Columns.Add("Failed", "Failed");
            listViewReport.Columns.Add("ResponseAvg", "ResponseAvg(ms)", 120);
            listViewReport.Columns.Add("ResponseMax", "ResponseMax(ms)", 120);

            timer.Interval = 2000;
            timer.Tick += Tick;
            timer.Start();

            buttonRun.Enabled = true;

            AllocConsole();
        }

        void Tick(object sender, EventArgs e)
        {
            long online = Interlocked.Read(ref OnlinePlayers);
            long offline = Interlocked.Read(ref OfflinePlayers);
            if (online != OnlinePlayersShow || offline != OfflinePlayersShow)
            {
                if (online == 0 && offline == 0)
                {
                    buttonLogin.Text = "登录";
                }
                else
                {
                    buttonLogin.Text = string.Format("登录 online {0} offline {1}", online, offline);
                }

                if (online != OnlinePlayersShow && !buttonLogin.Enabled && contexts.Count > 0)
                {
                    if (online == contexts.Count)
                    {
                        output("所有玩家登录成功");
                    }
                }

                OnlinePlayersShow = online;
                OfflinePlayersShow = offline;
            }

            if (!buttonRun.Enabled || !buttonRunTimes.Enabled && listViewReport.Items.Count > 0)
            {
                bool finish = true;

                listViewReport.BeginUpdate();
                foreach (ListViewItem item in listViewReport.Items)
                {
                    int index = listViewReport.Columns["Script"].Index;
                    string script = item.SubItems[index].Text;

                    int running = 0;
                    int success = 0;
                    int failed = 0;
                    TimeSpan response = new TimeSpan();
                    TimeSpan responseMax = new TimeSpan();
                    int responseCount = 0;
                    foreach (Context ctx in contexts)
                    {
                        if (ctx.GetRunningScript() != -1)
                        {
                            finish = false;
                        }

                        if (ctx.GetRunningScript() == item.Index)
                        {
                            running++;
                        }

                        bool bSuccess = false;
                        if (ctx.GetScriptState(item.Index, out bSuccess))
                        {
                            if (bSuccess)
                                success++;
                            else
                                failed++;
                        }

                        if (item.Index == 0)
                        {
                            TimeSpan span = ctx.GetSpanMax();
                            if (span > responseMax)
                            {
                                responseMax = span;
                            }

                            int count = ctx.GetSpans(out span);
                            if (count > 0)
                            {
                                responseCount += count;
                                response += span;
                            }
                        }
                    }

                    index = listViewReport.Columns["Running"].Index;
                    item.SubItems[index].Text = running.ToString();
                    index = listViewReport.Columns["Success"].Index;
                    item.SubItems[index].Text = success.ToString();
                    index = listViewReport.Columns["Failed"].Index;
                    item.SubItems[index].Text = failed.ToString();

                    if (responseCount > 0)
                    {
                        int avg = (int)(response.TotalMilliseconds / responseCount);

                        index = listViewReport.Columns["ResponseAvg"].Index;
                        item.SubItems[index].Text = avg.ToString();
                        index = listViewReport.Columns["ResponseMax"].Index;
                        item.SubItems[index].Text = responseMax.TotalMilliseconds.ToString();
                    }

                }
                listViewReport.EndUpdate();

                if (finish)
                {
                    buttonRun.Enabled = true;
                    buttonRunTimes.Enabled = true;
                    threads.Clear();
                    output("运行结束");
                }
            }
        }

        void output(string msg)
        {
            textBoxOutput.Text += string.Format("[{0}]{1}", DateTime.Now.ToString(), msg) + Environment.NewLine;
        }

        void LoginThreadFunc(Context[] ctxs)
        {
            foreach (Context ctx in ctxs)
            {
                while (true)
                {
                    try
                    {
                        ctx.Connect();
                        break;
                    }
                    catch (Exception)
                    {
                        Thread.Sleep(200);
                    }
                }

                object account = new object();
                ctx.LoadData("account", out account);
                object password = new object();
                ctx.LoadData("password", out password);

                ctx.RegisterMessageHandler(HandleLogin);

                UserLoginReq req = new UserLoginReq();
                req.platformType = 0;
                req.account = (string)account;
                req.password = (string)password;
                ctx.SendMessage(req);
            }
        }

        void HandleLogin(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            object account = new object();
            ctx.LoadData("account", out account);
            if (proto.GetID() == ProtoID.CMD_LOGIN_USERLOGIN_RSP)
            {
                UserLoginRsp loginRsp = (UserLoginRsp)proto.GetBody();
                if (loginRsp.errorCode != ErrorID.Success)
                {
                    Interlocked.Increment(ref OfflinePlayers);
                }
                else
                {
                    ctx.SaveData("roleID", loginRsp.roleID);
                    Interlocked.Increment(ref OnlinePlayers);
                }
            }
        }

        void ClearReport()
        {
            foreach (ListViewItem item in listViewReport.Items)
            {
                int index = listViewReport.Columns["Running"].Index;
                item.SubItems[index].Text = "0";
                index = listViewReport.Columns["Success"].Index;
                item.SubItems[index].Text = "0";
                index = listViewReport.Columns["Failed"].Index;
                item.SubItems[index].Text = "0";
                index = listViewReport.Columns["ResponseAvg"].Index;
                item.SubItems[index].Text = "0";
                index = listViewReport.Columns["ResponseMax"].Index;
                item.SubItems[index].Text = "0";
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            listViewScript.Items.Clear();

            using (FileStream fs = new FileStream(@"..\ClientScript\Config.xml", FileMode.Open))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SimConfig));
                SimConfig config = (SimConfig)serializer.Deserialize(fs);

                if (config.Entrys.Count > 0)
                {
                    string ip = config.Entrys[0].Ip;
                    int port = config.Entrys[0].Port;
                    textBoxServer.Text = ip;
                    textBoxPort.Text = port.ToString();
                    textBoxThreadCount.Text = config.Entrys.Count.ToString();
                    textBoxAccountCount.Text = config.Entrys[0].Accounts.Count.ToString();
                    textBoxPassword.Text = config.Entrys[0].Accounts[0].Password;

                    foreach (var script in config.Entrys[0].Scripts)
                    {
                        int index = listViewScript.Columns["Script"].Index;
                        ListViewItem item = new ListViewItem();
                        item.Text = script.script;

                        if (!string.IsNullOrEmpty(script.Param))
                        {
                            index = listViewScript.Columns["Param"].Index;
                            item.SubItems.Add(script.Param);
                        }
                        else
                        {
                            item.SubItems.Add("");
                        }

                        listViewScript.Items.Add(item);
                    }
                }
            }
            output("配置读取成功");
        }

        private void listViewScript_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (listViewScript.SelectedItems.Count > 0)
                {
                    foreach (ListViewItem item in listViewScript.SelectedItems)
                    {
                        listViewScript.Items.Remove(item);
                    }
                }
            }
        }

        private void listViewScript_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = null;
            if (listViewScript.SelectedItems.Count > 0)
            {
                item = listViewScript.SelectedItems[0];
            }

            if (item == null)
                return;

            int index = listViewScript.Columns["Script"].Index;
            string script = item.SubItems[index].Text;
            index = listViewScript.Columns["Param"].Index;
            string param = item.SubItems[index].Text;

            ScriptEdit form = new ScriptEdit(script, param);
            form.StartPosition = FormStartPosition.CenterParent;
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK
                && !string.IsNullOrEmpty(form.Script))
            {
                index = listViewScript.Columns["Script"].Index;
                item.SubItems[index].Text = form.Script;
                index = listViewScript.Columns["Param"].Index;
                item.SubItems[index].Text = form.Param;
            }
        }

        private void buttonAddScript_Click(object sender, EventArgs e)
        {
            ScriptEdit form = new ScriptEdit();
            form.StartPosition = FormStartPosition.CenterParent;
            if (form.ShowDialog(this) == System.Windows.Forms.DialogResult.OK
                && !string.IsNullOrEmpty(form.Script))
            {
                int index = -1;
                if (listViewScript.SelectedItems.Count > 0)
                {
                    index = listViewScript.SelectedItems[0].Index;
                }

                ListViewItem item = new ListViewItem();
                item.Text = form.Script;
                item.SubItems.Add(form.Param);

                if (index == -1)
                {
                    listViewScript.Items.Add(item);
                }
                else
                {
                    listViewScript.Items.Insert(index, item);
                }
            }
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            buttonRun.Enabled = false;

            output("开始运行");

            ClearReport();

            List<string> scripts = new List<string>();
            List<string> param = new List<string>();
            foreach (ListViewItem item in listViewScript.Items)
            {
                int index = listViewScript.Columns["Script"].Index;
                string script = item.SubItems[index].Text;
                index = listViewScript.Columns["Param"].Index;
                string para = item.SubItems[index].Text;

                scripts.Add(script);
                param.Add(para);
            }

            threads.Clear();
            foreach (ScriptConfig.Pipeline pipeline in config.Pipelines)
            {
                Context[] ctxs = new Context[pipeline.ctxs.Count];
                for (int i = 0; i < pipeline.ctxs.Count; i++)
                {
                    ctxs[i] = pipeline.ctxs[i];
                }
                threads.Add(new Thread(new ThreadStart(() => driver.RunScript(false, scripts.ToArray(), ctxs, param.ToArray()))));
            }

            foreach (Thread thread in threads)
            {
                thread.Start();

                Thread.Sleep(100);
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            listViewReport.Items.Clear();

            using (FileStream fs = new FileStream(@"..\ClientScript\Config.xml", FileMode.Create))
            {
                SimConfig config = new SimConfig();

                int thrdCount = 0;
                if (!int.TryParse(textBoxThreadCount.Text, out thrdCount))
                {
                    thrdCount = 1;
                }

                int acctCount = 0;
                if (!int.TryParse(textBoxAccountCount.Text, out acctCount))
                {
                    acctCount = 1;
                }

                int idx = 0;
                for (int i = 0; i < thrdCount; i++)
                {
                    SimEntry entry = new SimEntry();
                    entry.Ip = textBoxServer.Text;
                    entry.Port = int.Parse(textBoxPort.Text);
                    foreach (ListViewItem item in listViewScript.Items)
                    {
                        int index = listViewScript.Columns["Script"].Index;
                        string script = item.SubItems[index].Text;
                        index = listViewScript.Columns["Param"].Index;
                        string param = item.SubItems[index].Text;

                        SimScript scpt = new SimScript();
                        scpt.script = script;
                        scpt.Param = string.IsNullOrEmpty(param) ? null : param;
                        entry.Scripts.Add(scpt);
                    }

                    for (int j = 0; j < acctCount; j++)
                    {
                        ++idx;
                        SimAccount acct = new SimAccount();
                        acct.Account = string.Format("{0}{1}",
                            textBoxAccount.Text, idx + int.Parse(textBoxId.Text));
                        acct.Password = textBoxPassword.Text;
                        acct.Role = string.Format("{0}{1}",
                            textBoxPlayer.Text, idx + int.Parse(textBoxId.Text));
                        acct.Password = textBoxPassword.Text;
                        entry.Accounts.Add(acct);
                    }
                    config.Entrys.Add(entry);
                }

                XmlSerializer serializer = new XmlSerializer(typeof(SimConfig));
                serializer.Serialize(fs, config);
            }

            output("配置保存结束");
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            buttonLogin.Enabled = false;
            buttonLogout.Enabled = true;

            OnlinePlayers = 0;
            OfflinePlayers = 0;

            ClearReport();

            threads.Clear();
            foreach (ScriptConfig.Pipeline pipeline in config.Pipelines)
            {
                Context[] ctxs = new Context[pipeline.ctxs.Count];
                for (int i = 0; i < pipeline.ctxs.Count; i++)
                {
                    ctxs[i] = pipeline.ctxs[i];
                }
                threads.Add(new Thread(new ThreadStart(() => LoginThreadFunc(ctxs))));
            }

            output("开始登录");

            foreach (Thread thread in threads)
            {
                thread.Start();

                Thread.Sleep(100);
            }
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            buttonLogin.Enabled = true;
            buttonLogout.Enabled = false;

            ClearReport();

            foreach (Context ctx in contexts)
            {
                ctx.Disconnect();
            }

            OnlinePlayers = 0;
            OfflinePlayers = 0;

            driver = null;
            config = null;
            contexts.Clear();
            threads.Clear();
        }

        private void buttonConfig_Click(object sender, EventArgs e)
        {
            List<string> scripts = new List<string>();
            listViewReport.Items.Clear();
            foreach (ListViewItem item in listViewScript.Items)
            {
                int index = listViewScript.Columns["Script"].Index;
                scripts.Add(item.SubItems[index].Text);

                ListViewItem it = new ListViewItem();
                it.Text = item.SubItems[index].Text;
                it.SubItems.Add("0");
                it.SubItems.Add("0");
                it.SubItems.Add("0");
                it.SubItems.Add("0");
                it.SubItems.Add("0");
                listViewReport.Items.Add(it);
            }

            string errorMsg;
            driver = new ScriptDriver();
            driver.LoadScript(@"..\ClientScript");
            output("开始编译脚本");

            if (!driver.CompileScript(out errorMsg))
            {
                output(string.Format("编译失败: {0}", errorMsg));
            }
            else
            {
                output("编译完成，开始脚本初始化");

                config = new ScriptConfig();
                config.Load(@"..\ClientScript\Config.xml");

                foreach (ScriptConfig.Pipeline pipeline in config.Pipelines)
                {
                    foreach (Context ctx in pipeline.ctxs)
                    {
                        driver.RegisterScripts(ctx, scripts);
                        driver.InitScripts(ctx, scripts);
                        contexts.Add(ctx);
                    }
                }

                output("脚本初始化完成");
            }
        }

        private void buttonRunTimes_Click(object sender, EventArgs e)
        {
            buttonRunTimes.Enabled = false;

            output("开始持续运行");

            ClearReport();

            List<string> scripts = new List<string>();
            List<string> param = new List<string>();
            foreach (ListViewItem item in listViewScript.Items)
            {
                int index = listViewScript.Columns["Script"].Index;
                string script = item.SubItems[index].Text;
                index = listViewScript.Columns["Param"].Index;
                string para = item.SubItems[index].Text;

                scripts.Add(script);
                param.Add(para);
            }

            threads.Clear();
            foreach (ScriptConfig.Pipeline pipeline in config.Pipelines)
            {
                Context[] ctxs = new Context[pipeline.ctxs.Count];
                for (int i = 0; i < pipeline.ctxs.Count; i++)
                {
                    ctxs[i] = pipeline.ctxs[i];
                }
                threads.Add(new Thread(new ThreadStart(() => driver.RunScript(true, scripts.ToArray(), ctxs, param.ToArray()))));
            }

            foreach (Thread thread in threads)
            {
                thread.Start();

                Thread.Sleep(100);
            }
        }
    }
}
