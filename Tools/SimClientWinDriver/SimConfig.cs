﻿/* ============================================================
* Author:       xieqin
* Time:         2015/10/26 17:45:38
* FileName:     SimConfig
* Purpose:      Config
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SimClientWinDriver
{
    [XmlRoot("SimClientConfig")]
    public class SimConfig
    {
        [XmlElementAttribute("entry", IsNullable = false)]
        public List<SimEntry> Entrys = new List<SimEntry>();
    }

    public class SimEntry
    {
        [XmlAttribute("ip")]
        public string Ip
        {
            set;
            get;
        }

        [XmlAttribute("port")]
        public int Port
        {
            set;
            get;
        }

        [XmlElementAttribute("account", IsNullable = false)]
        public List<SimAccount> Accounts = new List<SimAccount>();

        [XmlElementAttribute("script", IsNullable = false)]
        public List<SimScript> Scripts = new List<SimScript>();
    }

    public class SimScript
    {
        [XmlAttribute("param")]
        public string Param
        {
            set;
            get;
        }

        [XmlText]
        public string script;
    }

    public class SimAccount
    {
        [XmlAttribute("account")]
        public string Account
        {
            set;
            get;
        }

        [XmlAttribute("password")]
        public string Password
        {
            set;
            get;
        }

        [XmlAttribute("role")]
        public string Role
        {
            set;
            get;
        }
    }
}
