﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigGen
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Argument error!");
            }
            else
            {
                string fileName = args[0];
                string outFileName = fileName.Replace(".xml", ".cs");
                ConvertMetaData(fileName, outFileName);
            }

            //Console.ReadLine();
        }

        static void ConvertMetaData(string metaFile, string csFile)
        {
            Console.WriteLine("Start convert...");

            MetaConverter.Convert(metaFile, csFile);

            Console.WriteLine("Convert complete!");
        }
    }
}
