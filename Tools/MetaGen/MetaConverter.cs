﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace ConfigGen
{
    //元数据转换
    public class MetaConverter
    {
        //转换
        public static void Convert(string metaFilePath, string resultFilePath)
        {
            //判断是否需要转换
            if (!File.Exists(metaFilePath))
            {
                Console.WriteLine("......No meta file!");
                return;
            }

            if (File.Exists(resultFilePath))
            {
                FileInfo metaInfo = new FileInfo(metaFilePath);
                FileInfo csInfo = new FileInfo(resultFilePath);
                if (csInfo.LastWriteTime > metaInfo.LastWriteTime)
                {
                    Console.WriteLine("......No need convert!");
                    return;
                }
            }

            FileStream fileStream = new FileStream(resultFilePath, FileMode.Create);
            StreamWriter streamWrite = new StreamWriter(fileStream);

            try
            {
                streamWrite.WriteLine("using System;");
                streamWrite.WriteLine("using System.Collections.Generic;");
                streamWrite.WriteLine("using System.Linq;");
                streamWrite.WriteLine("using System.Text;");
                streamWrite.WriteLine("using System.Threading.Tasks;");
                streamWrite.WriteLine("using System.Xml;");
                streamWrite.WriteLine("using System.IO;");
                streamWrite.WriteLine("\r\n");

                XmlDocument doc = new XmlDocument();
                doc.Load(metaFilePath);
                XmlElement rootElem = doc.DocumentElement;

                streamWrite.WriteLine("namespace Config");
                streamWrite.WriteLine("{");

                XmlNodeList classNodes = rootElem.GetElementsByTagName("class");
                foreach (XmlNode classNode in classNodes)
                {
                    ConvertClass(streamWrite, classNode);
                }

                streamWrite.WriteLine("}");
                streamWrite.WriteLine("\r\n");

                Console.WriteLine("......Generate {0}", resultFilePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred: {0}", ex.Message);
            }
            finally
            {
                streamWrite.Flush();
                streamWrite.Close();
                fileStream.Close();
            }
        }

        private static bool IsPrimitiveType(string type)
        {
            if (type == "sbyte" || type == "short" || type == "int" || type == "long"
                || type == "byte" || type == "ushort" || type == "uint" || type == "ulong"
                || type == "float" || type == "double" || type == "decimal" || type == "bool"
                || type == "char" || type == "string")
            {
                return true;
            }

            return false;
        }

        private static void ConvertClass(StreamWriter streamWrite, XmlNode classNode)
        {
            bool hasBase = false;
            string className = ((XmlElement)classNode).GetAttribute("name");
            string baseName = "IConfig";
            if (((XmlElement)classNode).HasAttribute("base"))
            {
                baseName = ((XmlElement)classNode).GetAttribute("base");
                hasBase = true;
            }
            streamWrite.WriteLine("    public class {0} : {1}", className, baseName);
            streamWrite.WriteLine("    {");

            //常量
            XmlNodeList constNodes = ((XmlElement)classNode).GetElementsByTagName("const");
            foreach (XmlNode constNode in constNodes)
            {
                string constName = ((XmlElement)constNode).GetAttribute("name");
                string type = ((XmlElement)constNode).GetAttribute("type");
                string value = ((XmlElement)constNode).GetAttribute("value");
                if (type == "string")
                {
                    value = "\"" + value + "\"";
                }
                if (((XmlElement)constNode).HasAttribute("desc"))
                {
                    string desc = ((XmlElement)constNode).GetAttribute("desc");
                    streamWrite.WriteLine("        const {0} {1} = {2}; //{3}", type, constName, value, desc);
                }
                else
                {
                    streamWrite.WriteLine("        const {0} {1} = {2};", type, constName, value);
                }
            }

            if (constNodes.Count > 0)
            {
                streamWrite.WriteLine("\r\n");
            }

            //成员属性
            XmlNodeList entryNodes = ((XmlElement)classNode).GetElementsByTagName("entry");
            foreach (XmlNode entryNode in entryNodes)
            {
                string entryName = ((XmlElement)entryNode).GetAttribute("name");
                string type = ((XmlElement)entryNode).GetAttribute("type");
                if (((XmlElement)entryNode).HasAttribute("ref"))
                {
                    if (((XmlElement)entryNode).HasAttribute("desc"))
                    {
                        string desc = ((XmlElement)entryNode).GetAttribute("desc");
                        streamWrite.WriteLine("        public List<{0}> {1} //{2}", type, entryName, desc);
                    }
                    else
                    {
                        streamWrite.WriteLine("        public List<{0}> {1}", type, entryName);
                    }
                }
                else
                {
                    if (((XmlElement)entryNode).HasAttribute("desc"))
                    {
                        string desc = ((XmlElement)entryNode).GetAttribute("desc");
                        streamWrite.WriteLine("        public {0} {1} //{2}", type, entryName, desc);
                    }
                    else
                    {
                        streamWrite.WriteLine("        public {0} {1}", type, entryName);
                    }
                }

                streamWrite.WriteLine("        {");
                streamWrite.WriteLine("            get;");
                streamWrite.WriteLine("            set;");
                streamWrite.WriteLine("        }\r\n");
            }

            if (constNodes.Count > 0 || entryNodes.Count > 0)
            {
                streamWrite.WriteLine("\r\n");
            }

            //构造函数
            streamWrite.WriteLine("        public {0}()", className);
            streamWrite.WriteLine("        {");
            foreach (XmlNode entryNode in entryNodes)
            {
                string entryName = ((XmlElement)entryNode).GetAttribute("name");
                string type = ((XmlElement)entryNode).GetAttribute("type");

                if (((XmlElement)entryNode).HasAttribute("ref"))
                {
                    streamWrite.WriteLine("            {0} = new List<{1}>();", entryName, type);
                }
                else if (IsPrimitiveType(type))
                {
                    if (((XmlElement)entryNode).HasAttribute("defaultvalue"))
                    {
                        string defaultValue = ((XmlElement)entryNode).GetAttribute("defaultvalue");
                        if (type == "string")
                        {
                            streamWrite.WriteLine("            {0} = \"{1}\";", entryName, defaultValue);
                        }
                        else
                        {
                            streamWrite.WriteLine("            {0} = {1};", entryName, defaultValue);
                        }
                    }
                    else
                    {
                        if (type == "string")
                        {
                            streamWrite.WriteLine("            {0} = \"\";", entryName);
                        }
                    }
                }
                else
                {
                    streamWrite.WriteLine("            {0} = new {1}();", entryName, type);
                }
            }
            streamWrite.WriteLine("        }");
            streamWrite.WriteLine("\r\n");

            //void Load(XmlElement rootElem)
            if (hasBase)
            {
                streamWrite.WriteLine("        public override void Load(XmlElement rootElem)");
            }
            else
            {
                streamWrite.WriteLine("        public virtual void Load(XmlElement rootElem)");
            }
            streamWrite.WriteLine("        {");
            if (hasBase)
            {
                streamWrite.WriteLine("            base.Load(rootElem);");

                if (entryNodes.Count > 0)
                {
                    streamWrite.WriteLine("\r\n");
                }
            }
            if (entryNodes.Count > 0)
            {
                streamWrite.WriteLine("            XmlNodeList elementNodes = rootElem.ChildNodes;");
                streamWrite.WriteLine("            foreach (XmlNode elementNode in elementNodes)");
                streamWrite.WriteLine("            {");
                foreach (XmlNode entryNode in entryNodes)
                {
                    string entryName = ((XmlElement)entryNode).GetAttribute("name");
                    string type = ((XmlElement)entryNode).GetAttribute("type");

                    if (((XmlElement)entryNode).HasAttribute("ref"))
                    {
                        streamWrite.WriteLine("                if (elementNode.Name == \"{0}s\")", entryName);
                        streamWrite.WriteLine("                {");
                    }
                    else
                    {
                        streamWrite.WriteLine("                if (elementNode.Name == \"{0}\")", entryName);
                        streamWrite.WriteLine("                {");
                    }
                    
                    if (((XmlElement)entryNode).HasAttribute("ref"))
                    {
                        streamWrite.WriteLine("                    XmlNodeList childNodes = elementNode.ChildNodes;");
                        streamWrite.WriteLine("                    foreach (XmlNode childNode in childNodes)");
                        streamWrite.WriteLine("                    {");

                        if (type == "string")
                        {
                            streamWrite.WriteLine("                        {0}.Add(childNode.InnerText);", entryName);
                        }
                        else if (IsPrimitiveType(type))
                        {
                            streamWrite.WriteLine("                        {0}.Add({1}.Parse(childNode.InnerText));", entryName, type);
                        }
                        else
                        {
                            streamWrite.WriteLine("                        {0} item = new {0}();", type);
                            streamWrite.WriteLine("                        item.Load((XmlElement)childNode);");
                            streamWrite.WriteLine("                        {0}.Add(item);", entryName);
                        }
                        streamWrite.WriteLine("                    }");
                    }
                    else if (type == "string")
                    {
                        streamWrite.WriteLine("                    {0} = elementNode.InnerText;", entryName);
                    }
                    else if (IsPrimitiveType(type))
                    {
                        streamWrite.WriteLine("                    {0} = {1}.Parse(elementNode.InnerText);", entryName, type);
                    }
                    else
                    {
                        streamWrite.WriteLine("                    {0}.Load((XmlElement)elementNode);", entryName);
                    }
                    streamWrite.WriteLine("                }");
                }
                streamWrite.WriteLine("            }");
            }
            streamWrite.WriteLine("        }");

            //void Load(string file)
            if (hasBase)
            {
                streamWrite.WriteLine("        public override void Load(string file)");
            }
            else
            {
                streamWrite.WriteLine("        public virtual void Load(string file)");
            }
            streamWrite.WriteLine("        {");
            streamWrite.WriteLine("            XmlDocument Doc = new XmlDocument();");
            streamWrite.WriteLine("            Doc.Load(file);");
            streamWrite.WriteLine("            XmlElement rootElem = Doc.DocumentElement;");
            streamWrite.WriteLine("            Load(rootElem);");
            streamWrite.WriteLine("        }");

            streamWrite.WriteLine("    }");
            streamWrite.WriteLine("\r\n");
        }
    }
}
