﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Config;
using StaticData;
using Utility.Common;
using System.Diagnostics;
using System.Reflection;

namespace StaticDataTool
{
    public class StaticDataToolImpl
    {
        private Dictionary<string, List<StaticDataTableInfo>> m_targetPath2TableInfo =
            new Dictionary<string, List<StaticDataTableInfo>>();

        /// <summary>
        /// 不同文件后缀对应不同的存储器
        /// </summary>
        private Dictionary<string, StaticDataSaver> m_suffix2Savers =
            new Dictionary<string, StaticDataSaver>();

        private StaticDataToolConfig m_config;

        public void Init(StaticDataToolConfig config)
        {
            m_config = config;
            StaticDataVisitor.Instance.Init(Console.WriteLine);
            StaticDataVisitor.Instance.AddLoader("xlsx", new ExcelDataLoader(Console.WriteLine));
            StaticDataVisitor.Instance.AddLoader("xls", new ExcelDataLoader(Console.WriteLine));
            StaticDataVisitor.Instance.AddLoader("csv", new CSVDataLoader(Console.WriteLine));
            m_suffix2Savers.Add("bytes", new ProtoDataSaver(Console.WriteLine));
        }

        /// <summary>
        /// 添加存储器
        /// </summary>
        /// <param name="suffix">文件后缀名字</param>
        /// <param name="saver">存储器</param>
        public void AddSaver(string suffix, StaticDataSaver saver)
        {
            if (!m_suffix2Savers.ContainsKey(suffix))
            {
                m_suffix2Savers.Add(suffix, saver);
            }
        }

        public void Load()
        {
            foreach (StaticDataTableConfig cfg in m_config.StaticDataCfg)
            {
                StaticDataTableInfo info = Convert(cfg);
                info.path = m_config.ConfigPath + "\\" + m_config.SourcePath;
                if (StaticDataVisitor.Instance.Load(info))
                {
                    string[] slices = info.tableName.Split(new char[] { '.' });
                    string tableNameNoSuffix = slices[0];
                    List<StaticDataTableInfo> infoList = null;
                    if (!m_targetPath2TableInfo.TryGetValue(tableNameNoSuffix, out infoList))
                    {
                        infoList = new List<StaticDataTableInfo>();
                        m_targetPath2TableInfo.Add(tableNameNoSuffix, infoList);
                    }
                    infoList.Add(info);
                }
            }
        }


        /// <summary>
        /// 用存储器保存多个表格，多种静态数据可能保存在同一文件
        /// </summary>
        /// <param name="path">目标路径，不含文件名</param>
        /// <param name="dictTableName2InfoList">目标文件名=>表格信息列表</param>
        /// <returns>结果</returns>
        public bool Save(string path, Dictionary<string, List<StaticDataTableInfo>> dictTableName2InfoList)
        {
            bool res = true;
            foreach (KeyValuePair<string, StaticDataSaver> saver in m_suffix2Savers)
            {
                foreach (KeyValuePair<string, List<StaticDataTableInfo>> pair in dictTableName2InfoList)
                {
                    Dictionary<Type, Dictionary<int, object>> datas =
                        new Dictionary<Type, Dictionary<int, object>>();
                    foreach (StaticDataTableInfo info in pair.Value)
                    {
                        StaticDataStorage storage = StaticDataVisitor.Instance.GetDatas(info.keyType);
                        if (storage != null && storage.GetCount() > 0)
                        {
                            Dictionary<int, object> dict = null;
                            if (!datas.TryGetValue(info.keyType, out dict))
                            {
                                dict = new Dictionary<int, object>();
                                datas.Add(info.keyType, dict);
                            }
                            foreach (var row in storage.GetPairEnumerator())
                            {
                                if (!dict.ContainsKey(row.Key))
                                {
                                    dict.Add(row.Key, row.Value);
                                }
                            }
                        }
                    }
                    string fullPath = path + "\\" + pair.Key + "." + saver.Key;
                    res = res && saver.Value.Save(datas, fullPath);
                }
            }
            return res;
        }

        public void Save()
        {
            string TargetPath = m_config.ConfigPath + "\\" + m_config.TargetPath;
            Save(TargetPath, m_targetPath2TableInfo);
        }

        private Type GetTypeByClassName(string name)
        {
            string[] libs = new string[] { "LogicLib", "Protocols" };
            foreach(string libName in libs)
            {
                Assembly assem = Assembly.Load(libName);
                foreach (Type tp in assem.GetTypes())
                {
                    if (tp.Name == name)
                    {
                        return tp;
                    }
                }
            }
            return AssemblyParser.GetTypeByClassName(name);
        }

        private StaticDataTableInfo Convert(StaticDataTableConfig config)
        {
            StaticDataTableInfo info = new StaticDataTableInfo();
            info.keyType = GetTypeByClassName(config.Type);
            info.createType = info.keyType;
            info.tableName = config.TableName;
            info.sheetName = config.SheetName;
            info.titleRow = config.TitleRow;
            info.dataBeginRow = config.DataBeginRow;
            Debug.Assert(info.keyType != null, string.Format("[StaticDataToolImpl.Convert] Type {0} is invalid", config.Type));
            return info;
        }
    }
}
