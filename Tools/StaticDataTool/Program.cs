﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Module;
using StaticData;
using Log;
using Config;
using System.IO;
using System.Diagnostics;

namespace StaticDataTool
{
    class Program
    {
        static void Main(string[] args)
        {
            StaticDataToolConfig config = new StaticDataToolConfig();
            config.Load(args[0]);
            config.ConfigPath = Path.GetDirectoryName(args[0]);

            StaticDataToolImpl impl = new StaticDataToolImpl();
            impl.Init(config);
            impl.Load();
            impl.Save();

            Console.WriteLine();
            Console.Write("press Enter to close......");
            Console.Read();
        }
    }
}
