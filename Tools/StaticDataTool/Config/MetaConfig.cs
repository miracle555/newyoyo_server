using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;


namespace Config
{
    public class StaticDataTableConfig : IConfig
    {
        public string Type
        {
            get;
            set;
        }

        public string TableName
        {
            get;
            set;
        }

        public string SheetName
        {
            get;
            set;
        }

        public int TitleRow
        {
            get;
            set;
        }

        public int DataBeginRow
        {
            get;
            set;
        }



        public StaticDataTableConfig()
        {
            Type = "";
            TableName = "";
            SheetName = "";
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "Type")
                {
                    Type = elementNode.InnerText;
                }
                if (elementNode.Name == "TableName")
                {
                    TableName = elementNode.InnerText;
                }
                if (elementNode.Name == "SheetName")
                {
                    SheetName = elementNode.InnerText;
                }
                if (elementNode.Name == "TitleRow")
                {
                    TitleRow = int.Parse(elementNode.InnerText);
                }
                if (elementNode.Name == "DataBeginRow")
                {
                    DataBeginRow = int.Parse(elementNode.InnerText);
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


    public class StaticDataToolConfig : IConfig
    {
        public string ConfigPath
        {
            get;
            set;
        }

        public string SourcePath
        {
            get;
            set;
        }

        public string TargetPath
        {
            get;
            set;
        }

        public List<StaticDataTableConfig> StaticDataCfg
        {
            get;
            set;
        }



        public StaticDataToolConfig()
        {
            ConfigPath = "";
            SourcePath = "";
            TargetPath = "";
            StaticDataCfg = new List<StaticDataTableConfig>();
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "ConfigPath")
                {
                    ConfigPath = elementNode.InnerText;
                }
                if (elementNode.Name == "SourcePath")
                {
                    SourcePath = elementNode.InnerText;
                }
                if (elementNode.Name == "TargetPath")
                {
                    TargetPath = elementNode.InnerText;
                }
                if (elementNode.Name == "StaticDataCfgs")
                {
                    XmlNodeList childNodes = elementNode.ChildNodes;
                    foreach (XmlNode childNode in childNodes)
                    {
                        StaticDataTableConfig item = new StaticDataTableConfig();
                        item.Load((XmlElement)childNode);
                        StaticDataCfg.Add(item);
                    }
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


}


