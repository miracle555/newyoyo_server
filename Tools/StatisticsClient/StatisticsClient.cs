﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protocols;
using System.Net;
using System.Runtime.InteropServices;
using System.IO;
using Protocols.Error;
using System.Diagnostics;

namespace StatisticsClient
{
    public partial class StatisticsClient : Form
    {
        //////////////////////////////////////////////////////////////////////////
        Timer timer = new Timer();
        Session session = new Session();
        Dictionary<StatisticsType, string> StatisticsKeys = new Dictionary<StatisticsType, string>();
        Dictionary<int, string> StatisticsItemKeys = new Dictionary<int, string>();

        //////////////////////////////////////////////////////////////////////////
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string section, string key, string defVal, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        //////////////////////////////////////////////////////////////////////////
        public StatisticsClient()
        {
            InitializeComponent();

            InitData();
            InitForm();
        }

        //////////////////////////////////////////////////////////////////////////
        void LoadData()
        {
            if (!File.Exists(".\\StatisticsClient.ini"))
            {
                var file = File.Create(".\\StatisticsClient.ini");
                file.Close();
            }
            else
            {
                StringBuilder sb = new StringBuilder(255);
                GetPrivateProfileString("Login", "ip", "", sb, 255, ".\\StatisticsClient.ini");
                TBServerIp.Text = sb.ToString();
                GetPrivateProfileString("Login", "account", "", sb, 255, ".\\StatisticsClient.ini");
                TBAccount.Text = sb.ToString();
                GetPrivateProfileString("Login", "password", "", sb, 255, ".\\StatisticsClient.ini");
                TBPassword.Text = sb.ToString();
            }
        }

        void SaveData()
        {
            if (!File.Exists(".\\StatisticsClient.ini"))
            {
                var file = File.Create(".\\StatisticsClient.ini");
                file.Close();
            }

            WritePrivateProfileString("Login", "ip", TBServerIp.Text, ".\\StatisticsClient.ini");
            WritePrivateProfileString("Login", "account", TBAccount.Text, ".\\StatisticsClient.ini");
            WritePrivateProfileString("Login", "password", TBPassword.Text, ".\\StatisticsClient.ini");
        }

        void InitData()
        {
            LoadData();

            timer.Interval = 200;
            timer.Tick += Tick;
            timer.Start();

            session.Init();
        }

        void InitForm()
        {
            BtnLogout.Enabled = false;

            DateTime now = DateTime.Now;
        }

        void Tick(object sender, EventArgs e)
        {
            Protocol proto = session.GetMessage();
            while (proto != null)
            {
                if (proto.GetID() == ProtoIDReserve.CMD_SERVER_CONNECTED)
                {
                    ServerConnected body = (ServerConnected)proto.GetBody();
                    HandleConnect(body.success);
                }
                else if (proto.GetID() == ProtoIDReserve.CMD_SERVER_DISCONNECTED)
                {
                    HandleDisconnect();
                }
                else
                {
                    HandleMessage(proto);
                }

                proto = session.GetMessage();
            }
        }

        void HandleConnect(bool success)
        {
            Output("Server <-----> Client");

            UserLoginReq req = new UserLoginReq();
            req.platformType = 0;
            req.account = TBAccount.Text;
            req.password = TBPassword.Text;
            SendMessage(req);
        }

        void HandleDisconnect()
        {
            Output("Server <--x--> Client");

            BtnLogin.Enabled = true;
            BtnLogout.Enabled = false;
        }

        void HandleMessage(Protocol proto)
        {
            Output(string.Format("Server ------> {0}", proto.GetID().ToString()));

            if (proto.GetID() == ProtoID.CMD_LOGIN_USERLOGIN_RSP)
            {
                UserLoginRsp rsp = (UserLoginRsp)proto.GetBody();
                if (rsp.errorCode != ErrorID.Success)
                {
                    Output(string.Format("[UserLoginRsp]error: {0}", rsp.errorCode.ToString()));

                    BtnLogin.Enabled = true;
                }
                else
                {
                    BtnLogout.Enabled = true;
                }
            }
            else if (proto.GetID() == ProtoID.CMD_STATISTICS_ITEM_RSP)
            {
                StatisticsItemDataRsp rsp = (StatisticsItemDataRsp)proto.GetBody();
                DataTable dt = new DataTable();
                DataColumn col = new DataColumn("Date", typeof(string));
                dt.Columns.Add(col);

                //加入所有列
                if (ModuleName.PKMall == rsp.req.moduleName || ModuleName.MallItemBuyUserCount == rsp.req.moduleName ||
                    ModuleName.MallItemIncome == rsp.req.moduleName || ModuleName.MallItemSaleCount == rsp.req.moduleName ||
                    ModuleName.DressItemUseCount == rsp.req.moduleName || ModuleName.ManufactureProductItem == rsp.req.moduleName
                    || ModuleName.NoviceGuide == rsp.req.moduleName)
                {
                    FillItemColumn(rsp);
                }

                foreach (var condition in rsp.req.conditions)
                {
                    col = new DataColumn(StatisticsItemKeys[condition], typeof(string));
                    dt.Columns.Add(col);
                }

                foreach (var result in rsp.results)
                {
                    if (result.Value != null)
                    {
                        DataRow row = dt.NewRow();

                        if (result.Key != DateTime.MaxValue)
                            row["Date"] = string.Format("{0}-{1}-{2}", result.Key.Year, result.Key.Month, result.Key.Day);
                        else
                            row["Date"] = "summary";
                        //一行一行的填充表
                        foreach (var condition in rsp.req.conditions)
                        {
                            if (result.Value.ContainsKey(condition))
                            {
                                row[StatisticsItemKeys[condition]] = result.Value[condition];
                            }
                            else
                            {
                                row[StatisticsItemKeys[condition]] = "0";
                            }
                        }

                        dt.Rows.Add(row);
                    }
                }

                DGVReport.DataSource = dt;
            }
            else if (proto.GetID() == ProtoID.CMD_STATISTICS_RSP)
            {
                StatisticsDataRsp rsp = (StatisticsDataRsp)proto.GetBody();
                DataTable dt = new DataTable();
                DataColumn col = new DataColumn("Date", typeof(string));
                dt.Columns.Add(col);

                foreach (var condition in rsp.req.conditions)
                {
                    col = new DataColumn(StatisticsKeys[condition], typeof(string));
                    dt.Columns.Add(col);
                }

                foreach (var result in rsp.results)
                {
                    if (result.Value != null)
                    {
                        DataRow row = dt.NewRow();

                        if (result.Key != DateTime.MaxValue)
                            row["Date"] = string.Format("{0}-{1}-{2}", result.Key.Year, result.Key.Month, result.Key.Day);
                        else
                            row["Date"] = "summary";
                        //一行一行的填充表
                        foreach (var condition in rsp.req.conditions)
                        {
                            if (result.Value.ContainsKey(condition))
                            {
                                row[StatisticsKeys[condition]] = result.Value[condition];
                            }
                            else
                            {
                                row[StatisticsKeys[condition]] = "0";
                            }
                        }

                        dt.Rows.Add(row);
                    }
                }

                DGVReport.DataSource = dt;
            }
            else if (proto.GetID() == ProtoID.CMD_STATISTICS_NOTDAILY_RSP)
            {
                StatisticsNotDailyDataRsp rsp = (StatisticsNotDailyDataRsp)proto.GetBody();

                DataTable dt = new DataTable();
                DataColumn col = new DataColumn("ID", typeof(string));
                dt.Columns.Add(col);
                //加入所有列
                foreach (var condition in rsp.req.conditions)
                {
                    col = new DataColumn(StatisticsKeys[condition], typeof(string));
                    dt.Columns.Add(col);
                }

                foreach (var result in rsp.results)
                {
                    DataRow row = dt.NewRow();

                    if (result.Key != Int32.MaxValue)
                        row["ID"] = string.Format("{0}", result.Key);
                    else
                        row["ID"] = "summary";
                    //一行一行的填充表
                    foreach (var condition in rsp.req.conditions)
                    {
                        if (result.Value.ContainsKey(condition))
                        {
                            row[StatisticsKeys[condition]] = result.Value[condition];
                        }
                        else
                        {
                            row[StatisticsKeys[condition]] = "0";
                        }
                    }
                    dt.Rows.Add(row);
                }
                DGVReport.DataSource = dt;
            }
        }


        /// <summary>
        /// 填充列
        /// </summary>
        /// <param name="rsp"></param>
        private void FillItemColumn(StatisticsItemDataRsp rsp)
        {
            Dictionary<int, string> itemDatas = GetItemData();

            if (ModuleName.DressItemUseCount == rsp.req.moduleName)
            {
                foreach (var item in rsp.results)
                {
                    if (DateTime.MaxValue == item.Key)
                    {
                        return;
                    }

                    foreach (var pair in item.Value)
                    {
                        if (!rsp.req.conditions.Contains(pair.Key))
                        {
                            rsp.req.conditions.Add(pair.Key);
                            StatisticsItemKeys[pair.Key] = itemDatas[pair.Key];
                        }
                    }
                }
                return;
            }

            if (ModuleName.ManufactureProductItem == rsp.req.moduleName)
            {
                FillManufactreItemColumn(itemDatas, rsp);
                return;
            }

            if (ModuleName.NoviceGuide == rsp.req.moduleName)
            {
                FillNoviceGuideColumn(rsp);
                return;
            }

            FillItemColumnStep(itemDatas, rsp, rsp.req.moduleName);
        }

        /// <summary>
        /// 新手引导
        /// </summary>
        /// <param name="rsp"></param>
        private void FillNoviceGuideColumn(StatisticsItemDataRsp rsp)
        {
            foreach (var item in rsp.results)
            {
                foreach (var pair in item.Value)
                {
                    if (!rsp.req.conditions.Contains(pair.Key))
                    {
                        rsp.req.conditions.Add(pair.Key);
                        StatisticsItemKeys[pair.Key] = string.Format("新手点击{0}完成人数", pair.Key);
                    }
                }
            }
        }


        /// <summary>
        /// 制作
        /// </summary>
        /// <param name="itemDatas"></param>
        /// <param name="rsp"></param>
        private void FillManufactreItemColumn(Dictionary<int, string> itemDatas, StatisticsItemDataRsp rsp)
        {
            string dataName = "ManufactureMakeData";
            List<int> makeIds = GetStatisticsData(dataName);
            string nameEx = " 制作产出";
            FillManufactreItemColumnStep(itemDatas, rsp, makeIds, nameEx);

            nameEx = " 染色产出";
            List<int> dyeIds = GetManufactureDyeData();
            FillManufactreItemColumnStep(itemDatas, rsp, dyeIds, nameEx);

            nameEx = " 进化产出";
            dataName = "ManufactureEvolveData";
            List<int> evolveIds = GetStatisticsData(dataName);
            FillManufactreItemColumnStep(itemDatas, rsp, evolveIds, nameEx);
        }

        /// <summary>
        /// 制作step
        /// </summary>
        /// <param name="itemDatas"></param>
        /// <param name="rsp"></param>
        /// <param name="ids"></param>
        /// <param name="nameEx"></param>
        private void FillManufactreItemColumnStep(Dictionary<int, string> itemDatas, StatisticsItemDataRsp rsp, List<int> ids, string nameEx)
        {
            for (int i = 0; i < ids.Count; i++)
            {
                int id = ids[i];
                if (!rsp.req.conditions.Contains(ids[i]))
                {
                    string name = GetItemName(itemDatas, id);
                    name += nameEx;

                    rsp.req.conditions.Add(id);
                    StatisticsItemKeys[id] = name;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemDatas"></param>
        /// <param name="rsp"></param>
        /// <param name="ids"></param>
        /// <param name="nameEx"></param>
        private void FillItemColumnStep(Dictionary<int, string> itemDatas, StatisticsItemDataRsp rsp,  ModuleName moduleName)
        {
            string dataName = string.Empty;

            if (ModuleName.PKMall == rsp.req.moduleName)
            {
                dataName = "PKMall";
            }
            else
            {
                dataName = "ItemMall";
            }

            List<int> ids = GetStatisticsData(dataName);

            string nameEx = string.Empty;

            if (ModuleName.PKMall == rsp.req.moduleName)
            {
                nameEx = " 售卖数量|购买人数|消耗兑换币数量";
            }

            for (int i = 0; i < ids.Count; i++)
            {
                int id = ids[i];
                if (!rsp.req.conditions.Contains(ids[i]))
                {
                    string name = GetItemName(itemDatas, id);
                    name += nameEx;

                    rsp.req.conditions.Add(id);
                    StatisticsItemKeys[id] = name;
                }
            }

            if (ModuleName.PKMall == moduleName)
            {
                int key = (int)StatisticsType.PKTokenDaysTotalIncome;
                if (!rsp.req.conditions.Contains(key))
                {
                    rsp.req.conditions.Add(key);
                    StatisticsItemKeys[key] = "每日PK代币回收总量";
                }
            }

            if (ModuleName.MallItemSaleCount == rsp.req.moduleName)
            {
                int key = (int)StatisticsType.MallInstantCount;
                if (!rsp.req.conditions.Contains(key))
                {
                    rsp.req.conditions.Add(key);
                    StatisticsItemKeys[key] = "使用立即购买的人数";
                }

                key = (int)StatisticsType.MallInstantTimes;
                if (!rsp.req.conditions.Contains(key))
                {
                    rsp.req.conditions.Add(key);
                    StatisticsItemKeys[key] = "使用立即购买的次数";
                }

                key = (int)StatisticsType.MallShoppingCartCount;
                if (!rsp.req.conditions.Contains(key))
                {
                    rsp.req.conditions.Add(key);
                    StatisticsItemKeys[key] = "使用购物车的的人数";
                }

                key = (int)StatisticsType.MallShoppingCartTimes;
                if (!rsp.req.conditions.Contains(key))
                {
                    rsp.req.conditions.Add(key);
                    StatisticsItemKeys[key] = "使用购物车的的次数";
                }
            }
        }

        void SendMessage(ProtoBody body)
        {
            Protocol proto = new Protocol(body);
            Output(string.Format("Server <------ {0}", proto.GetID().ToString()));
            session.SendMessage(proto);
        }

        void SendMessage(Protocol proto)
        {
            Output(string.Format("Server <------ {0}", proto.GetID().ToString()));
            session.SendMessage(proto);
        }

        void Output(string line)
        {
            line += "\n";
            TBOutput.AppendText(line);
        }

        //////////////////////////////////////////////////////////////////////////
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            BtnLogin.Enabled = false;

            string[] ips = TBServerIp.Text.Split(':');
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(ips[0]), int.Parse(ips[1]));
            session.Connect(endpoint);

            SaveData();
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            session.Disconnect();
        }

        private void GmClient_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 登录相关
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnStatistics_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Login;

            req.conditions.Add(StatisticsType.TotalAddCount);
            StatisticsKeys[StatisticsType.TotalAddCount] = "总注册人数";

            req.conditions.Add(StatisticsType.AddUserCount);
            StatisticsKeys[StatisticsType.AddUserCount] = "新增用户人数";

            req.conditions.Add(StatisticsType.LoginCount);
            StatisticsKeys[StatisticsType.LoginCount] = "日活跃人数";

            req.conditions.Add(StatisticsType.LoginCountPerDay);
            StatisticsKeys[StatisticsType.LoginCountPerDay] = "日均登录人数";

            req.conditions.Add(StatisticsType.MaxOnlineCount);
            StatisticsKeys[StatisticsType.MaxOnlineCount] = "日最大同时在线人数";

            req.conditions.Add(StatisticsType.OnlineTimePerDay);
            StatisticsKeys[StatisticsType.OnlineTimePerDay] = "日均在线时长";

            SendMessage(req);
        }

        /// <summary>
        /// 活跃用户等级分布
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLevelDistribution_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ActiveUserGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }

        /// <summary>
        /// 留存相关
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRemain_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Remain;

            req.conditions.Add(StatisticsType.LoginOldUser);
            StatisticsKeys[StatisticsType.LoginOldUser] = "当日登陆老用户数";

            req.conditions.Add(StatisticsType.NextDayRemainCount);
            StatisticsKeys[StatisticsType.NextDayRemainCount] = "次日留存数";

            req.conditions.Add(StatisticsType.NextDayRemainRate);
            StatisticsKeys[StatisticsType.NextDayRemainRate] = "次日留存率";

            req.conditions.Add(StatisticsType.ThreeDayRemainCount);
            StatisticsKeys[StatisticsType.ThreeDayRemainCount] = "三日留存数";

            req.conditions.Add(StatisticsType.ThreeDayRemainRate);
            StatisticsKeys[StatisticsType.ThreeDayRemainRate] = "三日留存率";

            req.conditions.Add(StatisticsType.SevenDayRemainCount);
            StatisticsKeys[StatisticsType.SevenDayRemainCount] = "七日留存数";

            req.conditions.Add(StatisticsType.SevenDayRemainRate);
            StatisticsKeys[StatisticsType.SevenDayRemainRate] = "七日留存率";

            req.conditions.Add(StatisticsType.FifteenDayRemainCount);
            StatisticsKeys[StatisticsType.FifteenDayRemainCount] = "十五日留存数";

            req.conditions.Add(StatisticsType.FifteenDayRemainRate);
            StatisticsKeys[StatisticsType.FifteenDayRemainRate] = "十五日留存率";

            req.conditions.Add(StatisticsType.ThirtyDayRemainCount);
            StatisticsKeys[StatisticsType.ThirtyDayRemainCount] = "三十日留存数";

            req.conditions.Add(StatisticsType.ThirtyDayRemainRate);
            StatisticsKeys[StatisticsType.ThirtyDayRemainRate] = "三十日留存率";

            SendMessage(req);
        }

        /// <summary>
        /// 自由换装系统
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDress_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Dress;

            req.conditions.Add(StatisticsType.DressCount);
            StatisticsKeys[StatisticsType.DressCount] = "每日参与换装人数";

            req.conditions.Add(StatisticsType.DressTimes);
            StatisticsKeys[StatisticsType.DressTimes] = "每日参与换装次数";

            req.conditions.Add(StatisticsType.DressRate);
            StatisticsKeys[StatisticsType.DressRate] = "换装人数与活跃人数比率";

            req.conditions.Add(StatisticsType.DressSaveSuitCount);
            StatisticsKeys[StatisticsType.DressSaveSuitCount] = "每日点击保存套装人数";

            req.conditions.Add(StatisticsType.DressSaveSuitTimes);
            StatisticsKeys[StatisticsType.DressSaveSuitTimes] = "每日点击保存套装次数";

            req.conditions.Add(StatisticsType.DressClickTileCount);
            StatisticsKeys[StatisticsType.DressClickTileCount] = "每日点击平铺人数";

            req.conditions.Add(StatisticsType.DressClickTileTimes);
            StatisticsKeys[StatisticsType.DressClickTileTimes] = "每日点击平铺次数";

            SendMessage(req);
        }

        /// <summary>
        /// 参与自由换装系统用户等级分布
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDressUserGrade_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.DressUserGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }

        /// <summary>
        /// PK系统
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPK_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.PK;

            req.conditions.Add(StatisticsType.PKExperienceCount);
            StatisticsKeys[StatisticsType.PKExperienceCount] = "每日体验PK人数";

            req.conditions.Add(StatisticsType.PKExperienceTimes);
            StatisticsKeys[StatisticsType.PKExperienceTimes] = "每日体验PK次数";

            req.conditions.Add(StatisticsType.PKExperienceRate);
            StatisticsKeys[StatisticsType.PKExperienceRate] = "PK人数与活跃人数比率";

            req.conditions.Add(StatisticsType.PKParticipationCount);
            StatisticsKeys[StatisticsType.PKParticipationCount] = "每日参与PK人数";

            req.conditions.Add(StatisticsType.PKParticipationTimes);
            StatisticsKeys[StatisticsType.PKParticipationTimes] = "每日参与PK次数";

            req.conditions.Add(StatisticsType.PKCompleteRate);
            StatisticsKeys[StatisticsType.PKCompleteRate] = "每日每人参与PK次数";

            req.conditions.Add(StatisticsType.PKTimePerDay);
            StatisticsKeys[StatisticsType.PKTimePerDay] = "每次参与PK的平均时长";

            req.conditions.Add(StatisticsType.PKCompleteCount);
            StatisticsKeys[StatisticsType.PKCompleteCount] = "每日完成PK人数";

            req.conditions.Add(StatisticsType.PKCompleteTimes);
            StatisticsKeys[StatisticsType.PKCompleteTimes] = "每日完成PK次数";

            req.conditions.Add(StatisticsType.BuyPKTimeUserCount);
            StatisticsKeys[StatisticsType.BuyPKTimeUserCount] = "购买PK次数的用户人数";

            req.conditions.Add(StatisticsType.BuyPKTimeUserTimes);
            StatisticsKeys[StatisticsType.BuyPKTimeUserTimes] = "购买PK次数的用户次数";

            req.conditions.Add(StatisticsType.PKSkipCount);
            StatisticsKeys[StatisticsType.PKSkipCount] = "PK中使用跳过的人数";

            req.conditions.Add(StatisticsType.PKClickTileCount);
            StatisticsKeys[StatisticsType.PKClickTileCount] = "点击平铺的用户人数";

            req.conditions.Add(StatisticsType.PKClickTileTimes);
            StatisticsKeys[StatisticsType.PKClickTileTimes] = "点击平铺的用户次数";

            req.conditions.Add(StatisticsType.PKMallInstantCount);
            StatisticsKeys[StatisticsType.PKMallInstantCount] = "PK兑换商城使用立即购买的人数";

            req.conditions.Add(StatisticsType.PKMallInstantTimes);
            StatisticsKeys[StatisticsType.PKMallInstantTimes] = "PK兑换商城使用立即购买的次数";

            req.conditions.Add(StatisticsType.PKMallShoppingCartCount);
            StatisticsKeys[StatisticsType.PKMallShoppingCartCount] = "PK兑换商城使用购物车结算的人数";

            req.conditions.Add(StatisticsType.PKMallShoppingCartTimes);
            StatisticsKeys[StatisticsType.PKMallShoppingCartTimes] = "PK兑换商城使用购物车结算的次数";

            SendMessage(req);
        }

        /// <summary>
        /// 商城物品售卖数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaleCount_Click(object sender, EventArgs e)
        {
            StatisticsItemDataReq req = new StatisticsItemDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.MallItemSaleCount;
            SendMessage(req);
        }

        /// <summary>
        /// 商城物品收入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnItemIncome_Click(object sender, EventArgs e)
        {
            StatisticsItemDataReq req = new StatisticsItemDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.MallItemIncome;
            SendMessage(req);
        }

        /// <summary>
        /// 剧情系统
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnScenario_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Scenario;

            req.conditions.Add(StatisticsType.ScenarioCount);
            StatisticsKeys[StatisticsType.ScenarioCount] = "每日参与剧情人数";

            req.conditions.Add(StatisticsType.ScenarioTimes);
            StatisticsKeys[StatisticsType.ScenarioTimes] = "每日参与剧情次数";

            req.conditions.Add(StatisticsType.ScenarioRate);
            StatisticsKeys[StatisticsType.ScenarioRate] = "参与剧情人数与活跃人数比率";

            req.conditions.Add(StatisticsType.ScenarioAverageTime);
            StatisticsKeys[StatisticsType.ScenarioAverageTime] = "每次参与剧情的平均时长";

            req.conditions.Add(StatisticsType.PrincessLevelCount);
            StatisticsKeys[StatisticsType.PrincessLevelCount] = "每日参与公主级关卡人数";

            req.conditions.Add(StatisticsType.PrincessLevelTimes);
            StatisticsKeys[StatisticsType.PrincessLevelTimes] = "每日参与公主级关卡次数";

            req.conditions.Add(StatisticsType.PrincessLevelRate);
            StatisticsKeys[StatisticsType.PrincessLevelRate] = "参与公主级关卡与活跃人数比率";

            req.conditions.Add(StatisticsType.PrincessLevelAverageTime);
            StatisticsKeys[StatisticsType.PrincessLevelAverageTime] = "每次公主级关卡的平均时长";

            req.conditions.Add(StatisticsType.MaidenLevelCount);
            StatisticsKeys[StatisticsType.MaidenLevelCount] = "每日参与少女级关卡人数";

            req.conditions.Add(StatisticsType.MaidenLevelTimes);
            StatisticsKeys[StatisticsType.MaidenLevelTimes] = "每日参与少女级关卡次数";

            req.conditions.Add(StatisticsType.MaidenLevelRate);
            StatisticsKeys[StatisticsType.MaidenLevelRate] = "参与少女级关卡与活跃人数比率";

            req.conditions.Add(StatisticsType.MaidenLevelAverageTime);
            StatisticsKeys[StatisticsType.MaidenLevelAverageTime] = "每次少女级关卡的平均时长";

            req.conditions.Add(StatisticsType.OpenSubtitleCount);
            StatisticsKeys[StatisticsType.OpenSubtitleCount] = "每日开启弹幕人数";

            req.conditions.Add(StatisticsType.SendSubtitleCount);
            StatisticsKeys[StatisticsType.SendSubtitleCount] = "每日发送弹幕人数";

            req.conditions.Add(StatisticsType.ScenarioClickTilingCount);
            StatisticsKeys[StatisticsType.ScenarioClickTilingCount] = "每日点击平铺人数";

            req.conditions.Add(StatisticsType.ScenarioClickTilingTimes);
            StatisticsKeys[StatisticsType.ScenarioClickTilingTimes] = "每日点击平铺次数";

            SendMessage(req);
        }

        /// <summary>
        /// 参与剧情系统用户等级分布
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSceUserGrade_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ScenarioUserGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }

        /// <summary>
        /// 换装系统-每件衣服的使用次数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnItemUseTimes_Click(object sender, EventArgs e)
        {
            StatisticsItemDataReq req = new StatisticsItemDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.DressItemUseCount;
            SendMessage(req);
        }

        /// <summary>
        /// 商品购买用户数量
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMallBuyCount_Click(object sender, EventArgs e)
        {
            StatisticsItemDataReq req = new StatisticsItemDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.MallItemBuyUserCount;
            SendMessage(req);
        }

        /// <summary>
        /// 代币消耗产出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMoneyConAPro_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.GameCurrency;

            req.conditions.Add(StatisticsType.PassLevelRewardGold);
            StatisticsKeys[StatisticsType.PassLevelRewardGold] = "剧情过关奖励金币数量";

            req.conditions.Add(StatisticsType.PassLevelRewardGoldCount);
            StatisticsKeys[StatisticsType.PassLevelRewardGoldCount] = "剧情过关奖励金币人数";

            req.conditions.Add(StatisticsType.PassLevelRewardGoldTimes);
            StatisticsKeys[StatisticsType.PassLevelRewardGoldTimes] = "剧情过关奖励金币人次";

            req.conditions.Add(StatisticsType.PassPkRewardGold);
            StatisticsKeys[StatisticsType.PassPkRewardGold] = "PK过关奖励金币数量";

            req.conditions.Add(StatisticsType.PassPkRewardGoldCount);
            StatisticsKeys[StatisticsType.PassPkRewardGoldCount] = "PK过关奖励金币人数";

            req.conditions.Add(StatisticsType.PassPkRewardGoldTimes);
            StatisticsKeys[StatisticsType.PassPkRewardGoldTimes] = "PK过关奖励金币人次";

            req.conditions.Add(StatisticsType.PkRankingTitleRewardGold);
            StatisticsKeys[StatisticsType.PkRankingTitleRewardGold] = "PK排名奖励金币数量";

            req.conditions.Add(StatisticsType.PkRankingTitleRewardGoldCount);
            StatisticsKeys[StatisticsType.PkRankingTitleRewardGoldCount] = "PK排名奖励金币人数";

            req.conditions.Add(StatisticsType.PkRankingTitleRewardGoldTimes);
            StatisticsKeys[StatisticsType.PkRankingTitleRewardGoldTimes] = "PK排名奖励金币人次";

            req.conditions.Add(StatisticsType.SignInRewardGold);
            StatisticsKeys[StatisticsType.SignInRewardGold] = "签到奖励金币数量";

            req.conditions.Add(StatisticsType.SignInRewardGoldCount);
            StatisticsKeys[StatisticsType.SignInRewardGoldCount] = "签到奖励金币人数";

            req.conditions.Add(StatisticsType.SignInRewardGoldTimes);
            StatisticsKeys[StatisticsType.SignInRewardGoldTimes] = "签到奖励金币人次";

            req.conditions.Add(StatisticsType.ExchangeFromDiamondGold);
            StatisticsKeys[StatisticsType.ExchangeFromDiamondGold] = "钻石兑换金币数量";

            req.conditions.Add(StatisticsType.ExchangeFromDiamondGoldCount);
            StatisticsKeys[StatisticsType.ExchangeFromDiamondGoldCount] = "钻石兑换金币人数";

            req.conditions.Add(StatisticsType.ExchangeFromDiamondGoldTimes);
            StatisticsKeys[StatisticsType.ExchangeFromDiamondGoldTimes] = "钻石兑换金币人次";

            req.conditions.Add(StatisticsType.GoldRewardTotalCount);
            StatisticsKeys[StatisticsType.GoldRewardTotalCount] = "金币奖励总数";

            req.conditions.Add(StatisticsType.GoldConsumeBuyDress);
            StatisticsKeys[StatisticsType.GoldConsumeBuyDress] = "购买服饰消耗金币数量";

            req.conditions.Add(StatisticsType.GoldConsumeBuyDressCount);
            StatisticsKeys[StatisticsType.GoldConsumeBuyDressCount] = "购买服饰消耗金币人数";

            req.conditions.Add(StatisticsType.GoldConsumeBuyDressTimes);
            StatisticsKeys[StatisticsType.GoldConsumeBuyDressTimes] = "购买服饰消耗金币人次";

            req.conditions.Add(StatisticsType.GoldConsumeBuyItem);
            StatisticsKeys[StatisticsType.GoldConsumeBuyItem] = "购买道具消耗金币数量";

            req.conditions.Add(StatisticsType.GoldConsumeBuyItemCount);
            StatisticsKeys[StatisticsType.GoldConsumeBuyItemCount] = "购买道具消耗金币人数";

            req.conditions.Add(StatisticsType.GoldConsumeBuyItemTimes);
            StatisticsKeys[StatisticsType.GoldConsumeBuyItemTimes] = "购买道具消耗金币人次";

            req.conditions.Add(StatisticsType.GoldConsumeBuyMaterial);
            StatisticsKeys[StatisticsType.GoldConsumeBuyMaterial] = "购买材料消耗金币数量";

            req.conditions.Add(StatisticsType.GoldConsumeBuyMaterialCount);
            StatisticsKeys[StatisticsType.GoldConsumeBuyMaterialCount] = "购买材料消耗金币人数";

            req.conditions.Add(StatisticsType.GoldConsumeBuyMaterialTimes);
            StatisticsKeys[StatisticsType.GoldConsumeBuyMaterialTimes] = "购买材料消耗金币人次";

            req.conditions.Add(StatisticsType.GoldConsumeBuyLevelTips);
            StatisticsKeys[StatisticsType.GoldConsumeBuyLevelTips] = "购买关卡提示消耗金币数量";

            req.conditions.Add(StatisticsType.GoldConsumeBuyLevelTipsCount);
            StatisticsKeys[StatisticsType.GoldConsumeBuyLevelTipsCount] = "购买关卡提示消耗金币人数";

            req.conditions.Add(StatisticsType.GoldConsumeBuyLevelTipsTimes);
            StatisticsKeys[StatisticsType.GoldConsumeBuyLevelTipsTimes] = "购买关卡提示消耗金币人次";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureMake);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureMake] = "服装制作消耗金币数量";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureMakeCount);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureMakeCount] = "服装制作消耗金币人数";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureMakeTimes);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureMakeTimes] = "服装制作消耗金币人次";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureDye);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureDye] = "服装染色消耗金币数量";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureDyeCount);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureDyeCount] = "服装染色消耗金币人数";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureDyeTimes);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureDyeTimes] = "服装染色消耗金币人次";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureEvolve);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureEvolve] = "服装进化消耗金币数量";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureEvolveCount);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureEvolveCount] = "服装进化消耗金币人数";

            req.conditions.Add(StatisticsType.GoldConsumeManufactureEvolveTimes);
            StatisticsKeys[StatisticsType.GoldConsumeManufactureEvolveTimes] = "服装简化消耗金币人次";

            req.conditions.Add(StatisticsType.GoldConsumeTotalCount);
            StatisticsKeys[StatisticsType.GoldConsumeTotalCount] = "金币消耗总数";

            req.conditions.Add(StatisticsType.GoldProductConsumeRate);
            StatisticsKeys[StatisticsType.GoldProductConsumeRate] = "金币产出消耗比";

            req.conditions.Add(StatisticsType.PassPkRewardDiamond);
            StatisticsKeys[StatisticsType.PassPkRewardDiamond] = "PK过关奖励钻石数量";

            req.conditions.Add(StatisticsType.PassPkRewardDiamondCount);
            StatisticsKeys[StatisticsType.PassPkRewardDiamondCount] = "PK过关奖励钻石人数";

            req.conditions.Add(StatisticsType.PassPkRewardDiamondTimes);
            StatisticsKeys[StatisticsType.PassPkRewardDiamondTimes] = "PK过关奖励钻石人次";

            req.conditions.Add(StatisticsType.PkRankingRewardDiamond);
            StatisticsKeys[StatisticsType.PkRankingRewardDiamond] = "PK排名奖励钻石数量";

            req.conditions.Add(StatisticsType.PkRankingRewardDiamondCount);
            StatisticsKeys[StatisticsType.PkRankingRewardDiamondCount] = "PK排名奖励钻石人数";

            req.conditions.Add(StatisticsType.PkRankingRewardDiamondTimes);
            StatisticsKeys[StatisticsType.PkRankingRewardDiamondTimes] = "PK排名奖励钻石人次";

            req.conditions.Add(StatisticsType.PkTitleRewardDiamond);
            StatisticsKeys[StatisticsType.PkTitleRewardDiamond] = "PK称号奖励钻石数量";

            req.conditions.Add(StatisticsType.PkTitleRewardDiamondCount);
            StatisticsKeys[StatisticsType.PkTitleRewardDiamondCount] = "PK称号奖励钻石人数";

            req.conditions.Add(StatisticsType.PkTitleRewardDiamondTimes);
            StatisticsKeys[StatisticsType.PkTitleRewardDiamondTimes] = "PK称号奖励钻石人次";

            req.conditions.Add(StatisticsType.SignInRewardDiamond);
            StatisticsKeys[StatisticsType.SignInRewardDiamond] = "签到奖励钻石数量";

            req.conditions.Add(StatisticsType.SignInRewardDiamondCount);
            StatisticsKeys[StatisticsType.SignInRewardDiamondCount] = "签到奖励钻石人数";

            req.conditions.Add(StatisticsType.SignInRewardDiamondTimes);
            StatisticsKeys[StatisticsType.SignInRewardDiamondTimes] = "签到奖励钻石人次";

            req.conditions.Add(StatisticsType.DaysTaskRewardDiamond);
            StatisticsKeys[StatisticsType.DaysTaskRewardDiamond] = "每日任务奖励钻石数量";

            req.conditions.Add(StatisticsType.DaysTaskRewardDiamondCount);
            StatisticsKeys[StatisticsType.DaysTaskRewardDiamondCount] = "每日任务奖励钻石人数";

            req.conditions.Add(StatisticsType.DaysTaskRewardDiamondTimes);
            StatisticsKeys[StatisticsType.DaysTaskRewardDiamondTimes] = "每日任务奖励钻石人次";

            req.conditions.Add(StatisticsType.DiamondRewardTotalCount);
            StatisticsKeys[StatisticsType.DiamondRewardTotalCount] = "钻石奖励总数";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyDress);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyDress] = "购买服饰消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyDressCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyDressCount] = "购买服饰消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyDressTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyDressTimes] = "购买服饰消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyItem);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyItem] = "购买道具消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyItemCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyItemCount] = "购买道具消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyItemTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyItemTimes] = "购买道具消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyMaterial);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyMaterial] = "购买材料消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyMaterialCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyMaterialCount] = "购买材料消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyMaterialTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyMaterialTimes] = "购买材料消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyPkTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyPkTimes] = "购买PK次数消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyPkTimesCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyPkTimesCount] = "购买PK次数消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyPkTimesTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyPkTimesTimes] = "购买PK次数消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeLevelMiddleTips);
            StatisticsKeys[StatisticsType.DiamondConsumeLevelMiddleTips] = "购买中级换装提示消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeLevelMiddleTipsCount);
            StatisticsKeys[StatisticsType.DiamondConsumeLevelMiddleTipsCount] = "购买中级换装提示消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeLevelMiddleTipsTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeLevelMiddleTipsTimes] = "购买中级换装提示消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeLevelHighTips);
            StatisticsKeys[StatisticsType.DiamondConsumeLevelHighTips] = "购买高级换装提示消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeLevelHighTipsCount);
            StatisticsKeys[StatisticsType.DiamondConsumeLevelHighTipsCount] = "购买高级换装提示消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeLevelHighTipsTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeLevelHighTipsTimes] = "购买高级换装提示消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeMainPrincessLevel);
            StatisticsKeys[StatisticsType.DiamondConsumeMainPrincessLevel] = "购买主线公主级过关次数消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeMainPrincessLevelCount);
            StatisticsKeys[StatisticsType.DiamondConsumeMainPrincessLevelCount] = "购买主线公主级过关次数消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeMainPrincessLevelTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeMainPrincessLevelTimes] = "购买主线公主级过关次数消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeBranchMaidenLevel);
            StatisticsKeys[StatisticsType.DiamondConsumeBranchMaidenLevel] = "购买支线少女级过关次数消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBranchMaidenLevelCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBranchMaidenLevelCount] = "购买支线少女级过关次数消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBranchMaidenLevelTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBranchMaidenLevelTimes] = "购买支线少女级过关次数消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeBranchPrincessLevel);
            StatisticsKeys[StatisticsType.DiamondConsumeBranchPrincessLevel] = "购买支线公主级过关次数消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBranchPrincessLevelCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBranchPrincessLevelCount] = "购买支线公主级过关次数消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBranchPrincessLevelTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBranchPrincessLevelTimes] = "购买支线公主级过关次数消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyGold);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyGold] = "购买金币消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyGoldCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyGoldCount] = "购买金币消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyGoldTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyGoldTimes] = "购买金币消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyStamina);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyStamina] = "购买体力消耗钻石数量";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyStaminaCount);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyStaminaCount] = "购买体力消耗钻石人数";

            req.conditions.Add(StatisticsType.DiamondConsumeBuyStaminaTimes);
            StatisticsKeys[StatisticsType.DiamondConsumeBuyStaminaTimes] = "购买体力消耗钻石人次";

            req.conditions.Add(StatisticsType.DiamondConsumeTotalCount);
            StatisticsKeys[StatisticsType.DiamondConsumeTotalCount] = "钻石消耗总量";

            req.conditions.Add(StatisticsType.DiamondProductConsumeRate);
            StatisticsKeys[StatisticsType.DiamondProductConsumeRate] = "钻石产出消耗比";

            SendMessage(req);
        }


        /// <summary>
        /// 参与PK用户等级分布
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPkUserGarde_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.PKUserGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }

        /// <summary>
        /// 公主级关卡用户等级分布
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrincessLevel_Grade_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.PrincessLevelGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }

        /// <summary>
        /// 少女级关卡用户等级分布
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMaidenLevel_Grade_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.MaidenLevelGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }

        /// <summary>
        /// pk兑换商城
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPKMall_Click(object sender, EventArgs e)
        {
            StatisticsItemDataReq req = new StatisticsItemDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.PKMall;
            SendMessage(req);
        }

        /// <summary>
        /// 制作系统
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnManufacture_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Manufacture;

            req.conditions.Add(StatisticsType.ManufactureClickCount);
            StatisticsKeys[StatisticsType.ManufactureClickCount] = "制作系统体验人数";

            req.conditions.Add(StatisticsType.ManufactureClickTimes);
            StatisticsKeys[StatisticsType.ManufactureClickTimes] = "制作系统体验次数";

            req.conditions.Add(StatisticsType.ManufactureMakeClickCount);
            StatisticsKeys[StatisticsType.ManufactureMakeClickCount] = "点击制作人数";

            req.conditions.Add(StatisticsType.ManufactureMakeClickTimes);
            StatisticsKeys[StatisticsType.ManufactureMakeClickTimes] = "点击制作次数";

            req.conditions.Add(StatisticsType.ManufactureMakeClickRate);
            StatisticsKeys[StatisticsType.ManufactureMakeClickRate] = "点击制作活跃度";

            req.conditions.Add(StatisticsType.ManufactureMakeCount);
            StatisticsKeys[StatisticsType.ManufactureMakeCount] = "参与制作人数";

            req.conditions.Add(StatisticsType.ManufactureMakeTimes);
            StatisticsKeys[StatisticsType.ManufactureMakeTimes] = "参与制作次数";

            req.conditions.Add(StatisticsType.ManufactureDyeClickCount);
            StatisticsKeys[StatisticsType.ManufactureDyeClickCount] = "点击染色人数";

            req.conditions.Add(StatisticsType.ManufactureDyeClickTimes);
            StatisticsKeys[StatisticsType.ManufactureDyeClickTimes] = "点击染色次数";

            req.conditions.Add(StatisticsType.ManufactureDyeClickRate);
            StatisticsKeys[StatisticsType.ManufactureDyeClickRate] = "点击染色活跃度";

            req.conditions.Add(StatisticsType.ManufactureDyeCount);
            StatisticsKeys[StatisticsType.ManufactureDyeCount] = "参与染色人数";

            req.conditions.Add(StatisticsType.ManufactureDyeTimes);
            StatisticsKeys[StatisticsType.ManufactureDyeTimes] = "参与染色次数";

            req.conditions.Add(StatisticsType.ManufactureEvolveClickCount);
            StatisticsKeys[StatisticsType.ManufactureEvolveClickCount] = "点击进化人数";

            req.conditions.Add(StatisticsType.ManufactureEvolveClickTimes);
            StatisticsKeys[StatisticsType.ManufactureEvolveClickTimes] = "点击进化次数";

            req.conditions.Add(StatisticsType.ManufactureEvolveClickRate);
            StatisticsKeys[StatisticsType.ManufactureEvolveClickRate] = "点击进化活跃度";

            req.conditions.Add(StatisticsType.ManufactureEvolveCount);
            StatisticsKeys[StatisticsType.ManufactureEvolveCount] = "参与进化人数";

            req.conditions.Add(StatisticsType.ManufactureEvolveTimes);
            StatisticsKeys[StatisticsType.ManufactureEvolveTimes] = "参与进化次数";

            SendMessage(req);
        }

        /// <summary>
        /// 制作系统物品产出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnManufactureProduct_Click(object sender, EventArgs e)
        {
            StatisticsItemDataReq req = new StatisticsItemDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ManufactureProductItem;
            SendMessage(req);
        }

        /// <summary>
        /// 七天累登
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SevenDayLogin_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.SevenDayLogin;

            req.conditions.Add(StatisticsType.SevenDayRewardDrawedTotalCount);
            StatisticsKeys[StatisticsType.SevenDayRewardDrawedTotalCount] = "领奖总人数";

            req.conditions.Add(StatisticsType.SevenDayCurDayRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDayCurDayRewardDrawedCount] = "每日领奖人数";

            req.conditions.Add(StatisticsType.SevenDayContinueTwoCount);
            StatisticsKeys[StatisticsType.SevenDayContinueTwoCount] = "连续两天领奖人数";

            req.conditions.Add(StatisticsType.SevenDayContinueThreeCount);
            StatisticsKeys[StatisticsType.SevenDayContinueThreeCount] = "连续三天领奖人数";

            req.conditions.Add(StatisticsType.SevenDayContinueFourCount);
            StatisticsKeys[StatisticsType.SevenDayContinueFourCount] = "连续四天领奖人数";

            req.conditions.Add(StatisticsType.SevenDayContinueFiveCount);
            StatisticsKeys[StatisticsType.SevenDayContinueFiveCount] = "连续五天领奖人数";

            req.conditions.Add(StatisticsType.SevenDayContinueSixCount);
            StatisticsKeys[StatisticsType.SevenDayContinueSixCount] = "连续六天领奖人数";

            req.conditions.Add(StatisticsType.SevenDayContinueSevenCount);
            StatisticsKeys[StatisticsType.SevenDayContinueSevenCount] = "连续七天领奖人数";

            req.conditions.Add(StatisticsType.SevenDayFirstRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDayFirstRewardDrawedCount] = "领取第一个奖励人数";

            req.conditions.Add(StatisticsType.SevenDayFirstRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDayFirstRewardTotalCount] = "领取第一个奖励总数";

            req.conditions.Add(StatisticsType.SevenDaySecondRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDaySecondRewardDrawedCount] = "领取第二个奖励人数";

            req.conditions.Add(StatisticsType.SevenDaySecondRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDaySecondRewardTotalCount] = "领取第二个奖励总数";

            req.conditions.Add(StatisticsType.SevenDayThirdRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDayThirdRewardDrawedCount] = "领取第三个奖励人数";

            req.conditions.Add(StatisticsType.SevenDayThirdRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDayThirdRewardTotalCount] = "领取第三个奖励总数";

            req.conditions.Add(StatisticsType.SevenDayForthRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDayForthRewardDrawedCount] = "领取第四个奖励人数";

            req.conditions.Add(StatisticsType.SevenDayForthRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDayForthRewardTotalCount] = "领取第四个奖励总数";

            req.conditions.Add(StatisticsType.SevenDayFifthRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDayFifthRewardDrawedCount] = "领取第五个奖励人数";

            req.conditions.Add(StatisticsType.SevenDayFifthRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDayFifthRewardTotalCount] = "领取第五个奖励总数";

            req.conditions.Add(StatisticsType.SevenDaySixthRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDaySixthRewardDrawedCount] = "领取第六个奖励人数";

            req.conditions.Add(StatisticsType.SevenDaySixthRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDaySixthRewardTotalCount] = "领取第六个奖励总数";

            req.conditions.Add(StatisticsType.SevenDaySeventhRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDaySeventhRewardDrawedCount] = "领取第七个奖励人数";

            req.conditions.Add(StatisticsType.SevenDaySeventhRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDaySeventhRewardTotalCount] = "领取第七个奖励总数";

            req.conditions.Add(StatisticsType.SevenDayEighthRewardDrawedCount);
            StatisticsKeys[StatisticsType.SevenDayEighthRewardDrawedCount] = "领取第八个奖励人数";

            req.conditions.Add(StatisticsType.SevenDayEighthRewardTotalCount);
            StatisticsKeys[StatisticsType.SevenDayEighthRewardTotalCount] = "领取第八个奖励总数";

            SendMessage(req);
        }

        /// <summary>
        /// 每日任务
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_DaysTask_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.DaysTask;

            req.conditions.Add(StatisticsType.TaskOneCompleteCount);
            StatisticsKeys[StatisticsType.TaskOneCompleteCount] = "任务1完成人数";

            req.conditions.Add(StatisticsType.TaskTwoCompleteCount);
            StatisticsKeys[StatisticsType.TaskTwoCompleteCount] = "任务2完成人数";

            req.conditions.Add(StatisticsType.TaskThreeCompleteCount);
            StatisticsKeys[StatisticsType.TaskThreeCompleteCount] = "任务3完成人数";

            req.conditions.Add(StatisticsType.TaskFourCompleteCount);
            StatisticsKeys[StatisticsType.TaskFourCompleteCount] = "任务4完成人数";

            req.conditions.Add(StatisticsType.TaskFiveCompleteCount);
            StatisticsKeys[StatisticsType.TaskFiveCompleteCount] = "任务5完成人数";

            req.conditions.Add(StatisticsType.TaskSixCompleteCount);
            StatisticsKeys[StatisticsType.TaskSixCompleteCount] = "任务6完成人数";

            req.conditions.Add(StatisticsType.TaskSevenCompleteCount);
            StatisticsKeys[StatisticsType.TaskSevenCompleteCount] = "任务7完成人数";

            req.conditions.Add(StatisticsType.TaskEightCompleteCount);
            StatisticsKeys[StatisticsType.TaskEightCompleteCount] = "任务8完成人数";

            req.conditions.Add(StatisticsType.Complete1TaskCount);
            StatisticsKeys[StatisticsType.Complete1TaskCount] = "完成1个任务人数";

            req.conditions.Add(StatisticsType.Complete2TaskCount);
            StatisticsKeys[StatisticsType.Complete2TaskCount] = "完成2个任务人数";

            req.conditions.Add(StatisticsType.Complete3TaskCount);
            StatisticsKeys[StatisticsType.Complete3TaskCount] = "完成3个任务人数";

            req.conditions.Add(StatisticsType.Complete4TaskCount);
            StatisticsKeys[StatisticsType.Complete4TaskCount] = "完成4个任务人数";

            req.conditions.Add(StatisticsType.Complete5TaskCount);
            StatisticsKeys[StatisticsType.Complete5TaskCount] = "完成5个任务人数";

            req.conditions.Add(StatisticsType.Complete6TaskCount);
            StatisticsKeys[StatisticsType.Complete6TaskCount] = "完成6个任务人数";

            req.conditions.Add(StatisticsType.Complete7TaskCount);
            StatisticsKeys[StatisticsType.Complete7TaskCount] = "完成7个任务人数";

            req.conditions.Add(StatisticsType.Complete8TaskCount);
            StatisticsKeys[StatisticsType.Complete8TaskCount] = "完成8个任务人数";

            SendMessage(req);
        }

        /// <summary>
        /// 玩家改名
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Character_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Character;

            req.conditions.Add(StatisticsType.ChangeNameFreeCount);
            StatisticsKeys[StatisticsType.ChangeNameFreeCount] = "免费改名人数";

            req.conditions.Add(StatisticsType.ChangeNamePayCount);
            StatisticsKeys[StatisticsType.ChangeNamePayCount] = "付费改名人数";

            SendMessage(req);
        }

        /// <summary>
        /// 制作系统用户等级
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ManufatureUserGrade_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ManufactureUserGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数 制作|染色|进化";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数 制作|染色|进化";

            SendMessage(req);
        }

        /// <summary>
        /// 各得分等级人数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ScoreGradeCount_Click(object sender, EventArgs e)
        {
            StatisticsNotDailyDataReq req = new StatisticsNotDailyDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ScenarioScoreGrade;

            req.conditions.Add(StatisticsType.PassLevelPlayerCount);
            StatisticsKeys[StatisticsType.PassLevelPlayerCount] = "过关人数";

            req.conditions.Add(StatisticsType.SCount);
            StatisticsKeys[StatisticsType.SCount] = "S级过关人数";

            req.conditions.Add(StatisticsType.STimes);
            StatisticsKeys[StatisticsType.STimes] = "S级过关次数";

            req.conditions.Add(StatisticsType.ACount);
            StatisticsKeys[StatisticsType.ACount] = "A级过关人数";

            req.conditions.Add(StatisticsType.ATimes);
            StatisticsKeys[StatisticsType.ATimes] = "A级过关次数";

            req.conditions.Add(StatisticsType.BCount);
            StatisticsKeys[StatisticsType.BCount] = "B级过关人数";

            req.conditions.Add(StatisticsType.BTimes);
            StatisticsKeys[StatisticsType.BTimes] = "B级过关次数";

            req.conditions.Add(StatisticsType.CCount);
            StatisticsKeys[StatisticsType.CCount] = "C级过关人数";

            req.conditions.Add(StatisticsType.CTimes);
            StatisticsKeys[StatisticsType.CTimes] = "C级过关次数";

            req.conditions.Add(StatisticsType.FCount);
            StatisticsKeys[StatisticsType.FCount] = "F级过关人数";

            req.conditions.Add(StatisticsType.FTimes);
            StatisticsKeys[StatisticsType.FTimes] = "F级过关次数";

            req.conditions.Add(StatisticsType.EachGradeRate);
            StatisticsKeys[StatisticsType.EachGradeRate] = "S:A:B:C:F";

            SendMessage(req);
        }


        //读取静态数据表数据
        private Dictionary<int, string> GetItemData()
        {
            Dictionary<int, string> itemDatas = new Dictionary<int, string>();

            string ProcessName = Process.GetCurrentProcess().MainModule.FileName;
            string PathOfEXE = Path.GetDirectoryName(ProcessName);
            string dataPath = PathOfEXE + @"\..\..\..\..\Client\Datas\ItemData.csv";
            using (StreamReader sr = new StreamReader(dataPath, Encoding.Default))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] itemComponents = line.Split('|');
                    if (itemComponents.Length > 0)
                    {
                        int id = 0;
                        if (int.TryParse(itemComponents[0], out id))
                        {
                            itemDatas.Add(id, itemComponents[1]);
                        }
                    }
                    line = sr.ReadLine();
                }
            }

            return itemDatas;
        }

        /// <summary>
        /// 获取制作系统-染色数据
        /// </summary>
        /// <returns></returns>
        private List<int> GetManufactureDyeData()
        {
            List<int> ids = new List<int>();

            string ProcessName = Process.GetCurrentProcess().MainModule.FileName;
            string PathOfEXE = Path.GetDirectoryName(ProcessName);
            string dataPath = PathOfEXE + @"\..\..\..\..\Client\Datas\ManufactureDyeData.csv";
            using (StreamReader sr = new StreamReader(dataPath, Encoding.Default))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] itemComponents = line.Split('|');
                    if (itemComponents.Length > 0)
                    {
                        int id = 0;
                        if (int.TryParse(itemComponents[1], out id))
                        {
                            if (!ids.Contains(id))
                            {
                                ids.Add(id);
                            }
                        }

                        if (int.TryParse(itemComponents[6], out id))
                        {
                            if (!ids.Contains(id))
                            {
                                ids.Add(id);
                            }
                        }

                        if (int.TryParse(itemComponents[9], out id))
                        {
                            if (!ids.Contains(id))
                            {
                                ids.Add(id);
                            }
                        }
                    }
                    line = sr.ReadLine();
                }
            }

            return ids;
        }

        /// <summary>
        /// 获取物品名称
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        private string GetItemName(Dictionary<int, string> itemDatas, int itemId)
        {
            string name = string.Empty;
            itemDatas.TryGetValue(itemId, out name);
            return name;
        }

        /// <summary>
        /// 获取静态数据
        /// </summary>
        /// <param name="dataName"></param>
        private List<int> GetStatisticsData(string dataName)
        {
            List<int> ids = new List<int>();

            string processName = Process.GetCurrentProcess().MainModule.FileName;
            string pathOfEXE = Path.GetDirectoryName(processName);
            string pathOfData = @"\..\..\..\..\Client\Datas\";
            string dataPath = string.Format("{0}{1}{2}.csv", pathOfEXE, pathOfData, dataName);
            using (StreamReader sr = new StreamReader(dataPath, Encoding.Default))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] itemComponents = line.Split('|');
                    if (itemComponents.Length > 0)
                    {
                        int id = 0;
                        if (int.TryParse(itemComponents[0], out id))
                        {
                            ids.Add(id);
                        }
                    }
                    line = sr.ReadLine();
                }
            }

            return ids;
        }


        #region  //Spare Code

        private void btnBranchLevelUnlock_Click(object sender, EventArgs e)
        {
            StatisticsNotDailyDataReq req = new StatisticsNotDailyDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ScenarioBranchLevel;

            req.conditions.Add(StatisticsType.BranchLevelUnlockCount);
            StatisticsKeys[StatisticsType.BranchLevelUnlockCount] = "分支关卡解锁人数";

            req.conditions.Add(StatisticsType.BranchLevelNomalUnlockCount);
            StatisticsKeys[StatisticsType.BranchLevelNomalUnlockCount] = "分支关卡正常解锁人数";

            req.conditions.Add(StatisticsType.BranchLevelPayUnlockCount);
            StatisticsKeys[StatisticsType.BranchLevelPayUnlockCount] = "分支关卡付费解锁人数";

            req.conditions.Add(StatisticsType.BranchLevelUnlockRate);
            StatisticsKeys[StatisticsType.BranchLevelUnlockRate] = "购买解锁人数与解锁总人数比";

            SendMessage(req);
        }

        private void btnBoyFriend_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.BoyFriend;

            req.conditions.Add(StatisticsType.BoyFriendCount);
            StatisticsKeys[StatisticsType.BoyFriendCount] = "每日参与男友系统人数";

            req.conditions.Add(StatisticsType.BoyFriendTimes);
            StatisticsKeys[StatisticsType.BoyFriendTimes] = "每日参与男友系统次数";

            req.conditions.Add(StatisticsType.BoyFriendRate);
            StatisticsKeys[StatisticsType.BoyFriendRate] = "参与男友系统人数与活跃人数比率";

            req.conditions.Add(StatisticsType.BoyFriendAverageTime);
            StatisticsKeys[StatisticsType.BoyFriendAverageTime] = "每次参与男友系统的平均时长";

            SendMessage(req);
        }

        private void bntBoyDating_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.BoyFriendDating;

            req.conditions.Add(StatisticsType.BoyFriendDatingCount);
            StatisticsKeys[StatisticsType.BoyFriendDatingCount] = "每日参与约会的人数";

            req.conditions.Add(StatisticsType.BoyFriendDatingTimes);
            StatisticsKeys[StatisticsType.BoyFriendDatingTimes] = "每日参与约会的次数";

            req.conditions.Add(StatisticsType.BoyFriendDatingRate);
            StatisticsKeys[StatisticsType.BoyFriendDatingRate] = "参与约会的人数与活跃人数比率";

            req.conditions.Add(StatisticsType.BoyFriendDatingAverageTime);
            StatisticsKeys[StatisticsType.BoyFriendDatingAverageTime] = "每次参与约会的平均时长";

            SendMessage(req);
        }

        private void bntBoySendGift_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.BoyFriendSendGift;

            req.conditions.Add(StatisticsType.BoyFriendSendGiftCount);
            StatisticsKeys[StatisticsType.BoyFriendSendGiftCount] = "每日参与送礼的人数";

            req.conditions.Add(StatisticsType.BoyFriendSendGiftTimes);
            StatisticsKeys[StatisticsType.BoyFriendSendGiftTimes] = "每日参与送礼的次数";

            req.conditions.Add(StatisticsType.BoyFriendSendGiftRate);
            StatisticsKeys[StatisticsType.BoyFriendSendGiftRate] = "参与送礼的人数与活跃人数比率";

            SendMessage(req);
        }

        private void btnBoyUserGrade_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.BoyFriendUserGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }


        #endregion


        /// <summary>
        /// 充值消费相关
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Money_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Money;

            req.conditions.Add(StatisticsType.DayCostSum);
            StatisticsKeys[StatisticsType.DayCostSum] = "日消费额";

            req.conditions.Add(StatisticsType.DiamondConsumePlayerCount);
            StatisticsKeys[StatisticsType.DiamondConsumePlayerCount] = "日消费人数";

            req.conditions.Add(StatisticsType.DiamondConsumeActiveRate);
            StatisticsKeys[StatisticsType.DiamondConsumeActiveRate] = "购买活跃人数";

            req.conditions.Add(StatisticsType.DiamondConsumeArpu);
            StatisticsKeys[StatisticsType.DiamondConsumeArpu] = "消费Arpu ";

            req.conditions.Add(StatisticsType.DiamondConsumeActiveArpu);
            StatisticsKeys[StatisticsType.DiamondConsumeActiveArpu] = "活跃用户消费Arpu";

            SendMessage(req);
        }

        /// <summary>
        /// 签到
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSign_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.Signin;

            req.conditions.Add(StatisticsType.SigninCount);
            StatisticsKeys[StatisticsType.SigninCount] = "签到人数";

            req.conditions.Add(StatisticsType.SigninLiveness);
            StatisticsKeys[StatisticsType.SigninLiveness] = "签到活跃度";

            SendMessage(req);
        }

        /// <summary>
        /// 线上奖励
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOnlineReward_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.OnlineReward;

            req.conditions.Add(StatisticsType.DrawStaminaNoonCount);
            StatisticsKeys[StatisticsType.DrawStaminaNoonCount] = "中午领体力人数";

            req.conditions.Add(StatisticsType.DrawStaminaNightCount);
            StatisticsKeys[StatisticsType.DrawStaminaNightCount] = "晚上领体力人数";

            req.conditions.Add(StatisticsType.DrawStaminaTwoTimesCount);
            StatisticsKeys[StatisticsType.DrawStaminaTwoTimesCount] = "两次领体力人数";

            SendMessage(req);
        }

        /// <summary>
        /// 各服饰类型下各收集度的人数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnItemTypeClct_Click(object sender, EventArgs e)
        {
            StatisticsNotDailyDataReq req = new StatisticsNotDailyDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ItemTypeCollection;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "收集度0-9.99%";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "收集度10-19.99%";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "收集度20-29.99%";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "收集度30-39.99%";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "收集度40-49.99%";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "收集度50-59.99%";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "收集度60-69.99%";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "收集度70-79.99%";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "收集度80-89.99%";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "收集度90-100%";

            SendMessage(req);
        }

        /// <summary>
        /// 新手引导完成情况
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNoviceGuide_Click(object sender, EventArgs e)
        {
            StatisticsItemDataReq req = new StatisticsItemDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.NoviceGuide;
            SendMessage(req);
        }

        /// <summary>
        /// 流失用户等级分布
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoseGrade_Click(object sender, EventArgs e)
        {
            StatisticsDataReq req = new StatisticsDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.LoseUserGrade;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "1-10级人数";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "11-20级人数";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "21-30级人数";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "31-40级人数";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "41-50级人数";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "51-60级人数";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "61-70级人数";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "71-80级人数";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "81-90级人数";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "91-100级人数";

            SendMessage(req);
        }

        /// <summary>
        /// 服饰收集度
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnItemCollection_Click(object sender, EventArgs e)
        {
            StatisticsNotDailyDataReq req = new StatisticsNotDailyDataReq();
            req.gameId = 1;
            req.zoneId = 1;
            req.moduleName = ModuleName.ItemRegionCount;

            req.conditions.Add(StatisticsType.zero);
            StatisticsKeys[StatisticsType.zero] = "收集度0-9.99%";

            req.conditions.Add(StatisticsType.ten);
            StatisticsKeys[StatisticsType.ten] = "收集度10-19.99%";

            req.conditions.Add(StatisticsType.twenty);
            StatisticsKeys[StatisticsType.twenty] = "收集度20-29.99%";

            req.conditions.Add(StatisticsType.thirty);
            StatisticsKeys[StatisticsType.thirty] = "收集度30-39.99%";

            req.conditions.Add(StatisticsType.forty);
            StatisticsKeys[StatisticsType.forty] = "收集度40-49.99%";

            req.conditions.Add(StatisticsType.fifty);
            StatisticsKeys[StatisticsType.fifty] = "收集度50-59.99%";

            req.conditions.Add(StatisticsType.sixty);
            StatisticsKeys[StatisticsType.sixty] = "收集度60-69.99%";

            req.conditions.Add(StatisticsType.seventy);
            StatisticsKeys[StatisticsType.seventy] = "收集度70-79.99%";

            req.conditions.Add(StatisticsType.eightty);
            StatisticsKeys[StatisticsType.eightty] = "收集度80-89.99%";

            req.conditions.Add(StatisticsType.ninety);
            StatisticsKeys[StatisticsType.ninety] = "收集度90-100%";

            SendMessage(req);
        }
    }
}
