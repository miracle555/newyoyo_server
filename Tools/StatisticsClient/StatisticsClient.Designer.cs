﻿namespace StatisticsClient
{
    partial class StatisticsClient
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.TBOutput = new System.Windows.Forms.TextBox();
            this.BtnLogout = new System.Windows.Forms.Button();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.TBPassword = new System.Windows.Forms.TextBox();
            this.LBPassword = new System.Windows.Forms.Label();
            this.LBAccount = new System.Windows.Forms.Label();
            this.LBServerIp = new System.Windows.Forms.Label();
            this.TBAccount = new System.Windows.Forms.TextBox();
            this.TBServerIp = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnLoseGrade = new System.Windows.Forms.Button();
            this.btnNoviceGuide = new System.Windows.Forms.Button();
            this.btnItemCollection = new System.Windows.Forms.Button();
            this.btnItemTypeClct = new System.Windows.Forms.Button();
            this.btnOnlineReward = new System.Windows.Forms.Button();
            this.btnSign = new System.Windows.Forms.Button();
            this.btn_Money = new System.Windows.Forms.Button();
            this.btn_ScoreGradeCount = new System.Windows.Forms.Button();
            this.btn_ManufatureUserGrade = new System.Windows.Forms.Button();
            this.btn_DaysTask = new System.Windows.Forms.Button();
            this.btn_SevenDayLogin = new System.Windows.Forms.Button();
            this.btnManufactureProduct = new System.Windows.Forms.Button();
            this.btnManufacture = new System.Windows.Forms.Button();
            this.btnPKMall = new System.Windows.Forms.Button();
            this.btnMaidenLevel_Grade = new System.Windows.Forms.Button();
            this.btnPrincessLevel_Grade = new System.Windows.Forms.Button();
            this.btnPkUserGarde = new System.Windows.Forms.Button();
            this.btnMoneyConAPro = new System.Windows.Forms.Button();
            this.btnMallBuyCount = new System.Windows.Forms.Button();
            this.btnItemUseTimes = new System.Windows.Forms.Button();
            this.btnSceUserGrade = new System.Windows.Forms.Button();
            this.btnScenario = new System.Windows.Forms.Button();
            this.btnItemIncome = new System.Windows.Forms.Button();
            this.btnSaleCount = new System.Windows.Forms.Button();
            this.btnPK = new System.Windows.Forms.Button();
            this.btnDressUserGrade = new System.Windows.Forms.Button();
            this.btnDress = new System.Windows.Forms.Button();
            this.btnRemain = new System.Windows.Forms.Button();
            this.BtnLevelDistribution = new System.Windows.Forms.Button();
            this.BtnStatistics = new System.Windows.Forms.Button();
            this.DGVReport = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVReport)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.TBOutput);
            this.panel1.Controls.Add(this.BtnLogout);
            this.panel1.Controls.Add(this.BtnLogin);
            this.panel1.Controls.Add(this.TBPassword);
            this.panel1.Controls.Add(this.LBPassword);
            this.panel1.Controls.Add(this.LBAccount);
            this.panel1.Controls.Add(this.LBServerIp);
            this.panel1.Controls.Add(this.TBAccount);
            this.panel1.Controls.Add(this.TBServerIp);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1322, 135);
            this.panel1.TabIndex = 1;
            // 
            // TBOutput
            // 
            this.TBOutput.Location = new System.Drawing.Point(188, 3);
            this.TBOutput.Multiline = true;
            this.TBOutput.Name = "TBOutput";
            this.TBOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.TBOutput.Size = new System.Drawing.Size(1131, 118);
            this.TBOutput.TabIndex = 2;
            // 
            // BtnLogout
            // 
            this.BtnLogout.Location = new System.Drawing.Point(5, 109);
            this.BtnLogout.Name = "BtnLogout";
            this.BtnLogout.Size = new System.Drawing.Size(177, 23);
            this.BtnLogout.TabIndex = 7;
            this.BtnLogout.Text = "登出";
            this.BtnLogout.UseVisualStyleBackColor = true;
            this.BtnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
            // 
            // BtnLogin
            // 
            this.BtnLogin.Location = new System.Drawing.Point(5, 80);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(177, 23);
            this.BtnLogin.TabIndex = 6;
            this.BtnLogin.Text = "登录";
            this.BtnLogin.UseVisualStyleBackColor = true;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // TBPassword
            // 
            this.TBPassword.Location = new System.Drawing.Point(50, 51);
            this.TBPassword.Name = "TBPassword";
            this.TBPassword.Size = new System.Drawing.Size(132, 21);
            this.TBPassword.TabIndex = 5;
            this.TBPassword.UseSystemPasswordChar = true;
            // 
            // LBPassword
            // 
            this.LBPassword.AutoSize = true;
            this.LBPassword.Location = new System.Drawing.Point(3, 54);
            this.LBPassword.Name = "LBPassword";
            this.LBPassword.Size = new System.Drawing.Size(29, 12);
            this.LBPassword.TabIndex = 4;
            this.LBPassword.Text = "密码";
            // 
            // LBAccount
            // 
            this.LBAccount.AutoSize = true;
            this.LBAccount.Location = new System.Drawing.Point(3, 30);
            this.LBAccount.Name = "LBAccount";
            this.LBAccount.Size = new System.Drawing.Size(29, 12);
            this.LBAccount.TabIndex = 3;
            this.LBAccount.Text = "帐号";
            // 
            // LBServerIp
            // 
            this.LBServerIp.AutoSize = true;
            this.LBServerIp.Location = new System.Drawing.Point(3, 6);
            this.LBServerIp.Name = "LBServerIp";
            this.LBServerIp.Size = new System.Drawing.Size(41, 12);
            this.LBServerIp.TabIndex = 2;
            this.LBServerIp.Text = "服务器";
            // 
            // TBAccount
            // 
            this.TBAccount.Location = new System.Drawing.Point(50, 27);
            this.TBAccount.Name = "TBAccount";
            this.TBAccount.Size = new System.Drawing.Size(132, 21);
            this.TBAccount.TabIndex = 1;
            // 
            // TBServerIp
            // 
            this.TBServerIp.Location = new System.Drawing.Point(50, 3);
            this.TBServerIp.Name = "TBServerIp";
            this.TBServerIp.Size = new System.Drawing.Size(132, 21);
            this.TBServerIp.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnLoseGrade);
            this.panel2.Controls.Add(this.btnNoviceGuide);
            this.panel2.Controls.Add(this.btnItemCollection);
            this.panel2.Controls.Add(this.btnItemTypeClct);
            this.panel2.Controls.Add(this.btnOnlineReward);
            this.panel2.Controls.Add(this.btnSign);
            this.panel2.Controls.Add(this.btn_Money);
            this.panel2.Controls.Add(this.btn_ScoreGradeCount);
            this.panel2.Controls.Add(this.btn_ManufatureUserGrade);
            this.panel2.Controls.Add(this.btn_DaysTask);
            this.panel2.Controls.Add(this.btn_SevenDayLogin);
            this.panel2.Controls.Add(this.btnManufactureProduct);
            this.panel2.Controls.Add(this.btnManufacture);
            this.panel2.Controls.Add(this.btnPKMall);
            this.panel2.Controls.Add(this.btnMaidenLevel_Grade);
            this.panel2.Controls.Add(this.btnPrincessLevel_Grade);
            this.panel2.Controls.Add(this.btnPkUserGarde);
            this.panel2.Controls.Add(this.btnMoneyConAPro);
            this.panel2.Controls.Add(this.btnMallBuyCount);
            this.panel2.Controls.Add(this.btnItemUseTimes);
            this.panel2.Controls.Add(this.btnSceUserGrade);
            this.panel2.Controls.Add(this.btnScenario);
            this.panel2.Controls.Add(this.btnItemIncome);
            this.panel2.Controls.Add(this.btnSaleCount);
            this.panel2.Controls.Add(this.btnPK);
            this.panel2.Controls.Add(this.btnDressUserGrade);
            this.panel2.Controls.Add(this.btnDress);
            this.panel2.Controls.Add(this.btnRemain);
            this.panel2.Controls.Add(this.BtnLevelDistribution);
            this.panel2.Controls.Add(this.BtnStatistics);
            this.panel2.Controls.Add(this.DGVReport);
            this.panel2.Location = new System.Drawing.Point(12, 153);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1322, 880);
            this.panel2.TabIndex = 2;
            // 
            // btnLoseGrade
            // 
            this.btnLoseGrade.Location = new System.Drawing.Point(5, 844);
            this.btnLoseGrade.Name = "btnLoseGrade";
            this.btnLoseGrade.Size = new System.Drawing.Size(177, 23);
            this.btnLoseGrade.TabIndex = 38;
            this.btnLoseGrade.Text = "流失用户等级分布";
            this.btnLoseGrade.UseVisualStyleBackColor = true;
            this.btnLoseGrade.Click += new System.EventHandler(this.btnLoseGrade_Click);
            // 
            // btnNoviceGuide
            // 
            this.btnNoviceGuide.Location = new System.Drawing.Point(5, 815);
            this.btnNoviceGuide.Name = "btnNoviceGuide";
            this.btnNoviceGuide.Size = new System.Drawing.Size(177, 23);
            this.btnNoviceGuide.TabIndex = 37;
            this.btnNoviceGuide.Text = "新手引导完成情况";
            this.btnNoviceGuide.UseVisualStyleBackColor = true;
            this.btnNoviceGuide.Click += new System.EventHandler(this.btnNoviceGuide_Click);
            // 
            // btnItemCollection
            // 
            this.btnItemCollection.Location = new System.Drawing.Point(5, 785);
            this.btnItemCollection.Name = "btnItemCollection";
            this.btnItemCollection.Size = new System.Drawing.Size(177, 23);
            this.btnItemCollection.TabIndex = 36;
            this.btnItemCollection.Text = "服饰收集度";
            this.btnItemCollection.UseVisualStyleBackColor = true;
            this.btnItemCollection.Click += new System.EventHandler(this.btnItemCollection_Click);
            // 
            // btnItemTypeClct
            // 
            this.btnItemTypeClct.Location = new System.Drawing.Point(5, 756);
            this.btnItemTypeClct.Name = "btnItemTypeClct";
            this.btnItemTypeClct.Size = new System.Drawing.Size(177, 23);
            this.btnItemTypeClct.TabIndex = 35;
            this.btnItemTypeClct.Text = "服饰类型收集度";
            this.btnItemTypeClct.UseVisualStyleBackColor = true;
            this.btnItemTypeClct.Click += new System.EventHandler(this.btnItemTypeClct_Click);
            // 
            // btnOnlineReward
            // 
            this.btnOnlineReward.Location = new System.Drawing.Point(5, 726);
            this.btnOnlineReward.Name = "btnOnlineReward";
            this.btnOnlineReward.Size = new System.Drawing.Size(177, 23);
            this.btnOnlineReward.TabIndex = 34;
            this.btnOnlineReward.Text = "上线奖励";
            this.btnOnlineReward.UseVisualStyleBackColor = true;
            this.btnOnlineReward.Click += new System.EventHandler(this.btnOnlineReward_Click);
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(5, 697);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(177, 23);
            this.btnSign.TabIndex = 33;
            this.btnSign.Text = "签到";
            this.btnSign.UseVisualStyleBackColor = true;
            this.btnSign.Click += new System.EventHandler(this.btnSign_Click);
            // 
            // btn_Money
            // 
            this.btn_Money.Location = new System.Drawing.Point(5, 668);
            this.btn_Money.Name = "btn_Money";
            this.btn_Money.Size = new System.Drawing.Size(179, 23);
            this.btn_Money.TabIndex = 32;
            this.btn_Money.Text = "充值消费";
            this.btn_Money.UseVisualStyleBackColor = true;
            this.btn_Money.Click += new System.EventHandler(this.btn_Money_Click);
            // 
            // btn_ScoreGradeCount
            // 
            this.btn_ScoreGradeCount.Location = new System.Drawing.Point(5, 379);
            this.btn_ScoreGradeCount.Name = "btn_ScoreGradeCount";
            this.btn_ScoreGradeCount.Size = new System.Drawing.Size(177, 23);
            this.btn_ScoreGradeCount.TabIndex = 31;
            this.btn_ScoreGradeCount.Text = "各得分等级人数";
            this.btn_ScoreGradeCount.UseVisualStyleBackColor = true;
            this.btn_ScoreGradeCount.Click += new System.EventHandler(this.btn_ScoreGradeCount_Click);
            // 
            // btn_ManufatureUserGrade
            // 
            this.btn_ManufatureUserGrade.Location = new System.Drawing.Point(5, 552);
            this.btn_ManufatureUserGrade.Name = "btn_ManufatureUserGrade";
            this.btn_ManufatureUserGrade.Size = new System.Drawing.Size(179, 23);
            this.btn_ManufatureUserGrade.TabIndex = 30;
            this.btn_ManufatureUserGrade.Text = "参与制作系统用户等级";
            this.btn_ManufatureUserGrade.UseVisualStyleBackColor = true;
            this.btn_ManufatureUserGrade.Click += new System.EventHandler(this.btn_ManufatureUserGrade_Click);
            // 
            // btn_DaysTask
            // 
            this.btn_DaysTask.Location = new System.Drawing.Point(5, 639);
            this.btn_DaysTask.Name = "btn_DaysTask";
            this.btn_DaysTask.Size = new System.Drawing.Size(179, 23);
            this.btn_DaysTask.TabIndex = 28;
            this.btn_DaysTask.Text = "每日任务";
            this.btn_DaysTask.UseVisualStyleBackColor = true;
            this.btn_DaysTask.Click += new System.EventHandler(this.btn_DaysTask_Click);
            // 
            // btn_SevenDayLogin
            // 
            this.btn_SevenDayLogin.Location = new System.Drawing.Point(5, 610);
            this.btn_SevenDayLogin.Name = "btn_SevenDayLogin";
            this.btn_SevenDayLogin.Size = new System.Drawing.Size(179, 23);
            this.btn_SevenDayLogin.TabIndex = 27;
            this.btn_SevenDayLogin.Text = "七天累登系统";
            this.btn_SevenDayLogin.UseVisualStyleBackColor = true;
            this.btn_SevenDayLogin.Click += new System.EventHandler(this.btn_SevenDayLogin_Click);
            // 
            // btnManufactureProduct
            // 
            this.btnManufactureProduct.Location = new System.Drawing.Point(5, 581);
            this.btnManufactureProduct.Name = "btnManufactureProduct";
            this.btnManufactureProduct.Size = new System.Drawing.Size(179, 23);
            this.btnManufactureProduct.TabIndex = 26;
            this.btnManufactureProduct.Text = "制作系统物品产出人数";
            this.btnManufactureProduct.UseVisualStyleBackColor = true;
            this.btnManufactureProduct.Click += new System.EventHandler(this.btnManufactureProduct_Click);
            // 
            // btnManufacture
            // 
            this.btnManufacture.Location = new System.Drawing.Point(5, 523);
            this.btnManufacture.Name = "btnManufacture";
            this.btnManufacture.Size = new System.Drawing.Size(179, 23);
            this.btnManufacture.TabIndex = 25;
            this.btnManufacture.Text = "制作系统";
            this.btnManufacture.UseVisualStyleBackColor = true;
            this.btnManufacture.Click += new System.EventHandler(this.btnManufacture_Click);
            // 
            // btnPKMall
            // 
            this.btnPKMall.Location = new System.Drawing.Point(5, 234);
            this.btnPKMall.Name = "btnPKMall";
            this.btnPKMall.Size = new System.Drawing.Size(177, 23);
            this.btnPKMall.TabIndex = 24;
            this.btnPKMall.Text = "PK兑换商城";
            this.btnPKMall.UseVisualStyleBackColor = true;
            this.btnPKMall.Click += new System.EventHandler(this.btnPKMall_Click);
            // 
            // btnMaidenLevel_Grade
            // 
            this.btnMaidenLevel_Grade.Location = new System.Drawing.Point(5, 350);
            this.btnMaidenLevel_Grade.Name = "btnMaidenLevel_Grade";
            this.btnMaidenLevel_Grade.Size = new System.Drawing.Size(177, 23);
            this.btnMaidenLevel_Grade.TabIndex = 23;
            this.btnMaidenLevel_Grade.Text = "少女关卡用户等级";
            this.btnMaidenLevel_Grade.UseVisualStyleBackColor = true;
            this.btnMaidenLevel_Grade.Click += new System.EventHandler(this.btnMaidenLevel_Grade_Click);
            // 
            // btnPrincessLevel_Grade
            // 
            this.btnPrincessLevel_Grade.Location = new System.Drawing.Point(5, 321);
            this.btnPrincessLevel_Grade.Name = "btnPrincessLevel_Grade";
            this.btnPrincessLevel_Grade.Size = new System.Drawing.Size(177, 23);
            this.btnPrincessLevel_Grade.TabIndex = 22;
            this.btnPrincessLevel_Grade.Text = "公主关卡用户等级";
            this.btnPrincessLevel_Grade.UseVisualStyleBackColor = true;
            this.btnPrincessLevel_Grade.Click += new System.EventHandler(this.btnPrincessLevel_Grade_Click);
            // 
            // btnPkUserGarde
            // 
            this.btnPkUserGarde.Location = new System.Drawing.Point(5, 206);
            this.btnPkUserGarde.Name = "btnPkUserGarde";
            this.btnPkUserGarde.Size = new System.Drawing.Size(179, 23);
            this.btnPkUserGarde.TabIndex = 21;
            this.btnPkUserGarde.Text = "参与PK用户等级";
            this.btnPkUserGarde.UseVisualStyleBackColor = true;
            this.btnPkUserGarde.Click += new System.EventHandler(this.btnPkUserGarde_Click);
            // 
            // btnMoneyConAPro
            // 
            this.btnMoneyConAPro.Location = new System.Drawing.Point(5, 494);
            this.btnMoneyConAPro.Name = "btnMoneyConAPro";
            this.btnMoneyConAPro.Size = new System.Drawing.Size(179, 23);
            this.btnMoneyConAPro.TabIndex = 17;
            this.btnMoneyConAPro.Text = "代币消耗";
            this.btnMoneyConAPro.UseVisualStyleBackColor = true;
            this.btnMoneyConAPro.Click += new System.EventHandler(this.btnMoneyConAPro_Click);
            // 
            // btnMallBuyCount
            // 
            this.btnMallBuyCount.Location = new System.Drawing.Point(5, 466);
            this.btnMallBuyCount.Name = "btnMallBuyCount";
            this.btnMallBuyCount.Size = new System.Drawing.Size(179, 23);
            this.btnMallBuyCount.TabIndex = 12;
            this.btnMallBuyCount.Text = "商品的购买人数";
            this.btnMallBuyCount.UseVisualStyleBackColor = true;
            this.btnMallBuyCount.Click += new System.EventHandler(this.btnMallBuyCount_Click);
            // 
            // btnItemUseTimes
            // 
            this.btnItemUseTimes.Location = new System.Drawing.Point(5, 148);
            this.btnItemUseTimes.Name = "btnItemUseTimes";
            this.btnItemUseTimes.Size = new System.Drawing.Size(179, 23);
            this.btnItemUseTimes.TabIndex = 11;
            this.btnItemUseTimes.Text = "每件衣服使用次数";
            this.btnItemUseTimes.UseVisualStyleBackColor = true;
            this.btnItemUseTimes.Click += new System.EventHandler(this.btnItemUseTimes_Click);
            // 
            // btnSceUserGrade
            // 
            this.btnSceUserGrade.Location = new System.Drawing.Point(5, 292);
            this.btnSceUserGrade.Name = "btnSceUserGrade";
            this.btnSceUserGrade.Size = new System.Drawing.Size(179, 23);
            this.btnSceUserGrade.TabIndex = 10;
            this.btnSceUserGrade.Text = "参与剧情用户等级";
            this.btnSceUserGrade.UseVisualStyleBackColor = true;
            this.btnSceUserGrade.Click += new System.EventHandler(this.btnSceUserGrade_Click);
            // 
            // btnScenario
            // 
            this.btnScenario.Location = new System.Drawing.Point(5, 263);
            this.btnScenario.Name = "btnScenario";
            this.btnScenario.Size = new System.Drawing.Size(179, 23);
            this.btnScenario.TabIndex = 9;
            this.btnScenario.Text = "剧情系统";
            this.btnScenario.UseVisualStyleBackColor = true;
            this.btnScenario.Click += new System.EventHandler(this.btnScenario_Click);
            // 
            // btnItemIncome
            // 
            this.btnItemIncome.Location = new System.Drawing.Point(5, 437);
            this.btnItemIncome.Name = "btnItemIncome";
            this.btnItemIncome.Size = new System.Drawing.Size(179, 23);
            this.btnItemIncome.TabIndex = 8;
            this.btnItemIncome.Text = "商品每天的收入";
            this.btnItemIncome.UseVisualStyleBackColor = true;
            this.btnItemIncome.Click += new System.EventHandler(this.btnItemIncome_Click);
            // 
            // btnSaleCount
            // 
            this.btnSaleCount.Location = new System.Drawing.Point(5, 408);
            this.btnSaleCount.Name = "btnSaleCount";
            this.btnSaleCount.Size = new System.Drawing.Size(179, 23);
            this.btnSaleCount.TabIndex = 7;
            this.btnSaleCount.Text = "商品每天售卖数量";
            this.btnSaleCount.UseVisualStyleBackColor = true;
            this.btnSaleCount.Click += new System.EventHandler(this.btnSaleCount_Click);
            // 
            // btnPK
            // 
            this.btnPK.Location = new System.Drawing.Point(5, 177);
            this.btnPK.Name = "btnPK";
            this.btnPK.Size = new System.Drawing.Size(179, 23);
            this.btnPK.TabIndex = 6;
            this.btnPK.Text = "PK系统";
            this.btnPK.UseVisualStyleBackColor = true;
            this.btnPK.Click += new System.EventHandler(this.btnPK_Click);
            // 
            // btnDressUserGrade
            // 
            this.btnDressUserGrade.Location = new System.Drawing.Point(5, 119);
            this.btnDressUserGrade.Name = "btnDressUserGrade";
            this.btnDressUserGrade.Size = new System.Drawing.Size(179, 23);
            this.btnDressUserGrade.TabIndex = 5;
            this.btnDressUserGrade.Text = "换装用户等级分布";
            this.btnDressUserGrade.UseVisualStyleBackColor = true;
            this.btnDressUserGrade.Click += new System.EventHandler(this.btnDressUserGrade_Click);
            // 
            // btnDress
            // 
            this.btnDress.Location = new System.Drawing.Point(5, 90);
            this.btnDress.Name = "btnDress";
            this.btnDress.Size = new System.Drawing.Size(179, 23);
            this.btnDress.TabIndex = 4;
            this.btnDress.Text = "自由换装系统";
            this.btnDress.UseVisualStyleBackColor = true;
            this.btnDress.Click += new System.EventHandler(this.btnDress_Click);
            // 
            // btnRemain
            // 
            this.btnRemain.Location = new System.Drawing.Point(5, 61);
            this.btnRemain.Name = "btnRemain";
            this.btnRemain.Size = new System.Drawing.Size(179, 23);
            this.btnRemain.TabIndex = 3;
            this.btnRemain.Text = "留存相关";
            this.btnRemain.UseVisualStyleBackColor = true;
            this.btnRemain.Click += new System.EventHandler(this.btnRemain_Click);
            // 
            // BtnLevelDistribution
            // 
            this.BtnLevelDistribution.Location = new System.Drawing.Point(5, 32);
            this.BtnLevelDistribution.Name = "BtnLevelDistribution";
            this.BtnLevelDistribution.Size = new System.Drawing.Size(179, 23);
            this.BtnLevelDistribution.TabIndex = 2;
            this.BtnLevelDistribution.Text = "活跃用户等级分布";
            this.BtnLevelDistribution.UseVisualStyleBackColor = true;
            this.BtnLevelDistribution.Click += new System.EventHandler(this.BtnLevelDistribution_Click);
            // 
            // BtnStatistics
            // 
            this.BtnStatistics.Location = new System.Drawing.Point(5, 3);
            this.BtnStatistics.Name = "BtnStatistics";
            this.BtnStatistics.Size = new System.Drawing.Size(179, 23);
            this.BtnStatistics.TabIndex = 1;
            this.BtnStatistics.Text = "登陆相关";
            this.BtnStatistics.UseVisualStyleBackColor = true;
            this.BtnStatistics.Click += new System.EventHandler(this.BtnStatistics_Click);
            // 
            // DGVReport
            // 
            this.DGVReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVReport.Location = new System.Drawing.Point(190, 3);
            this.DGVReport.Name = "DGVReport";
            this.DGVReport.RowTemplate.Height = 23;
            this.DGVReport.Size = new System.Drawing.Size(1132, 866);
            this.DGVReport.TabIndex = 0;
            // 
            // StatisticsClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1398, 1037);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "StatisticsClient";
            this.Text = "StatisticsClient";
            this.Load += new System.EventHandler(this.GmClient_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVReport)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox TBPassword;
        private System.Windows.Forms.Label LBPassword;
        private System.Windows.Forms.Label LBAccount;
        private System.Windows.Forms.Label LBServerIp;
        private System.Windows.Forms.TextBox TBAccount;
        private System.Windows.Forms.TextBox TBServerIp;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Button BtnLogout;
        private System.Windows.Forms.TextBox TBOutput;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView DGVReport;
        private System.Windows.Forms.Button BtnStatistics;
        private System.Windows.Forms.Button BtnLevelDistribution;
        private System.Windows.Forms.Button btnRemain;
        private System.Windows.Forms.Button btnDress;
        private System.Windows.Forms.Button btnDressUserGrade;
        private System.Windows.Forms.Button btnPK;
        private System.Windows.Forms.Button btnSaleCount;
        private System.Windows.Forms.Button btnItemIncome;
        private System.Windows.Forms.Button btnScenario;
        private System.Windows.Forms.Button btnSceUserGrade;
        private System.Windows.Forms.Button btnItemUseTimes;
        private System.Windows.Forms.Button btnMallBuyCount;
        private System.Windows.Forms.Button btnMoneyConAPro;
        private System.Windows.Forms.Button btnPkUserGarde;
        private System.Windows.Forms.Button btnPrincessLevel_Grade;
        private System.Windows.Forms.Button btnMaidenLevel_Grade;
        private System.Windows.Forms.Button btnPKMall;
        private System.Windows.Forms.Button btnManufacture;
        private System.Windows.Forms.Button btnManufactureProduct;
        private System.Windows.Forms.Button btn_SevenDayLogin;
        private System.Windows.Forms.Button btn_DaysTask;
        private System.Windows.Forms.Button btn_ManufatureUserGrade;
        private System.Windows.Forms.Button btn_ScoreGradeCount;
        private System.Windows.Forms.Button btn_Money;
        private System.Windows.Forms.Button btnSign;
        private System.Windows.Forms.Button btnOnlineReward;
        private System.Windows.Forms.Button btnItemCollection;
        private System.Windows.Forms.Button btnItemTypeClct;
        private System.Windows.Forms.Button btnNoviceGuide;
        private System.Windows.Forms.Button btnLoseGrade;

    }
}

