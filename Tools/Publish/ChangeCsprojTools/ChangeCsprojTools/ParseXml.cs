﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ChangeCsprojTools
{
    public class ParseXml
    {
        public ParseXml()
        {

        }

        public void ChangeXml(string fileName)
        {
            List<string> addList = new List<string>();
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            XmlElement rootElem = doc.DocumentElement;

            XmlNodeList elementNodes = rootElem.ChildNodes;
            //XmlNode removeNode = null;
            if (fileName.Contains("GameServer.csproj") || fileName.Contains("CommonLib.csproj"))
            {
                List<XmlNode> removeNodeList = new List<XmlNode>();
                foreach (XmlNode elementNode in elementNodes)
                {
                    if (elementNode.Name == "ItemGroup")
                    {
                        XmlNodeList listItemGroup = elementNode.ChildNodes;
                        foreach (XmlNode node in listItemGroup)
                        {
                            if (node.Name == "ProjectReference")
                            {

                                XmlNodeList listProjectReference = node.ChildNodes;
                                foreach (XmlNode nodeProjectReference in listProjectReference)
                                {
                                    if (nodeProjectReference.Name == "Name" && nodeProjectReference.InnerText == "ItemComponent")
                                    {
                                        addList.Add(nodeProjectReference.InnerText);
                                        removeNodeList.Add(node);
                                        //elementNode.RemoveChild(node);
                                    }
                                }
                            }
                        }

                        if (removeNodeList.Count != 0)
                        {
                            foreach (XmlNode node in removeNodeList)
                            {
                                elementNode.RemoveChild(node);
                            }
                            removeNodeList.Clear();
                        }
                    }
                }

            }


            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "ItemGroup")
                {
                    XmlNodeList list = elementNode.ChildNodes;
                    foreach (XmlNode node in list)
                    {
                        if (node.Name == "Reference")
                        {
                            foreach (string addstr in addList)
                            {
                                XmlElement element1 = doc.CreateElement("Reference");
                                XmlAttribute attribute1 = doc.CreateAttribute("Include");
                                attribute1.Value = addstr;
                                element1.Attributes.Append(attribute1);
                                XmlElement element2 = doc.CreateElement("HintPath");
                                element2.InnerText = "..\\..\\Lib\\" + addstr + ".dll";
                                element1.AppendChild(element2);
                                elementNode.AppendChild(element1);
                            }
                            break;
                        }
                    }
                    //break;
                }
                else if (elementNode.Name == "PropertyGroup")
                {
                    XmlNodeList list = elementNode.ChildNodes;
                    foreach (XmlNode node in list)
                    {
                        if (node.Name == "PostBuildEvent")
                        {
                            elementNode.RemoveChild(node);
                        }
                        else if (node.Name == "PreBuildEvent")
                        {
                            if (node.InnerText.Contains("AccountServer"))
                            {
                                node.InnerText = "..\\Tools\\MetaGen.exe ..\\..\\CommonPlatform\\AccountServer\\Config\\MetaConfig.xml";
                            }
                            else if (node.InnerText.Contains("GameServer"))
                            {
                                node.InnerText = "..\\Tools\\MetaGen.exe ..\\..\\CommonPlatform\\GameServer\\Config\\MetaConfig.xml";
                            }

                        }
                    }
                }
            }

            doc.Save(fileName);
            ChangeWords(fileName);
        }

        void ChangeWords(string fileName)
        {
            //FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
            string content = File.ReadAllText(fileName);
            //StreamReader sr = new StreamReader(fs,Encoding.Default);
            //string content = sr.ReadToEnd();
            content = content.Replace(" xmlns=\"\"","");
            File.WriteAllText(fileName, content);

        }
    }
}
