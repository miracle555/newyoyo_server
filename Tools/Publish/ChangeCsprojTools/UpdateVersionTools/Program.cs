﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpdateVersionTools
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.ReadLine();
            string versionFile = args[0];
            string targetPath = args[1];
            UpdateVersion update = new UpdateVersion();
            update.Update(versionFile, targetPath);
        }
    }
}
