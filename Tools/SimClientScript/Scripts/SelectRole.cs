﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/5 9:56:15
* FileName:     Login
* Purpose:      玩家选角脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;

namespace Script
{
    /// <summary>
    /// 登录
    /// </summary>
    public class SelectRole
    {
        public static void Run(Context ctx, string param)
        {
            object account = null;
            ctx.LoadData("account", out account);
            object roleID = null;
            ctx.LoadData("roleID", out roleID);

            LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] select role {0} for user {1}", roleID, (string)account);

            ctx.RegisterMessageHandler(HandleMessage);

            SelectRoleReq req = new SelectRoleReq();
            req.roleID = (UInt64)roleID;

            ctx.SendMessage(req);

            ctx.Wait("SelectRole");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            object account = new object();
            ctx.LoadData("account", out account);
            if (proto.GetID() == ProtoID.CMD_LOGIN_SELECT_ROLE_RSP)
            {
                SelectRoleRsp rsp = (SelectRoleRsp)proto.GetBody();
                if (rsp.errorCode != 0)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[LOGIN] select role fail for user {0}, errCode {1}", (string)account, rsp.errorCode);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] select role success for user {0}", (string)account);
                }

                ctx.Signal("SelectRole");
            }
        }
    }
}
