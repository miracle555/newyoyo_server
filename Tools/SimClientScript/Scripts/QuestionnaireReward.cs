﻿/********************************************************************
	created:	2016/04/28
	created:	28:4:2016   10:50
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\QuestionnaireReward.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts
	file base:	QuestionnaireReward
	file ext:	cs
	author:		黄海威
	
	purpose:	调查问卷测试脚本
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class QuestionnaireReward
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            OpenQuestionnaireReq protoBody = new OpenQuestionnaireReq();
            ctx.Output.WriteLine("[{0}][QuestionnaireReward]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("QuestionnaireReward");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
        }
    }
}
