﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;
using Utility.Common;

namespace Script
{
    public class AttrChange
    {
        public static void Run(Context ctx, string param)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }
        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_PLAYER_ATTR_CLIENT_NTF)
            {
                PlayerAttrClientNtf rsp = (PlayerAttrClientNtf)proto.GetBody();
                rsp.Decode();
                ctx.Output.WriteLine("[PlayerAttrClientNtf] {0}", ObjectDumper.Dump(rsp.AttrsDict, true));
            }
        }
    }
}
