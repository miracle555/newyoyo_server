﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utility.Common;
using Protocols;
using Protocols.Error;

namespace Script
{
    public class GmSendSystemMail
    {
        public static void Run(Context ctx, string param)
        {
            GmSendSystemMailReq req = new GmSendSystemMailReq();
            req.senderName = "system";
            req.title = "压力测试邮件";
            req.contents = "压力测试邮件压力测试邮件压力测试邮件压力测试邮件压力测试邮件压力测试邮件";

            List<MailAttacherResourceBody> resources = new List<MailAttacherResourceBody>();
            MailAttacherResourceBody resource = new MailAttacherResourceBody();
            resource.type = "Stamina";
            resource.amount = 25;
            resources.Add(resource);

            MailAttacherBody attacher = new MailAttacherBody();
            attacher.resources = resources.ToArray();
            req.attacher = attacher;

            req.mailItemType = 100;
            ctx.SendMessage(req);

            ctx.OnScriptComplete(true);
        }
    }
}
