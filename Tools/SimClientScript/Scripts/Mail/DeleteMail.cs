﻿using System.Collections.Generic;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    /// <summary>
    /// 删除邮件
    /// </summary>
    public class DeleteMail
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }


        public static void Run(Context ctx, string param)
        {
            var mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);

            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            // 传入需要删除的邮件Id
            var strAry = param.Split(',');
            var mailIds = new List<long>();
            foreach (var strId in strAry)
                mailIds.Add(long.Parse(strId));

            var mgrId = new ManagerIdentityBody { gameId = 1, zoneId = 1 };
            var req = new DeleteMailReq { managerId = mgrId, mailItemIds = mailIds.ToArray() };

            ctx.SendMessage(req);

            Thread.Sleep(1000);

        }


        static void HandleMessage(Protocol proto, object obj)
        {
            var ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MAIL_DELETEMAIL_RSP)
            {
                var protoBody = (DeleteMailRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                   Thread.CurrentThread.ManagedThreadId,
                   System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                   ObjectDumper.Dump(protoBody, true));

            }

        }


    }
}
