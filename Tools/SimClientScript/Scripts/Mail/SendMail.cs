﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Protocols;
using Script;
using Utility.Common;

namespace Script
{
    /// <summary>
    /// 发送邮件
    /// </summary>
    public class SendMail
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }


        public static void Run(Context ctx, string param)
        {
            var mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            string roleName = (string)role;

            object temp;
            ulong roleId = 0;
            ctx.LoadData("roleID", out temp);
            if (temp != null)
                roleId = ulong.Parse(temp.ToString());

            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            // 参数为收件人Id（角色Id）
            var receiverIds = new List<ulong>();
            bool hasReceivers = false;
            hasReceivers = !string.IsNullOrEmpty(param);
            if (hasReceivers)
            {
                var strAry = param.Split(',');
                foreach (var strId in strAry)
                    receiverIds.Add(ulong.Parse(strId));
            }

            //string title = string.Format("System{0}, broadcast test mail!", roleId);
            //string contents = string.Format("Hello, this is a test broadcast mail which is sent from System{0}", roleId);
            var mgrId = new ManagerIdentityBody { gameId = 1, zoneId = 1 };

            // 先发送广播邮件            
            //var req = new SendMailReq
            //{
            //    managerId = mgrId,
            //    title = title,
            //    contents = contents,
            //    mailItemType = 100,
            //attacher = new MailAttacherBody
            //{
            //    gameItems = new MailAttacherGameItemBody[] 
            //    {
            //        new MailAttacherGameItemBody 
            //        { 
            //            gameItemId = 1, gameItemName = "item1", amount = 10 
            //        },
            //        new MailAttacherGameItemBody 
            //        { 
            //            gameItemId = 2, gameItemName = "item2", amount = 5 
            //        }
            //    },
            //    resources = new MailAttacherResourceBody[] 
            //    {
            //        new MailAttacherResourceBody
            //        {
            //             type="Gold", amount=100
            //        },
            //        new MailAttacherResourceBody
            //        {
            //             type="Diamond", amount=1000
            //        }
            //    }
            //}
            //};

            //ctx.SendMessage(req);

            //再发送普通邮件，若不存在收件人则不发送
            if (hasReceivers)
            {
                string title = string.Format("Role {0}'s test mail!", roleName);
                string contents = string.Format("Hello this is a test mail which is sent from Role {0}", roleName);

                var req = new SendMailReq
                {
                      managerId = mgrId,
                      mailItemType = 1,
                      title = title,
                      contents = contents,
                      receiverIds = receiverIds.ToArray(),
                      attacher = new MailAttacherBody
                      {
                     //gameItems = new MailAttacherGameItemBody[] 
                     // {
                     //     new MailAttacherGameItemBody 
                     //     { 
                     //         gameItemId = 58002, gameItemName = "item1", amount = 10 
                     //     }

                     // },
                         resources = new MailAttacherResourceBody[] 
                         {
                             new MailAttacherResourceBody
                             {
                                  type="Gold", amount=100
                             },
                             new MailAttacherResourceBody
                             {
                                  type="Diamond", amount=1000
                             }
                         }
                      }
                  };

                ctx.SendMessage(req);
            }

            Thread.Sleep(1000);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            var ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MAIL_SENDMAIL_RSP)
            {
                var protoBody = (SendMailRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                   Thread.CurrentThread.ManagedThreadId,
                   System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                   ObjectDumper.Dump(protoBody, true));

            }
        }
    }
}
