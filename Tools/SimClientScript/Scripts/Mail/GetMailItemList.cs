﻿using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    /// <summary>
    /// 获取邮件列表
    /// </summary>
    public class GetMailItemList
    {

        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }


        public static void Run(Context ctx, string param)
        {
            var mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);

            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            var mgrId = new ManagerIdentityBody { gameId = 1, zoneId = 1 };
            var req = new GetRoleMailListReq { managerId = mgrId };
            ctx.SendMessage(req);

            Thread.Sleep(1000);

        }


        static void HandleMessage(Protocol proto, object obj)
        {
            var ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MAIL_GETLIST_RSP)
            {
                var protoBody = (GetRoleMailListRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                   Thread.CurrentThread.ManagedThreadId,
                   System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                   ObjectDumper.Dump(protoBody, true));

            }

        }


    }

}
