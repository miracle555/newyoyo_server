﻿/********************************************************************
	created:	2016/01/23
	created:	23:1:2016   11:20
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Mail\SendSystemMail.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Mail
	file base:	SendSystemMail
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本 - 发送系统邮件
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SendSystemMail
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ulong receiverId = UInt64.Parse(param);
            ManagerIdentityBody managerId = new ManagerIdentityBody { gameId = 1, zoneId = 1 };

            GmSendSystemMailReq protoBody = new GmSendSystemMailReq();
            protoBody.title = "测试邮件";
            protoBody.contents = "这是一封测试邮件，不要太在意!";
            protoBody.mailItemType = (int)MailBodyItemTypes.System;
            protoBody.receiverIds = new ulong[] { receiverId };
            protoBody.attacher = FillMailAttacher();
            protoBody.senderName = "hhw";

            ctx.Output.WriteLine("[{0}][SendSystemMail]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
        }

        private static MailAttacherBody FillMailAttacher()
        {
            MailAttacherBody attacher = new MailAttacherBody();

            MailAttacherGameItemBody item = new MailAttacherGameItemBody();
            item.gameItemId = 67022;
            item.gameItemName = "田园小清新";
            item.amount = 1;
            MailAttacherGameItemBody[] items = new MailAttacherGameItemBody[] { item }; 

            MailAttacherResourceBody rescoure = new MailAttacherResourceBody();
            rescoure.type = "Gold";
            rescoure.amount = 99;
            MailAttacherResourceBody[] rescoures = new MailAttacherResourceBody[] { rescoure };

            attacher.gameItems = items;
            attacher.resources = rescoures;
            return attacher;
        }
    }
}
