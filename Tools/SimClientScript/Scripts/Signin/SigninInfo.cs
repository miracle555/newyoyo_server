﻿/********************************************************************
	created:	2016/09/20
	created:	20:9:2016   13:45
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Signin\SigninInfo.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Signin
	file base:	SigninInfo
	file ext:	cs
	author:		黄海威
	
	purpose:	签到信息
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SigninInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetSignInRewardInfoReq protoBody = new GetSignInRewardInfoReq();
            ctx.Output.WriteLine("[{0}][SigninInfo]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SigninInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SIGNIN_GETREWARDINFO_RSP)
            {
                GetSignInRewardInfoRsp rsp = (GetSignInRewardInfoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SigninInfo]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SigninInfo");
            }
        }
    }
}
