﻿/********************************************************************
	created:	2016/09/20
	created:	20:9:2016   13:49
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Signin\SigninDrawReward.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Signin
	file base:	SigninDrawReward
	file ext:	cs
	author:		黄海威
	
	purpose:	领取签到奖励
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class SigninDrawReward
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            DrawSignInRewardReq protoBody = new DrawSignInRewardReq();
            protoBody.Id = Int32.Parse(param.ToString());
            ctx.Output.WriteLine("[{0}][SigninDrawReward]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SigninDrawReward");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SIGNIN_DRAWREWARD_RSP)
            {
                DrawSignInRewardRsp rsp = (DrawSignInRewardRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SigninDrawReward]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SigninDrawReward");
            }
        }
    }
}
