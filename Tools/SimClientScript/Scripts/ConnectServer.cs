﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/9 14:24:28
* FileName:     ConnectServer
* Purpose:      连接服务器的脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Script
{
    /// <summary>
    /// 连接Server
    /// </summary>
    public class ConnectServer
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            while (true)
            {
                try
                {
                    ctx.Connect();
                    ctx.OnScriptComplete(true);
                    break;
                }
                catch (Exception)
                {
                    Thread.Sleep(200);
                }
            }
        }
    }
}
