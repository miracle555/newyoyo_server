﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utility.Common;

namespace Script
{
    public class ChangeLevel
    {
        public static void Run(Context ctx, string param)
        {

            ctx.RegisterMessageHandler(HandleMessage);
            ctx.Output.WriteLine("[{0}][ChangeLevel] exp = {1}",
               Thread.CurrentThread.ManagedThreadId, param);

            int exp = Int32.Parse(param);
            GmCommandReq req = new GmCommandReq();
            req.cmd = string.Format("AddExp {0}", exp);
            ctx.SendMessage(req);
            ctx.Wait("ChangeLevel");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GM_COMMAND_RSP)
            {
                GmCommandRsp rsp = (GmCommandRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][ChangeLevel]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("ChangeLevel");
            }
        }
    }
}
