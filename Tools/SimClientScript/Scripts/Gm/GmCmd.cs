﻿/* ============================================================
* Author:       xieqin
* Time:         2015/6/18 15:17:20
* FileName:     GmCmd
* Purpose:      gm命令脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class GmCmd
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GmCommandReq req = new GmCommandReq();
            req.cmd = param;
            ctx.Output.WriteLine("[{0}][GmCmd]cmd = {1}",
                Thread.CurrentThread.ManagedThreadId, param);
            ctx.SendMessage(req);
            ctx.Wait("GmCmd");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GM_COMMAND_RSP)
            {
                GmCommandRsp protoBody = (GmCommandRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][GmCmd]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(protoBody, true));

                ctx.Signal("GmCmd");
            }
        }
    }
}
