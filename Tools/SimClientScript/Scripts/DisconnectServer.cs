﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/9 14:26:30
* FileName:     DisconnectServer
* Purpose:      和服务器断开连接的脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Script
{
    /// <summary>
    /// 断开连接
    /// </summary>
    public class DisconnectServer
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.Disconnect();
            ctx.OnScriptComplete(true);
        }
    }
}
