﻿/* ============================================================
* Author:       xieqin
* Time:         2015/3/25 11:42:04
* FileName:     Competition
* Purpose:      pk
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;
using Protocols.Error;

namespace Script
{
    public class Competition
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            SelectOppoReq req = new SelectOppoReq();
            ctx.SendMessage(req);
            ctx.Wait("Competition");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_COMPETITION_SELECTOPPO_RSP)
            {
                SelectOppoRsp rsp = (SelectOppoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectOppoRsp]",
                    Thread.CurrentThread.ManagedThreadId);

                if (rsp.error == ErrorID.Success)
                {
                    StartPkReq req = new StartPkReq();
                    req.equips.Add(68066);
                    req.equips.Add(67066);
                    req.equips.Add(72066);
                    ctx.SendMessage(req);
                }
                else
                {
                    ctx.OnScriptComplete(true);
                    ctx.Signal("Competition");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_COMPETITION_STARTPK_RSP)
            {
                StartPkRsp rsp = (StartPkRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][StartPkRsp]",
                    Thread.CurrentThread.ManagedThreadId);

                if (rsp.error == ErrorID.Success)
                {
                    EndPkReq req = new EndPkReq();
                    ctx.SendMessage(req);
                }
                else
                {
                    ctx.OnScriptComplete(true);
                    ctx.Signal("Competition");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_COMPETITION_ENDPK_RSP)
            {
                EndPkRsp rsp = (EndPkRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][EndPkRsp]",
                    Thread.CurrentThread.ManagedThreadId);

                ctx.OnScriptComplete(true);
                ctx.Signal("Competition");
            }
        }
    }
}
