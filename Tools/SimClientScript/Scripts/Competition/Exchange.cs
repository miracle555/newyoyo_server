﻿/* ============================================================
* Author:       xieqin
* Time:         2015/6/3 11:45:08
* FileName:     Exchange
* Purpose:      pk商品兑换
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class Exchange
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            CompetitionExchangeReq protoBody = new CompetitionExchangeReq();
            var strAry = param.Split(',');
            Dictionary<int, int> goods = new Dictionary<int, int>();
            protoBody.goods = goods;

            ctx.Output.WriteLine("[{0}][Exchange]",
                Thread.CurrentThread.ManagedThreadId);

            ctx.SendMessage(protoBody);
            ctx.Wait("Exchange");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_COMPETITION_EXCHANGE_RSP)
            {
                CompetitionExchangeRsp protoBody = (CompetitionExchangeRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][Exchange]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(protoBody, true));
                ctx.Signal("Exchange");
            }
        }
    }
}
