﻿/* ============================================================
* Author:       xieqin
* Time:         2015/6/3 11:42:04
* FileName:     CompetitionMallList
* Purpose:      pk商城显示
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class CompetitionMallList
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            CompetitionMallListReq protoBody = new CompetitionMallListReq();
            ctx.Output.WriteLine("[{0}][CompetitionMallList]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("CompetitionMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_COMPETITION_MALLLIST_RSP)
            {
                CompetitionMallListRsp protoBody = (CompetitionMallListRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][CompetitionMallList]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(protoBody, true));
                ctx.OnScriptComplete(true);
                ctx.Signal("CompetitionMallList");
            }
        }
    }
}
