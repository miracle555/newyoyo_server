﻿/* ============================================================
* Author:       xieqin
* Time:         2015/6/2 16:22:25
* FileName:     ChangeName
* Purpose:      改名脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Protocols.Error;
using Utility.Common;

namespace Script
{
    public class ChangeName
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ChangeNameReq protoBody = new ChangeNameReq();
            protoBody.name = param;
            ctx.Output.WriteLine("[{0}][ChangeName]name = {1}",
                Thread.CurrentThread.ManagedThreadId, param);
            ctx.SendMessage(protoBody);
            ctx.Wait("ChangeName");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_CHANGE_NAME_RSP)
            {
                ChangeNameRsp rsp = (ChangeNameRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][ChangeName]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("ChangeName");
            }
        }
    }
}
