﻿/********************************************************************
	created:	2015/12/24
	created:	24:12:2015   14:42
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\RandomEquips.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts
	file base:	RandomEquips
	file ext:	cs
	author:		黄海威
	
	purpose:    从item表里产生玩家随机公告
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ItemComponent;
using StaticData;
using Log;

namespace Script
{
    public class RandomEquips
    {

        public static List<int> GetRandomEquips(string dataName)
        {
            List<int> itemIds = new List<int>();
            string ProcessName = Process.GetCurrentProcess().MainModule.FileName;
            string PathOfEXE = Path.GetDirectoryName(ProcessName);
            string dataPath = @"\..\..\..\..\Client\Datas\";
            string path =string.Format("{0}{1}{2}",PathOfEXE,dataPath,dataName);
            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] itemComponents = line.Split('|');
                    if (itemComponents.Length > 0)
                    {
                        int id = 0;
                        if (int.TryParse(itemComponents[0], out id))
                        {
                            itemIds.Add(id);
                        }
                    }
                    line = sr.ReadLine();
                }
            }

            return GetRandomEquipsStep(itemIds);
        }

        public static List<int> GetRandomEquipsStep(List<int> ids)
        {
            int lastItemType = -1;
            List<int> equips = new List<int>();
            List<int> types = new List<int>();

            Random rd = new Random();
            while (equips.Count < 3)
            {
                int index = rd.Next(0, ids.Count);
                int itemId = ids[index];
                ItemStaticData itemData = StaticDataManager.GetItemStaticDataById(itemId);
                int curType = itemData.Type;

                if (lastItemType == curType)
                {
                    continue;
                }
                else
                {
                    lastItemType = curType;
                }

                if (IsMutual(types, curType))
                {
                    continue;
                }

                equips.Add(itemId);
                types.Add(curType);
            }

            return equips;
        }

        private static bool IsMutual(List<int> types, int curType)
        {
            bool isMutual = false;
            int dress = (int)OtomeItemType.Dress;
            int coat = (int)OtomeItemType.Coat;
            int pants = (int)OtomeItemType.Pants;

            if (dress == curType)
            {
                if (types.Contains(coat)||types.Contains(pants))
                {
                    isMutual=true;
                }
            }
            else if (coat ==curType||pants==curType)
            {
                if (types.Contains(dress))
                {
                    isMutual = true;
                }
            }

            if (types.Contains(curType))
            {
                isMutual = true;
            }

            return isMutual;
        }

        public static int GetRandomFlowerId()
        {
            List<int> flowerIds = new List<int>();
            int flowerId = 0;
            string ProcessName = Process.GetCurrentProcess().MainModule.FileName;
            string PathOfEXE = Path.GetDirectoryName(ProcessName);
            string path = PathOfEXE + @"\..\..\..\..\Client\Datas\SelectionCompetitionFlowerData.csv";
            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] flowerComponents = line.Split('|');
                    if (flowerComponents.Length > 0)
                    {
                        int id = 0;
                        if (int.TryParse(flowerComponents[0], out id))
                        {
                            flowerIds.Add(id);
                        }
                    }
                    line = sr.ReadLine();
                }
            }

            Random rd = new Random();
            int index= rd.Next(0, flowerIds.Count);
            flowerId = flowerIds[index];
            return flowerId;
        }
    }
}
