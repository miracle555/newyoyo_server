﻿/********************************************************************
	created:	2014/12/15
	created:	15:12:2014   15:01
	filename: 	d:\project\commonplatform\server\tools\simclientscript\scripts\playerrefuseinvitedjoinscene.cs
	file path:	d:\project\commonplatform\server\tools\simclientscript\scripts
	file base:	playerrefuseinvitedjoinscene
	file ext:	cs
	author:		范良元
	
	purpose:	拒绝邀请加入场景
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;

namespace Script
{
    public class PlayerRefuseInvitedJoinScene
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_INVITE_PLAYER_JOINSCENE_REQ_NTF)
            {
                PlayerInvitePlayerJoinSceneReqNtf msg = (PlayerInvitePlayerJoinSceneReqNtf)proto.GetBody();
                logText = string.Format("{0} recv InvitePlayerJoinSceneReqNtf inviterPlayerId = {1}, inviterPlayerName = {2}, targetSceneId = {3}, targetSceneName = {4}",
                    (string)role, msg.inviterPlayerId, msg.inviterPlayerName, msg.targetSceneId, msg.targetSceneName);

                if (logText != string.Empty)
                {
                    ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                    logText = string.Empty;
                }

                PlayerIsInvitedJoinSceneReqNtfRsp responseMsg = new PlayerIsInvitedJoinSceneReqNtfRsp();
                responseMsg.inviterPlayerId = msg.inviterPlayerId;
                responseMsg.sceneId = msg.targetSceneId;
                responseMsg.agreed = false;
                ctx.SendMessage(responseMsg);
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
