﻿/********************************************************************
	created:	2014/12/15
	created:	15:12:2014   12:03
	filename: 	d:\project\commonplatform\server\tools\simclientscript\scripts\scenehostrefuseplayerjoin.cs
	file path:	d:\project\commonplatform\server\tools\simclientscript\scripts
	file base:	scenehostrefuseplayerjoin
	file ext:	cs
	author:		范良元
	
	purpose:	拒绝玩家的加入请求
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;

namespace Script
{
    public class SceneHostRefusePlayerJoin
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_JOINSCENE_REQ_NTF)
            {
                PlayerJoinSceneReqNtf msg = proto.GetBody() as PlayerJoinSceneReqNtf;
                logText = string.Format("{0} recv JoinSceneReqNtf requesterId = {1}, requesterName = {2}", (string)role, msg.requesterId, msg.requesterName);

                if (logText != string.Empty)
                {
                    ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                    logText = string.Empty;
                }

                PlayerJoinSceneReqNtfRsp responseMsg = new PlayerJoinSceneReqNtfRsp();
                responseMsg.agreed = false;
                responseMsg.requesterId = (UInt64)msg.requesterId;
                ctx.SendMessage(responseMsg);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_AGREE_JOINSCENE_REQ_FAIL_RSP)
            {
                PlayerAgreeJoinSceneReqFailRsp msg = (PlayerAgreeJoinSceneReqFailRsp)proto.GetBody();
                Protocols.Error.ErrorID errid = (Protocols.Error.ErrorID)msg.errorCode;
                logText = string.Format("{0} recv AgreeJoinSceneReqFailRsp responserId = {1}, responserName = {2}, errorCode = {3}", (string)role, msg.responserId, msg.responserName, errid.ToString());
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
