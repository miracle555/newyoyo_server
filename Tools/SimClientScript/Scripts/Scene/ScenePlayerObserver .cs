﻿/********************************************************************
	created:	2014/12/15
	created:	15:12:2014   15:34
	filename: 	d:\project\commonplatform\server\tools\simclientscript\scripts\sceneguestobserver .cs
	file path:	d:\project\commonplatform\server\tools\simclientscript\scripts
	file base:	sceneguestobserver 
	file ext:	cs
	author:		范良元
	
	purpose:	房客观察者，用于观察房间内玩家行为
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class ScenePlayerObserver
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_PLAYER_ISKICKED_INSCENE_NTF)
            {
                PlayerIsKickedInSceneNtf msg = (PlayerIsKickedInSceneNtf)proto.GetBody();
                logText = ObjectDumper.Dump(msg, true);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_PLAYER_CHANGE_STATE_IN_SCENE_NTF)
            {
                PlayerChangeStateInSceneNtf msg = (PlayerChangeStateInSceneNtf)proto.GetBody();
                logText = ObjectDumper.Dump(msg, true);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_SCENEBRIEFINFO_LIST_RSP)
            {
                SceneBriefInfoListRsp msg = (SceneBriefInfoListRsp)proto.GetBody();
                logText = ObjectDumper.Dump(msg, true);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_SCENEDETAILINFO_NTF)
            {
                SceneDetailInfoNtf msg = (SceneDetailInfoNtf)proto.GetBody();
                logText = ObjectDumper.Dump(msg, true);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_PLAYER_JOINSCENE_NTF)
            {
                PlayerJoinSceneNtf msg = (PlayerJoinSceneNtf)proto.GetBody();
                logText = ObjectDumper.Dump(msg, true);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_PLAYER_LEAVESCENE_NTF)
            {
                PlayerLeaveSceneNtf msg = (PlayerLeaveSceneNtf)proto.GetBody();
                logText = string.Format("{0} recv PlayerLeaveSceneNtf sceneId = {1}, leavePlayerId = {2}", (string)role, msg.sceneId, msg.leavePlayerId);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_DESTROIED_NTF)
            {
                SceneDestroiedNtf msg = (SceneDestroiedNtf)proto.GetBody();
                logText = string.Format("{0} recv SceneDestroiedNtf sceneId = {1}", (string)role, msg.sceneId);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_PLAYER_CHANGE_STATE_IN_SCENE_NTF)
            {
                PlayerChangeStateInSceneNtf msg = (PlayerChangeStateInSceneNtf)proto.GetBody();
                logText = ObjectDumper.Dump(msg, true);
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
