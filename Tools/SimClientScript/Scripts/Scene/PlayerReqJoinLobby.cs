﻿/********************************************************************
	created:	2014/12/15
	created:	15:12:2014   16:00
	filename: 	D:\Project\CommonPlatform\Server\Tools\SimClientScript\Scripts\PlayerReqJoinLobby.cs
	file path:	D:\Project\CommonPlatform\Server\Tools\SimClientScript\Scripts
	file base:	PlayerReqJoinLobby
	file ext:	cs
	author:		范良元
	
	purpose:	请求加入大厅
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class PlayerReqJoinLobby
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);

            PlayerJoinSceneReq joinScene = new PlayerJoinSceneReq();
            joinScene.sceneId = 1;
            joinScene.password = "pass1";
            ctx.SendMessage(joinScene);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_JOINSCENE_REQ_FAIL_RSP)
            {
                PlayerJoinSceneReqFailRsp msg = (PlayerJoinSceneReqFailRsp)proto.GetBody();
                Protocols.Error.ErrorID errid = (Protocols.Error.ErrorID)msg.errorCode;
                logText = ObjectDumper.Dump(msg, true);
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
