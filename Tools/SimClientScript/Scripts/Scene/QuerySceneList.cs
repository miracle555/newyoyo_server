﻿/********************************************************************
	created:	2014/12/17
	created:	17:12:2014   18:20
	filename: 	D:\Project\CommonPlatform\Server\Tools\SimClientScript\Scripts\QuerySceneList.cs
	file path:	D:\Project\CommonPlatform\Server\Tools\SimClientScript\Scripts
	file base:	QuerySceneList
	file ext:	cs
	author:		范良元
	
	purpose:	获取场景列表
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;

namespace Script
{
    public class QuerySceneList
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);

            Thread.Sleep(3000);

            SceneBriefInfoListReq msg = new SceneBriefInfoListReq();
            ctx.SendMessage(msg);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_SCENEBRIEFINFO_LIST_RSP)
            {
                SceneBriefInfoListRsp msg = proto.GetBody() as SceneBriefInfoListRsp;
                logText = string.Format("{0} recv SceneBriefInfoListRsp listsize = {1}", (string)role, msg.listSceneInfo.Count);
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
