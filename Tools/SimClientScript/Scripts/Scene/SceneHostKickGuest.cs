﻿/********************************************************************
	created:	2014/12/15
	created:	15:12:2014   14:17
	filename: 	d:\project\commonplatform\server\tools\simclientscript\scripts\scenehostkickguest.cs
	file path:	d:\project\commonplatform\server\tools\simclientscript\scripts
	file base:	scenehostkickguest
	file ext:	cs
	author:		范良元
	
	purpose:	房主踢出房客
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;

namespace Script
{
    public class SceneHostKickGuest
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_PLAYER_JOINSCENE_NTF)
            {
                PlayerJoinSceneNtf msg = proto.GetBody() as PlayerJoinSceneNtf;

                if (msg.sceneId != 1 && !msg.playerInfo.statusList.Contains(1))
                {
                    PlayerKickPlayerInSceneReq kickMsg = new PlayerKickPlayerInSceneReq();
                    kickMsg.kickedPlayerId = msg.playerInfo.playerId;
                    ctx.SendMessage(kickMsg);
                }
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_KICKPLAYER_INSCENE_FAIL_RSP)
            {
                PlayerKickPlayerInSceneFailRsp msg = proto.GetBody() as PlayerKickPlayerInSceneFailRsp;
                Protocols.Error.ErrorID errid = (Protocols.Error.ErrorID)msg.errorCode;
                logText = string.Format("{0} recv KickPlayerInSceneFailRsp kickedPlayerId = {1}, errorCode = {2}", (string)role, msg.kickedPlayerId, errid.ToString());
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
