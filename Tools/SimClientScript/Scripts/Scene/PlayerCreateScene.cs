﻿/********************************************************************
	created:	2014/12/11
	created:	11:12:2014   16:47
	filename: 	D:\Project\CommonPlatform\Server\Tools\SimClientScript\Scripts\PlayerCreateScene.cs
	file path:	D:\Project\CommonPlatform\Server\Tools\SimClientScript\Scripts
	file base:	PlayerCreateScene
	file ext:	cs
	author:		范良元
	
	purpose:	玩家创建场景脚本
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;

namespace Script
{
    public class PlayerCreateScene
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);

            PlayerCreateSceneReq createScene = new PlayerCreateSceneReq();
            createScene.sceneType = 2;
            createScene.sceneName = "testscene1";
            createScene.password = "pass1";
            ctx.SendMessage(createScene);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_SCENEBRIEFINFO_LIST_RSP)
            {
                SceneBriefInfoListRsp msg = (SceneBriefInfoListRsp)proto.GetBody();
                logText = string.Format("{0} recv SceneBriefInfoListRsp listSceneInfo.Count = {1}", (string)role, msg.listSceneInfo.Count);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_CREATESCENE_RSP)
            {
                PlayerCreateSceneRsp msg = (PlayerCreateSceneRsp)proto.GetBody();
                Protocols.Error.ErrorID errid = (Protocols.Error.ErrorID)msg.errorCode;
                logText = string.Format("{0} recv CreateSceneRsp sceneId = {1}, errorCode = {2}", (string)role, msg.sceneId, errid.ToString());
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
