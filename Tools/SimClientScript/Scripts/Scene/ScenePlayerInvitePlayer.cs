﻿/********************************************************************
	created:	2014/12/15
	created:	15:12:2014   14:21
	filename: 	d:\project\commonplatform\server\tools\simclientscript\scripts\sceneplayerinviteplayer.cs
	file path:	d:\project\commonplatform\server\tools\simclientscript\scripts
	file base:	sceneplayerinviteplayer
	file ext:	cs
	author:		范良元
	
	purpose:	邀请玩家加入场景
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;

namespace Script
{
    public class ScenePlayerInvitePlayer
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);

            Thread.Sleep(3000);

            PlayerListOutOfSceneReq msg = new PlayerListOutOfSceneReq();
            msg.range = QueryPlayerRange.All;
            msg.page = 1;
            ctx.SendMessage(msg);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_PLAYERLIST_OUTOFSCENE_RSP)
            {
                PlayerListOutOfSceneRsp msg = (PlayerListOutOfSceneRsp)proto.GetBody();
                logText = string.Format("{0} recv PlayerListOutOfSceneRsp range = {1}, page = {2}, listPlayerInfoCount = {3}",
                    (string)role, msg.range, msg.page, msg.listPlayerInfo.Count);

                if (logText != string.Empty)
                {
                    ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                    logText = string.Empty;
                }

                if (msg.listPlayerInfo.Count > 0)
                {
                    PlayerInvitePlayerJoinSceneReq inviteMsg = new PlayerInvitePlayerJoinSceneReq();
                    inviteMsg.inviteePlayerId = msg.listPlayerInfo[0].playerId;
                    ctx.SendMessage(inviteMsg);
                }
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_INVITE_PLAYER_JOINSCENE_FAIL_RSP)
            {
                PlayerInvitePlayerJoinSceneFailRsp msg = (PlayerInvitePlayerJoinSceneFailRsp)proto.GetBody();
                Protocols.Error.ErrorID errid = (Protocols.Error.ErrorID)msg.errorCode;
                logText = string.Format("{0} recv PlayerListOutOfSceneRsp inviteePlayerId = {1}, errorCode = {2}",
                    (string)role, msg.inviteePlayerId, errid.ToString());
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_REFUSE_INVITED_JOINSCENE_REQ_RSP)
            {
                PlayerRefuseInvitedJoinSceneReqRsp msg = (PlayerRefuseInvitedJoinSceneReqRsp)proto.GetBody();
                logText = string.Format("{0} recv RefuseInvitedJoinSceneReqRsp inviteePlayerId = {1}, sceneId = {2}",
                    (string)role, msg.inviteePlayerId, msg.sceneId);
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
