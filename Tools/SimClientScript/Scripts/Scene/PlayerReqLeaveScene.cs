﻿/********************************************************************
	created:	2014/12/15
	created:	15:12:2014   14:18
	filename: 	d:\project\commonplatform\server\tools\simclientscript\scripts\playerreqleavescene.cs
	file path:	d:\project\commonplatform\server\tools\simclientscript\scripts
	file base:	playerreqleavescene
	file ext:	cs
	author:		范良元
	
	purpose:	玩家请求离开场景
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;

namespace Script
{
    public class PlayerReqLeaveScene
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);

            Thread.Sleep(3000);

            PlayerLeaveSceneReq msg = new PlayerLeaveSceneReq();
            ctx.SendMessage(msg);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
        }
    }
}
