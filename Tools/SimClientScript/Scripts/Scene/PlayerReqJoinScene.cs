﻿/********************************************************************
	created:	2014/12/12
	created:	12:12:2014   17:06
	filename: 	d:\project\commonplatform\server\tools\simclientscript\scripts\playerreqjoinscene.cs
	file path:	d:\project\commonplatform\server\tools\simclientscript\scripts
	file base:	playerreqjoinscene
	file ext:	cs
	author:		范良元
	
	purpose:	请求加入场景
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class PlayerReqJoinScene
    {
        /// <summary>
        /// 防止玩家从场景退出到大厅时再次发送加入场景请求消息导致死循环
        /// </summary>
        public static bool m_reqJoined = false;

        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            string logText = string.Empty;
            object role = new object();
            ctx.LoadData("role", out role);
            if (proto.GetID() == ProtoID.CMD_SCENE_SCENEBRIEFINFO_LIST_RSP || proto.GetID() == ProtoID.CMD_SCENE_SCENEBRIEFINFO_NTF)
            {
                int sceneId = 0;
                if (proto.GetID() == ProtoID.CMD_SCENE_SCENEBRIEFINFO_LIST_RSP)
                {
                    SceneBriefInfoListRsp msg = (SceneBriefInfoListRsp)proto.GetBody();
                    logText = string.Format("{0} recv SceneBriefInfoListRsp listSceneInfo.Count = {1}", (string)role, msg.listSceneInfo.Count);
                    if (msg.listSceneInfo.Count > 0)
                    {
                        sceneId = msg.listSceneInfo[0].sceneId;
                    }
                }
                else if (proto.GetID() == ProtoID.CMD_SCENE_SCENEBRIEFINFO_NTF)
                {
                    SceneBriefInfoNtf msg = (SceneBriefInfoNtf)proto.GetBody();
                    logText = string.Format("{0} recv SceneBriefInfoNtf sceneId = {1}, sceneName = {2}", (string)role, msg.sceneId, msg.sceneName);
                    sceneId = msg.sceneId;
                }

                if (logText != string.Empty)
                {
                    ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                    logText = string.Empty;
                }

                if (!m_reqJoined && sceneId > 0)
                {
                    PlayerJoinSceneReq joinScene = new PlayerJoinSceneReq();
                    joinScene.sceneId = sceneId;
                    joinScene.password = "pass1";
                    ctx.SendMessage(joinScene);

                    m_reqJoined = true;
                }
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_JOINSCENE_REQ_FAIL_RSP)
            {
                PlayerJoinSceneReqFailRsp msg = (PlayerJoinSceneReqFailRsp)proto.GetBody();
                Protocols.Error.ErrorID errid = (Protocols.Error.ErrorID)msg.errorCode;
                logText = ObjectDumper.Dump(msg, true);
            }
            else if (proto.GetID() == ProtoID.CMD_SCENE_REFUSE_JOINSCENE_REQ_RSP)
            {
                PlayerRefuseJoinSceneReqRsp msg = (PlayerRefuseJoinSceneReqRsp)proto.GetBody();
                logText = ObjectDumper.Dump(msg, true);
            }

            if (logText != string.Empty)
            {
                ctx.Output.WriteLine("[{0}][{1}]{2}", Thread.CurrentThread.ManagedThreadId, System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name, logText);
                logText = string.Empty;
            }
        }
    }
}
