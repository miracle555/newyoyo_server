﻿/********************************************************************
	created:	2015/12/17
	created:	17:12:2015   10:02
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionInfo.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionInfo
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-评选赛系统信息
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;
using Protocols.Error;
using Log;

namespace Script
{
    public class SelectionCompetitionInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetSelectionNecessaryInfoReq protoBody = new GetSelectionNecessaryInfoReq();
            ctx.Output.WriteLine("[{0}][SelectionCompetitionInfo]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_INFO_RSP)
            {
                GetSelectionNecessaryInfoRsp rsp = (GetSelectionNecessaryInfoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionInfo]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionInfo");
            }
        }
    }
}
