﻿/********************************************************************
	created:	2015/12/18
	created:	18:12:2015   9:41
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionGetThemeRank.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionGetThemeRank
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本 评选赛系统-主题排行榜
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionGetThemeRank
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetThemeLeaderboardReq protoBody = new GetThemeLeaderboardReq();
            protoBody.Index = Int32.Parse(param);
            ctx.Output.WriteLine("[{0}][SelectionCompetitionGetThemeRank]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionGetThemeRank");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_THEMELEADERBOARD_RSP)
            {
                GetThemeLeaderboardRsp rsp = (GetThemeLeaderboardRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionGetThemeRank]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionGetThemeRank");
            }
        }
    }
}
