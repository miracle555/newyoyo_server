﻿/********************************************************************
	created:	2015/12/17
	created:	17:12:2015   19:37
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionGetEquipsScore.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionGetEquipsScore
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-评选赛系统-评分
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionGetEquipsScore
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            List<int> equips = new List<int>();
            equips.Add(67003);
            equips.Add(68061);
            equips.Add(72060);

            GetEquipsScoreReq protoBody = new GetEquipsScoreReq();
            protoBody.Equips = equips;
            ctx.Output.WriteLine("[{0}][SelectionCompetitionGetEquipsScore]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionGetEquipsScore");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_GETEQUIPSCORE_RSP)
            {
                GetEquipsScoreRsp rsp = (GetEquipsScoreRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionGetEquipsScore]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionGetEquipsScore");
            }
        }
    }
}
