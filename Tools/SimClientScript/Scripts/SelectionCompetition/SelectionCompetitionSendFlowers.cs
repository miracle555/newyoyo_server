﻿/********************************************************************
	created:	2015/12/04
	created:	4:12:2015   9:57
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionSendFlowers.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionSendFlowers
	file ext:	cs
	author:		黄海威
	
	purpose:	评选赛系统 送花测试脚本
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionSendFlowers
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage); 
        }

        public static void Run(Context ctx, string param)
        {
            SendFlowerReq protoBody = new SendFlowerReq();
            protoBody.RoleId = UInt64.Parse(param);
            protoBody.FlowerId = RandomEquips.GetRandomFlowerId(); 
            protoBody.Count = 2;
            ctx.Output.WriteLine("[{0}][SelectionCompetitionSendFlowers]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionPraise");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_SENDFLOWER_RSP)
            {
                SendFlowerRsp rsp = (SendFlowerRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionSendFlowers]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionPraise");
            }
        }
    }
}
