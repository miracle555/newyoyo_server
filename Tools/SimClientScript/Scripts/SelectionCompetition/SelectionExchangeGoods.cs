﻿/********************************************************************
	created:	2015/12/21
	created:	21:12:2015   16:36
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionExchangeGoods.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionExchangeGoods
	file ext:	cs
	author:		黄海威
	
	purpose:	评选赛兑换商城
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionExchangeGoods
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            SelectionExchangeGoodsReq protoBody = new SelectionExchangeGoodsReq();
            protoBody.GoodsId = 3071;
            protoBody.Count = 1;
            ctx.Output.WriteLine("[{0}][SelectionExchangeGoods]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionExchangeGoods");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_EXCHANGEGOODS_RSP)
            {
                SelectionExchangeGoodsRsp rsp = (SelectionExchangeGoodsRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionExchangeGoods]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionExchangeGoods");
            }
        }
    }
}
