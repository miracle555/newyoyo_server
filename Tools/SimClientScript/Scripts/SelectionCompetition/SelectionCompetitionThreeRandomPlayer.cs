﻿/********************************************************************
	created:	2015/12/18
	created:	18:12:2015   14:48
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionThreeRandomPlayer.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionThreeRandomPlayer
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本 评选赛系统-随机玩家
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionThreeRandomPlayer
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetThreeRandomPlayerReq protoBody = new GetThreeRandomPlayerReq();
            ctx.Output.WriteLine("[{0}][SelectionCompetitionThreeRandomPlayer]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionThreeRandomPlayer");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_RANDOMTHREEPLAYER_RSP)
            {
                GetThreeRandomPlayerRsp rsp = (GetThreeRandomPlayerRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionThreeRandomPlayer]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionThreeRandomPlayer");
            }
        }
    }
}
