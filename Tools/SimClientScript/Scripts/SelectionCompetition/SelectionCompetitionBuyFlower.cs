﻿/********************************************************************
	created:	2015/12/25
	created:	25:12:2015   16:05
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionBuyFlower.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionBuyFlower
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本 买花
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionBuyFlower
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            BuyFlowerReq protoBody = new BuyFlowerReq();
            protoBody.FlowerId = Int32.Parse(param);
            protoBody.Count = 99;
            ctx.Output.WriteLine("[{0}][SelectionCompetitionPraise]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionPraise");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_BUYFLOWER_RSP)
            {
                BuyFlowerRsp rsp = (BuyFlowerRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionPraise]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.OnScriptComplete(true);
                ctx.Signal("SelectionCompetitionPraise");
            }
        }
    }
}
