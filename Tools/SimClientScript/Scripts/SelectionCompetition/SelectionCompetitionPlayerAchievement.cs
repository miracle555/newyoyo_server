﻿/********************************************************************
	created:	2015/12/17
	created:	17:12:2015   11:41
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionPlayerAchievement.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionPlayerAchievement
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-评选赛系统-玩家成绩
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionPlayerAchievement
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetSelectionAchievementReq protoBody = new GetSelectionAchievementReq();
            ctx.Output.WriteLine("[{0}][SelectionCompetitionPlayerAchievement]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionPlayerAchievement");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_GETACHIEVEMENT_RSP)
            {
                GetSelectionAchievementRsp rsp = (GetSelectionAchievementRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionPlayerAchievement]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionPlayerAchievement");
            }
        }
    }
}
