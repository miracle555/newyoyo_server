﻿/********************************************************************
	created:	2015/12/17
	created:	17:12:2015   10:52
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionSearchPlayer.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionSearchPlayer
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-评先赛系统 搜索玩家
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionSearchPlayer
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            SearchPlayerInSelelectionReq protoBody = new SearchPlayerInSelelectionReq();
            protoBody.RoleId = UInt64.Parse(param);
            ctx.Output.WriteLine("[{0}][SelectionCompetitionSearchPlayer]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionSearchPlayer");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_SEARCHPLAYER_RSP)
            {
                SearchPlayerInSelelectionRsp rsp = (SearchPlayerInSelelectionRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionSearchPlayer]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionSearchPlayer");
            }
        }
    }
}
