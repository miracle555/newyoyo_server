﻿/********************************************************************
	created:	2015/12/18
	created:	18:12:2015   9:58
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionGetSeasonRank.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionGetSeasonRank
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-评选赛系统-赛季排名
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionGetSeasonRank
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetSeasonLeaderboardReq protoBody = new GetSeasonLeaderboardReq();
            protoBody.Index = 0;
            ctx.Output.WriteLine("[{0}][SelectionCompetitionGetSeasonRank]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionGetSeasonRank");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_SEASONLEADERBOARD_RSP)
            {
                GetSeasonLeaderboardRsp rsp = (GetSeasonLeaderboardRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionGetSeasonRank]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionGetSeasonRank");
            }
        }
    }
}
