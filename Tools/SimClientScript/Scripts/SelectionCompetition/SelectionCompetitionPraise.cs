﻿/********************************************************************
	created:	2015/12/04
	created:	4:12:2015   9:50
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition\SelectionCompetitionPraise.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SelectionCompetition
	file base:	SelectionCompetitionPraise
	file ext:	cs
	author:		黄海威
	
	purpose:	点赞测试脚本
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocols;
using Script;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectionCompetitionPraise
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GivePraiseReq protoBody = new GivePraiseReq();
            protoBody.RoleId = UInt64.Parse(param);
            ctx.Output.WriteLine("[{0}][SelectionCompetitionPraise]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionPraise");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_GIVEPRAISE_RSP)
            {
                GivePraiseRsp rsp = (GivePraiseRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][SelectionCompetitionPraise]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("SelectionCompetitionPraise");
            }
        }
    }
}
