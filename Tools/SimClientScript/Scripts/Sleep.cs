﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/26 15:33:14
* FileName:     Sleep
* Purpose:      sleep
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Script
{
    public class Sleep
    {
        public static void Run(Context ctx, string param)
        {
            int interval = int.Parse(param);
            Thread.Sleep(interval);
            ctx.OnScriptComplete(true);
        }
    }
}
