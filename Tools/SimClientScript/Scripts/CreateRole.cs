﻿/* ============================================================
* Author:       shiyaoli
* Time:         2015/01/07 9:56:15
* FileName:     CreateRole
* Purpose:      创建角色脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;

namespace Script
{
    /// <summary>
    /// 创建角色,只有roleID为空或者为0的情况下才创建新角色 
    /// </summary>
    public class CreateRole
    {
        public static void Run(Context ctx, string param)
        {
            object obj = null;
            ctx.LoadData("roleID", out obj);

            ///只有roleID为空或者为0的情况下才创建新角色 
            if (null == obj )
            {
                object account = null;
                ctx.LoadData("account", out account);

                string roleName =  account + "_Role_" + new Random().Next(100000).ToString();

                LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] req to create role {0} for account {1}", roleName, account);

                ctx.RegisterMessageHandler(HandleMessage);

                CreateRoleReq req = new CreateRoleReq();
                req.roleName = roleName;

                ctx.SendMessage(req);

                ctx.Wait("CreateRole");
            }

        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            object account = null;
            ctx.LoadData("account", out account);
            if (proto.GetID() == ProtoID.CMD_LOGIN_CREATE_ROLE_RSP)
            {
                CreateRoleRsp rsp = (CreateRoleRsp)proto.GetBody();
                if (rsp.errorCode != 0)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[LOGIN] create role fail for user {0}, errCode {1}", (string)account, rsp.errorCode);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] create role {1} success for user {0}", (string)account, rsp.roleInfo.roleID);

                    ctx.SaveData("roleID", rsp.roleInfo.roleID);
                }

                ctx.Signal("CreateRole");
            }
        }
    }
}
