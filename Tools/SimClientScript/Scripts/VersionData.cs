﻿/* ============================================================
* Author:       xieqin
* Time:         2015/3/25 11:42:04
* FileName:     VersionData
* Purpose:      VersionData
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class VersionData
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetVersionDataReq req = new GetVersionDataReq();
            req.tag = param;
            ctx.SendMessage(req);
            ctx.Wait("VersionData");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GET_VERSIONDATA_RSP)
            {
                GetVersionDataRsp rsp = (GetVersionDataRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][VersionData]",
                    Thread.CurrentThread.ManagedThreadId);

                ctx.OnScriptComplete(true);
                ctx.Signal("VersionData");
            }
        }
    }
}
