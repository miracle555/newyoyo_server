﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib;
using Protocols;
using Protocols.Error;
using Script;
using System.Threading;

namespace Script
{
    public class LevelEntry
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            EnterLevelReq protoBody = new EnterLevelReq();
            protoBody.levelId = int.Parse(param);
            ctx.Output.WriteLine("[{0}][LevelEntry] LevelId: {1}",
                Thread.CurrentThread.ManagedThreadId,protoBody.levelId);
            ctx.SendMessage(protoBody);
            ctx.Wait("LevelEntry");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ENTER_LEVEL_RSP)
            {
                EnterLevelRsp rsp = (EnterLevelRsp)proto.GetBody();
                if (rsp.errCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}][EnterLevel] Success",
                        Thread.CurrentThread.ManagedThreadId);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][EnterLevel]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.errCode.ToString());
                }
                ctx.Signal("LevelEntry");
            }
        }
    }
}
