﻿/********************************************************************
	created:	2016/07/20
	created:	20:7:2016   14:51
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild\JeanMallExchange.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild
	file base:	JeanMallExchange
	file ext:	cs
	author:		黄海威
	
	purpose:	珍宝阁物品兑换
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using Protocols.Error;
using System.Threading;

namespace Script
{
    public class JeanMallExchange
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GuildJeanMallExchangeReq protoBody = new GuildJeanMallExchangeReq();
            protoBody.PaperSuitId = 1;
            ctx.Output.WriteLine("[{0}][CalculateTotalScore]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("UpdateTaoMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GUILD_TAOMALLLIST_NTF)
            {
                GuildJeanMallExchangeRsp rsp = (GuildJeanMallExchangeRsp)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}] Id:{1},",
                        Thread.CurrentThread.ManagedThreadId, rsp.PaperSuitId);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][CalculateTotalScore]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("UpdateTaoMallList");
            }
        }
    }
}
