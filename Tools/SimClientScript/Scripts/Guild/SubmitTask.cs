﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib;
using Protocols;
using Protocols.Error;
using System.Threading;

namespace Script
{
    public class SubmitTask
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GuildSubmitTaskReq protoBody = new GuildSubmitTaskReq();
            ctx.Output.WriteLine("[{0}][CalculateTotalScore]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("UpdateTaoMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GUILD_SUBMITTASK_RSP)
            {
                GuildSubmitTaskRsp rsp = (GuildSubmitTaskRsp)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}] Count:{1},",
                        Thread.CurrentThread.ManagedThreadId, rsp.TaskId);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][CalculateTotalScore]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("UpdateTaoMallList");
            }
        }
    }
}
