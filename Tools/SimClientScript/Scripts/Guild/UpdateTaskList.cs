﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib;
using Protocols;
using Protocols.Error;
using System.Threading;


namespace Script
{
    public class UpdateTaskList
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GuildPayUpdateTaskListReq protoBody = new GuildPayUpdateTaskListReq();
            ctx.Output.WriteLine("[{0}][CalculateTotalScore]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("UpdateTaoMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GUILD_GETTASKLIST_NTF)
            {
                GuildGetTaskListNtf rsp = (GuildGetTaskListNtf)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}] Count:{1},",
                        Thread.CurrentThread.ManagedThreadId, rsp.taskList.Count);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][CalculateTotalScore]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("UpdateTaoMallList");
            }
        }
    }
}
