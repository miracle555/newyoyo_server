﻿/********************************************************************
	created:	2016/07/19
	created:	19:7:2016   16:47
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild\TaoMallList.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild
	file base:	TaoMallList
	file ext:	cs
	author:		黄海威
	
	purpose:	淘淘乐商城列表
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CommonLib;
using Protocols;
using Protocols.Error;

namespace Script
{
    public class TaoMallList
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GuildTaoMallListReq protoBody = new GuildTaoMallListReq();
            ctx.Output.WriteLine("[{0}][CalculateTotalScore]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("TaoMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GUILD_TAOMALLLIST_NTF)
            {
                GuildTaoMallListNtf rsp = (GuildTaoMallListNtf)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}] Count:{1},",
                        Thread.CurrentThread.ManagedThreadId, rsp.TaoGoods.Count);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][CalculateTotalScore]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("TaoMallList");
            }
        }
    }
}
