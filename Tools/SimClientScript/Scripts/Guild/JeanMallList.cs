﻿/********************************************************************
	created:	2016/07/19
	created:	19:7:2016   17:13
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild\JeanMallList.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild
	file base:	JeanMallList
	file ext:	cs
	author:		hhw
	
	purpose:	珍宝阁
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CommonLib;
using Protocols;
using Protocols.Error;

namespace Script
{
    public class JeanMallList
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GuildJeanMallListReq protoBody = new GuildJeanMallListReq();
            ctx.Output.WriteLine("[{0}][CalculateTotalScore]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("JeanMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GUILD_JEANMALLLIST_NTF)
            {
                GuildJeanMallListNtf rsp = (GuildJeanMallListNtf)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}] Count:{1},",
                        Thread.CurrentThread.ManagedThreadId, rsp.JeanGoods.Count);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][CalculateTotalScore]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("JeanMallList");
            }
        }
    }
}
