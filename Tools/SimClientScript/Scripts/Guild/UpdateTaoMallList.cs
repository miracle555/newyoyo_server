﻿/********************************************************************
	created:	2016/07/20
	created:	20:7:2016   11:34
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild\UpdateTaoMallList.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Guild
	file base:	UpdateTaoMallList
	file ext:	cs
	author:		黄海威
	
	purpose:	更新淘淘乐商城列表
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib;
using Protocols;
using Protocols.Error;
using System.Threading;

namespace Script
{
    public class UpdateTaoMallList
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            UpdateTaoMallListReq protoBody = new UpdateTaoMallListReq();
            ctx.Output.WriteLine("[{0}][CalculateTotalScore]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("UpdateTaoMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GUILD_TAOMALLLIST_NTF)
            {
                GuildTaoMallListNtf rsp = (GuildTaoMallListNtf)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}] Count:{1},",
                        Thread.CurrentThread.ManagedThreadId, rsp.TaoGoods.Count);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][CalculateTotalScore]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("UpdateTaoMallList");
            }
        }
    }
}
