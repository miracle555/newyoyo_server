﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class QueryGoods
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            QueryGoodsReq req = new QueryGoodsReq();
            req.id = int.Parse(param);
            ctx.SendMessage(req);

            ctx.Wait("QueryGoods");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MALL_QUERY_GOODS_RSP)
            {
                QueryGoodsRsp protoBody = (QueryGoodsRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                    Thread.CurrentThread.ManagedThreadId,
                    System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    ObjectDumper.Dump(protoBody.goods, true));

                ctx.Signal("QueryGoods");
            }
        }
    }
}
