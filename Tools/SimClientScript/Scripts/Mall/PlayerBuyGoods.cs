﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;
using Protocols.Error;
using StaticData;
using Module;
using System.IO;
using System.Diagnostics;

namespace Script
{
    public class PlayerBuyGoods
    {
        static List<int> items = new List<int>();

        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            if (items.Count == 0)
            {
                string ProcessName = Process.GetCurrentProcess().MainModule.FileName;
                string PathOfEXE = Path.GetDirectoryName(ProcessName);
                string path = PathOfEXE + @"\..\..\..\..\Client\Datas\ItemMall.csv";
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line = sr.ReadLine();
                    while (line != null)
                    {
                        string[] ids = line.Split('|');
                        if (ids.Length > 0)
                        {
                            int id = 0;
                            if (int.TryParse(ids[0], out id))
                            {
                                items.Add(id);
                            }
                        }

                        line = sr.ReadLine();
                    }
                }
            }

            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);
            Random ra = new Random();
            PlayerBuyMerchandiseReq req = new PlayerBuyMerchandiseReq();
            BuyGoodsItem it = new BuyGoodsItem();
            int index = ra.Next(0, items.Count);
            it.id = items[index];
            it.count = ra.Next(1, 6);
            req.items.Add(it);
            ctx.SendMessage(req);
            ctx.SaveData("Response", DateTime.Now);
            ctx.Wait("PlayerBuyGoods");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MALL_PLAYER_BUY_MERCHANDISE_RSP)
            {
                object t;
                if (ctx.LoadData("Response", out t))
                {
                    ctx.OnResponse(DateTime.Now - (DateTime)t);
                }

                PlayerBuyMerchandiseRsp protoBody = (PlayerBuyMerchandiseRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][{1}]{2}",
                    Thread.CurrentThread.ManagedThreadId,
                    System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    ObjectDumper.Dump(protoBody, true));

                if (protoBody.error == ErrorID.Success)
                {
                    ctx.OnScriptComplete(true);
                }
                else
                {
                    ctx.OnScriptComplete(false);
                }

                ctx.Signal("PlayerBuyGoods");
            }
        }
    }
}
