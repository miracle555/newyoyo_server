﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class SystemGiveGoods
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            string[] array = param.Split(new char[] { ';' });
            SystemGiveGoodsReq req = new SystemGiveGoodsReq();
            req.targetRoleID = UInt64.Parse(array[0]);
            req.id = int.Parse(array[1]);
            ctx.SendMessage(req);

            Thread.Sleep(1000);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MALL_SYSTEM_GIVE_ITEM_RSP)
            {
                SystemGiveGoodsRsp protoBody = (SystemGiveGoodsRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                    Thread.CurrentThread.ManagedThreadId,
                    System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    ObjectDumper.Dump(protoBody.req, true));
            }
        }
    }
}
