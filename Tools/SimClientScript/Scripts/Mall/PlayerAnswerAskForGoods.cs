﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class PlayerAnswerAskForGoods
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            object roleid = new object();
            ctx.LoadData("roleID", out roleid);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            PlayerAnswerAskForGoodsRsp req = new PlayerAnswerAskForGoodsRsp();
            req.req = new PlayerAskForGoodsReq();
            req.req.askerRoleID = (UInt64)roleid+1;
            req.req.targetRoleID = (UInt64)roleid;
            req.req.id = int.Parse(param);
            req.agree = false;
            ctx.SendMessage(req);

            Thread.Sleep(1000);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MALL_PLAYER_ASKFOR_GOODS_RSP)
            {
                PlayerAskForGoodsRsp protoBody = (PlayerAskForGoodsRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                    Thread.CurrentThread.ManagedThreadId,
                    System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    ObjectDumper.Dump(protoBody, true));
            }
        }
    }
}
