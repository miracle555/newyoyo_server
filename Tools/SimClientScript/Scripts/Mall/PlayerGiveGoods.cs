﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Utility.Common;

namespace Script
{
    public class PlayerGiveGoods
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            object roleid = new object();
            ctx.LoadData("roleID", out roleid);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            PlayerGiveGoodsReq req = new PlayerGiveGoodsReq();
            req.buyerRoleID = (UInt64)roleid;
            req.targetRoleID = req.buyerRoleID+1;
            req.id = int.Parse(param);
            ctx.SendMessage(req);

            Thread.Sleep(1000);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MALL_PLAYER_GIVE_ITEM_RSP)
            {
                PlayerGiveGoodsRsp protoBody = (PlayerGiveGoodsRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                    Thread.CurrentThread.ManagedThreadId,
                    System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    ObjectDumper.Dump(protoBody, true));
            }
        }
    }
}
