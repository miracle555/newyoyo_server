﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using Utility.Common;
using System.Threading;

namespace Script
{
    public class GetGoodsInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object role = new object();
            ctx.LoadData("role", out role);
            ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, (string)role, mb.DeclaringType.Name, mb.Name);

            GetGoodsInfoReq req = new GetGoodsInfoReq();
            ctx.SendMessage(req);

            ctx.Wait("GetGoodsInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MALL_GETGOODSINFO_RSP)
            {
                GetGoodsInfoRsp protoBody = (GetGoodsInfoRsp)proto.GetBody();

                ctx.Output.WriteLine("[{0}][{1}]{2}",
                    Thread.CurrentThread.ManagedThreadId,
                    System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
                    ObjectDumper.Dump(protoBody, true));

                ctx.Signal("GetGoodsInfo");
            }
        }
    }
}
