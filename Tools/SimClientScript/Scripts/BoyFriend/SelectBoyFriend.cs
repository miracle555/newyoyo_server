﻿/********************************************************************
	created:	2016/01/14
	created:	14:1:2016   10:19
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend\SelectBoyFriend.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend
	file base:	SelectBoyFriend
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-选择男友
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectBoyFriend
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            SelectBoyFriendReq protoBody = new SelectBoyFriendReq();
            protoBody.BoyfriendId = Int32.Parse(param);
            ctx.Output.WriteLine("[{0}][SelectBoyFriend]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
        }
    }
}
