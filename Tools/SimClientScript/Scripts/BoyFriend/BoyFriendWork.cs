﻿/********************************************************************
	created:	2016/01/14
	created:	14:1:2016   10:30
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend\BoyFriendWork.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend
	file base:	BoyFriendWork
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-男友打工
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using Log;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class BoyFriendWork
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            BoyFriendWorkReq protoBody = new BoyFriendWorkReq();
            protoBody.BoyfriendId = Int32.Parse(param);
            protoBody.workType = EBoyFriendWorkType.selectCloth;
            ctx.Output.WriteLine("[{0}][BoyFriendWork]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("BoyFriendWork");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_BOYFRIEND_WORK_RSP)
            {
                BoyFriendWorkRsp rsp = (BoyFriendWorkRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][BoyFriendWork]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("BoyFriendWork");
            }
        }
    }
}
