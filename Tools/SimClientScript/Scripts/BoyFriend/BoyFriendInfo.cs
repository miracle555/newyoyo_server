﻿/********************************************************************
	created:	2016/01/20
	created:	20:1:2016   13:58
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend\BoyFriendInfo.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend
	file base:	BoyFriendInfo
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本 男友信息
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    class BoyFriendInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            BoyFriendInfoReq protoBody = new BoyFriendInfoReq();
            ctx.Output.WriteLine("[{0}][BoyFriendInfo]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("BoyFriendInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_BOYFRIEND_INFO_RSP)
            {
                BoyFriendInfoRsp rsp = (BoyFriendInfoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][BoyFriendInfo]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("BoyFriendInfo");
            }
        }
    }
}
