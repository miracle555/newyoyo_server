﻿/********************************************************************
	created:	2016/01/14
	created:	14:1:2016   9:40
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend\PayUnlockBoyFriend.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend
	file base:	PayUnlockBoyFriend
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-付费解锁男友
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Log;
using Protocols;
using Utility.Common;

namespace Script
{
    public class PayUnlockBoyFriend
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            PayUnlockBoyFriendReq protoBody = new PayUnlockBoyFriendReq();
            protoBody.BoyFriendId = Int32.Parse(param);
            ctx.Output.WriteLine("[{0}][PayUnlockBoyFriend]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("PayUnlockBoyFriend");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_BOYFRIEND_PAYUNLOCK_RSP)
            {
                PayUnlockBoyFriendRsp rsp = (PayUnlockBoyFriendRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][PayUnlockBoyFriend]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("PayUnlockBoyFriend");
            }
        }
    }
}
