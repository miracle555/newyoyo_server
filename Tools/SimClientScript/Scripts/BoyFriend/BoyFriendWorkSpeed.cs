﻿/********************************************************************
	created:	2016/01/14
	created:	14:1:2016   10:12
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend\BoyFriendWorkSpeed.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\BoyFriend
	file base:	BoyFriendWorkSpeed
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-男友打工加速
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using Log;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class BoyFriendWorkSpeed
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            BoyFriendWorkSpeedReq protoBody = new BoyFriendWorkSpeedReq();
            protoBody.BoyfriendId = Int32.Parse(param);
            protoBody.WorkType = EBoyFriendWorkType.selectCloth;
            ctx.Output.WriteLine("[{0}][BoyFriendWorkSpeed]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("BoyFriendWorkSpeed");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_BOYFRIEND_WORKSPEED_RSP)
            {
                BoyFriendWorkSpeedRsp rsp = (BoyFriendWorkSpeedRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][BoyFriendWorkSpeed]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("BoyFriendWorkSpeed");
            }
        }
    }
}
