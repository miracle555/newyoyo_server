﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class MakeDress
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ManufactureMakeDressReq protoBody = new ManufactureMakeDressReq();
            protoBody.productDressId = Int32.Parse(param);

            ctx.Output.WriteLine("[{0}][MakeDress]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("MakeDress");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MANUFACTURE_MAKEDRESS_RSP)
            {
                ManufactureMakeDressRsp rsp = (ManufactureMakeDressRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][MakeDress]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("MakeDress");
            }
        }
    }
}
