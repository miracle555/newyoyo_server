﻿using Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utility.Common;

namespace Script
{
    public class DyeDress
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ManufactureDyeDressReq protoBody = new ManufactureDyeDressReq();
            string[] ids = param.Split('|');
            protoBody.productDressId = Int32.Parse(ids[0]);
            protoBody.rawDressId = Int32.Parse(ids[1]);

            ctx.Output.WriteLine("[{0}][DyeDress]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("DyeDress");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MANUFACTURE_DYEDRESS_RSP)
            {
                ManufactureDyeDressRsp rsp = (ManufactureDyeDressRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][DyeDress]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("DyeDress");
            }
        }
    }
}
