﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class EvolveDress
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ManufactureEvolveDressReq protoBody = new ManufactureEvolveDressReq();
            protoBody.productDressId = Int32.Parse(param);

            ctx.Output.WriteLine("[{0}][EvolveDress]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("EvolveDress");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MANUFACTURE_DYEDRESS_RSP)
            {
                ManufactureEvolveDressRsp rsp = (ManufactureEvolveDressRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][EvolveDress]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("EvolveDress");
            }
        }
    }
}
