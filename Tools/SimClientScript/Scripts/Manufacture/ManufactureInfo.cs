﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class ManufactureInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ManufactureInfoReq protoBody = new ManufactureInfoReq();
            ctx.Output.WriteLine("[{0}][ManufactureInfo]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("ManufactureInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MANUFACTURE_DYEDRESS_RSP)
            {
                ManufactureInfoRsp rsp = (ManufactureInfoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][ManufactureInfo]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("ManufactureInfo");
            }
        }
    }
}
