﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;


namespace Script
{
    public class DressEffects
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ManufactureDressEffectsReq protoBody = new ManufactureDressEffectsReq();
            string[] ids = param.Split('|');
            protoBody.effectsReadId = Int32.Parse(ids[0]);
            protoBody.dressId = Int32.Parse(ids[1]);

            ctx.Output.WriteLine("[{0}][DressEffects]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("DressEffects");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MANUFACTURE_DRESSEFFECTS_RSP)
            {
                ManufactureDressEffectsRsp rsp = (ManufactureDressEffectsRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][DressEffects]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("DressEffects");
            }
        }
    }
}
