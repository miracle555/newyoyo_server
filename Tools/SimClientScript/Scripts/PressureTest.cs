﻿/* ============================================================
* Author:       xieqin
* Time:         2016/5/12 11:42:04
* FileName:     PressureTest
* Purpose:      压力测试
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;
using Protocols.Error;
using Log;

namespace Script
{
    public class PressureTest
    {
        static Random ra = new Random();

        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
            ctx.SaveData("ActionState", 0);
            ctx.SaveData("ActionTime", DateTime.MinValue);
        }

        public static void Run(Context ctx, string param)
        {
            object obj = null;
            ctx.LoadData("ActionState", out obj);
            int state = (int)obj;
            ctx.LoadData("ActionTime", out obj);
            DateTime t = (DateTime)obj;
            if (DateTime.Now < t)
                return;

            switch (state)
            {
                case 0:
                    {
                        int r = ra.Next(0, 100);
                        if (r < 20)
                        {
                            TestCompitition(ctx, param);
                        }
                        else if (r < 80)
                        {
                            TestScenario(ctx, param);
                        }
                        else
                        {
                            TestGuildLevel(ctx, param);
                        }
                    }
                    break;

                case 1:
                    {
                        StartPkReq req = new StartPkReq();
                        req.equips.Add(68066);
                        req.equips.Add(67066);
                        req.equips.Add(72066);
                        ctx.SendMessage(req);
                        ctx.Wait("Competition");
                    }
                    break;

                case 2:
                    {
                        EndPkReq req = new EndPkReq();
                        ctx.SendMessage(req);
                        ctx.Wait("Competition");
                    }
                    break;

                case 11:
                    {
                        ItemBatchEquipReq req = new ItemBatchEquipReq();
                        req.type = 1;
                        req.items.Add(new ItemPositon() { col = 0, itemId = 67066 });
                        req.items.Add(new ItemPositon() { col = 0, itemId = 68066 });
                        req.items.Add(new ItemPositon() { col = 0, itemId = 1 });
                        req.items.Add(new ItemPositon() { col = 0, itemId = 2 });
                        req.items.Add(new ItemPositon() { col = 0, itemId = 4 });
                        ctx.SendMessage(req);
                        ctx.Wait("Scenario");
                    }
                    break;

                case 12:
                    {
                        //AddSubtitleReq req = new AddSubtitleReq();
                        //req.Subtitle = new SubtitleInfo() { FontSize = SubtitleFontSize.middle, Subtitle = "dasddw", Color = SubtitleColor.blue };
                        //ctx.SendMessage(req);
                        //ctx.Wait("Scenario");
                    }
                    break;
            }
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_COMPETITION_SELECTOPPO_RSP)
            {
                SelectOppoRsp rsp = (SelectOppoRsp)proto.GetBody();
                if (rsp.error == ErrorID.Success)
                {
                    ctx.SaveData("ActionState", 1);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[Competition]Select opponent error {0}", rsp.error.ToString());
                    ctx.SaveData("ActionState", 0);
                    ctx.OnScriptComplete(true);
                }
                ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
                ctx.Signal("Competition");
            }
            else if (proto.GetID() == ProtoID.CMD_COMPETITION_STARTPK_RSP)
            {
                StartPkRsp rsp = (StartPkRsp)proto.GetBody();
                if (rsp.error == ErrorID.Success)
                {
                    ctx.SaveData("ActionState", 2);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[Competition]Start pk error {0}", rsp.error.ToString());
                    ctx.SaveData("ActionState", 0);
                    ctx.OnScriptComplete(true);
                }
                ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(15, 25)));
                ctx.Signal("Competition");
            }
            else if (proto.GetID() == ProtoID.CMD_COMPETITION_ENDPK_RSP)
            {
                EndPkRsp rsp = (EndPkRsp)proto.GetBody();
                LogSys.Screen(true, ConsoleColor.Green, "[Competition]End pk accomplished");
                ctx.SaveData("ActionState", 0);
                ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
                ctx.OnScriptComplete(true);
                ctx.Signal("Competition");
            }
            else if (proto.GetID() == ProtoID.CMD_ENTER_LEVEL_RSP)
            {
                EnterLevelRsp rsp = (EnterLevelRsp)proto.GetBody();
                if (rsp.errCode == ErrorID.Success)
                {
                    {
                        ctx.SaveData("ActionState", 11);
                        ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(10, 20)));
                    }
                    //else
                    //{
                    //    ctx.SaveData("ActionState", 12);
                    //    ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
                    //}
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[Scenario]Enter level error {0}", rsp.errCode.ToString());
                    ctx.SaveData("ActionState", 0);
                    ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
                    ctx.OnScriptComplete(true);
                }

                ctx.Signal("Scenario");
            }
            //else if (proto.GetID() == ProtoID.CMD_SUBTITLE_ADD_RSP)
            //{
            //    AddSubtitleRsp rsp = (AddSubtitleRsp)proto.GetBody();
            //    if (rsp.ErrCode == ErrorID.Success)
            //    {
            //        if (ra.Next(0, 100) < 90)
            //        {
            //            ctx.SaveData("ActionState", 11);
            //            ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(10, 20)));
            //        }
            //        else
            //        {
            //            ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
            //        }
            //    }
            //    else
            //    {
            //        LogSys.Screen(true, ConsoleColor.Red, "[Scenario]Add subtitle error {0}", rsp.ErrCode.ToString());
            //        ctx.SaveData("ActionState", 11);
            //        ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
            //    }

            //    ctx.Signal("Scenario");
            //}
            else if (proto.GetID() == ProtoID.CMD_ITEM_BATCHEQUIP_RSP)
            {
                ItemBatchEquipRsp rsp = (ItemBatchEquipRsp)proto.GetBody();
                if (rsp.errCode == ErrorID.Success)
                {
                    CalculateScoreReq req = new CalculateScoreReq();
                    ctx.SendMessage(req);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[Scenario]Equip item error {0}", rsp.errCode.ToString());
                    ctx.SaveData("ActionState", 0);
                    ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
                    ctx.OnScriptComplete(true);
                    ctx.Signal("Scenario");
                }
            }
            else if (proto.GetID() == ProtoID.CMD_CALCULATE_SCORE_RSP)
            {
                CalculateScoreRsp rsp = (CalculateScoreRsp)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    LeaveLevelReq req = new LeaveLevelReq();
                    req.levelId = 1010100;
                    ctx.SendMessage(req);

                    LogSys.Screen(true, ConsoleColor.Green, "[Scenario]Scenario accomplished");
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[Scenario]Calculate score error {0}", rsp.ErrCode.ToString());
                }

                ctx.SaveData("ActionState", 0);
                ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
                ctx.OnScriptComplete(true);
                ctx.Signal("Scenario");
            }
            else if (proto.GetID() == ProtoID.CMD_GUILD_PASSLEVEL_RSP)
            {
                GuildPassLevelRsp rsp = (GuildPassLevelRsp)proto.GetBody();
                LogSys.Screen(true, ConsoleColor.Green, "[GuildLevel]Pass Level accomplished");
                ctx.SaveData("ActionState", 0);
                ctx.SaveData("ActionTime", DateTime.Now + new TimeSpan(0, 0, ra.Next(5, 15)));
                ctx.OnScriptComplete(true);
                ctx.Signal("GuildLevel");
            }
        }

        /// <summary>
        /// PK
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="param"></param>
        static void TestCompitition(Context ctx, string param)
        {
            SelectOppoReq req = new SelectOppoReq();
            ctx.SendMessage(req);
            ctx.Wait("Competition");
        }

        /// <summary>
        /// 剧情
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="param"></param>
        static void TestScenario(Context ctx, string param)
        {
            {
                MapListReq req = new MapListReq();
                ctx.SendMessage(req);
            }

            {
                EnterLevelReq req = new EnterLevelReq();
                req.levelId = 1010100;
                ctx.SendMessage(req);
                ctx.Wait("Scenario");
            }
        }

        /// <summary>
        /// 公会剧情
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="param"></param>
        static void TestGuildLevel(Context ctx, string param)
        {
            {
                GuildMapListReq req = new GuildMapListReq();
                ctx.SendMessage(req);
            }

            {
                GuildPassLevelReq req = new GuildPassLevelReq();
                req.LevelId = 1010100;
                req.CurEquips = new List<int>();
                req.CurEquips.Add(67066);
                req.CurEquips.Add(68066);
                req.CurEquips.Add(72066);
                ctx.SendMessage(req);
                ctx.Wait("GuildLevel");
            }
        }
    }
}
