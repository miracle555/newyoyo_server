﻿/* ============================================================
* Author:       shiyaoli
* Time:         2015/01/07 9:56:15
* FileName:     Pipeline
* Purpose:      pipeline testing
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;

namespace Script
{
    public class Pipeline
    {
        public static void Run(Context ctx, string param)
        {
            GMPipelineReq req = new GMPipelineReq();
            req.pipelineID = 1;

            LogSys.Screen(true, ConsoleColor.DarkGreen, "[PIPELINE] Send Pipeline  Req {0}", req.pipelineID);

            ctx.SendMessage(req);

            ctx.RegisterMessageHandler(HandleMessage);

            ctx.Wait("Pipeline");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_GM_PIPELINE_RSP)
            {
                GMPipelineRsp rsp = (GMPipelineRsp)proto.GetBody();
                if (rsp.errCode != 0)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[PIPELINE] Pipeline test failed, errCode {1}", rsp.errCode);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.DarkGreen, "[PIPELINE] Pipeline {0} test success", rsp.pipelineID);

                }

                ctx.Signal("Pipeline");
            }
        }
    }
}
