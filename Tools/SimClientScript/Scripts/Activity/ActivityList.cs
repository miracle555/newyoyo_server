﻿/********************************************************************
	created:	2016/03/14
	created:	14:3:2016   15:29
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Activity\ActivityList.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Activity
	file base:	ActivityList
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-活动列表
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class ActivityList
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ActivityListReq protoBody = new ActivityListReq();
            ctx.Output.WriteLine("[{0}][ActivityList]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("ActivityList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ACTIVITY_LIST_RSP)
            {
                ActivityListRsp rsp = (ActivityListRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][ActivityList]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("ActivityList");
            }
        }
    }
}
