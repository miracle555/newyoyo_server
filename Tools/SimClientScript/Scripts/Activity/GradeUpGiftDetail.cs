﻿/********************************************************************
	created:	2016/03/14
	created:	14:3:2016   15:33
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Activity\GradeUpGiftDetail.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Activity
	file base:	GradeUpGiftDetail
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-等级礼包详细信息
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class GradeUpGiftDetail
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ActivityDetailReq protoBody = new ActivityDetailReq();
            //protoBody.type = ActivityType.GradeUpGift;

            ctx.Output.WriteLine("[{0}][GradeUpGiftDetail]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("GradeUpGiftDetail");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ACTIVITY_GRADEUP_DETAIL_NTF)
            {
                ActivityGradeUpDetailNtf rsp = (ActivityGradeUpDetailNtf)proto.GetBody();
                ctx.Output.WriteLine("[{0}][GradeUpGiftDetail]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("GradeUpGiftDetail");
            }
        }
    }
}
