﻿/********************************************************************
	created:	2016/03/14
	created:	14:3:2016   15:35
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Activity\DrawGradeUpReward.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\Activity
	file base:	DrawGradeUpReward
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-领取等级礼包奖励
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class DrawGradeUpReward
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ActivityPutRewardReq protoBody = new ActivityPutRewardReq();
            //protoBody.type = ActivityType.GradeUpGift;
            protoBody.index = Int32.Parse(param);

            ctx.Output.WriteLine("[{0}][DrawGradeUpReward]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("DrawGradeUpReward");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ACTIVITY_PUTREWARD_RSP)
            {
                ActivityPutRewardRsp rsp = (ActivityPutRewardRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][DrawGradeUpReward]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("DrawGradeUpReward");
            }
        }
    }
}
