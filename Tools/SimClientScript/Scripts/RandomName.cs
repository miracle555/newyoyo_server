﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;
using Utility.Common;

namespace Script
{
    public class RandomName
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            RandomNameReq protoBody = new RandomNameReq();
            ctx.Output.WriteLine("[{0}][RandomName]name = {1}",
                Thread.CurrentThread.ManagedThreadId, param);
            ctx.SendMessage(protoBody);
            ctx.Wait("RandomName");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_RANDOM_NAME_RSP)
            {
                RandomNameRsp rsp = (RandomNameRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][RandomName]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("RandomName");
            }
        }
    }
}
