﻿/********************************************************************
	created:	2015/12/24
	created:	24:12:2015   14:14
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\PerformanceTest\SelectionCompetitionRank.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\PerformanceTest
	file base:	SelectionCompetitionRank
	file ext:	cs
	author:		黄海威
	
	purpose:	压力测试脚本，评选赛排行榜
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;
using Protocols.Error;
using Log;

namespace Script
{
    /// <summary>
    /// 
    /// </summary>
    public class SelectionCompetitionRank
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GetEquipsScoreReq protoBody = new GetEquipsScoreReq();
            protoBody.Equips = RandomEquips.GetRandomEquips("itemData.csv");
            ctx.Output.WriteLine("[{0}][SelectionCompetitionRank]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectionCompetitionRank");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_GETEQUIPSCORE_RSP)
            {
                GetEquipsScoreRsp rsp = (GetEquipsScoreRsp)proto.GetBody();
                if (ErrorID.Success == rsp.ErrCode)
                {
                    GivePraiseReq protoBody = new GivePraiseReq();
                    protoBody.RoleId = proto.GetRoleID();
                    ctx.SendMessage(protoBody);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[SelectionCompetitionRank] GetEquipsScoreRsp failed!");
                    ctx.OnScriptComplete(false);
                }
            }

            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_GIVEPRAISE_RSP)
            {
                GivePraiseRsp rsp = (GivePraiseRsp)proto.GetBody();
                if (ErrorID.Success == rsp.ErrCode)
                {
                    GetThemeLeaderboardReq req = new GetThemeLeaderboardReq();
                    Random rd = new Random();
                    int index = rd.Next(0, 9);
                    req.Index = 0;
                    ctx.SendMessage(req);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[SelectionCompetitionRank] GivePraiseRsp failed!");
                    //ctx.OnScriptComplete(false);
                }
            }

            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_THEMELEADERBOARD_RSP)
            {
                GetThemeLeaderboardRsp rsp = (GetThemeLeaderboardRsp)proto.GetBody();

                SendFlowerReq protoBody = new SendFlowerReq();
                protoBody.RoleId = proto.GetRoleID();
                protoBody.FlowerId = RandomEquips.GetRandomFlowerId();
                Random rd = new Random();
                protoBody.Count = rd.Next(1, 6);
                ctx.SendMessage(protoBody);
            }

            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_SENDFLOWER_RSP)
            {
                SendFlowerRsp rsp = (SendFlowerRsp)proto.GetBody();;
                if (ErrorID.Success == rsp.ErrCode)
                {
                    GetThemeLeaderboardReq req = new GetThemeLeaderboardReq();
                    Random rd = new Random();
                    int index = rd.Next(0, 9);
                    req.Index = 0;
                    ctx.SendMessage(req);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[SelectionCompetitionRank] GivePraiseRsp failed!");
                    //ctx.OnScriptComplete(false);
                }
            }

            if (proto.GetID() == ProtoID.CMD_SELECTIONCOMPETITION_THEMELEADERBOARD_RSP)
            {
                GetThemeLeaderboardRsp rsp = (GetThemeLeaderboardRsp)proto.GetBody();
                if (ErrorID.Success == rsp.ErrCode)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[SelectionCompetitionRank] role:{0} GetEquipRsp Success!", proto.GetID());
                    ctx.OnScriptComplete(true);
                    ctx.Signal("SelectionCompetitionRank");
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[SelectionCompetitionRank] GetThreeRandomPlayerRsp failed!");
                    ctx.OnScriptComplete(false);
                }
            }
        }
    }
}
