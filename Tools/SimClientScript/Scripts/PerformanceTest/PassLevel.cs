﻿/********************************************************************
	created:	2015/11/12
	created:	12:11:2015   20:57
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\PerformanceTest\PassLevel.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\PerformanceTest
	file base:	PassLevel
	file ext:	cs
	author:		huanghaiwei
	
	purpose:	服务器性嫩测试 过关评分
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;
using Protocols.Error;
using Log;
using System.Diagnostics;
using System.IO;

namespace Script
{
    /// <summary>
    /// 过关评分脚本
    /// </summary>
    public class PassLevel
    {
        static List<int> levelIds = new List<int>();
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            if (levelIds.Count == 0)
            {
                string ProcessName = Process.GetCurrentProcess().MainModule.FileName;
                string PathOfEXE = Path.GetDirectoryName(ProcessName);
                string path = PathOfEXE + @"\..\..\..\..\Client\Datas\LevelData.csv";
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line = sr.ReadLine();
                    while (line != null)
                    {
                        string[] ids = line.Split('|');
                        if (ids.Length > 0)
                        {
                            int id = 0;
                            if (int.TryParse(ids[0], out id))
                            {
                                levelIds.Add(id);
                            }
                        }
                        line = sr.ReadLine();
                    }
                }
            }

            Random rd = new Random();
            int index = rd.Next(0, levelIds.Count);
            EnterLevelReq protoBody = new EnterLevelReq();
            protoBody.levelId = levelIds[index];
            ctx.Output.WriteLine("[{0}][EnterLevel] LevelId={1}", Thread.CurrentThread.ManagedThreadId, protoBody.levelId);
            ctx.SendMessage(protoBody);
            ctx.Wait("PassLevel");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;

            if (proto.GetID() == ProtoID.CMD_ENTER_LEVEL_RSP)
            {
                EnterLevelRsp rsp = (EnterLevelRsp)proto.GetBody();
                if (rsp.errCode != ErrorID.Success)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[PassLevel] EnterLevel fail errCode {0}", rsp.errCode);
                    ctx.OnScriptComplete(false);
                }
                else
                {
                    CalculateScoreReq req = new CalculateScoreReq();
                    ctx.SendMessage(req);
                }
            }

            if (proto.GetID()==ProtoID.CMD_CALCULATE_SCORE_RSP)
            {
                CalculateScoreRsp rsp = new CalculateScoreRsp();
                if (rsp.ErrCode != ErrorID.Success)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[PassLevel] CalculateScore fail errCode {0}", rsp.ErrCode);
                    ctx.OnScriptComplete(false);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[PassLevel] PassLevel Success errCode {0}", rsp.ErrCode);
                    ctx.OnScriptComplete(true);
                    ctx.Signal("PassLevel");
                }
            }
        }
    }
}
