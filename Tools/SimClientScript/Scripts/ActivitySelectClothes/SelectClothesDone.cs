﻿/********************************************************************
	created:	2016/10/10
	created:	10:10:2016   9:55
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\ActivitySelectClothes\SelectClothesDone.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\ActivitySelectClothes
	file base:	SelectClothesDone
	file ext:	cs
	author:		黄海威
	
	purpose:	搭配二选一选择
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib;
using Protocols;
using Protocols.Error;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectClothesDone
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ActivityPutRewardReq protoBody = new ActivityPutRewardReq();
            //protoBody.type = ActivityType.SelectClothes;
            protoBody.index = Int32.Parse(param.ToString());
            ctx.Output.WriteLine("[{0}][SelectClothesDone]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectClothesDone");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ACTIVITY_SELECTCLOTHES_REWARD_RSP)
            {
                ActivitySelectClothesRewardRsp rsp = (ActivitySelectClothesRewardRsp)proto.GetBody();
                if (rsp.Errcode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}][SelectClothesInfo]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][SelectClothesDone]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.Errcode.ToString());
                }
                ctx.Signal("SelectClothesDone");
            }
        }
    }
}
