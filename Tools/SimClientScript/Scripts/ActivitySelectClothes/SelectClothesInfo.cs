﻿/********************************************************************
	created:	2016/10/10
	created:	10:10:2016   9:53
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\ActivitySelectClothes\SelectClothesInfo.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\ActivitySelectClothes
	file base:	SelectClothesInfo
	file ext:	cs
	author:		黄海威
	
	purpose:	搭配二选一活动信息
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLib;
using Protocols;
using Protocols.Error;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SelectClothesInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ActivityDetailReq protoBody = new ActivityDetailReq();
            //protoBody.type = ActivityType.SelectClothes;
            ctx.Output.WriteLine("[{0}][SelectClothesInfo]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("SelectClothesInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ACTIVITY_SELECTCLOTHES_INFO_RSP)
            {
                ActivitySelectClothesInfoRsp rsp = (ActivitySelectClothesInfoRsp)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}][SelectClothesInfo]{1}",
                   Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][SelectClothesInfo]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("SelectClothesInfo");
            }
        }
    }
}
