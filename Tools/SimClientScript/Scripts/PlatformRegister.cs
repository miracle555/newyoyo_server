﻿/* ============================================================
* Author:       zhouyu
* Time:         2015/09/09 9:56:15
* FileName:     Platform
* Purpose:      点点乐平台注册脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;
using System.Net;
using Utility;
using System.IO;
using System.Security.Cryptography;

namespace Script
{
    public class PlatformRegister
    {
        public static void Run(Context ctx, string param)
        {
            LogSys.Screen("Register.Run");

            object username = new object();
            ctx.LoadData("account", out username);
            object password = new object();
            ctx.LoadData("password", out password);
            string passwordMD5 = GetMD5((string)password);

            JsonObject jo = new JsonObject();
            jo.SetStringValue("UserName", (string)username);
            jo.SetStringValue("Password", passwordMD5);
            jo.SetIntValue("UserType", 1);
            jo.SetIntValue("AppId", 101);
            string sign = "101" + "d89f3a35931c386956c1a402a8e09941" + (string)username + passwordMD5;
            jo.SetStringValue("Sign", GetMD5(sign));
            string data = jo.ToString();

            StringBuilder sb = new StringBuilder();
            sb.Append("http://sandbox.ddianle.com/api/registration");


            HttpWebRequest request = WebRequest.Create(sb.ToString()) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";

            Stream stream = request.GetRequestStream();
            StreamWriter streamWriter = new StreamWriter(stream, Encoding.UTF8);
            streamWriter.Write(data);
            streamWriter.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader streamReader = new StreamReader(response.GetResponseStream());
            string content = streamReader.ReadToEnd();
            streamReader.Close();

            LogSys.Screen("content = {0}", content);

            JsonObject js = new JsonObject(content);
            int code = 0;
            bool ret = js.GetIntValue("Code", out code);
            if (ret && code == 10000)
            {
                LogSys.Screen("register failed");
            }
            else
            {
                LogSys.Screen("register success");
            }

            Thread.Sleep(1000);
        }

        public static string GetMD5(string myString)
        {
            byte[] result = Encoding.Default.GetBytes(myString.Trim());    //tbPass为输入密码的文本框  
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(result);
            string ret = BitConverter.ToString(output).Replace("-", "");  //tbMd5pass为输出加密文本的文本框  
            return ret.ToLower();
        }
    }
}
