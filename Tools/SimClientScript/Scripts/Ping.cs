﻿/********************************************************************
	created:	2014/12/10
	created:	10:12:2014   11:14
	filename: 	CommonPlatform\Server\Tools\SimClientScript\Scripts\Ping.cs
	file path:	CommonPlatform\Server\Tools\SimClientScript\Scripts
	file base:	Ping
	file ext:	cs
	author:		史耀力
	
	purpose:    ping 服务器通讯	
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using Log;
using System.Threading;
using ServerLib.Bus;

namespace Script
{
    public class Ping
    {
        public static void Run(Context ctx, string param)
        {
            Console.WriteLine("[{0}][Ping.Run]", Thread.CurrentThread.ManagedThreadId);

            ctx.RegisterMessageHandler(HandlePong);

            ServerPing ping = new ServerPing();
            ping.echo = "SimClient Ping!";
            ctx.SendMessage(ping);
			
			Thread.Sleep(3000);
        }

        static void HandlePong(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_SERVER_PONG)
            {
                ServerPong pong = (ServerPong)proto.GetBody();
                VirtualAddress addr = new VirtualAddress(pong.from);
                Console.WriteLine("[{0}][Login.HandleMessage] pong info = {1} {2}", Thread.CurrentThread.ManagedThreadId,
                    addr, pong.echo);
            }
        }
    }
}
