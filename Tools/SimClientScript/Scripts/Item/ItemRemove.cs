﻿/* ============================================================
* Author:       xieqin
* Time:         2015/1/20 11:21:13
* FileName:     ItemRemove
* Purpose:      物品删除
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CommonLib;
using Protocols;
using Protocols.Error;
using ItemComponent;

namespace Script
{
    public class ItemRemove
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ItemRemoveReq protoBody = new ItemRemoveReq();
            int p = int.Parse(param);
            protoBody.col = p % 100000 / 10000;
            protoBody.itemId = p % 10000 / 100;
            protoBody.count = p % 100;
            ctx.Output.WriteLine("[{0}][ItemRemove]col = {1} itemId = {2} count = {3}",
                Thread.CurrentThread.ManagedThreadId, protoBody.col,
                protoBody.itemId, protoBody.count);
            ctx.SendMessage(protoBody);
            ctx.Wait("ItemRemove");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ITEM_REMOVE_RSP)
            {
                ItemRemoveRsp protoBody = (ItemRemoveRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][ItemRemove]{1}",
                    Thread.CurrentThread.ManagedThreadId, protoBody.errCode.ToString());

                ctx.Signal("ItemRemove");
            }
        }
    }
}
