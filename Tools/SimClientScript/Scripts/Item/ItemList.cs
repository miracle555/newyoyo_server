﻿/* ============================================================
* Author:       xieqin
* Time:         2015/1/20 10:00:54
* FileName:     ItemList
* Purpose:      显示道具列表
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using CommonLib;
using Protocols;
using Protocols.Error;
using StaticData;
using Module;
using ItemComponent;
using System.Diagnostics;

namespace Script
{
    public class ItemList
    {
        public static void Init(Context ctx)
        {
            StaticDataManager.AddTypesTobeLoad(typeof(ItemStaticData));
            string ProcessName = Process.GetCurrentProcess().MainModule.FileName;
            string PathOfEXE = Path.GetDirectoryName(ProcessName);
            string location = PathOfEXE + @"\..\..\..\..\Client\Datas";
            StaticDataManager.AbsolutePath = location;
           // Console.WriteLine("location:" + location);
            StaticDataManager.OnInit(new StaticDataModule(null));

            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ItemListReq protoBody = new ItemListReq();
            protoBody.col = int.Parse(param);
            ctx.Output.WriteLine("[{0}][ItemList]col = {1}", Thread.CurrentThread.ManagedThreadId, protoBody.col);
            ctx.SendMessage(protoBody);
            ctx.Wait("ItemList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ITEM_LIST_RSP)
            {
                ItemListRsp protoBody = (ItemListRsp)proto.GetBody();
                if (protoBody.errCode == ErrorID.Success)
                {
                    BinaryFormatter binFormat = new BinaryFormatter();
                    byte[] buffer = Convert.FromBase64String(protoBody.itemDatas[0]);
                    using (MemoryStream stream = new MemoryStream(buffer))
                    {
                        int type = (int)binFormat.Deserialize(stream);
                        int gridCount = (int)binFormat.Deserialize(stream);

                        ctx.Output.WriteLine("[{0}][ItemList]begin: column type = {1} grid = {2}",
                            Thread.CurrentThread.ManagedThreadId, type, gridCount);

                        for (int i = 0; i < gridCount; i++)
                        {
                            string itemData = (string)binFormat.Deserialize(stream);
                            byte[] itemBuffer = Convert.FromBase64String(itemData);
                            Item item = new OtomeItem();
                            using (MemoryStream itemStream = new MemoryStream(itemBuffer))
                            {
                                item.Deserialize(itemStream);
                            }

                            ctx.Output.WriteLine("[{0}][ItemList]grid = {1} item = {2} : {3}",
                                Thread.CurrentThread.ManagedThreadId, i, item.Id, item.Count);
                        }
                    }
                    ctx.Output.WriteLine("[{0}][ItemList]end", Thread.CurrentThread.ManagedThreadId);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][ItemList]{1}",
                        Thread.CurrentThread.ManagedThreadId, protoBody.errCode.ToString());
                }

                ctx.Signal("ItemList");
            }
            else if (proto.GetID() == ProtoID.CMD_ITEM_UPDATE_NTF)
            {
                ItemUpdateNtf protoBody = (ItemUpdateNtf)proto.GetBody();
                BinaryFormatter binFormat = new BinaryFormatter();

                ctx.Output.WriteLine("[{0}][ItemUpdate]begin: column type = {1}",
                    Thread.CurrentThread.ManagedThreadId, protoBody.col);

                foreach (GridItem grid in protoBody.updateData)
                {
                    ctx.Output.WriteLine("[{0}][ItemUpdate]item = {1} : {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        grid.ItemId, grid.Count);
                }

                ctx.Output.WriteLine("[{0}][ItemUpdate]end", Thread.CurrentThread.ManagedThreadId);
            }
        }
    }
}
