﻿/* ============================================================
* Author:       xieqin
* Time:         2015/1/22 10:53:39
* FileName:     ItemEquip
* Purpose:      装备
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CommonLib;
using Protocols;
using Protocols.Error;
using ItemComponent;

namespace Script
{
    public class ItemEquip
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            ItemEquipReq protoBody = new ItemEquipReq();
            int p = int.Parse(param);
            protoBody.col = p % 1000 / 100;
            protoBody.itemId = p % 100;
            ctx.Output.WriteLine("[{0}][ItemEquip]col = {1} itemId = {2}",
                Thread.CurrentThread.ManagedThreadId, protoBody.col, protoBody.itemId);
            ctx.SendMessage(protoBody);
            ctx.Wait("ItemEquip");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_ITEM_EQUIP_RSP)
            {
                ItemEquipRsp protoBody = (ItemEquipRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][ItemEquip]{1}",
                    Thread.CurrentThread.ManagedThreadId, protoBody.errCode.ToString());

                ctx.Signal("ItemEquip");
            }
        }
    }
}
