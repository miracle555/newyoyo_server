﻿/********************************************************************
	created:	2016/03/21
	created:	21:3:2016   15:59
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\SavePlayerChannelId.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts
	file base:	SavePlayerChannelId
	file ext:	cs
	author:		黄海威
	
	purpose:	保存玩家设备号
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using Log;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class SavePlayerChannel
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            SavePlayerChannelIdReq protoBody = new SavePlayerChannelIdReq();
            protoBody.ChannelId = "3974330867493411340";
            ctx.Output.WriteLine("[{0}][SavePlayerChannelId]name = {1}",
                Thread.CurrentThread.ManagedThreadId, param);
            ctx.SendMessage(protoBody);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
        }
    }
}
