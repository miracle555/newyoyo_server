﻿/* ============================================================
* Author:       xieqin
* Time:         2014/12/5 9:56:15
* FileName:     Login
* Purpose:      玩家登录脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;
using Utility.Common;

namespace Script
{
    /// <summary>
    /// 登录
    /// </summary>
    public class Login
    {
        public static void Run(Context ctx, string param)
        {
            System.Reflection.MethodBase mb = System.Reflection.MethodInfo.GetCurrentMethod();
            object account = new object();
            ctx.LoadData("account", out account);
            object password = new object();
            ctx.LoadData("password", out password);
            //ctx.Output.WriteLine("[{0}][{1}.{2}.{3}]", Thread.CurrentThread.ManagedThreadId, ctx.Account.RoleName, mb.DeclaringType.Name, mb.Name);
            LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] Login for user {0}", (string)account);

            ctx.RegisterMessageHandler(HandleMessage);

            UserLoginReq loginReq = new UserLoginReq();
            loginReq.platformType = 0;

            loginReq.account = (string)account;
            loginReq.password = (string)password;
            ctx.SendMessage(loginReq);
            ctx.SaveData("Response", DateTime.Now);
            ctx.Wait("login");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            object account = new object();
            ctx.LoadData("account", out account);
            if (proto.GetID() == ProtoID.CMD_LOGIN_USERLOGIN_RSP)
            {
                object t;
                if (ctx.LoadData("Response", out t))
                {
                    ctx.OnResponse(DateTime.Now - (DateTime)t);
                }

                UserLoginRsp loginRsp = (UserLoginRsp)proto.GetBody();
                if (loginRsp.errorCode != 0)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[LOGIN] Login fail for user {0}, errCode {1}", (string)account, loginRsp.errorCode);
                    ctx.OnScriptComplete(false);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] Login success for user {0}", (string)account);
                    ctx.SaveData("roleID", loginRsp.roleID);
                    ctx.OnScriptComplete(true);
                    ctx.Signal("login");
                }
            }
            if (proto.GetID() == ProtoID.CMD_LOGIN_ROLE_LOGIN_NTF)
            {
                RoleLoginNtf rsp = (RoleLoginNtf)proto.GetBody();
                LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] {0}", ObjectDumper.Dump(rsp, true));
                foreach(var pair in rsp.infos)
                {
                    if (pair.Key == EPlayerInfoType.Attr)
                    {
                        PlayerAttrClientNtf info = pair.Value as PlayerAttrClientNtf;
                        info.Decode();
                        LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] PlayerAttrClientNtf:{0}", ObjectDumper.Dump(info.AttrsDict, true));
                    }
                }
            }
        }
    }
}
