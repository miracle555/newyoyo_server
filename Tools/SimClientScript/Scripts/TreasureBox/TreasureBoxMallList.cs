﻿/********************************************************************
	created:	2015/11/18
	created:	18:11:2015   11:55
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\TreasureBox\TreasureBoxMallList.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\TreasureBox
	file base:	TreasureBoxMallList
	file ext:	cs
	author:		huanghaiwei
	
	purpose:	测试脚本:抽宝箱兑换商城
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using Log;
using System.Threading;
using Utility.Common;

namespace Script
{
    /// <summary>
    /// 抽宝箱兑换商城物品列表
    /// </summary>
    public class TreasureBoxMallList
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            TreasureBoxMallListReq protoBody = new TreasureBoxMallListReq();
            ctx.Output.WriteLine("[{0}][TreasureBoxMallList]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("TreasureBoxMallList");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_TREASUREBOX_MALLLIST_RSP)
            {
                TreasureBoxMallListRsp rsp = (TreasureBoxMallListRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][TreasureBoxMallList]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("TreasureBoxMallList");
            }
        }
    }
}
