﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class TreasureBoxCompondClove
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            TreasureBoxCompoundCloveReq protoBody = new TreasureBoxCompoundCloveReq();
            ctx.Output.WriteLine("[{0}][TreasureBoxCompondClove]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("TreasureBoxCompondClove");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_TREASUREBOX_COMPOUNDCLOVE_RSP)
            {
                TreasureBoxCompoundCloveRsp rsp = (TreasureBoxCompoundCloveRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][TreasureBoxCompondClove]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("TreasureBoxCompondClove");
            }
        }
    }
}
