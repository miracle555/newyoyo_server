﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class TreasureBoxInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            TreasureBoxInfoReq protoBody = new TreasureBoxInfoReq();
            ctx.Output.WriteLine("[{0}][TreasureBoxInfo]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("TreasureBoxInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_TREASUREBOX_INFO_RSP)
            {
                TreasureBoxInfoRsp rsp = (TreasureBoxInfoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][TreasureBoxInfo]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("TreasureBoxInfo");
            }
        }
    }
}
