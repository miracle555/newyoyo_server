﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class TreasureBoxLuckyDraw
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            TreasureBoxLuckyDrawReq protoBody = new TreasureBoxLuckyDrawReq();
            protoBody.treasureBoxId = Int32.Parse(param);
            ctx.Output.WriteLine("[{0}][TreasureBoxLuckyDraw]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("TreasureBoxLuckyDraw");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_TREASUREBOX_LUCKYDRAW_RSP)
            {
                TreasureBoxLuckyDrawRsp rsp = (TreasureBoxLuckyDrawRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][TreasureBoxLuckyDraw]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("TreasureBoxLuckyDraw");
            }
        }
    }
}
