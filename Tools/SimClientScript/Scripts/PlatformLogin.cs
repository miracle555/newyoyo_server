﻿/* ============================================================
* Author:       zhouyu
* Time:         2015/9/9 9:56:15
* FileName:     PlatformLogin
* Purpose:      点点乐平台登录脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;
using Utility.Common;
using Utility;
using System.IO;
using System.Net;
using System.Security.Cryptography;

namespace Script
{
    public class PlatformLogin
    {
        public static void Run(Context ctx, string param)
        {
            LogSys.Screen("LoginTest.Run");

            ctx.RegisterMessageHandler(HandleMessage);

            object username = new object();
            ctx.LoadData("account", out username);
              object password = new object();
            ctx.LoadData("password", out password);
            string passwordMD5 = GetMD5((string)password);

            LogSys.Screen("passwordMD5  {0}", passwordMD5);

            JsonObject jo = new JsonObject();
            jo.SetStringValue("UserName", (string)username);
            jo.SetStringValue("Password", passwordMD5);
            string sign = "101" + "d89f3a35931c386956c1a402a8e09941" + (string)username + passwordMD5;
            jo.SetStringValue("Sign", GetMD5(sign));
            jo.SetIntValue("AppId", 101);
            jo.SetIntValue("UserType", 1);
            string data = jo.ToString();

            StringBuilder sb = new StringBuilder();
            sb.Append("http://sandbox.ddianle.com/api/Login");


            HttpWebRequest request = WebRequest.Create(sb.ToString()) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";

            Stream stream = request.GetRequestStream();
            StreamWriter streamWriter = new StreamWriter(stream, Encoding.UTF8);
            streamWriter.Write(data);
            streamWriter.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader streamReader = new StreamReader(response.GetResponseStream());
            string content = streamReader.ReadToEnd();
            streamReader.Close();

            LogSys.Screen("content = {0}", content);

            JsonObject js = new JsonObject(content);
            int code = 0;
            string uid;
            string token;
            string name;
            bool ret = js.GetIntValue("Code", out code);
            bool ret1= js.GetStringValue("UId", out uid);
            bool ret2 = js.GetStringValue("Token", out token);
            bool ret3 = js.GetStringValue("UserName", out name);
            if (ret && code == 10000)
            {
                LogSys.Screen("login failed");
            }
            else
            {
                UserLoginReq body = new UserLoginReq();
                body.platformType = (UInt16)EPID.DDianle;
                body.account = name;
                body.puid = uid;
                body.token = token;
                LogSys.Screen("YES");
                ctx.SendMessage(body);
                ctx.Wait("PlatformLogin");
            }

            //Thread.Sleep(1000);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            object account = new object();
            ctx.LoadData("account", out account);
            if (proto.GetID() == ProtoID.CMD_LOGIN_USERLOGIN_RSP)
            {
                UserLoginRsp loginRsp = (UserLoginRsp)proto.GetBody();
                if (loginRsp.errorCode != 0)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[LOGIN] Login fail for user {0}, errCode {1}", (string)account, loginRsp.errorCode);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] Login success for user {0}", (string)account);
                    ctx.SaveData("roleID", loginRsp.roleID);
                }
                ctx.Signal("PlatformLogin");
            }
            if (proto.GetID() == ProtoID.CMD_LOGIN_ROLE_LOGIN_NTF)
            {
                RoleLoginNtf rsp = (RoleLoginNtf)proto.GetBody();
                LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] {0}", ObjectDumper.Dump(rsp, true));
                foreach (var pair in rsp.infos)
                {
                    if (pair.Key == EPlayerInfoType.Attr)
                    {
                        PlayerAttrClientNtf info = pair.Value as PlayerAttrClientNtf;
                        info.Decode();
                        LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] PlayerAttrClientNtf:{0}", ObjectDumper.Dump(info.AttrsDict, true));
                    }
                }
            }
        }

        public static string GetMD5(string myString)
        {
            byte[] result = Encoding.Default.GetBytes(myString.Trim());    //tbPass为输入密码的文本框  
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(result);
            string ret = BitConverter.ToString(output).Replace("-", "");  //tbMd5pass为输出加密文本的文本框  
            return ret.ToLower();
        }
    }
}
