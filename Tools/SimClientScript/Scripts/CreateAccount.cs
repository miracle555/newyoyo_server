﻿/* ============================================================
* Author:       huanghaiwei
* Time:         2015/3/10 9:56:15
* FileName:     CreateAccount
* Purpose:      玩家登录脚本
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Protocols;
using Log;
using Protocols.Error;

namespace Script
{
    /// <summary>
    /// 登录
    /// </summary>
    public class CreateAccount
    {
        public static void Run(Context ctx, string param)
        {
            object account = new object();
            ctx.LoadData("account", out account);
            object password = new object();
            ctx.LoadData("password", out password);
            LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] create account {0}", (string)account);

            ctx.RegisterMessageHandler(HandleMessage);

            CreateAccountReq body = new CreateAccountReq();
            body.account = (string)account;
            body.password = (string)password;
            body.platformType = 0;
            ctx.SendMessage(body);
            ctx.SaveData("Response", DateTime.Now);
            ctx.Wait("CreateAccount");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            object account = new object();
            ctx.LoadData("account", out account);
            if (proto.GetID() == ProtoID.CMD_LOGIN_CREATE_ROLE_RSP)
            {
                object t;
                if (ctx.LoadData("Response", out t))
                {
                    ctx.OnResponse(DateTime.Now - (DateTime)t);
                }

                CreateRoleRsp roleBody = (CreateRoleRsp)proto.GetBody();
                if (roleBody.errorCode != 0)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[LOGIN] Create role failed, errCode {0}", roleBody.errorCode);
                    ctx.OnScriptComplete(false);
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] Create role {0} success", roleBody.roleInfo.roleName);
                    ctx.OnScriptComplete(true);
                }
                ctx.Signal("CreateAccount");
            }
            else if (proto.GetID() == ProtoID.CMD_LOGIN_CREATE_ACCOUNT_RSP)
            {
                CreateAccountRsp body = (CreateAccountRsp)proto.GetBody();
                if (body.errorCode != 0)
                {
                    LogSys.Screen(true, ConsoleColor.Red, "[LOGIN] Create Account failed, errCode {0}", body.errorCode);
                    ctx.OnScriptComplete(false);
                    ctx.Signal("CreateAccount");
                }
                else
                {
                    LogSys.Screen(true, ConsoleColor.DarkGreen, "[LOGIN] Create Account success for user {0}", (string)account);
                }
            }
        }
    }
}
