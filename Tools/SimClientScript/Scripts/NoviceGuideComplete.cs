﻿/********************************************************************
	created:	2016/03/15
	created:	15:3:2016   19:32
	filename: 	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts\NoviceGuideComplete.cs
	file path:	E:\HHW\Dev\trunk\Server\Instance\Tools\SimClientScript\Scripts
	file base:	NoviceGuideComplete
	file ext:	cs
	author:		黄海威
	
	purpose:	测试脚本-新手引导
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class NoviceGuideComplete
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            NoviceGuideCompleteNtf protoBody = new NoviceGuideCompleteNtf();
            protoBody.SystemType = NoviceSystem.Manufacture;

            ctx.Output.WriteLine("[{0}][NoviceGuideComplete]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
        }
    }
}
