﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Protocols.Error;
using Utility.Common;

namespace Script
{
    public class GmSearch
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            GmGetRoleTokenLogReq req = new GmGetRoleTokenLogReq();
            req.RoleId = 282574488338449;
            req.StartTime = DateTime.MinValue;
            req.EndTime = DateTime.MaxValue;
            ctx.Output.WriteLine("[{0}][GmSearch]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(req);
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            //Context ctx = (Context)obj;
            //if (proto.GetID() == ProtoID.CMD_CHANGE_NAME_RSP)
            //{
            //    ChangeNameRsp rsp = (ChangeNameRsp)proto.GetBody();
            //    ctx.Output.WriteLine("[{0}][GmSearch]{1}",
            //        Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
            //    ctx.Signal("GmSearch");
            //}
        }
    }
}
