﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CommonLib;
using Protocols;
using Protocols.Error;

namespace Script
{
    public class CalculateTotalScore
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            CalculateScoreReq protoBody = new CalculateScoreReq();
            ctx.Output.WriteLine("[{0}][CalculateTotalScore]",
                Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("CalculateTotalScore");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_CALCULATE_SCORE_RSP)
            {
                CalculateScoreRsp rsp = (CalculateScoreRsp)proto.GetBody();
                if (rsp.ErrCode == ErrorID.Success)
                {
                    ctx.Output.WriteLine("[{0}] Total Score:{1}, Grade:{2}",
                        Thread.CurrentThread.ManagedThreadId, rsp.Scores[0], rsp.Grade);
                }
                else
                {
                    ctx.Output.WriteLine("[{0}][CalculateTotalScore]{1}",
                        Thread.CurrentThread.ManagedThreadId, rsp.ErrCode.ToString());
                }
                ctx.Signal("CalculateTotalScore");
            }
        }
    }
}
