﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class MysteryStoreInfo
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            MysteryStoreInfoReq protoBody = new MysteryStoreInfoReq();
            ctx.Output.WriteLine("[{0}][MysteryStoreInfo]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("MysteryStoreInfo");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MYSTERYSTORE_INFO_RSP)
            {
                MysteryStoreInfoRsp rsp = (MysteryStoreInfoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][MysteryStoreInfo]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("MysteryStoreInfo");
            }
        }
    }
}
