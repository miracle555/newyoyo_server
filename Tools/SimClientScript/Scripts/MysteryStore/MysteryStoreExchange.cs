﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class MysteryStoreExchange
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            MysteryStoreBuyGoodsReq protoBody = new MysteryStoreBuyGoodsReq();
            protoBody.Id = Int32.Parse(param.ToString());
            ctx.Output.WriteLine("[{0}][MysteryStoreExchange]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("MysteryStoreExchange");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MYSTERYSTORE_BUYGOODS_RSP)
            {
                MysteryStoreBuyGoodsRsp rsp = (MysteryStoreBuyGoodsRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][MysteryStoreExchange]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("MysteryStoreExchange");
            }
        }
    }
}
