﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Threading;
using Utility.Common;

namespace Script
{
    public class MysteryStoreRefresh
    {
        public static void Init(Context ctx)
        {
            ctx.RegisterMessageHandler(HandleMessage);
        }

        public static void Run(Context ctx, string param)
        {
            MysteryStoreRefreshReq protoBody = new MysteryStoreRefreshReq();
            ctx.Output.WriteLine("[{0}][MysteryStoreRefresh]", Thread.CurrentThread.ManagedThreadId);
            ctx.SendMessage(protoBody);
            ctx.Wait("MysteryStoreRefresh");
        }

        static void HandleMessage(Protocol proto, object obj)
        {
            Context ctx = (Context)obj;
            if (proto.GetID() == ProtoID.CMD_MYSTERYSTORE_INFO_RSP)
            {
                MysteryStoreInfoRsp rsp = (MysteryStoreInfoRsp)proto.GetBody();
                ctx.Output.WriteLine("[{0}][MysteryStoreRefresh]{1}",
                    Thread.CurrentThread.ManagedThreadId, ObjectDumper.Dump(rsp, true));
                ctx.Signal("MysteryStoreRefresh");
            }
        }
    }
}
