﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protocols;
using System.Net;
using System.Runtime.InteropServices;
using System.IO;
using OfficeOpenXml;

namespace TestClient
{
    public partial class TestClient : Form
    {
        Timer timer = new Timer();
        Session session = new Session();

        public UInt64 roleId = 0;
        public string name = string.Empty;

        Random ra = new Random();

        //////////////////////////////////////////////////////////////////////////
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string section, string key, string defVal, StringBuilder retVal, int size, string filePath);

        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        //////////////////////////////////////////////////////////////////////////
        public TestClient()
        {
            //System.Threading.SynchronizationContext context1 = System.Threading.SynchronizationContext.Current;

            InitializeComponent();

            InitData();
            InitForm();

            //System.Threading.SynchronizationContext context2 = System.Threading.SynchronizationContext.Current;
        }

        //////////////////////////////////////////////////////////////////////////
        void LoadData()
        {
            if (!File.Exists(".\\TestClient.ini"))
            {
                var file = File.Create(".\\TestClient.ini");
                file.Close();
            }
            else
            {
                StringBuilder sb = new StringBuilder(255);
                GetPrivateProfileString("Server", "Count", "", sb, 255, ".\\TestClient.ini");

                try
                {
                    int count = int.Parse(sb.ToString());
                    for (int i = 1; i <= count; i++)
                    {
                        string key = string.Format("Server{0}", i);
                        GetPrivateProfileString("Server", key, "", sb, 255, ".\\TestClient.ini");
                        comboBoxServer.Items.Add(sb.ToString());
                    }
                }
                catch (Exception)
                {
                }

                GetPrivateProfileString("Login", "account", "", sb, 255, ".\\TestClient.ini");
                TBAccount.Text = sb.ToString();
                GetPrivateProfileString("Login", "password", "", sb, 255, ".\\TestClient.ini");
                TBPassword.Text = sb.ToString();
            }
        }

        void SaveData()
        {
            if (!File.Exists(".\\TestClient.ini"))
            {
                var file = File.Create(".\\TestClient.ini");
                file.Close();
            }

            WritePrivateProfileString("Login", "account", TBAccount.Text, ".\\TestClient.ini");
            WritePrivateProfileString("Login", "password", TBPassword.Text, ".\\TestClient.ini");
        }

        public void InitData()
        {
            LoadData();

            timer.Interval = 200;
            timer.Tick += Tick;
            timer.Start();

            session.Init();
        }

        void InitForm()
        {
            BtnLogout.Enabled = false;

            DateTime now = DateTime.Now;
        }

        public void Tick(object sender, EventArgs e)
        {
            SockEvent ev = session.PopEvent();

            while (ev != SockEvent.Null)
            {
                if (ev == SockEvent.ConnSuccess)
                {
                    HandleConnect(true);
                }
                else if (ev == SockEvent.ConnFailed)
                {
                    HandleConnect(false);
                }
                else if (ev == SockEvent.Disconnected)
                {
                    HandleDisconnect();
                }

                ev = session.PopEvent();
            }

            Protocol proto = session.GetMessage();
            while (proto != null)
            {
                HandleMessage(proto);
                proto = session.GetMessage();
            }
        }

        void HandleConnect(bool success)
        {
            if(!success)
            {
                MessageBox.Show("连接失败");
                return;
            }

            UserLoginReq req = new UserLoginReq();
            req.platformType = 0;
            req.account = TBAccount.Text;
            req.password = TBPassword.Text;
            SendMessage(req);
        }

        void HandleDisconnect()
        {
            BtnLogin.Enabled = true;
            BtnLogout.Enabled = false;
            MessageBox.Show("连接断开");
        }

        void HandleMessage(Protocol proto)
        {
            if (proto.MsgId == MSGID.UserLoginRsp)
            {
                UserLoginRsp rsp = (UserLoginRsp)proto.Body;
                if (rsp.errorCode != UserLoginRsp.ErrorID.Success)
                {
                    MessageBox.Show("登录失败");

                    BtnLogin.Enabled = true;
                }
                else
                {
                    MessageBox.Show("登录成功");

                    BtnLogout.Enabled = true;
                }
            }
            else if (proto.MsgId == MSGID.PlayerDataRsp)
            {
                PlayerDataRsp rsp = (PlayerDataRsp)proto.Body;
                HandlePlayerDataRsp(rsp);
            }
            else if (proto.MsgId == MSGID.BattleSearchRoomRsp)
            {
                BattleSearchRoomRsp rsp = (BattleSearchRoomRsp)proto.Body;
                HandleBattleSearchRoomRsp(rsp);
            }
            else if(proto.MsgId == MSGID.KickNtf)
            {
                KickNtf ntf = (KickNtf)proto.Body;
                HandleKickNtf(ntf);
            }
        }

        void HandleKickNtf(KickNtf ntf)
        {
            if(ntf.reason == KickReason.AccountDuplicate)
            {
                MessageBox.Show("同名账号登录，你被踢");
            }
        }


        void HandlePlayerDataRsp(PlayerDataRsp rsp)
        {
            if (rsp.errorCode != PlayerDataRsp.ErrorID.Success)
            {
                MessageBox.Show("获取玩家数据失败");
            }
            else
            {
                MessageBox.Show("获取玩家数据成功," + GetPlayerData(rsp.playerData));
                playerData = rsp.playerData;
            }
        }

        /// <summary>
        /// 搜索房间返回
        /// </summary>
        /// <param name="rsp"></param>
        void HandleBattleSearchRoomRsp(BattleSearchRoomRsp rsp)
        {
            if (rsp.errorCode != BattleSearchRoomRsp.ErrorID.Success)
            {
                MessageBox.Show("搜索房间失败");
            }
            else
            {
                MessageBox.Show("搜索房间成功");
            }
        }

        string GetPlayerData(PlayerData data)
        {
            string result = "";
            result = result + "accountId=" + data.accountId.ToString();
            result = result + ",diamond=" + data.diamond.ToString();
            result = result + ",gold=" + data.gold.ToString();
            result = result + ",level=" + data.level.ToString();
            result = result + ",nickname=" + data.nickName;
            result = result + ",vip=" + data.vip.ToString();
            result = result + ",spirits=[";
            foreach(var item in data.lstSpirits)
            {
                result = result + "<spirituuid=" + item.m_spirit_uuid.ToString() + ",spiritid=" + item.m_spirit_id.ToString() + ">";
            }
            result = result + "]";
            return result;
        }

        PlayerData playerData = new PlayerData();

        void SendMessage(ProtoBody body)
        {
            Protocol proto = new Protocol(body);
            session.SendMessage(proto);
        }

        void SendMessage(Protocol proto)
        {
            session.SendMessage(proto);
        }

        //////////////////////////////////////////////////////////////////////////
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            BtnLogin.Enabled = false;

            if (!string.IsNullOrEmpty(comboBoxServer.Text))
            {
                string key = comboBoxServer.Text;
                StringBuilder sb = new StringBuilder(255);
                GetPrivateProfileString("Server", key, "", sb, 255, ".\\TestClient.ini");
                string serverIp = sb.ToString();
                string[] ips = serverIp.Split(':');
                IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(ips[0]), int.Parse(ips[1]));
                session.Connect(endpoint);
            }

            SaveData();
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            session.Disconnect();
        }

        private void GmClient_Load(object sender, EventArgs e)
        {

        }

        private void btnGetPlayerData_Click(object sender, EventArgs e)
        {
            PlayerDataReq req = new PlayerDataReq();
            SendMessage(req);
        }

        private void btnSearchRoom_Click(object sender, EventArgs e)
        {
            BattleSearchRoomReq req = new BattleSearchRoomReq();
            req.m_spirit_uuid = playerData.lstSpirits[0].m_spirit_uuid;
            SendMessage(req);
        }
    }
}
