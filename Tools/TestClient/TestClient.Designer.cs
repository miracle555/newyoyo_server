﻿namespace TestClient
{
    partial class TestClient
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxServer = new System.Windows.Forms.ComboBox();
            this.BtnLogout = new System.Windows.Forms.Button();
            this.BtnLogin = new System.Windows.Forms.Button();
            this.TBPassword = new System.Windows.Forms.TextBox();
            this.LBPassword = new System.Windows.Forms.Label();
            this.LBAccount = new System.Windows.Forms.Label();
            this.LBServerIp = new System.Windows.Forms.Label();
            this.TBAccount = new System.Windows.Forms.TextBox();
            this.btnGetPlayerData = new System.Windows.Forms.Button();
            this.btnSearchRoom = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBoxServer);
            this.panel1.Controls.Add(this.BtnLogout);
            this.panel1.Controls.Add(this.BtnLogin);
            this.panel1.Controls.Add(this.TBPassword);
            this.panel1.Controls.Add(this.LBPassword);
            this.panel1.Controls.Add(this.LBAccount);
            this.panel1.Controls.Add(this.LBServerIp);
            this.panel1.Controls.Add(this.TBAccount);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(190, 135);
            this.panel1.TabIndex = 1;
            // 
            // comboBoxServer
            // 
            this.comboBoxServer.FormattingEnabled = true;
            this.comboBoxServer.Location = new System.Drawing.Point(50, 3);
            this.comboBoxServer.Name = "comboBoxServer";
            this.comboBoxServer.Size = new System.Drawing.Size(131, 20);
            this.comboBoxServer.TabIndex = 28;
            // 
            // BtnLogout
            // 
            this.BtnLogout.Location = new System.Drawing.Point(3, 109);
            this.BtnLogout.Name = "BtnLogout";
            this.BtnLogout.Size = new System.Drawing.Size(179, 23);
            this.BtnLogout.TabIndex = 7;
            this.BtnLogout.Text = "登出";
            this.BtnLogout.UseVisualStyleBackColor = true;
            this.BtnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
            // 
            // BtnLogin
            // 
            this.BtnLogin.Location = new System.Drawing.Point(3, 80);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(179, 23);
            this.BtnLogin.TabIndex = 6;
            this.BtnLogin.Text = "登录";
            this.BtnLogin.UseVisualStyleBackColor = true;
            this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
            // 
            // TBPassword
            // 
            this.TBPassword.Location = new System.Drawing.Point(50, 51);
            this.TBPassword.Name = "TBPassword";
            this.TBPassword.Size = new System.Drawing.Size(132, 21);
            this.TBPassword.TabIndex = 5;
            this.TBPassword.UseSystemPasswordChar = true;
            // 
            // LBPassword
            // 
            this.LBPassword.AutoSize = true;
            this.LBPassword.Location = new System.Drawing.Point(3, 54);
            this.LBPassword.Name = "LBPassword";
            this.LBPassword.Size = new System.Drawing.Size(29, 12);
            this.LBPassword.TabIndex = 4;
            this.LBPassword.Text = "密码";
            // 
            // LBAccount
            // 
            this.LBAccount.AutoSize = true;
            this.LBAccount.Location = new System.Drawing.Point(3, 30);
            this.LBAccount.Name = "LBAccount";
            this.LBAccount.Size = new System.Drawing.Size(29, 12);
            this.LBAccount.TabIndex = 3;
            this.LBAccount.Text = "帐号";
            // 
            // LBServerIp
            // 
            this.LBServerIp.AutoSize = true;
            this.LBServerIp.Location = new System.Drawing.Point(3, 6);
            this.LBServerIp.Name = "LBServerIp";
            this.LBServerIp.Size = new System.Drawing.Size(41, 12);
            this.LBServerIp.TabIndex = 2;
            this.LBServerIp.Text = "服务器";
            // 
            // TBAccount
            // 
            this.TBAccount.Location = new System.Drawing.Point(50, 27);
            this.TBAccount.Name = "TBAccount";
            this.TBAccount.Size = new System.Drawing.Size(132, 21);
            this.TBAccount.TabIndex = 1;
            // 
            // btnGetPlayerData
            // 
            this.btnGetPlayerData.Location = new System.Drawing.Point(17, 180);
            this.btnGetPlayerData.Name = "btnGetPlayerData";
            this.btnGetPlayerData.Size = new System.Drawing.Size(179, 23);
            this.btnGetPlayerData.TabIndex = 7;
            this.btnGetPlayerData.Text = "获取玩家数据";
            this.btnGetPlayerData.UseVisualStyleBackColor = true;
            this.btnGetPlayerData.Click += new System.EventHandler(this.btnGetPlayerData_Click);
            // 
            // btnSearchRoom
            // 
            this.btnSearchRoom.Location = new System.Drawing.Point(17, 209);
            this.btnSearchRoom.Name = "btnSearchRoom";
            this.btnSearchRoom.Size = new System.Drawing.Size(179, 23);
            this.btnSearchRoom.TabIndex = 8;
            this.btnSearchRoom.Text = "搜索房间";
            this.btnSearchRoom.UseVisualStyleBackColor = true;
            this.btnSearchRoom.Click += new System.EventHandler(this.btnSearchRoom_Click);
            // 
            // TestClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 452);
            this.Controls.Add(this.btnSearchRoom);
            this.Controls.Add(this.btnGetPlayerData);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "TestClient";
            this.Text = "TestClient";
            this.Load += new System.EventHandler(this.GmClient_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox TBPassword;
        private System.Windows.Forms.Label LBPassword;
        private System.Windows.Forms.Label LBAccount;
        private System.Windows.Forms.Label LBServerIp;
        private System.Windows.Forms.TextBox TBAccount;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Button BtnLogout;
        private System.Windows.Forms.ComboBox comboBoxServer;
        private System.Windows.Forms.Button btnGetPlayerData;
        private System.Windows.Forms.Button btnSearchRoom;

    }
}

