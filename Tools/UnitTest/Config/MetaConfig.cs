using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;


namespace Config
{
    public class LogConfig : IConfig
    {
        public string Path
        {
            get;
            set;
        }

        public int RollingStyle
        {
            get;
            set;
        }

        public bool AppendToFile
        {
            get;
            set;
        }

        public string DatePattern
        {
            get;
            set;
        }

        public string Module
        {
            get;
            set;
        }



        public LogConfig()
        {
            Path = "";
            DatePattern = "";
            Module = "";
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "Path")
                {
                    Path = elementNode.InnerText;
                }
                if (elementNode.Name == "RollingStyle")
                {
                    RollingStyle = int.Parse(elementNode.InnerText);
                }
                if (elementNode.Name == "AppendToFile")
                {
                    AppendToFile = bool.Parse(elementNode.InnerText);
                }
                if (elementNode.Name == "DatePattern")
                {
                    DatePattern = elementNode.InnerText;
                }
                if (elementNode.Name == "Module")
                {
                    Module = elementNode.InnerText;
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


    public class ServerConfig : IConfig
    {
        public string IP
        {
            get;
            set;
        }

        public int Heartbeat
        {
            get;
            set;
        }

        public LogConfig LogCfg
        {
            get;
            set;
        }

        public BusConfig BusCfg
        {
            get;
            set;
        }



        public ServerConfig()
        {
            IP = "";
            Heartbeat = 20;
            LogCfg = new LogConfig();
            BusCfg = new BusConfig();
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "IP")
                {
                    IP = elementNode.InnerText;
                }
                if (elementNode.Name == "Heartbeat")
                {
                    Heartbeat = int.Parse(elementNode.InnerText);
                }
                if (elementNode.Name == "LogCfg")
                {
                    LogCfg.Load((XmlElement)elementNode);
                }
                if (elementNode.Name == "BusCfg")
                {
                    BusCfg.Load((XmlElement)elementNode);
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


    public class DemoServerConfig : ServerConfig
    {
        public int MaxPlayer
        {
            get;
            set;
        }

        public List<DBConfig> DbCfg
        {
            get;
            set;
        }



        public DemoServerConfig()
        {
            DbCfg = new List<DBConfig>();
        }


        public override void Load(XmlElement rootElem)
        {
            base.Load(rootElem);


            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "MaxPlayer")
                {
                    MaxPlayer = int.Parse(elementNode.InnerText);
                }
                if (elementNode.Name == "DbCfgs")
                {
                    XmlNodeList childNodes = elementNode.ChildNodes;
                    foreach (XmlNode childNode in childNodes)
                    {
                        DBConfig item = new DBConfig();
                        item.Load((XmlElement)childNode);
                        DbCfg.Add(item);
                    }
                }
            }
        }
        public override void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


    public class DemoServerConfigEx : ServerConfig
    {
        public DemoServerConfigEx()
        {
        }


        public override void Load(XmlElement rootElem)
        {
            base.Load(rootElem);
        }
        public override void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


    public class BusConfig : IConfig
    {
        public List<string> Peer
        {
            get;
            set;
        }

        public string RoutTablePath
        {
            get;
            set;
        }



        public BusConfig()
        {
            Peer = new List<string>();
            RoutTablePath = "";
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "Peers")
                {
                    XmlNodeList childNodes = elementNode.ChildNodes;
                    foreach (XmlNode childNode in childNodes)
                    {
                        Peer.Add(childNode.InnerText);
                    }
                }
                if (elementNode.Name == "RoutTablePath")
                {
                    RoutTablePath = elementNode.InnerText;
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


    public class DBConfig : IConfig
    {
        public string ip
        {
            get;
            set;
        }

        public int port
        {
            get;
            set;
        }



        public DBConfig()
        {
            ip = "";
        }


        public virtual void Load(XmlElement rootElem)
        {
            XmlNodeList elementNodes = rootElem.ChildNodes;
            foreach (XmlNode elementNode in elementNodes)
            {
                if (elementNode.Name == "ip")
                {
                    ip = elementNode.InnerText;
                }
                if (elementNode.Name == "port")
                {
                    port = int.Parse(elementNode.InnerText);
                }
            }
        }
        public virtual void Load(string file)
        {
            XmlDocument Doc = new XmlDocument();
            Doc.Load(file);
            XmlElement rootElem = Doc.DocumentElement;
            Load(rootElem);
        }
    }


}


