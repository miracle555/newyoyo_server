﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Config
{
    public interface IConfig
    {
        //读取配置
        void Load(string file);
        void Load(XmlElement rootElem);
    }
}
