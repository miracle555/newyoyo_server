﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using ConfigGen;
using Config;

namespace UnitTest
{
    [TestClass]
    public class UnitTestConfig
    {
        [TestMethod]
        public void TestConfig()
        {
            string dir = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(@"..\..\UnitTest\Config");
            ConfigConverter.Convert("ServerConfig.xml");

            DemoServerConfig config1 = new DemoServerConfig();
            config1.Load("DemoSvrCfg1.xml");

            Assert.AreEqual(config1.IP, "192.168.11.55");
            Assert.AreEqual(config1.MaxPlayer, 1000);
            Assert.AreEqual(config1.Heartbeat, 30);
            Assert.AreEqual(config1.LogCfg.AppendToFile, true);
            Assert.AreEqual(config1.LogCfg.DatePattern, "yyyyMMdd'.log'");
            Assert.AreEqual(config1.LogCfg.Path, @"..\Log\");
            Assert.AreEqual(config1.LogCfg.RollingStyle, 2);
            Assert.AreEqual(config1.LogCfg.Module, "File");
            Assert.AreEqual(config1.BusCfg.RoutTablePath, @"..\Cfg\RoutTable.xml");
            Assert.AreEqual(config1.BusCfg.Peer.Count, 4);
            Assert.AreEqual(config1.BusCfg.Peer.Contains("1.1.2.1"), true);
            Assert.AreEqual(config1.BusCfg.Peer.Contains("1.1.2.2"), true);
            Assert.AreEqual(config1.BusCfg.Peer.Contains("1.1.4.1"), true);
            Assert.AreEqual(config1.BusCfg.Peer.Contains("1.1.4.2"), true);
            Assert.AreEqual(config1.DbCfg.Count, 2);
            Assert.AreEqual(config1.DbCfg[0].port, 3000);
            Assert.AreEqual(config1.DbCfg[1].port, 4000);

            DemoServerConfig config2 = new DemoServerConfig();
            config2.Load("DemoSvrCfg2.xml");

            Assert.AreEqual(config2.IP, "192.168.11.66");
            Assert.AreEqual(config2.MaxPlayer, 2000);
            Assert.AreEqual(config2.Heartbeat, 50);
            Assert.AreEqual(config2.LogCfg.AppendToFile, false);
            Assert.AreEqual(config2.LogCfg.DatePattern, "yyyyMMdd'.log'");
            Assert.AreEqual(config2.LogCfg.Path, @"..\Log\");
            Assert.AreEqual(config2.LogCfg.RollingStyle, 3);
            Assert.AreEqual(config2.BusCfg.RoutTablePath, @"..\Cfg\RoutTable.xml");
            Assert.AreEqual(config2.BusCfg.Peer.Count, 1);
            Assert.AreEqual(config2.BusCfg.Peer.Contains("0.0.0.0"), true);
            Assert.AreEqual(config2.DbCfg.Count, 2);
            Assert.AreEqual(config2.DbCfg[0].port, 1000);
            Assert.AreEqual(config2.DbCfg[1].port, 2000);
        }
    }
}
