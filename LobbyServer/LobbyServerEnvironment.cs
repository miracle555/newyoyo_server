﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using ServerLib;
using ServerLib.Bus;
using Config;
using Module;
using Protocols;
using Log;
using Serializer = ProtoBuf.Serializer;
using ProtoBuf;
using System.Reflection;
using Utility.Debugger;
using Script;
using StaticData;
using System.Threading;
using Entity;
using MahjongPushDown;
using MahjongPoint;
using Landlord;
using Statistics;
using Room;
using LobbyServer.Other;

namespace LobbyServer
{
    class Lobbyenv
    {
        public Lobbyenv(ServerConfig config)
        {
            env.Init(config);
            env.ModuleManager.RegisterModule(new LoginModule());
            env.ModuleManager.RegisterModule(new MJPushDownModule());
            env.ModuleManager.RegisterModule(new MJPointModule());
            env.ModuleManager.RegisterModule(new LandlordModule());
            env.ModuleManager.RegisterModule(new GmModule());
            env.ModuleManager.RegisterModule(new TestModule());           
            env.ModuleManager.RegisterModule(new GoldenFlowerModule());
            env.ModuleManager.RegisterModule(new MJYunChengModule());
            env.ModuleManager.RegisterModule(new GameModule());
        }

        public bool Start()
        {
            //在部署正式环境服务器时要将此注释放开
            //g.payListener.InitPayListener();
            TimerEveryDay timer = new TimerEveryDay();
            //启动定时器
            timer.start();
            return env.Start();
        }
    }

    class g
    {
        /// <summary>
        /// 房间管理器
        /// </summary>
        public static RoomMgr roomMgr = new RoomMgr();

        /// <summary>
        /// 负载均衡
        /// </summary>
        public static RoomBalance roomBalance = new RoomBalance();

        /// <summary>
        /// 静态数据
        /// </summary>
        public static StaticData staticData = new StaticData();

        /// <summary>
        /// 玩家管理器
        /// </summary>
        public static LobbyPlayerMgr playerMgr = new LobbyPlayerMgr();

        /// <summary>
        /// 数据中心
        /// </summary>
        public static DataCenter dataCenter = new DataCenter();

        /// <summary>
        /// 消息异步处理
        /// </summary>
        public static MsgExtension msgExtension = new MsgExtension();

        /// <summary>
        /// 代理管理器
        /// </summary>
        public static AgentMgr agentMgr = new AgentMgr();

        /// <summary>
        /// 微信http模块
        /// </summary>
        public static HttpWeixin weixin = new HttpWeixin();

        /// <summary>
        /// 排行榜管理器
        /// </summary>
        public static RankMgr rankMgr = new RankMgr();

        /// <summary>
        /// 静态数据,读表
        /// </summary>
        public static StaticDataMgr staticDataMgr = new StaticDataMgr();

        /// <summary>
        /// 充值管理器
        /// </summary>
        public static RechargeMgr rechargeMgr = new RechargeMgr();

        /// <summary>
        /// 微信支付网关
        /// </summary>
        public static PayListener payListener = new PayListener();
    }
}
