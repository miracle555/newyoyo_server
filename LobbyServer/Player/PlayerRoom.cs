﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using ProtoBuf;
using Module;
using Log;
using StaticData;
using System.IO;
using Event;
using System.Data;
using System.Collections;
using ServerLib;
using Redis;

namespace Entity
{
    [ProtoContract]
    public class PlayerRoom
    {
        public PlayerRoom()
        { }

        /// <summary>
        /// 房间唯一ID
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roomUuid = 0;

        /// <summary>
        /// 创建的房间的uuid
        /// </summary>
        [ProtoMember(2)]
        public UInt64 createRoomUuid = 0;

        /// <summary>
        /// 战绩列表
        /// </summary>
        [ProtoMember(3)]
        public List<stBattleScoreItem> lstScoreItems = new List<stBattleScoreItem>();

        /// <summary>
        /// 是否在房间里
        /// </summary>
        /// <returns></returns>
        public bool IsInRoom
        {
            get
            {
                return roomUuid != 0;
            }
        }

        public bool IsAlreadyCreateRoom 
        {
            get
            {
                return createRoomUuid != 0;
            }
        }

        /// <summary>
        /// 添加战绩
        /// </summary>
        /// <param name="item"></param>
        public void AddBattleItem(stBattleScoreItem item)
        {
            lstScoreItems.Add(item);
        }
    }
}
