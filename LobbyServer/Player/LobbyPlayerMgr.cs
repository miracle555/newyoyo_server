﻿/********************************************************************
	created:	2014/12/16
	created:	16:12:2014   9:51
	filename: 	CommonPlatform\Server\ServerInstance\LobbyServer\Entity\LobbyPlayerMgr.cs
	file path:	CommonPlatform\Server\ServerInstance\LobbyServer\Entity
	file base:	LobbyPlayerMgr
	file ext:	cs
	author:		刘冰生
	
	purpose:    账号管理器	
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using Redis;
using ServerLib;
using StackExchange.Redis;
using LobbyServer;

namespace Entity
{
    internal class LobbyPlayerMgr
    {
        public LobbyPlayerMgr()
        {

        }

        /// <summary>
        /// 通过账号查找玩家
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        public LobbyPlayer FindPlayer(EPID platform, string account)
        {
            AccountKey key = new AccountKey(platform, account);
            return FindPlayer(key);
        }

        /// <summary>
        /// 通过账号查找玩家
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public LobbyPlayer FindPlayer(AccountKey key)
        {
            if (!mapAccKeys.ContainsKey(key))
            {
                return null;
            }

            UInt64 accountId = mapAccKeys[key];

            if(!mapPlayers.ContainsKey(accountId))
            {
                return null;
            }

            return mapPlayers[accountId];
        }

        public bool HasPlayer(EPID platform, string account)
        {
            return FindPlayer(platform, account) != null;
        }

        public bool HasPlayer(AccountKey key)
        {
            return FindPlayer(key) != null;
        }

        /// <summary>
        /// 通过账号ID查找玩家
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public LobbyPlayer FindPlayer(UInt64 accountId)
        {
            if (!mapPlayers.ContainsKey(accountId))
            {
                return null;
            }

            return mapPlayers[accountId];
        }

        public bool HasPlayer(UInt64 accountId)
        {
            return FindPlayer(accountId) != null;
        }

        public bool AddPlayer(LobbyPlayer player)
        {
            if (HasPlayer(player.AccountKey))
            {
                return false;
            }

            if (HasPlayer(player.AccountId))
            {
                return false;
            }

            mapAccKeys.Add(player.AccountKey, player.AccountId);
            mapPlayers.Add(player.AccountId, player);
            return true;
        }

        /// <summary>
        /// 通过账号删除玩家
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="account"></param>
        public void RemovePlayer(EPID platform, string account)
        {
            AccountKey key = new AccountKey(platform, account);

            if(!mapAccKeys.ContainsKey(key))
            {
                return;
            }

            UInt64 accountId = mapAccKeys[key];

            mapAccKeys.Remove(key);
            mapPlayers.Remove(accountId);

            return;
        }

        public void RemovePlayer(UInt64 accountId)
        {
            LobbyPlayer player = FindPlayer(accountId);
            if (null == player)
            {
                return;
            }

            mapAccKeys.Remove(player.AccountKey);
            mapPlayers.Remove(accountId);

            return;
        }

        /// <summary>
        /// 异步创建账号
        /// </summary>
        /// <param name="accKey">平台类型和账号</param>
        /// <param name="password">密码</param>
        /// <param name="accountType">账号类型</param>
        /// <returns></returns>
        public async Task<LobbyPlayer> CreateAccountAsync(AccountKey accKey, string password, EAccountType accountType)
        {
            RedisSystem redis = env.Redis;

            RedisValue value = await redis.db.StringIncrementAsync("max_account_id");

            if (value.IsNullOrEmpty)
            {
                return null;
            }

            UInt64 accountId = (UInt64)value;
            if(accountId == 0 || accountId==1)
            {
                accountId = 10001;
                await redis.db.StringSetAsync("max_account_id", accountId);
            }

            LobbyPlayer player = new LobbyPlayer(accountId);
            player.Account.AccountId = accountId;
            player.Account.PlatformType = accKey.platform;
            player.Account.Account = accKey.account;
            player.Account.Password = password;
            player.FirstInit(accountType);

            redis.db.StringSet(player.dbAccountKey, accountId, null, When.Always, CommandFlags.FireAndForget);
            player.Save();

            AddPlayer(player);
            if (player.IsPlayer)
            {
                g.dataCenter.NewAddRoleCount();
            } 
            return player;
        }

        public async Task<LobbyPlayer> FindPlayerAsync(UInt64 accountId)
        {
            LobbyPlayer player = FindPlayer(accountId);
            if (null != player)
            {
                return player;
            }

            player = new LobbyPlayer(accountId);
            bool bResult = await player.LoadFromDbAsync();
            if (!bResult)
            {
                return null;
            }

            AddPlayer(player);

            return player;
        }

        public async Task<LobbyPlayer> FindPlayerAsync(AccountKey accKey)
        {
            LobbyPlayer player = FindPlayer(accKey);
            if (null != player)
            {
                return player;
            }

            RedisSystem redis = env.Redis;

            player = new LobbyPlayer(accKey);
            bool bResult = await player.LoadFromDbAsync();
            if (!bResult)
            {
                return null;
            }

            AddPlayer(player);

            return player;
        }

        public async Task<LobbyPlayer> FindOrCreatePlayerAsync(AccountKey accKey, string password)
        {
            LobbyPlayer player = await FindPlayerAsync(accKey);
            if (null != player)
            {
                return player;               
            }

            player = await CreateAccountAsync(accKey, password, EAccountType.Player);

            return player;
        }

        public void CheckAllPlayer(Action<LobbyPlayer> function)
        {
            foreach(var item in mapPlayers.Values)
            {
                function(item);
            }
        }

        /// <summary>
        /// 当前在线人数
        /// </summary>
        public int onlineRoleCount
        {
            get
            {
                int count = 0;
                foreach(var item in mapPlayers.Values)
                {
                    if(item.isOnline)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        public async Task<UInt64> GetMaxAccountId()
        {
            RedisSystem redis = env.Redis;

            RedisValue value = await redis.db.StringGetAsync("max_account_id");

            if (value.IsNullOrEmpty)
            {
                return ConstValue.MIN_ACCOUNT_ID;
            }

            UInt64 accountId = (UInt64)value;
            return accountId;
        }

        //////////////////////////////////////////////////////////////////////////
        // Data Member
        private Dictionary<UInt64, LobbyPlayer> mapPlayers = new Dictionary<UInt64, LobbyPlayer>();
        private Dictionary<AccountKey, UInt64> mapAccKeys = new Dictionary<AccountKey, UInt64>();
    }
}
