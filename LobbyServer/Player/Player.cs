﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Module;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using ProtoBuf;
using Redis;
using Protocols.gm;

namespace Entity
{
    [ProtoContract]
    [ProtoInclude(100, typeof(LobbyPlayer))]
    public class Player
    {
        public Player()
        {
            Account = new PlayerAccount();
            Attribute = new PlayerAttribute();
            room = new PlayerRoom();
            agent = new PlayerAgent();
            anRoomList = new Dictionary<UInt64, AnthorRoom>();
        }

        /// <summary>
        /// 账号信息
        /// </summary>
        [ProtoMember(1)]
        public PlayerAccount Account;

        /// <summary>
        /// 账号属性
        /// </summary>
        [ProtoMember(2)]
        public PlayerAttribute Attribute;

        /// <summary>
        /// 推倒胡麻将信息
        /// </summary>
        [ProtoMember(3)]
        public PlayerRoom room;

        /// <summary>
        /// 代理商信息
        /// </summary>
        [ProtoMember(4)]
        public PlayerAgent agent;

        /// <summary>
        /// 房主建房列表
        /// </summary>
        [ProtoMember(5)]
        public Dictionary<UInt64, AnthorRoom> anRoomList = new Dictionary<UInt64, AnthorRoom>();

        /// <summary>
        /// 每日对局次数
        /// </summary>
        [ProtoMember(6)]
        public int playRoundCount = 0;

        /// <summary>
        /// 已经进行的抽奖次数
        /// </summary>
        [ProtoMember(7)]
        public int readyNumber = 0;

        /// <summary>
        /// 每日抽奖次数
        /// </summary>
        [ProtoMember(8)]
        public int luckDrawCount = 0;

        /// <summary>
        /// 累计红包金额
        /// </summary>
        [ProtoMember(9)]
        public decimal redPacketMoney = 0;

        /// <summary>
        /// 中奖列表
        /// </summary>
        [ProtoMember(10)]
        public List<Winning> winList = new List<Winning>();

        /// <summary>
        /// 是否进行过分享
        /// </summary>
        [ProtoMember(11)]
        public bool hasShare = false;

        /// <summary>
        /// 提现记录
        /// </summary>
        ///  
        [ProtoMember(12)]
        public List<GmQueryTurnRecord> depositList = new List<GmQueryTurnRecord>();
        /// <summary>
        /// 转让记录
        /// </summary>
        [ProtoMember(13)]
        public List<GmQueryTurnRecord> makeoverList = new List<GmQueryTurnRecord>();
        /// <summary>
        /// 扣除记录
        /// </summary>
        [ProtoMember(14)]
        public List<GmQueryTurnRecord> deductList = new List<GmQueryTurnRecord>();

    }
}
