﻿/* ============================================================
* Author:       liubingsheng
* Time:         2016/12/12 15:20:34
* FileName:     PlayerAttribute
* Purpose:      玩家属性
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using ProtoBuf;
using Module;
using Log;
using StaticData;
using System.IO;
using Event;
using System.Data;
using System.Collections;
using ServerLib;
using Redis;

namespace Entity
{
    [ProtoContract]
    public class PlayerAttribute
    {
        public PlayerAttribute()
        { }

        /// <summary>
        /// 玩家等级
        /// </summary>
        [ProtoMember(1)]
        public UInt32 Level { get; set; }

        /// <summary>
        /// 游戏币
        /// </summary>
        [ProtoMember(2)]
        public List<stRoomCardItem> lstRoomCards = new List<stRoomCardItem>();

        /// <summary>
        /// 昵称
        /// </summary>
        [ProtoMember(3)]
        public string NickName { get; set; }

        /// <summary>
        /// 玩家经验
        /// </summary>
        [ProtoMember(4)]
        public UInt32 Exp { get; set; }

        /// <summary>
        /// 玩家性别 0男 1女
        /// </summary>
        [ProtoMember(5)]
        public ERoleSex sex { get; set; }

        /// <summary>
        /// 邀请自己的人
        /// </summary>
        [ProtoMember(6)]
        public UInt64 inviteSelfRoleId = 0;

        /// <summary>
        /// 自己邀请的人
        /// </summary>
        [ProtoMember(7)]
        public HashSet<UInt64> setInviteRoles = new HashSet<UInt64>();

        public stRoomCardItem GetRoomCardItem(ERCSourceType source, UInt64 srcRoleId)
        {
            foreach (var item in lstRoomCards)
            {
                if (item.sourceType == source && item.srcRoleId == srcRoleId)
                {
                    return item;
                }
            }

            return null;
        }
    }
}
