﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using ProtoBuf;
using Module;
using Log;
using StaticData;
using System.IO;
using Event;
using System.Data;
using System.Collections;
using ServerLib;
using Redis;
using Protocols.gm;

namespace Entity
{


    [ProtoContract]
    public class PlayerAgent
    {
        [ProtoMember(1)]
        public UInt64 accountId = 0;

        /// <summary>
        /// 房卡余额
        /// </summary>
        [ProtoMember(2)]
        public int roomCard = 0;

        /// <summary>
        /// 代理累计房卡充值
        /// </summary>
        [ProtoMember(3)]
        public int roomCardRecharge = 0;

        /// <summary>
        /// 代理累计房卡消耗
        /// </summary>
        [ProtoMember(4)]
        public int roomCardConsume = 0;

        /// <summary>
        /// 代理状态(授权/停权)
        /// </summary>
        [ProtoMember(5)]
        public eAgencyState agencyState;

        /// <summary>
        /// 返利的钱
        /// </summary>
        [ProtoMember(6)]
        public double money = 0;

        /// <summary>
        /// 电话
        /// </summary>
        [ProtoMember(7)]
        public string phone = "";

        /// <summary>
        /// 姓名
        /// </summary>
        [ProtoMember(8)]
        public string name = "";

        /// <summary>
        /// 地区
        /// </summary>
        [ProtoMember(9)]
        public string area = "";

        /// <summary>
        /// 一级分成比例
        /// </summary>
        [ProtoMember(10)]
        public double oneLevelProportion = 0;

        /// <summary>
        /// 二级比例分成
        /// </summary>
        [ProtoMember(11)]
        public double twoLevelProportion = 0;

        /// <summary>
        /// 代理历史返利总金额
        /// </summary>
        [ProtoMember(12)]
        public double totalMoney = 0;

        public void TotalMoney(double _money)
        {
            if (totalMoney == 0)
            {
                totalMoney = money;
            }
            totalMoney += _money;
        }

        public void RoomCardRecharheCount(int roomCard)
        {
            roomCardRecharge += roomCard;
        }

        public void RoomCardConsume(int roomCard)
        {
            roomCardConsume += roomCard;
        }

        public void RoomCardReturn(int roomCard)
        {
            roomCardConsume -= roomCard;
        }

        public void Impower()
        {
            agencyState = eAgencyState.imPower;
        }

        public void StopPower()
        {
            agencyState = eAgencyState.rightToStop;
        }
    }
}
