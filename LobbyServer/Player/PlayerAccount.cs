﻿/* ============================================================
* Author:       liubingsheng
* Time:         2016/12/12 15:20:34
* FileName:     PlayerAttribute
* Purpose:      玩家属性
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using ProtoBuf;
using Module;
using Log;
using StaticData;
using System.IO;
using Event;
using System.Data;
using System.Collections;
using ServerLib;
using Redis;

namespace Entity
{
    public enum EAccountType
    {
        Null = 0,
        Player = 1, // 普通玩家
        Gm = 2, // GM后台
        Agent = 3, // 代理账号
    }

    [ProtoContract]
    public class PlayerAccount
    {
        public PlayerAccount()
        { 

        }

        /// <summary>
        /// 账号ID
        /// </summary>
        [ProtoMember(1)]
        public UInt64 AccountId { get; set; }

        /// <summary>
        /// 平台ID
        /// </summary>
        [ProtoMember(2)]
        public EPID PlatformType { get; set; }

        /// <summary>
        /// 账号名
        /// </summary>
        [ProtoMember(3)]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [ProtoMember(4)]
        public string Password { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [ProtoMember(5)]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 账号类型
        /// </summary>
        [ProtoMember(6)]
        public EAccountType accountType = EAccountType.Player;

        /// <summary>
        /// 授权用户唯一标识，普通用户的标识，对当前开发者帐号唯一
        /// </summary>
        [ProtoMember(7)]
        public string weixinOpenid = "";

        /// <summary>
        /// 用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的unionid是唯一的。开发者最好保存unionID信息，以便以后在不同应用之间进行用户信息互通。
        /// </summary>
        [ProtoMember(8)]
        public string weixinUnionid = "";

        /// <summary>
        /// 微信头像url
        /// </summary>
        [ProtoMember(9)]
        public string weixinHeadImgUrl = "";

        /// <summary>
        /// 微信登录临时token
        /// </summary>
        [ProtoMember(10)]
        public string weixinInnerToken = "";

        /// <summary>
        /// 微信刷新token
        /// </summary>
        [ProtoMember(11)]
        public string weixinRefreshToken = "";

        /// <summary>
        /// 微信刷新token过期时间
        /// </summary>
        [ProtoMember(12)]
        public DateTime weixinRefreshTokenExpireTime = DateTime.MinValue;

        /// <summary>
        /// 微信访问token
        /// </summary>
        [ProtoMember(13)]
        public string weixinAccessToken = "";

        /// <summary>
        /// 微信访问token过期时间
        /// </summary>
        [ProtoMember(14)]
        public DateTime weixinAccessTokenExpireTime = DateTime.MinValue; 
    }

    
}
