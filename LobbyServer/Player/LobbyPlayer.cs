﻿/********************************************************************
	created:	2014/12/16
	created:	16:12:2014   9:50
	filename: 	CommonPlatform\Server\ServerInstance\CommonLib\Entity\LobbyPlayer.cs
	file path:	CommonPlatform\Server\ServerInstance\CommonLib\Entity
	file base:	LobbyPlayer
	file ext:	cs
	author:		刘冰生
	
	purpose:    维护账号信息	
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Module;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using ProtoBuf;
using Entity;
using LobbyServer;
using Redis;
using StackExchange.Redis;
using System.Threading;
using Room;
using Protocols.gm;

namespace Entity
{
    public class AccountKey
    {
        public EPID platform;
        public string account;

        public AccountKey(EPID platform, string account)
        {
            this.platform = platform;
            this.account = account;
        }

        public AccountKey(UInt16 platform, string account)
        {
            this.platform = (EPID)platform;
            this.account = account;
        }

        public override bool Equals(object obj)
        {
            AccountKey other = obj as AccountKey;
            if (other != null)
            {
                if (other.account == account && other.platform == platform)
                    return true;
                return false;
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return string.Format("{0}::{1}", platform, account).GetHashCode();
        }

        public override string ToString()
        {
            return account + "_" + platform.ToString();
        }
    }

    [ProtoContract]
    public class LobbyPlayer : Player
    {
        public LobbyPlayer(UInt64 accountId)
        {
            AccountId = accountId;
        }

        public LobbyPlayer(AccountKey key)
        {
            accountKey = key;
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        public UInt32 SessionID
        {
            get;
            set;
        }

        public UInt32 GateAddress { get; set; }

        public UInt32 BattleAddress 
        { 
            get
            {
                GameRoom room = g.roomMgr.GetRoom(this.room.roomUuid);
                if (null == room)
                {
                    return 0;
                }
                return room.battleAddr;
            }
        }

        public string InnerToken { get; set; }

        public UInt64 AccountId { get; set; }

        public string clientIp = "";

        public bool hasGps = false;

        public float x = 0;

        public float y = 0;

        public String address = "";

       
        AccountKey accountKey;
        public AccountKey AccountKey
        {
            get
            {
                if (accountKey == null)
                {
                    accountKey = new AccountKey(Account.PlatformType, Account.Account);
                }
                return accountKey;
            }
        }

        /// <summary>
        /// 是否在线
        /// </summary>
        public bool isOnline = false;

        //////////////////////////////////////////////////////////////////////////////////////////
        // 函数定义
        public void FirstInit(EAccountType accountType)
        {
            if (Account.Account == g.staticData.GM_ACCOUNT)
            {
                accountType = EAccountType.Gm;
            }
           
            Account.accountType = accountType;

            if(IsPlayer)
            {
                AddDiamond(ERCSourceType.System, 0, 9); // 玩家初始化9个房卡
            }

            Attribute.Level = 1;
            Attribute.NickName = Account.Account;

            Account.CreateTime = env.Timer.CurrentDateTime;
            Attribute.sex = ERoleSex.Boy;

            // 浦东玩家才进排行榜
            if(IsPlayer || IsAgent)
            {
                g.rankMgr.UpdateRoleExp(AccountId, Attribute.Exp);
            }

            g.dataCenter.TotalHistoryRoleCount();//统计玩家数量
        }

        /// <summary>
        /// 增加房卡
        /// </summary>
        /// <param name="source"></param>
        /// <param name="srcRoleId"></param>
        /// <param name="roomCardCount"></param>
        public void AddDiamond(ERCSourceType source,  UInt64 srcRoleId, UInt32 roomCardCount)
        {
            stRoomCardItem item = Attribute.GetRoomCardItem(source, srcRoleId);
            if(null == item)
            {
                item = new stRoomCardItem();
                item.sourceType = source;
                item.srcRoleId = srcRoleId;
                item.roomCardCount = roomCardCount;
                Attribute.lstRoomCards.Add(item);
            }
            else
            {
                item.roomCardCount += roomCardCount;
            }

            RoomCardChangeNtf ntf = new RoomCardChangeNtf();
            ntf.diamond = GetDiamondCount();
            SendMsgToClient(ntf);
        }

        /// <summary>
        /// 归还房卡
        /// </summary>
        /// <param name="lstItems"></param>
        public void ReturnDiamonds(List<stRoomCardItem> lstItems)
        {
            foreach(var item in lstItems)
            {
                if(item.roleid == AccountId)
                {
                    AddDiamond(item.sourceType, item.srcRoleId, item.roomCardCount);
                }
            }
            AgentRoomCardConsume(lstItems, false);
        }

        public async void AgentRoomCardConsume(List<stRoomCardItem> lstItems,bool isConsume = true)
        {
            foreach (var item in lstItems)
            {
                if (item.sourceType == ERCSourceType.Agent)
                {
                    LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(item.srcRoleId);
                    if (isConsume)
                    {                    
                        exPlayer.agent.RoomCardConsume((int)item.roomCardCount);
                    }
                    else
                    {
                        exPlayer.agent.RoomCardReturn((int)item.roomCardCount);
                    }
                }
            }
        }

        /// <summary>
        /// 删除房卡
        /// </summary>
        /// <param name="diamond"></param>
        public List<stRoomCardItem> DelDiamond(UInt32 diamond)
        {
            List<stRoomCardItem> lstResults = new List<stRoomCardItem>();

            if(!HasEnoughDiamond(diamond))
            {
                diamond = GetDiamondCount();
            }

            // 优先扣除系统房卡
            while (DelDiamond(ERCSourceType.Mall, ref diamond, lstResults)) ;
            while (DelDiamond(ERCSourceType.System, ref diamond, lstResults));
            while (DelDiamond(ERCSourceType.Gm, ref diamond, lstResults));
            while (DelDiamond(ERCSourceType.Agent, ref diamond, lstResults));          
            AgentRoomCardConsume(lstResults, true);

            RoomCardChangeNtf ntf = new RoomCardChangeNtf();
            ntf.diamond = GetDiamondCount();
            SendMsgToClient(ntf);
            foreach(stRoomCardItem item in lstResults)
            {
                item.roleid = AccountId;
            }
            return lstResults;
        }

        /// <summary>
        /// 扣除房卡
        /// </summary>
        /// <param name="source"></param>
        /// <param name="diamond"></param>
        /// <param name="lstResults"></param>
        bool DelDiamond(ERCSourceType source, ref UInt32 diamond, List<stRoomCardItem> lstResults)
        {
            if(diamond == 0)
            {
                return false;
            }

            stRoomCardItem curItem = null;

            foreach (var item in Attribute.lstRoomCards)
            {
                if(source != item.sourceType)
                {
                    continue;
                }

                curItem = item;
                break;
            }

            if(curItem == null)
            {
                return false;
            }

            if (curItem.roomCardCount <= diamond)
            {
                lstResults.Add(curItem);
                Attribute.lstRoomCards.Remove(curItem);
                diamond -= curItem.roomCardCount;
                return true;
            }

            stRoomCardItem resultItem = new stRoomCardItem();
            resultItem.sourceType = source;
            resultItem.srcRoleId = curItem.srcRoleId;
            resultItem.roomCardCount = diamond;
            lstResults.Add(resultItem);

            curItem.roomCardCount -= diamond;
            diamond = 0;

            return false;
        }

        /// <summary>
        /// 获得玩家信息
        /// </summary>
        /// <param name="roleInfo"></param>
        public void GetRoleInfo(stRoleItem roleInfo)
        {
            roleInfo.roleid = Account.AccountId;
            roleInfo.nickName = Attribute.NickName;
            roleInfo.sex = Attribute.sex;
            roleInfo.level = Attribute.Level;
            roleInfo.clientIp = clientIp;
            roleInfo.weixinHeadImgUrl = Account.weixinHeadImgUrl;
            roleInfo.isOnline = isOnline;
        }

        /// <summary>
        /// 是否有足够的房卡
        /// </summary>
        /// <param name="needCount"></param>
        /// <returns></returns>
        public bool HasEnoughDiamond(UInt32 needCount)
        {
            return GetDiamondCount() >= needCount;
        }

        public UInt32 GetDiamondCount()
        {
            UInt32 count = 0;
            foreach (var item in Attribute.lstRoomCards)
            {
                count += item.roomCardCount;
            }
            return count;
        }

        /// <summary>
        /// 上线
        /// </summary>
        public void Online()
        {
            isOnline = true;

            // 统计活跃人数
            if (CheckIsFirstLogin())
            {
                g.dataCenter.IncActiveRoleCount();
            }
        }

        public HashSet<string> LoginTime = new HashSet<string>();
        public bool CheckIsFirstLogin()
        {         
            return LoginTime.Add(DateTime.Now.ToString("yyyyMMdd"));              
        }

        /// <summary>
        /// 下线
        /// </summary>
        public void Offline()
        {
            isOnline = false;

            SessionID = 0;
            GateAddress = 0;

            PlayerOfflineNtf ntf = new PlayerOfflineNtf();
            SendMsgToBattle(ntf);
        }

        public void SendMsgToClient(Protocol proto)
        {
            proto.SessionId = SessionID;
            proto.RoleId = AccountId;

            if (GateAddress == 0)
            {
                return;
            }

            env.BusService.SendMessageToServer(GateAddress, proto);
        }

        public void SendMsgToBattle(Protocol proto)
        {
            proto.SessionId = SessionID;
            proto.RoleId = AccountId;

            GameRoom room = g.roomMgr.GetRoom(this.room.roomUuid);
            if(null == room)
            {
                return;
            }
            env.BusService.SendMessageToServer(room.battleAddr, proto);
        }

        public bool Save()
        {
            byte[] data; 
            if(!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;
            redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);
            return true;
        }

        public async Task<bool> LoadFromDbAsync()
        {
            RedisSystem redis = env.Redis;

            if(AccountId == 0)
            {
                RedisValue value = await redis.db.StringGetAsync(dbAccountKey);

                if (value.IsNullOrEmpty)
                {
                    return false;
                }

                AccountId = (UInt64)value;
            }

            {
                RedisValue value = await redis.db.StringGetAsync(dbKey);
                if (value.IsNullOrEmpty)
                {
                    return false;
                }

                if (!this.SerializeFromBinary(value))
                {
                    return false;
                }
            }

            return true;
        }

        public string dbAccountKey
        {
            get
            {
                string key = env.Redis.MergeKey("accountkey", AccountKey.ToString());
                return key;
            }
        }

        public string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey("account", AccountId.ToString());
                return key;
            }
        }

        public void AddExp(UInt32 exp)
        {
            Attribute.Exp += exp;

            // 经验排行榜更新
            if (IsPlayer || IsAgent)
            {
                g.rankMgr.UpdateRoleExp(AccountId, Attribute.Exp);
            }

            RoleExpChangeNtf expNtf = new RoleExpChangeNtf();
            expNtf.curExp = Attribute.Exp;
            SendMsgToClient(expNtf);

            if (!g.staticData.levelData.HasLevel(Attribute.Level))
            {
                return;
            }

            stLevelItem item = g.staticData.levelData.GetLevelItem(Attribute.Level);

            if (Attribute.Exp < item.levelupExp)
            {
                return;
            }

            UInt32 newlevel = Attribute.Level + 1;
            if (!g.staticData.levelData.HasLevel(newlevel))
            {
                return;
            }

            stLevelItem newLevelItem = g.staticData.levelData.GetLevelItem(newlevel);

            Attribute.Level = newlevel;

            RoleLevelChangeNtf levelNtf = new RoleLevelChangeNtf();
            levelNtf.level = newlevel;
            levelNtf.title = newLevelItem.title;
            levelNtf.levelupExp = newLevelItem.levelupExp;
            SendMsgToClient(levelNtf);
        }

        /// <summary>
        /// 添加邀请人
        /// </summary>
        /// <param name="roleid"></param>
        public void AddInviteRole(UInt64 roleid)
        {
            UInt32 oldLevel = g.staticData.invitData.GetLevel((UInt32)Attribute.setInviteRoles.Count);

            Attribute.setInviteRoles.Add(roleid);

            UInt32 newLevel = g.staticData.invitData.GetLevel((UInt32)Attribute.setInviteRoles.Count);

            if(newLevel == (oldLevel+1) || newLevel == 0)
            {
                // 发奖励
                stInvitItem item = g.staticData.invitData.GetLevelItem(oldLevel);
                if(null != item)
                {
                    AddDiamond(ERCSourceType.System, 0, item.roomcard);
                }
            }

            // 获取邀请人相关信息
            InviteRoleCountChangeNtf ntf = new InviteRoleCountChangeNtf();
            ntf.inviteRoleCount = (UInt32)Attribute.setInviteRoles.Count;
            stInvitItem invitItem = g.staticData.invitData.GetLevelItem(newLevel);
            if (null != invitItem)
            {
                ntf.nextInviteRoleCount = invitItem.inviteCount;
                ntf.nextInviteAwardRoomCard = invitItem.roomcard;
            }

            SendMsgToClient(ntf);
        }

        /// <summary>
        /// 当玩家充值时,两级分销 邀请的人充值返利10%，A邀请B，B邀请C，那么C充值100，B返利10，A返利1
        /// </summary>
        /// <param name="rmb">充值人民币数量</param>
        /// <param name="CRoomCard">充值获取房卡数量</param>
        public async void OnRoleRecharge(UInt32 rmb, UInt32 CRoomCard)
        {
            g.dataCenter.AddRecharge((int)rmb);
            g.dataCenter.RecordUserRechargeData(AccountId, Attribute.NickName, rmb);      
            AddDiamond(ERCSourceType.System, 0, CRoomCard);
            OnRoleRechargeRebate(rmb);

            g.dataCenter.TotalHistoryRechargeRoomCardCount((int)CRoomCard);

            if (IsAgent)
            {
                g.agentMgr.RecordRCrechargeEvent(AccountId, 0, CRoomCard);
            }

            if (CRoomCard < 10)
            {
                return;
            }

            LobbyPlayer playerB = await g.playerMgr.FindPlayerAsync(Attribute.inviteSelfRoleId);
            if(null == playerB)
            {
                return;
            }

            UInt32 BRoomCard = CRoomCard / 10;
            playerB.AddDiamond(ERCSourceType.System, 0, BRoomCard);

            if(BRoomCard < 10)
            {
                return;
            }

            LobbyPlayer playerA = await g.playerMgr.FindPlayerAsync(playerB.Attribute.inviteSelfRoleId);
            if (null == playerA)
            {
                return;
            }

            UInt32 ARoomCard = BRoomCard / 10;
            playerA.AddDiamond(ERCSourceType.System, 0, ARoomCard);
        }

        /// <summary>
        /// rmb代理返利:
        /// 当玩家充值时,两级分销 ,A邀请B，B邀请C，那么C充值100 ;  
        ///  B是代理 返利10,  A是代理 返利5 ;  B不是代理 返利0,  A是代理,返利5; B是代理 返利10, A不是代理,返利0
        /// </summary>
        /// <param name="money"></param>
        public async void OnRoleRechargeRebate(double money)
        {
            LobbyPlayer playerB = await g.playerMgr.FindPlayerAsync(Attribute.inviteSelfRoleId);
            if (playerB == null)
            {
                return;
            }

            if (playerB.IsAgent && playerB.agent.agencyState == eAgencyState.imPower)
            {
                double _money = Math.Round((money * playerB.agent.oneLevelProportion), 2);
                playerB.agent.TotalMoney(_money);
                playerB.agent.money += _money;  
                g.agentMgr.RecordRCEventRebate(playerB.AccountId, AccountId, _money,playerB.Attribute.NickName);
                playerB.Save();
            }

            LobbyPlayer playerA = await g.playerMgr.FindPlayerAsync(playerB.Attribute.inviteSelfRoleId);
            if (playerA == null)
            {
                return;
            }

            if (playerA.IsAgent && playerA.agent.agencyState == eAgencyState.imPower)
            {
                double _money = Math.Round((money * playerB.agent.twoLevelProportion), 2);
                playerA.agent.TotalMoney(_money);
                playerA.agent.money += _money;                 
                g.agentMgr.RecordRCEventRebate(playerA.AccountId, AccountId, _money,playerA.Attribute.NickName);
                playerA.Save();
            }
        }

        public bool IsGm
        {
            get
            {
                return Account.accountType == EAccountType.Gm;
            }
        }

        public bool IsAgent
        {
            get
            {
                return Account.accountType == EAccountType.Agent;
            }
        }

        public bool IsPlayer
        {
            get
            {
                return Account.accountType == EAccountType.Player;
            }
        }

        /// <summary>
        /// 是否暂停
        /// </summary>
        public bool IsNull
        {
            get
            {
                return Account.accountType == EAccountType.Null;
            }
        }

        /// <summary>
        /// 在战斗服上创建 
        /// </summary>
        public void CreateOnBattle()
        {
            CreatePlayerOnBattleL2B msg = new CreatePlayerOnBattleL2B();
            msg.roleInfo.roleid = AccountId;
            msg.roleInfo.nickName = Attribute.NickName;
            msg.roleInfo.sex = Attribute.sex;
            msg.roleInfo.level = Attribute.Level;
            msg.roleInfo.gateAddr = GateAddress;
            msg.roleInfo.sessionId = SessionID;
            msg.roleInfo.clientIp = clientIp;
            msg.roleInfo.weixinHeadImgUrl = Account.weixinHeadImgUrl;
            msg.roleInfo.roomUuid = room.roomUuid;
            SendMsgToBattle(msg);
        }

        public void ClearRoomData(UInt64 roomUuid)
        {
            if(roomUuid == room.roomUuid)
            {
                room.roomUuid = 0;
            }
            
            if(room.createRoomUuid == roomUuid)
            {
                room.createRoomUuid = 0;
            }
        }

        /// <summary>
        /// 踢玩家下线
        /// </summary>
        public void KickOffline(KickReason reason)
        {
            KickAccountReq req = new KickAccountReq();
            req.platform = accountKey.platform;
            req.account = accountKey.account;
            req.reason = reason;
            SendMsgToClient(req);
        }
    }
}
