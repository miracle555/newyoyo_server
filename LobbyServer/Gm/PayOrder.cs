﻿using System;
using System.Text;
using ProtoBuf;

namespace Statistics
{
    [ProtoContract]
    public class PayOrder
    {
        /// <summary>
        /// 应用订单号
        /// </summary>
        [ProtoMember(1)]
        public string out_trade_no = string.Empty;

        /// <summary>
        /// 充值id
        /// </summary>
        [ProtoMember(2)]
        public uint recharge_id = 0;

        /// <summary>
        /// 充值表中的价格
        /// </summary>
        [ProtoMember(3)]
        public long recharge_price = 0;

        /// <summary>
        /// 微信预支付会话标识,用于后续接口调用中使用,该值有效期为2小时
        /// </summary>
        [ProtoMember(4)]
        public string prepay_id = string.Empty;

        /// <summary>
        /// 订单创建时间
        /// </summary>
        [ProtoMember(5)]
        public string create_date = string.Empty;

        /// <summary>
        /// 支付玩家id
        /// </summary>
        [ProtoMember(6)]
        public ulong playerId = 0;

        /// <summary>
        /// 支付玩家名字
        /// </summary>
        [ProtoMember(7)]
        public string playerName = string.Empty;

        /// <summary>
        /// 支付金额
        /// </summary>
        [ProtoMember(8)]
        public string total_fee = string.Empty;

        /// <summary>
        /// 支付完成时间
        /// </summary>
        [ProtoMember(9)]
        public string complete_date = string.Empty;

        /// <summary>
        /// 微信支付订单号
        /// </summary>
        [ProtoMember(10)]
        public string transaction_id = string.Empty;

        /// <summary>
        /// 订单状态.0:创建订单;1:支付成功;2:支付失败;3:添加物品成功;4:添加物品失败
        /// </summary>
        [ProtoMember(11)]
        public byte order_status = 0;
    }
}