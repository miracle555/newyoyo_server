﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using MahjongPushDown;
using MahjongPoint;
using Landlord;
using Entity;
using LobbyServer;

namespace Room
{
    public class AgentMgr
    {
        public void AddAgent(UInt64 roleid)
        {
            env.Redis.SetAdd(dbKey, roleid);
        }

        public void RemoveAgent(UInt64 roleid)
        {
            env.Redis.SetRemove(dbKey, roleid);
        }

        public string dbKey
        {
            get
            {
                return RedisName.all_agents;
            }
        }

        public async Task<List<UInt64>> GetAgentListAsync()
        {
            List<UInt64> agentListData = new List<UInt64>();

            RedisValue[] value = await env.Redis.db.SetMembersAsync(dbKey);

            if (value.Length > 0)
            {
                foreach (var item in value)
                {
                    UInt64 accountId = (UInt64)item;
                    agentListData.Add(accountId);
                }
            }
            return agentListData;
        }

        /// <summary>
        /// 创建房间事件
        /// </summary>
        /// <param name="roleid"></param>
        /// <param name="gameType"></param>
        /// <param name="roomRound"></param>
        /// <param name="lstRoomCards"></param>
        public async void RecordRCConsumeEvent(UInt64 roleid, EGameType gameType, int roomRound, List<stRoomCardItem> lstRoomCards)
        {
            foreach (var item in lstRoomCards)
            {
                if (item.sourceType != ERCSourceType.Agent)
                {
                    continue;
                }

                LobbyPlayer player = await g.playerMgr.FindPlayerAsync(item.srcRoleId);
                if (null == player)
                {
                    continue;
                }

                if (!player.IsAgent)
                {
                    continue;
                }

                stRCEventItem eventItem = new stRCEventItem();
                eventItem.dateTime = env.Timer.CurrentDateTime;
                eventItem.roleid = roleid;
                eventItem.eventType = ERoomCardEventType.Consume;
                eventItem.gameType = gameType;
                eventItem.roomRound = roomRound;
                eventItem.roomCardCount = item.roomCardCount;
                eventItem.name = player.Attribute.NickName;
                eventItem.agentId = item.srcRoleId;

                string dbkey = agentRCEventDbKey(item.srcRoleId, env.Timer.CurrentDateTime.Date);

                byte[] data;
                if (!eventItem.SerializeToBinary(out data))
                {
                    continue;
                }

                env.Redis.db.SetAdd(dbkey, data, CommandFlags.FireAndForget);
            }
        }

        /// <summary>
        /// 房卡转入事件
        /// </summary>
        /// <param name="agentRoleId"></param>
        /// <param name="roleid"></param>
        /// <param name="roomCardCount"></param>
        public async void RecordRCAddEvent(UInt64 agentRoleId, UInt64 roleid, UInt32 roomCardCount)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(agentRoleId);
            if (null == player)
            {
                return;
            }

            if (!player.IsAgent)
            {
                return;
            }

            stRCEventItem eventItem = new stRCEventItem();
            eventItem.dateTime = env.Timer.CurrentDateTime;
            eventItem.roleid = roleid;
            eventItem.eventType = ERoomCardEventType.Add;
            eventItem.roomCardCount = roomCardCount;
            eventItem.name = player.Attribute.NickName;
            eventItem.agentId = agentRoleId;

            string dbkey = agentRCEventDbKey(agentRoleId, env.Timer.CurrentDateTime.Date);

            byte[] data;
            if (!eventItem.SerializeToBinary(out data))
            {
                return;
            }

            env.Redis.db.SetAdd(dbkey, data, CommandFlags.FireAndForget);
        }

        /// <summary>
        /// 房卡转出事件
        /// </summary>
        /// <param name="agentRoleId"></param>
        /// <param name="roleid"></param>
        /// <param name="roomCardCount"></param>
        public async void RecordRCDelEvent(UInt64 agentRoleId, UInt64 roleid, UInt32 roomCardCount)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(agentRoleId);
            if (null == player)
            {
                return;
            }

            if (!player.IsAgent)
            {
                return;
            }

            stRCEventItem eventItem = new stRCEventItem();
            eventItem.dateTime = env.Timer.CurrentDateTime;
            eventItem.roleid = roleid;
            eventItem.eventType = ERoomCardEventType.Del;
            eventItem.roomCardCount = roomCardCount;
            eventItem.name = player.Attribute.NickName;
            eventItem.agentId = agentRoleId;

            string dbkey = agentRCEventDbKey(agentRoleId, env.Timer.CurrentDateTime.Date);

            byte[] data;
            if (!eventItem.SerializeToBinary(out data))
            {
                return;
            }

            env.Redis.db.SetAdd(dbkey, data, CommandFlags.FireAndForget);
        }

        /// <summary>
        /// 退卡
        /// </summary>
        /// <param name="agentRoleId"></param>
        /// <param name="roleid"></param>
        /// <param name="roomCardCount"></param>
        public async void RecordRCReturnEvent(UInt64 agentRoleId, UInt64 roleid,UInt32 roomCardCount)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(agentRoleId);
            if (null == player)
            {
                return;
            }

            if (!player.IsAgent)
            {
                return;
            }

            stRCEventItem eventItem = new stRCEventItem();
            eventItem.dateTime = env.Timer.CurrentDateTime;
            eventItem.roleid = roleid;
            eventItem.eventType = ERoomCardEventType.Return;
            eventItem.roomCardCount = roomCardCount;
            eventItem.name = player.Attribute.NickName;
            eventItem.agentId = agentRoleId;

            string dbkey = agentRCEventDbKey(agentRoleId, env.Timer.CurrentDateTime.Date);

            byte[] data;
            if (!eventItem.SerializeToBinary(out data))
            {
                return;
            }

            env.Redis.db.SetAdd(dbkey, data, CommandFlags.FireAndForget);
        }

        /// <summary>
        /// 充值
        /// </summary>
        /// <param name="agentRoleId"></param>
        /// <param name="roleid"></param>
        /// <param name="roomCardCount"></param>
        public async void RecordRCrechargeEvent(UInt64 agentRoleId, UInt64 roleid, UInt32 roomCardCount)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(agentRoleId);
            if (null == player)
            {
                return;
            }

            if (!player.IsAgent)
            {
                return;
            }

            stRCEventItem eventItem = new stRCEventItem();
            eventItem.dateTime = env.Timer.CurrentDateTime;
            eventItem.roleid = roleid;
            eventItem.eventType = ERoomCardEventType.recharge;
            eventItem.roomCardCount = roomCardCount;
            eventItem.name = player.Attribute.NickName;
            eventItem.agentId = agentRoleId;

            string dbkey = agentRCEventDbKey(agentRoleId, env.Timer.CurrentDateTime.Date);

            byte[] data;
            if (!eventItem.SerializeToBinary(out data))
            {
                return;
            }

            env.Redis.db.SetAdd(dbkey, data, CommandFlags.FireAndForget);
        }


        string agentRCEventDbKey(UInt64 agentRoleId, DateTime date)
        {
            string strDate = string.Format("{0:yyyyMMdd}", date);

            return agentRCEventDbKey(agentRoleId, strDate);
        }

        string agentRCEventDbKey(UInt64 agentRoleId, string strDate)
        {
            return "agent_rc_event_" + agentRoleId + "_" + strDate;
        }

        public async Task<List<stRCEventItem>> GetRCEventsAsync(UInt64 agentRoleId, string strDate)
        {
            List<stRCEventItem> lstResults = new List<stRCEventItem>();

            string dbkey = agentRCEventDbKey(agentRoleId, strDate);

            RedisValue[] values = await env.Redis.db.SetMembersAsync(dbkey);
            foreach (var item in values)
            {
                byte[] data = (byte[])item;

                stRCEventItem eventItem = new stRCEventItem();

                if (!eventItem.SerializeFromBinary(data))
                {
                    continue;
                }

                lstResults.Add(eventItem);
            }

            return lstResults;
        }

        string agentRCEventRebateDbKey(UInt64 agentRoleId, DateTime date)
        {
            string strDate = string.Format("{0:yyyyMMdd}", date);

            return agentRCEventRebateDbKey(agentRoleId, strDate);
        }
        string agentRCEventRebateDbKey(UInt64 agentRoleId, string strDate)
        {
            return "agent_rc_event_rebate_" + agentRoleId + "_" + strDate;
        }

        public async Task<List<stRCEventRebateItem>> GetRCEventsRebateAsync(UInt64 agentRoleId,string strDate)
        {
            List<stRCEventRebateItem> lstResults = new List<stRCEventRebateItem>();

            string dbkeyRebate = agentRCEventRebateDbKey(agentRoleId, strDate);

            RedisValue[] values = await env.Redis.db.SetMembersAsync(dbkeyRebate);
            foreach (var item in values)
            {
                byte[] data = (byte[])item;

                stRCEventRebateItem eventItem = new stRCEventRebateItem();
                if (!eventItem.SerializeFromBinary(data))
                {
                    continue;
                }
                lstResults.Add(eventItem);
            }
            return lstResults;
        }

        /// <summary>
        /// 返利事件
        /// </summary>
        /// <param name="agentRoleId"></param>
        /// <param name="roleid"></param>
        /// <param name="money"></param>
        public async void RecordRCEventRebate(UInt64 agentRoleId, UInt64 roleid, double money,string name)
        {
            LobbyPlayer agentplayer = await g.playerMgr.FindPlayerAsync(agentRoleId);

            if (agentplayer == null)
            {
                return;
            }

            if (!agentplayer.IsAgent)
            {
                return;
            }

            stRCEventRebateItem eventItem = new stRCEventRebateItem();
            eventItem.dateTime = env.Timer.CurrentDateTime;
            eventItem.eventType = ERebateEventTyp.Rebate;
            eventItem.money = money;
            eventItem.roleid = roleid;
            eventItem.name = name;


            string dbkey = agentRCEventRebateDbKey(agentRoleId, env.Timer.CurrentDateTime.Date);

            byte[] data;
            if (!eventItem.SerializeToBinary(out data))
            {
                return;
            }

            env.Redis.db.SetAdd(dbkey, data, CommandFlags.FireAndForget);
        }

        /// <summary>
        /// 提现事件
        /// </summary>
        /// <param name="agentRoleId"></param>
        /// <param name="money"></param>
        public async void RecorDRCEventWithDraw(UInt64 agentRoleId,double money)
        {
            LobbyPlayer agentplayer = await g.playerMgr.FindPlayerAsync(agentRoleId);

            if (agentplayer == null)
            {
                return;
            }

            if (!agentplayer.IsAgent)
            {
                return;
            }

            stRCEventRebateItem eventItem = new stRCEventRebateItem();
            eventItem.dateTime = env.Timer.CurrentDateTime;
            eventItem.eventType = ERebateEventTyp.WithDraw;
            eventItem.money = money;
            eventItem.roleid = agentRoleId;

            string dbkey = agentRCEventRebateDbKey(agentRoleId, env.Timer.CurrentDateTime.Date);

            byte[] data;
            if (!eventItem.SerializeToBinary(out data))
            {
                return;
            }

            env.Redis.db.SetAdd(dbkey, data, CommandFlags.FireAndForget);
        }
    }
}
