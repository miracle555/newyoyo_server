﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Protocols.gm;
using LobbyServer;

namespace Statistics
{
    [ProtoContract]
    class sOneDayData
    {
        [ProtoMember(1)]
        public DateTime date = DateTime.MinValue;

        // <summary>
        /// 当日活跃人数
        /// </summary>
        [ProtoMember(2)]
        public int dayActiveRoleCount = 0;

        /// <summary>
        /// 当日充值金额
        /// </summary>
        [ProtoMember(3)]
        public int dayRecharge = 0;

        /// <summary>
        /// 当日房卡消耗数量
        /// </summary>
        [ProtoMember(4)]
        public int dayRoomCardConsume = 0;

        /// <summary>
        /// 当日新增玩家数
        /// </summary>
        [ProtoMember(5)]
        public int dayNewAddRoleCount = 0;

        /// <summary>
        /// 当日最高同时在线
        /// </summary>
        [ProtoMember(6)]
        public int dayMaxRoleOnlineCount = 0;
    }

    /// <summary>
    /// 历史数据
    /// </summary>
    [ProtoContract]
    class sHistoryData
    {
        /// <summary>
        /// 累计充值金额
        /// </summary>
        [ProtoMember(1)]
        public int totalRecharge = 0;

        /// <summary>
        /// 累计房卡消耗数量
        /// </summary>
        [ProtoMember(2)]
        public int totalRoomCardConsume = 0;

        /// <summary>
        /// 注册玩家总数
        /// </summary>
        [ProtoMember(3)]
        public int totalRole = 0;

        /// <summary>
        /// 历史微信充值房卡总数
        /// </summary>
        [ProtoMember(4)]
        public int totalWeiXinRechargeRoomCardCount = 0;

        /// <summary>
        /// 历史房卡消耗总数
        /// </summary>
        [ProtoMember(5)]
        public int totalCreateRoom = 0;

    }

    [ProtoContract]
    public class DataCenter
    {
        /// <summary>
        /// 每日数据
        /// </summary>
        [ProtoMember(1)]
        Dictionary<DateTime, sOneDayData> mapDayDatas = new Dictionary<DateTime, sOneDayData>();

        /// <summary>
        /// 历史数据
        /// </summary>
        [ProtoMember(2)]
        sHistoryData historyData = new sHistoryData();

        /// <summary>
        /// 用户充值数据
        /// </summary>
        [ProtoMember(3)]
        List<stUserRechargeData> lstUserRechargeData = new List<stUserRechargeData>();
   

        public void RecordUserRechargeData(UInt64 roleId,string name,UInt32 money)
        {
            stUserRechargeData data = new stUserRechargeData();
            data.dateTime = env.Timer.CurrentDateTime;
            data.roleId = roleId;
            data.name = name;
            data.money = money;
            lstUserRechargeData.Add(data);

            AddRecharge((int)money);
            SaveDb();
        }

        /// <summary>
        /// 获取第几页50条数据
        /// </summary>
        /// <param name="data"></param>
        /// <param name="page"></param>
        public void GetLstUserRechargeData(List<stUserRechargeData> data, int page )
        {
            int num = (page - 1) * ConstValue.ONE_PAGE_COUNT;
            for (int i = num; i < num + ConstValue.ONE_PAGE_COUNT; i++)
            {
                if (i > lstUserRechargeData.Count - 1)
                {
                    return;
                }
                data.Add(lstUserRechargeData[i]);
            }
        }

        /// <summary>
        /// 获取总页数
        /// </summary>
        /// <returns></returns>
        public int GetPageCount()
        {
            int count = lstUserRechargeData.Count / ConstValue.ONE_PAGE_COUNT;
            int yushu = lstUserRechargeData.Count % ConstValue.ONE_PAGE_COUNT;
            if (yushu == 0)
            {
                return count;
            }
            else
            {
                return count + 1;
            }
        }

        public void GetLstUserRechargeData(List<stUserRechargeData> data,string strDate, UInt64 accountId)
        {
            foreach (var item in lstUserRechargeData)
            {
                if (item.roleId == accountId)
                {
                    string date = string.Format("{0:yyyyMMdd}", item.dateTime);
                    if (date == strDate)
                    {
                        data.Add(item);
                    }
                }
            }
        }

        /// <summary>
        /// 增加活跃人数
        /// </summary>
        public void IncActiveRoleCount()
        {
            sOneDayData item = GetCurrentDayData();
            item.dayActiveRoleCount++;
            if (item.dayNewAddRoleCount > item.dayMaxRoleOnlineCount)
            {
                item.dayMaxRoleOnlineCount = item.dayActiveRoleCount;
            }
            SaveDb();
        }

        public void GetAgentData()
        { }

        /// <summary>
        /// 统计历史玩家数量
        /// </summary>
        public void TotalHistoryRoleCount()
        {
            historyData.totalRole++;
            SaveDb();
        }

        /// <summary>
        /// 统计历史微信充值房卡总数
        /// </summary>
        /// <param name="roomCard"></param>
        public void TotalHistoryRechargeRoomCardCount(int roomCard)
        {
            historyData.totalWeiXinRechargeRoomCardCount += roomCard;
            SaveDb();
        }

        /// <summary>
        /// 统计历史创建房间数
        /// </summary>
        public void TotalHistoryCreateRoomCount()
        {
            historyData.totalCreateRoom++;
            SaveDb();
        }

        public void TotalRoomCardConsume(int roomCard)
        {
            historyData.totalRoomCardConsume += roomCard;
            SaveDb();
        }

        /// <summary>
        /// 当日新增玩家数
        /// </summary>
        public void NewAddRoleCount()
        {
            sOneDayData item = GetCurrentDayData();
            item.dayNewAddRoleCount++;
            SaveDb();
        }

        /// <summary>
        /// 增加充值数量
        /// </summary>
        /// <param name="rmb"></param>
        public void AddRecharge(int rmb)
        {
            sOneDayData item = GetCurrentDayData();
            item.dayRecharge += rmb;

            historyData.totalRecharge += rmb;

            SaveDb();
        }

        /// <summary>
        /// 增加房卡消耗数量
        /// </summary>
        /// <param name="cardcount"></param>
        public void AddRoomCardConsumeCount(int cardcount)
        {
            sOneDayData item = GetCurrentDayData();
            item.dayRoomCardConsume += cardcount;

            historyData.totalRoomCardConsume += cardcount;
            SaveDb();
        }

        sOneDayData GetCurrentDayData()
        {
            DateTime date = env.Timer.CurrentDateTime.Date;

            if (mapDayDatas.ContainsKey(date))
            {
                return mapDayDatas[date];
            }

            sOneDayData item = new sOneDayData();
            item.date = date;
            mapDayDatas[date] = item;
            return item;
        }


        /// <summary>
        /// 获取统计数据
        /// </summary>
        /// <param name="outDay"></param>
        /// <param name="outHistory"></param>
        public void GetStatisticsData(stOneDayData outDay, stHistoryData outHistory)
        {
            // 当日数据
            sOneDayData dayitem = GetCurrentDayData();
            // 当日活跃人数
            outDay.dayActiveRoleCount = dayitem.dayActiveRoleCount;
            // 当日充值金额
            outDay.dayRecharge = dayitem.dayRecharge;
            // 当日房卡消耗数量
            outDay.dayRoomCardConsume = dayitem.dayRoomCardConsume;
            // 当日平均付费金额
            if(dayitem.dayActiveRoleCount != 0)
            {
                outDay.dayAveragePay = Math.Round((double)dayitem.dayRecharge / (double)dayitem.dayActiveRoleCount, 2);
            }
            //当日新增人数
            outDay.dayNewAddRoleCount = dayitem.dayNewAddRoleCount;

            //当日最大在线人数
            outDay.dayMaxRoleOnlineCount = dayitem.dayMaxRoleOnlineCount;

            //历史数据
            outHistory.totalRecharge = historyData.totalRecharge;
            outHistory.totalRoomCardConsume = historyData.totalRoomCardConsume;
            outHistory.totalCreateRoom = historyData.totalCreateRoom;
            outHistory.totalRole = historyData.totalRole;
            outHistory.totalWeiXinRechargeRoomCardCount = historyData.totalWeiXinRechargeRoomCardCount;
        }

        public bool SaveDb()
        {
            byte[] data;
            if (!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;
            redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);

            return true;
        }

        public bool LoadFromDb()
        {
            RedisSystem redis = env.Redis;
            RedisValue value = redis.db.StringGet(dbKey);
            LogSys.Info(".............."+ dbKey + "-----------");
            if (value.IsNullOrEmpty)
            {
                SaveDb();
                return true;
            }

            if (!this.SerializeFromBinary(value))
            {
                return false;
            }

            return true;
        }

        public string dbKey
        {
            get
            {
                string key = RedisName.datacenter;
                return key;
            }
        }
    }
}
