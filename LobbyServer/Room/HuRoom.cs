﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Protocols.MajiangPushDown;
using Protocols.Majiang;
using LobbyServer;
using Entity;
using Room;

namespace MahjongPushDown
{
    [ProtoContract]
    public class HuRoom : GameRoom
    {
        const int MAX_ROLES_COUNT = 4;

        public override bool IsRoomFull()
        {
            return setRoles.Count >= MAX_ROLES_COUNT;
        }

        public override string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.basic_hu_room_, roomUuid.ToString());
                return key;
            }
        }
    }
}
