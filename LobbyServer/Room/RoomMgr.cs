﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using MahjongPushDown;
using MahjongPoint;
using Landlord;
using GoldenFlower;

namespace Room
{
    public class RoomMgr
    {
        public bool LoadFromDb()
        {
            RedisSystem redis = env.Redis;
           
            {
                RedisValue[] values = redis.db.SetMembers(dbKey(EGameType.MJPushDown));
                foreach (var item in values)
                {
                    UInt64 roomuuid = (UInt64)item;
                    GameRoom room = new HuRoom();
                    room.roomUuid = roomuuid;
                    if (!room.LoadFromDb())
                    {
                        return false;
                    }

                    mapRooms.Add(roomuuid, room);
                }
            }

            {
                RedisValue[] values = redis.db.SetMembers(dbKey(EGameType.MJPoint));
                foreach (var item in values)
                {
                    UInt64 roomuuid = (UInt64)item;
                    GameRoom room = new PointRoom();
                    room.roomUuid = roomuuid;
                    if (!room.LoadFromDb())
                    {
                        return false;
                    }

                    mapRooms.Add(roomuuid, room);
                }
            }


            {
                RedisValue[] values = redis.db.SetMembers(dbKey(EGameType.Landlord));
                foreach (var item in values)
                {
                    UInt64 roomuuid = (UInt64)item;
                    GameRoom room = new LordRoom();
                    room.roomUuid = roomuuid;
                    if (!room.LoadFromDb())
                    {
                        return false;
                    }

                    mapRooms.Add(roomuuid, room);
                }
            }
           
            {
                RedisValue[] values = redis.db.SetMembers(dbKey(EGameType.GoldenFlower));
                foreach (var item in values)
                {
                    UInt64 roomuuid = (UInt64)item;
                    GameRoom room = new FlowerRoom();
                    room.roomUuid = roomuuid;                    
                    if (!room.LoadFromDb())
                    {
                        return false;
                    }                 
                    mapRooms.Add(roomuuid, room);
                }
            }


            return true;
        }

        public void AddRoom(GameRoom room)
        {
            if (HasRoom(room.roomUuid))
            {
                return;
            }

            mapRooms.Add(room.roomUuid, room);

            env.Redis.SetAdd(dbKey(room.gameType), room.roomUuid);
        }

        public bool HasRoom(UInt64 roomuuid)
        {
            return mapRooms.ContainsKey(roomuuid);
        }

        public void DeleteRoom(UInt64 roomuuid)
        {
            GameRoom room = GetRoom(roomuuid);
            if (null == room)
            {
                return;
            }

            mapRooms.Remove(roomuuid);

            env.Redis.SetRemove(dbKey(room.gameType), roomuuid);

            room.DeleteFromDb();
        }

        public GameRoom GetRoom(UInt64 roomuuid)
        {
            if (mapRooms.ContainsKey(roomuuid))
            {
                return mapRooms[roomuuid];
            }

            return null;
        }

        public string dbKey(EGameType gameType)
        {
             if(gameType == EGameType.MJPushDown)
             {
                 return RedisName.hu_rooms;
             }
             if(gameType == EGameType.MJPoint)
             {
                 return RedisName.point_rooms;
             }
             if(gameType == EGameType.Landlord)
             {
                 return RedisName.lord_rooms;
             }
              if (gameType == EGameType.GoldenFlower)
            {               
                 return RedisName.flower_rooms;
             }
            return "default";
        }

        /// <summary>
        /// 在线房间数
        /// </summary>
        /// <param name="huRoomCount"></param>
        /// <returns></returns>
        public int onlineRoomCount(ref int huRoomCount, ref int pointRoomCount, ref int lordRoomCount )
        {
            huRoomCount = 0;
            pointRoomCount = 0;
            lordRoomCount = 0;           
            foreach (var item in mapRooms.Values)
            {
                if (item.isOnline)
                {
                    if(item.gameType == EGameType.MJPushDown)
                    {
                        huRoomCount++;
                    }
                    if(item.gameType == EGameType.MJPoint)
                    {
                        pointRoomCount++;
                    }
                    if(item.gameType == EGameType.Landlord)
                    {
                        lordRoomCount++;
                    }
                    
                }
            }

            return huRoomCount + pointRoomCount + lordRoomCount;
        }

        Random random = new Random();

        bool randomRoomUuid(ref UInt64 roomuuid)
        {
            roomuuid = (UInt64)random.Next(100001, 999999);
            if(HasRoom(roomuuid))
            {
                return false;
            }
            return true;
        }

        public UInt64 RandomRoomUuid()
        {
            UInt64 roomuuid = 0;
            while (!randomRoomUuid(ref roomuuid)) ;
            return roomuuid;
        }

        // 所有房间
        Dictionary<UInt64, GameRoom> mapRooms = new Dictionary<UInt64, GameRoom>();
    }
}
