﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Protocols.Majiang;
using LobbyServer;
using Entity;
using Protocols.GoldenFlower;

namespace Room
{
    [ProtoContract]
    [ProtoInclude(100, typeof(MahjongPushDown.HuRoom))]
    [ProtoInclude(101, typeof(MahjongPoint.PointRoom))]
    [ProtoInclude(102, typeof(Landlord.LordRoom))]
    [ProtoInclude(103, typeof(GoldenFlower.FlowerRoom))]
    public abstract class GameRoom
    {
        [ProtoMember(1)]
        public UInt64 roomUuid { get; set; }

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(2)]
        public EGameType gameType { get; set; }

        /// <summary>
        /// 房间主人
        /// </summary>
        [ProtoMember(3)]
        public UInt64 OwnerId { get; set; }

        [ProtoMember(4)]
        public HashSet<UInt64> setRoles = new HashSet<UInt64>();

        [ProtoMember(5)]
        public List<stRoomCardItem> lstRoomCards = new List<stRoomCardItem>();

        /// <summary>
        /// 付款方式
        /// </summary>
        [ProtoMember(6)]
        public Pay pay = Pay.masterPay;

        /// <summary>
        /// 选择局数
        /// </summary>
        [ProtoMember(7)]
        public ERoomRound roomRound = ERoomRound.Null;

        public int GetRoomCardConsume()
        {
            int roomCard = 0;
            foreach (var item in lstRoomCards)
            {
                roomCard += (int)item.roomCardCount;
            }
            return roomCard;
        }

        public void AddRole(UInt64 roleid)
        {
            setRoles.Add(roleid);
        }

        public void RemoveRole(UInt64 roleid)
        {
            setRoles.Remove(roleid);
        }

        public abstract bool IsRoomFull();

        public int roleCount
        {
            get
            {
                return setRoles.Count;
            }
        }

        public bool isOnline
        {
            get
            {
                foreach(var roleid in setRoles)
                {
                    LobbyPlayer player = g.playerMgr.FindPlayer(roleid);
                    if(null != player)
                    {
                        if(player.isOnline)
                        {
                            if(player.room.roomUuid == roomUuid)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// 战斗服地址
        /// </summary>
        public UInt32 battleAddr
        {
            get
            {
                if(_battleAddr == 0)
                {
                    _battleAddr = g.roomBalance.GetMinLoadServer();
                }
                return _battleAddr;
            }
            set
            {
                _battleAddr = value;
            }
        }
        UInt32 _battleAddr = 0;

        public bool SaveToDb()
        {
            byte[] data;
            if (!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;

            redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);
            return true;
        }

        public bool LoadFromDb()
        {
            RedisSystem redis = env.Redis;

            RedisValue value = redis.db.StringGet(dbKey);

            if (value.IsNullOrEmpty)
            {          
                
               return false;
            }
            
            if (!this.SerializeFromBinary(value))
            {
                return false;
            }

            return true;
        }

        public bool DeleteFromDb()
        {
            RedisSystem redis = env.Redis;

            redis.db.KeyDelete(dbKey, CommandFlags.FireAndForget);

            return true;
        }

        public abstract string dbKey { get; }
    }
}
