﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;

namespace LobbyServer
{
    public class RoomBalance
    {
        public Dictionary<UInt32, HashSet<UInt64>> mapServerRoles = new Dictionary<UInt32, HashSet<UInt64>>();

        public void IncRoleCount(UInt32 serverid, UInt64 roleid)
        {
            if(!mapServerRoles.ContainsKey(serverid))
            {
                HashSet<UInt64> setRoles = new HashSet<ulong>();
                setRoles.Add(roleid);

                mapServerRoles.Add(serverid, setRoles);
            }
            else
            {
                mapServerRoles[serverid].Add(roleid);
            }
        }

        public void DecRoleCount(UInt32 serverid, UInt64 roleid)
        {
            if(mapServerRoles.ContainsKey(serverid))
            {
                mapServerRoles[serverid].Remove(roleid);
            }
        }

        /// <summary>
        /// 获得最小负载服务器
        /// </summary>
        /// <returns></returns>
        public UInt32 GetMinLoadServer()
        {
            UInt32 serverid = 0;
            int mincount = 1000000;

            foreach(var item in mapServerRoles)
            {
                if(item.Value.Count < mincount)
                {
                    mincount = item.Value.Count;
                    serverid = item.Key;
                }
            }

            return serverid;
        }

        public void AddBattleServerId(UInt32 serverid)
        {
            if(!mapServerRoles.ContainsKey(serverid))
            {
                HashSet<UInt64> setRoles = new HashSet<ulong>();
                mapServerRoles.Add(serverid, setRoles);
            }
        }
    }
}
