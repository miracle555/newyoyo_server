﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;

using LobbyServer;
using Entity;
using Room;
namespace GoldenFlower
{
    [ProtoContract]
    public class FlowerRoom : GameRoom
    {
        const int MAX_ROLES_COUNT = 5;

        /// <summary>
        /// 房间是否开始
        /// </summary>
        public Boolean start = false;

        public override bool IsRoomFull()
        {
            return setRoles.Count >= MAX_ROLES_COUNT;
        }

        public override string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.basic_flower_room_, roomUuid.ToString());
                return key;
            }
        }
    }
}
