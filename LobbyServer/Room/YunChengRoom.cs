﻿using Redis;
using Room;
using ServerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace MahjongYunCheng
{
    [ProtoContract]
    public class YunChengRoom : GameRoom

    {
        const int MAX_ROLES_COUNT = 4;//最大人数是4
        public override string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.basic_yuncheng_room_, roomUuid.ToString());
                return key;
            }
        }


        public override bool IsRoomFull()
        {
            return setRoles.Count >= MAX_ROLES_COUNT;
        }
    }
}
