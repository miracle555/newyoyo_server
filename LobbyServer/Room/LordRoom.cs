﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Protocols.Landlord;
using LobbyServer;
using Entity;
using Room;

namespace Landlord
{
    [ProtoContract]
    public class LordRoom : GameRoom
    {
        const int MAX_ROLES_COUNT = 3;

        public override bool IsRoomFull()
        {
            return setRoles.Count >= MAX_ROLES_COUNT;
        }

        public override string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.basic_lord_room_, roomUuid.ToString());
                return key;
            }
        }
    }
}
