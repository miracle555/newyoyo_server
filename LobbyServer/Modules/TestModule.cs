﻿/********************************************************************
	purpose:    游戏模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;
using Protocols.gm;

namespace Module
{
    public class TestModule : LogicModule
    {
        public TestModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();
            LogSys.Info("TestModule.OnInit()");

            // 测试添加经验
           // RegisterMsgHandler(MSGID.TestAddExpReq, new MsgHandler<TestAddExpReq>(OnTestAddExpReq));

            //RegisterMsgHandler(MSGID.TestRechargeReq, new MsgHandler<TestRechargeReq>(OnTestRechrgeReq));

            return true;
        }

        /// <summary>
        /// 测试添加经验
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnTestAddExpReq(UInt32 from, UInt64 accountId, TestAddExpReq req)
        {
            LogSys.Warn("[GameModule][OnTestAddExpReq],暂关闭,accountId={0}", accountId);
            return;

            //TestAddExpRsp rsp = new TestAddExpRsp();

            //LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            //if (null == player)
            //{
            //    LogSys.Warn("[GameModule][OnTestAddExpReq],找不到玩家,accountId={0}", accountId);
            //    return;
            //}

            //player.AddExp(req.exp);
            //player.Save();

            //rsp.errorCode = TestAddExpRsp.ErrorID.Success;
            //player.SendMsgToClient(rsp);
        }
    }
}
