﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Protocols.MajiangPoint;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using MahjongPoint;

namespace Module
{
    /// <summary>
    /// 抠点模块
    /// </summary>
    public class MJPointModule : LogicModule
    {
        public MJPointModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("MJPointModule.OnInit()");

            //创建房间
            RegisterMsgHandler(MSGID.MJPointCreateRoomReq, new MsgHandler<MJPointCreateRoomReq>(OnMJPointCreateRoomReq));

            //加入房间
            RegisterMsgHandler(MSGID.MJPointJoinRoomReq, new MsgHandler<MJPointJoinRoomReq>(OnMJPointJoinRoomReq));

            //离开房间
            RegisterMsgHandler(MSGID.MJPointLeaveRoomB2L, new MsgHandler<MJPointLeaveRoomB2L>(OnMJPointLeaveRoomB2L));

            //房间关闭
            RegisterMsgHandler(MSGID.MJPointRoomCloseB2L, new MsgHandler<MJPointRoomCloseB2L>(OnMJPointRoomCloseB2L));

            //获取房间信息
            RegisterMsgHandler(MSGID.MJPointGetRoomInfoReq, new MsgHandler<MJPointGetRoomInfoReq>(OnMJPointGetRoomInfoReq));

            // 抠点赢了一局
            RegisterMsgHandler(MSGID.MJPointWinOneRoundB2L, new MsgHandler<MJPointWinOneRoundB2L>(OnMJPointWinOneRoundB2L));

            // 房间所有人都下线
            RegisterMsgHandler(MSGID.MJPointAllRoleOfflineB2L, new MsgHandler<MJPointAllRoleOfflineB2L>(OnMJPointAllRoleOfflineB2L));

            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointCreateRoomReq(UInt32 from, UInt64 accountId, MJPointCreateRoomReq req)
        {
            MJPointCreateRoomRsp rsp = new MJPointCreateRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMahjongCreateRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 已经创建过房间
            if (player.room.IsAlreadyCreateRoom)
            {
                rsp.errorCode = MJPointCreateRoomRsp.ErrorID.AlreadyCreateRoom;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMahjongCreateRoomReq],已经创建房间,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (player.room.IsInRoom)
            {
                rsp.errorCode = MJPointCreateRoomRsp.ErrorID.AlreadInRoom;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMahjongCreateRoomReq],已经在房间里,accountId={0}", accountId);
                return;
            }

            if (!player.HasEnoughDiamond(req.config.roomCardCount))
            {
                rsp.errorCode = MJPointCreateRoomRsp.ErrorID.RoomCardNotEnough;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnMahjongCreateRoomReq],房卡数量不足,accountId={0}", accountId);
                return;
            }
            List<stRoomCardItem> lstRoomCards = player.DelDiamond(req.config.roomCardCount);

            // 记录房卡消耗事件
            g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.MJPoint, req.config.roomRound, lstRoomCards);

            UInt64 roomuuid = g.roomMgr.RandomRoomUuid();
            player.room.roomUuid = roomuuid;
            player.room.createRoomUuid = roomuuid;
            player.Save();

            PointRoom room = new PointRoom();
            room.lstRoomCards = lstRoomCards;
            room.roomUuid = roomuuid;
            room.gameType = EGameType.MJPoint;
            room.OwnerId = accountId;
            g.roomMgr.AddRoom(room);

            room.AddRole(accountId);

            g.roomBalance.IncRoleCount(room.battleAddr, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            MJPointCreateRoomL2B ntfBattle = new MJPointCreateRoomL2B();
            ntfBattle.roomUuid = roomuuid;
            ntfBattle.config = req.config;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointJoinRoomReq(UInt32 from, UInt64 accountId, MJPointJoinRoomReq req)
        {
            MJPointJoinRoomRsp rsp = new MJPointJoinRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (player.room.IsInRoom)
            {
                rsp.errorCode = MJPointJoinRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomReq],已经在房间里,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.roomMgr.GetRoom(req.roomUuid) as PointRoom;
            if(null == room)
            {
                rsp.errorCode = MJPointJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            if (room.IsRoomFull())
            {
                rsp.errorCode = MJPointJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomReq],房间已满,accountId={0}", accountId);
                return;
            }

            room.AddRole(accountId);

            player.room.roomUuid = req.roomUuid;
            player.Save();

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            MJPointJoinRoomL2B ntfBattle = new MJPointJoinRoomL2B();
            ntfBattle.roomUuid = req.roomUuid;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
        }

        /// <summary>
        /// （抠点）离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointLeaveRoomB2L(UInt32 from, UInt64 accountId, MJPointLeaveRoomB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                return;
            }

            if (player.room.createRoomUuid == req.roomUuid)
            {
                player.room.createRoomUuid = 0;
            }
            if (player.room.roomUuid == req.roomUuid)
            {
                player.room.roomUuid = 0;
            }

            player.Save();

            g.roomBalance.DecRoleCount(player.BattleAddress, accountId);

            PointRoom room = g.roomMgr.GetRoom(req.roomUuid) as PointRoom;
            if (null != room)
            {
                room.RemoveRole(accountId);

                if (room.roleCount == 0)
                {
                    g.roomMgr.DeleteRoom(req.roomUuid);
                }
                else
                {
                    room.SaveToDb();
                }
            }
        }

        /// <summary>
        /// 房间关闭
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointRoomCloseB2L(UInt32 from, UInt64 accountId, MJPointRoomCloseB2L req)
        {
            PointRoom room = g.roomMgr.GetRoom(req.roomUuid) as PointRoom;
            if (null == room)
            {
                return;
            }

            // 归还房主房卡
            if(req.bReturnRoomCard)
            {
                LobbyPlayer roomOwner = await g.playerMgr.FindPlayerAsync(req.roomOwnerId);
                if (null != roomOwner)
                {
                    roomOwner.ReturnDiamonds(room.lstRoomCards);
                }
            }
            else
            {
                g.dataCenter.TotalHistoryCreateRoomCount();
                g.dataCenter.AddRoomCardConsumeCount(room.GetRoomCardConsume());
            }
            
            foreach (var roleid in room.setRoles)
            {
                LobbyPlayer player = await g.playerMgr.FindPlayerAsync(roleid);
                if (null == player)
                {
                    continue;
                }

                // 记录战绩
                if(req.bRecordBattleScore)
                {
                    player.room.AddBattleItem(req.scoreItem);
                }

                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                player.Save();

                g.roomBalance.DecRoleCount(player.BattleAddress, roleid);
            }

            g.roomMgr.DeleteRoom(req.roomUuid);
        }

        /// <summary>
        /// 抠点赢了一局
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointWinOneRoundB2L(UInt32 from, UInt64 accountId, MJPointWinOneRoundB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointWinOneRoundB2L],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.AddExp(g.staticData.levelData.WIN_ONE_ROUND_EXP);

            player.Save();
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointGetRoomInfoReq(UInt32 from, UInt64 accountId, MJPointGetRoomInfoReq req)
        {
            MJPointGetRoomInfoRsp rsp = new MJPointGetRoomInfoRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointGetRoomInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (!player.room.IsInRoom)
            {
                rsp.errorCode = MJPointGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointGetRoomInfoReq],不在房间里,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.roomMgr.GetRoom(player.room.roomUuid) as PointRoom;
            if(null == room)
            {
                rsp.errorCode = MJPointGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            MJPointGetRoomInfoL2B ntfBattle = new MJPointGetRoomInfoL2B();
            ntfBattle.roomUuid = player.room.roomUuid;
            player.SendMsgToBattle(ntfBattle);
        }

        /// <summary>
        /// 所有人下线
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointAllRoleOfflineB2L(UInt32 from, UInt64 accountId, MJPointAllRoleOfflineB2L req)
        {
            PointRoom room = g.roomMgr.GetRoom(req.roomUuid) as PointRoom;

            if(null == room)
            {
                LogSys.Warn("[MJPointModule][OnMJPointAllRoleOfflineB2L],房间不存在,roomUuid={0}", req.roomUuid);
                return;
            }

            room.battleAddr = 0;

            room.SaveToDb();
        }
    }
}
