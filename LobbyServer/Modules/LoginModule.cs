﻿/********************************************************************
	created:	2014/12/16
	created:	16:12:2014   10:00
	filename: 	CommonPlatform\Server\ServerInstance\LobbyServer\Modules\LoginModule.cs
	file path:	CommonPlatform\Server\ServerInstance\LobbyServer\Modules
	file base:	LoginModule
	file ext:	cs
	author:		刘冰生
	
	purpose:    登录模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;
using Room;

namespace Module
{
    public class LoginModule : LogicModule
    {
        public LoginModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            //Register message handler
            RegisterMsgHandler<UserRegisterReq>(OnUserRegisterReq);
            RegisterMsgHandler<UserLoginReq>(OnUserLoginReq);
            RegisterMsgHandler<ReloginReq>(OnReloginReq);
            RegisterMsgHandler<PlayerDataReq>(MSGID.PlayerDataReq, OnPlayerDataReq);
            RegisterMsgHandler<GetPlayerStateReq>(MSGID.GetPlayerStateReq, OnGetPlayerStateReq);

            // 玩家上线
            RegisterMsgHandler<PlayerOnlineNtf>(MSGID.PlayerOnlineNtf, OnPlayerOnline);

            // 玩家下线
            RegisterMsgHandler<PlayerOfflineNtf>(MSGID.PlayerOfflineNtf, OnPlayerOffline);
    
            return true;
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers  内部注册
        async void OnUserRegisterReq(UserRegisterReq req, UInt32 from, UInt32 sessionId)
        {
            LogSys.Info("[LoginModule][OnUserRegisterReq],内部注册, account={0}, password={1}", req.account, req.password);

            UserRegisterRsp rsp = new UserRegisterRsp();

            AccountKey accKey = new AccountKey(EPID.Internal, req.account);

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accKey);
            if (null != player)
            {
                LogSys.Warn("[LoginModule][OnUserRegisterReq],内部注册, 账号已经存在 account={0}, password={1}", req.account, req.password);
                rsp.errorCode = UserRegisterRsp.ErrorID.AlreadyExist;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            // 检查本地内存该账号是否存在
            player = await g.playerMgr.CreateAccountAsync(accKey, req.password, EAccountType.Player);
            if(null == player)
            {
                LogSys.Warn("[LoginModule][OnUserRegisterReq],内部注册, 创建账号失败, account={0}, password={1}", req.account, req.password);
                rsp.errorCode = UserRegisterRsp.ErrorID.Failed;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            rsp.errorCode = UserRegisterRsp.ErrorID.Success;
            SendMsgToClient(from, sessionId, rsp);
        }

        /// <summary>
        /// 玩家登录请求
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="sessionId"></param>
        void OnUserLoginReq(UserLoginReq req, UInt32 from, UInt32 sessionId)
        {
            // 使用账号ID直接登录
            if(req.accountId != 0)
            {
                AccountIdLogin(from, sessionId, req.accountId, req.password);
                return;
            }

            if (req.platformType == EPID.Weixin)
            {
                if (req.weixinCode != "")
                {
                    WeixinCodeLogin(from, sessionId, req.weixinCode);
                }
                else
                {
                    WeixinInnerTokenLogin(from, sessionId, req.weixinInnerToken);
                }
                return;
            }

            if(req.platformType == EPID.Internal)
            {
                InternalLogin(from, sessionId, req.account, req.password);
                return;
            }
        }

        /// <summary>
        /// 账号ID直接登录
        /// </summary>
        /// <param name="from"></param>
        /// <param name="sessionId"></param>
        /// <param name="account"></param>
        /// <param name="password"></param>
        async void AccountIdLogin(UInt32 from, UInt32 sessionId, UInt64 accountId, string password)
        {
            LogSys.Info("[LoginModule][InternalLogin],账号ID直接登录, accountId={0}, password={1}", accountId, password);

            UserLoginRsp rsp = new UserLoginRsp();

            // 检查本地内存该账号是否存在
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                rsp.errorCode = UserLoginRsp.ErrorID.AccountNotExist;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if (password != player.Account.Password)
            {
                // 密码错误
                rsp.errorCode = UserLoginRsp.ErrorID.PassWord_Wrong;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if (player.isOnline)
            {
                // 重复账号登录
                rsp.errorCode = UserLoginRsp.ErrorID.AccountDuplicateLogin;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            // 更新玩家在gate上的会话ID
            player.SessionID = sessionId;
            player.GateAddress = from;
            player.InnerToken = randomOneToken();

            player.Save();

            rsp.errorCode = UserLoginRsp.ErrorID.Success;
            rsp.gateInnerToken = player.InnerToken;
            rsp.accountid = player.AccountId;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 内部登录
        /// </summary>
        /// <param name="from"></param>
        /// <param name="sessionId"></param>
        /// <param name="account"></param>
        /// <param name="password"></param>
        async void InternalLogin(UInt32 from, UInt32 sessionId, string account, string password)
        {
            LogSys.Info("[LoginModule][InternalLogin],内部登录, account={0}, password={1}", account, password);

            UserLoginRsp rsp = new UserLoginRsp();

            AccountKey accKey = new AccountKey(EPID.Internal, account);

            // 检查本地内存该账号是否存在
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accKey);
            if (null == player)
            {
                rsp.errorCode = UserLoginRsp.ErrorID.AccountNotExist;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if (password != player.Account.Password)
            {
                // 密码错误
                rsp.errorCode = UserLoginRsp.ErrorID.PassWord_Wrong;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if (player.isOnline)
            {
                // 重复账号登录
                rsp.errorCode = UserLoginRsp.ErrorID.AccountDuplicateLogin;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            // 更新玩家在gate上的会话ID
            player.SessionID = sessionId;
            player.GateAddress = from;
            player.InnerToken = randomOneToken();

            player.Save();

            rsp.errorCode = UserLoginRsp.ErrorID.Success;
            rsp.gateInnerToken = player.InnerToken;
            rsp.accountid = player.AccountId;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 微信code登录
        /// </summary>
        /// <param name="from"></param>
        /// <param name="sessionId"></param>
        /// <param name="weixinCode"></param>
        async void WeixinCodeLogin(UInt32 from, UInt32 sessionId, string weixinCode)
        {
            LogSys.Info("[LoginModule][WeixinCodeLogin],微信code登录, weixinCode={0}", weixinCode);

            UserLoginRsp rsp = new UserLoginRsp();

            stWeixinCodeItem loginItem = await g.weixin.CodeLoginAsync(weixinCode);
            if (loginItem.errorCode == EWeixinErrorID.Failed)
            {
                LogSys.Warn("[LoginModule][WeixinCodeLogin],微信登录失败1, weixinCode={0}, weixinErrorCode={1}", weixinCode, loginItem.weixinErrorCode);
                rsp.errorCode = UserLoginRsp.ErrorID.Failed;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if(loginItem.errorCode == EWeixinErrorID.InvalidCode || loginItem.errorCode == EWeixinErrorID.CodeExpired)
            {
                LogSys.Warn("[LoginModule][WeixinCodeLogin],微信登录失败2, 接口调用凭证code错误, weixinCode={0}, weixinErrorCode={1}", weixinCode, loginItem.weixinErrorCode);
                rsp.errorCode = UserLoginRsp.ErrorID.weixinCodeWrong;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if (loginItem.errorCode != EWeixinErrorID.Success)
            {
                LogSys.Warn("[LoginModule][WeixinCodeLogin],微信登录失败3, 其他错误, weixinCode={0}, weixinErrorCode={1}", weixinCode, loginItem.weixinErrorCode);
                rsp.errorCode = UserLoginRsp.ErrorID.Failed;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            AccountKey accKey = new AccountKey(EPID.Weixin, loginItem.openid);

            // 检查本地内存该账号是否存在
            LobbyPlayer player = await g.playerMgr.FindOrCreatePlayerAsync(accKey, "");
            if (null == player)
            {
                LogSys.Warn("[LoginModule][WeixinCodeLogin],微信登录创建或加载玩家失败, weixinCode={0}", weixinCode);
                return;
            }

            if (player.isOnline)
            {
                // 重复账号登录
                rsp.errorCode = UserLoginRsp.ErrorID.AccountDuplicateLogin;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            player.Account.weixinOpenid = loginItem.openid;
            if(loginItem.hasUserInfoPermission)
            {
                player.Account.weixinUnionid = loginItem.unionid;

                stWeixinUserInfo userInfo = await g.weixin.UserInfoAsync(loginItem.access_token, loginItem.openid);
                if(userInfo.errorCode != EWeixinErrorID.Success)
                {
                    LogSys.Warn("[LoginModule][WeixinCodeLogin],微信登录失败4, 获取玩家信息失败, weixinCode={0}, weixinErrorCode={1}, loginItem.unionid={2}", weixinCode, userInfo.errorCode, loginItem.unionid);
                    rsp.errorCode = UserLoginRsp.ErrorID.Failed;
                    SendMsgToClient(from, sessionId, rsp);
                    return;
                }

                player.Account.weixinHeadImgUrl = userInfo.headimgurl;

                // 1为男性
                if(userInfo.sex == 1)
                {
                    player.Attribute.sex = ERoleSex.Boy;
                } 
                else if(userInfo.sex == 2)
                {
                    // 2为女性
                    player.Attribute.sex = ERoleSex.Girl;
                }

                player.Attribute.NickName = userInfo.nickname;
            }

            player.Account.weixinInnerToken = randomWeixinInnerToken(player.AccountId);
            player.Account.weixinAccessToken = loginItem.access_token;
            player.Account.weixinAccessTokenExpireTime = env.Timer.CurrentDateTime.AddSeconds(loginItem.expires_in-100);
            player.Account.weixinRefreshToken = loginItem.refresh_token;
            player.Account.weixinRefreshTokenExpireTime = env.Timer.CurrentDateTime.AddSeconds(28 * 24 * 3600);

            // 更新玩家在gate上的会话ID
            player.SessionID = sessionId;
            player.GateAddress = from;
            player.InnerToken = randomOneToken();

            player.Save();

            rsp.errorCode = UserLoginRsp.ErrorID.Success;
            rsp.gateInnerToken = player.InnerToken;
            rsp.weixinInnerToken = player.Account.weixinInnerToken;
            rsp.accountid = player.AccountId;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 微信内部token登录
        /// </summary>
        /// <param name="from"></param>
        /// <param name="sessionId"></param>
        /// <param name="weixinInnerToken"></param>
        async void WeixinInnerTokenLogin(UInt32 from, UInt32 sessionId, string weixinInnerToken)
        {
            LogSys.Info("[LoginModule][WeixinCodeLogin],微信内部token登录, weixinInnerToken={0}", weixinInnerToken);

            UserLoginRsp rsp = new UserLoginRsp();

            string[] strResult = weixinInnerToken.Split(',');
            if(strResult.Length != 2)
            {
                LogSys.Warn("[LoginModule][WeixinInnerTokenLogin],微信登录失败,参数数量不为2, weixinInnerToken={0}", weixinInnerToken);
                rsp.errorCode = UserLoginRsp.ErrorID.weixinInnerTokenWrong;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            string strAccountId = strResult[strResult.Length - 1];
            UInt64 accountId = 0;
            if(!UInt64.TryParse(strAccountId, out accountId))
            {
                LogSys.Warn("[LoginModule][WeixinInnerTokenLogin],微信登录失败,解析账号id失败, weixinInnerToken={0}", weixinInnerToken);
                rsp.errorCode = UserLoginRsp.ErrorID.weixinInnerTokenWrong;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            // 检查本地内存该账号是否存在
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[LoginModule][WeixinInnerTokenLogin],微信登录加载玩家失败, weixinInnerToken={0}", weixinInnerToken);
                rsp.errorCode = UserLoginRsp.ErrorID.weixinInnerTokenWrong;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if (player.isOnline)
            {
                // 重复账号登录
                rsp.errorCode = UserLoginRsp.ErrorID.AccountDuplicateLogin;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            if(player.Account.weixinInnerToken != weixinInnerToken)
            {
                LogSys.Warn("[LoginModule][WeixinInnerTokenLogin],微信登录失败, 内部token不一致, weixinInnerToken={0}, player.Account.weixinInnerToken={1}", weixinInnerToken, player.Account.weixinInnerToken);
                rsp.errorCode = UserLoginRsp.ErrorID.weixinInnerTokenWrong;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            // access_token过期，进行刷新
            if(env.Timer.CurrentDateTime > player.Account.weixinAccessTokenExpireTime)
            {
                // 检查刷新token是否过期
                stWeixinRefreshItem refreshItem = await g.weixin.RefreshAsync(player.Account.weixinRefreshToken);
                if (refreshItem.errorCode == EWeixinErrorID.Failed)
                {
                    LogSys.Warn("[LoginModule][WeixinInnerTokenLogin],微信刷新token失败, weixinInnerToken={0}, weixinErrorCode={1}", weixinInnerToken, refreshItem.errorCode);
                    rsp.errorCode = UserLoginRsp.ErrorID.weixinInnerTokenWrong;
                    SendMsgToClient(from, sessionId, rsp);
                    return;
                }

                if (refreshItem.errorCode == EWeixinErrorID.InvalidRefreshToken || refreshItem.errorCode == EWeixinErrorID.RefreshTokenExpired)
                {
                    LogSys.Warn("[LoginModule][WeixinInnerTokenLogin],微信刷新token失败, refresh_token不合法或超时 , weixinInnerToken={0}, weixinErrorCode={1}", weixinInnerToken, refreshItem.errorCode);
                    rsp.errorCode = UserLoginRsp.ErrorID.weixinInnerTokenWrong;
                    SendMsgToClient(from, sessionId, rsp);
                    return;
                }

                if (refreshItem.errorCode != EWeixinErrorID.Success)
                {
                    LogSys.Warn("[LoginModule][WeixinInnerTokenLogin],微信刷新token失败, 其他错误, weixinInnerToken={0}, weixinErrorCode={1}", weixinInnerToken, refreshItem.weixinErrorCode);
                    rsp.errorCode = UserLoginRsp.ErrorID.weixinInnerTokenWrong;
                    SendMsgToClient(from, sessionId, rsp);
                    return;
                }

                player.Account.weixinAccessToken = refreshItem.access_token;
                player.Account.weixinAccessTokenExpireTime = env.Timer.CurrentDateTime.AddSeconds(refreshItem.expires_in - 100);
            }

            // 更新玩家在gate上的会话ID
            player.SessionID = sessionId;
            player.GateAddress = from;
            player.InnerToken = randomOneToken();

            player.Save();

            rsp.errorCode = UserLoginRsp.ErrorID.Success;
            rsp.gateInnerToken = player.InnerToken;
            rsp.accountid = player.AccountId;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 玩家上线
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnPlayerOnline(PlayerOnlineNtf req, UInt32 from, stServerBody serverBody)
        {
            LobbyPlayer player = g.playerMgr.FindPlayer(serverBody.accountId);
            if (null == player)
            {
                return;
            }

            if(serverBody.sessionId == player.SessionID)
            {
                player.clientIp = req.clientIp;
                player.Online();
            }
        }

        /// <summary>
        /// 玩家下线
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnPlayerOffline(PlayerOfflineNtf req, UInt32 from, stServerBody serverBody)
        {
            LobbyPlayer player = g.playerMgr.FindPlayer(serverBody.accountId);
            if (null == player)
            {
                return;
            }

            if(serverBody.sessionId == player.SessionID)
            {
                player.Offline();
            }
        }

        /// <summary>
        /// 生成一个token，断线重连时使用
        /// </summary>
        /// <returns></returns>
        protected string randomOneToken()
        {
            return "123456789";
        }

        string randomWeixinInnerToken(UInt64 accountId)
        {
            TimeSpan span = env.Timer.CurrentDateTime - DateTime.MinValue;
            UInt64 TotalMilliseconds = (UInt64)span.TotalMilliseconds;
            return TotalMilliseconds.ToString() + "," + accountId.ToString();
        }

        /// <summary>
        /// 重新登录
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="sessionId"></param>
        async void OnReloginReq(ReloginReq req, UInt32 from, UInt32 sessionId)
        {
            ReloginRsp rsp = new ReloginRsp();

            // 检查本地内存该账号是否存在
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(req.accountid);
            if (null == player)
            {
                return;
            }

            if (player.isOnline)
            {
                //检查到重复登录，踢掉原来的连接
                KickAccountReq msg = new KickAccountReq();
                msg.account = player.AccountKey.account;
                msg.platform = player.AccountKey.platform;
                msg.reason = KickReason.AccountDuplicate;
                SendMsgToGate(player.GateAddress, player.SessionID, msg);

                player.Offline();
            }

            if (player.InnerToken != req.innerToken)
            {
                rsp.errorCode = ReloginRsp.ErrorID.InnerToken_Expire;
                SendMsgToClient(from, sessionId, rsp);
                return;
            }

            player.SessionID = sessionId;
            player.GateAddress = from;

            rsp.errorCode = ReloginRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 玩家数据请求
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        async void OnPlayerDataReq(PlayerDataReq req, UInt32 from, stServerBody serverBody)
        {
            PlayerDataRsp rsp = new PlayerDataRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(serverBody.accountId);
            if (null == player)
            {
                return;
            }

            rsp.playerData.accountId = player.AccountId; // 账号id
            rsp.playerData.nickName = player.Attribute.NickName;
            rsp.playerData.diamond = player.GetDiamondCount();
            rsp.playerData.level = player.Attribute.Level;
            rsp.playerData.curExp = player.Attribute.Exp;
            rsp.playerData.inviteSelfRoleId = player.Attribute.inviteSelfRoleId;
            rsp.playerData.selfIp = player.clientIp;
            rsp.playerData.sex = player.Attribute.sex;
            rsp.playerData.weixinHeadImgUrl = player.Account.weixinHeadImgUrl;

            stLevelItem item = g.staticData.levelData.GetLevelItem(player.Attribute.Level);
            if(null != item)
            {
                rsp.playerData.title = item.title;
                rsp.playerData.levelupExp = item.levelupExp;
            }

            // 获取邀请人相关信息
            rsp.playerData.inviteRoleCount = (UInt32)player.Attribute.setInviteRoles.Count;
            UInt32 invitLevel = g.staticData.invitData.GetLevel((UInt32)player.Attribute.setInviteRoles.Count);
            stInvitItem invitItem = g.staticData.invitData.GetLevelItem(invitLevel);
            if(null != invitItem)
            {
                rsp.playerData.nextInviteRoleCount = invitItem.inviteCount;
                rsp.playerData.nextInviteAwardRoomCard = invitItem.roomcard;
            }

            rsp.errorCode = PlayerDataRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 获取玩家状态
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        async void OnGetPlayerStateReq(GetPlayerStateReq req, UInt32 from, stServerBody serverBody)
        {
            GetPlayerStateRsp rsp = new GetPlayerStateRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(serverBody.accountId);
            if (null == player)
            {
                return;
            }

            GameRoom room = g.roomMgr.GetRoom(player.room.roomUuid);
            if(null == room)
            {
                player.room.roomUuid = 0;
                player.room.createRoomUuid = 0;
                player.Save();

                rsp.errorCode = GetPlayerStateRsp.ErrorID.Success;
                rsp.playerState = EPlayerState.Play;
                player.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = GetPlayerStateRsp.ErrorID.Success;
            rsp.playerState = EPlayerState.Battle;
            rsp.roomUuid = player.room.roomUuid;
            rsp.gameType = room.gameType;
            player.SendMsgToClient(rsp);
        }

        public void SendMsgToClient(UInt32 from, UInt32 sessionId, Protocol proto)
        {
            proto.SessionId = sessionId;
            proto.RoleId = 0;

            env.BusService.SendMessageToServer(from, proto);
        }

        public void SendMsgToGate(VirtualAddress from, UInt32 sessionId, Protocol proto)
        {
            proto.SessionId = sessionId;
            proto.RoleId = 0;

            env.BusService.SendMessageToServer(from, proto);
        }

        public Task<ProtoBody> SendMsgToGateAsync(VirtualAddress from, UInt32 sessionId, Protocol proto)
        {
            UInt64 uuid = g.msgExtension.GenerateUuid();

            Task<ProtoBody> task = g.msgExtension.BackAsync(uuid);

            proto.SessionId = sessionId;
            proto.RoleId = 0;
            proto.uuid = uuid;

            env.BusService.SendMessageToServer(from, proto);

            return task;
        }
    }
}
