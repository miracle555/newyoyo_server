﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Protocols.Landlord;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using Landlord;

namespace Module
{
    /// <summary>
    /// 斗地主
    /// </summary>
    public class LandlordModule : LogicModule
    {
        public LandlordModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            //创建房间
            RegisterMsgHandler(MSGID.LandlordCreateRoomReq, new MsgHandler<LandlordCreateRoomReq>(OnLandlordCreateRoomReq));

            //加入房间
            RegisterMsgHandler(MSGID.LandlordJoinRoomReq, new MsgHandler<LandlordJoinRoomReq>(OnLandlordJoinRoomReq));

            //离开房间
            RegisterMsgHandler(MSGID.LandlordLeaveRoomB2L, new MsgHandler<LandlordLeaveRoomB2L>(OnLandlordLeaveRoomB2L));

            //房间关闭
            RegisterMsgHandler(MSGID.LandlordRoomCloseB2L, new MsgHandler<LandlordRoomCloseB2L>(OnLandlordRoomCloseB2L));

            //获取房间信息
            RegisterMsgHandler(MSGID.LandlordGetRoomInfoReq, new MsgHandler<LandlordGetRoomInfoReq>(OnLandlordGetRoomInfoReq));

            // 玩家赢了一局
            RegisterMsgHandler(MSGID.LandlordWinOneRoundB2L, new MsgHandler<LandlordWinOneRoundB2L>(OnLandlordWinOneRoundB2L));

            // 房间所有人都下线
            RegisterMsgHandler(MSGID.LandlordAllRoleOfflineB2L, new MsgHandler<LandlordAllRoleOfflineB2L>(OnLandlordAllRoleOfflineB2L));

            //获取与另一个玩家之间的距离
            RegisterMsgHandler(MSGID.LandlordDistanceReq, new MsgHandler<LandlordDistanceReq>(OnLandlordDistanceReq));

            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordCreateRoomReq(UInt32 from, UInt64 accountId, LandlordCreateRoomReq req)
        {
            LandlordCreateRoomRsp rsp = new LandlordCreateRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordCreateRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            //不是房主建房
            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                // 已经创建过房间
                if (player.room.IsAlreadyCreateRoom)
                {
                    rsp.errorCode = LandlordCreateRoomRsp.ErrorID.AlreadyCreateRoom;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[LandlordModule][OnLandlordCreateRoomReq],已经创建房间,accountId={0}", accountId);
                    return;
                }
            }

            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                // 如果在其他人的房间里
                if (player.room.IsInRoom)
                {
                    rsp.errorCode = LandlordCreateRoomRsp.ErrorID.AlreadInRoom;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[LandlordModule][OnLandlordCreateRoomReq],已经在房间里,accountId={0}", accountId);
                    return;
                }
            }

            if (!player.HasEnoughDiamond(req.config.roomCardCount))
            {
                rsp.errorCode = LandlordCreateRoomRsp.ErrorID.RoomCardNotEnough;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordCreateRoomReq],房卡数量不足,accountId={0}", accountId);
                return;
            }

            List<stRoomCardItem> lstRoomCards = player.DelDiamond(req.config.roomCardCount);

            // 记录房卡消耗事件
            g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.Landlord, req.config.roomRoundCount, lstRoomCards);

            UInt64 roomuuid = g.roomMgr.RandomRoomUuid();

            LordRoom room = new LordRoom();
            room.lstRoomCards = lstRoomCards;
            room.roomUuid = roomuuid;
            room.gameType = EGameType.Landlord;
            room.OwnerId = accountId;

            if (req.config.payMethod.Equals(PayMethod.masterPay))
            {
                room.pay = Pay.masterPay;
            }
            else if (req.config.payMethod.Equals(PayMethod.AAPay))
            {
                room.pay = Pay.AAPay;
            }
            else if (req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                room.pay = Pay.anthorPay;
            }

            g.roomMgr.AddRoom(room);

            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                player.room.roomUuid = roomuuid;
                player.room.createRoomUuid = roomuuid;
                player.Save();

                room.AddRole(accountId);
            }
            else
            {
                //房主建房,用户新增字段list表示房主创建的房间列表
                AnthorRoom anthorRoom = new AnthorRoom();
                anthorRoom.roomUuid = roomuuid;
                anthorRoom.creatTime = env.Timer.CurrentDateTime;
                anthorRoom.gameType = EGameType.Landlord;
                anthorRoom.isEnd = false;
                anthorRoom.totalNumber = 3;
                anthorRoom.currentNumber = 0;
                anthorRoom.shareTitle[0] = "一起斗地主";
                string neirong = getNeirong(req.config, roomuuid);
                anthorRoom.shareTitle[1] = neirong;
                player.anRoomList.Add(roomuuid, anthorRoom);
                player.Save();
                player.room.roomUuid = roomuuid;
            }
            g.roomBalance.IncRoleCount(room.battleAddr, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            LandlordCreateRoomL2B ntfBattle = new LandlordCreateRoomL2B();
            ntfBattle.roomUuid = roomuuid;
            ntfBattle.config = req.config;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();

            if (req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                player.room.roomUuid = 0;
            }
        }

        string getTitle(RoomConfig config, UInt64 roomuuid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[斗地主]-");
            sb.Append("房号:"+ roomuuid);
            return sb.ToString();
        }
        string getNeirong(RoomConfig config,UInt64 roomuuid)
        {
            StringBuilder sb = new StringBuilder();
            switch (config.roomRound)
            {
                case ERoomRound.OneRound:
                    sb.Append("局数:1局,");
                    break;
                case ERoomRound.FourRound:
                    sb.Append("局数:4局,");
                    break;
                case ERoomRound.EightRound:
                    sb.Append("局数:8局,");
                    break;
            }
            switch (config.payMethod)
            {
                case PayMethod.masterPay:
                    sb.Append("支付方式:房主支付");
                    break;
                case PayMethod.AAPay:
                    sb.Append("支付方式:AA支付");
                    break;
                case PayMethod.anotherPay:
                    sb.Append("支付方式:房主建房");
                    break;
            }
            return sb.ToString();
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordJoinRoomReq(UInt32 from, UInt64 accountId, LandlordJoinRoomReq req)
        {
            LandlordJoinRoomRsp rsp = new LandlordJoinRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (player.room.IsInRoom)
            {
                rsp.errorCode = LandlordJoinRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomReq],已经在房间里,accountId={0}", accountId);
                return;
            }

            //如果是房主创建的房间不允许加入
            if (player.anRoomList.ContainsKey(req.roomUuid))
            {
                rsp.errorCode = LandlordJoinRoomRsp.ErrorID.AnthorNotJoin;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomReq],房主建房不予许加入房间,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.roomMgr.GetRoom(req.roomUuid) as LordRoom;
            if(null == room)
            {
                rsp.errorCode = LandlordJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            if(room.IsRoomFull())
            {
                rsp.errorCode = LandlordJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomReq],房间已满,accountId={0}", accountId);
                return;
            }
            if (room.pay.Equals(Pay.AAPay))
            {
                //如果是AA制检测房卡
                if (!(player.HasEnoughDiamond(1)))
                {
                    rsp.errorCode = LandlordJoinRoomRsp.ErrorID.RoomCardNotEnough;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[LandlordModule][OnLandlordJoinRoomReq],房卡数量不足,accountId={0}", accountId);
                    return;
                }
                List<stRoomCardItem> lstRoomCards = player.DelDiamond(1);
                //记录方法消耗事件
                g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.MJYunCheng, 8, lstRoomCards);

            }
            room.AddRole(accountId);

            player.room.roomUuid = req.roomUuid;
            player.Save();

            g.roomBalance.IncRoleCount(room.battleAddr, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            LandlordJoinRoomL2B ntfBattle = new LandlordJoinRoomL2B();
            ntfBattle.roomUuid = req.roomUuid;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
            if (room.pay == Pay.anthorPay)
            {
                //如果是房主建房
                LobbyPlayer ownPlayer = await g.playerMgr.FindPlayerAsync(room.OwnerId);
                AnthorRoom anthor = ownPlayer.anRoomList[req.roomUuid];
                if (anthor.currentNumber < anthor.totalNumber)
                {
                    anthor.currentNumber++;
                }
                ownPlayer.Save();
            }
            //当房间人数已满,记录房间内所有玩家玩一局
            if (room.IsRoomFull())
            {
                foreach (UInt64 p in room.setRoles)
                {
                    LobbyPlayer lobbyPlayer = await g.playerMgr.FindPlayerAsync(p);
                    lobbyPlayer.playRoundCount++;
                    //每天  1 3 5 10能进行抽奖
                    if (lobbyPlayer.playRoundCount == 3
                        || lobbyPlayer.playRoundCount == 5 || lobbyPlayer.playRoundCount == 10)
                    {
                        lobbyPlayer.luckDrawCount++;
                    }

                    lobbyPlayer.Save();
                }
            }
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordGetRoomInfoReq(UInt32 from, UInt64 accountId, LandlordGetRoomInfoReq req)
        {
            LandlordGetRoomInfoRsp rsp = new LandlordGetRoomInfoRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordGetRoomInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (!player.room.IsInRoom)
            {
                rsp.errorCode = LandlordGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordGetRoomInfoReq],不在房间里,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.roomMgr.GetRoom(player.room.roomUuid) as LordRoom;
            if(null == room)
            {
                rsp.errorCode = LandlordGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            LandlordGetRoomInfoL2B ntfBattle = new LandlordGetRoomInfoL2B();
            ntfBattle.roomUuid = player.room.roomUuid;
            player.SendMsgToBattle(ntfBattle);
        }

        /// <summary>
        /// 玩家赢了一局
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordWinOneRoundB2L(UInt32 from, UInt64 accountId, LandlordWinOneRoundB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordWinOneRoundB2L],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.AddExp(g.staticData.levelData.WIN_ONE_ROUND_EXP);

            player.Save();
        }

        /// <summary>
        /// 房间结束
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordLeaveRoomB2L(UInt32 from, UInt64 accountId, LandlordLeaveRoomB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                return;
            }

            LordRoom room = g.roomMgr.GetRoom(req.roomUuid) as LordRoom;
            if(room.pay != Pay.anthorPay)
            {
                if (room.pay == Pay.AAPay)
                {
                    //如果是AA制需要返还房卡
                    player.ReturnDiamonds(room.lstRoomCards);
                }
                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                player.Save();
                //到战斗服
                g.roomBalance.DecRoleCount(player.BattleAddress, accountId);
                if(null != room)
                {
                    //房间中将玩家去掉
                    room.RemoveRole(accountId);

                    //如果房间内的人数为0 直接将房间删除掉
                    if (room.roleCount == 0)
                    {
                        g.roomMgr.DeleteRoom(req.roomUuid);
                    }
                    else
                    {
                        //保存房间信息
                        room.SaveToDb();
                    }
                }
            }
            else
            {
                player.room.roomUuid = 0;
                player.Save();

                room.RemoveRole(accountId);
                room.SaveToDb();
                //到战斗服
                g.roomBalance.DecRoleCount(player.BattleAddress, accountId);
                LobbyPlayer ownPlayer = await g.playerMgr.FindPlayerAsync(room.OwnerId);
                AnthorRoom anthor = ownPlayer.anRoomList[req.roomUuid];
                if (anthor.currentNumber > 0)
                {
                    anthor.currentNumber--;
                }
                ownPlayer.Save();
            }
        }

        /// <summary>
        /// 房间关闭
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordRoomCloseB2L(UInt32 from, UInt64 accountId, LandlordRoomCloseB2L req)
        {
            LordRoom room = g.roomMgr.GetRoom(req.roomUuid) as LordRoom;
            if (null == room)
            {
                return;
            }

            LobbyPlayer roomOwner = await g.playerMgr.FindPlayerAsync(req.roomOwnerId);
            if (req.bReturnRoomCard)
            {
                //找到房主
                if (null != roomOwner)
                {
                    //将房主房卡归还
                    roomOwner.ReturnDiamonds(room.lstRoomCards);
                }
                //如果是房主建房 去掉
                if (room.pay == Pay.anthorPay)
                {
                    //将房间删掉
                    roomOwner.anRoomList.Remove(room.roomUuid);
                }
            }
            else
            {
                //如果不需要归还房卡
                //增加一局创建房间的总数统计
                g.dataCenter.TotalHistoryCreateRoomCount();
                //增加消耗房卡的统计
                g.dataCenter.AddRoomCardConsumeCount(room.GetRoomCardConsume());
                //如果是房主建房 将房间设置成过去式
                if (room.pay == Pay.anthorPay)
                {
                    //将房间删掉
                    if (roomOwner.anRoomList.ContainsKey(req.roomUuid))
                    {
                        AnthorRoom anthor = roomOwner.anRoomList[req.roomUuid];
                        anthor.isEnd = true;
                    }
                }
            }
            roomOwner.Save();
            foreach (var roleid in room.setRoles)
            {
                LobbyPlayer player = await g.playerMgr.FindPlayerAsync(roleid);
                if (null == player)
                {
                    continue;
                }

                // 记录战绩
                if(req.bRecordBattleScore)
                {
                    player.room.AddBattleItem(req.scoreItem);
                }
                
                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                player.Save();

                g.roomBalance.DecRoleCount(player.BattleAddress, roleid);
            }

            g.roomMgr.DeleteRoom(req.roomUuid);
        }

        /// <summary>
        /// 所有人下线
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordAllRoleOfflineB2L(UInt32 from, UInt64 accountId, LandlordAllRoleOfflineB2L req)
        {
            LordRoom room = g.roomMgr.GetRoom(req.roomUuid) as LordRoom;
            if (null == room)
            {
                LogSys.Warn("[LandlordModule][OnLandlordAllRoleOfflineB2L],房间不存在,roomUuid={0}", req.roomUuid);
                return;
            }

            room.battleAddr = 0;

            room.SaveToDb();
        }

        /// <summary>
        /// 获取两个玩家之间的距离
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordDistanceReq(UInt32 from, UInt64 accountId, LandlordDistanceReq req)
        {
            LandlordDistanceRsp rsp = new LandlordDistanceRsp();
            rsp.errorCode = LandlordDistanceRsp.ErrorID.Failed;

            LobbyPlayer lobbyPlayer = g.playerMgr.FindPlayer(accountId);
            if (lobbyPlayer == null)
            {
                LogSys.Warn("[LandlordModule][OnLandlordDistanceReq],玩家不存在,accountId={0}", accountId);
                //lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            if (lobbyPlayer.x == 0 || lobbyPlayer.y == 0
                || lobbyPlayer.address.Equals(""))
            {
                LogSys.Warn("[LandlordModule][OnMJYunChengDistanceReq],获取玩家定位失败,accountId={0}", accountId);
                lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            LobbyPlayer oLobbyPlayer = g.playerMgr.FindPlayer(req.roleid);
            if (oLobbyPlayer == null || oLobbyPlayer.x == 0
                || oLobbyPlayer.y == 0 || oLobbyPlayer.address.Equals(""))
            {
                LogSys.Warn("[LandlordModule][OnLandlordDistanceReq],获取玩家定位失败,anthorAccountId={0}", req.roleid);
                lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = LandlordDistanceRsp.ErrorID.Success;
            rsp.address = oLobbyPlayer.address;
            int distance = BaiduMap.getDistance(lobbyPlayer.x, lobbyPlayer.y, oLobbyPlayer.x, oLobbyPlayer.y);
            rsp.distance = distance + "米";
            lobbyPlayer.SendMsgToClient(rsp);
        }
    }
}
