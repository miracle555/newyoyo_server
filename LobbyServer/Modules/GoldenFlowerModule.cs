﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Protocols.GoldenFlower;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using GoldenFlower;

namespace Module
{
    /// <summary>
    /// 扎金花
    /// </summary>
    public class GoldenFlowerModule : LogicModule
    {
        public GoldenFlowerModule()
            : base()
        {
        }

        protected override bool OnInit()
        {
            base.OnInit();
            LogSys.Info("GoldenFlowerModule.OnInit()");

            //创建房间
            RegisterMsgHandler(MSGID.GoldenFlowerCreateRoomReq, new MsgHandler<GoldenFlowerCreateRoomReq>(OnGoldenFlowerCreateRoomReq));

            //加入房间
            RegisterMsgHandler(MSGID.GoldenFlowerJoinRoomReq, new MsgHandler<GoldenFlowerJoinRoomReq>(OnGoldenFlowerJoinRoomReq));

            //离开房间
            RegisterMsgHandler(MSGID.GoldenFlowerLeaveRoomB2L, new MsgHandler<GoldenFlowerLeaveRoomB2L>(OnGoldenFlowerLeaveRoomB2L));

            //房间关闭
            RegisterMsgHandler(MSGID.GoldenFlowerRoomCloseB2L, new MsgHandler<GoldenFlowerRoomCloseB2L>(OnGoldenFlowerRoomCloseB2L));

            //获取房间信息
            RegisterMsgHandler(MSGID.GoldenFlowerGetRoomInfoReq, new MsgHandler<GoldenFlowerGetRoomInfoReq>(OnGoldenFlowerGetRoomInfoReq));

            // 玩家赢了一局
            RegisterMsgHandler(MSGID.GoldenFlowerWinOneRoundB2L, new MsgHandler<GoldenFlowerWinOneRoundB2L>(OnGoldenFlowerWinOneRoundB2L));

            // 房间所有人都下线
            RegisterMsgHandler(MSGID.GoldenFlowerAllRoleOfflineB2L, new MsgHandler<GoldenFlowerAllRoleOfflineB2L>(OnGoldenFlowerAllRoleOfflineB2L));

            //获取与另一个玩家之间的距离
            RegisterMsgHandler(MSGID.GoldenFlowerDistanceReq, new MsgHandler<GoldenFlowerDistanceReq>(OnGoldenFlowerDistanceReq));

            //游戏开始战斗服转大厅服
            RegisterMsgHandler(MSGID.GoldenFlowerStartB2L, new MsgHandler<GoldenFlowerStartB2L>(OnGoldenFlowerStartB2L));

            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("GoldenFlowerModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerCreateRoomReq(UInt32 from, UInt64 accountId, GoldenFlowerCreateRoomReq req)
        {
            GoldenFlowerCreateRoomRsp rsp = new GoldenFlowerCreateRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerCreateRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                // 已经创建过房间
                if (player.room.IsAlreadyCreateRoom)
                {
                    rsp.errorCode = GoldenFlowerCreateRoomRsp.ErrorID.AlreadyCreateRoom;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerCreateRoomReq],已经创建房间,accountId={0}", accountId);
                    return;
                }
            }


            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                // 如果在其他人的房间里
                if (player.room.IsInRoom)
                {
                    rsp.errorCode = GoldenFlowerCreateRoomRsp.ErrorID.AlreadInRoom;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerCreateRoomReq],已经在房间里,accountId={0}", accountId);
                    return;
                }
            }
                        
            List<stRoomCardItem> lstRoomCards = new List<stRoomCardItem>();
          

            if (req.config.payMethod.Equals(PayMethod.AAPay))
            {
                UInt32 fangKa = 0;
                switch ((UInt32)req.config.roomRound)
                {
                    case 3:
                        fangKa = 1;
                        break;
                    case 5:
                        fangKa = 2;
                        break;
                    case 7:
                        fangKa = 3;
                        break;

                }
                if (!(player.HasEnoughDiamond(fangKa)))
                {
                    rsp.errorCode = GoldenFlowerCreateRoomRsp.ErrorID.RoomCardNotEnough;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],房卡数量不足,accountId={0}", accountId);
                    return;
                }

                lstRoomCards = player.DelDiamond(fangKa);

            }
            else
            {
                if (!player.HasEnoughDiamond((UInt32)req.config.roomRound))
                {
                    rsp.errorCode = GoldenFlowerCreateRoomRsp.ErrorID.RoomCardNotEnough;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerCreateRoomReq],房卡数量不足,accountId={0}", accountId);
                    return;
                }
                lstRoomCards = player.DelDiamond((UInt32)req.config.roomRound);


            }


            // 记录房卡消耗事件 扎金花
            g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.GoldenFlower, req.config.roomRoundCount, lstRoomCards);

            UInt64 roomuuid = g.roomMgr.RandomRoomUuid();

            FlowerRoom room = new FlowerRoom();
            room.lstRoomCards = lstRoomCards;
            room.roomUuid = roomuuid;
            room.gameType = EGameType.GoldenFlower; //扎金花
            room.OwnerId = accountId;
            room.roomRound = req.config.roomRound;
            room.AddRole(accountId);

            if (req.config.payMethod.Equals(PayMethod.masterPay))
            {
                room.pay = Pay.masterPay;
            }
            else if (req.config.payMethod.Equals(PayMethod.AAPay))
            {
                room.pay = Pay.AAPay;
            }
            else if (req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                room.pay = Pay.anthorPay;
            }



            g.roomMgr.AddRoom(room);

            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                player.room.roomUuid = roomuuid;
                player.room.createRoomUuid = roomuuid;
                player.Save();

                room.AddRole(accountId);

            }
            else
            {
                //房主建房,用户新增字段list表示房主创建的房间列表
                AnthorRoom anthorRoom = new AnthorRoom();
                anthorRoom.roomUuid = roomuuid;
                anthorRoom.creatTime = env.Timer.CurrentDateTime;
                anthorRoom.gameType = EGameType.GoldenFlower;
                anthorRoom.isEnd = false;
                anthorRoom.totalNumber = 5;
                anthorRoom.currentNumber = 0;
                anthorRoom.shareTitle[0] = getTitle(req.config, roomuuid);
                string neirong = getNeirong(req.config, roomuuid);
                anthorRoom.shareTitle[1] = neirong;
                player.anRoomList.Add(roomuuid, anthorRoom);
                player.Save();
                player.room.roomUuid = roomuuid;
            }
            g.roomBalance.IncRoleCount(room.battleAddr, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            GoldenFlowerCreateRoomL2B ntfBattle = new GoldenFlowerCreateRoomL2B();
            ntfBattle.roomUuid = roomuuid;
            ntfBattle.config = req.config;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
            if (req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                player.room.roomUuid = 0;
            }
        }

        string getTitle(RoomConfig config, UInt64 roomuuid)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[赢三张]-");
            sb.Append("房号:" + roomuuid);
            return sb.ToString();
        }
        string getNeirong(RoomConfig config,UInt64 roomuuid)
        {
            StringBuilder sb = new StringBuilder();
            switch (config.roomRound)
            {
                case ERoomRound.EightRound:
                    sb.Append("局数:8局,");
                    break;
                case ERoomRound.SixteenRound:
                    sb.Append("局数:16局,");
                    break;
                case ERoomRound.TwentyfourRound:
                    sb.Append("局数:24局,");
                    break;
            }
            switch (config.pariMutuel)
            {
                case Pari.Null:
                    sb.Append("彩分:0分,");
                    break;
                case Pari.Five:
                    sb.Append("彩分:5分,");
                    break;
                case Pari.Ten:
                    sb.Append("彩分:10分,");
                    break;
            }
            switch (config.roundNumber)
            {
                case Rounds.Null:
                    sb.Append("下注次数限制:不限制,");
                    break;
                case Rounds.Twenty:
                    sb.Append("下注次数限制:20轮,");
                    break;
                case Rounds.forty:
                    sb.Append("下注次数限制:40轮,");
                    break;
            }
            switch (config.fold)
            {
                case Multiple.Null:
                    break;
                case Multiple.One:
                    sb.Append("分场:1分场,");
                    break;
                case Multiple.Two:
                    sb.Append("分场:2分场,");
                    break;
                case Multiple.Three:
                    sb.Append("分场:3分场,");
                    break;
                case Multiple.Four:
                    sb.Append("分场:4分场,");
                    break;
            }
            switch (config.payMethod)
            {
                case PayMethod.masterPay:
                    sb.Append("支付方式:房主支付");
                    break;
                case PayMethod.AAPay:
                    sb.Append("支付方式:AA支付");
                    break;
                case PayMethod.anotherPay:
                    sb.Append("支付方式:房主建房");
                    break;
            }
            return sb.ToString();
        }


        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerJoinRoomReq(UInt32 from, UInt64 accountId, GoldenFlowerJoinRoomReq req)
        {
            GoldenFlowerJoinRoomRsp rsp = new GoldenFlowerJoinRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (player.room.IsInRoom)
            {
                rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],已经在房间里,accountId={0}", accountId);
                return;
            }

            //如果是房主创建的房间不允许加入
            if (player.anRoomList.ContainsKey(req.roomUuid))
            {
                rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.AnthorNotJoin;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],房主建房不予许加入房间,accountId={0}", accountId);
                return;
            }

            FlowerRoom room = g.roomMgr.GetRoom(req.roomUuid) as FlowerRoom;
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            if (room.IsRoomFull())
            {
                rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],房间已满,accountId={0}", accountId);
                return;
            }

            if (room.start)
            {
                rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.HasStart;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],游戏已经开始,accountId={0}", accountId);
                return;

            }
            
            if (room.pay.Equals(Pay.AAPay))
            {
                UInt32 fangKa = 0;
                switch ((UInt32)room.roomRound)
                {
                    case 3:
                        fangKa = 1;
                        break;
                    case 5:
                        fangKa = 2;
                        break;
                    case 7:
                        fangKa = 3;
                        break;

                }
                //如果是AA制检测房卡
                if (!(player.HasEnoughDiamond(fangKa)))
                {
                    rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.RoomCardNotEnough;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],房卡数量不足,accountId={0}", accountId);
                    return;
                }

                List<stRoomCardItem> lstRoomCards = player.DelDiamond(fangKa);
                room.lstRoomCards = lstRoomCards;
                // 记录房卡消耗事件 扎金花
                int juRound = 0;
                switch ((UInt32)room.roomRound)
                {
                    case 3:
                        juRound = 8;
                        break;
                    case 5:
                        juRound = 16;
                        break;
                    case 7:
                        juRound = 24;
                        break;

                }

                g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.GoldenFlower, juRound, lstRoomCards);
            }
            
            room.AddRole(accountId);

            player.room.roomUuid = req.roomUuid;
            player.Save();

            g.roomBalance.IncRoleCount(room.battleAddr, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            GoldenFlowerJoinRoomL2B ntfBattle = new GoldenFlowerJoinRoomL2B();
            ntfBattle.roomUuid = req.roomUuid;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
            if (room.pay == Pay.anthorPay)
            {
                //如果是房主建房
                LobbyPlayer ownPlayer = await g.playerMgr.FindPlayerAsync(room.OwnerId);
                AnthorRoom anthor = ownPlayer.anRoomList[req.roomUuid];
                if (anthor.currentNumber < anthor.totalNumber)
                {
                    anthor.currentNumber++;
                }
                ownPlayer.Save();
            }
        }


        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerGetRoomInfoReq(UInt32 from, UInt64 accountId, GoldenFlowerGetRoomInfoReq req)
        {
            GoldenFlowerGetRoomInfoRsp rsp = new GoldenFlowerGetRoomInfoRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerGetRoomInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (!player.room.IsInRoom)
            {
                rsp.errorCode = GoldenFlowerGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerGetRoomInfoReq],不在房间里,accountId={0}", accountId);
                return;
            }

            FlowerRoom room = g.roomMgr.GetRoom(player.room.roomUuid) as FlowerRoom;
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            GoldenFlowerGetRoomInfoL2B ntfBattle = new GoldenFlowerGetRoomInfoL2B();
            ntfBattle.roomUuid = player.room.roomUuid;
            player.SendMsgToBattle(ntfBattle);
        }

        /// <summary>
        /// 玩家赢了一局
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerWinOneRoundB2L(UInt32 from, UInt64 accountId, GoldenFlowerWinOneRoundB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerWinOneRoundB2L],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.AddExp(g.staticData.levelData.WIN_ONE_ROUND_EXP);

            player.Save();
        }


        /// <summary>
        /// 房间结束
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerLeaveRoomB2L(UInt32 from, UInt64 accountId, GoldenFlowerLeaveRoomB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                return;
            }

            FlowerRoom room = g.roomMgr.GetRoom(req.roomUuid) as FlowerRoom;
            if (room.pay != Pay.anthorPay)
            {
                if (room.pay == Pay.AAPay)
                {
                    //如果是AA制需要返还房卡
                    player.ReturnDiamonds(room.lstRoomCards);
                }
                //将用户的房间uuid 设置为0
                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                //更新当前玩家的信息
                player.Save();

                //到战斗服
                g.roomBalance.DecRoleCount(player.BattleAddress, accountId);

                if (null != room)
                {
                    //房间中将玩家去掉
                    room.RemoveRole(accountId);

                    //如果房间内的人数为0 直接将房间删除掉
                    if (room.roleCount == 0)
                    {
                        g.roomMgr.DeleteRoom(req.roomUuid);
                    }
                    else
                    {
                        //保存房间信息
                        room.SaveToDb();
                    }
                }
            }
            else
            {
                player.room.roomUuid = 0;
                player.Save();
                room.RemoveRole(accountId);
                room.SaveToDb();
                //到战斗服
                g.roomBalance.DecRoleCount(player.BattleAddress, accountId);
                LobbyPlayer ownPlayer = await g.playerMgr.FindPlayerAsync(room.OwnerId);
                AnthorRoom anthor = ownPlayer.anRoomList[req.roomUuid];
                if (anthor.currentNumber > 0)
                {
                    anthor.currentNumber--;
                }
                ownPlayer.Save();
            }

        }

        /// <summary>
        /// 房间关闭
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerRoomCloseB2L(UInt32 from, UInt64 accountId, GoldenFlowerRoomCloseB2L req)
        {
            FlowerRoom room = g.roomMgr.GetRoom(req.roomUuid) as FlowerRoom;
            if (null == room)
            {
                return;
            }

            LobbyPlayer roomOwner = await g.playerMgr.FindPlayerAsync(req.roomOwnerId);

            // 归还房主房卡
            if (req.bReturnRoomCard)
            {
                //找到房主
                if (null != roomOwner)
                {
                    //将房主房卡归还
                    roomOwner.ReturnDiamonds(room.lstRoomCards);
                }
                //如果是房主建房 去掉
                if (room.pay == Pay.anthorPay)
                {
                    //将房间删掉
                    roomOwner.anRoomList.Remove(room.roomUuid);
                }
            }
            else
            {
                //如果不需要归还房卡
                //增加一局创建房间的总数统计
                g.dataCenter.TotalHistoryCreateRoomCount();
                //增加消耗房卡的统计
                g.dataCenter.AddRoomCardConsumeCount(room.GetRoomCardConsume());
                //如果是房主建房 将房间设置成过去式
                if (room.pay == Pay.anthorPay)
                {
                    //将房间删掉
                    if (roomOwner.anRoomList.ContainsKey(req.roomUuid))
                    {
                        //将房间删掉
                        AnthorRoom anthor = roomOwner.anRoomList[req.roomUuid];
                        anthor.isEnd = true;
                    }
                }
            }
            roomOwner.Save();
            foreach (var roleid in room.setRoles)
            {
                LobbyPlayer player = await g.playerMgr.FindPlayerAsync(roleid);
                if (null == player)
                {
                    continue;
                }

                // 记录战绩
                if (req.bRecordBattleScore)
                {
                    player.room.AddBattleItem(req.scoreItem);
                }

                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                player.Save();

                g.roomBalance.DecRoleCount(player.BattleAddress, roleid);
            }

            g.roomMgr.DeleteRoom(req.roomUuid);
        }

        /// <summary>
        /// 所有人下线
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerAllRoleOfflineB2L(UInt32 from, UInt64 accountId, GoldenFlowerAllRoleOfflineB2L req)
        {
            FlowerRoom room = g.roomMgr.GetRoom(req.roomUuid) as FlowerRoom;
            if (null == room)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerAllRoleOfflineB2L],房间不存在,roomUuid={0}", req.roomUuid);
                return;
            }

            room.battleAddr = 0;

            room.SaveToDb();
        }

        /// <summary>
        /// 获取两个玩家之间的距离
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerDistanceReq(UInt32 from, UInt64 accountId, GoldenFlowerDistanceReq req)
        {
            GoldenFlowerDistanceRsp rsp = new GoldenFlowerDistanceRsp();
            rsp.errorCode = GoldenFlowerDistanceRsp.ErrorID.Failed;

            LobbyPlayer lobbyPlayer = g.playerMgr.FindPlayer(accountId);
            if (lobbyPlayer == null)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerAllRoleOfflineB2L],玩家不存在,accountId={0}", accountId);
                //lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            if (lobbyPlayer.x == 0 || lobbyPlayer.y == 0
                || lobbyPlayer.address.Equals(""))
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerAllRoleOfflineB2L],获取玩家定位失败,accountId={0}", accountId);
                lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            LobbyPlayer oLobbyPlayer = g.playerMgr.FindPlayer(req.roleid);
            if (oLobbyPlayer == null || oLobbyPlayer.x == 0
                || oLobbyPlayer.y == 0 || oLobbyPlayer.address.Equals(""))
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerAllRoleOfflineB2L],获取玩家定位失败,anthorAccountId={0}", req.roleid);
                lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = GoldenFlowerDistanceRsp.ErrorID.Success;
            rsp.address = oLobbyPlayer.address;
            int distance = BaiduMap.getDistance(lobbyPlayer.x, lobbyPlayer.y, oLobbyPlayer.x, oLobbyPlayer.y);
            rsp.distance = distance + "米";
            lobbyPlayer.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 战斗服转大厅服
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerStartB2L(UInt32 from, UInt64 accountId, GoldenFlowerStartB2L req)
        {
            FlowerRoom room = g.roomMgr.GetRoom(req.roomId) as FlowerRoom;

            room.start = true;

            //当房间人数已满,记录房间内所有玩家玩一局
            //if (room.IsRoomFull())
            //{
                foreach (UInt64 p in room.setRoles)
                {
                    LobbyPlayer lobbyPlayer = await g.playerMgr.FindPlayerAsync(p);
                    lobbyPlayer.playRoundCount++;
                    //每天  1 3 5 10能进行抽奖
                    if (lobbyPlayer.playRoundCount == 3
                        || lobbyPlayer.playRoundCount == 5 || lobbyPlayer.playRoundCount == 10)
                    {
                        lobbyPlayer.luckDrawCount++;
                    }

                    lobbyPlayer.Save();
                }
           // }
        }
    }
}
