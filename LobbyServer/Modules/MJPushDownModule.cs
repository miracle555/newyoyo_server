﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Protocols.MajiangPushDown;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using MahjongPushDown;

namespace Module
{
    /// <summary>
    /// 推倒胡模块
    /// </summary>
    public class MJPushDownModule : LogicModule
    {
        public MJPushDownModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            // 创建房间
            RegisterMsgHandler(MSGID.MJPushDownCreateRoomReq, new MsgHandler<MJPushDownCreateRoomReq>(OnMJPushDownCreateRoomReq));

            // 加入房间
            RegisterMsgHandler(MSGID.MJPushDownJoinRoomReq, new MsgHandler<MJPushDownJoinRoomReq>(OnMJPushDownJoinRoomReq));

            // 离开房间
            RegisterMsgHandler(MSGID.MJPushDownLeaveRoomB2L, new MsgHandler<MJPushDownLeaveRoomB2L>(OnMJPushDownLeaveRoomB2L));

            // 房间结束
            RegisterMsgHandler(MSGID.MJPushDownRoomCloseB2L, new MsgHandler<MJPushDownRoomCloseB2L>(OnMJPushDownRoomCloseB2L));

            //获取房间信息
            RegisterMsgHandler(MSGID.MJPushDownGetRoomInfoReq, new MsgHandler<MJPushDownGetRoomInfoReq>(OnMJPushDownGetRoomInfoReq));

            // 推倒胡赢了一局
            RegisterMsgHandler(MSGID.MJPushDownWinOneRoundB2L, new MsgHandler<MJPushDownWinOneRoundB2L>(OnMJPushDownWinOneRoundB2L));

            // 房间所有人都下线
            RegisterMsgHandler(MSGID.MJPushDownAllRoleOfflineB2L, new MsgHandler<MJPushDownAllRoleOfflineB2L>(OnMJPushDownAllRoleOfflineB2L));

            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownCreateRoomReq(UInt32 from, UInt64 accountId, MJPushDownCreateRoomReq req)
        {
            MJPushDownCreateRoomRsp rsp = new MJPushDownCreateRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMahjongCreateRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 已经创建过房间
            if (player.room.IsAlreadyCreateRoom)
            {
                rsp.errorCode = MJPushDownCreateRoomRsp.ErrorID.AlreadyCreateRoom;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMahjongCreateRoomReq],已经创建房间,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (player.room.IsInRoom)
            {
                rsp.errorCode = MJPushDownCreateRoomRsp.ErrorID.AlreadInRoom;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMahjongCreateRoomReq],已经在房间里,accountId={0}", accountId);
                return;
            }

            if (!player.HasEnoughDiamond(req.config.roomCardCount))
            {
                rsp.errorCode = MJPushDownCreateRoomRsp.ErrorID.RoomCardNotEnough;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnMahjongCreateRoomReq],房卡数量不足,accountId={0}", accountId);
                return;
            }
            List<stRoomCardItem> lstRoomCards = player.DelDiamond(req.config.roomCardCount);

            // 记录房卡消耗事件
            g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.MJPushDown, req.config.roomRound, lstRoomCards);

            UInt64 roomuuid = g.roomMgr.RandomRoomUuid();
            player.room.roomUuid = roomuuid;
            player.room.createRoomUuid = roomuuid;
            player.Save();

            HuRoom room = new HuRoom();
            room.lstRoomCards = lstRoomCards;
            room.roomUuid = roomuuid;
            room.gameType = EGameType.MJPushDown;
            room.OwnerId = accountId;
            g.roomMgr.AddRoom(room);

            room.AddRole(accountId);

            g.roomBalance.IncRoleCount(room.battleAddr, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            MJPushDownCreateRoomL2B ntfBattle = new MJPushDownCreateRoomL2B();
            ntfBattle.roomUuid = roomuuid;
            ntfBattle.config = req.config;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownJoinRoomReq(UInt32 from, UInt64 accountId, MJPushDownJoinRoomReq req)
        {
            MJPushDownJoinRoomRsp rsp = new MJPushDownJoinRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (player.room.IsInRoom)
            {
                rsp.errorCode = MJPushDownJoinRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomReq],已经在房间里,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.roomMgr.GetRoom(req.roomUuid) as HuRoom;
            if(null == room)
            {
                rsp.errorCode = MJPushDownJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            if(room.IsRoomFull())
            {
                rsp.errorCode = MJPushDownJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomReq],房间已满,accountId={0}", accountId);
                return;
            }

            room.AddRole(accountId);

            player.room.roomUuid = req.roomUuid;
            player.Save();

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            MJPushDownJoinRoomL2B ntfBattle = new MJPushDownJoinRoomL2B();
            ntfBattle.roomUuid = req.roomUuid;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
        }

        /// <summary>
        /// （推倒胡）离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownLeaveRoomB2L(UInt32 from, UInt64 accountId, MJPushDownLeaveRoomB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                return;
            }

            if (player.room.createRoomUuid == req.roomUuid)
            {
                player.room.createRoomUuid = 0;
            }
            if (player.room.roomUuid == req.roomUuid)
            {
                player.room.roomUuid = 0;
            }

            player.Save();

            g.roomBalance.DecRoleCount(player.BattleAddress, accountId);

            HuRoom room = g.roomMgr.GetRoom(req.roomUuid) as HuRoom;
            if(null != room)
            {
                room.RemoveRole(accountId);
                
                if(room.roleCount == 0)
                {
                    g.roomMgr.DeleteRoom(req.roomUuid);
                }
                else
                {
                    room.SaveToDb();
                }
            }
        }

        /// <summary>
        /// 房间结束
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownRoomCloseB2L(UInt32 from, UInt64 accountId, MJPushDownRoomCloseB2L req)
        {
            HuRoom room = g.roomMgr.GetRoom(req.roomUuid) as HuRoom;
            if (null == room)
            {
                return;
            }

            // 归还房主房卡
            if (req.bReturnRoomCard)
            {
                LobbyPlayer roomOwner = await g.playerMgr.FindPlayerAsync(req.roomOwnerId);
                if (null != roomOwner)
                {
                    roomOwner.ReturnDiamonds(room.lstRoomCards);
                }
            }
            else
            {
                g.dataCenter.TotalHistoryCreateRoomCount();
                g.dataCenter.AddRoomCardConsumeCount(room.GetRoomCardConsume());
            }

            LogSys.Info("[MJPushDownModule][OnMJPushDownRoomCloseB2L],房间结束,ownerId={0}, roomUuid={1}", req.roomOwnerId, req.roomUuid);

            foreach (var roleid in room.setRoles)
            {
                LobbyPlayer player = await g.playerMgr.FindPlayerAsync(roleid);
                if (null == player)
                {
                    continue;
                }

                LogSys.Info("[MJPushDownModule][OnMJPushDownRoomCloseB2L],房间结束,ownerId={0}, roomUuid={1}, member={2}, membername={3}, member_createRoomUuid={4}, member_roomUuid={5}", req.roomOwnerId, req.roomUuid, player.AccountId, player.Attribute.NickName, player.room.createRoomUuid, player.room.roomUuid);

                // 记录战绩
                if(req.bRecordBattleScore)
                {
                    player.room.AddBattleItem(req.scoreItem);
                }

                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                player.Save();

                g.roomBalance.DecRoleCount(player.BattleAddress, roleid);
            }

            g.roomMgr.DeleteRoom(req.roomUuid);
        }

        /// <summary>
        /// 推倒胡赢了一局
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownWinOneRoundB2L(UInt32 from, UInt64 accountId, MJPushDownWinOneRoundB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownWinOneRoundB2L],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.AddExp(g.staticData.levelData.WIN_ONE_ROUND_EXP);

            player.Save();
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownGetRoomInfoReq(UInt32 from, UInt64 accountId, MJPushDownGetRoomInfoReq req)
        {
            MJPushDownGetRoomInfoRsp rsp = new MJPushDownGetRoomInfoRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownGetRoomInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (!player.room.IsInRoom)
            {
                rsp.errorCode = MJPushDownGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownGetRoomInfoReq],不在房间里,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.roomMgr.GetRoom(player.room.roomUuid) as HuRoom;
            if(null == room)
            {
                rsp.errorCode = MJPushDownGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            MJPushDownGetRoomInfoL2B ntfBattle = new MJPushDownGetRoomInfoL2B();
            ntfBattle.roomUuid = player.room.roomUuid;
            player.SendMsgToBattle(ntfBattle);
        }

        /// <summary>
        /// 所有人下线
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownAllRoleOfflineB2L(UInt32 from, UInt64 accountId, MJPushDownAllRoleOfflineB2L req)
        {
            HuRoom room = g.roomMgr.GetRoom(req.roomUuid) as HuRoom;
            if(null == room)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownAllRoleOfflineB2L],房间不存在,roomUuid={0}", req.roomUuid);
                return;
            }

            room.battleAddr = 0;

            room.SaveToDb();
        }
    }
}
