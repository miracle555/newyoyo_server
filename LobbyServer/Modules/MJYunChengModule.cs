﻿using Entity;
using LobbyServer;
using Log;
using MahjongYunCheng;
using Protocols;
using Protocols.MajiangYunCheng;
using ServerLib;
using ServerLib.Bus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module
{
    /// <summary>
    /// 运城贴金模块
    /// </summary>
    public class MJYunChengModule:LogicModule
    {
        public MJYunChengModule()
            :base()
        {
        }

        //Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("MJYunChengModule.OnInit()");

            //创建房间
            RegisterMsgHandler(MSGID.MJYunChengCreateRoomReq,new MsgHandler<MJYunChengCreateRoomReq>(OnMJYunChengCreatRoomReq));
            
            //加入房间
            RegisterMsgHandler(MSGID.MJYunChengJoinRoomReq, new MsgHandler<MJYunChengJoinRoomReq>(OnMJYunChengJoinRoomReq));

            //离开房间
            RegisterMsgHandler(MSGID.MJYunChengLeaveRoomB2L, new MsgHandler<MJYunChengLeaveRoomB2L>(OnMJYunChengLeaveRoomB2L));

            //房间关闭
            RegisterMsgHandler(MSGID.MJYunChengRoomCloseB2L, new MsgHandler<MJYunChengRoomCloseB2L>(OnMJYunChengRoomCloseB2L));

            //获取房间信息
            RegisterMsgHandler(MSGID.MJYunChengGetRoomInfoReq, new MsgHandler<MJYunChengGetRoomInfoReq>(OnMjYunChengGetRoomInfoReq));

            //运城贴金赢了一局
            RegisterMsgHandler(MSGID.MJYunChengWinOneRoundB2L, new MsgHandler<MJYunChengWinOneRoundB2L>(OnMJYunChengWinOneRoundB2L));

            // 房间所有人都下线
            RegisterMsgHandler(MSGID.MJYunChengAllRoleOfflineB2L, new MsgHandler<MJYunChengAllRoleOfflineB2L>(OnMJYunChengAllRoleOfflineB2L));

            //获取与另一个玩家之间的距离
            RegisterMsgHandler(MSGID.MJYunChengDistanceReq, new MsgHandler<MJYunChengDistanceReq>(OnMJYunChengDistanceReq));
            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //Message Handlers
        /// <summary>
        /// 运城贴金创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJYunChengCreatRoomReq(UInt32 from,UInt64 accountId,MJYunChengCreateRoomReq req)
        {
            MJYunChengCreateRoomRsp rsp = new MJYunChengCreateRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);

            //如果找不到用户
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][MJYunChengCreateRoomRsp],找不到玩家,accountId={0}", accountId);
                return;
            }

            //如果已经创建过房间 且不是房主代开房
            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                if (player.room.IsAlreadyCreateRoom)
                {
                    rsp.errorCode = MJYunChengCreateRoomRsp.ErrorID.AlreadyCreateRoom;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[MJYunChengModule][MJYunChengCreateRoomRsp],已经创建过房间,accountId={0}", accountId);
                    return;
                }
            }

            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                //如果在其他人的房间里
                if (player.room.IsInRoom)
                {
                    rsp.errorCode = MJYunChengCreateRoomRsp.ErrorID.AlreadInRoom;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[MJYunChengModule][MJYunChengCreateRoomRsp],已经在房间里,accountId={0}", accountId);
                    return;
                }
            }

            //如果房卡数量不足
            if (!(player.HasEnoughDiamond(req.config.roomCardCount)))
            {
                rsp.errorCode = MJYunChengCreateRoomRsp.ErrorID.RoomCardNotEnough;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][MJYunChengCreateRoomRsp],房卡数量不足,accountId={0}", accountId);
                return;
            }

            List<stRoomCardItem> lstRoomCards = player.DelDiamond(req.config.roomCardCount);

            //记录方法消耗事件
            g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.MJYunCheng, req.config.roomRound, lstRoomCards);

            //随机房间uuid
            UInt64 roomuuid = g.roomMgr.RandomRoomUuid();

            //创建房间并初始化数据
            YunChengRoom room = new YunChengRoom();
            room.lstRoomCards = lstRoomCards;
            room.roomUuid = roomuuid;
            room.gameType = EGameType.MJYunCheng;
            room.OwnerId = accountId;

            if (req.config.payMethod.Equals(PayMethod.masterPay))
            {
                room.pay = Pay.masterPay;
            }
            else if (req.config.payMethod.Equals(PayMethod.AAPay))
            {
                room.pay = Pay.AAPay;
            }
            else if (req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                room.pay = Pay.anthorPay;
            }

            g.roomMgr.AddRoom(room);

            if (!req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                player.room.roomUuid = roomuuid;
                player.room.createRoomUuid = roomuuid;
                player.Save();

                room.AddRole(accountId);

            }
            else
            {
                //房主建房,用户新增字段list表示房主创建的房间列表
                AnthorRoom anthorRoom = new AnthorRoom();
                anthorRoom.roomUuid = roomuuid;
                anthorRoom.creatTime = env.Timer.CurrentDateTime;
                anthorRoom.gameType = EGameType.MJYunCheng;
                anthorRoom.isEnd = false;
                anthorRoom.totalNumber = 4;
                anthorRoom.currentNumber = 0;
                anthorRoom.shareTitle[0] = getTitle(req.config, roomuuid);
                string neirong = getNeirong(req.config, roomuuid);
                anthorRoom.shareTitle[1] = neirong;
                if (req.config.goldens == GoldenNumber.FourGolden)
                {
                    anthorRoom.danjin = true;
                }
                player.anRoomList.Add(roomuuid, anthorRoom);
                player.Save();
                player.room.roomUuid = roomuuid;
            }
            g.roomBalance.IncRoleCount(room.battleAddr, accountId);

            //创建角色在战斗服上
            player.CreateOnBattle();

            //转发消息到battleSever
            MJYunChengCreateRoomL2B ntfBattle = new MJYunChengCreateRoomL2B();
            ntfBattle.roomUuid = roomuuid;
            ntfBattle.config = req.config;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();
            if (req.config.payMethod.Equals(PayMethod.anotherPay))
            {
                player.room.roomUuid = 0;
            }
        }

        string getTitle(RoomConfig config, UInt64 roomuuid)
        {
            StringBuilder sb = new StringBuilder();
            switch (config.goldens)
            {
                case GoldenNumber.FourGolden:
                    sb.Append("[单金麻将]-");
                    break;
                case GoldenNumber.EightGolden:
                    sb.Append("[双金麻将]-");
                    break;
            }
            sb.Append("房号:"+ roomuuid);
            return sb.ToString();
        }

        string getNeirong(RoomConfig config, UInt64 roomuuid)
        {
            StringBuilder sb = new StringBuilder();
            switch (config.cardRound)
            {
                case ECardRound.OneRound:
                    sb.Append("局数:1局,");
                    break;
                case ECardRound.EightRound:
                    sb.Append("局数:8局,");
                    break;
                case ECardRound.FourRound:
                    sb.Append("局数:4局,");
                    break;
            }
            if (config.bOnlyWinSelfWhenGoldenLess)
            {
                sb.Append("只可自摸,");
            }
            switch (config.cappingNumber)
            {
                case CappingNumber.Two:
                    sb.Append("双金封顶,");
                    break;
                case CappingNumber.Three:
                    sb.Append("三金封顶,");
                    break;
                case CappingNumber.Four:
                    sb.Append("四金封顶,");
                    break;
            }
            if (config.isKunJin)
            {
                sb.Append("捆金,");
            }
            else
            {
                sb.Append("不捆金,");
            }
            switch (config.payMethod)
            {
                case PayMethod.masterPay:
                    sb.Append("支付方式:房主支付");
                    break;
                case PayMethod.AAPay:
                    sb.Append("支付方式:AA支付");
                    break;
                case PayMethod.anotherPay:
                    sb.Append("支付方式:房主建房");
                    break;
            }
            return sb.ToString();
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJYunChengJoinRoomReq(UInt32 from,UInt64 accountId, MJYunChengJoinRoomReq req)
        {
            MJYunChengJoinRoomRsp rsp = new MJYunChengJoinRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            //如果查找不到用户
            if(null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomReq],找不到玩家,accountId={0}",accountId);
                return;
            }
            //如果在其他人房间
            if(player.room.IsInRoom)
            {
                rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomReq],已经在房间里,accountId={0}", accountId);
                return;
            }

            //如果是房主创建的房间不允许加入
            if (player.anRoomList.ContainsKey(req.roomUuid))
            {
                rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.AnthorNotJoin;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomReq],房主建房不予许加入房间,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.roomMgr.GetRoom(req.roomUuid) as YunChengRoom;

            //如果房间不存在
            if(null == room)
            {
                rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomReq],房间不存在,accountId={0}",accountId);
                return;
            }

            //如果房间人数已满
            if(room.IsRoomFull())
            {
                rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomReq],房间已满,accountId={0}", accountId);
                return;
            }

            if (room.pay.Equals(Pay.AAPay))
            {
                //如果是AA制检测房卡
                if (!(player.HasEnoughDiamond(1)))
                {
                    rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.RoomCardNotEnough;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[MJYunChengModule][MJYunChengCreateRoomRsp],房卡数量不足,accountId={0}", accountId);
                    return;
                }
                List<stRoomCardItem> lstRoomCards = player.DelDiamond(1);
                //记录方法消耗事件
                g.agentMgr.RecordRCConsumeEvent(accountId, EGameType.MJYunCheng,8 , lstRoomCards);
            }


            room.AddRole(accountId);

            player.room.roomUuid = req.roomUuid;

           
            player.Save();

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            //创建角色在战斗服
            player.CreateOnBattle();

            //转发消息到battleserver
            MJYunChengJoinRoomL2B ntfBattle = new MJYunChengJoinRoomL2B();
            ntfBattle.roomUuid = req.roomUuid;
            player.SendMsgToBattle(ntfBattle);

            room.SaveToDb();

            if(room.pay == Pay.anthorPay)
            {
                //如果是房主建房
                LobbyPlayer ownPlayer = await g.playerMgr.FindPlayerAsync(room.OwnerId);
                AnthorRoom anthor = ownPlayer.anRoomList[req.roomUuid];
                if(anthor.currentNumber < anthor.totalNumber)
                {
                    anthor.currentNumber++;
                }
                ownPlayer.Save();
            }

            //当房间人数已满,记录房间内所有玩家玩一局
            if (room.IsRoomFull())
            {
                foreach(UInt64 p in room.setRoles){
                    LobbyPlayer lobbyPlayer =await g.playerMgr.FindPlayerAsync(p);
                    lobbyPlayer.playRoundCount++;
                    //每天  1 3 5 10能进行抽奖
                    if(lobbyPlayer.playRoundCount == 3
                        || lobbyPlayer.playRoundCount == 5 || lobbyPlayer.playRoundCount == 10)
                    {
                        lobbyPlayer.luckDrawCount++;
                    }

                    lobbyPlayer.Save();
                }
            }
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMjYunChengGetRoomInfoReq(UInt32 from,UInt64 accountId, MJYunChengGetRoomInfoReq req)
        {
            MJYunChengGetRoomInfoRsp rsp = new MJYunChengGetRoomInfoRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            //如果找不到玩家
            if(null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMjYunChengGetRoomInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            // 如果在其他人的房间里
            if (!player.room.IsInRoom)
            {
                rsp.errorCode = MJYunChengGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMjYunChengGetRoomInfoReq],不在房间里,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.roomMgr.GetRoom(player.room.roomUuid) as YunChengRoom;
            if (null == room)
            {
                rsp.errorCode = MJYunChengGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMjYunChengGetRoomInfoReq],房间不存在,accountId={0}", accountId);
                return;
            }

            g.roomBalance.IncRoleCount(player.BattleAddress, accountId);

            // 创建角色在战斗服上
            player.CreateOnBattle();

            // 转发消息到battleserver
            MJYunChengGetRoomInfoL2B ntfBattle = new MJYunChengGetRoomInfoL2B();
            ntfBattle.roomUuid = player.room.roomUuid;
            player.SendMsgToBattle(ntfBattle);
        }

        /// <summary>
        /// （抠点）离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJYunChengLeaveRoomB2L(UInt32 from, UInt64 accountId, MJYunChengLeaveRoomB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                return;
            }
            //获取玩家的房间
            YunChengRoom room = g.roomMgr.GetRoom(req.roomUuid) as YunChengRoom;
            if (room.pay != Pay.anthorPay)
            {
                if(room.pay == Pay.AAPay)
                {
                    //如果是AA制需要返还房卡
                    player.ReturnDiamonds(room.lstRoomCards);
                }
                //将用户的房间uuid 设置为0
                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                //更新当前玩家的信息
                player.Save();

                //到战斗服
                g.roomBalance.DecRoleCount(player.BattleAddress, accountId);

                if (null != room)
                {
                    //房间中将玩家去掉
                    room.RemoveRole(accountId);

                    //如果房间内的人数为0 直接将房间删除掉
                    if (room.roleCount == 0)
                    {
                        g.roomMgr.DeleteRoom(req.roomUuid);
                    }
                    else
                    {
                        //保存房间信息
                        room.SaveToDb();
                    }
                }
            }
            else
            {
                //房间中将玩家去掉
                room.RemoveRole(accountId);
                room.SaveToDb();
                player.room.roomUuid = 0;
                player.Save();
                //到战斗服
                g.roomBalance.DecRoleCount(player.BattleAddress, accountId);
                LobbyPlayer ownPlayer = await g.playerMgr.FindPlayerAsync(room.OwnerId);
                AnthorRoom anthor = ownPlayer.anRoomList[req.roomUuid];
                if (anthor.currentNumber > 0)
                {
                    anthor.currentNumber--;
                }
                ownPlayer.Save();
            }
            
        }

        /// <summary>
        /// 房间关闭
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJYunChengRoomCloseB2L(UInt32 from, UInt64 accountId, MJYunChengRoomCloseB2L req)
        {
            //获取运城贴金房间
            YunChengRoom room = g.roomMgr.GetRoom(req.roomUuid) as YunChengRoom;
            if (null == room)
            {
                return;
            }

            LobbyPlayer roomOwner = await g.playerMgr.FindPlayerAsync(req.roomOwnerId);
            //如果需要归还房主房卡
            if (req.bReturnRoomCard)
            {

                //找到房主
                if (null != roomOwner)
                {
                    //将房主房卡归还
                    roomOwner.ReturnDiamonds(room.lstRoomCards);
                }
                //如果是房主建房 去掉
                if(room.pay == Pay.anthorPay)
                {
                    //将房间删掉
                    roomOwner.anRoomList.Remove(room.roomUuid);
                }
            }
            else
            {
                //如果不需要归还房卡
                //增加一局创建房间的总数统计
                g.dataCenter.TotalHistoryCreateRoomCount();
                //增加消耗房卡的统计
                g.dataCenter.AddRoomCardConsumeCount(room.GetRoomCardConsume());
                //如果是房主建房 将房间设置成过去式
                if (room.pay == Pay.anthorPay)
                {
                    //将房间删掉
                    if (roomOwner.anRoomList.ContainsKey(req.roomUuid))
                    {
                        AnthorRoom anthor = roomOwner.anRoomList[req.roomUuid];
                        if (anthor != null)
                        {
                            anthor.isEnd = true;
                        }
                    }
                }
            }
            roomOwner.Save();
            //遍历房间中所有的用户
            foreach (var roleid in room.setRoles)
            {
                //找到房间内的用户
                LobbyPlayer player = await g.playerMgr.FindPlayerAsync(roleid);
                if (null == player)
                {
                    continue;
                }

                // 记录战绩
                if (req.bRecordBattleScore)
                {
                    player.room.AddBattleItem(req.scoreItem);
                }

                if (player.room.createRoomUuid == req.roomUuid)
                {
                    player.room.createRoomUuid = 0;
                }
                if (player.room.roomUuid == req.roomUuid)
                {
                    player.room.roomUuid = 0;
                }

                player.Save();

                g.roomBalance.DecRoleCount(player.BattleAddress, roleid);
            }

            g.roomMgr.DeleteRoom(req.roomUuid);
        }

        /// <summary>
        /// 运城贴金赢了一局
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJYunChengWinOneRoundB2L(UInt32 from, UInt64 accountId, MJYunChengWinOneRoundB2L req)
        {
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            //if (null == player)
           // {
            //    LogSys.Warn("[MJYunChengModule][OnMJYunChengWinOneRoundB2L],找不到玩家,accountId={0}", accountId);
             //   return;
            //}

            //player.AddExp(g.staticData.levelData.WIN_ONE_ROUND_EXP);

           // player.Save();
        }

        /// <summary>
        /// 所有人下线
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengAllRoleOfflineB2L(UInt32 from, UInt64 accountId, MJYunChengAllRoleOfflineB2L req)
        {
            //运城麻将
            YunChengRoom room = g.roomMgr.GetRoom(req.roomUuid) as YunChengRoom;

            if (null == room)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengWinOneRoundB2L],房间不存在,roomUuid={0}", req.roomUuid);
                return;
            }
            //设置战斗服地址为0表示所有人下线
            room.battleAddr = 0;

            room.SaveToDb();
        }

        /// <summary>
        /// 获取两个玩家之间的距离
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengDistanceReq(UInt32 from, UInt64 accountId, MJYunChengDistanceReq req)
        {
            MJYunChengDistanceRsp rsp = new MJYunChengDistanceRsp();
            rsp.errorCode = MJYunChengDistanceRsp.ErrorID.Failed;

            LobbyPlayer lobbyPlayer = g.playerMgr.FindPlayer(accountId);
            if (lobbyPlayer == null)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengDistanceReq],玩家不存在,accountId={0}", accountId);
                //lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            if (lobbyPlayer.x == 0 || lobbyPlayer.y == 0
                || lobbyPlayer.address.Equals(""))
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengDistanceReq],获取玩家定位失败,accountId={0}", accountId);
                lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            LobbyPlayer oLobbyPlayer = g.playerMgr.FindPlayer(req.roleid);
            if(oLobbyPlayer == null || oLobbyPlayer.x == 0
                || oLobbyPlayer.y == 0 || oLobbyPlayer.address.Equals(""))
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengDistanceReq],获取玩家定位失败,anthorAccountId={0}", req.roleid);
                lobbyPlayer.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = MJYunChengDistanceRsp.ErrorID.Success;
            rsp.address = oLobbyPlayer.address;
            int distance = BaiduMap.getDistance(lobbyPlayer.x, lobbyPlayer.y,oLobbyPlayer.x,oLobbyPlayer.y);
            rsp.distance = distance + "米";
            lobbyPlayer.SendMsgToClient(rsp);
        }
    }
}
