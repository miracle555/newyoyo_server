﻿/********************************************************************
	purpose:    游戏模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;
using Room;
using Protocols.MajiangYunCheng;
using Protocols.GoldenFlower;
using Protocols.Landlord;

namespace Module
{
    public class GameModule : LogicModule
    {
        public GameModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("GameModule.OnInit()");

            // 获取房间游戏类型
           RegisterMsgHandler(MSGID.GetRoomGameTypeReq, new MsgHandler<GetRoomGameTypeReq>(OnGetRoomGameTypeReq));

            // 填写邀请自己的人
           RegisterMsgHandler(MSGID.InputInviteSelfRoleReq, new MsgHandler<InputInviteSelfRoleReq>(OnInputInviteSelfRoleReq));
         
            // 获取房间其他玩家信息
           RegisterMsgHandler(MSGID.GetRoomRolesInfoReq, new MsgHandler<GetRoomRolesInfoReq>(OnGetRoomRolesInfoReq));

            //获取战绩
            RegisterMsgHandler(MSGID.GetBattleScoreItemsReq, new MsgHandler<GetBattleScoreItemsReq>(OnGetBattleScoreItemsReq));

            // 获取订单
            RegisterMsgHandler(MSGID.GetPayOrderReq, new MsgHandler<GetPayOrderReq>(OnGetPayOrderReq));

            //获取定位信息
            RegisterMsgHandler(MSGID.UserLocationInfoReq, new MsgHandler<UserLocationInfoReq>(OnUserLocationInfoReq));

            //当前房主建房
            RegisterMsgHandler(MSGID.CurrentAnthorCreatRoomReq, new MsgHandler<CurrentAnthorCreatRoomReq>(OnCurrentAnthorCreatRoomReq));

            //历史房主建房
            RegisterMsgHandler(MSGID.HistoryAnthorCreatRoomReq, new MsgHandler<HistoryAnthorCreatRoomReq>(OnHistoryAnthorCreatRoomReq));

            //红包请求
            RegisterMsgHandler(MSGID.RedPackageReq, new MsgHandler<RedPackageReq>(OnRedPackageReq));

            //转盘请求
            RegisterMsgHandler(MSGID.TurntableReq, new MsgHandler<TurntableReq>(OnTurntableReq));

            //房主建房解散房间请求
            RegisterMsgHandler(MSGID.DissolveTheRoomReq, new MsgHandler<DissolveTheRoomReq>(OnDissolveTheRoomReq));

            //微信分享请求
            RegisterMsgHandler(MSGID.WeChatShareReq, new MsgHandler<WeChatShareReq>(OnWeChatShareReq));

            env.eventSystem.AddListener<EventServiceStart>(EventId.EventServiceStart, HandleEventServiceStart);
            
            if (!g.roomMgr.LoadFromDb())
            {
                return false;
            }
           
            g.staticData.Init();

            if (!g.staticDataMgr.Init())
            {
                return false;
            }

            if (!g.dataCenter.LoadFromDb())
            {
                return false;
            }
            
            if (!g.rechargeMgr.Init())
            {
                return false;
            }

            g.msgExtension.Init(env.BusService);

            return true;
        }

        protected override void OnTick()
        {
            g.msgExtension.Update(env.Timer.CurrentDateTime);
        }

        void HandleEventServiceStart(EventServiceStart ev)
        {
            VirtualAddress addr = ev.serverid;
            if (!addr.IsBattle())
            {
                return;
            }
            g.roomBalance.AddBattleServerId(ev.serverid);
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 获得房间游戏类型
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        async void OnGetRoomGameTypeReq(UInt32 from, UInt64 accountId, GetRoomGameTypeReq req)
        {
            GetRoomGameTypeRsp rsp = new GetRoomGameTypeRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GameModule][GetRoomGameTypeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if(!g.roomMgr.HasRoom(req.roomUuid))
            {
                 // 没有该房间
                rsp.errorCode = GetRoomGameTypeRsp.ErrorID.NotExist;
                player.SendMsgToClient(rsp);
                LogSys.Info("[GameModule][GetRoomGameTypeReq],房间不存在,accountId={0}", accountId);
                return;
            }

            GameRoom room = g.roomMgr.GetRoom(req.roomUuid);

            rsp.errorCode = GetRoomGameTypeRsp.ErrorID.Success;
            rsp.gameType = room.gameType;
            player.SendMsgToClient(rsp);
            return;
        }

        /// <summary>
        /// 填写邀请自己的人
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnInputInviteSelfRoleReq(UInt32 from, UInt64 accountId, InputInviteSelfRoleReq req)
        {
            InputInviteSelfRoleRsp rsp = new InputInviteSelfRoleRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GameModule][OnInputInviteSelfRoleReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if(player.Attribute.inviteSelfRoleId != 0)
            {
                rsp.errorCode = InputInviteSelfRoleRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GameModule][OnInputInviteSelfRoleReq],已经输入过邀请人,accountId={0}", accountId);
                return;
            }

            if (req.roleid == player.AccountId)
            {
                rsp.errorCode = InputInviteSelfRoleRsp.ErrorID.NotSelf;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GameModule][OnInputInviteSelfRoleReq],邀请人不能填写自己,accountId={0}", accountId);
                return;
            }

            LobbyPlayer playerB = await g.playerMgr.FindPlayerAsync(req.roleid);
            if (null == playerB)
            {
                rsp.errorCode = InputInviteSelfRoleRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GameModule][OnInputInviteSelfRoleReq],找不到目标玩家,accountId={0}", accountId);
                return;
            }

            if(playerB.Attribute.inviteSelfRoleId == accountId)
            {
                rsp.errorCode = InputInviteSelfRoleRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GameModule][OnInputInviteSelfRoleReq],目标是自己邀请的,循环邀请,accountId={0}, targetRoleId={1}", accountId, req.roleid);
                return;
            }

            if(playerB.Attribute.inviteSelfRoleId != 0)
            {
                LobbyPlayer playerA = await g.playerMgr.FindPlayerAsync(playerB.Attribute.inviteSelfRoleId);
                if (null == playerA)
                {
                    rsp.errorCode = InputInviteSelfRoleRsp.ErrorID.MutualInvit;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GameModule][OnInputInviteSelfRoleReq],找不到目标玩家A,accountId={0},B={1},A={2}", accountId, playerB.AccountId, playerB.Attribute.inviteSelfRoleId);
                    return;
                }

                if (playerA.Attribute.inviteSelfRoleId == accountId)
                {
                    rsp.errorCode = InputInviteSelfRoleRsp.ErrorID.MutualInvit;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GameModule][OnInputInviteSelfRoleReq],目标的邀请人是自己邀请的,循环邀请,accountId={0}, B={1}, A={2}", accountId, playerB.AccountId, playerA.AccountId);
                    return;
                }
            }
            
            player.Attribute.inviteSelfRoleId = req.roleid;
            playerB.AddInviteRole(accountId);

            // 添加邀请人获得房卡
            player.AddDiamond(ERCSourceType.System, 0, g.staticData.invitData.INVIT_AWARD_ROOMCARD);

            player.Save();
            playerB.Save();

            rsp.errorCode = InputInviteSelfRoleRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 获取房间其他玩家信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGetRoomRolesInfoReq(UInt32 from, UInt64 accountId, GetRoomRolesInfoReq req)
        {
            GetRoomRolesInfoRsp rsp = new GetRoomRolesInfoRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GameModule][OnGetRoomRolesInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!g.roomMgr.HasRoom(req.roomuuid))
            {
                // 没有该房间
                rsp.errorCode = GetRoomRolesInfoRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GameModule][OnGetRoomRolesInfoReq],房间不存在,accountId={0}", accountId);
                return;
            }

            GameRoom room = g.roomMgr.GetRoom(req.roomuuid);
            if (room.IsRoomFull())
            {
                // 没有该房间
                rsp.errorCode = GetRoomRolesInfoRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GameModule][OnGetRoomRolesInfoReq],房间已满,accountId={0}", accountId);
                return;
            }

            foreach (var roleid in room.setRoles)
            {
                LobbyPlayer other = await g.playerMgr.FindPlayerAsync(roleid);
                if (null == other)
                {
                    rsp.errorCode = GetRoomRolesInfoRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GameModule][OnGetRoomRolesInfoReq],找不到玩家,accountId={0}, otherRoleId={1}", accountId, roleid);
                    return;
                }

                stRoleItem roleInfo = new stRoleItem();
                other.GetRoleInfo(roleInfo);
                rsp.lstRoles.Add(roleInfo);
            }

            rsp.errorCode = GetRoomRolesInfoRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 获取战绩列表
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGetBattleScoreItemsReq(UInt32 from, UInt64 accountId, GetBattleScoreItemsReq req)
        {
            GetBattleScoreItemsRsp rsp = new GetBattleScoreItemsRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GameModule][OnGetBattleScoreItemsReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            rsp.lstScoreItems = player.room.lstScoreItems;

            rsp.errorCode = GetBattleScoreItemsRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 获取经验排行榜
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGetExpRankReq(UInt32 from, UInt64 accountId, GetExpRankReq req)
        {
            GetExpRankRsp rsp = new GetExpRankRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GameModule][OnGetExpRankReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            rsp.selfRank = await g.rankMgr.GetRankAsync(accountId);
            rsp.selfLevel = player.Attribute.Level;
            stLevelItem selfItem = g.staticData.levelData.GetLevelItem(rsp.selfLevel);
            if (selfItem != null)
            {
                rsp.selfTitle = selfItem.title;
            }

            List<UInt64> lstRoleIds = await g.rankMgr.GetTopRankItemsAsync(50);

            int rank = 1;
            foreach(var roleid in lstRoleIds)
            {
                LobbyPlayer playerRank = await g.playerMgr.FindPlayerAsync(roleid);
                if(playerRank == null)
                {
                    continue;
                }

                stExpRankItem rankItem = new stExpRankItem();
                rankItem.rank = rank++;

                rankItem.roleid = roleid;
                rankItem.nickName = playerRank.Attribute.NickName;
                rankItem.level = playerRank.Attribute.Level;
                rankItem.sex = playerRank.Attribute.sex;

                stLevelItem item = g.staticData.levelData.GetLevelItem(rankItem.level);
                if(item != null)
                {
                    rankItem.title = item.title;
                }

                rankItem.curExp = playerRank.Attribute.Exp;
                rankItem.weixinHeadImgUrl = playerRank.Account.weixinHeadImgUrl;

                rsp.lstRankItems.Add(rankItem);
            }

            rsp.errorCode = GetExpRankRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 请求下单
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGetPayOrderReq(UInt32 from, UInt64 accountId, GetPayOrderReq req)
        {
            GetPayOrderRsp rsp = new GetPayOrderRsp();
            rsp.errorCode = GetPayOrderRsp.ErrorID.Failed;

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GameModule][OnGetPayOrderReq], 找不到玩家, accountId={0}", accountId);
                return;
            }

            t_recharge config = g.staticDataMgr.rechargeData.GetConfig(req.recharge_id);
            if (null == config)
            {
                rsp.errorCode = GetPayOrderRsp.ErrorID.ItemNotExist;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GameModule][OnGetPayOrderReq], 找不到充值商品, accountId={0}, rechargeId={1}", accountId, req.recharge_id);
                return;
            }

            if (player.Attribute.inviteSelfRoleId == 0)
            {
                rsp.errorCode = GetPayOrderRsp.ErrorID.NoInviter;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GameModule][OnGetPayOrderReq], 玩家未填写邀请者, accountId={0}, rechargeId={1}", accountId, req.recharge_id);
                return;
            }

            stWeixinOrderItem orderItem = await g.weixin.PrePayOrderAsync(req.recharge_id, config.f_price, player.AccountId, player.Attribute.NickName, player.clientIp);
            if (orderItem.errCode == EWeixinPayErrorID.Failed)
            {
                rsp.errorCode = GetPayOrderRsp.ErrorID.WeiXinPrePayFailed;
                player.SendMsgToClient(rsp);
                LogSys.WarnModule("PayModule", "[GameModule][OnGetPayOrderReq], 微信下单失败, accountId={0}, rechargeId={1}", accountId, req.recharge_id);
                return;
            }

            rsp.partnerid = HttpWeixin.MchID;
            rsp.prepayid = orderItem.payorder.prepay_id;
            rsp.appid = HttpWeixin.AppID;
            rsp.MchKey = HttpWeixin.MchKey;
            rsp.errorCode = GetPayOrderRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 获取玩家定位信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnUserLocationInfoReq(UInt32 from,UInt64 accountId, UserLocationInfoReq req)
        {
            UserLocationInfoRsp rsp = new UserLocationInfoRsp();
            rsp.errorCode = UserLocationInfoRsp.ErrorID.Failed;

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GameModule][OnUserLocationInfoReq], 找不到玩家, accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }

            if (req.isOpenGps)
            {
                //如果有经纬度
                if (req.latitude <= 0 || req.latitude <= 0)
                {
                    LogSys.Warn("[GameModule][OnUserLocationInfoReq], 经纬度数据不正确, accountId={0}", accountId);
                    player.SendMsgToClient(rsp);
                    return;
                }
                BaiduMap baidumap = new BaiduMap();
                BaiDuVO baiduvo = baidumap.baiDuApi(req.longitude, req.latitude);
                if(baiduvo == null || baiduvo.getSematic_description() == null)
                {
                    LogSys.Warn("[GameModule][OnUserLocationInfoReq], 获取位置失败, accountId={0}", accountId);
                    player.SendMsgToClient(rsp);
                    return;
                }
                rsp.adress = baiduvo.getSematic_description();
                player.x = req.latitude;
                player.y = req.longitude;
                player.hasGps = true;
                player.address = baiduvo.getSematic_description();
            }
            else
            {
                rsp.adress = "未开启定位服务";
                player.hasGps = false;
            }
            rsp.errorCode = UserLocationInfoRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
            player.Save();
        }



        /// <summary>
        /// 当前房主建房列表
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnCurrentAnthorCreatRoomReq(UInt32 from,UInt64 accountId, CurrentAnthorCreatRoomReq req)
        {
            CurrentAnthorCreatRoomRsp rsp = new CurrentAnthorCreatRoomRsp();
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                rsp.errorCode = CurrentAnthorCreatRoomRsp.ErrorID.Failed;
                LogSys.Warn("[GameModule][OnCurrentAnthorCreatRoomReq], 找不到玩家, accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }

            Dictionary<UInt64, AnthorRoom> list = new Dictionary<UInt64, AnthorRoom>();
            int i = 0;
            foreach(var item in player.anRoomList)
            {
                if (!item.Value.isEnd)
                {
                    list.Add(item.Key,item.Value);
                    i++;
                    if (i >= 50)
                    {
                        break;
                    }
                }
                
            }
            rsp.errorCode = CurrentAnthorCreatRoomRsp.ErrorID.Success;

            rsp.roomList = list;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 房主历史建房
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnHistoryAnthorCreatRoomReq(UInt32 from,UInt64 accountId, HistoryAnthorCreatRoomReq req)
        {
            HistoryAnthorCreatRoomRsp rsp = new HistoryAnthorCreatRoomRsp();
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                rsp.errorCode = HistoryAnthorCreatRoomRsp.ErrorID.Failed;
                LogSys.Warn("[GameModule][OnHistoryAnthorCreatRoomRsp], 找不到玩家, accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = HistoryAnthorCreatRoomRsp.ErrorID.Success;

            Dictionary<UInt64, AnthorRoom> list = new Dictionary<UInt64, AnthorRoom>();
            int i = 0;
            foreach (var item in player.anRoomList)
            {
                list.Add(item.Key, item.Value);
                i++;
                if (i >= 50)
                {
                    break;
                }
            }

            rsp.roomList = list;

            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 玩家红包请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnRedPackageReq(UInt32 from,UInt64 accountId, RedPackageReq req)
        {
            RedPackageRsp rsp = new RedPackageRsp();
            rsp.errorCode = RedPackageRsp.ErrorID.Failed;

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if(player == null)
            {
                LogSys.Warn("[GameModule][OnRedPackageReq], 找不到玩家, accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = RedPackageRsp.ErrorID.Success;
            rsp.luckDrawCount = player.luckDrawCount;
            rsp.playRoundCount = player.playRoundCount;
            rsp.readyNumber = player.readyNumber;
            rsp.redPacketMoney = player.redPacketMoney;
            rsp.hasShare = player.hasShare;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 转盘请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnTurntableReq(UInt32 from,UInt64 accountId, TurntableReq req)
        {
            TurntableRsp rsp = new TurntableRsp();
            rsp.errorCode = TurntableRsp.ErrorID.Failed;

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GameModule][OnTurntableReq], 找不到玩家, accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }

            //判断是否还有转盘次数
            if(player.readyNumber >= player.luckDrawCount)
            {
                LogSys.Warn("[GameModule][OnTurntableReq], 转盘次数已用完, accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }
            
            rsp.errorCode = TurntableRsp.ErrorID.Success;
            Random random = new Random();
            Winning winning = new Winning();
            winning.winTime = env.Timer.CurrentDateTime;
            if(player.winList.Count == 0)
            {
                rsp.winType = WinType.OnePointEightEight;
                winning.winType = WinType.OnePointEightEight;
                player.redPacketMoney = player.redPacketMoney + (decimal)1.88;
            }
            else
            {
                int ran = random.Next(0, 2);
                if (ran == 0)
                {
                    rsp.winType = WinType.RoomCardOne;
                    winning.winType = WinType.RoomCardOne;
                    //房卡加一张
                    stRoomCardItem stRoomCardItem = new stRoomCardItem();
                    stRoomCardItem.sourceType = ERCSourceType.System;
                    stRoomCardItem.roomCardCount = 1;
                    player.Attribute.lstRoomCards.Add(stRoomCardItem);
                    RoomCardChangeNtf ntf = new RoomCardChangeNtf();
                    ntf.diamond = player.GetDiamondCount();
                    player.SendMsgToClient(ntf);
                }
                else
                {
                    rsp.winType = WinType.ZeroPointOneEight;
                    winning.winType = WinType.ZeroPointOneEight;
                    player.redPacketMoney = player.redPacketMoney + (decimal)0.18;
                }
            }
            
            player.winList.Add(winning);
            player.readyNumber++;
            player.Save();
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 房主建房解散房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnDissolveTheRoomReq(UInt32 from, UInt64 accountId, DissolveTheRoomReq req)
        {
            DissolveTheRoomRsp rsp = new DissolveTheRoomRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                LogSys.Warn("[GameModule][OnDissolveTheRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!g.roomMgr.HasRoom(req.roomId))
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Success;
                player.anRoomList.Remove(req.roomId);
                // 没有该房间
                player.SendMsgToClient(rsp);
                LogSys.Info("[GameModule][OnDissolveTheRoomReq],房间不存在,accountId={0}", accountId);
                return;
            }

            AnthorRoom anthorRoom = player.anRoomList[req.roomId];
            if(anthorRoom == null || anthorRoom.totalNumber == anthorRoom.currentNumber)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Info("[GameModule][OnDissolveTheRoomReq],房间已经开始,accountId={0}", accountId);
                return;
            }

            GameRoom room = g.roomMgr.GetRoom(req.roomId);
            player.room.roomUuid = req.roomId;
            switch (room.gameType)
            {
                case EGameType.MJYunCheng:
                    MJYunChengOtherCloseRoomL2B ycl2b = new MJYunChengOtherCloseRoomL2B();
                    ycl2b.roomId = req.roomId;
                    player.SendMsgToBattle(ycl2b);
                    break;
                case EGameType.GoldenFlower:
                    GoldenFlowerOtherCloseRoomL2B gfl2b = new GoldenFlowerOtherCloseRoomL2B();
                    gfl2b.roomId = req.roomId;
                    player.SendMsgToBattle(gfl2b);
                    break;
                case EGameType.Landlord:
                    LandlordOtherCloseRoomL2B lll2b = new LandlordOtherCloseRoomL2B();
                    lll2b.roomId = req.roomId;
                    player.SendMsgToBattle(lll2b);
                    break;
            }
            player.room.roomUuid = 0;
        }

        /// <summary>
        /// 微信分享返回
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnWeChatShareReq(UInt32 from, UInt64 accountId, WeChatShareReq req)
        {
            WeChatShareRsp rsp = new WeChatShareRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            rsp.errorCode = WeChatShareRsp.ErrorID.Failed;
            if (null == player)
            {
                LogSys.Warn("[GameModule][OnWeChatShareReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (player.hasShare)
            {
                rsp.errorCode = WeChatShareRsp.ErrorID.HaveShare;
                LogSys.Warn("[GameModule][OnWeChatShareReq],玩家已经进行过分享,accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }
            //设置今日已经分享过
            player.hasShare = true;

            //抽奖次数加一
            player.luckDrawCount++;
            rsp.errorCode = WeChatShareRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
            player.Save();
        }
    }
}
