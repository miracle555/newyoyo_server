﻿/********************************************************************
	purpose:    gm模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Protocols.gm;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;
using Room;
using Redis;

namespace Module
{
    public class GmModule : LogicModule
    {
        public GmModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            // GM修改自己密码
            RegisterMsgHandler(MSGID.GmModifySelfPasswordReq, new MsgHandler<GmModifySelfPasswordReq>(OnGmModifySelfPasswordReq));

            // GM获取服务器当前状态
            RegisterMsgHandler(MSGID.GmGetServerCurrentStatusReq, new MsgHandler<GmGetServerCurrentStatusReq>(OnGmGetServerCurrentStatusReq));

            // GM创建代理账号
            RegisterMsgHandler(MSGID.GmCreateAgentAccountReq, new MsgHandler<GmCreateAgentAccountReq>(OnGmCreateAgentAccountReq));

            // GM停用代理账号
            RegisterMsgHandler(MSGID.GmStopAgentAccountReq, new MsgHandler<GmStopAgentAccountReq>(OnGmStopAgentAccountReq));

            // GM修改代理账号密码
            RegisterMsgHandler(MSGID.GmModifyAgentAccountReq, new MsgHandler<GmModifyAgentAccountReq>(OnGmModifyAgentAccountReq));

            // GM增加房卡
            RegisterMsgHandler(MSGID.GmAddRoomCardReq, new MsgHandler<GmAddRoomCardReq>(OnGmAddRoomCardReq));

            // GM扣除房卡
            RegisterMsgHandler(MSGID.GmDecRoomCardReq, new MsgHandler<GmDecRoomCardReq>(OnGmDecRoomCardReq));
  
            // 代理转移房卡
            RegisterMsgHandler(MSGID.GmTransferRoomCardReq, new MsgHandler<GmTransferRoomCardReq>(OnGmTransferRoomCardReq));

            // gm发公告
            RegisterMsgHandler(MSGID.GmPublicNoticeReq, new MsgHandler<GmPublicNoticeReq>(OnGmPublicNoticeReq));

            // gm查询代理流水
            RegisterMsgHandler(MSGID.GmQueryAgentRoomCardEventsReq, new MsgHandler<GmQueryAgentRoomCardEventsReq>(OnGmQueryAgentRoomCardEventsReq));

            // 代理查询流水
            RegisterMsgHandler(MSGID.AgemtQueryRoomCardEventsReq, new MsgHandler<AgemtQueryRoomCardEventsReq>(OnAgemtQueryRoomCardEventsReq));

            // gm获取玩家信息
            RegisterMsgHandler(MSGID.GmGetPlayerInfoReq, new MsgHandler<GmGetPlayerInfoReq>(OnGmGetPlayerInfoReq));

            // gm增加代理商房卡
            RegisterMsgHandler(MSGID.GmAddAgentRoomCardReq, new MsgHandler<GmAddAgentRoomCardReq>(OnGmAddAgentRoomCardReq));

            // gm减少代理商房卡
            RegisterMsgHandler(MSGID.GmDecAgentRoomCardReq, new MsgHandler<GmDecAgentRoomCardReq>(OnGmDecAgentRoomCardReq));

            // gm获取代理商列表
            RegisterMsgHandler(MSGID.GmGetAgentListReq, new MsgHandler<GmGetAgentListReq>(OnGmGetAgentListReq));

            //gm授权
            RegisterMsgHandler(MSGID.GmAgentImpowerReq, new MsgHandler<GmAgentImpowerReq>(OnGmAgentImpowerReq));

            //gm停权
            RegisterMsgHandler(MSGID.GmAgentStopPowerReq, new MsgHandler<GmAgentStopPowerReq>(OnGmAgentStopPowerReq));

            //获取代理商数据
            RegisterMsgHandler(MSGID.AgentDataReq, new MsgHandler<AgentDataReq>(OnAgentDataReq));

            // gm清理房间数据
            RegisterMsgHandler(MSGID.GmClearRoomReq, new MsgHandler<GmClearRoomReq>(OnGmClearRoomReq));

            // gm清理玩家房间数据
            RegisterMsgHandler(MSGID.GmClearPlayerRoomReq, new MsgHandler<GmClearPlayerRoomReq>(OnGmClearPlayerRoomReq));

            //gm查询代理返利流水
            RegisterMsgHandler(MSGID.GmQueryAgentRebateEventsReq, new MsgHandler<GmQueryAgentRebateEventsReq>(OnGmQueryAgentRebateEventsReq));

            //代理查询返利流水
            RegisterMsgHandler(MSGID.AgentQueryRebateEventsReq, new MsgHandler<AgentQueryRebateEventsReq>(OnAgentQueryRebateEventsReq));

            //gm结算返利
            RegisterMsgHandler(MSGID.GmBettleMentReq, new MsgHandler<GmBettleMentReq>(OnGmSettleMentReq));

            //gm查询下级用户列表
            RegisterMsgHandler(MSGID.GmQuerylowerLevelUserListReq, new MsgHandler<GmQuerylowerLevelUserListReq>(OnGmQuerylowerLevelUserListReq));

            //gm查询用户充值流水
            RegisterMsgHandler(MSGID.GmQueryUserRechargeEventReq, new MsgHandler<GmQueryUserRechargeEventReq>(OnGmQueryUserRechargeEventReq));

            //gm查询用户列表
            RegisterMsgHandler(MSGID.UserInfoListReq, new MsgHandler<UserInfoListReq>(OnUserInfoListReq));

            //代理查询用户列表
            RegisterMsgHandler(MSGID.AgentUserListReq, new MsgHandler<AgentUserListReq>(OnAgentUserListReq));

            //代理修改密码
            RegisterMsgHandler(MSGID.AgentModifyPasswordReq, new MsgHandler<AgentModifyPasswordReq>(OnAgentModifyPasswordReq));

            //测试充值返利
            RegisterMsgHandler(MSGID.TestRechargeReq, new MsgHandler<TestRechargeReq>(OnTestRechargeReq));

            //提现
            RegisterMsgHandler(MSGID.DeductionsMoneyReq, new MsgHandler<DeductionsMoneyReq>(OnDeductionsMoneyReq));

            //转让记录
            RegisterMsgHandler(MSGID.GmQueryTurnRecordReq, new MsgHandler<GmQueryTurnRecordReq>(OnGmQueryTurnRecordReq));
            //扣除记录
            RegisterMsgHandler(MSGID.GmQueryDeductRecordReq, new MsgHandler<GmQueryDeductRecordReq>(OnGmQueryDeductRecordReq));
            //提现记录
            RegisterMsgHandler(MSGID.GmQueryTixianRecordReq, new MsgHandler<GmQueryTixianRecordReq>(OnGmQueryTixianRecordReq));
            //重置密码
            RegisterMsgHandler(MSGID.GmResetServicePasswordReq, new MsgHandler<GmResetServicePasswordReq>(OnGmResetServicePasswordReq));           
            return true;
        }

        /// <summary>
        /// GM修改自己密码
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmModifySelfPasswordReq(UInt32 from, UInt64 accountId, GmModifySelfPasswordReq req)
        {
            GmModifySelfPasswordRsp rsp = new GmModifySelfPasswordRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmModifySelfPasswordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if(!player.IsGm)
            {
                rsp.errorCode = GmModifySelfPasswordRsp.ErrorID.NotGm;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmModifySelfPasswordReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            if (req.newPassword1 != req.newPassword2)
            {
                rsp.errorCode = GmModifySelfPasswordRsp.ErrorID.NewPasswordNotSame;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmModifySelfPasswordReq],新密码不一致,accountId={0}", accountId);
                return;
            }

            player.Account.Password = req.newPassword1;

            player.Save();

            rsp.errorCode = GmModifySelfPasswordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// GM获取服务器当前状态
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmGetServerCurrentStatusReq(UInt32 from, UInt64 accountId, GmGetServerCurrentStatusReq req)
        {
            GmGetServerCurrentStatusRsp rsp = new GmGetServerCurrentStatusRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmGetServerCurrentStatusReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmGetServerCurrentStatusRsp.ErrorID.NotGm;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmGetServerCurrentStatusReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            g.dataCenter.GetStatisticsData(rsp.dayInfo, rsp.historyData);

            rsp.serverInfo.roleCount = g.playerMgr.onlineRoleCount;
            rsp.serverInfo.roomCount = g.roomMgr.onlineRoomCount(ref rsp.serverInfo.pushdownRoomCount, ref rsp.serverInfo.pointRoomCount, ref rsp.serverInfo.lordRoomCount);
            rsp.errorCode = GmGetServerCurrentStatusRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// GM创建代理账号
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmCreateAgentAccountReq(UInt32 from, UInt64 accountId, GmCreateAgentAccountReq req)
        {
            GmCreateAgentAccountRsp rsp = new GmCreateAgentAccountRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],找不到玩家,accountId={0}", accountId);
                return;
            }
            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (b)
            {
                if (!player.IsGm)
                {
                    rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.NotGm;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }
            }
            LobbyPlayer agentPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (agentPlayer == null)
            {
                rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.NotFound;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],没有找到该账号，无此权限,req.accountId={0}", req.accountId);
                return;
            }

            if (agentPlayer.IsGm)
            {
                rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.NotQuanXian;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],不能修改Gm权限,req.accountId={0}", req.accountId);
                return;
            }

            if (req.password1 != req.password2)
            {
                rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.PasswordNotSame;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],创建代理账号，密码不一致,accountId={0}", accountId);
                return;
            }

            if (agentPlayer.Account.accountType == EAccountType.Agent)
            {
                rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.RepeatedCreate;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],重复创建,accountId={0}, proxyAccount={1}", accountId, req.accountId);
                return;
            }

            if (req.name == "")
            {
                rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.NameNotNull;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],姓名不能为空,accountId={0}", accountId);
                return;
            }

           

            agentPlayer.Account.Password = "123456";
            agentPlayer.Account.accountType = EAccountType.Agent;
            agentPlayer.agent.agencyState = eAgencyState.imPower;
            agentPlayer.agent.phone = req.phone;
            agentPlayer.agent.name = req.name;
           // agentPlayer.agent.area = req.area;
           // agentPlayer.agent.oneLevelProportion = req.oneLevelProportion;
           // agentPlayer.agent.twoLevelProportion = req.twoLevelProportion;
            agentPlayer.Save();
            g.agentMgr.AddAgent(agentPlayer.AccountId);

            rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
  
           // AccountKey key = new AccountKey(EPID.Internal, req.account);
            //LobbyPlayer agentPlayer = await g.playerMgr.FindPlayerAsync(key);
            //if (null == agentPlayer)
            //{
            //    agentPlayer = await g.playerMgr.CreateAccountAsync(key, req.password1, EAccountType.Agent);
            //    if (null == agentPlayer)
            //    {
            //        rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.Failed;
            //        player.SendMsgToClient(rsp);

            //        LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],创建代理账号失败,accountId={0}, proxyAccount={1}", accountId, req.account);
            //        return;
            //    }
            //}
            //else
            //{
            //    if (!agentPlayer.IsNull)
            //    {
            //        rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.RepeatedCreate;
            //        player.SendMsgToClient(rsp);

            //        LogSys.Warn("[GmModule][OnGmCreateAgentAccountReq],重复创建,accountId={0}, proxyAccount={1}", accountId, req.account);
            //        return;
            //    }
            //}

            //agentPlayer.agent.agencyState = eAgencyState.imPower;
            //agentPlayer.Save();

            //增加到代理管理器中去
            //g.agentMgr.AddAgent(agentPlayer.AccountId);

            //rsp.errorCode = GmCreateAgentAccountRsp.ErrorID.Success;
            //player.SendMsgToClient(rsp);
        }       

        /// <summary>
        /// GM停用代理账号
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmStopAgentAccountReq(UInt32 from, UInt64 accountId, GmStopAgentAccountReq req)
        {
            GmStopAgentAccountRsp rsp = new GmStopAgentAccountRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmStopAgentAccountReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (b)
            {
                    if (!player.IsGm)
                {
                    rsp.errorCode = GmStopAgentAccountRsp.ErrorID.NotGm;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GmModule][OnGmStopAgentAccountReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }

            }
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountid);
            if (null == exPlayer)
            {
                rsp.errorCode = GmStopAgentAccountRsp.ErrorID.InputAccountNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmStopAgentAccountReq],找不到该代理账号,accountId={0}, agentAccountId", accountId, req.accountid);
                return;
            }

            if (!exPlayer.IsAgent)
            {
                rsp.errorCode = GmStopAgentAccountRsp.ErrorID.InputAccountNotAgent;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmStopAgentAccountReq],该账号不是代理,accountId={0}, agentAccountId", accountId, req.accountid);
                return;
            }

            exPlayer.Account.accountType = EAccountType.Null;
            exPlayer.Save();

            // 从代理管理器删除
            g.agentMgr.RemoveAgent(exPlayer.AccountId);

            rsp.errorCode = GmStopAgentAccountRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// GM修改代理账号密码
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmModifyAgentAccountReq(UInt32 from, UInt64 accountId, GmModifyAgentAccountReq req)
        {
            GmModifyAgentAccountRsp rsp = new GmModifyAgentAccountRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],找不到玩家,accountId={0}", accountId);
                return;
            }
            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (b)
            {          
                if (!player.IsGm)
                {
                    rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.NotGm;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }
            }
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (null == exPlayer)
            {
                rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.InputAccoutNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],找不到该代理账号,accountId={0}, agentAccountId", accountId, req.accountId);
                return;
            }

            if (!exPlayer.IsAgent)
            {
                rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.InputAccountNotAgent;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmStopAgentAccountReq],该账号不是代理,accountId={0}, agentAccountId", accountId, req.accountId);
                return;
            }

            if (req.password1 != req.password2)
            {
                rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.PasswordNotSame;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],修改代理账号密码，密码不一致,accountId={0}", accountId);
                return;
            }

            if (req.area == "")
            {
                rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],地区不能为空，密码不一致,accountId={0}", accountId);
                return;
            }

            //if (req.oneLevelProportion < 0)
            //{
            //    rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.Failed;
            //    player.SendMsgToClient(rsp);

            //    LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],一级分成不能为空，密码不一致,accountId={0}", accountId);
            //    return;
            //}

            //if (req.twoLevelProportion < 0)
            //{
            //    rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.Failed;
            //    player.SendMsgToClient(rsp);

            //    LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],一级分成不能为空，密码不一致,accountId={0}", accountId);
            //    return;
            //}

            exPlayer.Account.Password = req.password1;
            //exPlayer.agent.name = req.name;
           // exPlayer.agent.phone = req.phone;
           // exPlayer.agent.area = req.area;
           // exPlayer.agent.oneLevelProportion = req.oneLevelProportion;
          //  exPlayer.agent.twoLevelProportion = req.twoLevelProportion;
            exPlayer.Save();

            rsp.errorCode = GmModifyAgentAccountRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// GM增加房卡
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmAddRoomCardReq(UInt32 from, UInt64 accountId, GmAddRoomCardReq req)
        {
            GmAddRoomCardRsp rsp = new GmAddRoomCardRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmAddRoomCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmAddRoomCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.roleid);
            if (null == exPlayer)
            {
                rsp.errorCode = GmAddRoomCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmModifyAgentAccountPasswordReq],找不到该操作账号,accountId={0}, exAccountId", accountId, req.roleid);
                return;
            }

            if(exPlayer.IsAgent)
            {
                g.agentMgr.RecordRCAddEvent(exPlayer.AccountId, accountId, req.roomcardCount);
                exPlayer.agent.RoomCardRecharheCount((int)req.roomcardCount);
                //g.agentMgr.RecordRCrechargeEvent(exPlayer.AccountId,accountId, req.roomcardCount);
            }
            g.dataCenter.RecordUserRechargeData(req.roleid, exPlayer.Attribute.NickName, req.roomcardCount);
            exPlayer.AddDiamond(ERCSourceType.Gm, accountId, req.roomcardCount);
            //将充值返利这里注释掉
            //exPlayer.OnRoleRechargeRebate(req.roomcardCount);
            exPlayer.Save();

            rsp.errorCode = GmAddRoomCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            RoomCardChangeNtf ntf = new RoomCardChangeNtf();
            ntf.diamond = exPlayer.GetDiamondCount();
            exPlayer.SendMsgToClient(ntf);
        }

        /// <summary>
        /// GM扣除房卡
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmDecRoomCardReq(UInt32 from, UInt64 accountId, GmDecRoomCardReq req)
        {
            GmDecRoomCardRsp rsp = new GmDecRoomCardRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmDecRoomCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }
                
                if (!player.IsGm)
                {
                    rsp.errorCode = GmDecRoomCardRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GmModule][OnGmDecRoomCardReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }
           
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.roleid);
            if (null == exPlayer)
            {
                rsp.errorCode = GmDecRoomCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmDecRoomCardReq],找不到该操作账号,accountId={0}, exAccountId", accountId, req.roleid);
                return;
            }

            if(exPlayer.HasEnoughDiamond(req.roomcardCount))
            {
                exPlayer.DelDiamond(req.roomcardCount);
            }
            else
            {
                exPlayer.DelDiamond(exPlayer.GetDiamondCount());
            }

            if (exPlayer.IsAgent)
            {
                g.agentMgr.RecordRCReturnEvent(req.roleid, accountId, req.roomcardCount);
            }
         
            exPlayer.Save();

            rsp.errorCode = GmDecRoomCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            RoomCardChangeNtf ntf = new RoomCardChangeNtf();
            ntf.diamond = exPlayer.GetDiamondCount();
            exPlayer.SendMsgToClient(ntf);
        }
        
        /// <summary>
        /// 代理转移房卡
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmTransferRoomCardReq(UInt32 from, UInt64 accountId, GmTransferRoomCardReq req)
        {
            GmTransferRoomCardRsp rsp = new GmTransferRoomCardRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmTransferRoomCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!player.IsAgent)
            {
                rsp.errorCode = GmTransferRoomCardRsp.ErrorID.NotAgent;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmTransferRoomCardReq],不是Agent账号，无此权限,accountId={0}", accountId);
                return;
            }

          
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.roleid);
            if (null == exPlayer)
            {
                rsp.errorCode = GmTransferRoomCardRsp.ErrorID.InputAccountNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmTransferRoomCardReq],找不到该操作账号,accountId={0}, exAccountId={1}", accountId, req.roleid);
                return;
            }

            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (!b)
            {
                if (exPlayer.IsPlayer||exPlayer.IsGm)
                {
                    rsp.errorCode = GmTransferRoomCardRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GmModule][OnGmTransferRoomCardReq],对方不是代理商或运营商,不允许,accountId={0}, exAccountId={1}", accountId, req.roleid);
                    return;
                }
            }
            if (b)
            {
                if (exPlayer.IsAgent|| exPlayer.IsGm)
                {
                    rsp.errorCode = GmTransferRoomCardRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);
                    LogSys.Warn("[GmModule][OnGmTransferRoomCardReq],对方是代理商或运营商,不允许,accountId={0}, exAccountId={1}", accountId, req.roleid);
                    return;
                }
            }


            // 房卡数量不足
            if(!player.HasEnoughDiamond(req.roomcardCount))
            {
                rsp.errorCode = GmTransferRoomCardRsp.ErrorID.RoomCardNotEnough;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmTransferRoomCardReq],找不到该操作账号,accountId={0}, currentDiamond={1}, needDiamond={2}", accountId, player.GetDiamondCount(), req.roomcardCount);
                return;
            }
            GmQueryTurnRecord t = new GmQueryTurnRecord();
            t.service = req.roleid;
            t.name = exPlayer.Attribute.NickName;           
            t.number = req.roomcardCount;
            t.time =DateTime.Now;
            player.makeoverList.Add(t);
            
            g.agentMgr.RecordRCDelEvent(accountId, exPlayer.AccountId, req.roomcardCount);
            
            player.DelDiamond(req.roomcardCount);
            player.Save();

            exPlayer.AddDiamond(ERCSourceType.Agent, accountId, req.roomcardCount);
            exPlayer.Save();

            rsp.errorCode = GmTransferRoomCardRsp.ErrorID.Success;
            rsp.diamond = player.GetDiamondCount();
            player.SendMsgToClient(rsp);

            RoomCardChangeNtf ntf = new RoomCardChangeNtf();
            ntf.diamond = exPlayer.GetDiamondCount();
            exPlayer.SendMsgToClient(ntf);
        }

        /// <summary>
        /// gm发公告
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmPublicNoticeReq(UInt32 from, UInt64 accountId, GmPublicNoticeReq req)
        {
            GmPublicNoticeRsp rsp = new GmPublicNoticeRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmPublicNoticeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmPublicNoticeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmPublicNoticeReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            PublicNoticeNtf ntf = new PublicNoticeNtf();
            ntf.notice = req.notice;
            g.playerMgr.CheckAllPlayer( delegate(LobbyPlayer p)
            {
                if(p.isOnline)
                {
                    p.SendMsgToClient(ntf);
                }
            });

            rsp.errorCode = GmPublicNoticeRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// gm查询流水
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmQueryAgentRoomCardEventsReq(UInt32 from, UInt64 accountId, GmQueryAgentRoomCardEventsReq req)
        {
            GmQueryAgentRoomCardEventsRsp rsp = new GmQueryAgentRoomCardEventsRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmQueryAgentRoomCardEventsReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmQueryAgentRoomCardEventsRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmQueryAgentRoomCardEventsReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            rsp.agentEvents = await g.agentMgr.GetRCEventsAsync(req.roleid, req.strDate);

            rsp.errorCode = GmQueryAgentRoomCardEventsRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 代理查询流水
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnAgemtQueryRoomCardEventsReq(UInt32 from, UInt64 accountId, AgemtQueryRoomCardEventsReq req)
        {
            AgemtQueryRoomCardEventsRsp rsp = new AgemtQueryRoomCardEventsRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnAgemtQueryRoomCardEventsReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            if (!player.IsAgent)
            {
                rsp.errorCode = AgemtQueryRoomCardEventsRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnAgemtQueryRoomCardEventsReq],不是代理，无此权限,accountId={0}", accountId);
                return;
            }

            rsp.agentEvents = await g.agentMgr.GetRCEventsAsync(accountId, req.strDate);

            rsp.errorCode = AgemtQueryRoomCardEventsRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// gm获取玩家信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmGetPlayerInfoReq(UInt32 from, UInt64 accountId, GmGetPlayerInfoReq req)
        {
            GmGetPlayerInfoRsp rsp = new GmGetPlayerInfoRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnGmGetPlayerInfoReq],找不到玩家1,accountId={0}, req.accountId", accountId, req.accountId);
                return;
            }

            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (b)
            {
                if (!player.IsGm)
                {
                    rsp.errorCode = GmGetPlayerInfoRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GmModule][OnGmGetPlayerInfoReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }
            }
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (null == exPlayer)
            {
                rsp.errorCode = GmGetPlayerInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmGetPlayerInfoReq],找不到玩家2,accountId={0}, req.accountId", accountId, req.accountId);
                return;
            }

            if (exPlayer.IsPlayer)
            {
                rsp.errorCode = GmGetPlayerInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmGetPlayerInfoReq],目标不是玩家,accountId={0}, req.accountId", accountId, req.accountId);
                return;
            }
            rsp.diamond = exPlayer.GetDiamondCount();
            rsp.redPacketMoney = exPlayer.redPacketMoney;
            rsp.errorCode = GmGetPlayerInfoRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// gm申请提现
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnDeductionsMoneyReq(UInt32 from, UInt64 accountId, DeductionsMoneyReq req)
        {
            DeductionsMoneyRsp rsp = new DeductionsMoneyRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == player)
            {
                LogSys.Warn("[GmModule][OnDeductionsMoneyReq],找不到玩家1,accountId={0}, req.roleid", accountId, req.roleid);
                return;
            }

            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (b)
            {
                if (!player.IsGm)
                {
                    rsp.errorCode = DeductionsMoneyRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GmModule][OnDeductionsMoneyReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }
            }
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.roleid);
            if (null == exPlayer)
            {
                rsp.errorCode = DeductionsMoneyRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnDeductionsMoneyReq],找不到玩家2,accountId={0}, req.roleid", accountId, req.roleid);
                return;
            }

            if(exPlayer.redPacketMoney <= 0)
            {
                rsp.errorCode = DeductionsMoneyRsp.ErrorID.NotSufficientFunds;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmGetPlayerInfoReq],玩家余额不足,accountId={0}, req.roleid", accountId, req.roleid);
                return;
            }

            exPlayer.redPacketMoney = 0;
            rsp.errorCode = DeductionsMoneyRsp.ErrorID.Success;
            rsp.redPacketMoney = exPlayer.redPacketMoney;
            rsp.playerID = req.roleid;
            GmQueryTurnRecord t = new GmQueryTurnRecord();
            t.service = req.roleid;
            t.name = exPlayer.Attribute.NickName;
            t.number = exPlayer.redPacketMoney;
            t.time =DateTime.Now;
            player.depositList.Add(t);
            exPlayer.Save();
            player.Save();
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 增加代理商房卡
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmAddAgentRoomCardReq(UInt32 from, UInt64 accountId, GmAddAgentRoomCardReq req)
        {
            GmAddAgentRoomCardRsp rsp = new GmAddAgentRoomCardRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAddAgentRoomCardReq],找不到Gm,accountId={0}, req.accountId={1}", accountId, req.accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmAddAgentRoomCardRsp.ErrorID.NotIsGm;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAddAgentRoomCardReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (exPlayer == null)
            {
                rsp.errorCode = GmAddAgentRoomCardRsp.ErrorID.NotFound;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAddAgentRoomCardReq],找不到代理商,accountId={0}, req.accountId={1}", accountId, req.accountId);
                return;
            }

            if (!exPlayer.IsAgent)
            {
                rsp.errorCode = GmAddAgentRoomCardRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAddAgentRoomCardReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            g.agentMgr.RecordRCAddEvent(exPlayer.AccountId, accountId, req.roomcardCount);            

            exPlayer.AddDiamond(ERCSourceType.Gm, accountId, req.roomcardCount);
            exPlayer.agent.RoomCardRecharheCount((int)req.roomcardCount);
            GmQueryTurnRecord t = new GmQueryTurnRecord();
            t.service = req.accountId;
            t.name = exPlayer.Attribute.NickName;
            t.number = req.roomcardCount;
            t.time = DateTime.Now;
            player.makeoverList.Add(t);
            player.Save();
            exPlayer.Save();
            rsp.errorCode = GmAddAgentRoomCardRsp.ErrorID.Success;
            rsp.diamond = exPlayer.GetDiamondCount();
            player.SendMsgToClient(rsp);

            RoomCardChangeNtf ntf = new RoomCardChangeNtf();
            ntf.diamond = exPlayer.GetDiamondCount();
            exPlayer.SendMsgToClient(ntf);
        }

        /// <summary>
        /// 减少代理商房卡
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmDecAgentRoomCardReq(UInt32 from,UInt64 accountId,GmDecAgentRoomCardReq req)
        {
            GmDecAgentRoomCardRsp rsp = new GmDecAgentRoomCardRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnGmDecAgentRoomCardReq],找不到Gm,accountId={0}, req.accountId={1}", accountId, req.accountId);
                return;
            }
            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (b)
            {          
                if (!player.IsGm)
                {
                    rsp.errorCode = GmDecAgentRoomCardRsp.ErrorID.NotIsGm;
                    player.SendMsgToClient(rsp);
                    LogSys.Warn("[GmModule][OnGmDecAgentRoomCardReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }
            }
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (exPlayer == null)
            {
                rsp.errorCode = GmDecAgentRoomCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmDecAgentRoomCardReq],没有找到玩家，无此权限,accountId={0}", accountId);
                return;
            }

            if (!exPlayer.IsAgent)
            {
                rsp.errorCode = GmDecAgentRoomCardRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmDecAgentRoomCardReq],目标不是代理商，无此权限,accountId={0}", accountId);
                return;
            }
            GmQueryTurnRecord t = new GmQueryTurnRecord();
            t.service = req.accountId;
            t.name = exPlayer.Attribute.NickName;
           
            if (exPlayer.HasEnoughDiamond(req.roomcardCount))
            {
                exPlayer.DelDiamond(req.roomcardCount);
                t.number = req.roomcardCount;
            }
            else
            {
                exPlayer.DelDiamond(exPlayer.GetDiamondCount());
                t.number = exPlayer.GetDiamondCount();
            }
            if (!b)
            {
                player.AddDiamond(ERCSourceType.System, 0, req.roomcardCount);
            }
            t.time = DateTime.Now;
            player.deductList.Add(t);
            exPlayer.Save();
            player.Save();
            rsp.errorCode = GmDecAgentRoomCardRsp.ErrorID.Success;
            rsp.diamond = exPlayer.GetDiamondCount();
            player.SendMsgToClient(rsp);
            

            RoomCardChangeNtf ntf = new RoomCardChangeNtf();
            ntf.diamond = exPlayer.GetDiamondCount();
            exPlayer.SendMsgToClient(ntf);
        }

        /// <summary>
        /// 获取代理商列表
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmGetAgentListReq(UInt32 from,UInt64 accountId, GmGetAgentListReq req)
        {
            GmGetAgentListRsp rsp = new GmGetAgentListRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnGetAgentListReq],找不到Gm,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmGetAgentListRsp.ErrorID.NotIsGM;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGetAgentListReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            List<UInt64> list = await g.agentMgr.GetAgentListAsync();

            if (list.Count == 0)
            {
                rsp.errorCode = GmGetAgentListRsp.ErrorID.NoAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGetAgentListReq],没有代理商,accountId={0}", accountId);
                return;
            }
            foreach (var item in list)
            {
                LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(item);

                if (exPlayer == null)
                {
                    LogSys.Warn("[GmModule][OnGetAgentListReq],找不到代理商,accountId={0}", item);
                    continue;
                }
                if (!exPlayer.IsAgent)
                {
                    LogSys.Warn("[GmModule][OnGetAgentListReq],不是代理商,accountId={0}", item);
                    continue;
                }
                stAgentData angent = new stAgentData();
                angent.accountId = exPlayer.AccountId;
                angent.agencyState = exPlayer.agent.agencyState;
                angent.roomCard =(int)exPlayer.GetDiamondCount();
                angent.roomCardConsume = exPlayer.agent.roomCardConsume;
                angent.roomCardRecharge = exPlayer.agent.roomCardRecharge;
                angent.money = exPlayer.agent.money;
                angent.name = exPlayer.agent.name;
                angent.phone = exPlayer.agent.phone;
                angent.area = exPlayer.agent.area;
                angent.lowerLevelUsersCount = exPlayer.Attribute.setInviteRoles.Count;
               // angent.oneLevelProportion = exPlayer.agent.oneLevelProportion;
               // angent.twoLevelProportion = exPlayer.agent.twoLevelProportion;
                angent.password = exPlayer.Account.Password;
                rsp.agentData.Add(angent);
            }
            rsp.errorCode = GmGetAgentListRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 授权
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmAgentImpowerReq(UInt32 from,UInt64 accountId, GmAgentImpowerReq req)
        {
            GmAgentImpowerRsp rsp = new GmAgentImpowerRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAgentImpowerReq],找不到Gm账号,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmAgentImpowerRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentImpowerReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (exPlayer == null)
            {
                rsp.errorCode = GmAgentImpowerRsp.ErrorID.NotFound;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentImpowerReq],找不到代理商,accountId={0}", req.accountId);
                return;
            }

            if (!exPlayer.IsAgent)
            {
                rsp.errorCode = GmAgentImpowerRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentImpowerReq],不是代理商,accountId={0}", req.accountId);
                return;
            }
            exPlayer.agent.Impower();
            exPlayer.Save();

            rsp.errorCode = GmAgentImpowerRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 停权
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmAgentStopPowerReq(UInt32 from, UInt64 accountId, GmAgentStopPowerReq req)
        {
            GmAgentStopPowerRsp rsp = new GmAgentStopPowerRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAgentStopPowerReq],找不到Gm账号,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmAgentStopPowerRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentStopPowerReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (exPlayer == null)
            {
                rsp.errorCode = GmAgentStopPowerRsp.ErrorID.NotFound;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentStopPowerReq],找不到代理商,accountId={0}", req.accountId);
                return;
            }

            if (!exPlayer.IsAgent)
            {
                rsp.errorCode = GmAgentStopPowerRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentStopPowerReq],不是代理商,accountId={0}", req.accountId);
                return;
            }

            exPlayer.agent.StopPower();
            exPlayer.Save();

            rsp.errorCode = GmAgentStopPowerRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 获取代理数据
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnAgentDataReq(UInt32 from, UInt64 accountId, AgentDataReq req)
        {
            AgentDataRsp rsp = new AgentDataRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnGetAgentDataReq],账号不存在,accountId={0}", accountId);
                return;
            }

            if (!player.IsAgent)
            {
                rsp.errorCode = AgentDataRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGetAgentDataReq],不是代理商,accountId={0}", accountId);
                return;
            }
          

            if (player.IsGm|| player.AccountId == 10022 || player.AccountId == 10023)
            {
                LobbyPlayer eplayer = await g.playerMgr.FindPlayerAsync(req.accountId);
                if (eplayer == null)
                {
                    LogSys.Warn("[GmModule][OnGetAgentDataReq],账号不存在,accountId={0}", accountId);
                    return;
                }
                if (!eplayer.IsAgent)
                {
                    rsp.errorCode = AgentDataRsp.ErrorID.NotIsAgent;
                    player.SendMsgToClient(rsp);
                    LogSys.Warn("[GmModule][OnGetAgentDataReq],目标不是代理商,accountId={0}", accountId);
                    return;
                }
                rsp.agencyState = eplayer.agent.agencyState;
                rsp.roomCardConsume = eplayer.agent.roomCardConsume;
                rsp.roomCardRecharge = eplayer.agent.roomCardRecharge;
                rsp.money = eplayer.redPacketMoney;
                rsp.userCount = (uint)eplayer.Attribute.setInviteRoles.Count;
                rsp.totalMoney = eplayer.agent.totalMoney;
                rsp.errorCode = AgentDataRsp.ErrorID.Success;
                rsp.accountId = req.accountId;
                rsp.name = eplayer.agent.name;
                UInt32 roomcards = 0;
                foreach (var card in eplayer.Attribute.lstRoomCards)
                {
                    roomcards += card.roomCardCount;
                }
                rsp.roomCard = roomcards;
                player.SendMsgToClient(rsp);

            }
            else
            {
                rsp.agencyState = player.agent.agencyState;
                rsp.roomCardConsume = player.agent.roomCardConsume;
                rsp.roomCardRecharge = player.agent.roomCardRecharge;
                rsp.money = player.redPacketMoney;
                rsp.userCount = (uint)player.Attribute.setInviteRoles.Count;
                rsp.totalMoney = player.agent.totalMoney;
                rsp.errorCode = AgentDataRsp.ErrorID.Success;
                rsp.accountId = accountId;
                rsp.name = player.Account.Account;
                UInt32 roomcards = 0;
                foreach (var card in player.Attribute.lstRoomCards)
                {
                    roomcards += card.roomCardCount;
                }
                rsp.roomCard = roomcards;
                player.SendMsgToClient(rsp);
            }
            
        }

        /// <summary>
        /// 清理房间数据
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmClearRoomReq(UInt32 from, UInt64 accountId, GmClearRoomReq req)
        {
            GmClearRoomRsp rsp = new GmClearRoomRsp();

            LobbyPlayer playerGm= await g.playerMgr.FindPlayerAsync(accountId);
            if (null == playerGm)
            {
                LogSys.Warn("[GmModule][OnGmClearRoomReq],找不到玩家1,accountId={0}, req.roomUuid={1}", accountId, req.roomUuid);
                return;
            }

            if (!playerGm.IsGm)
            {
                rsp.errorCode = GmClearRoomRsp.ErrorID.Failed;
                playerGm.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmClearRoomReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            // 通知battle清理房间数据
            GmClearRoomL2B ntfBattle = new GmClearRoomL2B();
            ntfBattle.roomUuid = req.roomUuid;
            env.BusService.SendMessage("battle", ntfBattle);

            rsp.errorCode = GmClearRoomRsp.ErrorID.Success;
            playerGm.SendMsgToClient(rsp);
        }

        async void OnGmClearPlayerRoomReq(UInt32 from, UInt64 accountId, GmClearPlayerRoomReq req)
        {
            GmClearPlayerRoomRsp rsp = new GmClearPlayerRoomRsp();

            LobbyPlayer playerGm = await g.playerMgr.FindPlayerAsync(accountId);
            if (null == playerGm)
            {
                LogSys.Warn("[GmModule][OnGmClearPlayerRoomReq],找不到gm,accountId={0}, req.roleid", accountId, req.roleid);
                return;
            }

            if (!playerGm.IsGm)
            {
                rsp.errorCode = GmClearPlayerRoomRsp.ErrorID.Failed;
                playerGm.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmClearPlayerRoomReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.roleid);
            if (null == exPlayer)
            {
                rsp.errorCode = GmClearPlayerRoomRsp.ErrorID.Failed;
                playerGm.SendMsgToClient(rsp);

                LogSys.Warn("[GmModule][OnGmClearPlayerRoomReq],找不到玩家,accountId={0}, req.roleid={1}", accountId, req.roleid);
                return;
            }

            UInt64 roomUuid = exPlayer.room.roomUuid;
            if(roomUuid == 0)
            {
                rsp.errorCode = GmClearPlayerRoomRsp.ErrorID.Success;
                playerGm.SendMsgToClient(rsp);

                LogSys.Info("[GmModule][OnGmClearPlayerRoomReq],玩家不在房间里,accountId={0}, req.roleid={1}", accountId, req.roleid);
                return;
            }

            exPlayer.room.createRoomUuid = 0;
            exPlayer.room.roomUuid = 0;

            // 通知battle清理房间数据
            GmClearRoomL2B ntfBattle = new GmClearRoomL2B();
            ntfBattle.roomUuid = roomUuid;
            env.BusService.SendMessage("battle", ntfBattle);

            rsp.errorCode = GmClearPlayerRoomRsp.ErrorID.Success;
            playerGm.SendMsgToClient(rsp);
        }

        /// <summary>
        /// gm查询代理返利流水
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmQueryAgentRebateEventsReq(UInt32 from, UInt64 accountId, GmQueryAgentRebateEventsReq req)
        {
            GmQueryAgentRebateEventsRsp rsp = new GmQueryAgentRebateEventsRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnGmQueryAgentRebateEventsReq],找不到Gm,accountId={0}", accountId);
                return;
            }
            bool b = true;
            if (player.AccountId == 10022 || player.AccountId == 10023)
            {
                b = false;
            }
            if (b)
            {
                if (!player.IsGm)
                {
                    rsp.errorCode = GmQueryAgentRebateEventsRsp.ErrorID.NotIsGm;
                    player.SendMsgToClient(rsp);
                    LogSys.Warn("[GmModule][OnGmQueryAgentRebateEventsReq],不是GM账号，无此权限,accountId={0}", accountId);
                    return;
                }
            }
           
            LobbyPlayer agentPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (agentPlayer == null)
            {
                rsp.errorCode = GmQueryAgentRebateEventsRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmQueryAgentRebateEventsReq],没有找到代理商账号，无此权限,accountId={0}", accountId);
                return;
            }

            rsp.id = agentPlayer.AccountId;
            rsp.name = agentPlayer.Account.Account;
            rsp.money = agentPlayer.redPacketMoney;
            rsp.errorCode = GmQueryAgentRebateEventsRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 代理查询返利流水
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnAgentQueryRebateEventsReq(UInt32 from, UInt64 accountId, AgentQueryRebateEventsReq req)
        {
            AgentQueryRebateEventsRsp rsp = new AgentQueryRebateEventsRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAgentQueryRebateEventsReq],找不到代理商,accountId={0}", accountId);
                return;
            }

            if (!player.IsAgent)
            {
                rsp.errorCode = AgentQueryRebateEventsRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentQueryRebateEventsReq],不是代理商账号，无此权限,accountId={0}", accountId);
                return;
            }

            rsp.rebateEvent = await g.agentMgr.GetRCEventsRebateAsync(accountId, req.strDate);
            rsp.errorCode = AgentQueryRebateEventsRsp.ErrorID.Success;          
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 返利结算
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmSettleMentReq(UInt32 from, UInt64 accountId, GmBettleMentReq req)
        {
            GmBettleMentRsp rsp = new GmBettleMentRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);

            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAgemtQueryRoomCardEventsReq],找不到Gm,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmBettleMentRsp.ErrorID.NotIsGm;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            LobbyPlayer agentPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);

            if (agentPlayer == null)
            {
                rsp.errorCode = GmBettleMentRsp.ErrorID.NotFound;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],代理商未找到,req.accountId={0}", req.accountId);
                return;
            }

            if (!agentPlayer.IsAgent)
            {
                rsp.errorCode = GmBettleMentRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],该账号没有代理商权限,req.accountId={0}", req.accountId);
                return;
            }

            double money = req.money;
            if (agentPlayer.agent.money < money)
            {
                rsp.errorCode = GmBettleMentRsp.ErrorID.MoneyNotEnough;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],提现金额不足,accountId={0} money ={1}", req.accountId, player.agent.money);
                return;
            }

            g.agentMgr.RecorDRCEventWithDraw(req.accountId, money);
            agentPlayer.agent.money = Math.Round(agentPlayer.agent.money - money, 2);
            agentPlayer.Save();

            rsp.errorCode = GmBettleMentRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// gm查询代理下级用户信息列表
        /// </summary>
        /// <param name="form"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmQuerylowerLevelUserListReq(UInt32 from, UInt64 accountId, GmQuerylowerLevelUserListReq req)
        {
            GmQuerylowerLevelUserListRsp rsp = new GmQuerylowerLevelUserListRsp();

            LobbyPlayer agentPlayer;
            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAgemtQueryRoomCardEventsReq],找不到Gm,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmQuerylowerLevelUserListRsp.ErrorID.NotIsGm;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmQuerylowerLevelUserListReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            agentPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (agentPlayer == null)
            {
                rsp.errorCode = GmQuerylowerLevelUserListRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmQuerylowerLevelUserListReq],代理账号不存在,accountId={0}",req.accountId);
                return;
            }

            if (!agentPlayer.IsAgent)
            {
                rsp.errorCode = GmQuerylowerLevelUserListRsp.ErrorID.NotIsAgent;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmQuerylowerLevelUserListReq],不是代理账号，无此权限,accountId={0}", req.accountId);
                return;
            }

            foreach (var roleId in agentPlayer.Attribute.setInviteRoles)
            {
                LobbyPlayer lowerPlayer = await g.playerMgr.FindPlayerAsync(roleId);

                if (lowerPlayer == null)
                {
                    continue;
                }
                stUserData LowerUserData = new stUserData();
                LowerUserData.accountId = roleId;
                LowerUserData.name = lowerPlayer.Attribute.NickName;
                LowerUserData.roomCard = lowerPlayer.GetDiamondCount();

                rsp.lstUserData.Add(LowerUserData);
            }

            rsp.errorCode = GmQuerylowerLevelUserListRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 用户列表
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnUserInfoListReq(UInt32 from,UInt64 accountId, UserInfoListReq req)
        {
            UserInfoListRsp rsp = new UserInfoListRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnGmQueryUserRechargeEventReq],找不到Gm,accountId={0}", accountId);
                return;
            }

            if (req.accountId != 0)
            {
                LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
                if (exPlayer == null)
                {
                    rsp.errorCode = UserInfoListRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);
                    LogSys.Warn("[GmModule][OnUserInfoListReq],玩家不存在,accountId={0}", req.accountId);
                    return;
                }

                stUserData userData = new stUserData();
                userData.accountId = exPlayer.Account.AccountId;
                userData.name = exPlayer.Attribute.NickName;
                userData.roomCard = exPlayer.GetDiamondCount();
                rsp.lstUserData.Add(userData);

                rsp.totalPage = 1;
                rsp.errorCode = UserInfoListRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);
                return;
            }

            if (req.page == 0)
            {
                rsp.errorCode = UserInfoListRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnUserInfoListReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            UInt64 maxAcount = await g.playerMgr.GetMaxAccountId();
            if (maxAcount < ConstValue.MIN_ACCOUNT_ID)
            {
                rsp.errorCode = UserInfoListRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnUserInfoListReq],最大账号id错误,accountId={0}", accountId);
                return;
            }

            int remainder =(int)((maxAcount - ConstValue.MIN_ACCOUNT_ID) % ConstValue.ONE_PAGE_COUNT );
            int totalPage = (int)((maxAcount - ConstValue.MIN_ACCOUNT_ID) / ConstValue.ONE_PAGE_COUNT);

            if (remainder != 0)
            {
                totalPage++;
            }

            if(totalPage < 1)
            {
                rsp.errorCode = UserInfoListRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnUserInfoListReq],页数错误,accountId={0}", accountId);
                return;
            }

            UInt64 initAccountId = (ConstValue.MIN_ACCOUNT_ID + (UInt64)((req.page - 1) * ConstValue.ONE_PAGE_COUNT)) + 1;
            for (UInt64 i = initAccountId; i < initAccountId + ConstValue.ONE_PAGE_COUNT; i++)
            {
                LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(i);
                if (exPlayer == null)
                {
                    continue;
                }

                if (exPlayer.IsGm)
                {
                    continue;
                }

                stUserData userData = new stUserData();
                userData.accountId = exPlayer.Account.AccountId;
                userData.name = exPlayer.Attribute.NickName;
                userData.roomCard = exPlayer.GetDiamondCount();

                rsp.lstUserData.Add(userData);
            }
            rsp.totalPage = totalPage;
            rsp.errorCode = UserInfoListRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// gm查询用户充值流水
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmQueryUserRechargeEventReq(UInt32 from,UInt64 accountId, GmQueryUserRechargeEventReq req)
        {
            GmQueryUserRechargeEventRsp rsp = new GmQueryUserRechargeEventRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnGmQueryUserRechargeEventReq],找不到Gm,accountId={0}", accountId);
                return;
            }

            if (!player.IsGm)
            {
                rsp.errorCode = GmQueryUserRechargeEventRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmQueryUserRechargeEventReq],不是GM账号,没有权限,accountId={0}", accountId);
                return;
            }

            if (req.isCheckAll)
            {
                if (req.page <= 0)
                {
                    rsp.errorCode = GmQueryUserRechargeEventRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);
                    LogSys.Warn("[GmModule][OnGmQueryUserRechargeEventReq],请求查看的页数错误,accountId={0}", accountId);
                    return;
                }

                g.dataCenter.GetLstUserRechargeData(rsp.userRechargeDate, req.page);
                if (rsp.userRechargeDate.Count <= 0)
                {
                    rsp.errorCode = GmQueryUserRechargeEventRsp.ErrorID.Success;
                    player.SendMsgToClient(rsp);
                    LogSys.Debug("[GmModule][OnGmQueryUserRechargeEventReq],未搜索到用户数据,accountId={0}", accountId);
                    return;
                }

                rsp.totalPage = g.dataCenter.GetPageCount();
                rsp.errorCode = GmQueryUserRechargeEventRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);
                return;
            }

            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.accountId);
            if (exPlayer == null)
            {
                rsp.errorCode = GmQueryUserRechargeEventRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmQueryUserRechargeEventReq],搜索条件错误,accountId={0}", accountId);
                return;
            }

            if (req.strDate == "")
            {
                rsp.errorCode = GmQueryUserRechargeEventRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmQueryUserRechargeEventReq],搜索条件错误,accountId={0}", req.strDate);
                return;
            }

            g.dataCenter.GetLstUserRechargeData(rsp.userRechargeDate, req.strDate, req.accountId);
            rsp.totalPage = 1;
            rsp.errorCode = GmQueryUserRechargeEventRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 代理商查询用户列表
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnAgentUserListReq(UInt32 from,UInt64 accountId, AgentUserListReq req)
        {
            AgentUserListRsp rsp = new AgentUserListRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAgentUserListReq],找不到Gm,accountId={0}", accountId);
                return;
            }

            if (player.Attribute.setInviteRoles.Count <= 0)
            {
                rsp.errorCode = AgentUserListRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentUserListReq],没有数据,accountId={0}", accountId);
                return;
            }

            foreach (var roleId in player.Attribute.setInviteRoles)
            {
                LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(roleId);
                if (exPlayer == null)
                {
                    continue;
                }
                stUserData data = new stUserData();
                data.name = exPlayer.Attribute.NickName;
                data.roomCard = exPlayer.GetDiamondCount();
                data.accountId = roleId;

                rsp.lstUserData.Add(data);
            }

            rsp.errorCode = AgentUserListRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        async void OnAgentModifyPasswordReq(UInt32 from,UInt64 accountId, AgentModifyPasswordReq req)
        {
            AgentModifyPasswordRsp rsp = new AgentModifyPasswordRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][OnAgentModifyPasswordReq],找不到Gm,accountId={0}", accountId);
                return;
            }

            if (player.IsGm)
            {
                rsp.errorCode = AgentModifyPasswordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentModifyPasswordReq],账号权限错误,accountId={0}", accountId);
                return;
            }

            if (!player.IsAgent)
            {
                rsp.errorCode = AgentModifyPasswordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentModifyPasswordReq],账号权限不是代理商权限,accountId={0}", accountId);
                return;
            }

            if (req.password != req.password1)
            {
                rsp.errorCode = AgentModifyPasswordRsp.ErrorID.NotEqual;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnAgentModifyPasswordReq],俩次密码不一致,accountId={0}", accountId);
                return;
            }

            player.Account.Password = req.password;
            player.Save();
            rsp.errorCode = AgentModifyPasswordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 测试可提现金额
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnTestRechargeReq(UInt32 from, UInt64 accountId, TestRechargeReq req)
        {
            TestRechargeRsp rsp = new TestRechargeRsp();
            LobbyPlayer self = await g.playerMgr.FindPlayerAsync(accountId);
            if (self == null)
            {
                LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", accountId);
                rsp.errorCode = TestRechargeRsp.ErrorID.Failed;
                self.SendMsgToClient(rsp);
                return;
            }

            if (!self.IsGm)
            {
                rsp.errorCode = TestRechargeRsp.ErrorID.NotIsGm;
                self.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],不是GM账号，无此权限,accountId={0}", accountId);
                return;
            }

            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.roleid);
            if (null == exPlayer)
            {
                rsp.errorCode = TestRechargeRsp.ErrorID.Failed;
                self.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],目标玩家不存在,accountId={0}", accountId);
                return;
            }

            t_recharge config = g.staticDataMgr.rechargeData.GetConfig(req.rechargeid);
            if (config == null)
            {
                rsp.errorCode = TestRechargeRsp.ErrorID.Failed;
                self.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],找不到充值ID,accountId={0}, rechageid={1}", accountId, req.rechargeid);
                return;
            }

            if (config.f_itemid == 1)
            {
                exPlayer.AddDiamond(ERCSourceType.Mall, exPlayer.AccountId, (uint)config.f_count);
                exPlayer.OnRoleRechargeRebate(config.f_price / 100);
                g.dataCenter.RecordUserRechargeData(exPlayer.AccountId, exPlayer.Attribute.NickName, (uint)config.f_price / 100);
                g.dataCenter.TotalHistoryRechargeRoomCardCount(config.f_count);
                exPlayer.Save();           
            }
            else
            {
                LogSys.Warn("[GmModule][TestRechargeRspeq],未知错误,accountId={0}", accountId);
                rsp.errorCode = TestRechargeRsp.ErrorID.Failed;
                self.SendMsgToClient(rsp);
            }

            LogSys.Debug("[GmModule][TestRechargeRspeq],成功,accountId={0}", accountId);
            rsp.errorCode = TestRechargeRsp.ErrorID.Success;
            self.SendMsgToClient(rsp);
        }
        ///转让记录返回
        async void OnGmQueryTurnRecordReq(UInt32 from, UInt64 accountId, GmQueryTurnRecordReq req)
        {
            GmQueryTurnRecordRsp rsp = new GmQueryTurnRecordRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", accountId);
                rsp.errorCode = GmQueryTurnRecordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                return;
            }
            if (req.isSelf)
            {
                rsp.list = player.makeoverList;
                player.SendMsgToClient(rsp);
            }
            else
            {

                LobbyPlayer eplayer = await g.playerMgr.FindPlayerAsync(req.roleid);
                if (eplayer == null)
                {
                    LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", req.roleid);
                    rsp.errorCode = GmQueryTurnRecordRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);
                    return;
                }

                rsp.list = eplayer.makeoverList;
                player.SendMsgToClient(rsp);
            }
        }

        ///扣除记录返回
        async void OnGmQueryDeductRecordReq(UInt32 from, UInt64 accountId, GmQueryDeductRecordReq req)
        {
            GmQueryDeductRecordRsp rsp = new GmQueryDeductRecordRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", accountId);
                rsp.errorCode = GmQueryDeductRecordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                return;
            }
            if (req.isSelf)
            {
                rsp.list = player.deductList;
                player.SendMsgToClient(rsp);
            }
            else
            {

                LobbyPlayer eplayer = await g.playerMgr.FindPlayerAsync(req.roleid);
                if (eplayer == null)
                {
                    LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", req.roleid);
                    rsp.errorCode = GmQueryDeductRecordRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);
                    return;
                }

                rsp.list = eplayer.deductList;
                player.SendMsgToClient(rsp);
            }
        }

        ///提现记录返回
        async void OnGmQueryTixianRecordReq(UInt32 from, UInt64 accountId, GmQueryTixianRecordReq req)
        {
            GmQueryTixianRecordRsp rsp = new GmQueryTixianRecordRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", accountId);
                rsp.errorCode = GmQueryTixianRecordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                return;
            }

            if (req.isSelf)
            {
                rsp.list = player.depositList;
                player.SendMsgToClient(rsp);
            }
            else
            {

                LobbyPlayer eplayer = await g.playerMgr.FindPlayerAsync(req.roleid);
                if (eplayer == null)
                {
                    LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", req.roleid);
                    rsp.errorCode = GmQueryTixianRecordRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);
                    return;
                }

                rsp.list= eplayer.depositList;
                player.SendMsgToClient(rsp);
            }
           
                         
        }


        ///重置密码返回
        async void OnGmResetServicePasswordReq(UInt32 from, UInt64 accountId, GmResetServicePasswordReq req)
        {
            GmResetServicePasswordRsp rsp = new GmResetServicePasswordRsp();

            LobbyPlayer player = await g.playerMgr.FindPlayerAsync(accountId);
            if (player == null)
            {
                LogSys.Warn("[GmModule][TestRechargeReq],找不到该玩家,accountId={0}", accountId);
                rsp.errorCode = GmResetServicePasswordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                return;
            }
            LobbyPlayer exPlayer = await g.playerMgr.FindPlayerAsync(req.i);
            if (null == exPlayer)
            {
                rsp.errorCode = GmResetServicePasswordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);
                LogSys.Warn("[GmModule][OnGmSettleMentReq],目标玩家不存在,accountId={0}", accountId);
                return;
            }
            if (!player.IsGm)
            {          
              if (exPlayer.AccountId == 10022 || exPlayer.AccountId == 10023 || exPlayer.IsGm)
                {
                    rsp.errorCode = GmResetServicePasswordRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);
                    LogSys.Warn("[GmModule][OnGmSettleMentReq],权限不够,accountId={0}", exPlayer.AccountId);
                    return;
                }
            }
            exPlayer.Account.Password = "123456";
            exPlayer.Save();
            player.SendMsgToClient(rsp);
        }


    }

}
    
