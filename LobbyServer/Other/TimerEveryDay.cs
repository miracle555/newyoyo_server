﻿using Entity;
using Log;
using Protocols;
using Redis;
using ServerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LobbyServer.Other
{
    public class TimerEveryDay
    {
        public TimerEveryDay()
        {
        }

        public void start()
        {
            Timer timer = new Timer();
            timer.Enabled = true;
            timer.Interval = 60000;
            timer.Start();
            timer.Elapsed += new ElapsedEventHandler(test);
            LogSys.Warn("[TimerEveryDay][TimerEveryDay],启动定时任务！");
        }

        async private void test(object source, ElapsedEventArgs e)
        {
            //AccountKey accountKey = new AccountKey(EPID.Internal ,"gm");

            //生成客服
            //LobbyPlayer player = await g.playerMgr.FindOrCreatePlayerAsync(accountKey, "longmen");
            if (DateTime.Now.Hour == 0 && DateTime.Now.Minute == 1)
            {
                LogSys.Warn("[TimerEveryDay][test],每天晚上12点，检测可以启动定时器成功！");
                //每日执行定时器
                timerActiveValueAsync();
            }
            //if (DateTime.Now.Hour == 17 && DateTime.Now.Minute == 13)
            //{
            //    AccountKey accKey4 = new AccountKey(EPID.Internal, "A");
            //    LobbyPlayer player4 = await g.playerMgr.CreateAccountAsync(accKey4, "1234", EAccountType.Gm);
              
            //    AccountKey accKey = new AccountKey(EPID.Internal, "z");
            //    LobbyPlayer player = await g.playerMgr.CreateAccountAsync(accKey, "1234", EAccountType.Agent);            
            //    LogSys.Warn("z,accountId={0}", player.AccountId);

            //    AccountKey accKey1 = new AccountKey(EPID.Internal, "x");
            //    LobbyPlayer player1 = await g.playerMgr.CreateAccountAsync(accKey1, "1234", EAccountType.Agent);
                
            //    LogSys.Warn("x,accountId={0}", player1.AccountId);
            //    AccountKey accKey2 = new AccountKey(EPID.Internal, "c");
            //    LobbyPlayer player2 = await g.playerMgr.CreateAccountAsync(accKey2, "1234", EAccountType.Agent);
                
            //    LogSys.Warn("c,accountId={0}", player2.AccountId);
            //    AccountKey accKey3 = new AccountKey(EPID.Internal, "v");
            //    LobbyPlayer player3 = await g.playerMgr.CreateAccountAsync(accKey3, "1234", EAccountType.Player);
               

            //    LogSys.Warn("v,accountId={0}", player3.AccountId);
            //    player.AddDiamond(ERCSourceType.System, 0, 1000);
            //    player.Save();
                
            //    player1.AddDiamond(ERCSourceType.System, 0, 1000);
            //    player1.Save();
                
            //    player2.AddDiamond(ERCSourceType.System, 0, 1000);
            //    player2.Save();
                
            //    player3.AddDiamond(ERCSourceType.System, 0, 1000);
            //    player3.redPacketMoney = 10000;
            //    player3.Save();

            //    player4.AddDiamond(ERCSourceType.System, 0, 1000);
            //    player4.Save();

            //}
        }

        private async void timerActiveValueAsync()
        {
            //获取最大ID
            UInt64 maxAcount = await g.playerMgr.GetMaxAccountId();
            for(UInt64 i = 10001; i <= maxAcount; i++)
            {
                LobbyPlayer player = await g.playerMgr.FindPlayerAsync(i);
                if(player == null)
                {
                    continue;
                }
                player.playRoundCount = 0;
                player.readyNumber = 0;
                player.luckDrawCount = 0;
                player.hasShare = false;
                player.Save();
            }
        }
    }
}
