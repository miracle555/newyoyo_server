﻿using System;
using System.Collections.Generic;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Event;
using LobbyServer;
using Entity;
using Protocols.gm;
using Statistics;

namespace Room
{
    [ProtoContract]
    public class RechargeMgr
    {
        /// <summary>
        /// 支付订单列表
        /// </summary>
        [ProtoMember(1)]
        List<PayOrder> lstPayOrder = new List<PayOrder>();

        /// <summary>
        /// 待处理订单
        /// </summary>
        Dictionary<string, PayOrder> mapPayDatas = new Dictionary<string, PayOrder>();

        const string PayModuleLogName = "PayModule";

        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="out_tarde_no"></param>
        /// <param name="recharge_id"></param>
        /// <param name="recharge_price"></param>
        /// <param name="prepay_id"></param>
        /// <param name="create_date"></param>
        /// <param name="playerId"></param>
        /// <param name="playerName"></param>
        public PayOrder AddPayOrder(string out_tarde_no, uint recharge_id, long recharge_price, string prepay_id, string create_date, ulong playerId, string playerName)
        {
            PayOrder payorder = new PayOrder();
            payorder.order_status = 0;
            payorder.out_trade_no = out_tarde_no;
            payorder.recharge_id = recharge_id;
            payorder.recharge_price = recharge_price;
            payorder.prepay_id = prepay_id;
            payorder.create_date = create_date;
            payorder.playerId = playerId;
            payorder.playerName = playerName;
            lstPayOrder.Add(payorder);
            mapPayDatas[payorder.out_trade_no] = payorder;
            SaveDb();
            return payorder;
        }

        /// <summary>
        /// 处理支付网关监听到的订单
        /// </summary>
        /// <param name="out_trade_no"></param>
        /// <param name="total_fee"></param>
        /// <param name="complete_date"></param>
        /// <param name="transaction_id"></param>
        /// <param name="paySuccess"></param>
        async public void ProcessPayNotify(string out_trade_no, string total_fee, string complete_date, string transaction_id, bool paySuccess)
        {
            if (mapPayDatas.ContainsKey(out_trade_no))
            {
                PayOrder payorder = mapPayDatas[out_trade_no];
                if (payorder.order_status != 0)
                {
                    LogSys.WarnModule(PayModuleLogName, "收到已处理过的订单. 订单号={0}, 微信支付订单号={1}, 支付者={2}, 支付金额={3}, 购买物品id={4}", out_trade_no, transaction_id, payorder.playerId, total_fee, payorder.recharge_id);
                    return;
                }
                if (paySuccess)
                {
                    payorder.total_fee = total_fee;
                    payorder.complete_date = complete_date;
                    payorder.transaction_id = transaction_id;

                    long payMoney = 0;
                    bool bParse = long.TryParse(total_fee, out payMoney);
                    if (bParse && payMoney == payorder.recharge_price)
                    {
                        payorder.order_status = 1;
                        LobbyPlayer player = await g.playerMgr.FindPlayerAsync(payorder.playerId);
                        if (player != null)
                        {
                            t_recharge config = g.staticDataMgr.rechargeData.GetConfig(payorder.recharge_id);
                            if (config != null)
                            {
                                if (config.f_itemid == 1)
                                {
                                    //充值购买房卡
                                    payorder.order_status = 3;
                                    player.AddDiamond(ERCSourceType.Mall, player.AccountId, (uint)config.f_count);
                                    player.OnRoleRechargeRebate(config.f_price / 100);
                                    g.dataCenter.RecordUserRechargeData(player.AccountId, player.Attribute.NickName,(uint)config.f_price / 100);
                                    g.dataCenter.TotalHistoryRechargeRoomCardCount(config.f_count);
                                    player.Save();
                                    LogSys.InfoModule(PayModuleLogName, "处理购买房卡订单成功. 订单号={0}, 微信支付订单号={1}, 支付者={2}, 支付金额={3}, 购买物品id={4}, 增加房卡数={5}", out_trade_no, transaction_id, payorder.playerId, total_fee, payorder.recharge_id, config.f_count.ToString());
                                }
                                else
                                {
                                    payorder.order_status = 4;
                                    LogSys.WarnModule(PayModuleLogName, "处理订单失败.购买物品类型未定义. 订单号={0}, 微信支付订单号={1}, 支付者={2}, 支付金额={3}, 购买物品id={4}, 物品类型={5}", out_trade_no, transaction_id, payorder.playerId, total_fee, payorder.recharge_id, config.f_itemid.ToString());
                                }
                            }
                            else
                            {
                                payorder.order_status = 4;
                                LogSys.WarnModule(PayModuleLogName, "处理订单失败. 找不到购买物品配置. 订单号={0}, 微信支付订单号={1}, 支付者={2}, 支付金额={3}, 购买物品id={4}", out_trade_no, transaction_id, payorder.playerId, total_fee, payorder.recharge_id);
                            }
                        }
                        else
                        {
                            payorder.order_status = 4;
                            LogSys.WarnModule(PayModuleLogName, "处理订单失败. 找不到支付玩家. 订单号={0}, 微信支付订单号={1}, 支付者={2}, 支付金额={3}, 购买物品id={4}", out_trade_no, transaction_id, payorder.playerId, total_fee, payorder.recharge_id);
                        }
                    }
                    else
                    {
                        payorder.order_status = 2;
                        LogSys.WarnModule(PayModuleLogName, "订单金额验证未通过. 订单号={0}, 微信支付订单号={1}, 支付者={2}, 支付金额={3}, 购买物品id={4}, 购买物品价格={5}", out_trade_no, transaction_id, payorder.playerId, total_fee, payorder.recharge_id, payorder.recharge_price.ToString());
                    }
                }
                else
                {
                    payorder.order_status = 2;
                    LogSys.WarnModule(PayModuleLogName, "订单支付失败通知. 订单号={0}, 微信支付订单号={1}, 支付者={2}, 购买物品id={3}", out_trade_no, transaction_id, payorder.playerId, payorder.recharge_id);
                }
                SaveDb();
            }
            else
            {
                LogSys.WarnModule(PayModuleLogName, "处理支付订单通知失败, 找不到指定的订单号={0}, 微信支付订单号={1}", out_trade_no, transaction_id);
            }
        }

        public bool Init()
        {
            if (!LoadFromDb())
            {
                return false;
            }

            return true;
        }

        public bool SaveDb()
        {
            byte[] data;
            if (!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;
            redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);

            return true;
        }

        bool LoadFromDb()
        {
            RedisSystem redis = env.Redis;

            RedisValue value = redis.db.StringGet(dbKey);
            if (value.IsNullOrEmpty)
            {
                SaveDb();
                return true;
            }

            if (!this.SerializeFromBinary(value))
            {
                return false;
            }

            for (int i = 0; i < lstPayOrder.Count; i++)
                mapPayDatas[lstPayOrder[i].out_trade_no] = lstPayOrder[i];

            return true;
        }

        public string dbKey
        {
            get
            {
                string key = RedisName.rechargedata;
                return key;
            }
        }
    }
}