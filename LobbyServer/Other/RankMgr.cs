﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using MahjongPushDown;
using MahjongPoint;
using Landlord;

namespace Room
{
    public class RankMgr
    {
        public void UpdateRoleExp(UInt64 roleid, UInt64 exp)
        {
            double fexp = (double)exp;
            env.Redis.db.SortedSetAdd(dbKey, roleid, fexp, CommandFlags.FireAndForget);
        }

        public void DeleteRoleFromRank(UInt64 roleid)
        {
            env.Redis.db.SortedSetRemove(dbKey, roleid, CommandFlags.FireAndForget);
        }

        /// <summary>
        /// 获取前topCount名玩家id
        /// </summary>
        /// <param name="topCount"></param>
        /// <returns></returns>
        public async Task<List<UInt64>> GetTopRankItemsAsync(int topCount)
        {
            List<UInt64> lstResults = new List<UInt64>();

            if (topCount < 1)
            {
                return lstResults;
            }

            RedisValue[] values = await env.Redis.db.SortedSetRangeByRankAsync(dbKey, 0, topCount-1, Order.Descending);

            if (values.Length == 0)
            {
                return lstResults;
            }

            foreach (var value in values)
            {
                UInt64 roleid = (UInt64)(long)(value);
                lstResults.Add(roleid);
            }

            return lstResults;
        }

        public async Task<int> GetRankAsync(UInt64 roleid)
        {
            long? rank = await env.Redis.db.SortedSetRankAsync(dbKey, roleid, Order.Descending);
            if(null == rank)
            {
                return 0;
            }

            return (int)rank + 1;
        }

        public string dbKey
        {
            get
            {
                return RedisName.rank_exp;
            }
        }
    }
}
