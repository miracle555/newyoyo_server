﻿/********************************************************************
	created:	2015/03/31
	created:	31:3:2015   13:50
	filename: 	D:\source\tfs\Otome\Dev\trunk\Server\ServerInstance\LobbyServer\Program.cs
	file path:	D:\source\tfs\Otome\Dev\trunk\Server\ServerInstance\LobbyServer
	file base:	Program
	file ext:	cs
	author:		刘冰生
	
	purpose:	处理单区跨进程逻辑
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerLib;
using Config;
using System.IO;
using Log;
using Redis;
using System.Threading;

namespace LobbyServer
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string cmd in args)
            {
                if (cmd.Equals("step"))
                {
                    Console.WriteLine("press Enter to start......");
                    Console.ReadLine();
                    Console.WriteLine("LobbyServer startup......");
                }
            }
            ServerConfig config = null;

            try
            {
                //读取配置
                config = new ServerConfig();
                config.Load(args[0]);
                config.ConfigPath = Path.GetDirectoryName(args[0]);

                Lobbyenv server = new Lobbyenv(config);

                //启动服务器
                if (!server.Start())
                {
                    LogSys.Error("......LobbyServer start failed");
                }
            }
            catch (Exception ex)
            {
                if (config == null)
                {
                    Utility.ExceptionHandle.StandardExceptionHandler.HandleException(ex, Utility.ExceptionHandle.MiniDumper.Typ.MiniDumpWithFullMemory);
                }
                else
                {
                    if (config.DumpCfg.DumpSwitch)
                    {
                        Utility.ExceptionHandle.StandardExceptionHandler.HandleException(ex, (Utility.ExceptionHandle.MiniDumper.Typ)config.DumpCfg.DumpSetting);
                    }
                }
                LogSys.Error("......Program.Main Exception occurred: {0}", ex.Message);
                LogSys.Error("{0}", ex.StackTrace);
            }

            LogSys.Info("LobbyServer stop......");
            Console.ReadLine();
        }
    }
}
