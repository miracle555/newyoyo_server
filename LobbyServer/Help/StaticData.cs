﻿/********************************************************************
	purpose:    游戏模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;

namespace LobbyServer
{
    public class StaticData
    {
        public StaticData()
            : base()
        {
        }

        public string GM_ACCOUNT = "gm";

        public LevelData levelData = new LevelData();

        public InvitData invitData = new InvitData();

        public void Init()
        {
            levelData.Init();
            invitData.Init();
        }
    }
}
