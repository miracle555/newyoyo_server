﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LobbyServer
{
    public class ConstValue
    {
        public const UInt64 MIN_ACCOUNT_ID = 10000;
        public const int ONE_PAGE_COUNT = 50;
    }
}
