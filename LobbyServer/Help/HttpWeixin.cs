﻿/********************************************************************
	purpose:    游戏模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;
using System.IO;
using Newtonsoft.Json;
using Utility;
using Statistics;
using Utility.String;
using System.Xml;

namespace LobbyServer
{
    /// <summary>
    /// Json帮助类
    /// </summary>
    public class JsonHelper
    {
        /// <summary>
        /// 将对象序列化为JSON格式
        /// </summary>
        /// <param name="o">对象</param>
        /// <returns>json字符串</returns>
        public static string SerializeObject(object o)
        {
            string json = JsonConvert.SerializeObject(o);
            return json;
        }

        /// <summary>
        /// 解析JSON字符串生成对象实体
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="json">json字符串(eg.{"ID":"112","Name":"石子儿"})</param>
        /// <returns>对象实体</returns>
        public static T DeserializeJsonToObject<T>(string json) where T : class
        {
            JsonSerializer serializer = new JsonSerializer();
            StringReader sr = new StringReader(json);
            object o = serializer.Deserialize(new JsonTextReader(sr), typeof(T));
            T t = o as T;
            return t;
        }

        /// <summary>
        /// 解析JSON数组生成对象实体集合
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="json">json数组字符串(eg.[{"ID":"112","Name":"石子儿"}])</param>
        /// <returns>对象实体集合</returns>
        public static List<T> DeserializeJsonToList<T>(string json) where T : class
        {
            JsonSerializer serializer = new JsonSerializer();
            StringReader sr = new StringReader(json);
            object o = serializer.Deserialize(new JsonTextReader(sr), typeof(List<T>));
            List<T> list = o as List<T>;
            return list;
        }

        /// <summary>
        /// 反序列化JSON到给定的匿名对象.
        /// </summary>
        /// <typeparam name="T">匿名对象类型</typeparam>
        /// <param name="json">json字符串</param>
        /// <param name="anonymousTypeObject">匿名对象</param>
        /// <returns>匿名对象</returns>
        public static T DeserializeAnonymousType<T>(string json, T anonymousTypeObject)
        {
            T t = JsonConvert.DeserializeAnonymousType(json, anonymousTypeObject);
            return t;
        }
    }

    public class stWeixinCodeItem
    {
        public EWeixinErrorID errorCode = EWeixinErrorID.Failed;
        public int weixinErrorCode = 0;
  
        public string access_token = ""; //	接口调用凭证
        public int expires_in = 0;  //access_token接口调用凭证超时时间，单位（秒）
        public string refresh_token = "";	// 用户刷新access_token
        public string openid = "";	// 授权用户唯一标识
        public string scope = ""; // 用户授权的作用域，使用逗号（,）分隔
        public string unionid = "";// 当且仅当该移动应用已获得该用户的userinfo授权时，才会出现该字段

        /// <summary>
        /// 是否有用户信息权限
        /// </summary>
        public bool hasUserInfoPermission
        {
            get
            {
                return scope.Contains("snsapi_userinfo");
            }
        }
    }

    /// <summary>
    /// 获取玩家信息
    /// </summary>
    public class stWeixinUserInfo
    {
        public EWeixinErrorID errorCode = EWeixinErrorID.Failed;
        public int weixinErrorCode = 0;

        public string openid = "";//	普通用户的标识，对当前开发者帐号唯一
        public string nickname = "";//	普通用户昵称
        public int sex = 1; //	普通用户性别，1为男性，2为女性
        //public string province = "";//	普通用户个人资料填写的省份
        //public string city = "";//	普通用户个人资料填写的城市
        //public string country = "";//	国家，如中国为CN
        public string headimgurl = "";//	用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
        public string unionid = "";//	用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的unionid是唯一的。
    }

    /// <summary>
    /// 刷新access_token
    /// </summary>
    public class stWeixinRefreshItem
    {
        public EWeixinErrorID errorCode = EWeixinErrorID.Failed;
        public int weixinErrorCode = 0;

        public string access_token = ""; //	接口调用凭证
        public int expires_in = 0;//	access_token接口调用凭证超时时间，单位（秒）
        public string refresh_token = ""; // 用户刷新access_token
        public string openid = ""; // 授权用户唯一标识
        public string scope = ""; // 用户授权的作用域，使用逗号（,）分隔
    }

    /// <summary>
    /// 微信生成预支付交易单
    /// </summary>
    public class stWeixinOrderItem
    {
        public EWeixinPayErrorID errCode = EWeixinPayErrorID.Failed;

        /// <summary>
        /// 返回状态码
        /// </summary>
        public string return_code = string.Empty;

        /// <summary>
        /// 生成预支付结果
        /// </summary>
        public string result_code = string.Empty;

        /// <summary>
        /// 错误代码
        /// </summary>
        public string error = string.Empty;

        /// <summary>
        /// 预支付成功时创建的订单,失败时为null
        /// </summary>
        public PayOrder payorder = null;
    }

    public class ParaSignComparer : IComparer<KeyValuePair<string, string>>
    {
        public int Compare(KeyValuePair<string, string> a, KeyValuePair<string, string> b)
        {
            return a.Key.CompareTo(b.Key);
        }
    }

    public enum EWeixinPayErrorID
    {
        Success = 0,
        Failed = 1,
    }

    public enum EWeixinErrorID
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 0,

        /// <summary>
        /// 失败
        /// </summary>
        Failed = 1,

        /// <summary>
        /// 不合法的调用凭证
        /// </summary>
        InvalidCredential = 40001,

        /// <summary>
        /// 不合法或已过期的code
        /// </summary>
        InvalidCode = 40029,		

        /// <summary>
        /// 不合法的access_token
        /// </summary>
        InvalidAccessToken = 40014,

        /// <summary>
        /// 不合法的refresh_token
        /// </summary>
        InvalidRefreshToken = 40030,

        /// <summary>
        /// access_token超时
        /// </summary>
        AccessTokenExpired = 42001,

        /// <summary>
        /// refresh_token超时
        /// </summary>
        RefreshTokenExpired = 42002,

		/// <summary>
        /// code超时
		/// </summary>
        CodeExpired = 42003,
    }

    public class HttpWeixin
    {
        public HttpWeixin()
            : base()
        {
        }

        public void Init()
        {
           
        }

        public int RequestCount
        {
            get
            {
                return _requestCount++;
            }
        }

        /// <summary>
        /// 获取访问token
        /// </summary>
        /// <param name="weixinCode"></param>
        /// <returns></returns>
        public async Task<stWeixinCodeItem> CodeLoginAsync(string weixinCode)
        {
            stWeixinCodeItem resultItem = new stWeixinCodeItem();
            resultItem.errorCode = EWeixinErrorID.Failed;

            int requestCount = RequestCount;

            string url = string.Format("https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type=authorization_code", AppID, AppSecret, weixinCode);

            LogSys.Info("[HttpWeixin][CodeLoginAsync],微信登录开始, requestId={0}, httpReq={1}", requestCount, url);
            string strResult = "";

            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "GET";

                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        strResult = streamReader.ReadToEnd();
                    }
                }

                JsonObject jsonObject = new JsonObject(strResult);

                LogSys.Info("[HttpWeixin][CodeLoginAsync],微信登录开始, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());

                if (jsonObject.GetIntValue("errcode", out resultItem.weixinErrorCode))
                {
                    LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());

                    resultItem.errorCode = (EWeixinErrorID)resultItem.weixinErrorCode;
                     return resultItem;
                }
                else
                {
                    if (!jsonObject.GetStringValue("access_token", out resultItem.access_token))
                    {
                        LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录成功,获取access_token失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetIntValue("expires_in", out resultItem.expires_in))
                    {
                        LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录成功,获取expires_in失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("refresh_token", out resultItem.refresh_token))
                    {
                        LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录成功,获取refresh_token失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("openid", out resultItem.openid))
                    {
                        LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录成功,获取openid失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("scope", out resultItem.scope))
                    {
                        LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录成功,获取scope失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    // 当且仅当该移动应用已获得该用户的userinfo授权时，才会出现unionid字段
                    if (!resultItem.hasUserInfoPermission)
                    {
                        LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录成功,没有snsapi_userinfo, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("unionid", out resultItem.unionid))
                    {
                        LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录成功,获取unionid失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    resultItem.errorCode = EWeixinErrorID.Success;
                    return resultItem;
                }
            }
            catch (Exception ex)
            {
                LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信登录异常, requestId={0}, httpReq={1}, httpResult={2}, exception={3}", requestCount, url, strResult, ex.Message);
                return resultItem;
            }
        }

        /// <summary>
        /// 获取玩家信息
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        public async Task<stWeixinUserInfo> UserInfoAsync(string access_token, string openid)
        {
            stWeixinUserInfo resultItem = new stWeixinUserInfo();
            resultItem.errorCode = EWeixinErrorID.Failed;

            int requestCount = RequestCount;

            string url = string.Format("https://api.weixin.qq.com/sns/userinfo?access_token={0}&openid={1}", access_token, openid);

            LogSys.Info("[HttpWeixin][UserInfoAsync],微信获取玩家信息, requestId={0}, httpReq={1}", requestCount, url);

            string strResult = "";
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "GET";

                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        strResult = streamReader.ReadToEnd();
                    }
                }

                JsonObject jsonObject = new JsonObject(strResult);

                LogSys.Info("[HttpWeixin][UserInfoAsync],微信获取玩家信息, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());

                if (jsonObject.GetIntValue("errcode", out resultItem.weixinErrorCode))
                {
                    LogSys.Warn("[HttpWeixin][UserInfoAsync],微信获取玩家信息失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());

                    resultItem.errorCode = (EWeixinErrorID)resultItem.weixinErrorCode;
                    return resultItem;
                }
                else
                {
                    if (!jsonObject.GetStringValue("openid", out resultItem.openid))
                    {
                        LogSys.Warn("[HttpWeixin][UserInfoAsync],微信获取玩家信息成功,获取openid失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("nickname", out resultItem.nickname))
                    {
                        LogSys.Warn("[HttpWeixin][UserInfoAsync],微信获取玩家信息成功,获取nickname失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetIntValue("sex", out resultItem.sex))
                    {
                        LogSys.Warn("[HttpWeixin][UserInfoAsync],微信获取玩家信息成功,获取sex失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("headimgurl", out resultItem.headimgurl))
                    {
                        LogSys.Warn("[HttpWeixin][UserInfoAsync],微信获取玩家信息成功,获取headimgurl失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("unionid", out resultItem.unionid))
                    {
                        LogSys.Warn("[HttpWeixin][UserInfoAsync],微信获取玩家信息成功,获取unionid失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    resultItem.errorCode = EWeixinErrorID.Success;
                    return resultItem;
                }
            }
            catch (Exception ex)
            {
                LogSys.Warn("[HttpWeixin][CodeLoginAsync],微信获取玩家信息异常, requestId={0}, httpReq={1}, httpResult={2}, exception={3}", requestCount, url, strResult, ex.Message);
                return resultItem;
            }
        }

        /// <summary>
        /// 刷新access_token
        /// </summary>
        /// <param name="refresh_token"></param>
        /// <returns></returns>
        public async Task<stWeixinRefreshItem> RefreshAsync(string refresh_token)
        {
            stWeixinRefreshItem resultItem = new stWeixinRefreshItem();
            resultItem.errorCode = EWeixinErrorID.Failed;

            int requestCount = RequestCount;

            string url = string.Format("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid={0}&grant_type=refresh_token&refresh_token={1}", AppID, refresh_token);

            LogSys.Info("[HttpWeixin][RefreshAsync],微信刷新access_token, requestId={0}, httpReq={1}", requestCount, url);

            string strResult = "";
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "GET";

                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        strResult = streamReader.ReadToEnd();
                    }
                }

                JsonObject jsonObject = new JsonObject(strResult);

                LogSys.Info("[HttpWeixin][RefreshAsync],微信刷新access_token, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());

                if (jsonObject.GetIntValue("errcode", out resultItem.weixinErrorCode))
                {
                    LogSys.Warn("[HttpWeixin][RefreshAsync],微信获取玩家信息失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());

                    resultItem.errorCode = (EWeixinErrorID)resultItem.weixinErrorCode;
                    return resultItem;
                }
                else
                {
                    if (!jsonObject.GetStringValue("access_token", out resultItem.access_token))
                    {
                        LogSys.Warn("[HttpWeixin][RefreshAsync],微信获取玩家信息成功,获取access_token失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetIntValue("expires_in", out resultItem.expires_in))
                    {
                        LogSys.Warn("[HttpWeixin][RefreshAsync],微信获取玩家信息成功,获取expires_in失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("refresh_token", out resultItem.refresh_token))
                    {
                        LogSys.Warn("[HttpWeixin][RefreshAsync],微信获取玩家信息成功,获取refresh_token失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("openid", out resultItem.openid))
                    {
                        LogSys.Warn("[HttpWeixin][RefreshAsync],微信获取玩家信息成功,获取openid失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("scope", out resultItem.scope))
                    {
                        LogSys.Warn("[HttpWeixin][RefreshAsync],微信获取玩家信息成功,获取scope失败, requestId={0}, httpReq={1}, httpResult={2}", requestCount, url, jsonObject.ToString());
                        return resultItem;
                    }

                    resultItem.errorCode = EWeixinErrorID.Success;
                    return resultItem;
                }
            }
            catch (Exception ex)
            {
                LogSys.Warn("[HttpWeixin][RefreshAsync],微信获取玩家信息异常, requestId={0}, httpReq={1}, httpResult={2}, exception={3}", requestCount, url, strResult, ex.Message);
                return resultItem;
            }
        }

        #region 微信支付
        public async Task<stWeixinOrderItem> PrePayOrderAsync(uint recharge_Id, long recharge_price, ulong playerId, string playerName, string ip)
        {
            stWeixinOrderItem resultItem = new stWeixinOrderItem();
            resultItem.errCode = EWeixinPayErrorID.Failed;
            string strResult = string.Empty;
            try
            {
                string nonce_str = getNonceStr(16);
                string out_trade_no = getOutTradeNo();
                List<KeyValuePair<string, string>> paraList = new List<KeyValuePair<string, string>>();
                paraList.Add(new KeyValuePair<string, string>("appid", AppID));
                paraList.Add(new KeyValuePair<string, string>("mch_id", MchID));
                paraList.Add(new KeyValuePair<string, string>("nonce_str", nonce_str));
                paraList.Add(new KeyValuePair<string, string>("body", Pay_Body));
                paraList.Add(new KeyValuePair<string, string>("out_trade_no", out_trade_no));
                paraList.Add(new KeyValuePair<string, string>("total_fee", recharge_price.ToString()));
                paraList.Add(new KeyValuePair<string, string>("spbill_create_ip", ip));
                paraList.Add(new KeyValuePair<string, string>("notify_url", PayNotify_Url));
                paraList.Add(new KeyValuePair<string, string>("trade_type", "APP"));
                string sign = WeiXinHelper.GetParamSign(paraList);
                paraList.Add(new KeyValuePair<string, string>("sign", sign));
                HttpWebRequest request = WebRequest.Create(WXPrePay_Url) as HttpWebRequest;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                string postStr = WeiXinHelper.GetPostParamString(paraList);
                LogSys.InfoModule(PayModuleLogName, "请求微信预支付信息. 充值玩家={0}, 充值商品={1}, 订单号={2}", playerId, recharge_Id, out_trade_no);
                byte[] postData = Encoding.UTF8.GetBytes(postStr);
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(postData, 0, postData.Length);
                }

                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        strResult = streamReader.ReadToEnd();
                    }
                }

                LogSys.InfoModule(PayModuleLogName, "微信预支付信息返回. 充值玩家={0}, 充值商品={1}, httpRsp={2}", playerId, recharge_Id, strResult);
                JsonObject jsonObject;
                WeiXinHelper.ParseXMLResp(strResult, out jsonObject);
                jsonObject.GetStringValue("return_code", out resultItem.return_code);
                string return_msg = string.Empty;
                jsonObject.GetStringValue("return_msg", out return_msg);
                if (resultItem.return_code.Equals("SUCCESS"))
                {
                    if (!return_msg.Equals("OK"))
                    {
                        LogSys.WarnModule(PayModuleLogName, "微信预支付参数格式校验错误. 充值玩家={0}, 充值商品={1}, httpRsp={2}, return_msg={3}", playerId, recharge_Id, strResult, return_msg);
                        return resultItem;
                    }

                    string wx_appid = string.Empty;
                    if (!jsonObject.GetStringValue("appid", out wx_appid))
                    {
                        LogSys.WarnModule(PayModuleLogName, "微信预支付获取appid失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}", playerId, recharge_Id, strResult);
                        return resultItem;
                    }

                    string wx_mch_id = string.Empty;
                    if (!jsonObject.GetStringValue("mch_id", out wx_mch_id))
                    {
                        LogSys.WarnModule(PayModuleLogName, "微信预支付获取mch_id失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}", playerId, recharge_Id, strResult);
                        return resultItem;
                    }

                    string wx_nonce_str = string.Empty;
                    if (!jsonObject.GetStringValue("nonce_str", out wx_nonce_str))
                    {
                        LogSys.WarnModule(PayModuleLogName, "微信预支付获取nonce_str失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}", playerId, recharge_Id, strResult);
                        return resultItem;
                    }

                    string wx_sign = string.Empty;
                    if (!jsonObject.GetStringValue("sign", out wx_sign))
                    {
                        LogSys.WarnModule(PayModuleLogName, "微信预支付获取sign失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}", playerId, recharge_Id, strResult);
                        return resultItem;
                    }

                    if (!jsonObject.GetStringValue("result_code", out resultItem.result_code))
                    {
                        LogSys.WarnModule(PayModuleLogName, "微信预支付获取result_code失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}", playerId, recharge_Id, strResult);
                        return resultItem;
                    }

                    if (resultItem.result_code.Equals("SUCCESS"))
                    {
                        string trade_type = string.Empty;
                        if (!jsonObject.GetStringValue("trade_type", out trade_type))
                        {
                            LogSys.WarnModule(PayModuleLogName, "微信预支付获取trade_type失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}, ", playerId, recharge_Id, strResult);
                            return resultItem;
                        }

                        string prepay_id = string.Empty;
                        if (!jsonObject.GetStringValue("prepay_id", out prepay_id))
                        {
                            LogSys.WarnModule(PayModuleLogName, "微信预支付获取prepay_id失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}, ", playerId, recharge_Id, strResult);
                            return resultItem;
                        }
                        //校验签名
                        paraList.Clear();
                        paraList.Add(new KeyValuePair<string, string>("return_code", resultItem.return_code));
                        paraList.Add(new KeyValuePair<string, string>("return_msg", return_msg));
                        paraList.Add(new KeyValuePair<string, string>("appid", wx_appid));
                        paraList.Add(new KeyValuePair<string, string>("mch_id", wx_mch_id));
                        paraList.Add(new KeyValuePair<string, string>("nonce_str", wx_nonce_str));
                        paraList.Add(new KeyValuePair<string, string>("result_code", resultItem.result_code));
                        paraList.Add(new KeyValuePair<string, string>("trade_type", trade_type));
                        paraList.Add(new KeyValuePair<string, string>("prepay_id", prepay_id));
                        string localSign = WeiXinHelper.GetParamSign(paraList);
                        if (wx_sign.Equals(localSign))
                        {
                            resultItem.errCode = EWeixinPayErrorID.Success;
                            string create_date = DateTime.Now.ToString("yyyyMMddhhmmss");
                            resultItem.payorder = g.rechargeMgr.AddPayOrder(out_trade_no, recharge_Id, recharge_price, prepay_id, create_date, playerId, playerName);
                            LogSys.InfoModule(PayModuleLogName, "微信预支付下单成功. 创建订单. 订单号={0}, 充值玩家={1}, 充值商品={2}, 金额={3}", out_trade_no, playerId, recharge_Id, recharge_price);
                            return resultItem;
                        }
                        else
                        {
                            LogSys.WarnModule(PayModuleLogName, "微信预支付签名校验失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}, wx_sign={3}, local_sign={4}", playerId, recharge_Id, strResult, wx_sign, localSign);
                            return resultItem;
                        }
                    }
                    else
                    {
                        jsonObject.GetStringValue("err_code", out resultItem.error);
                        string err_des = string.Empty;
                        LogSys.WarnModule(PayModuleLogName, "微信预支付失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}, errCode={3}, err_des={4}", playerId, recharge_Id, strResult, resultItem.error, err_des);
                        return resultItem;
                    }
                }
                else
                {
                    LogSys.WarnModule(PayModuleLogName, "微信预支付信息返回失败. 充值玩家={0}, 充值商品={1}, httpRsp={2}, returnCode={3}, return_msg={4}", playerId, recharge_Id, strResult, resultItem.return_code, return_msg);
                    return resultItem;
                }
            }
            catch (Exception e)
            {
                LogSys.WarnModule(PayModuleLogName, "获取微信支付预支付信息异常. 充值玩家={0}, 充值商品={1}, httpRsp={2}, exception={3}", playerId, recharge_Id, strResult, e.Message);
                return resultItem;
            }
        }

        private const string base_nonce_str = "abcdefghijklmnopqrstuvwxyz0123456789";
        /// <summary>
        /// 获取微信随机字符串
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string getNonceStr(int length)
        {
            Random r = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                int index = r.Next(base_nonce_str.Length);
                sb.Append(base_nonce_str[index]);
            }
            return sb.ToString();
        }

        private int orderNo = 0;
        /// <summary>
        /// 订单序列号,5位数
        /// </summary>
        public int OrderNo
        {
            get
            {
                if (orderNo == 99999)
                    orderNo = 1;
                else
                    orderNo += 1;
                return orderNo;
            }
        }

        /// <summary>
        /// 生成订单号
        /// </summary>
        /// <returns></returns>
        private string getOutTradeNo()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(DateTime.Now.ToString("yyyyMMddhhmmss"));
            sb.Append('-');
            sb.Append(string.Format("{0:00000}", OrderNo));
            return sb.ToString();
        }
        #endregion

        public const string AppID = "wx2d9ce7e2d97f6159";
        public const string AppSecret = "3b4c63957be743c80f62977630acae0f";
        public const string MchID = "1466929502";
        public const string MchKey = "shanxijunyouwangluo6666666666666";
        public const string Pay_Body = "优优娱乐-游戏充值";
        public const string WXPrePay_Url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        public const string PayNotify_Url = "http://101.37.119.211:8000/yoyo/paynotify";

        public int _requestCount = 0;
        private const string PayModuleLogName = "PayModule";
    }

    public class WeiXinHelper
    {
        private static ParaSignComparer comparer = new ParaSignComparer();

        /// <summary>
        /// 获取签名字符串
        /// </summary>
        /// <param name="paraList"></param>
        /// <returns></returns>
        public static string GetParamSign(List<KeyValuePair<string, string>> paraList)
        {
            StringBuilder sb = new StringBuilder();
            paraList.Sort(comparer);
            for (int i = 0; i < paraList.Count; i++)
            {
                if (i > 0)
                    sb.Append('&');
                sb.AppendFormat("{0}={1}", paraList[i].Key, paraList[i].Value);
            }
            sb.Append("&key=");
            sb.Append(HttpWeixin.MchKey);
            string sign = MD5Encrypt.GetMD5String(sb.ToString());
            return sign;
        }

        public static string GetPostParamString(List<KeyValuePair<string, string>> paraList)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<xml>");
            for (int i = 0; i < paraList.Count; i++)
            {
                sb.Append("<" + paraList[i].Key + ">" + paraList[i].Value + "</" + paraList[i].Key + ">");
            }
            sb.Append("</xml>");
            return sb.ToString();
        }

        public static void ParseXMLResp(string respString, out JsonObject json)
        {
            json = new JsonObject();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(respString);
            XmlNode rootNode = doc.SelectSingleNode("xml");
            foreach (XmlNode node in rootNode)
            {
                if (node.FirstChild.NodeType == XmlNodeType.CDATA)
                {
                    XmlCDataSection cdata = (XmlCDataSection)node.FirstChild;
                    json.SetStringValue(node.Name, cdata.InnerText);
                }
                else
                    json.SetStringValue(node.Name, node.FirstChild.Value);
            }
        }
    }
}
