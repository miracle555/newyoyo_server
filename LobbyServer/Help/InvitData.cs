﻿/********************************************************************
	purpose:    游戏模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;

namespace LobbyServer
{
    public class stInvitItem
    {
        /// <summary>
        /// 等级
        /// </summary>
        public UInt32 level = 0;

        /// <summary>
        /// 邀请人数
        /// </summary>
        public UInt32 inviteCount = 0;

        /// <summary>
        /// 奖励房卡数
        /// </summary>
        public UInt32 roomcard = 0;
    }

    public class InvitData
    {
        public InvitData()
            : base()
        {
        }

        /// <summary>
        /// 输入邀请人获得5房卡
        /// </summary>
        public UInt32 INVIT_AWARD_ROOMCARD = 5;

        Dictionary<UInt32, stInvitItem> mapLevelItems = new Dictionary<UInt32, stInvitItem>();

        public void Init()
        {
            mapLevelItems.Clear();

            {
                stInvitItem item = new stInvitItem();
                item.level = 1;
                item.inviteCount = 3;
                item.roomcard = 3;
                mapLevelItems.Add(item.level, item);
            }

            {
                stInvitItem item = new stInvitItem();
                item.level = 2;
                item.inviteCount = 10;
                item.roomcard = 15;
                mapLevelItems.Add(item.level, item);
            }

            {
                stInvitItem item = new stInvitItem();
                item.level = 3;
                item.inviteCount = 30;
                item.roomcard = 50;
                mapLevelItems.Add(item.level, item);
            }

            {
                stInvitItem item = new stInvitItem();
                item.level = 4;
                item.inviteCount = 50;
                item.roomcard = 80;
                mapLevelItems.Add(item.level, item);
            }

            {
                stInvitItem item = new stInvitItem();
                item.level = 5;
                item.inviteCount = 90;
                item.roomcard = 120;
                mapLevelItems.Add(item.level, item);
            }         
        }

        public bool HasLevel(UInt32 level)
        {
            return mapLevelItems.ContainsKey(level);
        }

        public stInvitItem GetLevelItem(UInt32 level)
        {
            if(HasLevel(level))
            {
                return mapLevelItems[level];
            }

            return null;
        }

        public UInt32 GetLevel(UInt32 rolecount)
        {
            foreach(var item in mapLevelItems)
            {
                if(item.Value.inviteCount > rolecount)
                {
                    return item.Key;
                }
            }

            return 0;
        }
    }
}
