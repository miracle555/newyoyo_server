﻿/********************************************************************
	purpose:    游戏模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using LobbyServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Entity;
using System.Runtime.CompilerServices;

namespace LobbyServer
{
    public class stLevelItem
    {
        public UInt32 level = 0;
        public string title = "";
        public UInt32 levelupExp = 0;
    }

    public class LevelData
    {
        public LevelData()
            : base()
        {
        }

        /// <summary>
        /// 赢一局获得经验
        /// </summary>
        public UInt32 WIN_ONE_ROUND_EXP = 100;

        Dictionary<UInt32, stLevelItem> mapLevelItems = new Dictionary<UInt32, stLevelItem>();

        public void Init()
        {
            mapLevelItems.Clear();

            {
                stLevelItem item = new stLevelItem();
                item.level = 1;
                item.title = "贫民";
                item.levelupExp = 1000;
                mapLevelItems.Add(item.level, item);
            }

            {
                stLevelItem item = new stLevelItem();
                item.level = 2;
                item.title = "富农";
                item.levelupExp = 5000;
                mapLevelItems.Add(item.level, item);
            }

            {
                stLevelItem item = new stLevelItem();
                item.level = 3;
                item.title = "地主";
                item.levelupExp = 25000;
                mapLevelItems.Add(item.level, item);
            }

            {
                stLevelItem item = new stLevelItem();
                item.level = 4;
                item.title = "县令";
                item.levelupExp = 125000;
                mapLevelItems.Add(item.level, item);
            }

            {
                stLevelItem item = new stLevelItem();
                item.level = 5;
                item.title = "将军";
                item.levelupExp = 625000;
                mapLevelItems.Add(item.level, item);
            }

            {
                stLevelItem item = new stLevelItem();
                item.level = 6;
                item.title = "宰相";
                item.levelupExp = 3125000;
                mapLevelItems.Add(item.level, item);
            }

            {
                stLevelItem item = new stLevelItem();
                item.level = 7;
                item.title = "皇帝";
                item.levelupExp = 15625000;
                mapLevelItems.Add(item.level, item);
            }
        }

        public bool HasLevel(UInt32 level)
        {
            return mapLevelItems.ContainsKey(level);
        }

        public stLevelItem GetLevelItem(UInt32 level)
        {
            if(HasLevel(level))
            {
                return mapLevelItems[level];
            }

            return null;
        }
    }
}
