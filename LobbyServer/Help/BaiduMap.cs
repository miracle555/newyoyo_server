﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;

namespace LobbyServer
{
    class BaiduMap
    {
        /// <summary>
        /// 百度地图ak
        /// </summary>
        private static String ak = "PyBnMiRl63zZif2MkR2W9QlTWvAesxkb";

        /// <summary>
        /// 百度地图sk
        /// </summary>
        private static String sk = "UF8l5aDXabw5plCItYZrdkeql7aeyQY0";

        /// <summary>
        /// 百度地图转换坐标http
        /// </summary>
        private static String turnGps = "http://api.map.baidu.com/geoconv/v1/";

        /// <summary>
        /// 百度地图逆向app
        /// </summary>
        private static String geographicalInverse = "http://api.map.baidu.com/geocoder/v2/";

        /// <summary>
        /// 赤道半径
        /// </summary>
        private static double EARTH_RADIUS = 6378.137;

        public static int getDistance(float lat1, float lon1, float lat2, float lon2)
        {
            double radLat1 = rad((double)lat1);
            double radLat2 = rad((double)lat2);
            double a = (double)radLat1 - (double)radLat2;
            double b = rad((double)lon1) - rad((double)lon2);
            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) + Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * EARTH_RADIUS;
            s *= 1000;
            if(s > int.MaxValue)
            {
                s = int.MaxValue;
            }
            return (int)s;
        }
        
        /// <summary>
        /// 经纬度转为弧度
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }


        public BaiDuVO baiDuApi(float x,float y)
        {
            XY xy = turnCoordinate(x, y);
            BaiDuVO baiDuVO = getAddress(xy);
            return baiDuVO;
        }

        public XY turnCoordinate(float x,float y)
        {
            String coords = "121.6035" + "," + "31.1934";
            String sn = "";
            StringBuilder param = new StringBuilder();
            
            sn = SnCal.CaculateAKSN(coords, ak, sk);
            param.Append("coords=");//x,y坐标
            param.Append(coords);
            param.Append("&ak=");//开发者密钥
            param.Append(ak);
            param.Append("&output=json");//from 源坐标类型  to 目的坐标类型 output 返回结果格式 
            param.Append("&sn=");//用户的权限签名 
            param.Append(sn);
            String result = GetURLResult(turnGps,param.ToString());
            JObject jo = (JObject)JsonConvert.DeserializeObject(result);
            String res = jo["result"].ToString();
            JArray jArray = JArray.Parse(res);
            JObject jObject = JObject.Parse(jArray[0].ToString());
            XY xy = new XY();
            xy.setX(jObject["x"].ToString());
            xy.setY(jObject["y"].ToString());
            return xy;
        }


        public BaiDuVO getAddress(XY xy)
        {
            String sn = "";
            //根据坐标获取位置
            try
            {
                sn = SnCal.CaculateAKSN2(xy.getX(),xy.getY(), ak, sk);
                StringBuilder sb = new StringBuilder();
                sb.Append("location=");
                sb.Append(xy.getY() + "," + xy.getX());
                sb.Append("&output=json&pois=0&ak=");
                sb.Append(ak);
                sb.Append("&sn=");
                sb.Append(sn);
                String data = GetURLResult(geographicalInverse,sb.ToString());
                JObject jo = (JObject)JsonConvert.DeserializeObject(data);
                JObject res = (JObject)JObject.Parse(jo["result"].ToString());
                BaiDuVO baiDuVO = new BaiDuVO();
                baiDuVO.setFormatted_address(res["formatted_address"].ToString());
                baiDuVO.setSematic_description(res["sematic_description"].ToString());
                return baiDuVO;
            }
            catch
            {

            }
            return null;
        }

        string GetURLResult(string Url, string postDataStr)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == "" ? "" : "?") + postDataStr);

            request.Method = "GET";

            request.ContentType = "text/html;charset=UTF-8";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            Stream myResponseStream = response.GetResponseStream();

            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));

            string retString = myStreamReader.ReadToEnd();

            myStreamReader.Close();

            myResponseStream.Close();

            return retString;

        }
    }

    class BaiDuVO
    {
        private XY xy;

        private String formatted_address;

        private String sematic_description;

        public XY getXy()
        {
            return xy;
        }

        public void setXy(XY xy)
        {
            this.xy = xy;
        }

        public String getFormatted_address()
        {
            return formatted_address;
        }

        public void setFormatted_address(String formatted_address)
        {
            this.formatted_address = formatted_address;
        }

        public String getSematic_description()
        {
            return sematic_description;
        }

        public void setSematic_description(String sematic_description)
        {
            this.sematic_description = sematic_description;
        }
    }
 

    public class SnCal
    {
        private static string MD5(string password)
        {
            byte[] textBytes = Encoding.UTF8.GetBytes(password);
            try
            {
                MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    ret += a.ToString("x2");
                }
                return ret;
            }
            catch
            {
                throw;
            }
        }

        private static string UrlEncode(string str)
        {
            str = System.Web.HttpUtility.UrlEncode(str);
            byte[] buf = Encoding.ASCII.GetBytes(str);//等同于Encoding.ASCII.GetBytes(str)
            for (int i = 0; i < buf.Length; i++)
                if (buf[i] == '%')
                {
                    if (buf[i + 1] >= 'a') buf[i + 1] -= 32;
                    if (buf[i + 2] >= 'a') buf[i + 2] -= 32;
                    i += 2;
                }
            return Encoding.ASCII.GetString(buf);//同上，等同于Encoding.ASCII.GetString(buf)
        }

        private static string HttpBuildQuery(IDictionary<string, string> querystring_arrays)
        {

            StringBuilder sb = new StringBuilder();
            foreach (var item in querystring_arrays)
            {
                sb.Append(UrlEncode(item.Key));
                sb.Append("=");
                String[] ss = item.Value.Split(',');
                if(ss.Length >= 2)
                {
                    for(int i = 0;i < ss.Length;i++)
                    {
                        if(i == ss.Length - 1)
                        {
                            sb.Append(UrlEncode(ss[i]));
                        }
                        else
                        {
                            sb.Append(UrlEncode(ss[i]) + ",");
                        }
                    }
                }
                else
                {
                    sb.Append(UrlEncode(item.Value));
                }
                sb.Append("&");
            }
            sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        public static string CaculateAKSN(string coords, string ak, String sk)
        {
            IDictionary<String, String> paramsMap = new Dictionary<String, String>();
            paramsMap.Add("coords", coords);
            paramsMap.Add("ak", ak);
            paramsMap.Add("output", "json");
            var queryString = HttpBuildQuery(paramsMap);

            var str = UrlEncode("/geoconv/v1/" + "?" + queryString + sk);

            return MD5(str);
        }

        public static string CaculateAKSN2(String x,String y, String ak, String sk)
        {
            IDictionary<String, String> paramsMap = new Dictionary<String, String>();
            paramsMap.Add("location", y + "," + x);
            paramsMap.Add("output", "json");
            paramsMap.Add("pois", "0");
            paramsMap.Add("ak", ak);
            var queryString = HttpBuildQuery(paramsMap);

            var str = UrlEncode("/geocoder/v2/" + "?" + queryString + sk);

            return MD5(str);
        }

       
    }

    public class TurnXY
    {
        public int status { get; set; }
        public List<XY> result { get; set; }
    }


    public class XY
    {
        private String x;

        private String y;

        public String getX()
        {
            return x;
        }

        public void setX(String x)
        {
            this.x = x;
        }

        public String getY()
        {
            return y;
        }

        public void setY(String y)
        {
            this.y = y;
        }
    }

}
