﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Web;
using Log;
using Utility;
using Statistics;

namespace LobbyServer
{
    public class PayListener
    {
        Thread httpListener_payNotify;

        private const string PayModuleLogName = "PayModule";

        public void InitPayListener()
        {
            httpListener_payNotify = new Thread(listenPay);
            httpListener_payNotify.Start();
        }

        private void addAddress(string address, string domain, string user)
        {
            string argsDll = string.Format(@"http delete urlacl url={0}", address);
            string args = string.Format(@"http add urlacl url={0} user={1}\{2}", address, domain, user);
            ProcessStartInfo psi = new ProcessStartInfo("netsh", argsDll);
            psi.Verb = "runas";
            psi.CreateNoWindow = false;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;
            Process.Start(psi).WaitForExit();//删除urlacl
            psi = new ProcessStartInfo("netsh", args);
            psi.Verb = "runas";
            psi.CreateNoWindow = false;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.UseShellExecute = false;
            Process.Start(psi).WaitForExit();//添加urlacl
        }

        private void listenPay()
        {
            if (!HttpListener.IsSupported)
            {
                LogSys.Warn("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }
            HttpListener listener = new HttpListener();
            string prefixes = "http://+:8000/yoyo/paynotify/";
            listener.Prefixes.Add(prefixes);
            addAddress(prefixes, Environment.UserDomainName, Environment.UserName);
            listener.Start();
            while (true)
            {
                try
                {
                    HttpListenerContext context = listener.GetContext();
                    HttpListenerRequest request = context.Request;
                    HttpListenerResponse response = context.Response;

                    List<KeyValuePair<string, string>> rspParaList = new List<KeyValuePair<string, string>>();
                    string decodeStr = string.Empty;
                    using (StreamReader reader = new StreamReader(request.InputStream, Encoding.UTF8))
                    {
                        string body = reader.ReadToEnd();
                        decodeStr = HttpUtility.UrlDecode(body);
                        //LogSys.InfoModule(PayModuleLogName, "收到微信支付通知. 内容={0}", decodeStr);
                    }
                    JsonObject json;
                    WeiXinHelper.ParseXMLResp(decodeStr, out json);
                    string return_code = string.Empty;
                    string return_msg = string.Empty;
                    json.GetStringValue("return_code", out return_code);
                    json.GetStringValue("return_msg", out return_msg);
                    if (return_code.Equals("SUCCESS"))
                    {
                        string result_code = string.Empty;
                        json.GetStringValue("result_code", out result_code);

                        string appid = string.Empty;
                        json.GetStringValue("appid", out appid);

                        string mch_id = string.Empty;
                        json.GetStringValue("mch_id", out mch_id);

                        string nonce_str = string.Empty;
                        json.GetStringValue("nonce_str", out nonce_str);

                        string sign = string.Empty;
                        json.GetStringValue("sign", out sign);

                        string openid = string.Empty;
                        json.GetStringValue("openid", out openid);

                        string trade_type = string.Empty;
                        json.GetStringValue("trade_type", out trade_type);

                        string bank_type = string.Empty;
                        json.GetStringValue("bank_type", out bank_type);

                        string total_fee = string.Empty;
                        json.GetStringValue("total_fee", out total_fee);

                        string cash_fee = string.Empty;
                        json.GetStringValue("cash_fee", out cash_fee);

                        string fee_type = string.Empty;
                        json.GetStringValue("fee_type", out fee_type);

                        string is_subscribe = string.Empty;
                        json.GetStringValue("is_subscribe", out is_subscribe);

                        string transaction_id = string.Empty;
                        json.GetStringValue("transaction_id", out transaction_id);

                        string out_trade_no = string.Empty;
                        json.GetStringValue("out_trade_no", out out_trade_no);

                        string time_end = string.Empty;
                        json.GetStringValue("time_end", out time_end);

                        if (result_code.Equals("SUCCESS"))
                        {
                            LogSys.InfoModule(PayModuleLogName, "微信支付成功通知. 订单号={0}, 微信支付订单号={1}", out_trade_no, transaction_id);

                            List<KeyValuePair<string, string>> reqParaList = new List<KeyValuePair<string, string>>();
                            reqParaList.Add(new KeyValuePair<string, string>("return_code", return_code));
                            reqParaList.Add(new KeyValuePair<string, string>("result_code", result_code));
                            reqParaList.Add(new KeyValuePair<string, string>("appid", appid));
                            reqParaList.Add(new KeyValuePair<string, string>("mch_id", mch_id));
                            reqParaList.Add(new KeyValuePair<string, string>("nonce_str", nonce_str));
                            reqParaList.Add(new KeyValuePair<string, string>("openid", openid));
                            reqParaList.Add(new KeyValuePair<string, string>("trade_type", trade_type));
                            reqParaList.Add(new KeyValuePair<string, string>("bank_type", bank_type));
                            reqParaList.Add(new KeyValuePair<string, string>("total_fee", total_fee));
                            reqParaList.Add(new KeyValuePair<string, string>("cash_fee", cash_fee));
                            reqParaList.Add(new KeyValuePair<string, string>("fee_type", fee_type));
                            reqParaList.Add(new KeyValuePair<string, string>("is_subscribe", is_subscribe));
                            reqParaList.Add(new KeyValuePair<string, string>("transaction_id", transaction_id));
                            reqParaList.Add(new KeyValuePair<string, string>("out_trade_no", out_trade_no));
                            reqParaList.Add(new KeyValuePair<string, string>("time_end", time_end));
                            string LocalSign = WeiXinHelper.GetParamSign(reqParaList);
                            if (LocalSign.Equals(sign))
                            {
                                //签名校验成功,处理订单
                                g.rechargeMgr.ProcessPayNotify(out_trade_no, total_fee, time_end, transaction_id, true);
                                rspParaList.Add(new KeyValuePair<string, string>("return_code", "SUCCESS"));
                                rspParaList.Add(new KeyValuePair<string, string>("return_msg", "OK"));
                            }
                            else
                            {
                                LogSys.WarnModule(PayModuleLogName, "微信支付通知签名校验失败. 订单号={0}, 微信支付订单号={1}, LocalSign={2}, WeiXinSign={3}, 返回数据={4}", out_trade_no, transaction_id, LocalSign, sign, decodeStr);
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < reqParaList.Count; i++)
                                {
                                    sb.AppendFormat("key={0}, value={1},", reqParaList[i].Key, reqParaList[i].Value);
                                }
                                LogSys.WarnModule(PayModuleLogName, "本地签名参数列表:{0}", sb.ToString());
                                rspParaList.Add(new KeyValuePair<string, string>("return_code", "FAIL"));
                                rspParaList.Add(new KeyValuePair<string, string>("return_msg", "签名验证失败"));
                            }
                        }
                        else
                        {
                            string err_code = string.Empty;
                            json.GetStringValue("err_code", out err_code);
                            string err_code_des = string.Empty;
                            json.GetStringValue("err_code_des", out err_code_des);
                            LogSys.WarnModule(PayModuleLogName, "微信支付失败通知. 订单号={0}, 微信支付订单号={1}, result_code={2}, err_code={3}, err_code_des={4}", out_trade_no, transaction_id, result_code, err_code, err_code_des);
                            //处理支付失败订单
                            g.rechargeMgr.ProcessPayNotify(out_trade_no, total_fee, time_end, transaction_id, false);
                            rspParaList.Add(new KeyValuePair<string, string>("return_code", "FAIL"));
                            rspParaList.Add(new KeyValuePair<string, string>("return_msg", "支付失败"));
                        }
                    }
                    else
                    {
                        rspParaList.Add(new KeyValuePair<string, string>("return_code", "FAIL"));
                        rspParaList.Add(new KeyValuePair<string, string>("return_msg", "支付通知失败"));
                        LogSys.WarnModule(PayModuleLogName, "微信支付通知通信失败. return_code={0}, return_msg={1}", return_code, return_msg);
                    }

                    string postStr = WeiXinHelper.GetPostParamString(rspParaList);
                    using (StreamWriter writer = new StreamWriter(response.OutputStream, Encoding.UTF8))
                    {
                        writer.Write(postStr);
                        writer.Close();
                        response.Close();
                    }
                }
                catch (Exception e)
                {
                    LogSys.Error("httpListener_payNotify throw exception : {0}", e.Message);
                }
            }
        }
    }
}