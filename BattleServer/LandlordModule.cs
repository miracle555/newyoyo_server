﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using BattleServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Protocols.Landlord;
using Landlord;
using HelpAsset.Landlord;

namespace Module
{
    /// <summary>
    /// 斗地主
    /// </summary>
    public class LandlordModule : LogicModule
    {
        public LandlordModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            // 创建房间
            RegisterMsgHandler(MSGID.LandlordCreateRoomL2B, new MsgHandler<LandlordCreateRoomL2B>(OnLandlordCreateRoomL2B));

            // 加入房间
            RegisterMsgHandler(MSGID.LandlordJoinRoomL2B, new MsgHandler<LandlordJoinRoomL2B>(OnLandlordJoinRoomL2B));

            // 获取房间信息
            RegisterMsgHandler(MSGID.LandlordGetRoomInfoL2B, new MsgHandler<LandlordGetRoomInfoL2B>(OnLandlordGetRoomInfoL2B));

            // 明牌或正常开始
            RegisterMsgHandler(MSGID.LandlordRoundConfigSelectReq, new MsgHandler<LandlordRoundConfigSelectReq>(OnLandlordRoundConfigSelectReq));

            // (斗地主)明牌倍数
            RegisterMsgHandler(MSGID.LandlordMingCardScalesReq, new MsgHandler<LandlordMingCardScalesReq>(OnLandlordMingCardScalesReq));

            // (斗地主)叫地主
            RegisterMsgHandler(MSGID.LandlordCallLandlordReq, new MsgHandler<LandlordCallLandlordReq>(OnLandlordCallLandlordReq));

            // (斗地主)抢地主
            RegisterMsgHandler(MSGID.LandlordRobLandlordReq, new MsgHandler<LandlordRobLandlordReq>(OnLandlordRobLandlordReq));

            // (斗地主)加倍请求
            RegisterMsgHandler(MSGID.LandlordDoubleScalesReq, new MsgHandler<LandlordDoubleScalesReq>(OnLandlordDoubleScalesReq));

            // (斗地主)出牌请求
            RegisterMsgHandler(MSGID.LandlordHandOutCardReq, new MsgHandler<LandlordHandOutCardReq>(OnLandlordHandOutCardReq));

            // (斗地主)下一步
            RegisterMsgHandler(MSGID.LandlordNextStepReq, new MsgHandler<LandlordNextStepReq>(OnLandlordNextStepReq));

            // (斗地主)解散游戏
            RegisterMsgHandler(MSGID.LandlordDismissGameReq, new MsgHandler<LandlordDismissGameReq>(OnLandlordDismissGameReq));

            // 解散是否同意
            RegisterMsgHandler(MSGID.LandlordDismissAgreeReq, new MsgHandler<LandlordDismissAgreeReq>(OnLandlordDismissAgreeReq));

            // 离开房间，房间未开始时
            RegisterMsgHandler(MSGID.LandlordLeaveRoomReq, new MsgHandler<LandlordLeaveRoomReq>(OnLandlordLeaveRoomReq));

            // 聊天
            RegisterMsgHandler(MSGID.LandlordChatReq, new MsgHandler<LandlordChatReq>(OnLandlordChatReq));

            //文字聊天
            RegisterMsgHandler(MSGID.LandlordChatWordReq, new MsgHandler<LandlordChatWordReq>(OnLandlordChatWordReq));

            //扔鸡蛋
            RegisterMsgHandler(MSGID.LandlordEggReq, new MsgHandler<LandlordEggReq>(OnLandlordEggReq));

            //房主建房
            RegisterMsgHandler(MSGID.LandlordOtherCloseRoomL2B, new MsgHandler<LandlordOtherCloseRoomL2B>(OnLandlordOtherCloseRoomL2B));

            //stCardFormInfo otherInfo = new stCardFormInfo();
            //otherInfo.minCardType = ECardType.Five;
            //otherInfo.maxCardType = ECardType.Seven;
            //otherInfo.cardTypeCount = 3;

            //List<stCardFormInfo> lstBigInfos = new List<stCardFormInfo>();

            //LordHelp.CheckStraightBig(otherInfo, lstBigInfos);
            if (!g.lordRoomMgr.Init())
            {
                return false;
            }
            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordCreateRoomL2B(UInt32 from, UInt64 accountId, LandlordCreateRoomL2B req)
        {
            LandlordCreateRoomRsp rsp = new LandlordCreateRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                player = new BattlePlayer();
                player.AccountId = accountId;
                //LogSys.Warn("[MJYunChengModule][OnMJYunChengCreateRoomL2B],找不到玩家,accountId={0}", accountId);
                //return;
            }

            UInt64 roomuuid = req.roomUuid;
            LordRoom room = new LordRoom(roomuuid);
            room.config = req.config;
            if (req.config.payMethod != PayMethod.anotherPay)
            {
                room.InitRoom(accountId, player.NickName, EGameType.Landlord);
                player.RoomUuid = roomuuid;
                AddRoleToRoom(player, room);
            }
            else
            {
                room.InitRoom2(accountId, player.NickName, EGameType.Landlord);
            }

            g.lordRoomMgr.AddRoom(room);

            rsp.errorCode = LandlordCreateRoomRsp.ErrorID.Success;
            if (req.config.payMethod != PayMethod.anotherPay)
            {
                rsp.roomInfo = room.GetRoomInfo(accountId);
            }
            player.SendMsgToClient(rsp);

            room.SaveDb(true);
        }

        void AddRoleToRoom(BattlePlayer player, LordRoom room)
        {
            LordRoomRole role = new LordRoomRole();
            role.Init(player.AccountId, player.NickName, room);
            role.position = room.GetOneSeat();
            role.sex = player.sex;
            role.level = player.level;
            role.roleState = ERoomRoleState.WaitRoomer;
            role.weixinHeadImgUrl = player.weixinHeadImgUrl;
            room.AddRole(role);
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordJoinRoomL2B(UInt32 from, UInt64 accountId, LandlordJoinRoomL2B req)
        {
            LandlordJoinRoomRsp rsp = new LandlordJoinRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = await g.lordRoomMgr.FindRoomAsync(req.roomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            if (room.IsRoomFull())
            {
                rsp.errorCode = LandlordJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordJoinRoomL2B],房间已满,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;
            AddRoleToRoom(player, room);

            rsp.errorCode = LandlordJoinRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            // 通知其他人
            LandlordJoinRoomNtf ntf = new LandlordJoinRoomNtf();
            ntf.remoteRoleInfo = room.GetRole(accountId).remoteInfo;
            room.SendRoomMsgEx(accountId, ntf);

            // 房间人数已满，回合开始
            if (room.IsRoomFull())
            {
                room.FirstInit();
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnLandlordGetRoomInfoL2B(UInt32 from, UInt64 accountId, LandlordGetRoomInfoL2B req)
        {
            LandlordGetRoomInfoRsp rsp = new LandlordGetRoomInfoRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordGetRoomInfoL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;

            LordRoom room = await g.lordRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordGetRoomInfoL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordGetRoomInfoL2B],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = LandlordGetRoomInfoRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.CheckDismissTimer();
        }

        /// <summary>
        /// 明牌或正常开始
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordRoundConfigSelectReq(UInt32 from, UInt64 accountId, LandlordRoundConfigSelectReq req)
        {
            LandlordRoundConfigSelectRsp rsp = new LandlordRoundConfigSelectRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordRoundConfigSelectReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordRoundConfigSelectRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordRoundConfigSelectReq],房间不存在,accountId={0}", accountId);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordRoundConfigSelectRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordRoundConfigSelectReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsRoundConfigSelectState)
            {
                rsp.errorCode = LandlordRoundConfigSelectRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordRoundConfigSelectReq],玩家不在回合配置选择状态,accountId={0}", accountId);
                return;
            }

            role.currentRound.bMingCardStart = req.bMingCardStart;
            if (req.bMingCardStart)
            {
                // 明牌5倍
                room.currentRound.SetMaxMingCardScales(5);
                role.currentRound.bMingPai = true;
            }

            rsp.errorCode = LandlordRoundConfigSelectRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LandlordRoundConfigSelectNtf ntf = new LandlordRoundConfigSelectNtf();
            ntf.roleid = accountId;
            ntf.bMingCardStart = req.bMingCardStart;
            room.SendRoomMsgEx(accountId, ntf);

            // 通知总倍数变化
            room.RefreshAllRoleTotalScales();

            role.currentRound.playState = ERolePlayState.Wait;

            if (room.IsAllRoleRoundConfigFinish())
            {
                foreach (var roleitem in room.lstRoles)
                {
                    roleitem.NotifyShowNewCards();
                }
            }

            room.SaveDb();
        }

        /// <summary>
        /// (斗地主)明牌倍数
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordMingCardScalesReq(UInt32 from, UInt64 accountId, LandlordMingCardScalesReq req)
        {
            LandlordMingCardScalesRsp rsp = new LandlordMingCardScalesRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordMingCardScalesReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordMingCardScalesReq],房间不存在,accountId={0}", accountId);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordMingCardScalesReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsShowNewCardsState)
            {
                rsp.errorCode = LandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordMingCardScalesReq],是否在显示新牌状态,accountId={0}", accountId);
                return;
            }

            if (req.scales < 1 || req.scales > 5)
            {
                rsp.errorCode = LandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordMingCardScalesReq],明牌倍数不对,accountId={0},scales={1}", accountId, req.scales);
                return;
            }

            room.currentRound.SetMaxMingCardScales(req.scales);
            if (req.scales != 1)
            {
                role.currentRound.bMingPai = true;
            }

            rsp.errorCode = LandlordMingCardScalesRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LandlordMingCardScalesNtf ntf = new LandlordMingCardScalesNtf();
            ntf.roleid = accountId;
            ntf.scales = req.scales;
            if (req.scales != 1)
            {
                ntf.handcards.AddRange(role.currentRound.handCards);
            }
            room.SendRoomMsgEx(accountId, ntf);

            // 通知总倍数变化
            room.RefreshAllRoleTotalScales();

            role.currentRound.playState = ERolePlayState.Wait;

            if (room.IsAllRoleShowNewCardsFinish())
            {
                // 庄家开始叫地主
                room.bankerRole.NotifyCallLandlord();
            }

            LandlordNeedCallLandlordNtf ntf1 = new LandlordNeedCallLandlordNtf();
            ntf1.roleId = room.bankerRole.roleid;
            room.SendRoomMsgEx(room.bankerRole.roleid, ntf1);

            room.SaveDb();
        }

        /// <summary>
        /// 叫地主
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordCallLandlordReq(UInt32 from, UInt64 accountId, LandlordCallLandlordReq req)
        {
            LandlordCallLandlordRsp rsp = new LandlordCallLandlordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordCallLandlordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordCallLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordCallLandlordReq],房间不存在,accountId={0}", accountId);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordCallLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordCallLandlordReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsCallLandlordState)
            {
                rsp.errorCode = LandlordCallLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordCallLandlordReq],不在抢地主状态,accountId={0}", accountId);
                return;
            }

            if (req.jiaofen != 0 && req.jiaofen <= room.currentRound.maxFen)
            {
                rsp.errorCode = LandlordCallLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordCallLandlordReq],叫分不合理,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = LandlordCallLandlordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            role.currentRound.playState = ERolePlayState.Wait;

            LandlordCallLandlordNtf ntf = new LandlordCallLandlordNtf();
            ntf.roleid = accountId;
            ntf.jiaofen = req.jiaofen;
            room.SendRoomMsgEx(accountId, ntf);

            room.currentRound.AddRobRole(accountId, req.jiaofen);
            role.currentRound.currentFen = req.jiaofen;
            role.currentRound.hasFen = true;
            if (req.jiaofen > room.currentRound.maxFen)
            {
                //设置当前回玩家所叫的最高分
                room.currentRound.maxFen = req.jiaofen;
            }
            LogSys.Info("玩家叫分" + req.jiaofen);

            //如果叫的三分直接就是地主
            if (req.jiaofen == 3)
            {
                //UInt64 nextJiaofenRoleId = 0;
                //ERobLordResult robResult = room.currentRound.RobLordResult(ref room.currentRound.lordRoleId, ref nextJiaofenRoleId);

                //if (robResult == ERobLordResult.Success)
                //{

                room.currentRound.lordRoleId = role.roleid;
                // 抢地主确定
                LandlordRobLandlordResultNtf ntfResult = new LandlordRobLandlordResultNtf();
                ntfResult.lordRoleId = room.currentRound.lordRoleId;
                room.SendRoomMsg(ntfResult);

                // 把底牌加到地主身上
                room.lordRole.currentRound.AddHandCards(room.currentRound.bottomCards);

                //开始加倍
                foreach (var roleitem in room.lstRoles)
                {
                    roleitem.NotifyDoubleScales();
                }
                //}
                //else
                //{
                // 下个人抢地主
                // role.nextRole.NotifyRobLandlord();
                //  }

                // 通知总倍数变化
                room.RefreshAllRoleTotalScales();

                //room.lordRole.NotifyNeedHandOutCard(false);
            }
            else
            {
                //判断下个玩家是否叫过
                LordRoomRole nextRole = role.nextRole;

                //如果下个玩家叫过分
                if (nextRole.currentRound.hasFen)
                {
                    if (room.currentRound.maxFen == 0)
                    {
                        foreach (LordRoomRole lr in room.lstRoles)
                        {
                            lr.currentRound.Clear();
                        }
                        room.currentRound.maxFen = 0;
                        //如果所有玩家都选择不叫重新发牌
                        room.StartNewRound2();
                        return;
                    }

                    //比较分最大的玩家为地主
                    LordRoomRole dizhu = null;
                    foreach (LordRoomRole lr in room.lstRoles)
                    {
                        if (lr.currentRound.currentFen == room.currentRound.maxFen)
                        {
                            dizhu = lr;
                        }
                    }
                    room.currentRound.lordRoleId = dizhu.roleid;
                    //通知确定地主
                    LandlordRobLandlordResultNtf ntfResult = new LandlordRobLandlordResultNtf();
                    ntfResult.lordRoleId = room.currentRound.lordRoleId;
                    room.SendRoomMsg(ntfResult);

                    // 把底牌加到地主身上
                    room.lordRole.currentRound.AddHandCards(room.currentRound.bottomCards);

                    //开始加倍
                    foreach (var roleitem in room.lstRoles)
                    {
                        roleitem.NotifyDoubleScales();
                    }
                    //room.lordRole.NotifyNeedHandOutCard(false);
                }
                else
                {
                    //没有叫过分 通知叫分
                    role.nextRole.NotifyCallLandlord();
                }
            }
            room.SaveDb();
        }

        /// <summary>
        /// (斗地主)抢地主
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordRobLandlordReq(UInt32 from, UInt64 accountId, LandlordRobLandlordReq req)
        {
            LandlordRobLandlordRsp rsp = new LandlordRobLandlordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordRobLandlordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordRobLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordRobLandlordReq],房间不存在,accountId={0}", accountId);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordRobLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordRobLandlordReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsRobLandlordState)
            {
                rsp.errorCode = LandlordRobLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordRobLandlordReq],不在抢地主状态,accountId={0}", accountId);
                return;
            }
            /**
            room.currentRound.AddRobRole(accountId, req.bRob);
            room.currentRound.publicScales.robScales = room.currentRound.CalRobScales();

            rsp.errorCode = LandlordRobLandlordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            role.currentRound.playState = ERolePlayState.Wait;

            UInt64 nextRobRoleId = 0;
            ERobLordResult robResult = room.currentRound.RobLordResult(ref room.currentRound.lordRoleId, ref nextRobRoleId);

            if (robResult == ERobLordResult.Success)
            {
                // 抢地主确定
                LandlordRobLandlordResultNtf ntf = new LandlordRobLandlordResultNtf();
                ntf.lordRoleId = room.currentRound.lordRoleId;
                room.SendRoomMsg(ntf);

                // 把底牌加到地主身上
                room.lordRole.currentRound.AddHandCards(room.currentRound.bottomCards);

                // 开始加倍
                foreach(var roleitem in room.lstRoles)
                {
                    roleitem.NotifyDoubleScales();
                }
            }
            else if(robResult == ERobLordResult.Failed)
            {
                // 抢地主失败，重新发牌
                room.RefreshShowCard();
            }
            else
            {
                LandlordRobLandlordNtf ntf = new LandlordRobLandlordNtf();
                ntf.roleid = accountId;
                ntf.bRob = req.bRob;
                room.SendRoomMsgEx(accountId, ntf);

                if(nextRobRoleId == 0)
                {
                    role.nextRole.NotifyRobLandlord();
                }
                else
                {
                    var nextRobRole = room.GetRole(nextRobRoleId);
                    // 下个人抢地主
                    nextRobRole.NotifyRobLandlord();
                }
            }

            // 通知总倍数变化
            room.RefreshAllRoleTotalScales();

            room.SaveDb();
            */
        }

        /// <summary>
        /// (斗地主)加倍请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordDoubleScalesReq(UInt32 from, UInt64 accountId, LandlordDoubleScalesReq req)
        {
            LandlordDoubleScalesRsp rsp = new LandlordDoubleScalesRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordDoubleScalesReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordDoubleScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDoubleScalesReq],房间不存在,accountId={0}", accountId);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordDoubleScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDoubleScalesReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsDoubleScaleState)
            {
                rsp.errorCode = LandlordDoubleScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordRobLandlordReq],不是加倍状态,accountId={0}", accountId);
                return;
            }


            if (req.bDoubleScales)
            {
                role.currentRound.scales = 2;
            }
            else
            {
                role.currentRound.scales = 1;
            }

            rsp.errorCode = LandlordDoubleScalesRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 通知总倍数变化
            room.RefreshAllRoleTotalScales();

            role.currentRound.playState = ERolePlayState.Wait;

            LandlordDoubleScalesNtf ntf = new LandlordDoubleScalesNtf();
            ntf.roleid = accountId;
            ntf.bDoubleScales = req.bDoubleScales;
            room.SendRoomMsgEx(accountId, ntf);

            if (room.IsAllRoleDoubleScalesFinish())
            {
                // 地主出牌
                room.lordRole.NotifyNeedHandOutCard(false);
            }

            room.SaveDb();

        }

        /// <summary>
        /// (斗地主)出牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordHandOutCardReq(UInt32 from, UInt64 accountId, LandlordHandOutCardReq req)
        {
            LandlordHandOutCardRsp rsp = new LandlordHandOutCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsHandOutCardState)
            {
                rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],不是出牌状态,accountId={0}", accountId);
                return;
            }

            ECardFormType formType = ECardFormType.Null;
            if (req.bHandOut)
            {
                if (req.lstCards.Count == 0)
                {
                    rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],出牌数量不能为0,accountId={0}, lstCards={1}", accountId, ListToString(req.lstCards));
                    return;
                }

                foreach (var cardid in req.lstCards)
                {
                    if (!role.currentRound.HasHandCard(cardid))
                    {
                        rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],手牌中没有该牌,accountId={0}, lstCards={1}, cardid={2}", accountId, ListToString(req.lstCards), cardid);
                        return;
                    }
                }

                formType = LordHelp.GetCardFormType(req.lstCards, req.laiziType);
                if (formType == ECardFormType.Null || formType == ECardFormType.Laizigou)
                {
                    rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],不是合法牌形,accountId={0}, lstCards={1},formType={2},laiziType={3}", accountId, ListToString(req.lstCards), formType, req.laiziType);
                    return;
                }

                //炸弹翻倍 包括三炸
                if (formType == ECardFormType.Bomb || formType == ECardFormType.KingBomb
                    || formType == ECardFormType.LaizigouBomb || formType == ECardFormType.ThreeBomb)
                {
                    room.currentRound.publicScales.bombScales *= 2;

                    // 通知总倍数变化
                    room.RefreshAllRoleTotalScales();
                }

                // 上次出牌人不是自己
                if (room.currentRound.lastOutCardItem.roleid != 0)
                {
                    if (room.currentRound.lastOutCardItem.roleid != accountId)
                    {
                        // 牌不大于上家
                        if (!LordHelp.IsGreaterCards(req.lstCards, req.laiziType, room.currentRound.lastOutCardItem.lstCards, room.currentRound.lastOutCardItem.laiziType, room.currentRound.laiziCard))
                        {
                            rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Failed;
                            player.SendMsgToClient(rsp);

                            LogSys.Warn("[LandlordModule][OnLandlordHandOutCardReq],牌不大于上家不能出,accountId={0}, lstCards={1}", accountId, ListToString(req.lstCards));
                            return;
                        }
                    }
                }

                // 出牌次数增加
                role.currentRound.handOutCardTimes++;
                role.currentRound.RemoveHandCards(req.lstCards);
                room.currentRound.laiziCard = false;
                LandlordHandOutCardNtf ntf = new LandlordHandOutCardNtf();
                ntf.roleid = accountId;
                ntf.bHandOut = req.bHandOut;
                ntf.cardForm = formType;
                ntf.lstCards = req.lstCards;
                ntf.laiziType = req.laiziType;
                room.SendRoomMsgEx(accountId, ntf);

                if (role.currentRound.handCards.Count == 0)
                {
                    // 春天或反春天
                    if (room.isSpring || room.isRSpring)
                    {
                        // 春天
                        room.currentRound.publicScales.springScales *= 2;

                        // 通知总倍数变化
                        room.RefreshAllRoleTotalScales();
                    }

                    room.currentRound.lastOutCardItem.RecordOutCard(accountId, req.lstCards, req.laiziType);

                    // 结算
                    room.currentRound.winRoleId = accountId;
                    room.Settle();

                    LandlordWinOneRoundB2L winNtf = new LandlordWinOneRoundB2L();
                    player.SendMsgToLobby(winNtf);
                }
                else
                {
                    room.currentRound.lastOutCardItem.RecordOutCard(accountId, req.lstCards, req.laiziType);

                    // 下个人出牌
                    role.nextRole.NotifyNeedHandOutCard(false);
                }
            }
            else
            {
                LandlordHandOutCardNtf ntf = new LandlordHandOutCardNtf();
                ntf.roleid = accountId;
                ntf.bHandOut = req.bHandOut;
                ntf.cardForm = formType;
                ntf.lstCards = req.lstCards;
                ntf.laiziType = req.laiziType;
                room.SendRoomMsgEx(accountId, ntf);

                LordRoomRole nextRole = role.nextRole;
                ECardFormType eCardForm = LordHelp.GetCardFormType(nextRole.currentRound.handCards.ToList(), 0);
                room.currentRound.laiziCard = eCardForm == ECardFormType.Laizigou;
                //如果下一个出牌的玩家只剩下一个癞子跳过这个玩家  如果出癞子的玩家是当前轮最大的玩家强制下家出牌
                if (eCardForm == ECardFormType.Laizigou)
                {
                    if (room.currentRound.lastOutCardItem.roleid == nextRole.roleid)
                    {
                        nextRole.nextRole.NotifyNeedHandOutCard(true);
                    }
                    else
                    {
                        nextRole.nextRole.NotifyNeedHandOutCard(false);
                    }
                }
                else
                {
                    nextRole.NotifyNeedHandOutCard(false);
                }
            }

            rsp.errorCode = LandlordHandOutCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            role.currentRound.playState = ERolePlayState.Wait;
            room.SaveDb();
        }

        string ListToString(List<UInt32> lstCards)
        {
            string strResult = "";
            foreach (var cardid in lstCards)
            {
                strResult += cardid.ToString();
                strResult += ",";
            }
            return strResult;
        }

        /// <summary>
        /// (斗地主)下一步
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordNextStepReq(UInt32 from, UInt64 accountId, LandlordNextStepReq req)
        {
            LandlordNextStepRsp rsp = new LandlordNextStepRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordNextStepReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordNextStepReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            if (room.roomState != ERoomState.WaitRound)
            {
                rsp.errorCode = LandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnLandlordNextStepReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordNextStepReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (role.roleState != ERoomRoleState.Settle)
            {
                rsp.errorCode = LandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordNextStepReq],玩家状态不对，不在结算状态,accountId={0}", accountId);
                return;
            }

            role.roleState = ERoomRoleState.WaitRound;

            rsp.errorCode = LandlordNextStepRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LandlordNextStepNtf ntf = new LandlordNextStepNtf();
            ntf.roleid = accountId;
            room.SendRoomMsgEx(accountId, ntf);
            room.currentRound.maxFen = 0;
            // 所有玩家都过了结算界面
            if (room.IsAllRoleWaitRound())
            {
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// (斗地主)解散游戏
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordDismissGameReq(UInt32 from, UInt64 accountId, LandlordDismissGameReq req)
        {
            LandlordDismissGameRsp rsp = new LandlordDismissGameRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordDismissGameReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDismissGameReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDismissGameReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = LandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDismissGameReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            if (room.dismissInfo.bStart)
            {
                rsp.errorCode = LandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDismissGameReq],上次解散流程未走完,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            // 重新开始解散流程
            room.dismissInfo.Clear();
            room.dismissInfo.bStart = true;
            room.dismissInfo.expireTime = env.Timer.CurrentDateTime.AddMinutes(2);
            room.dismissInfo.firstRoleId = accountId;
            room.dismissInfo.setAgreeRoles.Add(accountId);

            rsp.errorCode = LandlordDismissGameRsp.ErrorID.Success;
            rsp.remainSecond = room.dismissRemainSecond;
            player.SendMsgToClient(rsp);

            LandlordDismissGameNtf ntf = new LandlordDismissGameNtf();
            ntf.roleid = role.roleid;
            ntf.remainSecond = room.dismissRemainSecond;
            room.SendRoomMsgEx(role.roleid, ntf);

            room.SaveDb();
        }

        /// <summary>
        /// 解散是否同意
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordDismissAgreeReq(UInt32 from, UInt64 accountId, LandlordDismissAgreeReq req)
        {
            LandlordDismissAgreeRsp rsp = new LandlordDismissAgreeRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordDismissAgreeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDismissAgreeReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDismissAgreeReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = LandlordDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordDismissAgreeReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = LandlordDismissAgreeRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 解散流程已结束
            if (room.dismissInfo.firstRoleId == 0)
            {
                return;
            }

            LandlordDismissAgreeNtf ntf = new LandlordDismissAgreeNtf();
            ntf.roleid = accountId;
            ntf.bAgree = req.bAgree;
            room.SendRoomMsgEx(accountId, ntf);

            if (req.bAgree)
            {
                room.dismissInfo.setAgreeRoles.Add(accountId);
            }
            else
            {
                room.dismissInfo.setNotRoles.Add(accountId);
            }

            // 解散房间成功
            if (room.dismissInfo.setAgreeRoles.Count >= (LordRoom.MAX_ROLES_COUNT))
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
                return;
            }

            // 解散房间失败
            if (room.dismissInfo.setNotRoles.Count >= 1)
            {
                LandlordDismissGameSuccessNtf successNtf = new LandlordDismissGameSuccessNtf();
                successNtf.bSuccess = false;
                room.SendRoomMsg(successNtf);

                room.dismissInfo.Clear();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordLeaveRoomReq(UInt32 from, UInt64 accountId, LandlordLeaveRoomReq req)
        {
            LandlordLeaveRoomRsp rsp = new LandlordLeaveRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[LandlordModule][OnLandlordLeaveRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordLeaveRoomReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            LordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = LandlordLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordLeaveRoomReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = LandlordLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = LandlordLeaveRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LandlordLeaveRoomNtf ntf = new LandlordLeaveRoomNtf();
            ntf.roleid = accountId;

            // 不是房主
            if (accountId != room.roomOwner.roleid)
            {
                room.SendRoomMsgEx(accountId, ntf);

                // 解绑战斗服
                player.UnBindBattleFromGate();

                // 通知lobby离开房间
                LandlordLeaveRoomB2L ntfLobby = new LandlordLeaveRoomB2L();
                ntfLobby.roomUuid = room.roomUuid;
                player.SendMsgToLobby(ntfLobby);

                g.playerMgr.RemovePlayer(role.roleid);

                room.RemoveRole(accountId);

                room.SaveDb();

                return;
            }

            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }

        /// <summary>
        /// 聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordChatReq(UInt32 from, UInt64 accountId, LandlordChatReq req)
        {
            LandlordChatRsp rsp = new LandlordChatRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnLandlordChatReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = LandlordChatRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnLandlordChatReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = LandlordChatRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LandlordChatNtf ntf = new LandlordChatNtf();
            ntf.roleid = accountId;
            ntf.chatid = req.chatid;
            room.SendRoomMsgEx(accountId, ntf);
        }
        /// <summary>
        /// 用户输入文字聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordChatWordReq(UInt32 from, UInt64 accountId, LandlordChatWordReq req)
        {
            LandlordChatWordRsp rsp = new LandlordChatWordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[LandlordModule][OnLandlordChatWordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            if (room == null)
            {
                rsp.errorCode = LandlordChatWordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnLandlordChatWordReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = LandlordChatWordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LandlordChatWordNtf ntf = new LandlordChatWordNtf();
            ntf.roleId = accountId;
            ntf.chatWord = req.chatWord;
            room.SendRoomMsgEx(accountId, ntf);
        }

        /// <summary>
        /// 扔鸡蛋动画
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordEggReq(UInt32 from, UInt64 accountId, LandlordEggReq req)
        {
            LandlordEggRsp rsp = new LandlordEggRsp();
            rsp.errorCode = LandlordEggRsp.ErrorID.Failed;

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[LandlordModule][OnLandlordEggReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            BattlePlayer bPlayer = g.playerMgr.FindPlayer(req.roleid);
            if (bPlayer == null)
            {
                LogSys.Warn("[LandlordModule][OnLandlordEggReq],找不到玩家,accountId={0}", req.roleid);
                player.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = LandlordEggRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LordRoom room = g.lordRoomMgr.GetRoom(player.RoomUuid);
            LandlordEggNtf ntf = new LandlordEggNtf();
            ntf.throwRoleid = accountId;
            ntf.beRoleid = req.roleid;
            ntf.eggType = req.eggType;
            foreach (LordRoomRole role in room.lstRoles)
            {
                if (role.roleid != accountId)
                {
                    role.SendMsgToClient(ntf);
                }
            }
        }

        /// <summary>
        /// 房主建房解散房间l2b
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnLandlordOtherCloseRoomL2B(UInt32 from, UInt64 accountId, LandlordOtherCloseRoomL2B req)
        {
            DissolveTheRoomRsp rsp = new DissolveTheRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                player = new BattlePlayer();
                player.AccountId = accountId;
            }

            LordRoom room = g.lordRoomMgr.GetRoom(req.roomId);
            if (null == room)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnMJYunChengOtherCloseRoomL2B],房间不存在,accountId={0}, roomUuid={1}", accountId, req.roomId);
                return;
            }
            room.roomOwner.roleid = accountId;

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[LandlordModule][OnMJYunChengLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = DissolveTheRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            LandlordLeaveRoomNtf ntf = new LandlordLeaveRoomNtf();
            ntf.roleid = accountId;

            //直接关闭房间
            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }
    }
}