﻿using BattleServer;
using GoldenFlower;
using HelpAsset.FlowerHelp;
using Log;
using Protocols;
using Protocols.GoldenFlower;
using ServerLib;
using ServerLib.Bus;
using System;
using System.Collections.Generic;

namespace Module
{
    public class GoldenFlowerModule : LogicModule
    {
        public GoldenFlowerModule()
            : base()
        {
        }

        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("GoldenFlowerModule.OnInit()");
            // (扎金花)创建房间
            RegisterMsgHandler(MSGID.GoldenFlowerCreateRoomL2B, new MsgHandler<GoldenFlowerCreateRoomL2B>(OnGoldenFlowerCreateRoomL2B));

            // (扎金花)加入房间
            RegisterMsgHandler(MSGID.GoldenFlowerJoinRoomL2B, new MsgHandler<GoldenFlowerJoinRoomL2B>(OnGoldenFlowerJoinRoomL2B));

            // (扎金花)获取房间信息
            RegisterMsgHandler(MSGID.GoldenFlowerGetRoomInfoL2B, new MsgHandler<GoldenFlowerGetRoomInfoL2B>(OnGoldenFlowerGetRoomInfoL2B));

            //(扎金花)开始游戏
            RegisterMsgHandler(MSGID.GoldenFlowerStartReq, new MsgHandler<GoldenFlowerStartReq>(OnGoldenFlowerStartReq));

            //(扎金花)开始底分请求
            RegisterMsgHandler(MSGID.GoldenFlowerStartFenReq, new MsgHandler<GoldenFlowerStartFenReq>(OnGoldenFlowerStartFenReq));

            // (扎金花)看牌通知
            RegisterMsgHandler(MSGID.GoldenFlowerShowCardReq, new MsgHandler<GoldenFlowerShowCardReq>(OnGoldenFlowerShowCardReq));

            // (扎金花)加注通知
            RegisterMsgHandler(MSGID.GoldenFlowerRaiseCardReq, new MsgHandler<GoldenFlowerRaiseCardReq>(OnGoldenFlowerRaiseCardReq));

            // (扎金花)跟注通知
            RegisterMsgHandler(MSGID.GoldenFlowerDarkCardReq, new MsgHandler<GoldenFlowerDarkCardReq>(OnGoldenFlowerDarkCardReq));

            // (扎金花)比牌
            RegisterMsgHandler(MSGID.GoldenFlowerHandOutCardReq, new MsgHandler<GoldenFlowerHandOutCardReq>(OnGoldenFlowerHandOutCardReq));

            // (扎金花)弃牌
            RegisterMsgHandler(MSGID.GoldenFlowerCallCardReq, new MsgHandler<GoldenFlowerCallCardReq>(OnGoldenFlowerCallCardReq));

            // (扎金花)下一步
            RegisterMsgHandler(MSGID.GoldenFlowerNextStepReq, new MsgHandler<GoldenFlowerNextStepReq>(OnGoldenFlowerNextStepReq));

            // (扎金花)解散游戏
            RegisterMsgHandler(MSGID.GoldenFlowerDismissGameReq, new MsgHandler<GoldenFlowerDismissGameReq>(OnGoldenFlowerDismissGameReq));

            // (扎金花)解散是否同意
            RegisterMsgHandler(MSGID.GoldenFlowerDismissAgreeReq, new MsgHandler<GoldenFlowerDismissAgreeReq>(OnGoldenFlowerDismissAgreeReq));

            // (扎金花)离开房间，房间未开始时
            RegisterMsgHandler(MSGID.GoldenFlowerLeaveRoomReq, new MsgHandler<GoldenFlowerLeaveRoomReq>(OnGoldenFlowerLeaveRoomReq));

            // (扎金花)聊天
            RegisterMsgHandler(MSGID.GoldenFlowerChatReq, new MsgHandler<GoldenFlowerChatReq>(OnGoldenFlowerChatReq));

            // (扎金花)文字聊天
            RegisterMsgHandler(MSGID.GoldenFlowerChatWordReq, new MsgHandler<GoldenFlowerChatWordReq>(OnGoldenFlowerChatWordReq));

            //扔鸡蛋
            RegisterMsgHandler(MSGID.GoldenFlowerEggReq, new MsgHandler<GoldenFlowerEggReq>(OnGoldenFlowerEggReq));

            //房主建房
            RegisterMsgHandler(MSGID.GoldenFlowerOtherCloseRoomL2B, new MsgHandler<GoldenFlowerOtherCloseRoomL2B>(OnGoldenFlowerOtherCloseRoomL2B));
            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }


        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerCreateRoomL2B(UInt32 from, UInt64 accountId, GoldenFlowerCreateRoomL2B req)
        {
            GoldenFlowerCreateRoomRsp rsp = new GoldenFlowerCreateRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerCreateRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            UInt64 roomuuid = req.roomUuid;
            GoldenFlowerRoom room = new GoldenFlowerRoom(roomuuid);
            room.config = req.config;
            if (req.config.payMethod != PayMethod.anotherPay)
            {
                room.InitRoom(accountId, player.NickName, EGameType.GoldenFlower);
                player.RoomUuid = roomuuid;
                AddRoleToRoom(player, room);
            }
            else
            {
                room.InitRoom2(accountId,player.NickName, EGameType.GoldenFlower);
            }
            g.goldenflowerRoomMgr.AddRoom(room);

            rsp.errorCode = GoldenFlowerCreateRoomRsp.ErrorID.Success;
            if (req.config.payMethod != PayMethod.anotherPay)
            {
                rsp.roomInfo = room.GetRoomInfo(accountId);
            }
            player.SendMsgToClient(rsp);

            room.SaveDb(true);
        }

        void AddRoleToRoom(BattlePlayer player, GoldenFlowerRoom room)
        {
            GoldenFlowerRoomRole role = new GoldenFlowerRoomRole();
            role.Init(player.AccountId, player.NickName, room);
            role.position = room.GetOneSeat();
            role.sex = player.sex;
            role.level = player.level;
            role.roleState = ERoomRoleState.WaitRoomer;
            role.weixinHeadImgUrl = player.weixinHeadImgUrl;
            room.AddRole(role);
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerJoinRoomL2B(UInt32 from, UInt64 accountId, GoldenFlowerJoinRoomL2B req)
        {
            GoldenFlowerJoinRoomRsp rsp = new GoldenFlowerJoinRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(req.roomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            if (room.IsRoomFull())
            {
                rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerJoinRoomL2B],房间已满,accountId={0}", accountId);
                return;
            }
            
            player.RoomUuid = req.roomUuid;
            AddRoleToRoom(player, room);
            if (room.config.payMethod== PayMethod.anotherPay)
            {
                if (room.lstRoles.Count==1)
                {
                    room.roomPlayerID = accountId;
                }
            }
            rsp.errorCode = GoldenFlowerJoinRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);
            room.SaveDb();
            // 通知其他人
            GoldenFlowerJoinRoomNtf ntf = new GoldenFlowerJoinRoomNtf();
            ntf.remoteRoleInfo = room.GetRole(accountId).remoteInfo;
            ntf.roomPlayerId = room.roomPlayerID;
            room.SendRoomMsgEx(accountId, ntf);      
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerGetRoomInfoL2B(UInt32 from, UInt64 accountId, GoldenFlowerGetRoomInfoL2B req)
        {
            GoldenFlowerGetRoomInfoRsp rsp = new GoldenFlowerGetRoomInfoRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerGetRoomInfoL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;

            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerGetRoomInfoL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerGetRoomInfoL2B],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = GoldenFlowerGetRoomInfoRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.CheckDismissTimer();
        }

        ///开始游戏
        ///
        async void OnGoldenFlowerStartReq(UInt32 from, UInt64 accountId, GoldenFlowerStartReq req)
        {

            GoldenFlowerStartRsp rsp = new GoldenFlowerStartRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerStartRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerStartRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerStartRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerStartRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerStartRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }
            foreach (var i in room.lstRoles)
            {
                BattlePlayer player1 = g.playerMgr.FindPlayer(i.roleid);
                if (player1==null)
                {
                    rsp.errorCode = GoldenFlowerStartRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[GoldenFlowerModule][GoldenFlowerStartRsp],有位玩家不在线,accountId={0}", accountId);
                    return;
                }                
            }

            room.FirstInit();
            room.StartNewRound(accountId);                   
            room.SaveDb();

            GoldenFlowerStartB2L b2l = new GoldenFlowerStartB2L();
            b2l.roomId = room.roomUuid;
            player.SendMsgToLobby(b2l);
        }

        ///开始底分请求
        ///
        async void OnGoldenFlowerStartFenReq(UInt32 from, UInt64 accountId, GoldenFlowerStartFenReq req)
        {

            GoldenFlowerStartFenRsp rsp = new GoldenFlowerStartFenRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerStartFenRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerStartFenRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerStartFenRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerStartFenRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerStartFenRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }
            GoldenFlowerRoomInfo roomInfo = room.GetRoomInfo(accountId);           
            role.currentRound.scales -= (int)room.config.fold;
            room.currentRound.publicScales.Scales+=(int)room.config.fold;
            room.SaveDb();
            rsp.fen = (int)room.config.fold;

            GoldenFlowerTotalScalesChangeNtf totalNtf = new GoldenFlowerTotalScalesChangeNtf();
            totalNtf.playerId = accountId;
            totalNtf.roomFen = role.currentRound.scales;
            totalNtf.totalScales = room.currentRound.publicScales.Scales;
            room.SendRoomMsgAll(totalNtf);
           
        }



        /// <summary>
        /// 看牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="reg"></param>
        async void OnGoldenFlowerShowCardReq(UInt32 from, UInt64 accountId, GoldenFlowerShowCardReq req)
        {
            
            GoldenFlowerShowCardRsp rsp = new GoldenFlowerShowCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerShowCardRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerShowCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerShowCardRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerShowCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerShowCardRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }
            
                              
           
            role.currentRound.currentState = ERoomPlayerState.ShowCard;           
            rsp.errorCode = GoldenFlowerShowCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
            // 通知总倍数变化
            // room.RefreshAllRoleTotalScales();
            GoldenFlowerShowCardNtf ntf = new GoldenFlowerShowCardNtf();
            ntf.errorCode = GoldenFlowerShowCardNtf.ErrorID.Success;
            ntf.playId = accountId;
            room.SendRoomMsgEx(accountId, ntf);
            room.SaveDb();
        }

        /// <summary>
        /// 弃牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="reg"></param>
        async void OnGoldenFlowerCallCardReq(UInt32 from, UInt64 accountId, GoldenFlowerCallCardReq req)
        {

            GoldenFlowerCallCardRsp rsp = new GoldenFlowerCallCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerShowCardRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerCallCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerShowCardRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerCallCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerShowCardRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }
            GoldenFlowerCallCardNtf ntf = new GoldenFlowerCallCardNtf();
            ntf.errorCode = GoldenFlowerCallCardNtf.ErrorID.Success;
            ntf.playId = accountId;          
            role.currentRound.playState = ERolePlayState.Null; 
            role.currentRound.currentState = ERoomPlayerState.CallGoldFlower;
            rsp.errorCode = GoldenFlowerCallCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);


            room.SendRoomMsgEx(accountId, ntf);
            if (!room.isWinplayer())
            {
                room.SendRoomMsgAllNextPlay(accountId);
            }
            room.SaveDb();
            // 通知总倍数变化
            // room.RefreshAllRoleTotalScales();
        }
        /// <summary>
        /// 加注通知
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerRaiseCardReq(UInt32 from, UInt64 accountId, GoldenFlowerRaiseCardReq req)
        {
            GoldenFlowerRaiseCardRsp rsp = new GoldenFlowerRaiseCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerRaiseCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerRaiseCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }
            GoldenFlowerRoomInfo roomInfo = room.GetRoomInfo(accountId);
            int roundNumber = 0;
            switch (roomInfo.config.roundNumber)
            {
                case Rounds.Null:
                    roundNumber = 99999;
                    break;
                case Rounds.Twenty:
                    roundNumber = 20;
                    break;
                case Rounds.forty:
                    roundNumber = 40;
                    break;
            }


            if (role.betOnNumber> roundNumber)
            {
                rsp.errorCode=GoldenFlowerRaiseCardRsp.ErrorID.Failed;
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],下注次数已达限制,accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }
            int JiaZhu = 0;
            if (role.currentRound.currentState == ERoomPlayerState.ShowCard)
            {
                JiaZhu = req.fen * 2;
                room.currentRound.counterList.Add(req.fen);
                room.currentRound.counterList.Add(req.fen);
            }
            else
            {
                JiaZhu = req.fen;
                room.currentRound.counterList.Add(req.fen);
            }
            role.currentRound.scales -= JiaZhu;
            rsp.fen = req.fen;
            room.currentRound.publicScales.Scales += JiaZhu;
            role.betOnNumber += 1;
            GoldenFlowerRaiseCardNtf ntf = new GoldenFlowerRaiseCardNtf();
            ntf.playId = accountId;
            ntf.fen = req.fen;
            room.currentRound.playerFen = req.fen;        
            role.currentRound.playState = ERolePlayState.Wait;


            room.SaveDb();
            // 通知总倍数变化       
            room.SendRoomMsgEx(accountId, ntf);
            player.SendMsgToClient(rsp);
            GoldenFlowerTotalScalesChangeNtf totalNtf = new GoldenFlowerTotalScalesChangeNtf();
            totalNtf.playerId = accountId;
            totalNtf.roomFen = role.currentRound.scales;
            totalNtf.totalScales = room.currentRound.publicScales.Scales;
            room.SendRoomMsgAll(totalNtf);
            room.SendRoomMsgAllNextPlay(accountId);

        }

        /// <summary>
        /// 跟注通知
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerDarkCardReq(UInt32 from, UInt64 accountId, GoldenFlowerDarkCardReq req)
        {
            GoldenFlowerDarkCardRsp rsp = new GoldenFlowerDarkCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerDarkCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerDarkCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomInfo roomInfo = room.GetRoomInfo(accountId);
            int roundNumber = 0;
            switch (roomInfo.config.roundNumber)
            {
                case Rounds.Null:
                    roundNumber = 99999;
                    break;
                case Rounds.Twenty:
                    roundNumber = 20;
                    break;
                case Rounds.forty:
                    roundNumber = 40;
                    break;
            }


            if (role.betOnNumber > roundNumber)
            {
                rsp.errorCode = GoldenFlowerDarkCardRsp.ErrorID.Failed;
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],下注次数已达限制,accountId={0}", accountId);
                player.SendMsgToClient(rsp);
                return;
            }

            int JiaZhu = 0;
            if (role.currentRound.currentState == ERoomPlayerState.ShowCard)
            {
                JiaZhu = room.currentRound.playerFen * 2;
                room.currentRound.counterList.Add(room.currentRound.playerFen);
                room.currentRound.counterList.Add(room.currentRound.playerFen);
            }
            else
            {
                JiaZhu = room.currentRound.playerFen;
                room.currentRound.counterList.Add(room.currentRound.playerFen);
            }
            role.currentRound.scales-=JiaZhu;            
            rsp.fen = room.currentRound.playerFen;
            room.currentRound.publicScales.Scales += JiaZhu;
            role.betOnNumber += 1;
            GoldenFlowerDarkCardNtf ntf = new GoldenFlowerDarkCardNtf();
            ntf.playId = accountId;
            ntf.fen = room.currentRound.playerFen;
            role.currentRound.playState = ERolePlayState.Wait;
            room.SaveDb();
            // 通知总倍数变化       
            room.SendRoomMsgEx(accountId, ntf);
            player.SendMsgToClient(rsp);
            GoldenFlowerTotalScalesChangeNtf totalNtf = new GoldenFlowerTotalScalesChangeNtf();
            totalNtf.playerId = accountId;
            totalNtf.roomFen = role.currentRound.scales;
            totalNtf.totalScales = room.currentRound.publicScales.Scales;
            room.SendRoomMsgAll(totalNtf);
            room.SendRoomMsgAllNextPlay(accountId);
        }

        /// <summary>
        /// 比牌通知
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerHandOutCardReq(UInt32 from, UInt64 accountId, GoldenFlowerHandOutCardReq req)
        {
            GoldenFlowerHandOutCardRsp rsp = new GoldenFlowerHandOutCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerRaiseCardRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }
            GoldenFlowerRoomInfo roomInfo = room.GetRoomInfo(accountId);
            if (role.currentRound.currentState == ERoomPlayerState.ShowCard)
            {
                role.currentRound.scales = role.currentRound.scales - room.currentRound.playerFen * 2;
                room.currentRound.publicScales.Scales = room.currentRound.publicScales.Scales + room.currentRound.playerFen * 2;
            }else if (role.currentRound.currentState == ERoomPlayerState.NoShowCard)
            {
                role.currentRound.scales = role.currentRound.scales - room.currentRound.playerFen;
                room.currentRound.publicScales.Scales = room.currentRound.publicScales.Scales + room.currentRound.playerFen;
            }


               
            GoldenFlowerRoomRole otherRole = room.GetRole(req.otherId);
            List<UInt32> list = new List<UInt32>();
            List<UInt32> otherList = new List<UInt32>();
            foreach ( var i in role.currentRound.handCards)
            {
                list.Add(i);
            }
            foreach (var i in otherRole.currentRound.handCards)
            {
                otherList.Add(i);
            }
            bool bo= FlowerHelp.pkPai(list, otherList);
            GoldenFlowerHandOutCardNtf ntf = new GoldenFlowerHandOutCardNtf();
            ntf.errorCode = GoldenFlowerHandOutCardNtf.ErrorID.Success;
            if (bo)
            {
                rsp.iswin = true;
                rsp.otherId = req.otherId;
                ntf.playId =accountId;
                ntf.otherId = req.otherId;
                ntf.winId = accountId;                
                otherRole.currentRound.currentState = ERoomPlayerState.Defeated;
            }
            else
            {
                role.currentRound.currentState = ERoomPlayerState.Defeated;              
                ntf.playId = accountId;
                ntf.otherId = req.otherId;
                ntf.winId = req.otherId;
                rsp.iswin = false;
                rsp.otherId = req.otherId;
            }
            role.currentRound.playState = ERolePlayState.Wait;
            room.SendRoomMsgEx(accountId, ntf);
            player.SendMsgToClient(rsp);
            room.SaveDb();
            // 通知总倍数变化
            GoldenFlowerTotalScalesChangeNtf totalNtf = new GoldenFlowerTotalScalesChangeNtf();
            totalNtf.playerId = accountId;
            totalNtf.roomFen =  role.currentRound.scales;
            totalNtf.totalScales = room.currentRound.publicScales.Scales;
            room.SendRoomMsgAll(totalNtf);

            
            if (!room.isWinplayer())
            {
                room.SendRoomMsgAllNextPlay(accountId);
            }
            
        }

        /// <summary>
        /// 下一步请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGoldenFlowerNextStepReq(UInt32 from, UInt64 accountId, GoldenFlowerNextStepReq req)
        {
            GoldenFlowerNextStepRsp rsp = new GoldenFlowerNextStepRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerNextStepRsp],找不到玩家,accountId={0}", accountId);
                return;
            }


            GoldenFlowerRoom room = await g.goldenflowerRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerNextStepRsp],房间不存在,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            role.roleState = ERoomRoleState.Play;
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][GoldenFlowerNextStepRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }
            
            foreach (var i  in room.playerList)
            {
                if (i==accountId)
                {
                    rsp.errorCode = GoldenFlowerNextStepRsp.ErrorID.Failed;
                    return;             
                }               
            }
            room.playerList.Add(accountId);
            role.currentRound.prepare = true;
            GoldenFlowerNextStepNtf ntf = new GoldenFlowerNextStepNtf();
            ntf.playID = accountId;
            room.SendRoomMsgEx(accountId, ntf);
            player.SendMsgToClient(rsp);           
            if (room.playerList.Count == room.lstRoles.Count)
            {                
                room.StartNewRound(room.currentRound.nextPlay);
                
            }
            room.SaveDb();


        }
            /// <summary>
            /// 扎金花解散游戏
            /// </summary>
            /// <param name="from"></param>
            /// <param name="accountId"></param>
            /// <param name="req"></param>
            void OnGoldenFlowerDismissGameReq(UInt32 from, UInt64 accountId, GoldenFlowerDismissGameReq req)
        {
            GoldenFlowerDismissGameRsp rsp = new GoldenFlowerDismissGameRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissGameReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoom room = g.goldenflowerRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissGameReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissGameReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = GoldenFlowerDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissGameReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            if (room.dismissInfo.bStart)
            {
                rsp.errorCode = GoldenFlowerDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissGameReq],上次解散流程未走完,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            // 重新开始解散流程
            room.dismissInfo.Clear();
            room.dismissInfo.bStart = true;
            room.dismissInfo.expireTime = env.Timer.CurrentDateTime.AddMinutes(2);
            room.dismissInfo.firstRoleId = accountId;
            room.dismissInfo.setAgreeRoles.Add(accountId);

            rsp.errorCode = GoldenFlowerDismissGameRsp.ErrorID.Success;
            rsp.remainSecond = room.dismissRemainSecond;
            player.SendMsgToClient(rsp);

            GoldenFlowerDismissGameNtf ntf = new GoldenFlowerDismissGameNtf();
            ntf.roleid = role.roleid;
            ntf.remainSecond = room.dismissRemainSecond;
            room.SendRoomMsgEx(role.roleid, ntf);
            room.settle();
            room.SaveDb();
        }

        /// <summary>
        /// 解散是否同意
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerDismissAgreeReq(UInt32 from, UInt64 accountId, GoldenFlowerDismissAgreeReq req)
        {
            GoldenFlowerDismissAgreeRsp rsp = new GoldenFlowerDismissAgreeRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissAgreeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoom room = g.goldenflowerRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissAgreeReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissAgreeReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = GoldenFlowerDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerDismissAgreeReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = GoldenFlowerDismissAgreeRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 解散流程已结束
            if (room.dismissInfo.firstRoleId == 0)
            {
                return;
            }

            GoldenFlowerDismissAgreeNtf ntf = new GoldenFlowerDismissAgreeNtf();
            ntf.roleid = accountId;
            ntf.bAgree = req.bAgree;
            room.SendRoomMsgEx(accountId, ntf);

            if (req.bAgree)
            {
                room.dismissInfo.setAgreeRoles.Add(accountId);
            }
            else
            {
                room.dismissInfo.setNotRoles.Add(accountId);
            }


            
            // 解散房间成功
            if (room.dismissInfo.setAgreeRoles.Count >= room.lstRoles.Count)
            {
                room.NotifyDismissSuccess();
                room.settle();
                room.RoomClose();
                return;
            }

            // 解散房间失败
            if (room.dismissInfo.setNotRoles.Count >= 1)
            {
                GoldenFlowerDismissGameSuccessNtf successNtf = new GoldenFlowerDismissGameSuccessNtf();
                successNtf.bSuccess = false;
                room.SendRoomMsg(successNtf);

                room.dismissInfo.Clear();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerLeaveRoomReq(UInt32 from, UInt64 accountId, GoldenFlowerLeaveRoomReq req)
        {
            GoldenFlowerLeaveRoomRsp rsp = new GoldenFlowerLeaveRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerLeaveRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoom room = g.goldenflowerRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerLeaveRoomReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            GoldenFlowerRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = GoldenFlowerLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerLeaveRoomReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = GoldenFlowerLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = GoldenFlowerLeaveRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            GoldenFlowerLeaveRoomNtf ntf = new GoldenFlowerLeaveRoomNtf();
            ntf.roleid = accountId;

            // 不是房主
            if (accountId != room.roomOwner.roleid)
            {
              

                // 解绑战斗服
                player.UnBindBattleFromGate();

                // 通知lobby离开房间
                GoldenFlowerLeaveRoomB2L ntfLobby = new GoldenFlowerLeaveRoomB2L();
                ntfLobby.roomUuid = room.roomUuid;
                player.SendMsgToLobby(ntfLobby);

                g.playerMgr.RemovePlayer(role.roleid);

                room.RemoveRole(accountId);

                if (room.config.payMethod == PayMethod.anotherPay)
                {
                    if (room.lstRoles.Count!= 0)
                    {
                        room.roomPlayerID = room.lstRoles[0].roleid;
                    }
                }
                room.SaveDb();
                ntf.roomPlayID = room.roomPlayerID;
                room.SendRoomMsgEx(accountId, ntf);
              
                return;
            }
           
            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }

        /// <summary>
        /// 聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerChatReq(UInt32 from, UInt64 accountId, GoldenFlowerChatReq req)
        {
            GoldenFlowerChatRsp rsp = new GoldenFlowerChatRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnGoldenFlowerChatReq],找不到玩家,accountId={0}", accountId);
                return;
            }            
            GoldenFlowerRoom room= g.goldenflowerRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = GoldenFlowerChatRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnGoldenFlowerChatReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = GoldenFlowerChatRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            GoldenFlowerChatNtf ntf = new GoldenFlowerChatNtf();
            ntf.roleid = accountId;
            ntf.chatid = req.chatid;
            room.SendRoomMsgEx(accountId, ntf);
        }
        /// <summary>
        /// 用户输入文字聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerChatWordReq(UInt32 from, UInt64 accountId, GoldenFlowerChatWordReq req)
        {
            GoldenFlowerChatWordRsp rsp = new GoldenFlowerChatWordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerChatWordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            GoldenFlowerRoom room = g.goldenflowerRoomMgr.GetRoom(player.RoomUuid);
            if (room == null)
            {
                rsp.errorCode = GoldenFlowerChatWordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerChatWordReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = GoldenFlowerChatWordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            GoldenFlowerChatWordNtf ntf = new GoldenFlowerChatWordNtf();
            ntf.roleId = accountId;
            ntf.chatWord = req.chatWord;
            room.SendRoomMsgEx(accountId, ntf);
        }

        /// <summary>
        /// 扔鸡蛋动画
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerEggReq(UInt32 from, UInt64 accountId, GoldenFlowerEggReq req)
        {
            GoldenFlowerEggRsp rsp = new GoldenFlowerEggRsp();
            rsp.errorCode = GoldenFlowerEggRsp.ErrorID.Failed;

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerEggReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            BattlePlayer bPlayer = g.playerMgr.FindPlayer(req.roleid);
            if (bPlayer == null)
            {
                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerEggReq],找不到玩家,accountId={0}", req.roleid);
                player.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = GoldenFlowerEggRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            GoldenFlowerRoom room = g.goldenflowerRoomMgr.GetRoom(player.RoomUuid);
            GoldenFlowerEggNtf ntf = new GoldenFlowerEggNtf();
            ntf.throwRoleid = accountId;
            ntf.beRoleid = req.roleid;
            ntf.eggType = req.eggType;
            foreach (GoldenFlowerRoomRole role in room.lstRoles)
            {
                if (role.roleid != accountId)
                {
                    role.SendMsgToClient(ntf);
                }
            }
        }

        /// <summary>
        /// 房主建房解散房间l2b
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnGoldenFlowerOtherCloseRoomL2B(UInt32 from, UInt64 accountId, GoldenFlowerOtherCloseRoomL2B req)
        {
            DissolveTheRoomRsp rsp = new DissolveTheRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                player = new BattlePlayer();
                player.AccountId = accountId;
            }

            GoldenFlowerRoom room = g.goldenflowerRoomMgr.GetRoom(req.roomId);
            if (null == room)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerOtherCloseRoomL2B],房间不存在,accountId={0}, roomUuid={1}", accountId, req.roomId);
                return;
            }
            room.roomOwner.roleid = accountId;

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[GoldenFlowerModule][OnGoldenFlowerLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = DissolveTheRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            GoldenFlowerLeaveRoomNtf ntf = new GoldenFlowerLeaveRoomNtf();
            ntf.roleid = accountId;

            //直接关闭房间
            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }
    }
}
