﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerLib;
using Config;
using System.IO;
using Log;

namespace BattleServer
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string cmd in args)
            {
                if (cmd.Equals("step"))
                {
                    Console.WriteLine("press Enter to start......");
                    Console.ReadLine();
                    Console.WriteLine("BattleServer startup......");
                }
            }
            ServerConfig config = null;
            try
            {
                //读取配置
                config = new ServerConfig();
                config.Load(args[0]);
                config.ConfigPath = Path.GetDirectoryName(args[0]);
                //启动服务器
                if (!Battleenv.Start(config))
                {
                    LogSys.Error("......BattleServer start failed");
                }
            }
            catch (Exception ex)
            {
                if (config == null)
                {
                    Utility.ExceptionHandle.StandardExceptionHandler.HandleException(ex, Utility.ExceptionHandle.MiniDumper.Typ.MiniDumpWithFullMemory);
                }
                else
                {
                    if (config.DumpCfg.DumpSwitch)
                    {
                        Utility.ExceptionHandle.StandardExceptionHandler.HandleException(ex, (Utility.ExceptionHandle.MiniDumper.Typ)config.DumpCfg.DumpSetting);
                    }
                }
                // 这里查看内部异常，将内部实际的逻辑模块异常信息捕捉出来
                string errMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                string trace = ex.InnerException != null ? ex.InnerException.StackTrace : ex.StackTrace;

                LogSys.Error("......Program.Main Exception occurred: {0}", errMsg);
                LogSys.Error("{0}", trace);
            }

            LogSys.Info("BattleServer stop......");
            Console.ReadLine();
        }
    }
}
