﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using BattleServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using Protocols.ClassicLandlord;
using ClassicLandlord;
using HelpAsset.ClassicLandlord;

namespace Module
{
    /// <summary>
    /// 斗地主
    /// </summary>
    public class ClassicLandlordModule : LogicModule
    {
        public ClassicLandlordModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            // 创建房间
            RegisterMsgHandler(MSGID.ClassicLandlordCreateRoomL2B, new MsgHandler<ClassicLandlordCreateRoomL2B>(OnClassicLandlordCreateRoomL2B));

            // 加入房间
            RegisterMsgHandler(MSGID.ClassicLandlordJoinRoomL2B, new MsgHandler<ClassicLandlordJoinRoomL2B>(OnClassicLandlordJoinRoomL2B));

            // 获取房间信息
            RegisterMsgHandler(MSGID.ClassicLandlordGetRoomInfoL2B, new MsgHandler<ClassicLandlordGetRoomInfoL2B>(OnClassicLandlordGetRoomInfoL2B));

            // 开始游戏
            RegisterMsgHandler(MSGID.ClassicLandlordRoundConfigSelectReq, new MsgHandler<ClassicLandlordRoundConfigSelectReq>(OnClassicLandlordRoundConfigSelectReq));

            // (斗地主)明牌倍数
            //RegisterMsgHandler(MSGID.ClassicLandlordMingCardScalesReq, new MsgHandler<ClassicLandlordMingCardScalesReq>(OnClassicLandlordMingCardScalesReq));

            // (斗地主)叫地主
            RegisterMsgHandler(MSGID.ClassicLandlordCallLandlordReq, new MsgHandler<ClassicLandlordCallLandlordReq>(OnClassicLandlordCallLandlordReq));

            // (斗地主)抢地主
            //RegisterMsgHandler(MSGID.ClassicLandlordRobLandlordReq, new MsgHandler<ClassicLandlordRobLandlordReq>(OnClassicLandlordRobLandlordReq));
             
            // (斗地主)加倍请求
            //RegisterMsgHandler(MSGID.ClassicLandlordDoubleScalesReq, new MsgHandler<ClassicLandlordDoubleScalesReq>(OnClassicLandlordDoubleScalesReq));
            
            // (斗地主)出牌请求
            RegisterMsgHandler(MSGID.ClassicLandlordHandOutCardReq, new MsgHandler<ClassicLandlordHandOutCardReq>(OnClassicLandlordHandOutCardReq));
            
            // (斗地主)下一步
            RegisterMsgHandler(MSGID.ClassicLandlordNextStepReq, new MsgHandler<ClassicLandlordNextStepReq>(OnClassicLandlordNextStepReq));
            
            // (斗地主)解散游戏
            RegisterMsgHandler(MSGID.ClassicLandlordDismissGameReq, new MsgHandler<ClassicLandlordDismissGameReq>(OnClassicLandlordDismissGameReq));

            // 解散是否同意
            RegisterMsgHandler(MSGID.ClassicLandlordDismissAgreeReq, new MsgHandler<ClassicLandlordDismissAgreeReq>(OnClassicLandlordDismissAgreeReq));

            // 离开房间，房间未开始时
            RegisterMsgHandler(MSGID.ClassicLandlordLeaveRoomReq, new MsgHandler<ClassicLandlordLeaveRoomReq>(OnClassicLandlordLeaveRoomReq));

            // 聊天
            RegisterMsgHandler(MSGID.ClassicLandlordChatReq, new MsgHandler<ClassicLandlordChatReq>(OnClassicLandlordChatReq));

            //文字聊天
            RegisterMsgHandler(MSGID.ClassicLandlordChatWordReq, new MsgHandler<ClassicLandlordChatWordReq>(OnClassicLandlordChatWordReq));

            stCardFormInfo otherInfo = new stCardFormInfo();
            otherInfo.minCardType = ECardType.Five;
            otherInfo.maxCardType = ECardType.Seven;
            otherInfo.cardTypeCount = 3;

            List<stCardFormInfo> lstBigInfos = new List<stCardFormInfo>();

            LordHelp.CheckStraightBig(otherInfo, lstBigInfos);
            if (!g.lordRoomMgr.Init())
            {
                return false;
            }           
            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordCreateRoomL2B(UInt32 from, UInt64 accountId, ClassicLandlordCreateRoomL2B req)
        {
            ClassicLandlordCreateRoomRsp rsp = new ClassicLandlordCreateRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordCreateRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            UInt64 roomuuid = req.roomUuid;
            player.RoomUuid = roomuuid;

            ClassicLordRoom room = new ClassicLordRoom(roomuuid);
            room.InitRoom(accountId, player.NickName, EGameType.ClassicLandlord);
            room.config = req.config;
            AddRoleToRoom(player, room);

            g.classicLordRoomMgr.AddRoom(room);

            rsp.errorCode = ClassicLandlordCreateRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.SaveDb(true);
        }

        void AddRoleToRoom(BattlePlayer player, ClassicLordRoom room)
        {
            ClassicLordRoomRole role = new ClassicLordRoomRole();
            role.Init(player.AccountId, player.NickName, room);
            role.position = room.GetOneSeat();
            role.sex = player.sex;
            role.level = player.level;
            role.roleState = ERoomRoleState.WaitRoomer;
            role.weixinHeadImgUrl = player.weixinHeadImgUrl;
            role.activeValue = player.activeValue;
            role.vip = player.vip;
            role.chengweiId = player.chengweiId;
            room.AddRole(role);
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnClassicLandlordJoinRoomL2B(UInt32 from, UInt64 accountId, ClassicLandlordJoinRoomL2B req)
        {
            ClassicLandlordJoinRoomRsp rsp = new ClassicLandlordJoinRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordJoinRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = await g.classicLordRoomMgr.FindRoomAsync(req.roomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordJoinRoomL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            if (room.IsRoomFull())
            {
                rsp.errorCode = ClassicLandlordJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordJoinRoomL2B],房间已满,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;
            AddRoleToRoom(player, room);

            rsp.errorCode = ClassicLandlordJoinRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            // 通知其他人
            ClassicLandlordJoinRoomNtf ntf = new ClassicLandlordJoinRoomNtf();
            ntf.remoteRoleInfo = room.GetRole(accountId).remoteInfo;
            room.SendRoomMsgEx(accountId, ntf);

            // 房间人数已满，回合开始
            if (room.IsRoomFull())
            {
                room.FirstInit();
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnClassicLandlordGetRoomInfoL2B(UInt32 from, UInt64 accountId, ClassicLandlordGetRoomInfoL2B req)
        {
            ClassicLandlordGetRoomInfoRsp rsp = new ClassicLandlordGetRoomInfoRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordGetRoomInfoL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;

            ClassicLordRoom room = await g.classicLordRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordGetRoomInfoL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordGetRoomInfoL2B],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = ClassicLandlordGetRoomInfoRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.CheckDismissTimer();
        }

        /// <summary>
        /// 经典斗地主准备开始游戏
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordRoundConfigSelectReq(UInt32 from, UInt64 accountId, ClassicLandlordRoundConfigSelectReq req)
        {
            ClassicLandlordRoundConfigSelectRsp rsp = new ClassicLandlordRoundConfigSelectRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRoundConfigSelectReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordRoundConfigSelectRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRoundConfigSelectReq],房间不存在,accountId={0}", accountId);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordRoundConfigSelectRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRoundConfigSelectReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if(!role.IsRoundConfigSelectState)
            {
                rsp.errorCode = ClassicLandlordRoundConfigSelectRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRoundConfigSelectReq],玩家不在回合配置选择状态,accountId={0}", accountId);
                return;
            }

            role.currentRound.bMingCardStart = req.bMingCardStart;

            rsp.errorCode = ClassicLandlordRoundConfigSelectRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            ClassicLandlordRoundConfigSelectNtf ntf = new ClassicLandlordRoundConfigSelectNtf();
            ntf.roleid = accountId;
            //是否开始游戏
            ntf.bMingCardStart = req.bMingCardStart;
            room.SendRoomMsgEx(accountId, ntf);

            // 通知总倍数变化
            //room.RefreshAllRoleTotalScales();

            role.currentRound.playState = ERolePlayState.Wait;

            if(room.IsAllRoleRoundConfigFinish())
            {
                foreach(var roleitem in room.lstRoles)
                {
                    roleitem.NotifyShowNewCards();
                }
            }

            room.SaveDb();
        }

        /// <summary>
        /// (斗地主)明牌倍数
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordMingCardScalesReq(UInt32 from, UInt64 accountId, ClassicLandlordMingCardScalesReq req)
        {
            ClassicLandlordMingCardScalesRsp rsp = new ClassicLandlordMingCardScalesRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordMingCardScalesReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordMingCardScalesReq],房间不存在,accountId={0}", accountId);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordMingCardScalesReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if(!role.IsShowNewCardsState)
            {
                rsp.errorCode = ClassicLandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordMingCardScalesReq],是否在显示新牌状态,accountId={0}", accountId);
                return;
            }

            if(req.scales<1 || req.scales > 5)
            {
                rsp.errorCode = ClassicLandlordMingCardScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordMingCardScalesReq],明牌倍数不对,accountId={0},scales={1}", accountId, req.scales);
                return;
            }

            room.currentRound.SetMaxMingCardScales(req.scales);
            if(req.scales != 1)
            {
                role.currentRound.bMingPai = true;
            }

            rsp.errorCode = ClassicLandlordMingCardScalesRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            ClassicLandlordMingCardScalesNtf ntf = new ClassicLandlordMingCardScalesNtf();
            ntf.roleid = accountId;
            ntf.scales = req.scales;
            if(req.scales != 1)
            {
                ntf.handcards.AddRange(role.currentRound.handCards);
            }
            room.SendRoomMsgEx(accountId, ntf);

            // 通知总倍数变化
            room.RefreshAllRoleTotalScales();

            role.currentRound.playState = ERolePlayState.Wait;

            if (room.IsAllRoleShowNewCardsFinish())
            {
                // 庄家开始叫地主
               room.bankerRole.NotifyCallLandlord();                                      
            }

            ClassicLandlordNeedCallLandlordNtf ntf1 = new ClassicLandlordNeedCallLandlordNtf();
            ntf1.roleId = room.bankerRole.roleid;
            room.SendRoomMsgEx(room.bankerRole.roleid, ntf1);

            room.SaveDb();
        }

        /// <summary>
        /// 叫地主
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordCallLandlordReq(UInt32 from, UInt64 accountId, ClassicLandlordCallLandlordReq req)
        {
            ClassicLandlordCallLandlordRsp rsp = new ClassicLandlordCallLandlordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordCallLandlordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordCallLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordCallLandlordReq],房间不存在,accountId={0}", accountId);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordCallLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordCallLandlordReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsCallLandlordState)
            {
                rsp.errorCode = ClassicLandlordCallLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordCallLandlordReq],不在抢地主状态,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = ClassicLandlordCallLandlordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            role.currentRound.playState = ERolePlayState.Wait;
            //保存玩家当前回合叫的分
            role.currentRound.fen = req.fen;

            ClassicLandlordCallLandlordNtf ntf = new ClassicLandlordCallLandlordNtf();
            ntf.roleid = accountId;
            ntf.fen = req.fen;
            //通知叫地主的分数
            room.SendRoomMsgEx(accountId, ntf);

            //如果玩家叫3分 直接是地主 ，如果玩家叫的其他分 需要比对是否所有玩家都已经叫分，如果玩家没有都叫分
            //发送通知下一个玩家叫分，如果玩家都叫分选择叫分最大的为地主

            //获取当前叫分的最大值
            if (req.fen > room.fen)
            {
                //如果当前玩家叫的分大于最高分
                room.fen = req.fen;
            }

            bool flag = false;
            if (req.fen == Fen.threeFen)
            {
                //玩家叫3分 直接是地主
                room.currentRound.lordRoleId = player.AccountId;
                flag = true;
                // 抢地主确定
                ClassicLandlordRobLandlordResultNtf clrntf = new ClassicLandlordRobLandlordResultNtf();
                clrntf.lordRoleId = room.currentRound.lordRoleId;
                room.SendRoomMsg(ntf);

                // 把底牌加到地主身上
                room.lordRole.currentRound.AddHandCards(room.currentRound.bottomCards);
                return;
            }

            //没有确定地主 判断是否所有玩家都叫过地主
            foreach (ClassicLordRoomRole crole in room.lstRoles)
            {
                if (crole.currentRound.fen == Fen.weiJiao)
                {
                    //如果当前有玩家未叫过分，flag 设置为true
                    flag = true;
                }
            }

            //如果有玩家没有叫过地主
            if(flag)
            {
                // 下个人叫地主
                role.nextRole.NotifyCallLandlord();
                return;
            }
            else
            {
                //假设第一个用户叫的分是最高的分
                ClassicLordRoomRole dizhu = room.lstRoles[0];
                //如果三个人都叫过地主  找到谁是地主
                foreach (ClassicLordRoomRole crole in room.lstRoles)
                {
                    if (!crole.Equals(dizhu))
                    {
                        //如果当前用户的分数大于上一个玩家的分数
                        if(crole.currentRound.fen > dizhu.currentRound.fen)
                        {
                            dizhu = crole;
                        }
                    }
                }

                if(dizhu.currentRound.fen == Fen.buJiao)
                {
                    //所有玩家都选择的不叫 重新

                    // 抢地主失败，重新发牌
                    room.RefreshShowCard();
                    return;
                }

                room.currentRound.lordRoleId = dizhu.roleid;
                // 抢地主确定
                ClassicLandlordRobLandlordResultNtf clrntf = new ClassicLandlordRobLandlordResultNtf();
                clrntf.lordRoleId = room.currentRound.lordRoleId;
                room.SendRoomMsg(ntf);

                // 把底牌加到地主身上
                room.lordRole.currentRound.AddHandCards(room.currentRound.bottomCards);
            }

            room.SaveDb();
        }

        /// <summary>
        /// (斗地主)抢地主
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordRobLandlordReq(UInt32 from, UInt64 accountId, ClassicLandlordRobLandlordReq req)
        {
            ClassicLandlordRobLandlordRsp rsp = new ClassicLandlordRobLandlordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRobLandlordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordRobLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRobLandlordReq],房间不存在,accountId={0}", accountId);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordRobLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRobLandlordReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsRobLandlordState)
            {
                rsp.errorCode = ClassicLandlordRobLandlordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRobLandlordReq],不在抢地主状态,accountId={0}", accountId);
                return;
            }

            room.currentRound.AddRobRole(accountId, req.bRob);
            room.currentRound.publicScales.robScales = room.currentRound.CalRobScales();

            rsp.errorCode = ClassicLandlordRobLandlordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            role.currentRound.playState = ERolePlayState.Wait;

            ERobLordResult robResult = room.currentRound.RobLordResult(ref room.currentRound.lordRoleId);

            if (robResult == ERobLordResult.Success)
            {
                // 抢地主确定
                ClassicLandlordRobLandlordResultNtf ntf = new ClassicLandlordRobLandlordResultNtf();
                ntf.lordRoleId = room.currentRound.lordRoleId;
                room.SendRoomMsg(ntf);

                // 把底牌加到地主身上
                room.lordRole.currentRound.AddHandCards(room.currentRound.bottomCards);

                // 开始加倍
                foreach(var roleitem in room.lstRoles)
                {
                    roleitem.NotifyDoubleScales();
                }
            }
            else if(robResult == ERobLordResult.Failed)
            {
                // 抢地主失败，重新发牌
                room.RefreshShowCard();
            }
            else
            {
                ClassicLandlordRobLandlordNtf ntf = new ClassicLandlordRobLandlordNtf();
                ntf.roleid = accountId;
                ntf.bRob = req.bRob;
                room.SendRoomMsgEx(accountId, ntf);

                // 下个人抢地主
                role.nextRole.NotifyRobLandlord();
            }

            // 通知总倍数变化
            room.RefreshAllRoleTotalScales();

            room.SaveDb();
        }
             
        /// <summary>
        /// (斗地主)加倍请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordDoubleScalesReq(UInt32 from, UInt64 accountId, ClassicLandlordDoubleScalesReq req)
        {
            ClassicLandlordDoubleScalesRsp rsp = new ClassicLandlordDoubleScalesRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDoubleScalesReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordDoubleScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDoubleScalesReq],房间不存在,accountId={0}", accountId);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordDoubleScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDoubleScalesReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsDoubleScaleState)
            {
                rsp.errorCode = ClassicLandlordDoubleScalesRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordRobLandlordReq],不是加倍状态,accountId={0}", accountId);
                return;
            }

            if(req.bDoubleScales)
            {
                role.currentRound.scales = 2;
            }
            else
            {
                role.currentRound.scales = 1;
            }

            rsp.errorCode = ClassicLandlordDoubleScalesRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 通知总倍数变化
            room.RefreshAllRoleTotalScales();

            role.currentRound.playState = ERolePlayState.Wait;

            ClassicLandlordDoubleScalesNtf ntf = new ClassicLandlordDoubleScalesNtf();
            ntf.roleid = accountId;
            ntf.bDoubleScales = req.bDoubleScales;
            room.SendRoomMsgEx(accountId, ntf);

            if(room.IsAllRoleDoubleScalesFinish())
            {
                // 地主出牌
                room.lordRole.NotifyNeedHandOutCard();
            }

            room.SaveDb();
        }

        /// <summary>
        /// (斗地主)出牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordHandOutCardReq(UInt32 from, UInt64 accountId, ClassicLandlordHandOutCardReq req)
        {
            ClassicLandlordHandOutCardRsp rsp = new ClassicLandlordHandOutCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.IsHandOutCardState)
            {
                rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],不是出牌状态,accountId={0}", accountId);
                return;
            }

            ECardFormType formType = ECardFormType.Null;
            if (req.bHandOut)
            {
                if (req.lstCards.Count == 0)
                {
                    rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],出牌数量不能为0,accountId={0}, lstCards={1}", accountId, ListToString(req.lstCards));
                    return;
                }

                foreach (var cardid in req.lstCards)
                {
                    if (!role.currentRound.HasHandCard(cardid))
                    {
                        rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],手牌中没有该牌,accountId={0}, lstCards={1}, cardid={2}", accountId, ListToString(req.lstCards), cardid);
                        return;
                    }
                }

                formType = LordHelp.GetCardFormType(req.lstCards);
                if (formType == ECardFormType.Null)
                {
                    rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Failed;
                    player.SendMsgToClient(rsp);

                    LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],不是合法牌形,accountId={0}, lstCards={1}", accountId, ListToString(req.lstCards));
                    return;
                }

                if(formType == ECardFormType.Bomb || formType == ECardFormType.KingBomb)
                {
                    room.currentRound.publicScales.bombScales *= 2;

                    // 通知总倍数变化
                    room.RefreshAllRoleTotalScales();
                }

                // 上次出牌人不是自己
                if (room.currentRound.lastOutCardItem.roleid != 0)
                {
                    if (room.currentRound.lastOutCardItem.roleid != accountId)
                    {
                        // 牌不大于上家
                        if (!LordHelp.IsGreaterCards(req.lstCards, room.currentRound.lastOutCardItem.lstCards))
                        {
                            rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Failed;
                            player.SendMsgToClient(rsp);

                            LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordHandOutCardReq],牌不大于上家不能出,accountId={0}, lstCards={1}", accountId, ListToString(req.lstCards));
                            return;
                        }
                    }
                }

                // 出牌次数增加
                role.currentRound.handOutCardTimes++;
                role.currentRound.RemoveHandCards(req.lstCards);

                ClassicLandlordHandOutCardNtf ntf = new ClassicLandlordHandOutCardNtf();
                ntf.roleid = accountId;
                ntf.bHandOut = req.bHandOut;
                ntf.cardForm = formType;
                ntf.lstCards = req.lstCards;
                room.SendRoomMsgEx(accountId, ntf);

                if (role.currentRound.handCards.Count == 0)
                {
                    // 春天或反春天
                    if(room.isSpring || room.isRSpring)
                    {
                        // 春天
                        room.currentRound.publicScales.springScales *= 2;

                        // 通知总倍数变化
                        room.RefreshAllRoleTotalScales();
                    }

                    room.currentRound.lastOutCardItem.RecordOutCard(accountId, req.lstCards);

                    // 结算
                    room.currentRound.winRoleId = accountId;
                    room.Settle();

                    ClassicLandlordWinOneRoundB2L winNtf = new ClassicLandlordWinOneRoundB2L();
                    List<ClassicLordRoomRole> lstRoles = room.lstRoles;
                    //获取玩家地主还是农民
                    winNtf.winers.Add(accountId);
                    if (room.currentRound.lordRoleId == accountId)
                    {
                        //地主胜利
                        foreach(ClassicLordRoomRole lr in lstRoles)
                        {
                            if(lr.roleid != accountId)
                            {
                                winNtf.losers.Add(lr.roleid);
                            }
                        }
                    }
                    else
                    {
                        //农民胜利
                        foreach(ClassicLordRoomRole lr in lstRoles)
                        {
                            if(lr.roleid != accountId
                                && lr.roleid != room.currentRound.lordRoleId)
                            {
                                winNtf.winers.Add(lr.roleid);
                            }
                            else if(lr.roleid == room.currentRound.lordRoleId)
                            {
                                winNtf.losers.Add(lr.roleid);
                            }
                        }
                    }
                    player.SendMsgToLobby(winNtf);
                }
                else
                {
                    room.currentRound.lastOutCardItem.RecordOutCard(accountId, req.lstCards);

                    // 下个人出牌
                    role.nextRole.NotifyNeedHandOutCard();
                }
            }
            else
            {
                ClassicLandlordHandOutCardNtf ntf = new ClassicLandlordHandOutCardNtf();
                ntf.roleid = accountId;
                ntf.bHandOut = req.bHandOut;
                ntf.cardForm = formType;
                ntf.lstCards = req.lstCards;
                room.SendRoomMsgEx(accountId, ntf);

                // 下个人出牌
                role.nextRole.NotifyNeedHandOutCard();
            }

            rsp.errorCode = ClassicLandlordHandOutCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            role.currentRound.playState = ERolePlayState.Wait;
            room.SaveDb();
        }
        
        string ListToString(List<UInt32> lstCards)
        {
            string strResult = "";
            foreach(var cardid in lstCards)
            {
                strResult += cardid.ToString();
                strResult += ",";
            }
            return strResult;
        }
            
        /// <summary>
        /// (斗地主)下一步
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordNextStepReq(UInt32 from, UInt64 accountId, ClassicLandlordNextStepReq req)
        {
            ClassicLandlordNextStepRsp rsp = new ClassicLandlordNextStepRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordNextStepReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordNextStepReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            if (room.roomState != ERoomState.WaitRound)
            {
                rsp.errorCode = ClassicLandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnClassicLandlordNextStepReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordNextStepReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (role.roleState != ERoomRoleState.Settle)
            {
                rsp.errorCode = ClassicLandlordNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordNextStepReq],玩家状态不对，不在结算状态,accountId={0}", accountId);
                return;
            }

            role.roleState = ERoomRoleState.WaitRound;

            rsp.errorCode = ClassicLandlordNextStepRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            ClassicLandlordNextStepNtf ntf = new ClassicLandlordNextStepNtf();
            ntf.roleid = accountId;
            room.SendRoomMsgEx(accountId, ntf);

            // 所有玩家都过了结算界面
            if (room.IsAllRoleWaitRound())
            {
                room.StartNewRound();
            }

            room.SaveDb();
        }
            
        /// <summary>
        /// (斗地主)解散游戏
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordDismissGameReq(UInt32 from, UInt64 accountId, ClassicLandlordDismissGameReq req)
        {
            ClassicLandlordDismissGameRsp rsp = new ClassicLandlordDismissGameRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissGameReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissGameReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissGameReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = ClassicLandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissGameReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            if (room.dismissInfo.bStart)
            {
                rsp.errorCode = ClassicLandlordDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissGameReq],上次解散流程未走完,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            // 重新开始解散流程
            room.dismissInfo.Clear();
            room.dismissInfo.bStart = true;
            room.dismissInfo.expireTime = env.Timer.CurrentDateTime.AddMinutes(3);
            room.dismissInfo.firstRoleId = accountId;
            room.dismissInfo.setAgreeRoles.Add(accountId);

            rsp.errorCode = ClassicLandlordDismissGameRsp.ErrorID.Success;
            rsp.remainSecond = room.dismissRemainSecond;
            player.SendMsgToClient(rsp);

            ClassicLandlordDismissGameNtf ntf = new ClassicLandlordDismissGameNtf();
            ntf.roleid = role.roleid;
            ntf.remainSecond = room.dismissRemainSecond;
            room.SendRoomMsgEx(role.roleid, ntf);

            room.SaveDb();
        }

         /// <summary>
        /// 解散是否同意
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordDismissAgreeReq(UInt32 from, UInt64 accountId, ClassicLandlordDismissAgreeReq req)
        {
            ClassicLandlordDismissAgreeRsp rsp = new ClassicLandlordDismissAgreeRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissAgreeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissAgreeReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissAgreeReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = ClassicLandlordDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordDismissAgreeReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = ClassicLandlordDismissAgreeRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 解散流程已结束
            if (room.dismissInfo.firstRoleId == 0)
            {
                return;
            }

            ClassicLandlordDismissAgreeNtf ntf = new ClassicLandlordDismissAgreeNtf();
            ntf.roleid = accountId;
            ntf.bAgree = req.bAgree;
            room.SendRoomMsgEx(accountId, ntf);

            if(req.bAgree)
            {
                room.dismissInfo.setAgreeRoles.Add(accountId);
            }
            else
            {
                room.dismissInfo.setNotRoles.Add(accountId);
            }

            // 解散房间成功
            if (room.dismissInfo.setAgreeRoles.Count >= (ClassicLordRoom.MAX_ROLES_COUNT))
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
                return;
            }

            // 解散房间失败
            if (room.dismissInfo.setNotRoles.Count >= 1)
            {
                ClassicLandlordDismissGameSuccessNtf successNtf = new ClassicLandlordDismissGameSuccessNtf();
                successNtf.bSuccess = false;
                room.SendRoomMsg(successNtf);

                room.dismissInfo.Clear();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordLeaveRoomReq(UInt32 from, UInt64 accountId, ClassicLandlordLeaveRoomReq req)
        {
            ClassicLandlordLeaveRoomRsp rsp = new ClassicLandlordLeaveRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordLeaveRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordLeaveRoomReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            ClassicLordRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = ClassicLandlordLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordLeaveRoomReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = ClassicLandlordLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = ClassicLandlordLeaveRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            ClassicLandlordLeaveRoomNtf ntf = new ClassicLandlordLeaveRoomNtf();
            ntf.roleid = accountId;

            // 不是房主
            if(accountId != room.roomOwner.roleid)
            {
                room.SendRoomMsgEx(accountId, ntf);

                // 解绑战斗服
                player.UnBindBattleFromGate();

                // 通知lobby离开房间
                ClassicLandlordLeaveRoomB2L ntfLobby = new ClassicLandlordLeaveRoomB2L();
                ntfLobby.roomUuid = room.roomUuid;
                player.SendMsgToLobby(ntfLobby);

                g.playerMgr.RemovePlayer(role.roleid);

                room.RemoveRole(accountId);

                room.SaveDb();

                return;
            }

            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }

        /// <summary>
        /// 聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordChatReq(UInt32 from, UInt64 accountId, ClassicLandlordChatReq req)
        {
            ClassicLandlordChatRsp rsp = new ClassicLandlordChatRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnClassicLandlordChatReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = ClassicLandlordChatRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnClassicLandlordChatReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = ClassicLandlordChatRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            ClassicLandlordChatNtf ntf = new ClassicLandlordChatNtf();
            ntf.roleid = accountId;
            ntf.chatid = req.chatid;
            room.SendRoomMsgEx(accountId, ntf);
        }
        /// <summary>
        /// 用户输入文字聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnClassicLandlordChatWordReq(UInt32 from, UInt64 accountId, ClassicLandlordChatWordReq req)
        {
            ClassicLandlordChatWordRsp rsp = new ClassicLandlordChatWordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordChatWordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            ClassicLordRoom room = g.classicLordRoomMgr.GetRoom(player.RoomUuid);
            if (room == null)
            {
                rsp.errorCode = ClassicLandlordChatWordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[ClassicLandlordModule][OnClassicLandlordChatWordReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = ClassicLandlordChatWordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            ClassicLandlordChatWordNtf ntf = new ClassicLandlordChatWordNtf();
            ntf.roleId = accountId;
            ntf.chatWord = req.chatWord;
            room.SendRoomMsgEx(accountId, ntf);
        }
    }
}
