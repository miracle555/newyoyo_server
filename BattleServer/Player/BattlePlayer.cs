﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ServerLib.Bus;
using ServerLib;

namespace BattleServer
{
    public class BattlePlayer
    {
        public BattlePlayer()
        {
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 账号ID
        /// </summary>
        public UInt64 AccountId { get; set; }

        /// <summary>
        /// sessionid
        /// </summary>
        public UInt32 SessionId { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 房间Uuid
        /// </summary>
        public UInt64 RoomUuid { get; set; }

        /// <summary>
        /// 网关地址
        /// </summary>
        public UInt32 GateAddr { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public ERoleSex sex { get; set; }

        /// <summary>
        /// 大厅地址
        /// </summary>
        public UInt32 LobbyAddr { get; set; }

        /// <summary>
        /// 等级
        /// </summary>
        public UInt32 level { get; set; }

        public string clientIp = "";

        /// <summary>
        /// 微信头像url
        /// </summary>
        public string weixinHeadImgUrl = "";

        /// <summary>
        /// 是否在线
        /// </summary>
        public bool isOnline = false;

        //////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// 发送到客户端
        /// </summary>
        /// <param name="proto"></param>
        /// <returns></returns>
        public void SendMsgToClient(Protocol proto)
        {
            proto.SessionId = 0;
            proto.RoleId = AccountId;

            if (GateAddr != 0)
            {
                env.BusService.SendMessageToServer(GateAddr, proto);
            }
        }

        /// <summary>
        /// 发送消息到网关
        /// </summary>
        /// <param name="proto"></param>
        /// <returns></returns>
        public void SendMsgToGate(Protocol proto)
        {
            proto.SessionId = 0;
            proto.RoleId = AccountId;

            if (GateAddr != 0)
            {
               env.BusService.SendMessageToServer(GateAddr, proto);
            }
        }

        public void SendMsgToLobby(Protocol proto)
        {
            proto.SessionId = 0;
            proto.RoleId = AccountId;

            if (LobbyAddr != 0)
            {
                env.BusService.SendMessageToServer(LobbyAddr, proto);
            }
        }

        public void BindBattleToGate()
        {
            // 绑定战斗服
            BattleServerBindReq bindReq = new BattleServerBindReq();
            SendMsgToGate(bindReq);
        }

        public void UnBindBattleFromGate()
        {
            // 解绑战斗服
            BattleServerUnbindReq unBindReq = new BattleServerUnbindReq();
            SendMsgToGate(unBindReq);
        }

        public void Online()
        {
            isOnline = true;

            BindBattleToGate();

            g.huRoomMgr.OnRoleOnline(this);
            g.pointRoomMgr.OnRoleOnline(this);
            g.lordRoomMgr.OnRoleOnline(this);
            g.goldenflowerRoomMgr.OnRoleOnline(this);
        }

        public void Offline()
        {
            UnBindBattleFromGate();

            isOnline = false;
            GateAddr = 0;

            g.playerMgr.RemovePlayer(AccountId);

            g.huRoomMgr.OnRoleOffline(this);
            g.pointRoomMgr.OnRoleOffline(this);
            g.lordRoomMgr.OnRoleOffline(this);
            g.goldenflowerRoomMgr.OnRoleOffline(this);
        }
    }
}
