﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Module;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using ProtoBuf;

namespace BattleServer
{
    internal class BattlePlayerMgr
    {
        public BattlePlayerMgr()
        {

        }

        /// <summary>
        /// 通过账号ID查找玩家
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public BattlePlayer FindPlayer(UInt64 accountId)
        {
            if (!mapPlayers.ContainsKey(accountId))
            {
                return null;
            }

            return mapPlayers[accountId];
        }

        public bool hasPlayer(UInt64 accountId)
        {
            return FindPlayer(accountId) != null;
        }

        public bool AddPlayer(BattlePlayer player)
        {
            if (hasPlayer(player.AccountId))
            {
                return false;
            }

            mapPlayers.Add(player.AccountId, player);
            return true;
        }

        public void RemovePlayer(UInt64 accountId)
        {
            mapPlayers.Remove(accountId);

            return;
        }

        public bool IsOnline(UInt64 accountId)
        {
            BattlePlayer player = FindPlayer(accountId);
            if(null == player)
            {
                return false;
            }

            return player.isOnline;
        }

        //////////////////////////////////////////////////////////////////////////
        // Data Member
        private Dictionary<UInt64, BattlePlayer> mapPlayers = new Dictionary<UInt64, BattlePlayer>();
    }
}
