﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using BattleServer;
using MahjongPushDown;
using MahjongPoint;
using Landlord;
using GoldenFlower;

namespace Module
{
    public class BattleModule : LogicModule
    {
        public BattleModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            // 玩家下线
            RegisterMsgHandler<PlayerOfflineNtf>(MSGID.PlayerOfflineNtf, OnPlayerOffline);

            //创建房间
            RegisterMsgHandler(MSGID.CreatePlayerOnBattleL2B, new MsgHandler<CreatePlayerOnBattleL2B>(OnCreatePlayerOnBattleL2B));

            // 清理房间数据
            RegisterMsgHandler(MSGID.GmClearRoomL2B, new MsgHandler<GmClearRoomL2B>(OnGmClearRoomL2B));

            return true;
        }

        DateTime lastTime = env.Timer.CurrentDateTime;
        protected override void OnTick()
        {
            base.OnTick();

            TimeSpan span = env.Timer.CurrentDateTime - lastTime;
            if(span.TotalSeconds >= 1.0f)
            {
                g.huRoomMgr.OnSecond();
                g.lordRoomMgr.OnSecond();
                g.pointRoomMgr.OnSecond();
                g.goldenflowerRoomMgr.OnSecond();
                g.yunChengRoomMgr.OnSecond();
            }
        }

        /// <summary>
        /// 玩家下线
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnPlayerOffline(PlayerOfflineNtf req, UInt32 from, stServerBody serverBody)
        {
            BattlePlayer player = g.playerMgr.FindPlayer(serverBody.accountId);
            if (null == player)
            {
                return;
            }
            player.Offline();
        }

        void OnCreatePlayerOnBattleL2B(UInt32 from, UInt64 accountId, CreatePlayerOnBattleL2B req)
        {
            BattlePlayer player = new BattlePlayer();
            player.AccountId = req.roleInfo.roleid;
            player.NickName = req.roleInfo.nickName;
            player.GateAddr = req.roleInfo.gateAddr;
            player.LobbyAddr = from;
            player.sex = req.roleInfo.sex;
            player.SessionId = req.roleInfo.sessionId;
            player.clientIp = req.roleInfo.clientIp;
            player.weixinHeadImgUrl = req.roleInfo.weixinHeadImgUrl;
            player.RoomUuid = req.roleInfo.roomUuid;
            player.level = req.roleInfo.level;

            g.playerMgr.RemovePlayer(req.roleInfo.roleid);
            g.playerMgr.AddPlayer(player);

            player.Online();
        }

        /// <summary>
        /// 清理房间数据
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnGmClearRoomL2B(UInt32 from, UInt64 accountId, GmClearRoomL2B req)
        {
            HuRoom room1 = await g.huRoomMgr.FindRoomAsync(req.roomUuid);
            if (null != room1)
            {
                room1.RoomClose();
            }

            PointRoom room2 = await g.pointRoomMgr.FindRoomAsync(req.roomUuid);
            if (null != room2)
            {
                room2.RoomClose();
            }

            LordRoom room3 = await g.lordRoomMgr.FindRoomAsync(req.roomUuid);
            if (null != room3)
            {
                room3.RoomClose();
            }
            GoldenFlowerRoom room4 = await g.goldenflowerRoomMgr.FindRoomAsync(req.roomUuid);
            if (null != room4)
            {
                room4.RoomClose();
            }

        }
    }
}
