﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using ServerLib;
using Config;
using Protocols;
using Module;
using Script;
using StaticData;
using Event;
using MahjongPushDown;
using MahjongPoint;
using Landlord;

using GoldenFlower;

using MahjongYunCheng;


namespace BattleServer
{
    class Battleenv
    {
        public static bool Start(ServerConfig config)
        {
            env.Init(config);
            env.ModuleManager.RegisterModule(new BattleModule());
            env.ModuleManager.RegisterModule(new MJPushDownModule());
            env.ModuleManager.RegisterModule(new MJPointModule());
            env.ModuleManager.RegisterModule(new LandlordModule());
            env.ModuleManager.RegisterModule(new GoldenFlowerModule());
            env.ModuleManager.RegisterModule(new MJYunChengModule());


            return env.Start();
        }
    }

    class g
    {
        /// <summary>
        /// 推到胡房间管理器
        /// </summary>
        public static HuRoomMgr huRoomMgr = new HuRoomMgr();

        /// <summary>
        /// 抠点房间管理器
        /// </summary>
        public static PointRoomMgr pointRoomMgr = new PointRoomMgr();

        /// <summary>
        /// 斗地主房间管理器
        /// </summary>
        public static LordRoomMgr lordRoomMgr = new LordRoomMgr();

        /// <summary>
        /// 扎金花房间管理器
        /// </summary>
        public static GoldenFlowerRoomMgr goldenflowerRoomMgr = new GoldenFlowerRoomMgr();

        /// <summary>
        /// 玩家管理器
        /// </summary>
        public static BattlePlayerMgr playerMgr = new BattlePlayerMgr();

        /// <summary>
        /// 运城贴金房间管理器
        /// </summary>
        public static YunChengRoomMgr yunChengRoomMgr = new YunChengRoomMgr();
    }
}
