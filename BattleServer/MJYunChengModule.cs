﻿using BattleServer;
using HelpAsset;
using HelpAsset.MahjongYunCheng;
using Log;
using MahjongYunCheng;
using Protocols;
using Protocols.Majiang;
using Protocols.MajiangYunCheng;
using ServerLib;
using ServerLib.Bus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Module
{
    class MJYunChengModule: LogicModule
    {
        public MJYunChengModule()
            :base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            //创建房间
            RegisterMsgHandler(MSGID.MJYunChengCreateRoomL2B, new MsgHandler<MJYunChengCreateRoomL2B>(OnMJYunChengCreateRoomL2B));

            //加入房间
            RegisterMsgHandler(MSGID.MJYunChengJoinRoomL2B,new MsgHandler<MJYunChengJoinRoomL2B>(OnMJYunChengJoinRoomL2B));

            //获取房间信息
            RegisterMsgHandler(MSGID.MJYunChengGetRoomInfoL2B,new MsgHandler<MJYunChengGetRoomInfoL2B>(OnMJYunChengGetRoomInfoL2B));

            //出牌请求
            RegisterMsgHandler(MSGID.MJYunChengHandOutCardReq, new MsgHandler<MJYunChengHandOutCardReq>(OnMJYunChengHandOutCardReq));
            
            //碰牌请求
            RegisterMsgHandler(MSGID.MJYunChengPengCardReq, new MsgHandler<MJYunChengPengCardReq>(OnMJYunChengPengCardReq));

            //胡牌请求
            RegisterMsgHandler(MSGID.MJYunChengHuCardReq, new MsgHandler<MJYunChengHuCardReq>(OnMJYunChengHuCardReq));

            //杠牌请求
            RegisterMsgHandler(MSGID.MJYunChengGangCardReq,new MsgHandler<MJYunChengGangCardReq>(OnMJYunChengGangCardReq));

            //听牌请求
            RegisterMsgHandler(MSGID.MJYunChengReadyCardReq, new MsgHandler<MJYunChengReadyCardReq>(OnMJYunChengReadyCardReq));

            //过
            RegisterMsgHandler(MSGID.MJYunChengPassReq, new MsgHandler<MJYunChengPassReq>(OnMJYunChengPassReq));

            // 下一步
            RegisterMsgHandler(MSGID.MJYunChengNextStepReq, new MsgHandler<MJYunChengNextStepReq>(OnMJYunChengNextStepReq));

            // (抠点麻将)解散游戏
            RegisterMsgHandler(MSGID.MJYunChengDismissGameReq, new MsgHandler<MJYunChengDismissGameReq>(OnMJYunChengDismissGameReq));

            // 解散是否同意
            RegisterMsgHandler(MSGID.MJYunChengDismissAgreeReq, new MsgHandler<MJYunChengDismissAgreeReq>(OnMJYunChengDismissAgreeReq));

            // 离开房间，房间未开始时
            RegisterMsgHandler(MSGID.MJYunChengLeaveRoomReq, new MsgHandler<MJYunChengLeaveRoomReq>(OnMJYunChengLeaveRoomReq));

            // 聊天
            RegisterMsgHandler(MSGID.MJYunChengChatReq, new MsgHandler<MJYunChengChatReq>(OnMJYunChengChatReq));

            //文字聊天
            RegisterMsgHandler(MSGID.MJYunChengChatWordReq, new MsgHandler<MJYunChengChatWordReq>(OnMJYunChengChatWordReq));

            //扔鸡蛋
            RegisterMsgHandler(MSGID.MJYunChengEggReq, new MsgHandler<MJYunChengEggReq>(OnMJYunChengEggReq));

            //房主建房
            RegisterMsgHandler(MSGID.MJYunChengOtherCloseRoomL2B, new MsgHandler<MJYunChengOtherCloseRoomL2B>(OnMJYunChengOtherCloseRoomL2B));

            return true;
        }

        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengCreateRoomL2B(UInt32 from,UInt64 accountId,MJYunChengCreateRoomL2B req)
        {
            MJYunChengCreateRoomRsp rsp = new MJYunChengCreateRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if(null == player)
            {
                player = new BattlePlayer();
                player.AccountId = accountId;
                //LogSys.Warn("[MJYunChengModule][OnMJYunChengCreateRoomL2B],找不到玩家,accountId={0}", accountId);
                //return;
            }

            UInt64 roomuuid = req.roomUuid;
            YunChengRoom room = new YunChengRoom(roomuuid);
            room.config = req.config;
            if (req.config.payMethod != PayMethod.anotherPay)
            {
                room.InitRoom(accountId, player.NickName, EGameType.MJYunCheng);
                player.RoomUuid = roomuuid;
                AddRoleToRoom(player,room);
            }
            else
            {
                room.InitRoom2(accountId, player.NickName, EGameType.MJYunCheng);
            }
            g.yunChengRoomMgr.AddRoom(room);

            rsp.errorCode = MJYunChengCreateRoomRsp.ErrorID.Success;
            if(req.config.payMethod != PayMethod.anotherPay)
            {
                rsp.roomInfo = room.GetRoomInfo(accountId);
            }
            player.SendMsgToClient(rsp);

            room.SaveDb(true);
        }

        void AddRoleToRoom(BattlePlayer player, YunChengRoom room)
        {
            YunChengRoomRole role = new YunChengRoomRole();
            role.Init(player.AccountId, player.NickName, room);
            role.direction = room.GetOneSeat();
            role.sex = player.sex;
            role.level = player.level;
            //设置玩家状态为等待其他人
            role.roleState = ERoomRoleState.WaitRoomer;
            role.weixinHeadImgUrl = player.weixinHeadImgUrl;
            //将role添加到room的 role列表中
            room.AddRole(role);
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJYunChengJoinRoomL2B(UInt32 from,UInt64 accountId,MJYunChengJoinRoomL2B req)
        {
            MJYunChengJoinRoomRsp rsp = new MJYunChengJoinRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            //如果查找不到用户
            if(null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomL2B],找不到用户,accountId={0}",accountId);
                return;
            }

            //如果房间不存在
            YunChengRoom room = await g.yunChengRoomMgr.FindRoomAsync(req.roomUuid);
            if(null == room)
            {
                rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomL2B],房间不存在,accountId={0}",accountId);
                return;
            }

            //如果房间已满
            if(room.IsRoomFull())
            {
                rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengJoinRoomL2B],房间已满,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;
            AddRoleToRoom(player,room);

            rsp.errorCode = MJYunChengJoinRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            //通知其他玩家
            MJYunChengJoinRoomNtf ntf = new MJYunChengJoinRoomNtf();
            ntf.remoteRoleInfo = room.GetRole(accountId).remoteInfo;
            room.SendRoomMsgEx(accountId,ntf);

            //如果房间人满 开始游戏
            if(room.IsRoomFull()){
                room.FirstInit();
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJYunChengGetRoomInfoL2B(UInt32 from,UInt64 accountId, MJYunChengGetRoomInfoL2B req)
        {
            MJYunChengGetRoomInfoRsp rsp = new MJYunChengGetRoomInfoRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if(null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengGetRoomInfoL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;

            YunChengRoom room = await g.yunChengRoomMgr.FindRoomAsync(player.RoomUuid);

            if (null == room)
            {
                rsp.errorCode = MJYunChengGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengGetRoomInfoL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengGetRoomInfoL2B],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = MJYunChengGetRoomInfoRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.CheckDismissTimer();
        }

        /// <summary>
        /// 碰牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengPengCardReq(UInt32 from,UInt64 accountId, MJYunChengPengCardReq req)
        {
            MJYunChengPengCardRsp rsp = new MJYunChengPengCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);

            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengPengCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPengCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPengCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            // 不是碰牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJYunChengPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPengCardReq],不是碰牌状态,accountId={0}", accountId);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if(!readyItem.readyInfo.bPeng)
            {
                rsp.errorCode = MJYunChengPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPengCardReq],玩家不能碰，却发碰牌消息,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole handRole = room.GetRole(readyItem.readyInfo.cardRoleId);
            if(null == handRole)
            {
                rsp.errorCode = MJYunChengPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPengCardReq],出牌人不在房间中,accountId={0}", accountId);
            }

            if (!role.CanPeng(readyItem.readyInfo.cardid))
            {
                rsp.errorCode = MJYunChengPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPengCardReq],不能碰,accountId={0}, lastHandOutCardRole={1}", accountId, readyItem.readyInfo.cardRoleId);
                return;
            }

            // 从出牌人池牌中删除
            handRole.currentRound.RemovePoolCard(readyItem.readyInfo.cardid);

            // 把牌添加到碰牌者的明牌中
            EMJCardType cardType = MJCardHelp.GetMJType(readyItem.readyInfo.cardid);
            List<UInt32> lstPengCards = role.GetPengCards(cardType);
            role.currentRound.RemoveHandCards(lstPengCards);
            role.AddPengCards(readyItem.readyInfo.cardid, lstPengCards);

            rsp.errorCode = MJYunChengPengCardRsp.ErrorID.Success;
            rsp.cardRoleId = readyItem.readyInfo.cardRoleId;
            rsp.cardid = readyItem.readyInfo.cardid;
            rsp.lstPengCards = lstPengCards;
            player.SendMsgToClient(rsp);

            // 通知其他人碰牌
            MJYunChengPengCardNtf ntf = new MJYunChengPengCardNtf();
            ntf.cardRoleId = readyItem.readyInfo.cardRoleId;
            ntf.cardid = readyItem.readyInfo.cardid;
            ntf.pengRoleId = accountId;
            ntf.lstCards = lstPengCards;
            room.SendRoomMsgEx(accountId, ntf);

            role.currentRound.playState = ERolePlayState.Wait;

            // 碰牌也算起新牌,需要客户端检测听牌暗杠
            role.CheckSelfGangHuReady(false,true);

            room.SaveDb();
        }

        /// <summary>
        /// 出牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengHandOutCardReq(UInt32 from,UInt64 accountId, MJYunChengHandOutCardReq req)
        {
            MJYunChengHandOutCardRsp rsp = new MJYunChengHandOutCardRsp();

            //如果找不到玩家
            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if(null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengHandOutCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            //如果房间不存在
            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if(null == room)
            {
                rsp.errorCode = MJYunChengHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHandOutCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            //如果玩家不再房间里
            if(null == role)
            {
                rsp.errorCode = MJYunChengHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHandOutCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            //如果手牌中没有该牌
            if (!role.currentRound.HasHandCard(req.cardid))
            {
                rsp.errorCode = MJYunChengHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHandOutCardReq],手牌中没有该牌,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }

            // 不是出牌状态
            if (!role.IsHandingOutState)
            {
                rsp.errorCode = MJYunChengHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHandOutCardReq],不是出牌状态,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }

            stCheckRoomItem roomitem = new stCheckRoomItem();
            stCheckRoleItem roleitem = new stCheckRoleItem();
            role.GetCheckRoomRoleInfo(roomitem, roleitem);

            //听口改变
            if(YunChengHelp.CheckReadyChange(req.cardid,roomitem,roleitem))
            {
                rsp.errorCode = MJYunChengHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHandOutCardReq],听口改变,accountId={0}", accountId);
                return;
            }
            //获取出牌的类型
            EMJCardType cardType = MJCardHelp.GetMJType(req.cardid);
            bool gold = false;
            if (room.goldenType.Contains(cardType))
            {
                gold = true;
            }
            else
            {
                //如果不是金牌先设置这张牌的类型为0
                cardType = EMJCardType.Null;
            }

            HandleHandOutCard(accountId, room, role, req.cardid,cardType);

            rsp.errorCode = MJYunChengHandOutCardRsp.ErrorID.Success;
            rsp.isGolden = gold;

            player.SendMsgToClient(rsp);
            room.SaveDb();
        }

        /// <summary>
        /// 出牌
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="room"></param>
        /// <param name="role"></param>
        /// <param name="cardid"></param>
        void HandleHandOutCard(UInt64 accountId,YunChengRoom room, YunChengRoomRole role,UInt32 cardid,EMJCardType cardType, bool bReadyOut = false)
        {
            //过胡后,只能自摸
            //role.currentRound.bPassHu = false;
            role.bRobGangHu = false;

            // 记录出牌信息
            room.currentRound.lastHandOutCard.Clear();
            room.currentRound.lastHandOutCard.RecordHandOutCard(accountId, cardid);

            // 从手牌中删除该牌
            role.currentRound.RemoveHandCard(cardid);
            bool bIsGolden = false;
            if (cardType != 0)
            {
                //如果是金牌
                //将金牌加入上金列表 从手牌中抹去
                role.currentRound.putGoldenList.Add(cardid);
                role.currentRound.handGoldenList.Remove(cardid);
                bIsGolden = true;
            }
            else
            {
                role.currentRound.AddPoolCard(cardid);
            }

            // 通知其他人自己出牌
            MJYunChengHandOutCardNtf ntf = new MJYunChengHandOutCardNtf();
            ntf.cardRoleId = accountId;
            ntf.cardid = cardid;
            ntf.bReadyOut = bReadyOut;
            ntf.bIsGolden = bIsGolden;
            room.SendRoomMsgEx(accountId, ntf);

            role.currentRound.playState = ERolePlayState.Wait;

            // 记录听牌时出的这张牌
            if (bReadyOut)
            {
               
                role.currentRound.readyOutCardId = cardid;

                // 下一个人起牌
                role.nextRole.DrawNewCard();

                return;
            }

            // 检测其他人碰杠胡
            List<YunChengRoomRole> lstOthers = room.NextRoles(role);

            // 检测胡
            foreach (var other in lstOthers)
            {
                if (bIsGolden)
                {
                    //如果出的是金牌 不可胡
                    break;
                }
                EHuCardType huType = EHuCardType.Null;
                if (other.CanHuOther(cardid, ref huType))
                {
                    stOutCardReadyItem readyItem = new stOutCardReadyItem();
                    readyItem.roleid = other.roleid;
                    readyItem.huType = huType;
                    readyItem.bSelfTouch = false;

                    readyItem.readyInfo.cardRoleId = accountId;
                    readyItem.readyInfo.cardid = cardid;
                    readyItem.readyInfo.bHuPai = true;
                    room.currentRound.lastHandOutCard.AddReadyItem(readyItem);
                }
            }

            // 检测碰
            foreach (var other in lstOthers)
            {
                if (bIsGolden)
                {
                    //如果这张牌是金牌不许碰
                    //如果有玩家可以胡 其他玩家不许碰
                    break;
                }

                if (other.CanPeng(cardid))
                {
                    stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();
                    readyInfo.bPeng = true;
                    readyInfo.cardRoleId = accountId;
                    readyInfo.cardid = cardid;
                    room.currentRound.lastHandOutCard.AddReadyItem(other.roleid, readyInfo);
                }
            }

            // 检测明杠
            foreach (var other in lstOthers)
            {
                if (bIsGolden)
                {
                    //如果是金牌不许杠
                    break;
                }

                if (other.CanMingGang(cardid))
                {
                    stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();
                    readyInfo.gangType = EGangCardType.MingGang;
                    readyInfo.cardRoleId = accountId;
                    readyInfo.cardid = cardid;
                    room.currentRound.lastHandOutCard.AddReadyItem(other.roleid, readyInfo);
                }
            }
            
            //如果玩家听牌了 并且出金牌玩家当前回合的金牌数量加1
            if(bIsGolden)
            {
                //金牌数+1
                role.currentRound.goldenCount++;
            }

            if (room.currentRound.lastHandOutCard.HasReadyItem())
            {
                stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                YunChengRoomRole readyRole = room.GetRole(item.roleid);
                readyRole.NotifyPengGangHuReady(item, bIsGolden);
            }
            else
            {
                role.nextRole.DrawNewCard();
            }
        }

        string ToString(List<UInt32> lstCards)
        {
            string temp = "";
            foreach (var cardid in lstCards)
            {
                temp += cardid.ToString();
                temp += ",";
            }
            return temp;
        }

        /// <summary>
        /// 胡牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengHuCardReq(UInt32 from,UInt64 accountId,MJYunChengHuCardReq req)
        {
            MJYunChengHuCardRsp rsp = new MJYunChengHuCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengHuCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHuCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHuCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            // 不是胡牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJYunChengHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHuCardReq],不是胡牌状态,accountId={0}", accountId);
                return;
            }

            // 未胡牌
            if (!role.currentRound.readyItem.readyInfo.bHuPai)
            {
                rsp.errorCode = MJYunChengHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengHuCardReq],玩家未胡牌,accountId={0}", accountId);
                return;
            }

            room.currentRound.huRoleId = accountId;

            stOutCardReadyItem readyItem = role.currentRound.readyItem;

            // 自摸胡
            if (readyItem.bSelfTouch)
            {
                rsp.errorCode = MJYunChengHuCardRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                MJYunChengHuCardNtf ntf = new MJYunChengHuCardNtf();
                ntf.bSelfTouch = true;
                ntf.huRoleId = accountId;
                room.SendRoomMsgEx(accountId, ntf);

                room.currentRound.RecordSettleItem(readyItem.huType, true, role, 0, role.currentRound.lastNewCardId, false);
            }
            else
            {
                if (role.bRobGangHu)
                {
                    YunChengRoomRole otherRole = room.GetRole(role.currentRound.readyItem.readyInfo.cardRoleId);
                    if (otherRole == null)
                    {
                        rsp.errorCode = MJYunChengHuCardRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[MJYunChengModule][OnMJYunChengHuCardReq],被抢杠胡玩家不在房间中,accountId={0}", accountId);
                        return;
                    }
                    RemoveCardInShowCard(otherRole, role.currentRound.readyItem.readyInfo.cardid);
                }

                rsp.errorCode = MJYunChengHuCardRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                MJYunChengHuCardNtf ntf = new MJYunChengHuCardNtf();
                ntf.bSelfTouch = false;
                ntf.cardRoleId = readyItem.readyInfo.cardRoleId;
                ntf.huRoleId = accountId;
                room.SendRoomMsgEx(accountId, ntf);

                bool bOtherBaoTing = room.GetRole(readyItem.readyInfo.cardRoleId).currentRound.AlreadyBaoTing;
                room.currentRound.RecordSettleItem(readyItem.huType, false, role, readyItem.readyInfo.cardRoleId, readyItem.readyInfo.cardid, bOtherBaoTing);
            }

            // 玩家赢了一局
            MJYunChengWinOneRoundB2L winNtf = new MJYunChengWinOneRoundB2L();
            player.SendMsgToLobby(winNtf);

            room.Settle();

            room.SaveDb();
        }

        void RemoveCardInShowCard(YunChengRoomRole role, uint cardId)
        {
            for (int i = 0; i < role.currentRound.showCards.Count; i++)
            {
                if (role.currentRound.showCards[i].mingType == EMingCardType.MingGang)
                {
                    if (role.currentRound.showCards[i].showCards.Contains(cardId))
                    {
                        role.currentRound.showCards[i].showCards.Remove(cardId);
                        role.currentRound.showCards[i].mingType = EMingCardType.Peng;
                    }
                }
            }
        }

        /// <summary>
        /// 杠牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengGangCardReq(UInt32 from,UInt64 accountId, MJYunChengGangCardReq req)
        {
            MJYunChengGangCardRsp rsp = new MJYunChengGangCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengGangCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengGangCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengGangCardReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是杠牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengGangCardReq],不是杠牌状态,accountId={0}", accountId);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if (readyItem.readyInfo.gangType == EGangCardType.Null)
            {
                rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengGangCardReq],玩家不能杠牌,accountId={0}", accountId);
                return;
            }

            //报听后不可杠
            //if(role.currentRound.AlreadyBaoTing)
            //{
                //rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Failed;
                //player.SendMsgToClient(rsp);

                //LogSys.Warn("[MJYunChengModule][OnMJYunChengGangCardReq]，报听后不能杠牌,accountId={0}", accountId);
                //return;
            //}

            // 暗杠
            if (readyItem.readyInfo.gangType == EGangCardType.AnGang)
            {
                HandleAnGang(room, role, readyItem.readyInfo.cardid);
            }
            else if (readyItem.readyInfo.gangType == EGangCardType.BuGang)
            {
                HandleBuGang(room, role, readyItem.readyInfo.cardid);
            }
            else if (readyItem.readyInfo.gangType == EGangCardType.MingGang)
            {
                HandleMingGang(room, role);
            }

            role.currentRound.playState = ERolePlayState.Wait;

            if (room.currentRound.lastHandOutCard.HasReadyItem())
            {
                stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                YunChengRoomRole readyRole = room.GetRole(item.roleid);
                readyRole.NotifyPengGangHuReady(item,false);
            }
            else
            {
                // 通知当前玩家起牌
                role.DrawNewCard();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 处理明杠
        /// </summary>
        /// <param name="room"></param>
        /// <param name="cardRole"></param>
        /// <param name="self"></param>
        void HandleMingGang(YunChengRoom room, YunChengRoomRole self)
        {
            UInt32 cardid = room.currentRound.lastHandOutCard.cardid;
            UInt64 cardRoleId = room.currentRound.lastHandOutCard.roleid;
            YunChengRoomRole handRole = room.GetRole(cardRoleId);

            EMJCardType type = MJCardHelp.GetMJType(cardid);
            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);
            handRole.currentRound.RemovePoolCard(cardid);

            self.AddMingGangCards(cardid, lstCards);

            MJYunChengGangCardRsp rsp = new MJYunChengGangCardRsp();
            rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.MingGang;
            rsp.cardRoleId = cardRoleId;
            rsp.cardid = cardid;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJYunChengGangCardNtf ntf = new MJYunChengGangCardNtf();
            ntf.gangType = EGangCardType.MingGang;
            ntf.loseRoleId = cardRoleId;
            ntf.winRoleId = self.roleid;
            ntf.cardid = cardid;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 记录结算条目
            room.currentRound.RecordSettleItem(EGangCardType.MingGang, false, self.roleid, cardRoleId, cardid, handRole.currentRound.AlreadyBaoTing);
        }

        /// <summary>
        /// 处理暗杠
        /// </summary>
        /// <param name="room"></param>
        /// <param name="self"></param>
        /// <param name="cardid"></param>
        void HandleAnGang(YunChengRoom room, YunChengRoomRole self, UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);

            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);

            self.AddAnGangCards(lstCards);

            MJYunChengGangCardRsp rsp = new MJYunChengGangCardRsp();
            rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.AnGang;
            rsp.cardRoleId = 0;
            rsp.cardid = 0;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJYunChengGangCardNtf ntf = new MJYunChengGangCardNtf();
            ntf.gangType = EGangCardType.AnGang;
            ntf.loseRoleId = 0;
            ntf.winRoleId = self.roleid;
            ntf.cardid = 0;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 记录结算条目
            room.currentRound.RecordSettleItem(EGangCardType.AnGang, true, self.roleid, 0, cardid, false);
        }

        /// <summary>
        /// 处理补杠
        /// </summary>
        void HandleBuGang(YunChengRoom room, YunChengRoomRole self, UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);
            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);

            self.AddBuGangCards(lstCards);

            MJYunChengGangCardRsp rsp = new MJYunChengGangCardRsp();
            rsp.errorCode = MJYunChengGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.BuGang;
            rsp.cardRoleId = 0;
            rsp.cardid = 0;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJYunChengGangCardNtf ntf = new MJYunChengGangCardNtf();
            ntf.gangType = EGangCardType.BuGang;
            ntf.loseRoleId = 0;
            ntf.winRoleId = self.roleid;
            ntf.cardid = 0;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 检测其他人胡
            List<YunChengRoomRole> lstOthers = room.NextRoles(self);

            //是否有人可接炮
            bool takeAGun = false;
            //假设索引为0的用户的金数最多
            List<int> indexs = new List<int>();
            indexs.Add(0);

            UInt32 goldenCount = lstOthers[0].currentRound.goldenCount;
            //遍历所有用户
            for (int i = 1; i < lstOthers.Count; i++)
            {
                if (!lstOthers[i].currentRound.AlreadyBaoTing)
                {
                    continue;
                }
                //如果金数大于上一个用户的金数
                if (lstOthers[i].currentRound.goldenCount > goldenCount)
                {
                    indexs = new List<int>();
                    indexs.Add(i);
                    goldenCount = lstOthers[i].currentRound.goldenCount;
                }
                else if (lstOthers[i].currentRound.goldenCount == goldenCount)
                {
                    indexs.Add(i);
                }
            }

            if (goldenCount > 0)
            {
                takeAGun = true;
            }

            // 检测胡
            foreach (var other in lstOthers)
            {
                if (!takeAGun)
                {
                    break;
                }

                if(indexs.Contains(lstOthers.IndexOf(other)))
                {
                    continue;
                }

                stOutCardReadyItem readyItem = new stOutCardReadyItem();
                EHuCardType huType = EHuCardType.Null;
                other.bRobGangHu = true;
                if (other.CanHuOther(cardid, ref huType))
                {
                    readyItem.roleid = other.roleid;
                    readyItem.huType = huType;
                    readyItem.bSelfTouch = false;

                    readyItem.readyInfo.cardRoleId = self.roleid;
                    readyItem.readyInfo.cardid = cardid;
                    readyItem.readyInfo.bHuPai = true;
                    room.currentRound.lastHandOutCard.AddReadyItem(readyItem);
                }
            }

            if (!room.currentRound.lastHandOutCard.HasReadyItem())
            {
                // 记录结算条目
                room.currentRound.RecordSettleItem(EGangCardType.BuGang, true, self.roleid, 0, cardid, false);
            }
        }

        /// <summary>
        /// 听牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengReadyCardReq(UInt32 from,UInt64 accountId, MJYunChengReadyCardReq req)
        {
            MJYunChengReadyCardRsp rsp = new MJYunChengReadyCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是听牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJYunChengReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],不是听牌状态,accountId={0}", accountId);
                return;
            }

            // 已经报听，不能再报听
            if (role.currentRound.AlreadyBaoTing)
            {
                rsp.errorCode = MJYunChengReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],玩家已经听牌,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if (!readyItem.readyInfo.bReadyCard)
            {
                rsp.errorCode = MJYunChengReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],玩家未听牌,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            if (!readyItem.CanReadyCard(req.cardid))
            {
                rsp.errorCode = MJYunChengReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],出该牌不能听,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            HandleHandOutCard(accountId, room, role, req.cardid,0, true);

            readyItem.GetReadyTypes(req.cardid, role.currentRound.lstReadyTypes);
            if (role.currentRound.lstReadyTypes.Count == 0)
            {
                rsp.errorCode = MJYunChengReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengReadyCardReq],听牌异常错误,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            role.currentRound.playState = ERolePlayState.Wait;

            player.SendMsgToClient(rsp);

            MJYunChengReadyCardNtf ntf = new MJYunChengReadyCardNtf();
            ntf.readyRoleId = accountId;
            room.SendRoomMsgEx(accountId, ntf);

            room.SaveDb();
        }

        /// <summary>
        /// 过
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengPassReq(UInt32 from, UInt64 accountId, MJYunChengPassReq req)
        {
            MJYunChengPassRsp rsp = new MJYunChengPassRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengPassReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPassReq],房间不存在,accountId={0}", accountId);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPassReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是碰杠胡听状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJYunChengPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengPassReq],不是碰杠胡听状态,accountId={0}", accountId);
                return;
            }

            role.currentRound.playState = ERolePlayState.Wait;

            rsp.errorCode = MJYunChengPassRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            stOutCardReadyItem readyItem = role.currentRound.readyItem;

            // 过胡后一圈内不能再胡牌
            if (readyItem.readyInfo.bHuPai)
            {
                if (!readyItem.bSelfTouch)
                {
                    // 过胡设置为true
                    role.currentRound.bPassHu = true;
                }
            }

            if (IsSelfHandCard(role.currentRound.readyItem))
            {
                // 该你出牌了
                role.NotifyNeedHandOutCard();
            }
            else
            {
                if (room.currentRound.lastHandOutCard.HasReadyItem())
                {
                    stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                    YunChengRoomRole readyRole = room.GetRole(item.roleid);
                    readyRole.NotifyPengGangHuReady(item,false);
                }
                else
                {
                    // 出牌人
                    YunChengRoomRole handRole = room.GetRole(room.currentRound.lastHandOutCard.roleid);
                    if (null == handRole)
                    {
                        rsp.errorCode = MJYunChengPassRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[MJYunChengModule][OnMJYunChengPassReq],出牌人不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                        return;
                    }

                    if (role.bRobGangHu)
                    {
                        room.currentRound.RecordSettleItem(EGangCardType.BuGang, true, room.currentRound.lastHandOutCard.roleid, 0, room.currentRound.lastHandOutCard.cardid, false);
                    }

                    // 下个人起牌
                    handRole.nextRole.DrawNewCard();
                }
            }

            room.SaveDb();
        }

        bool IsSelfHandCard(stOutCardReadyItem readyItem)
        {
            stPengGangHuReadyInfo readyInfo = readyItem.readyInfo;

            // 暗杠和补杠
            if (readyInfo.gangType == EGangCardType.AnGang || readyInfo.gangType == EGangCardType.BuGang)
            {
                return true;
            }

            // 自摸
            if (readyInfo.bHuPai)
            {
                if (readyItem.bSelfTouch)
                {
                    return true;
                }
            }

            if (readyInfo.bReadyCard)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 运城贴金 下一步
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengNextStepReq(UInt32 from, UInt64 accountId, MJYunChengNextStepReq req)
        {
            MJYunChengNextStepRsp rsp = new MJYunChengNextStepRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengNextStepReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengNextStepReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            if (room.roomState != ERoomState.WaitRound)
            {
                rsp.errorCode = MJYunChengNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][MJYunChengNextStepRsp],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][MJYunChengNextStepRsp],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (role.roleState != ERoomRoleState.Settle)
            {
                rsp.errorCode = MJYunChengNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][MJYunChengNextStepRsp],玩家状态不对，不在结算状态,accountId={0}", accountId);
                return;
            }

            role.roleState = ERoomRoleState.WaitRound;

            rsp.errorCode = MJYunChengNextStepRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJYunChengNextStepNtf ntf = new MJYunChengNextStepNtf();
            ntf.roleid = accountId;
            room.SendRoomMsgEx(accountId, ntf);

            // 所有玩家都过了结算界面
            if (room.IsAllRoleWaitRound())
            {
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// (运城贴金)解散游戏
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengDismissGameReq(UInt32 from, UInt64 accountId, MJYunChengDismissGameReq req)
        {
            MJYunChengDismissGameRsp rsp = new MJYunChengDismissGameRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissGameReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissGameReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissGameReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = MJYunChengDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissGameReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            if (room.dismissInfo.bStart)
            {
                rsp.errorCode = MJYunChengDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissGameReq],上次解散流程未走完,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            // 重新开始解散流程
            room.dismissInfo.Clear();
            room.dismissInfo.bStart = true;
            room.dismissInfo.expireTime = env.Timer.CurrentDateTime.AddMinutes(2);
            room.dismissInfo.firstRoleId = accountId;
            room.dismissInfo.setAgreeRoles.Add(accountId);

            rsp.errorCode = MJYunChengDismissGameRsp.ErrorID.Success;
            rsp.remainSecond = room.dismissRemainSecond;
            player.SendMsgToClient(rsp);

            MJYunChengDismissGameNtf ntf = new MJYunChengDismissGameNtf();
            ntf.roleid = role.roleid;
            ntf.remainSecond = room.dismissRemainSecond;
            room.SendRoomMsgEx(role.roleid, ntf);

            room.SaveDb();
        }

        /// <summary>
        /// 解散是否同意
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengDismissAgreeReq(UInt32 from, UInt64 accountId, MJYunChengDismissAgreeReq req)
        {
            MJYunChengDismissAgreeRsp rsp = new MJYunChengDismissAgreeRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissAgreeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissAgreeReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissAgreeReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = MJYunChengDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengDismissAgreeReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = MJYunChengDismissAgreeRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 解散流程已结束
            if (room.dismissInfo.firstRoleId == 0)
            {
                return;
            }

            MJYunChengDismissAgreeNtf ntf = new MJYunChengDismissAgreeNtf();
            ntf.roleid = accountId;
            ntf.bAgree = req.bAgree;
            room.SendRoomMsgEx(accountId, ntf);

            if (req.bAgree)
            {
                room.dismissInfo.setAgreeRoles.Add(accountId);
            }
            else
            {
                room.dismissInfo.setNotRoles.Add(accountId);
            }

            // 解散房间成功
            if (room.dismissInfo.setAgreeRoles.Count >= (YunChengRoom.MAX_ROLES_COUNT - 1))
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
                return;
            }

            // 解散房间失败
            if (room.dismissInfo.setNotRoles.Count >= 1)
            {
                MJYunChengDismissGameSuccessNtf successNtf = new MJYunChengDismissGameSuccessNtf();
                successNtf.bSuccess = false;
                room.SendRoomMsg(successNtf);

                room.dismissInfo.Clear();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengLeaveRoomReq(UInt32 from, UInt64 accountId, MJYunChengLeaveRoomReq req)
        {
            MJYunChengLeaveRoomRsp rsp = new MJYunChengLeaveRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengLeaveRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengLeaveRoomReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            YunChengRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJYunChengLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengLeaveRoomReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = MJYunChengLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = MJYunChengLeaveRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJYunChengLeaveRoomNtf ntf = new MJYunChengLeaveRoomNtf();
            ntf.roleid = accountId;

            // 不是房主 或者是房主代开放 离开房间不解散房间
            if (accountId != room.roomOwner.roleid
                || room.config.payMethod == PayMethod.anotherPay)
            {
                //通知房间其他人离开房间
                room.SendRoomMsgEx(accountId, ntf);

                // 解绑战斗服
                player.UnBindBattleFromGate();

                // 通知lobby离开房间
                MJYunChengLeaveRoomB2L ntfLobby = new MJYunChengLeaveRoomB2L();
                ntfLobby.roomUuid = room.roomUuid;
                player.SendMsgToLobby(ntfLobby);

                g.playerMgr.RemovePlayer(role.roleid);

                room.RemoveRole(accountId);

                room.SaveDb();

                return;
            }
            //是房主且 不是房主代开房离开 直接关闭房间
            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }

        /// <summary>
        /// 聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengChatReq(UInt32 from, UInt64 accountId, MJYunChengChatReq req)
        {
            MJYunChengChatRsp rsp = new MJYunChengChatRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengChatReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJYunChengChatRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengChatReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = MJYunChengChatRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJYunChengChatNtf ntf = new MJYunChengChatNtf();
            ntf.roleid = accountId;
            ntf.chatid = req.chatid;
            room.SendRoomMsgEx(accountId, ntf);
        }

        /// <summary>
        /// 用户输入文字聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengChatWordReq(UInt32 from, UInt64 accountId, MJYunChengChatWordReq req)
        {
            MJYunChengChatWordRsp rsp = new MJYunChengChatWordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengChatWordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            if (room == null)
            {
                rsp.errorCode = MJYunChengChatWordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengChatWordReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = MJYunChengChatWordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJYunChengChatWordNtf ntf = new MJYunChengChatWordNtf();
            ntf.roleId = accountId;
            ntf.chatWord = req.chatWord;
            room.SendRoomMsgEx(accountId, ntf);
        }

        /// <summary>
        /// 扔鸡蛋动画
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengEggReq(UInt32 from,UInt64 accountId, MJYunChengEggReq req)
        {
            MJYunChengEggRsp rsp = new MJYunChengEggRsp();
            rsp.errorCode = MJYunChengEggRsp.ErrorID.Failed;

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengEggReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            BattlePlayer bPlayer = g.playerMgr.FindPlayer(req.roleid);
            if(bPlayer == null)
            {
                LogSys.Warn("[MJYunChengModule][OnMJYunChengEggReq],找不到玩家,accountId={0}", req.roleid);
                player.SendMsgToClient(rsp);
                return;
            }

            rsp.errorCode = MJYunChengEggRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(player.RoomUuid);
            MJYunChengEggNtf ntf = new MJYunChengEggNtf();
            ntf.throwRoleid = accountId;
            ntf.beRoleid = req.roleid;
            ntf.eggType = req.eggType;
            foreach (YunChengRoomRole role in room.lstRoles)
            {
                if(role.roleid != accountId)
                {
                    role.SendMsgToClient(ntf);
                }
            }
        }

        /// <summary>
        /// 房主建房解散房间l2b
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJYunChengOtherCloseRoomL2B(UInt32 from, UInt64 accountId, MJYunChengOtherCloseRoomL2B req)
        {
            DissolveTheRoomRsp rsp = new DissolveTheRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                player = new BattlePlayer();
                player.AccountId = accountId;
            }

            YunChengRoom room = g.yunChengRoomMgr.GetRoom(req.roomId);
            if (null == room)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengOtherCloseRoomL2B],房间不存在,accountId={0}, roomUuid={1}", accountId, req.roomId);
                return;
            }
            room.roomOwner.roleid = accountId;

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = DissolveTheRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJYunChengModule][OnMJYunChengLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = DissolveTheRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJYunChengLeaveRoomNtf ntf = new MJYunChengLeaveRoomNtf();
            ntf.roleid = accountId;

            //直接关闭房间
            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }
    }
}
