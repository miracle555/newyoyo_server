﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Protocols.MajiangPushDown;
using Utility.Debugger;
using BattleServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using MahjongPushDown;
using Protocols.Majiang;
using HelpAsset;
using HelpAsset.MahjongPushDown;

namespace Module
{
    /// <summary>
    /// 推倒胡模块
    /// </summary>
    public class MJPushDownModule : LogicModule
    {
        public MJPushDownModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            //创建房间
            RegisterMsgHandler(MSGID.MJPushDownCreateRoomL2B, new MsgHandler<MJPushDownCreateRoomL2B>(OnMJPushDownCreateRoomL2B));

            //加入房间
            RegisterMsgHandler(MSGID.MJPushDownJoinRoomL2B, new MsgHandler<MJPushDownJoinRoomL2B>(OnMJPushDownJoinRoomL2B));

            //获取房间信息
            RegisterMsgHandler(MSGID.MJPushDownGetRoomInfoL2B, new MsgHandler<MJPushDownGetRoomInfoL2B>(OnMJPushDownGetRoomInfoL2B));

            //出牌请求
            RegisterMsgHandler(MSGID.MJPushDownHandOutCardReq, new MsgHandler<MJPushDownHandOutCardReq>(OnMJPushDownHandOutCardReq));

            //碰牌请求
            RegisterMsgHandler(MSGID.MJPushDownPengCardReq, new MsgHandler<MJPushDownPengCardReq>(OnMJPushDownPengCardReq));

            //胡牌请求
            RegisterMsgHandler(MSGID.MJPushDownHuCardReq, new MsgHandler<MJPushDownHuCardReq>(OnMJPushDownHuCardReq));

            //杠牌请求
            RegisterMsgHandler(MSGID.MJPushDownGangCardReq, new MsgHandler<MJPushDownGangCardReq>(OnMJPushDownGangCardReq));

            //听牌请求
            RegisterMsgHandler(MSGID.MJPushDownReadyCardReq, new MsgHandler<MJPushDownReadyCardReq>(OnMJPushDownReadyCardReq));

            //过
            RegisterMsgHandler(MSGID.MJPushDownPassReq, new MsgHandler<MJPushDownPassReq>(OnMJPushDownPassReq));
            
            // 下一步
            RegisterMsgHandler(MSGID.MJPushDownNextStepReq, new MsgHandler<MJPushDownNextStepReq>(OnMJPushDownNextStepReq));

            // (推倒胡麻将)解散游戏
            RegisterMsgHandler(MSGID.MJPushDownDismissGameReq, new MsgHandler<MJPushDownDismissGameReq>(OnMJPushDownDismissGameReq));

            // 解散是否同意
            RegisterMsgHandler(MSGID.MJPushDownDismissAgreeReq, new MsgHandler<MJPushDownDismissAgreeReq>(OnMJPushDownDismissAgreeReq));

            // 缺一门选择
            RegisterMsgHandler(MSGID.MJPushDownLackingOneDoorReq, new MsgHandler<MJPushDownLackingOneDoorReq>(OnMJPushDownLackingOneDoorReq));

            // 离开房间，房间未开始时
            RegisterMsgHandler(MSGID.MJPushDownLeaveRoomReq, new MsgHandler<MJPushDownLeaveRoomReq>(OnMJPushDownLeaveRoomReq));

            // 检测自摸胡，暗杠，补杠，听牌
            RegisterMsgHandler(MSGID.MJPushDownCheckSelfGangHuReadyReq, new MsgHandler<MJPushDownCheckSelfGangHuReadyReq>(OnMJPushDownCheckSelfGangHuReadyReq));

            // 聊天
            RegisterMsgHandler(MSGID.MJPushDownChatReq, new MsgHandler<MJPushDownChatReq>(OnMJPushDownChatReq));

            //文字聊天
            RegisterMsgHandler(MSGID.MJPushDownChatWordReq, new MsgHandler<MJPushDownChatWordReq>(OnMJPushDownChatWordReq));
            
            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownCreateRoomL2B(UInt32 from, UInt64 accountId, MJPushDownCreateRoomL2B req)
        {
            MJPushDownCreateRoomRsp rsp = new MJPushDownCreateRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if(null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownCreateRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            UInt64 roomuuid = req.roomUuid;
            player.RoomUuid = roomuuid;

            HuRoom room = new HuRoom(roomuuid);
            room.InitRoom(accountId, player.NickName, EGameType.MJPushDown);
            room.config = req.config;
            AddRoleToRoom(player, room);
            
            g.huRoomMgr.AddRoom(room);

            rsp.errorCode = MJPushDownCreateRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.SaveDb(true);
        }

        void AddRoleToRoom(BattlePlayer player, HuRoom room)
        {
            HuRoomRole role = new HuRoomRole();
            role.Init(player.AccountId, player.NickName, room);
            role.direction = room.GetOneSeat();
            role.sex = player.sex;
            role.level = player.level;
            role.roleState = HuRoomRoleState.WaitRoomer;
            role.weixinHeadImgUrl = player.weixinHeadImgUrl;
            room.AddRole(role);
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownJoinRoomL2B(UInt32 from, UInt64 accountId, MJPushDownJoinRoomL2B req)
        {
            MJPushDownJoinRoomRsp rsp = new MJPushDownJoinRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = await g.huRoomMgr.FindRoomAsync(req.roomUuid);
            if(null == room)
            {
                rsp.errorCode = MJPushDownJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            if(room.IsRoomFull())
            {
                rsp.errorCode = MJPushDownJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownJoinRoomL2B],房间已满,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;
            AddRoleToRoom(player, room);

            rsp.errorCode = MJPushDownJoinRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            // 通知其他人
            MJPushDownJoinRoomNtf ntf = new MJPushDownJoinRoomNtf();
            ntf.remoteRoleInfo = room.GetRole(accountId).remoteInfo;
            room.SendRoomMsgEx(accountId, ntf);

            // 房间人数已满，回合开始
            if(room.IsRoomFull())
            {
                room.FirstInit();
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPushDownGetRoomInfoL2B(UInt32 from, UInt64 accountId, MJPushDownGetRoomInfoL2B req)
        {
            MJPushDownGetRoomInfoRsp rsp = new MJPushDownGetRoomInfoRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownGetRoomInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;

            HuRoom room = await g.huRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownGetRoomInfoL2B],房间不存在呢,accountId={0}, nickName={1}, roomUuid={2}", accountId, player.NickName, player.RoomUuid);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownGetRoomInfoL2B],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = MJPushDownGetRoomInfoRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.CheckDismissTimer();
        }

        /// <summary>
        /// 出牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownHandOutCardReq(UInt32 from, UInt64 accountId, MJPushDownHandOutCardReq req)
        {
            MJPushDownHandOutCardRsp rsp = new MJPushDownHandOutCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownHandOutCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHandOutCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHandOutCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.currentRound.HasHandCard(req.cardid))
            {
                rsp.errorCode = MJPushDownHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHandOutCardReq],手牌中没有该牌,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }

            // 不是出牌状态
            if(!role.IsHandingOutState)
            {
                rsp.errorCode = MJPushDownHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHandOutCardReq],不是出牌状态,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }
            //EMJCardType type = MJCardHelp.GetMJType(req.cardid);
            //LogSys.Warn("出牌accountId:" + accountId + "cardid:" + req.cardid + "   type:" + type);

            stCheckRoomItem roomitem = new stCheckRoomItem();
            stCheckRoleItem roleitem = new stCheckRoleItem();
            role.GetCheckRoomRoleInfo(roomitem, roleitem);

            // 听口改变
            if (PushDownHelp.CheckReadyChange(req.cardid, roomitem, roleitem))
            {
                rsp.errorCode = MJPushDownHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHandOutCardReq],听口改变,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }

            HandleHandOutCard(accountId, room, role, req.cardid);

            rsp.errorCode = MJPushDownHandOutCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);
            
            room.SaveDb();
        }

        /// <summary>
        /// 出牌
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="room"></param>
        /// <param name="role"></param>
        /// <param name="cardid"></param>
        /// <param name="bReadyOut">是否是听牌出牌</param>
        void HandleHandOutCard(UInt64 accountId, HuRoom room, HuRoomRole role, UInt32 cardid, bool bReadyOut = false)
        {
            // 过胡后，直至玩家打出一张牌，才可以正常胡牌
            role.currentRound.bPassHu = false;
            role.bRobGangHu = false;

            // 记录出牌信息
            room.currentRound.lastHandOutCard.Clear();
            room.currentRound.lastHandOutCard.RecordHandOutCard(accountId, cardid);

            // 从手牌中删除该牌
            role.currentRound.RemoveHandCard(cardid);
            role.currentRound.AddPoolCard(cardid);

            // 通知其他人自己出牌
            MJPushDownHandOutCardNtf ntf = new MJPushDownHandOutCardNtf();
            ntf.cardRoleId = accountId;
            ntf.cardid = cardid;
            ntf.bReadyOut = bReadyOut;
            room.SendRoomMsgEx(accountId, ntf);

            role.currentRound.playState = ERolePlayState.Wait;
            
            // 记录听牌时出的这张牌
            if(bReadyOut)
            {
                role.currentRound.readyOutCardId = cardid;

                // 下一个人起牌
                role.nextRole.DrawNewCard();

                return;
            }

            // 检测其他人碰杠胡
            List<HuRoomRole> lstOthers = room.NextRoles(role);

            // 检测胡
            foreach (var other in lstOthers)
            {
                EHuCardType huType = EHuCardType.Null;
                if (other.CanHuOther(cardid, ref huType))
                {
                    stOutCardReadyItem readyItem = new stOutCardReadyItem();
                    readyItem.roleid = other.roleid;
                    readyItem.huType = huType;
                    readyItem.bSelfTouch = false;

                    readyItem.readyInfo.cardRoleId = accountId;
                    readyItem.readyInfo.cardid = cardid;
                    readyItem.readyInfo.bHuPai = true;
                    room.currentRound.lastHandOutCard.AddReadyItem(readyItem);
                }
            }

            // 检测碰
            foreach (var other in lstOthers)
            {
                if (other.CanPeng(cardid))
                {
                    stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();
                    readyInfo.bPeng = true;
                    readyInfo.cardRoleId = accountId;
                    readyInfo.cardid = cardid;
                    room.currentRound.lastHandOutCard.AddReadyItem(other.roleid, readyInfo);
                }
            }

            // 检测明杠
            foreach (var other in lstOthers)
            {
                if (other.CanMingGang(cardid))
                {
                    stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();
                    readyInfo.gangType = EGangCardType.MingGang;
                    readyInfo.cardRoleId = accountId;
                    readyInfo.cardid = cardid;
                    room.currentRound.lastHandOutCard.AddReadyItem(other.roleid, readyInfo);
                }
            }

            if (room.currentRound.lastHandOutCard.HasReadyItem())
            {
                stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                HuRoomRole readyRole = room.GetRole(item.roleid);
                readyRole.NotifyPengGangHuReady(item);
            }
            else
            {
                role.nextRole.DrawNewCard();
            }
        }

        string ToString(List<UInt32> lstCards)
        {
            string temp = "";
            foreach (var cardid in lstCards)
            {
                temp += cardid.ToString();
                temp += ",";
            }
            return temp;
        }

        /// <summary>
        /// 碰牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownPengCardReq(UInt32 from, UInt64 accountId, MJPushDownPengCardReq req)
        {
            MJPushDownPengCardRsp rsp = new MJPushDownPengCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownPengCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPengCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPengCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            // 不是碰牌状态
            if(!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPushDownPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPengCardReq],不是碰牌状态,accountId={0}", accountId);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if(!readyItem.readyInfo.bPeng)
            {
                rsp.errorCode = MJPushDownPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPengCardReq],玩家不能碰，却发碰牌消息,accountId={0}", accountId);
                return;
            }

            // 出牌人
            HuRoomRole handRole = room.GetRole(readyItem.readyInfo.cardRoleId);
            if (null == handRole)
            {
                rsp.errorCode = MJPushDownPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPengCardReq],出牌人不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, readyItem.readyInfo.cardRoleId);
                return;
            }

            if(!role.CanPeng(readyItem.readyInfo.cardid))
            {
                rsp.errorCode = MJPushDownPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPengCardReq],不能碰,accountId={0}, lastHandOutCardRole={1}", accountId, readyItem.readyInfo.cardRoleId);
                return;
            }

            // 从出牌人池牌中删除
            handRole.currentRound.RemovePoolCard(readyItem.readyInfo.cardid);

            // 把牌添加到碰牌者的明牌中
            EMJCardType cardType = MJCardHelp.GetMJType(readyItem.readyInfo.cardid);
            List<UInt32> lstPengCards = role.GetPengCards(cardType);
            role.currentRound.RemoveHandCards(lstPengCards);
            role.AddPengCards(readyItem.readyInfo.cardid, lstPengCards);

            rsp.errorCode = MJPushDownPengCardRsp.ErrorID.Success;
            rsp.cardRoleId = readyItem.readyInfo.cardRoleId;
            rsp.cardid = readyItem.readyInfo.cardid;
            rsp.lstPengCards = lstPengCards;
            player.SendMsgToClient(rsp);

            // 通知其他人碰牌
            MJPushDownPengCardNtf ntf = new MJPushDownPengCardNtf();
            ntf.cardRoleId = readyItem.readyInfo.cardRoleId;
            ntf.cardid = readyItem.readyInfo.cardid;
            ntf.pengRoleId = accountId;
            ntf.lstCards = lstPengCards;
            room.SendRoomMsgEx(accountId, ntf);

            role.currentRound.playState = ERolePlayState.Wait;

            // 碰牌也算起新牌,需要客户端检测听牌暗杠
            role.CheckSelfGangHuReady(true);

            room.SaveDb();
        }

        /// <summary>
        /// 胡牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownHuCardReq(UInt32 from, UInt64 accountId, MJPushDownHuCardReq req)
        {
            MJPushDownHuCardRsp rsp = new MJPushDownHuCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownHuCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHuCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHuCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            // 不是胡牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPushDownHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHuCardReq],不是胡牌状态,accountId={0}", accountId);
                return;
            }

            // 未胡牌
            if (!role.currentRound.readyItem.readyInfo.bHuPai)
            {
                rsp.errorCode = MJPushDownHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownHuCardReq],玩家未胡牌,accountId={0}", accountId);
                return;
            }

            room.currentRound.huRoleId = accountId;

            stOutCardReadyItem readyItem = role.currentRound.readyItem;

            // 自摸胡
            if (readyItem.bSelfTouch)
            {
                rsp.errorCode = MJPushDownHuCardRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                MJPushDownHuCardNtf ntf = new MJPushDownHuCardNtf();
                ntf.bSelfTouch = true;
                ntf.huRoleId = accountId;
                room.SendRoomMsgEx(accountId, ntf);

                room.currentRound.RecordSettleItem(readyItem.huType, true, role, 0, role.currentRound.lastNewCardId, false);
            }
            else
            {
                if (role.bRobGangHu)
                {
                    HuRoomRole otherRole = room.GetRole(role.currentRound.readyItem.readyInfo.cardRoleId);
                    if (otherRole == null)
                    {
                        rsp.errorCode = MJPushDownHuCardRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[MJPushDownModule][OnMJPushDownHuCardReq],被抢杠胡玩家不在房间中,accountId={0}", accountId);
                        return;
                    }
                    RemoveCardInShowCard(otherRole, role.currentRound.readyItem.readyInfo.cardid);
                }

                rsp.errorCode = MJPushDownHuCardRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                MJPushDownHuCardNtf ntf = new MJPushDownHuCardNtf();
                ntf.bSelfTouch = false;
                ntf.cardRoleId = readyItem.readyInfo.cardRoleId;
                ntf.huRoleId = accountId;
                room.SendRoomMsgEx(accountId, ntf);


                // 点炮人是否报听
                bool bOtherBaoTing = room.GetRole(readyItem.readyInfo.cardRoleId).currentRound.AlreadyBaoTing;
                room.currentRound.RecordSettleItem(readyItem.huType, false, role, readyItem.readyInfo.cardRoleId, readyItem.readyInfo.cardid, bOtherBaoTing);
            }

            // 玩家赢了一局
            MJPushDownWinOneRoundB2L winNtf = new MJPushDownWinOneRoundB2L();
            player.SendMsgToLobby(winNtf);

            room.Settle();

            room.SaveDb();
        }

        void RemoveCardInShowCard(HuRoomRole role,uint cardId)
        {
            for (int i = 0; i < role.currentRound.showCards.Count; i++)
            {
                if (role.currentRound.showCards[i].mingType == EMingCardType.MingGang)
                {
                    if (role.currentRound.showCards[i].showCards.Contains(cardId))
                    {
                        role.currentRound.showCards[i].showCards.Remove(cardId);
                        role.currentRound.showCards[i].mingType = EMingCardType.Peng;
                    } 
                }
            }
        }

        /// <summary>
        /// 杠牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownGangCardReq(UInt32 from, UInt64 accountId, MJPushDownGangCardReq req)
        {
            MJPushDownGangCardRsp rsp = new MJPushDownGangCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownGangCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownGangCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownGangCardReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是杠牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPushDownGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownGangCardReq],不是杠牌状态,accountId={0}", accountId);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if(readyItem.readyInfo.gangType == EGangCardType.Null)
            {
                rsp.errorCode = MJPushDownGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownGangCardReq],玩家不能杠牌,accountId={0}", accountId);
                return;
            }      

            // 暗杠
            if(readyItem.readyInfo.gangType == EGangCardType.AnGang)
            {
                HandleAnGang(room, role, readyItem.readyInfo.cardid);
            }
            else if(readyItem.readyInfo.gangType == EGangCardType.BuGang)
            {
                HandleBuGang(room, role, readyItem.readyInfo.cardid);
            }
            else if (readyItem.readyInfo.gangType == EGangCardType.MingGang)
            {
                HandleMingGang(room, role);
            }          

            role.currentRound.playState = ERolePlayState.Wait;

            if (room.currentRound.lastHandOutCard.HasReadyItem())
            {
                stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                HuRoomRole readyRole = room.GetRole(item.roleid);
                readyRole.NotifyPengGangHuReady(item);
            }
            else
            {
                // 通知当前玩家起牌
                role.DrawNewCard();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 处理明杠
        /// </summary>
        /// <param name="room"></param>
        /// <param name="cardRole"></param>
        /// <param name="self"></param>
        void HandleMingGang(HuRoom room, HuRoomRole self)
        {
            UInt32 cardid = room.currentRound.lastHandOutCard.cardid;
            UInt64 cardRoleId = room.currentRound.lastHandOutCard.roleid;
            HuRoomRole handRole = room.GetRole(cardRoleId);

            EMJCardType type = MJCardHelp.GetMJType(cardid);
            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);
            handRole.currentRound.RemovePoolCard(cardid);

            self.AddMingGangCards(cardid, lstCards);
            //LogSys.Warn("[MJPushDownModule][明杠],role={0},cardid={1}, lstCards={2},type={3}", self.nickName, cardid, ToString(lstCards), type);

            MJPushDownGangCardRsp rsp = new MJPushDownGangCardRsp();
            rsp.errorCode = MJPushDownGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.MingGang;
            rsp.cardRoleId = cardRoleId;
            rsp.cardid = cardid;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJPushDownGangCardNtf ntf = new MJPushDownGangCardNtf();
            ntf.gangType = EGangCardType.MingGang;
            ntf.loseRoleId = cardRoleId;
            ntf.winRoleId = self.roleid;
            ntf.cardid = cardid;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 记录结算条目
            room.currentRound.RecordSettleItem(EGangCardType.MingGang, false, self.roleid, cardRoleId, cardid, handRole.currentRound.AlreadyBaoTing);
        }

        /// <summary>
        /// 处理暗杠
        /// </summary>
        /// <param name="room"></param>
        /// <param name="cardRole"></param>
        /// <param name="self"></param>
        void HandleAnGang(HuRoom room, HuRoomRole self, UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);
            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);

            self.AddAnGangCards(lstCards);
            //LogSys.Warn("[MJPushDownModule][暗杠],role={0},cardid={1}, lstCards={2},type={3}", self.nickName, cardid, ToString(lstCards), type);

            MJPushDownGangCardRsp rsp = new MJPushDownGangCardRsp();
            rsp.errorCode = MJPushDownGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.AnGang;
            rsp.cardRoleId = 0;
            rsp.cardid = 0;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJPushDownGangCardNtf ntf = new MJPushDownGangCardNtf();
            ntf.gangType = EGangCardType.AnGang;
            ntf.loseRoleId = 0;
            ntf.winRoleId = self.roleid;
            ntf.cardid = 0;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 记录结算条目
            room.currentRound.RecordSettleItem(EGangCardType.AnGang, true, self.roleid, 0, cardid, false);
        }

        /// <summary>
        /// 处理补杠
        /// </summary>
        void HandleBuGang(HuRoom room, HuRoomRole self, UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);
            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);

            self.AddBuGangCards(lstCards);

            //LogSys.Warn("[MJPushDownModule][补杠],role={0},cardid={1}, lstCards={2},type={3}", self.nickName, cardid, ToString(lstCards), type);

            MJPushDownGangCardRsp rsp = new MJPushDownGangCardRsp();
            rsp.errorCode = MJPushDownGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.BuGang;
            rsp.cardRoleId = 0;
            rsp.cardid = 0;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJPushDownGangCardNtf ntf = new MJPushDownGangCardNtf();
            ntf.gangType = EGangCardType.BuGang;
            ntf.loseRoleId = 0;
            ntf.winRoleId = self.roleid;
            ntf.cardid = 0;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 检测其他人胡
            List<HuRoomRole> lstOthers = room.NextRoles(self);

            // 检测胡
            foreach (var other in lstOthers)
            {
                stOutCardReadyItem readyItem = new stOutCardReadyItem();
                EHuCardType huType = EHuCardType.Null;       
                if (other.CanHuOther(cardid, ref huType))
                {
                    other.bRobGangHu = true;
                    readyItem.roleid = other.roleid;
                    readyItem.huType = huType;
                    readyItem.bSelfTouch = false;

                    readyItem.readyInfo.cardRoleId = self.roleid;
                    readyItem.readyInfo.cardid = cardid;
                    readyItem.readyInfo.bHuPai = true;
                    room.currentRound.lastHandOutCard.AddReadyItem(readyItem);
                    room.currentRound.lastHandOutCard.roleid = self.roleid;
                }
            }

            if (!room.currentRound.lastHandOutCard.HasReadyItem())
            {
                // 记录结算条目
                room.currentRound.RecordSettleItem(EGangCardType.BuGang, true, self.roleid, 0, cardid, false);
            }
        }

        /// <summary>
        /// 听牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownReadyCardReq(UInt32 from, UInt64 accountId, MJPushDownReadyCardReq req)
        {
            MJPushDownReadyCardRsp rsp = new MJPushDownReadyCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是听牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPushDownReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],不是听牌状态,accountId={0}", accountId);
                return;
            }

            // 已经报听，不能再报听
            if(role.currentRound.AlreadyBaoTing)
            {
                rsp.errorCode = MJPushDownReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],玩家已经听牌,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if(!readyItem.readyInfo.bReadyCard)
            {
                rsp.errorCode = MJPushDownReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],玩家未听牌,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            if(!readyItem.CanReadyCard(req.cardid))
            {
                rsp.errorCode = MJPushDownReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],出该牌不能听,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            HandleHandOutCard(accountId, room, role, req.cardid, true);

            readyItem.GetReadyTypes(req.cardid,role.currentRound.lstReadyTypes);
            if (role.currentRound.lstReadyTypes.Count == 0)
            {
                rsp.errorCode = MJPushDownReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownReadyCardReq],听牌异常错误,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            role.currentRound.playState = ERolePlayState.Wait;

            player.SendMsgToClient(rsp);

            MJPushDownReadyCardNtf ntf = new MJPushDownReadyCardNtf();
            ntf.readyRoleId = accountId;
            room.SendRoomMsgEx(accountId, ntf);

            room.SaveDb();
        }

        /// <summary>
        /// 过
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownPassReq(UInt32 from, UInt64 accountId, MJPushDownPassReq req)
        {
            MJPushDownPassRsp rsp = new MJPushDownPassRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownPassReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPassReq],房间不存在,accountId={0}", accountId);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPassReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是碰杠胡听状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPushDownPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownPassReq],不是碰杠胡听状态,accountId={0}", accountId);
                return;
            }

            role.currentRound.playState = ERolePlayState.Wait;

            rsp.errorCode = MJPushDownPassRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            stOutCardReadyItem readyItem = role.currentRound.readyItem;

            // 过胡后一圈内不能再胡牌
            if (readyItem.readyInfo.bHuPai)
            {
                if (!readyItem.bSelfTouch)
                {
                    // 过胡设置为true
                    role.currentRound.bPassHu = true;
                }
            }

            if (IsSelfHandCard(role.currentRound.readyItem))
            {
                // 该你出牌了
                role.NotifyNeedHandOutCard();
            }
            else
            {
                if (room.currentRound.lastHandOutCard.HasReadyItem())
                {
                    stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                    HuRoomRole readyRole = room.GetRole(item.roleid);
                    readyRole.NotifyPengGangHuReady(item);
                }
                else
                {
                    // 出牌人
                    HuRoomRole handRole = room.GetRole(room.currentRound.lastHandOutCard.roleid);
                    if (null == handRole)
                    {
                        rsp.errorCode = MJPushDownPassRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[MJPushDownModule][OnMJPushDownPassReq],出牌人不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                        return;
                    }

                    if (role.bRobGangHu)
                    {
                        room.currentRound.RecordSettleItem(EGangCardType.BuGang, true, room.currentRound.lastHandOutCard.roleid, 0, room.currentRound.lastHandOutCard.cardid, false);
                        handRole.DrawNewCard();
                    }
                    else
                    {
                        // 下个人起牌
                        handRole.nextRole.DrawNewCard();
                    }
                }
            }
            room.SaveDb();
        }

        bool IsSelfHandCard(stOutCardReadyItem readyItem)
        {
            stPengGangHuReadyInfo readyInfo = readyItem.readyInfo;

            // 暗杠和补杠
            if(readyInfo.gangType == EGangCardType.AnGang || readyInfo.gangType == EGangCardType.BuGang)
            {
                return true;
            }

            // 自摸
            if (readyInfo.bHuPai)
            {
                if(readyItem.bSelfTouch)
                {
                    return true;
                }
            }

            if(readyInfo.bReadyCard)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 缺一门选择
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownLackingOneDoorReq(UInt32 from, UInt64 accountId, MJPushDownLackingOneDoorReq req)
        {
            MJPushDownLackingOneDoorRsp rsp = new MJPushDownLackingOneDoorRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownLackingOneDoorReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownLackingOneDoorRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLackingOneDoorReq],房间不存在,accountId={0}", accountId);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownLackingOneDoorRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLackingOneDoorReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if(!MJCardHelp.IsRightDoor(req.doorType))
            {
                rsp.errorCode = MJPushDownLackingOneDoorRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLackingOneDoorReq],门类型不正确,accountId={0}", accountId);
                return;
            }

            if(!room.config.bLackingDoor)
            {
                rsp.errorCode = MJPushDownLackingOneDoorRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLackingOneDoorReq],不是缺一门房间,accountId={0}", accountId);
                return;
            }

            if(!room.IsLackingDoorState)
            {
                rsp.errorCode = MJPushDownLackingOneDoorRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLackingOneDoorReq],房间不在缺一门状态,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = MJPushDownLackingOneDoorRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            if (role.roleState != HuRoomRoleState.Play)
            {
                return;
            }

            if (role.currentRound.playState != ERolePlayState.LackingDoor)
            {
                return;
            }

            role.currentRound.doorType = req.doorType;

            role.currentRound.playState = ERolePlayState.Wait;

            if (room.IsAllRoleLackingDoorFinish())
            {
                MJPushDownLackingDoorFinishNtf ntf = new MJPushDownLackingDoorFinishNtf();
                foreach (var item in room.lstRoles)
                {
                    ntf.mapDoors.Add(item.roleid, item.currentRound.doorType);
                }
                room.SendRoomMsg(ntf);

                // 需要客户端检测听牌暗杠
                room.bankerRole.CheckSelfGangHuReady();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 推倒胡麻将 下一步
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownNextStepReq(UInt32 from, UInt64 accountId, MJPushDownNextStepReq req)
        {
            MJPushDownNextStepRsp rsp = new MJPushDownNextStepRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownNextStepReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownNextStepReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            if(room.roomState != ERoomState.WaitRound)
            {
                rsp.errorCode = MJPushDownNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownNextStepReq],房间状态不对,accountId={0}, roomState={1}", accountId, room.roomState);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownNextStepReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (role.roleState != HuRoomRoleState.Settle)
            {
                rsp.errorCode = MJPushDownNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownNextStepReq],玩家状态不对，不在结算状态,accountId={0}", accountId);
                return;
            }

            role.roleState = HuRoomRoleState.WaitRound;

            rsp.errorCode = MJPushDownNextStepRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPushDownNextStepNtf ntf = new MJPushDownNextStepNtf();
            ntf.roleid = accountId;
            room.SendRoomMsgEx(accountId, ntf);

            // 所有玩家都过了结算界面
            if (room.IsAllRoleWaitRound())
            {
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 检测暗杠，补杠，自摸胡，听牌
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownCheckSelfGangHuReadyReq(UInt32 from, UInt64 accountId, MJPushDownCheckSelfGangHuReadyReq req)
        {
            LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],该消息不再使用,accountId={0}", accountId);
            /*
            MJPushDownCheckSelfGangHuReadyRsp rsp = new MJPushDownCheckSelfGangHuReadyRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownCheckSelfGangHuReadyRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownCheckSelfGangHuReadyRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            if(!role.IsNeedCheckSelfPengGangHuReadyState)
            {
                rsp.errorCode = MJPushDownCheckSelfGangHuReadyRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],玩家状态不对，不在检测听牌状态,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            bool bCheck = req.checkItem.bAnGang || req.checkItem.bBuGang || req.checkItem.bHuCard || req.checkItem.bReadyCard;
            if(!bCheck)
            {
                rsp.errorCode = MJPushDownCheckSelfGangHuReadyRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                role.NotifyNeedHandOutCard();

                room.SaveDb();
                return;
            }

            stOutCardReadyItem readyItem = new stOutCardReadyItem();
            readyItem.roleid = accountId;
            readyItem.bSelfTouch = true;

            stPengGangHuReadyInfo readyInfo = readyItem.readyInfo;
            readyInfo.cardRoleId = accountId;

            stCheckRoomItem roomitem = new stCheckRoomItem();
            stCheckRoleItem roleitem = new stCheckRoleItem();
            role.GetCheckRoomRoleInfo(roomitem, roleitem);

            List<UInt32> lstReadyCards = new List<UInt32>();

            if(req.checkItem.bReadyCard)
            {
                if (role.currentRound.checkItem.bReadyCard && PushDownHelp.CanReadyCard(lstReadyCards, roomitem, roleitem))
                {
                    // 是否听牌
                    readyInfo.bReadyCard = true;
                    readyInfo.lstReadyCards = role.GetReadyInfos(lstReadyCards);
                }
                else
                {
                     LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],客户端计算能听牌，服务器却不能听牌,accountId={0}", accountId);
                }
            }

            if(req.checkItem.bHuCard)
            {
                if (role.currentRound.checkItem.bHuCard && PushDownHelp.CanHuSelf(ref readyItem.huType, roomitem, roleitem))
                {
                    readyInfo.bHuPai = true;
                }
                else
                {
                    LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],客户端计算能胡牌，服务器却不能胡牌,accountId={0}", accountId);
                }
            }

            if(req.checkItem.bAnGang)
            {
                if (role.currentRound.checkItem.bAnGang && PushDownHelp.CanAnGang(roomitem, roleitem))
                {
                    EMJCardType type = role.currentRound.vecHandCards.GetAnGangType();
                    UInt32 outcardid = role.currentRound.GetHandCardByType(type);

                    readyInfo.gangType = EGangCardType.AnGang;
                    readyInfo.cardid = outcardid;
                }
                else
                {
                    LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],客户端计算能暗杠，服务器却不能暗杠,accountId={0}", accountId);
                }
            }

            if(req.checkItem.bBuGang)
            {
                if (role.currentRound.checkItem.bBuGang && PushDownHelp.CanBuGang(roomitem, roleitem))
                {
                    // 判断补杠
                    UInt32 outcardid = PushDownHelp.GetBuGangCardId(roomitem, roleitem);
                    if (outcardid != 0)
                    {
                        readyInfo.gangType = EGangCardType.BuGang;
                        readyInfo.cardid = outcardid;
                    }
                }
                else
                {
                    LogSys.Warn("[MJPushDownModule][OnMJPushDownCheckSelfGangHuReadyReq],客户端计算能补杠，服务器却不能补杠,accountId={0}", accountId);
                }
            }

            rsp.errorCode = MJPushDownCheckSelfGangHuReadyRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            if(role.IsPengGangHuReady(readyItem))
            {
                role.NotifyPengGangHuReady(readyItem);
            }
            else
            {
                role.NotifyNeedHandOutCard();
            }

            room.SaveDb();
            */
        }

        /// <summary>
        /// (推倒胡麻将)解散游戏
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownDismissGameReq(UInt32 from, UInt64 accountId, MJPushDownDismissGameReq req)
        {
            MJPushDownDismissGameRsp rsp = new MJPushDownDismissGameRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissGameReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissGameReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissGameReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = MJPushDownDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissGameReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            if(room.dismissInfo.bStart)
            {
                rsp.errorCode = MJPushDownDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissGameReq],上次解散流程未走完,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            // 重新开始解散流程
            room.dismissInfo.Clear();
            room.dismissInfo.bStart = true;
            room.dismissInfo.expireTime = env.Timer.CurrentDateTime.AddMinutes(3);
            room.dismissInfo.firstRoleId = accountId;
            room.dismissInfo.setAgreeRoles.Add(accountId);

            rsp.errorCode = MJPushDownDismissGameRsp.ErrorID.Success;
            rsp.remainSecond = room.dismissRemainSecond;
            player.SendMsgToClient(rsp);

            MJPushDownDismissGameNtf ntf = new MJPushDownDismissGameNtf();
            ntf.roleid = role.roleid;
            ntf.remainSecond = room.dismissRemainSecond;
            room.SendRoomMsgEx(role.roleid, ntf);

            room.SaveDb();
        }

        /// <summary>
        /// 解散是否同意
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownDismissAgreeReq(UInt32 from, UInt64 accountId, MJPushDownDismissAgreeReq req)
        {
            MJPushDownDismissAgreeRsp rsp = new MJPushDownDismissAgreeRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissAgreeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissAgreeReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissAgreeReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = MJPushDownDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownDismissAgreeReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = MJPushDownDismissAgreeRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 解散流程已结束
            if(room.dismissInfo.firstRoleId == 0)
            {
                return;
            }

            MJPushDownDismissAgreeNtf ntf = new MJPushDownDismissAgreeNtf();
            ntf.roleid = accountId;
            ntf.bAgree = req.bAgree;
            room.SendRoomMsgEx(accountId, ntf);

            if(req.bAgree)
            {
                room.dismissInfo.setAgreeRoles.Add(accountId);
            }
            else
            {
                room.dismissInfo.setNotRoles.Add(accountId);
            }

            // 解散房间成功
            if(room.dismissInfo.setAgreeRoles.Count >= (HuRoom.MAX_ROLES_COUNT -1))
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
                return;
            }
            
            // 解散房间失败
            if(room.dismissInfo.setNotRoles.Count >= 1)
            {
                MJPushDownDismissGameSuccessNtf successNtf = new MJPushDownDismissGameSuccessNtf();
                successNtf.bSuccess = false;
                room.SendRoomMsg(successNtf);

                room.dismissInfo.Clear();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownLeaveRoomReq(UInt32 from, UInt64 accountId, MJPushDownLeaveRoomReq req)
        {
            MJPushDownLeaveRoomRsp rsp = new MJPushDownLeaveRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownLeaveRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLeaveRoomReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            HuRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPushDownLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLeaveRoomReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = MJPushDownLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = MJPushDownLeaveRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPushDownLeaveRoomNtf ntf = new MJPushDownLeaveRoomNtf();
            ntf.roleid = accountId;

            // 不是房主
            if(accountId != room.roomOwner.roleid)
            {
                room.SendRoomMsgEx(accountId, ntf);

                // 解绑战斗服
                player.UnBindBattleFromGate();

                // 通知lobby离开房间
                MJPushDownLeaveRoomB2L ntfLobby = new MJPushDownLeaveRoomB2L();
                ntfLobby.roomUuid = room.roomUuid;
                player.SendMsgToLobby(ntfLobby);

                g.playerMgr.RemovePlayer(role.roleid);

                room.RemoveRole(accountId);

                room.SaveDb();

                return;
            }

            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }

        /// <summary>
        /// 聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownChatReq(UInt32 from, UInt64 accountId, MJPushDownChatReq req)
        {
            MJPushDownChatRsp rsp = new MJPushDownChatRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownChatReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPushDownChatRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownChatReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = MJPushDownChatRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPushDownChatNtf ntf = new MJPushDownChatNtf();
            ntf.roleid = accountId;
            ntf.chatid = req.chatid;
            room.SendRoomMsgEx(accountId, ntf);
        }

        /// <summary>
        /// 用户输入文字聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPushDownChatWordReq(UInt32 from, UInt64 accountId, MJPushDownChatWordReq req)
        {
            MJPushDownChatWordRsp rsp = new MJPushDownChatWordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[MJPushDownModule][OnMJPushDownChatWordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            HuRoom room = g.huRoomMgr.GetRoom(player.RoomUuid);
            if (room == null)
            {
                rsp.errorCode = MJPushDownChatWordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPushDownModule][OnMJPushDownChatWordReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = MJPushDownChatWordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPushDownChatWordNtf ntf = new MJPushDownChatWordNtf();
            ntf.roleId = accountId;
            ntf.chatWord = req.chatWord;
            room.SendRoomMsgEx(accountId, ntf);
        }
    }
}
