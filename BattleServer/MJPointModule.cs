﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Protocols.MajiangPoint;
using Utility.Debugger;
using BattleServer;
using System.Collections;
using Utility.Coroutines;
using StaticData;
using Event;
using Config;
using MahjongPoint;
using Protocols.Majiang;
using HelpAsset;
using HelpAsset.MahjongPoint;

namespace Module
{
    /// <summary>
    /// 抠点模块
    /// </summary>
    public class MJPointModule : LogicModule
    {
        public MJPointModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Implement
        protected override bool OnInit()
        {
            base.OnInit();

            LogSys.Info("LoginModule.OnInit()");

            //创建房间
            RegisterMsgHandler(MSGID.MJPointCreateRoomL2B, new MsgHandler<MJPointCreateRoomL2B>(OnMJPointCreateRoomL2B));

            //加入房间
            RegisterMsgHandler(MSGID.MJPointJoinRoomL2B, new MsgHandler<MJPointJoinRoomL2B>(OnMJPointJoinRoomL2B));

            //获取房间信息
            RegisterMsgHandler(MSGID.MJPointGetRoomInfoL2B, new MsgHandler<MJPointGetRoomInfoL2B>(OnMJPointGetRoomInfoL2B));

            //出牌请求
            RegisterMsgHandler(MSGID.MJPointHandOutCardReq, new MsgHandler<MJPointHandOutCardReq>(OnMJPointHandOutCardReq));

            //碰牌请求
            RegisterMsgHandler(MSGID.MJPointPengCardReq, new MsgHandler<MJPointPengCardReq>(OnMJPointPengCardReq));

            //胡牌请求
            RegisterMsgHandler(MSGID.MJPointHuCardReq, new MsgHandler<MJPointHuCardReq>(OnMJPointHuCardReq));

            //杠牌请求
            RegisterMsgHandler(MSGID.MJPointGangCardReq, new MsgHandler<MJPointGangCardReq>(OnMJPointGangCardReq));

            //听牌请求
            RegisterMsgHandler(MSGID.MJPointReadyCardReq, new MsgHandler<MJPointReadyCardReq>(OnMJPointReadyCardReq));

            //过
            RegisterMsgHandler(MSGID.MJPointPassReq, new MsgHandler<MJPointPassReq>(OnMJPointPassReq));

            // 下一步
            RegisterMsgHandler(MSGID.MJPointNextStepReq, new MsgHandler<MJPointNextStepReq>(OnMJPointNextStepReq));

            // (抠点麻将)解散游戏
            RegisterMsgHandler(MSGID.MJPointDismissGameReq, new MsgHandler<MJPointDismissGameReq>(OnMJPointDismissGameReq));

            // 解散是否同意
            RegisterMsgHandler(MSGID.MJPointDismissAgreeReq, new MsgHandler<MJPointDismissAgreeReq>(OnMJPointDismissAgreeReq));

            // 离开房间，房间未开始时
            RegisterMsgHandler(MSGID.MJPointLeaveRoomReq, new MsgHandler<MJPointLeaveRoomReq>(OnMJPointLeaveRoomReq));

            // 检测自摸胡，暗杠，补杠，听牌
            RegisterMsgHandler(MSGID.MJPointCheckSelfGangHuReadyReq, new MsgHandler<MJPointCheckSelfGangHuReadyReq>(OnMJPointCheckSelfGangHuReadyReq));

            // 聊天
            RegisterMsgHandler(MSGID.MJPointChatReq, new MsgHandler<MJPointChatReq>(OnMJPointChatReq));

            //文字聊天
            RegisterMsgHandler(MSGID.MJPointChatWordReq, new MsgHandler<MJPointChatWordReq>(OnMJPointChatWordReq));

            return true;
        }

        protected override void OnDestroy()
        {
            LogSys.Info("LoginModule.OnDestroy()");

            base.OnDestroy();
        }

        //////////////////////////////////////////////////////////////////////////
        // Message Handlers
        /// <summary>
        /// 创建房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointCreateRoomL2B(UInt32 from, UInt64 accountId, MJPointCreateRoomL2B req)
        {
            MJPointCreateRoomRsp rsp = new MJPointCreateRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointCreateRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            UInt64 roomuuid = req.roomUuid;
            player.RoomUuid = roomuuid;

            PointRoom room = new PointRoom(roomuuid);
            room.InitRoom(accountId, player.NickName, EGameType.MJPoint);
            room.config = req.config;
            AddRoleToRoom(player, room);

            g.pointRoomMgr.AddRoom(room);

            rsp.errorCode = MJPointCreateRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.SaveDb(true);
        }

        void AddRoleToRoom(BattlePlayer player, PointRoom room)
        {
            PointRoomRole role = new PointRoomRole();
            role.Init(player.AccountId, player.NickName, room);
            role.direction = room.GetOneSeat();
            role.sex = player.sex;
            role.level = player.level;
            role.roleState = ERoomRoleState.WaitRoomer;
            role.weixinHeadImgUrl = player.weixinHeadImgUrl;
            room.AddRole(role);
        }

        /// <summary>
        /// 加入房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointJoinRoomL2B(UInt32 from, UInt64 accountId, MJPointJoinRoomL2B req)
        {
            MJPointJoinRoomRsp rsp = new MJPointJoinRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomL2B],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = await g.pointRoomMgr.FindRoomAsync(req.roomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointJoinRoomRsp.ErrorID.RoomNotExist;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomL2B],房间不存在,accountId={0}", accountId);
                return;
            }

            if (room.IsRoomFull())
            {
                rsp.errorCode = MJPointJoinRoomRsp.ErrorID.RoomFull;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointJoinRoomL2B],房间已满,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;
            AddRoleToRoom(player, room);

            rsp.errorCode = MJPointJoinRoomRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            // 通知其他人
            MJPointJoinRoomNtf ntf = new MJPointJoinRoomNtf();
            ntf.remoteRoleInfo = room.GetRole(accountId).remoteInfo;
            room.SendRoomMsgEx(accountId, ntf);

            // 房间人数已满，回合开始
            if (room.IsRoomFull())
            {
                room.FirstInit();
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 获取房间信息
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        async void OnMJPointGetRoomInfoL2B(UInt32 from, UInt64 accountId, MJPointGetRoomInfoL2B req)
        {
            MJPointGetRoomInfoRsp rsp = new MJPointGetRoomInfoRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointGetRoomInfoReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            player.RoomUuid = req.roomUuid;

            PointRoom room = await g.pointRoomMgr.FindRoomAsync(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointGetRoomInfoReq],房间不存在,accountId={0}", accountId);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointGetRoomInfoRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointGetRoomInfoReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            rsp.errorCode = MJPointGetRoomInfoRsp.ErrorID.Success;
            rsp.roomInfo = room.GetRoomInfo(accountId);
            player.SendMsgToClient(rsp);

            room.CheckDismissTimer();
        }

        /// <summary>
        /// 出牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointHandOutCardReq(UInt32 from, UInt64 accountId, MJPointHandOutCardReq req)
        {
            MJPointHandOutCardRsp rsp = new MJPointHandOutCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointHandOutCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHandOutCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHandOutCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!role.currentRound.HasHandCard(req.cardid))
            {
                rsp.errorCode = MJPointHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHandOutCardReq],手牌中没有该牌,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }

            // 不是出牌状态
            if (!role.IsHandingOutState)
            {
                rsp.errorCode = MJPointHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHandOutCardReq],不是出牌状态,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }

            stCheckRoomItem roomitem = new stCheckRoomItem();
            stCheckRoleItem roleitem = new stCheckRoleItem();
            role.GetCheckRoomRoleInfo(roomitem, roleitem);

            // 听口改变
            if (PointHelp.CheckReadyChange(req.cardid, roomitem, roleitem))
            {
                rsp.errorCode = MJPointHandOutCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHandOutCardReq],听口改变,accountId={0},cardid={1}", accountId, req.cardid);
                return;
            }

            HandleHandOutCard(accountId, room, role, req.cardid);

            rsp.errorCode = MJPointHandOutCardRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            room.SaveDb();
        }

        /// <summary>
        /// 出牌
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="room"></param>
        /// <param name="role"></param>
        /// <param name="cardid"></param>
        /// <param name="bReadyOut">是否是听牌出牌</param>
        void HandleHandOutCard(UInt64 accountId, PointRoom room, PointRoomRole role, UInt32 cardid, bool bReadyOut = false)
        {
            // 过胡后，直至玩家打出一张牌，才可以正常胡牌
            role.currentRound.bPassHu = false;
            role.bRobGangHu = false;

            // 记录出牌信息
            room.currentRound.lastHandOutCard.Clear();
            room.currentRound.lastHandOutCard.RecordHandOutCard(accountId, cardid);

            // 从手牌中删除该牌
            role.currentRound.RemoveHandCard(cardid);
            role.currentRound.AddPoolCard(cardid);

            // 通知其他人自己出牌
            MJPointHandOutCardNtf ntf = new MJPointHandOutCardNtf();
            ntf.cardRoleId = accountId;
            ntf.cardid = cardid;
            ntf.bReadyOut = bReadyOut;
            room.SendRoomMsgEx(accountId, ntf);

            role.currentRound.playState = ERolePlayState.Wait;

            // 记录听牌时出的这张牌
            if (bReadyOut)
            {
                role.currentRound.readyOutCardId = cardid;

                // 下一个人起牌
                role.nextRole.DrawNewCard();

                return;
            }

            // 检测其他人碰杠胡
            List<PointRoomRole> lstOthers = room.NextRoles(role);

            // 检测胡
            foreach (var other in lstOthers)
            {
                EHuCardType huType = EHuCardType.Null;
                if (other.CanHuOther(cardid, ref huType))
                {
                    stOutCardReadyItem readyItem = new stOutCardReadyItem();
                    readyItem.roleid = other.roleid;
                    readyItem.huType = huType;
                    readyItem.bSelfTouch = false;

                    readyItem.readyInfo.cardRoleId = accountId;
                    readyItem.readyInfo.cardid = cardid;
                    readyItem.readyInfo.bHuPai = true;
                    room.currentRound.lastHandOutCard.AddReadyItem(readyItem);
                }
            }

            // 检测碰
            foreach (var other in lstOthers)
            {
                if (other.CanPeng(cardid))
                {
                    stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();
                    readyInfo.bPeng = true;
                    readyInfo.cardRoleId = accountId;
                    readyInfo.cardid = cardid;
                    room.currentRound.lastHandOutCard.AddReadyItem(other.roleid, readyInfo);
                }
            }

            // 检测明杠
            foreach (var other in lstOthers)
            {
                if (other.CanMingGang(cardid))
                {
                    stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();
                    readyInfo.gangType = EGangCardType.MingGang;
                    readyInfo.cardRoleId = accountId;
                    readyInfo.cardid = cardid;
                    room.currentRound.lastHandOutCard.AddReadyItem(other.roleid, readyInfo);
                }
            }

            if (room.currentRound.lastHandOutCard.HasReadyItem())
            {
                stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                PointRoomRole readyRole = room.GetRole(item.roleid);
                readyRole.NotifyPengGangHuReady(item);
            }
            else
            {
                role.nextRole.DrawNewCard();
            }
        }

        string ToString(List<UInt32> lstCards)
        {
            string temp = "";
            foreach (var cardid in lstCards)
            {
                temp += cardid.ToString();
                temp += ",";
            }
            return temp;
        }

        /// <summary>
        /// 碰牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointPengCardReq(UInt32 from, UInt64 accountId, MJPointPengCardReq req)
        {
            MJPointPengCardRsp rsp = new MJPointPengCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointPengCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPengCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPengCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            // 不是碰牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPointPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPengCardReq],不是碰牌状态,accountId={0}", accountId);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if (!readyItem.readyInfo.bPeng)
            {
                rsp.errorCode = MJPointPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPengCardReq],玩家不能碰，却发碰牌消息,accountId={0}", accountId);
                return;
            }

            // 出牌人
            PointRoomRole handRole = room.GetRole(readyItem.readyInfo.cardRoleId);
            if (null == handRole)
            {
                rsp.errorCode = MJPointPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPengCardReq],出牌人不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, readyItem.readyInfo.cardRoleId);
                return;
            }

            if (!role.CanPeng(readyItem.readyInfo.cardid))
            {
                rsp.errorCode = MJPointPengCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPengCardReq],不能碰,accountId={0}, lastHandOutCardRole={1}", accountId, readyItem.readyInfo.cardRoleId);
                return;
            }

            // 从出牌人池牌中删除
            handRole.currentRound.RemovePoolCard(readyItem.readyInfo.cardid);

            // 把牌添加到碰牌者的明牌中
            EMJCardType cardType = MJCardHelp.GetMJType(readyItem.readyInfo.cardid);
            List<UInt32> lstPengCards = role.GetPengCards(cardType);
            role.currentRound.RemoveHandCards(lstPengCards);
            role.AddPengCards(readyItem.readyInfo.cardid, lstPengCards);

            rsp.errorCode = MJPointPengCardRsp.ErrorID.Success;
            rsp.cardRoleId = readyItem.readyInfo.cardRoleId;
            rsp.cardid = readyItem.readyInfo.cardid;
            rsp.lstPengCards = lstPengCards;
            player.SendMsgToClient(rsp);

            // 通知其他人碰牌
            MJPointPengCardNtf ntf = new MJPointPengCardNtf();
            ntf.cardRoleId = readyItem.readyInfo.cardRoleId;
            ntf.cardid = readyItem.readyInfo.cardid;
            ntf.pengRoleId = accountId;
            ntf.lstCards = lstPengCards;
            room.SendRoomMsgEx(accountId, ntf);

            role.currentRound.playState = ERolePlayState.Wait;

            // 碰牌也算起新牌,需要客户端检测听牌暗杠
            role.CheckSelfGangHuReady(true);

            room.SaveDb();
        }
        
        /// <summary>
        /// 胡牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointHuCardReq(UInt32 from, UInt64 accountId, MJPointHuCardReq req)
        {
            MJPointHuCardRsp rsp = new MJPointHuCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointHuCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHuCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHuCardReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            // 不是胡牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPointHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHuCardReq],不是胡牌状态,accountId={0}", accountId);
                return;
            }

            // 未胡牌
            if (!role.currentRound.readyItem.readyInfo.bHuPai)
            {
                rsp.errorCode = MJPointHuCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointHuCardReq],玩家未胡牌,accountId={0}", accountId);
                return;
            }

            room.currentRound.huRoleId = accountId;

            stOutCardReadyItem readyItem = role.currentRound.readyItem;

            // 自摸胡
            if (readyItem.bSelfTouch)
            {
                rsp.errorCode = MJPointHuCardRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                MJPointHuCardNtf ntf = new MJPointHuCardNtf();
                ntf.bSelfTouch = true;
                ntf.huRoleId = accountId;
                room.SendRoomMsgEx(accountId, ntf);

                room.currentRound.RecordSettleItem(readyItem.huType, true, role, 0, role.currentRound.lastNewCardId, false);
            }
            else
            {
                if (role.bRobGangHu)
                {
                    PointRoomRole otherRole = room.GetRole(role.currentRound.readyItem.readyInfo.cardRoleId);
                    if (otherRole == null)
                    {
                        rsp.errorCode = MJPointHuCardRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[MJPushDownModule][OnMJPushDownHuCardReq],被抢杠胡玩家不在房间中,accountId={0}", accountId);
                        return;
                    }
                    RemoveCardInShowCard(otherRole, role.currentRound.readyItem.readyInfo.cardid);
                }

                rsp.errorCode = MJPointHuCardRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                MJPointHuCardNtf ntf = new MJPointHuCardNtf();
                ntf.bSelfTouch = false;
                ntf.cardRoleId = readyItem.readyInfo.cardRoleId;
                ntf.huRoleId = accountId;
                room.SendRoomMsgEx(accountId, ntf);

                bool bOtherBaoTing = room.GetRole(readyItem.readyInfo.cardRoleId).currentRound.AlreadyBaoTing;
                room.currentRound.RecordSettleItem(readyItem.huType, false, role, readyItem.readyInfo.cardRoleId, readyItem.readyInfo.cardid, bOtherBaoTing);
            }

            // 玩家赢了一局
            MJPointWinOneRoundB2L winNtf = new MJPointWinOneRoundB2L();
            player.SendMsgToLobby(winNtf);

            room.Settle();

            room.SaveDb();
        }

        void RemoveCardInShowCard(PointRoomRole role, uint cardId)
        {
            for (int i = 0; i < role.currentRound.showCards.Count; i++)
            {
                if (role.currentRound.showCards[i].mingType == EMingCardType.MingGang)
                {
                    if (role.currentRound.showCards[i].showCards.Contains(cardId))
                    {
                        role.currentRound.showCards[i].showCards.Remove(cardId);
                        role.currentRound.showCards[i].mingType = EMingCardType.Peng;
                    }
                }
            }
        }

        /// <summary>
        /// 杠牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointGangCardReq(UInt32 from, UInt64 accountId, MJPointGangCardReq req)
        {
            MJPointGangCardRsp rsp = new MJPointGangCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointGangCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointGangCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointGangCardReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是杠牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPointGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointGangCardReq],不是杠牌状态,accountId={0}", accountId);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if (readyItem.readyInfo.gangType == EGangCardType.Null)
            {
                rsp.errorCode = MJPointGangCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointGangCardReq],玩家不能杠牌,accountId={0}", accountId);
                return;
            }

            // 暗杠
            if (readyItem.readyInfo.gangType == EGangCardType.AnGang)
            {
                HandleAnGang(room, role, readyItem.readyInfo.cardid);
            }
            else if (readyItem.readyInfo.gangType == EGangCardType.BuGang)
            {
                HandleBuGang(room, role, readyItem.readyInfo.cardid);
            }
            else if (readyItem.readyInfo.gangType == EGangCardType.MingGang)
            {
                HandleMingGang(room, role);
            }

            role.currentRound.playState = ERolePlayState.Wait;

            if (room.currentRound.lastHandOutCard.HasReadyItem())
            {
                stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                PointRoomRole readyRole = room.GetRole(item.roleid);
                readyRole.NotifyPengGangHuReady(item);
            }
            else
            {
                // 通知当前玩家起牌
                role.DrawNewCard();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 处理明杠
        /// </summary>
        /// <param name="room"></param>
        /// <param name="cardRole"></param>
        /// <param name="self"></param>
        void HandleMingGang(PointRoom room, PointRoomRole self)
        {
            UInt32 cardid = room.currentRound.lastHandOutCard.cardid;
            UInt64 cardRoleId = room.currentRound.lastHandOutCard.roleid;
            PointRoomRole handRole = room.GetRole(cardRoleId);

            EMJCardType type = MJCardHelp.GetMJType(cardid);
            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);
            handRole.currentRound.RemovePoolCard(cardid);

            self.AddMingGangCards(cardid, lstCards);

            MJPointGangCardRsp rsp = new MJPointGangCardRsp();
            rsp.errorCode = MJPointGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.MingGang;
            rsp.cardRoleId = cardRoleId;
            rsp.cardid = cardid;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJPointGangCardNtf ntf = new MJPointGangCardNtf();
            ntf.gangType = EGangCardType.MingGang;
            ntf.loseRoleId = cardRoleId;
            ntf.winRoleId = self.roleid;
            ntf.cardid = cardid;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 记录结算条目
            room.currentRound.RecordSettleItem(EGangCardType.MingGang, false, self.roleid, cardRoleId, cardid, handRole.currentRound.AlreadyBaoTing);
        }

        /// <summary>
        /// 处理暗杠
        /// </summary>
        /// <param name="room"></param>
        /// <param name="self"></param>
        /// <param name="cardid"></param>
        void HandleAnGang(PointRoom room, PointRoomRole self, UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);

            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);

            self.AddAnGangCards(lstCards);

            MJPointGangCardRsp rsp = new MJPointGangCardRsp();
            rsp.errorCode = MJPointGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.AnGang;
            rsp.cardRoleId = 0;
            rsp.cardid = 0;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJPointGangCardNtf ntf = new MJPointGangCardNtf();
            ntf.gangType = EGangCardType.AnGang;
            ntf.loseRoleId = 0;
            ntf.winRoleId = self.roleid;
            ntf.cardid = 0;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 记录结算条目
            room.currentRound.RecordSettleItem(EGangCardType.AnGang, true, self.roleid, 0, cardid, false);
        }

        /// <summary>
        /// 处理补杠
        /// </summary>
        void HandleBuGang(PointRoom room, PointRoomRole self, UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);
            List<UInt32> lstCards = self.GetGangCards(type);
            self.currentRound.RemoveHandCards(lstCards);

            self.AddBuGangCards(lstCards);

            MJPointGangCardRsp rsp = new MJPointGangCardRsp();
            rsp.errorCode = MJPointGangCardRsp.ErrorID.Success;
            rsp.gangType = EGangCardType.BuGang;
            rsp.cardRoleId = 0;
            rsp.cardid = 0;
            rsp.lstCards = lstCards;
            self.SendMsgToClient(rsp);

            MJPointGangCardNtf ntf = new MJPointGangCardNtf();
            ntf.gangType = EGangCardType.BuGang;
            ntf.loseRoleId = 0;
            ntf.winRoleId = self.roleid;
            ntf.cardid = 0;
            ntf.lstCards = lstCards;
            room.SendRoomMsgEx(self.roleid, ntf);

            // 检测其他人胡
            List<PointRoomRole> lstOthers = room.NextRoles(self);

            // 检测胡
            foreach (var other in lstOthers)
            {
                stOutCardReadyItem readyItem = new stOutCardReadyItem();
                EHuCardType huType = EHuCardType.Null;
                other.bRobGangHu = true;
                if (other.CanHuOther(cardid, ref huType))
                {
                    readyItem.roleid = other.roleid;
                    readyItem.huType = huType;
                    readyItem.bSelfTouch = false;

                    readyItem.readyInfo.cardRoleId = self.roleid;
                    readyItem.readyInfo.cardid = cardid;
                    readyItem.readyInfo.bHuPai = true;
                    room.currentRound.lastHandOutCard.AddReadyItem(readyItem);
                }
            }

            if (!room.currentRound.lastHandOutCard.HasReadyItem())
            {
                // 记录结算条目
                room.currentRound.RecordSettleItem(EGangCardType.BuGang, true, self.roleid, 0, cardid, false);
            }
        }

        /// <summary>
        /// 听牌请求
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointReadyCardReq(UInt32 from, UInt64 accountId, MJPointReadyCardReq req)
        {
            MJPointReadyCardRsp rsp = new MJPointReadyCardRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],房间不存在,accountId={0}", accountId);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是听牌状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPointReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],不是听牌状态,accountId={0}", accountId);
                return;
            }

            // 已经报听，不能再报听
            if (role.currentRound.AlreadyBaoTing)
            {
                rsp.errorCode = MJPointReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],玩家已经听牌,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            stOutCardReadyItem readyItem = role.currentRound.readyItem;
            if (!readyItem.readyInfo.bReadyCard)
            {
                rsp.errorCode = MJPointReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],玩家未听牌,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            if (!readyItem.CanReadyCard(req.cardid))
            {
                rsp.errorCode = MJPointReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],出该牌不能听,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            HandleHandOutCard(accountId, room, role, req.cardid, true);

            readyItem.GetReadyTypes(req.cardid, role.currentRound.lstReadyTypes);
            if (role.currentRound.lstReadyTypes.Count == 0)
            {
                rsp.errorCode = MJPointReadyCardRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointReadyCardReq],听牌异常错误,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            role.currentRound.playState = ERolePlayState.Wait;

            player.SendMsgToClient(rsp);

            MJPointReadyCardNtf ntf = new MJPointReadyCardNtf();
            ntf.readyRoleId = accountId;
            room.SendRoomMsgEx(accountId, ntf);

            room.SaveDb();
        }

        /// <summary>
        /// 过
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointPassReq(UInt32 from, UInt64 accountId, MJPointPassReq req)
        {
            MJPointPassRsp rsp = new MJPointPassRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointPassReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPassReq],房间不存在,accountId={0}", accountId);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPassReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            // 不是碰杠胡听状态
            if (!role.IsPengGangHuReadyState)
            {
                rsp.errorCode = MJPointPassRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointPassReq],不是碰杠胡听状态,accountId={0}", accountId);
                return;
            }

            role.currentRound.playState = ERolePlayState.Wait;

            rsp.errorCode = MJPointPassRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            stOutCardReadyItem readyItem = role.currentRound.readyItem;

            // 过胡后一圈内不能再胡牌
            if (readyItem.readyInfo.bHuPai)
            {
                if (!readyItem.bSelfTouch)
                {
                    // 过胡设置为true
                    role.currentRound.bPassHu = true;
                }
            }

            if (IsSelfHandCard(role.currentRound.readyItem))
            {
                // 该你出牌了
                role.NotifyNeedHandOutCard();
            }
            else
            {
                if (room.currentRound.lastHandOutCard.HasReadyItem())
                {
                    stOutCardReadyItem item = room.currentRound.lastHandOutCard.FirstReadyItem;
                    PointRoomRole readyRole = room.GetRole(item.roleid);
                    readyRole.NotifyPengGangHuReady(item);
                }
                else
                {
                    // 出牌人
                    PointRoomRole handRole = room.GetRole(room.currentRound.lastHandOutCard.roleid);
                    if (null == handRole)
                    {
                        rsp.errorCode = MJPointPassRsp.ErrorID.Failed;
                        player.SendMsgToClient(rsp);

                        LogSys.Warn("[MJPointModule][OnMJPointPassReq],出牌人不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                        return;
                    }

                    if (role.bRobGangHu)
                    {
                        room.currentRound.RecordSettleItem(EGangCardType.BuGang, true, room.currentRound.lastHandOutCard.roleid, 0, room.currentRound.lastHandOutCard.cardid, false);
                    }

                    // 下个人起牌
                    handRole.nextRole.DrawNewCard();
                }
            }

            room.SaveDb();
        }

        bool IsSelfHandCard(stOutCardReadyItem readyItem)
        {
            stPengGangHuReadyInfo readyInfo = readyItem.readyInfo;

            // 暗杠和补杠
            if (readyInfo.gangType == EGangCardType.AnGang || readyInfo.gangType == EGangCardType.BuGang)
            {
                return true;
            }

            // 自摸
            if (readyInfo.bHuPai)
            {
                if (readyItem.bSelfTouch)
                {
                    return true;
                }
            }

            if (readyInfo.bReadyCard)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 抠点麻将 下一步
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointNextStepReq(UInt32 from, UInt64 accountId, MJPointNextStepReq req)
        {
            MJPointNextStepRsp rsp = new MJPointNextStepRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointNextStepReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointNextStepReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            if (room.roomState != ERoomState.WaitRound)
            {
                rsp.errorCode = MJPointNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointNextStepReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointNextStepReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (role.roleState != ERoomRoleState.Settle)
            {
                rsp.errorCode = MJPointNextStepRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointNextStepReq],玩家状态不对，不在结算状态,accountId={0}", accountId);
                return;
            }

            role.roleState = ERoomRoleState.WaitRound;

            rsp.errorCode = MJPointNextStepRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPointNextStepNtf ntf = new MJPointNextStepNtf();
            ntf.roleid = accountId;
            room.SendRoomMsgEx(accountId, ntf);

            // 所有玩家都过了结算界面
            if (room.IsAllRoleWaitRound())
            {
                room.StartNewRound();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 检测暗杠，补杠，自摸胡，听牌
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointCheckSelfGangHuReadyReq(UInt32 from, UInt64 accountId, MJPointCheckSelfGangHuReadyReq req)
        {
            LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],该消息不再使用,accountId={0}", accountId);

            /*
            MJPointCheckSelfGangHuReadyRsp rsp = new MJPointCheckSelfGangHuReadyRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointCheckSelfGangHuReadyRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointCheckSelfGangHuReadyRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],玩家不在房间中,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            if (!role.IsNeedCheckSelfPengGangHuReadyState)
            {
                rsp.errorCode = MJPointCheckSelfGangHuReadyRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],玩家状态不对，不在检测听牌状态,accountId={0}, lastHandOutCardRole={1}", accountId, room.currentRound.lastHandOutCard.roleid);
                return;
            }

            bool bCheck = req.checkItem.bAnGang || req.checkItem.bBuGang || req.checkItem.bHuCard || req.checkItem.bReadyCard;
            if (!bCheck)
            {
                rsp.errorCode = MJPointCheckSelfGangHuReadyRsp.ErrorID.Success;
                player.SendMsgToClient(rsp);

                role.NotifyNeedHandOutCard();

                room.SaveDb();
                return;
            }

            stOutCardReadyItem readyItem = new stOutCardReadyItem();
            readyItem.roleid = accountId;
            readyItem.bSelfTouch = true;

            stPengGangHuReadyInfo readyInfo = readyItem.readyInfo;
            readyInfo.cardRoleId = accountId;

            stCheckRoomItem roomitem = new stCheckRoomItem();
            stCheckRoleItem roleitem = new stCheckRoleItem();
            role.GetCheckRoomRoleInfo(roomitem, roleitem);

            List<UInt32> lstReadyCards = new List<UInt32>();

            if (req.checkItem.bReadyCard)
            {
                if (role.currentRound.checkItem.bReadyCard && PointHelp.CanReadyCard(lstReadyCards, roomitem, roleitem))
                {
                    // 是否听牌
                    readyInfo.bReadyCard = true;
                    readyInfo.lstReadyCards = role.GetReadyInfos(lstReadyCards);
                }
                else
                {
                    LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],客户端计算能听牌，服务器却不能听牌,accountId={0}", accountId);
                }
            }

            if (req.checkItem.bHuCard)
            {
                if (role.currentRound.checkItem.bHuCard && PointHelp.CanHuSelf(role.currentRound.lastNewCardId, ref readyItem.huType, roomitem, roleitem))
                {
                    readyInfo.bHuPai = true;
                }
                else
                {
                    LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],客户端计算能胡牌，服务器却不能胡牌,accountId={0}", accountId);
                }
            }


            if (req.checkItem.bAnGang)
            {
                if (role.currentRound.checkItem.bAnGang && PointHelp.CanAnGang(roomitem, roleitem))
                {
                    EMJCardType type = role.currentRound.vecHandCards.GetAnGangType();
                    UInt32 outcardid = role.currentRound.GetHandCardByType(type);

                    readyInfo.gangType = EGangCardType.AnGang;
                    readyInfo.cardid = outcardid;
                }
                else
                {
                    LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],客户端计算能暗杠，服务器却不能暗杠,accountId={0}", accountId);
                }
            }

            if (req.checkItem.bBuGang)
            {
                if (role.currentRound.checkItem.bBuGang && PointHelp.CanBuGang(roomitem, roleitem))
                {
                    // 判断补杠
                    UInt32 outcardid = PointHelp.GetBuGangCardId(roomitem, roleitem);
                    if (outcardid != 0)
                    {
                        readyInfo.gangType = EGangCardType.BuGang;
                        readyInfo.cardid = outcardid;
                    }
                }
                else
                {
                    LogSys.Warn("[MJPointModule][OnMJPointCheckSelfGangHuReadyReq],客户端计算能补杠，服务器却不能补杠,accountId={0}", accountId);
                }
            }

            rsp.errorCode = MJPointCheckSelfGangHuReadyRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            if (role.IsPengGangHuReady(readyItem))
            {
                role.NotifyPengGangHuReady(readyItem);
            }
            else
            {
                role.NotifyNeedHandOutCard();
            }

            room.SaveDb();
            */
        }

        /// <summary>
        /// (抠点麻将)解散游戏
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointDismissGameReq(UInt32 from, UInt64 accountId, MJPointDismissGameReq req)
        {
            MJPointDismissGameRsp rsp = new MJPointDismissGameRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointDismissGameReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointDismissGameReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointDismissGameReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = MJPointDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointDismissGameReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            if (room.dismissInfo.bStart)
            {
                rsp.errorCode = MJPointDismissGameRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointDismissGameReq],上次解散流程未走完,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            // 重新开始解散流程
            room.dismissInfo.Clear();
            room.dismissInfo.bStart = true;
            room.dismissInfo.expireTime = env.Timer.CurrentDateTime.AddMinutes(3);
            room.dismissInfo.firstRoleId = accountId;
            room.dismissInfo.setAgreeRoles.Add(accountId);

            rsp.errorCode = MJPointDismissGameRsp.ErrorID.Success;
            rsp.remainSecond = room.dismissRemainSecond;
            player.SendMsgToClient(rsp);

            MJPointDismissGameNtf ntf = new MJPointDismissGameNtf();
            ntf.roleid = role.roleid;
            ntf.remainSecond = room.dismissRemainSecond;
            room.SendRoomMsgEx(role.roleid, ntf);

            room.SaveDb();
        }

        /// <summary>
        /// 解散是否同意
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointDismissAgreeReq(UInt32 from, UInt64 accountId, MJPointDismissAgreeReq req)
        {
            MJPointDismissAgreeRsp rsp = new MJPointDismissAgreeRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointDismissAgreeReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointDismissAgreeReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointDismissAgreeReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (!room.IsInCanDismissState)
            {
                rsp.errorCode = MJPointDismissAgreeRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointDismissAgreeReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = MJPointDismissAgreeRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            // 解散流程已结束
            if (room.dismissInfo.firstRoleId == 0)
            {
                return;
            }

            MJPointDismissAgreeNtf ntf = new MJPointDismissAgreeNtf();
            ntf.roleid = accountId;
            ntf.bAgree = req.bAgree;
            room.SendRoomMsgEx(accountId, ntf);

            if(req.bAgree)
            {
                room.dismissInfo.setAgreeRoles.Add(accountId);
            }
            else
            {
                room.dismissInfo.setNotRoles.Add(accountId);
            }

            // 解散房间成功
            if (room.dismissInfo.setAgreeRoles.Count >= (PointRoom.MAX_ROLES_COUNT - 1))
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
                return;
            }

            // 解散房间失败
            if (room.dismissInfo.setNotRoles.Count >= 1)
            {
                MJPointDismissGameSuccessNtf successNtf = new MJPointDismissGameSuccessNtf();
                successNtf.bSuccess = false;
                room.SendRoomMsg(successNtf);

                room.dismissInfo.Clear();
            }

            room.SaveDb();
        }

        /// <summary>
        /// 离开房间
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointLeaveRoomReq(UInt32 from, UInt64 accountId, MJPointLeaveRoomReq req)
        {
            MJPointLeaveRoomRsp rsp = new MJPointLeaveRoomRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointLeaveRoomReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointLeaveRoomReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            PointRoomRole role = room.GetRole(accountId);
            if (null == role)
            {
                rsp.errorCode = MJPointLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointLeaveRoomReq],玩家不在房间中,accountId={0}", accountId);
                return;
            }

            if (room.roomState != ERoomState.WaitRoomer)
            {
                rsp.errorCode = MJPointLeaveRoomRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointLeaveRoomReq],房间状态不对,accountId={0}, roomState", accountId, room.roomState);
                return;
            }

            rsp.errorCode = MJPointLeaveRoomRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPointLeaveRoomNtf ntf = new MJPointLeaveRoomNtf();
            ntf.roleid = accountId;

            // 不是房主
            if (accountId != room.roomOwner.roleid)
            {
                room.SendRoomMsgEx(accountId, ntf);

                // 解绑战斗服
                player.UnBindBattleFromGate();

                // 通知lobby离开房间
                MJPointLeaveRoomB2L ntfLobby = new MJPointLeaveRoomB2L();
                ntfLobby.roomUuid = room.roomUuid;
                player.SendMsgToLobby(ntfLobby);

                g.playerMgr.RemovePlayer(role.roleid);

                room.RemoveRole(accountId);

                room.SaveDb();

                return;
            }

            ntf.bRoomOwner = true;
            room.SendRoomMsgEx(accountId, ntf);

            room.RoomClose();
        }

        /// <summary>
        /// 聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointChatReq(UInt32 from, UInt64 accountId, MJPointChatReq req)
        {
            MJPointChatRsp rsp = new MJPointChatRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (null == player)
            {
                LogSys.Warn("[MJPointModule][OnMJPointChatReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (null == room)
            {
                rsp.errorCode = MJPointChatRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointChatReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = MJPointChatRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPointChatNtf ntf = new MJPointChatNtf();
            ntf.roleid = accountId;
            ntf.chatid = req.chatid;
            room.SendRoomMsgEx(accountId, ntf);
        }

        /// <summary>
        /// 用户输入文字聊天
        /// </summary>
        /// <param name="from"></param>
        /// <param name="accountId"></param>
        /// <param name="req"></param>
        void OnMJPointChatWordReq(UInt32 from, UInt64 accountId, MJPointChatWordReq req)
        {
            MJPointChatWordRsp rsp = new MJPointChatWordRsp();

            BattlePlayer player = g.playerMgr.FindPlayer(accountId);
            if (player == null)
            {
                LogSys.Warn("[MJPointModule][OnMJPointChatWordReq],找不到玩家,accountId={0}", accountId);
                return;
            }

            PointRoom room = g.pointRoomMgr.GetRoom(player.RoomUuid);
            if (room == null)
            {
                rsp.errorCode = MJPointChatWordRsp.ErrorID.Failed;
                player.SendMsgToClient(rsp);

                LogSys.Warn("[MJPointModule][OnMJPointChatWordReq],房间不存在,accountId={0}, roomUuid={1}", accountId, player.RoomUuid);
                return;
            }

            rsp.errorCode = MJPointChatWordRsp.ErrorID.Success;
            player.SendMsgToClient(rsp);

            MJPointChatWordNtf ntf = new MJPointChatWordNtf();
            ntf.roleId = accountId;
            ntf.chatWord = req.chatWord;
            room.SendRoomMsgEx(accountId, ntf);
        }
    }
}
