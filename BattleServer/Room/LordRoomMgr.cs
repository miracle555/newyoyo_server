﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using BattleServer;

namespace Landlord
{
    public class LordRoomMgr
    {
        public void AddRoom(LordRoom room)
        {
            if(HasRoom(room.roomUuid))
            {
                return;
            }

            mapRooms.Add(room.roomUuid, room);
        }

        public bool HasRoom(UInt64 roomuuid)
        {
            return mapRooms.ContainsKey(roomuuid);
        }

        public void RemoveRoom(UInt64 roomuuid)
        {
            mapRooms.Remove(roomuuid);
        }

        public void DeleteRoom(UInt64 roomuuid)
        {
            LordRoom room = GetRoom(roomuuid);
            if (null == room)
            {
                return;
            }

            mapRooms.Remove(roomuuid);

            room.DeleteFromDb();
        }

        public LordRoom GetRoom(UInt64 roomuuid)
        {
            if(mapRooms.ContainsKey(roomuuid))
            {
                return mapRooms[roomuuid];
            }

            return null;
        }

        public async Task<LordRoom> FindRoomAsync(UInt64 roomuuid)
        {
            LordRoom room = GetRoom(roomuuid);
            if (null != room)
            {
                return room;
            }

            room = new LordRoom(roomuuid);
            bool bResult = await room.LoadFromDbAsync();
            if (!bResult)
            {
                return null;
            }

            AddRoom(room);

            return room;
        }

        public void OnRoleOnline(BattlePlayer player)
        {
            LordRoom room = GetRoom(player.RoomUuid);
            if (null == room)
            {
                return;
            }
            room.OnRoleOnline(player);
        }

        public void OnRoleOffline(BattlePlayer player)
        {
            LordRoom room = GetRoom(player.RoomUuid);
            if (null == room)
            {
                return;
            }
            room.OnRoleOffline(player);
        }

        public void OnSecond()
        {
            List<LordRoom> lstDels = new List<LordRoom>();
            foreach (var item in mapRooms)
            {
                if (item.Value.CheckDismissTimer())
                {
                    lstDels.Add(item.Value);
                }
            }

            foreach (var room in lstDels)
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
            }
        }

        public bool Init()
        {
            env.Timer.AddTimer(1000, 0, false, OneSecondTimer, null);

            return true;
        }

        public void OneSecondTimer(int timerID, object param)
        {
            var lstRoomUuids = mapRooms.Keys.ToList();
            foreach (var roomuuid in lstRoomUuids)
            {
                var room = FindRoom(roomuuid);
                if (null != room)
                {
                    room.Tick();
                }
            }
        }

        public LordRoom FindRoom(UInt64 roomuuid)
        {
            if (mapRooms.ContainsKey(roomuuid))
            {
                return mapRooms[roomuuid];
            }

            return null;
        }

        //////////////////////////////////////////////////////////////////////////

        // 所有房间
        Dictionary<UInt64, LordRoom> mapRooms = new Dictionary<UInt64, LordRoom>();
    }
}
