﻿using BattleServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahjongYunCheng
{
    public class YunChengRoomMgr
    {
        public void AddRoom(YunChengRoom room)
        {
            if (HasRoom(room.roomUuid))
            {
                return;
            }

            mapRooms.Add(room.roomUuid, room);
        }

        public bool HasRoom(UInt64 roomuuid)
        {
            return mapRooms.ContainsKey(roomuuid);
        }

        public void RemoveRoom(UInt64 roomuuid)
        {
            mapRooms.Remove(roomuuid);
        }

        public void DeleteRoom(UInt64 roomuuid)
        {
            YunChengRoom room = GetRoom(roomuuid);
            if (null == room)
            {
                return;
            }

            mapRooms.Remove(roomuuid);

            room.DeleteFromDb();
        }

        public YunChengRoom GetRoom(UInt64 roomuuid)
        {
            if (mapRooms.ContainsKey(roomuuid))
            {
                return mapRooms[roomuuid];
            }

            return null;
        }

        public async Task<YunChengRoom> FindRoomAsync(UInt64 roomuuid)
        {
            YunChengRoom room = GetRoom(roomuuid);
            if (null != room)
            {
                return room;
            }

            room = new YunChengRoom(roomuuid);
            bool bResult = await room.LoadFromDbAsync();
            if (!bResult)
            {
                return null;
            }

            AddRoom(room);

            return room;
        }

        public void OnRoleOnline(BattlePlayer player)
        {
            YunChengRoom room = GetRoom(player.RoomUuid);
            if (null == room)
            {
                return;
            }
            room.OnRoleOnline(player);
        }

        public void OnRoleOffline(BattlePlayer player)
        {
            YunChengRoom room = GetRoom(player.RoomUuid);
            if (null == room)
            {
                return;
            }
            room.OnRoleOffline(player);
        }

        public void OnSecond()
        {
            List<YunChengRoom> lstDels = new List<YunChengRoom>();
            foreach (var item in mapRooms)
            {
                if (item.Value.CheckDismissTimer())
                {
                    lstDels.Add(item.Value);
                }
            }

            foreach (var room in lstDels)
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
            }
        }

        //////////////////////////////////////////////////////////////////////////

        // 所有房间
        Dictionary<UInt64, YunChengRoom> mapRooms = new Dictionary<UInt64, YunChengRoom>();
    }
}
