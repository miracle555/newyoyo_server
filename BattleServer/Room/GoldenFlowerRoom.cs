﻿using BattleServer;
using HelpAsset.FlowerHelp;
using ProtoBuf;
using Protocols;
using Protocols.GoldenFlower;
using Redis;
using ServerLib;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoldenFlower
{

    /// <summary>
    /// 看牌
    /// </summary>
    [ProtoContract]
    public class stRobShowCardItem
    {
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 是看牌
        /// </summary>
        [ProtoMember(2)]
        public bool bShow = false;
    }


    /// <summary>
    /// 回合信息
    /// </summary>
    [ProtoContract]
    public class RoomRoundInfo
    {
        /// <summary>
        /// 手牌
        /// </summary>
        [ProtoMember(1)]
        public List<UInt32> handCards = new List<UInt32>();

        /// <summary>
        /// 房主
        /// </summary>
        [ProtoMember(2)]
        public stRoomOwner roomOwner = new stRoomOwner();
        
        /// <summary>
        /// 看牌
        /// </summary>
        [ProtoMember(3)]
        public List<stRobShowCardItem> showCardList = new List<stRobShowCardItem>();
        

        /// <summary>
        /// 该回合最先出完牌的人
        /// </summary>
        [ProtoMember(4)]
        public UInt64 winRoleId = 0;

        /// <summary>
        /// 公共倍数
        /// </summary>
        [ProtoMember(5)]
        public stPublicScales publicScales = new stPublicScales();


        /// <summary>
        /// 剩余牌
        /// </summary>
        [ProtoMember(6)]
        public List<UInt32> bottomCards = new List<UInt32>();

        /// <summary>
        /// 上次出牌信息
        /// </summary>
        [ProtoMember(7)]
        public stOutCardItem lastOutCardItem = new stOutCardItem();

        /// <summary>
        /// 其他角色列表
        /// </summary>
        [ProtoMember(8)]
        public HashSet<UInt64> setNotCallLords = new HashSet<ulong>();

        /// <summary>
        /// 玩家加注信息
        /// </summary>
        [ProtoMember(9)]
        public int playerFen = 1;

        /// <summary>
        /// 筹码列表
        /// </summary>
        [ProtoMember(10)]
        public List<int> counterList = new List<int>();

        /// <summary>
        /// 下一局开始玩家ID
        /// </summary>
        [ProtoMember(11)]
        public UInt64 nextPlay = 0;

       
        public void Clear()
        {
            //bottomCards.Clear();
            //lordRoleId = 0;
           // bankerRoleId = 0;
            //winRoleId = 0;
            publicScales.Clear();
            //lstRobLordRoles.Clear();
            lastOutCardItem.Clear();
            setNotCallLords.Clear();
            counterList.Clear();            
           // playerFen =1;
        }

        //public void SetMaxMingCardScales(int scale)
        //{
        //    if (scale > publicScales.mingScales)
        //    {
        //        publicScales.mingScales = scale;
        //    }
        //}

        //public void AddRobRole(UInt64 roleid, bool bRob)
        //{
        //    stRobLordItem item = new stRobLordItem();
        //    item.roleid = roleid;
        //    item.bRob = bRob;
        //    lstRobLordRoles.Add(item);
        //}

        ///// <summary>
        ///// 抢地主倍数
        ///// </summary>
        //public int CalRobScales()
        //{
        //    if (lstRobLordRoles.Count == 0)
        //    {
        //        return 1;
        //    }

        //    int scales = 1;

        //    for (int i = 1; i < lstRobLordRoles.Count; i++)
        //    {
        //        stRobLordItem item = lstRobLordRoles[i];
        //        if (item.bRob)
        //        {
        //            scales *= 2;
        //        }
        //    }

        //    return scales;
        //}

        ///// <summary>
        ///// 抢地主结果
        ///// </summary>
        ///// <param name="outRoleId"></param>
        ///// <returns></returns>
        //public ERobLordResult RobLordResult(ref UInt64 outRoleId)
        //{
        //    if (lstRobLordRoles.Count < 3)
        //    {
        //        return ERobLordResult.Continue;
        //    }

        //    if (lstRobLordRoles.Count == 3)
        //    {
        //        stRobLordItem role1 = lstRobLordRoles[0];
        //        stRobLordItem role2 = lstRobLordRoles[1];
        //        stRobLordItem role3 = lstRobLordRoles[2];

        //        if (role2.bRob || role3.bRob)
        //        {
        //            return ERobLordResult.Continue;
        //        }
        //        else
        //        {
        //            outRoleId = role1.roleid;
        //            return ERobLordResult.Success;
        //        }
        //    }

        //    for (int i = lstRobLordRoles.Count - 1; i >= 0; i--)
        //    {
        //        stRobLordItem item = lstRobLordRoles[i];
        //        if (item.bRob)
        //        {
        //            outRoleId = item.roleid;
        //            return ERobLordResult.Success;
        //        }
        //    }

        //    return ERobLordResult.Failed;
        //}
    }

   [ProtoContract]
   public class GoldenFlowerRoom
   {
       public GoldenFlowerRoom(UInt64 uuid)
        {
            roomUuid = uuid;
        }

        

        public void InitRoom(UInt64 ownerid, string ownerName, EGameType game)
        {
            roomOwner.roleid = ownerid;
            roomOwner.nickName = ownerName;
            gameType = game;
            roomState = ERoomState.WaitRoomer;

            lstSeats.Add(ESeatPostion.First);
            lstSeats.Add(ESeatPostion.Second);
            lstSeats.Add(ESeatPostion.Third);
            lstSeats.Add(ESeatPostion.Fourth);
            lstSeats.Add(ESeatPostion.Fifth);
        }

        public void InitRoom2(UInt64 accountid,string ownerName, EGameType game)
        {
            roomOwner.falseFangzhu = accountid;
            gameType = game;
            roomState = ERoomState.WaitRoomer;

            lstSeats.Add(ESeatPostion.First);
            lstSeats.Add(ESeatPostion.Second);
            lstSeats.Add(ESeatPostion.Third);
            lstSeats.Add(ESeatPostion.Fourth);
            lstSeats.Add(ESeatPostion.Fifth);
        }

        [ProtoMember(1)]
        public UInt64 roomUuid { get; set; }

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(2)]
        public EGameType gameType { get; set; }

        /// <summary>
        /// 房间主人
        /// </summary>
        [ProtoMember(3)]
        public stRoomOwner roomOwner = new stRoomOwner();

        /// <summary>
        /// 房间人数
        /// </summary>
        [ProtoMember(4)]
        public List<GoldenFlowerRoomRole> lstRoles = new List<GoldenFlowerRoomRole>();

        /// <summary>
        /// 房间状态
        /// </summary>
        [ProtoMember(5)]
        public ERoomState roomState = ERoomState.WaitRoomer;

        /// <summary>
        /// 第几牌局
        /// </summary>
        [ProtoMember(6)]
        public UInt32 currentCardRound = 0;

        /// <summary>
        /// 房间配置
        /// </summary>
        [ProtoMember(7)]
        public RoomConfig config = new RoomConfig();

        /// <summary>
        /// 回合信息
        /// </summary>
        [ProtoMember(8)]
        public RoomRoundInfo currentRound = new RoomRoundInfo();

        /// <summary>
        /// 最终结算
        /// </summary>
        [ProtoMember(9)]
        public stFinalSettle finalSettle = new stFinalSettle();
       
        public const int MAX_ROLES_COUNT = 5;

        /// <summary>
        /// 出牌时间10秒
        /// </summary>
        public const int HANDOUT_CARD_SECOND = 10;

        /// <summary>
        /// 解散信息
        /// </summary>
        [ProtoMember(10)]
        public stDismissInfo dismissInfo = new stDismissInfo();

        /// <summary>
        /// 座位列表
        /// </summary>
        [ProtoMember(11)]
        public List<ESeatPostion> lstSeats = new List<ESeatPostion>();

        /// <summary>
        /// 准备列表
        /// </summary>
        [ProtoMember(12)]
        public List<UInt64> playerList = new List<UInt64>();

        ///伪房主
        [ProtoMember(13)]
        public UInt64 roomPlayerID = 0;


        /// <summary>
        /// 获得一个座位
        /// </summary>
        /// <returns></returns>
        public ESeatPostion GetOneSeat()
        {
            lstSeats.Sort();
            if (lstSeats.Count > 0)
            {
                ESeatPostion seat = lstSeats[0];
                lstSeats.Remove(seat);
                return seat;
            }
            return ESeatPostion.Null;
        }

        /// <summary>
        /// 归还座位
        /// </summary>
        /// <param name="seat"></param>
        public void ReturnSeat(ESeatPostion seat)
        {
            lstSeats.Add(seat);
        }
        public GoldenFlowerRoomInfo GetRoomInfo(UInt64 selfid)
        {
            GoldenFlowerRoomInfo info = new GoldenFlowerRoomInfo();
            info.roomUuid = roomUuid;
            info.roomState = roomState;
            foreach (var role in lstRoles)
            {
                if (selfid != role.roleid)
                {
                    info.lstRoles.Add(role.remoteInfo);
                }
            }

            info.localRoleInfo = GetRole(selfid).localInfo;
            info.localRoleInfo.remainSecond = dismissRemainSecond;
            info.roomFen = currentRound.publicScales.Scales;
            //第几牌局
            info.currentCardRound = currentCardRound;
            
            info.config = config;

            info.roomOwner = roomOwner;

            info.counterList = currentRound.counterList;
            // 房间底牌
            //info.h = currentRound.bottomCards;

            info.lastOutCardItem = currentRound.lastOutCardItem;

            info.dismissInfo = dismissInfo;

            info.roomPlayID = roomPlayerID;
            return info;
        }

        /// <summary>
        /// 是否可以解散
        /// </summary>
        public bool IsInCanDismissState
        {
            get
            {
                if (roomState == ERoomState.WaitRound)
                {
                    return true;
                }

                if (roomState == ERoomState.Play)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 通知总倍数变化
        /// </summary>
        public void RefreshAllRoleTotalScales()
        {
            foreach (var role in lstRoles)
            {
                role.NotifyTotalScales();
            }
        }

        public GoldenFlowerRoomRole GetRole(UInt64 roleid)
        {
            foreach (var role in lstRoles)
            {
                if (role.roleid == roleid)
                {
                    return role;
                }
            }

            return null;
        }



        //public GoldenFlowerRoomRole NextRole(GoldenFlowerRoomRole role)
        //{
        //    ESeatPostion position = role.position + 1;
        //    if (position == ESeatPostion.Max)
        //    {
        //        position = ESeatPostion.First;
        //    }

        //    return GetRole(position);
        //}
        //玩家上下线
        public void OnRoleOnline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = true;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);
        }

        public void OnRoleOffline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = false;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);

            if (IsAllRoleOffline)
            {
                g.goldenflowerRoomMgr.RemoveRoom(roomUuid);

                GoldenFlowerAllRoleOfflineB2L offlineNtf = new GoldenFlowerAllRoleOfflineB2L();
                offlineNtf.roomUuid = roomUuid;
                SendMsgToLobby(ntf);
            }
        }
        public void SendMsgToLobby(Protocol proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }
        /// <summary>
        /// 房间内所有人都下线了
        /// </summary>
        /// <returns></returns>
        bool IsAllRoleOffline
        {
            get
            {
                foreach (var role in lstRoles)
                {
                    BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                    if (player != null)
                    {
                        if (player.isOnline)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }



        /// <summary>
        /// 获取战绩
        /// </summary>
        /// <returns></returns>
        stBattleScoreItem GetBattleScoreItem()
        {
            stBattleScoreItem scoreItem = new stBattleScoreItem();
            scoreItem.dateTime = env.Timer.CurrentDateTime;
            scoreItem.roomUuid = roomUuid;
            scoreItem.gameType = EGameType.GoldenFlower;
            scoreItem.roomOwner = roomOwner;

            foreach (var item in finalSettle.mapScores)
            {
                GoldenFlowerRoomRole role = GetRole(item.Key);
                if (null == role)
                {
                    continue;
                }
                stBattleScoreRole scoreRole = new stBattleScoreRole();
                scoreRole.roleid = role.roleid;
                scoreRole.nickName = role.nickName;
                scoreRole.score = item.Value;
                scoreItem.lstRoles.Add(scoreRole);
            }
            return scoreItem;
        }

        /// <summary>
        /// 检测解散超时
        /// </summary>
        public bool CheckDismissTimer()
        {
            if (dismissInfo.bStart)
            {
                if (env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 解散倒计时
        /// </summary>
        public int dismissRemainSecond
        {
            get
            {
                if (dismissInfo.firstRoleId == 0)
                {
                    return 0;
                }

                if (env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return 0;
                }

                TimeSpan span = dismissInfo.expireTime - env.Timer.CurrentDateTime;
                return (int)span.TotalSeconds;
            }
        }

        public void NotifyDismissSuccess()
        {
            GoldenFlowerDismissGameSuccessNtf successNtf = new GoldenFlowerDismissGameSuccessNtf();           
            successNtf.bSuccess = true;
            foreach (var j in lstRoles)              
            {
                finalSettle.ChangeScore(j.roleid, GetRole(j.roleid).currentRound.scales);
            }
            successNtf.finalSettle = finalSettle;
            SendRoomMsg(successNtf);
        }

        /// <summary>
        /// 关闭房间
        /// </summary>
        public void RoomClose()
        {
            foreach (var role in lstRoles)
            {
                BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                if (null == player)
                {
                    continue;
                }

                // 解绑战斗服
                player.UnBindBattleFromGate();

                g.playerMgr.RemovePlayer(role.roleid);
            }

            // 通知lobby房间关闭
            GoldenFlowerRoomCloseB2L closeNtf = new GoldenFlowerRoomCloseB2L();
            closeNtf.roomUuid = roomUuid;
            if (roomOwner.roleid != 0)
            {
                closeNtf.roomOwnerId = roomOwner.roleid;
            }
            else
            {
                closeNtf.roomOwnerId = roomOwner.falseFangzhu;
            }
            if (roomState == ERoomState.WaitRoomer)
            {
                closeNtf.bReturnRoomCard = true;
            }
            closeNtf.bRecordBattleScore = isRecordBattleScore;
            closeNtf.scoreItem = GetBattleScoreItem();
            SendMsgToLobby(closeNtf);

            g.goldenflowerRoomMgr.DeleteRoom(roomUuid);

        }

        
        /// <summary>
        /// 是否记录战绩
        /// </summary>
        public bool isRecordBattleScore
        {
            get
            {
                return roomState != ERoomState.WaitRoomer;
            }
        }

        ///检测是否剩最后一位玩家赢
        ///
        public bool isWinplayer()
        {
            int i = 0;
            UInt64 playerID = 0;
            foreach (var role in lstRoles)
            {
                if (role.currentRound.currentState== ERoomPlayerState.NoShowCard|| role.currentRound.currentState == ERoomPlayerState.ShowCard)
                {
                    i++;
                    playerID = role.roleid;
                }       
             }

            if (i == 1)
            {
                GoldenFlowerWinNtf ntf = new GoldenFlowerWinNtf();
                int playId = lstRoles.IndexOf(GetRole(playerID));
                if (playId + 1 == lstRoles.Count)
                {
                    currentRound.nextPlay = lstRoles[0].roleid;
                }
                else
                {
                    currentRound.nextPlay = lstRoles[playId + 1].roleid;
                }
                ntf.playID = playerID;
                
                GoldenFlowerRoomRole role = GetRole(playerID);
                GoldenFlowerRoomInfo roomInfo = GetRoomInfo(playerID);
                role.currentRound.scales+= currentRound.publicScales.Scales;
                if ( FlowerHelp.Getpaixing(roomInfo.localRoleInfo.handCards)== ECardFormType.Leopard)
                {
                    role.currentRound.scales += ((int)config.pariMutuel* (lstRoles.Count-1));
                    GoldenFlowerPariMutuelNtf pariNtf = new GoldenFlowerPariMutuelNtf();
                    foreach (var rolei in lstRoles)
                    {
                        if (rolei.roleid==role.roleid)
                        {
                            continue;
                        }
                        rolei.currentRound.scales -= (int)config.pariMutuel;
                        pariNtf.fen.Add(rolei.currentRound.scales);
                        pariNtf.playerID.Add(rolei.roleid);
                    }
                    pariNtf.zha = true;
                    SendRoomMsgAll(pariNtf);
                }
                GoldenFlowerTotalScalesChangeNtf totalNtf = new GoldenFlowerTotalScalesChangeNtf();
                totalNtf.playerId = playerID;
                totalNtf.roomFen = role.currentRound.scales;
                totalNtf.totalScales = 0;
                SendRoomMsgAll(totalNtf);
                if (currentCardRound==config.roomRoundCount)
                {
                    foreach (var j in lstRoles)
                    {
                        finalSettle.ChangeScore(j.roleid, GetRole(j.roleid).currentRound.scales);
                    }

                    ntf.finallyInning =true;
                    settle(ntf);
                   
                }
                SendRoomMsg(ntf);
                foreach (var rolei in lstRoles)
                {
                    rolei.currentRound.playState = ERolePlayState.NextGame;
                    rolei.currentRound.prepare = false;
                    rolei.currentRound.old = rolei.currentRound.scales;
                }
                roomState = ERoomState.WaitRound;
               
                return true;
            }
            return false;
        }

        ///结算tongzhi
        public void settle()
        {
            GoldenFlowerRoundSettleNtf settleNtf = new GoldenFlowerRoundSettleNtf();
            foreach (var j in lstRoles)
            {
                settleNtf.player.Add(GetRole(j.roleid).remoteInfo.baseInfo);
                settleNtf.fen.Add(GetRole(j.roleid).currentRound.old);
                settleNtf.settle.ChangeScore(j.roleid, GetRole(j.roleid).currentRound.old);
            }
            foreach (var k in lstRoles)
            {
                k.SendMsgToClient(settleNtf);
            }           
          
        }
         ///结算tongzhi
        public void settle(GoldenFlowerWinNtf ntf)
        {
            GoldenFlowerRoundSettleNtf settleNtf = new GoldenFlowerRoundSettleNtf();
            foreach (var j in lstRoles)
            {
                settleNtf.player.Add(GetRole(j.roleid).remoteInfo.baseInfo);
                settleNtf.fen.Add(GetRole(j.roleid).currentRound.scales);
                settleNtf.settle.ChangeScore(j.roleid, GetRole(j.roleid).currentRound.scales);
            }
            foreach (var k in lstRoles)
            {
                k.SendMsgToClient(settleNtf);
            }
            SendRoomMsg(ntf);
            RoomClose();
        }




        /// <summary>
        /// 是否是最后一局
        /// </summary>
        /// <returns></returns>
        public bool IsLastRound()
        {
            if (config.roomRound == ERoomRound.EightRound)
            {
                return currentCardRound >= 8;
            }
            else if (config.roomRound == ERoomRound.SixteenRound)
            {
                return currentCardRound >= 16;
            }
            else if (config.roomRound == ERoomRound.TwentyfourRound)
            {
                return currentCardRound >= 24;
            }

            return true;
        }


        /// <summary>
        /// 发送消息到大厅服
        /// </summary>
        /// <param name="proto"></param>
        void SendMsgToLobby(ProtoBody proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }

        public void SendRoomMsg(ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {
                role.SendMsgToClient(msg);
            }
        }

        public void SendRoomMsgEx(UInt64 roleid, ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {
                if (role.roleid == roleid)
                {
                    continue;
                }
                role.SendMsgToClient(msg);
            }
        }

        public void SendRoomMsgAll( ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {                             
                role.SendMsgToClient(msg);
            }
        }

        public void SendRoomMsgAllNextPlay(UInt64 roleid)
        {
            GoldenFlowerNeedHandOutCardNtf ntf = new GoldenFlowerNeedHandOutCardNtf();
            int i=lstRoles.IndexOf(GetRole(roleid));
            bool b = false;
                for (int j = i; j <lstRoles.Count; j++)
                {
                  
                if (j+1==lstRoles.Count)
                {
                    b = true;
                    j = 0;
                    if (lstRoles[j].currentRound.currentState == ERoomPlayerState.NoShowCard || lstRoles[j].currentRound.currentState == ERoomPlayerState.ShowCard)
                    {
                        ntf.nextPlayerID = lstRoles[j].roleid;
                        break;
                    }
                }
                else
                {
                    if (b)
                    { 
                        if (lstRoles[j].currentRound.currentState == ERoomPlayerState.NoShowCard || lstRoles[j].currentRound.currentState == ERoomPlayerState.ShowCard)
                        {
                           ntf.nextPlayerID = lstRoles[j].roleid;
                           break;
                        }
                    }
                    else
                    {
                        if (lstRoles[j+1].currentRound.currentState == ERoomPlayerState.NoShowCard || lstRoles[j+1].currentRound.currentState == ERoomPlayerState.ShowCard)
                        {
                            ntf.nextPlayerID = lstRoles[j+1].roleid;
                            break;
                        }
                    }
                }
                  
                }                              
            foreach (var role in lstRoles)
            {
                if (ntf.nextPlayerID==role.roleid)
                {
                    role.currentRound.playState = ERolePlayState.Play;
                }
                else
                {
                    role.currentRound.playState = ERolePlayState.Wait;
                }              
                role.SendMsgToClient(ntf);
            }
        }

        public bool IsRoomFull()
        {
            return lstRoles.Count >= MAX_ROLES_COUNT;
        }

        public void AddRole(GoldenFlowerRoomRole role)
        {
            lstRoles.Add(role);
        }

        public void RemoveRole(UInt64 roleid)
        {
            GoldenFlowerRoomRole role = GetRole(roleid);

            ReturnSeat(role.position);

            lstRoles.Remove(role);
        }

      


        public string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.detail_flower_room_, roomUuid.ToString());
                return key;
            }
        }

        public bool SaveDb(bool bAlways = false)
        {
            byte[] data;
            if (!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;

            if (bAlways)
            {
                redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);
            }
            else
            {
                redis.db.StringSet(dbKey, data, null, When.Exists, CommandFlags.FireAndForget);
            }

            return true;
        }

        public bool DeleteFromDb()
        {
            RedisSystem redis = env.Redis;

            redis.db.KeyDelete(dbKey, CommandFlags.FireAndForget);

            return true;
        }


        public async Task<bool> LoadFromDbAsync()
        {
            RedisSystem redis = env.Redis;

            RedisValue value = await redis.db.StringGetAsync(dbKey);
            if (value.IsNullOrEmpty)
            {
                return false;
            }

            if (!this.SerializeFromBinary(value))
            {
                return false;
            }

            foreach (var role in lstRoles)
            {
                role.Init(this);
            }

            return true;
        }

        public void FirstInit()
        {
            foreach (var role in lstRoles)
            {
                finalSettle.InitScore(role.roleid);
            }
        }

        public void StartNewRound(UInt64 playID)
        {
            InitNewRound(playID);
            SetRoomState(ERoomState.Play);

            foreach (var role in lstRoles)
            {
                // 回合开始通知
                GoldenFlowerRoundStartNtf roundStartNtf = new GoldenFlowerRoundStartNtf();
                roundStartNtf.playerId = playID;
                roundStartNtf.roomInfo = GetRoomInfo(role.roleid);
                role.SendMsgToClient(roundStartNtf);
            }

            currentRound.nextPlay = 0;

        }


        void InitNewRound(UInt64 playID)
        {
            currentCardRound++;


            // UInt64 bankerId = currentRound.bankerRoleId;
                playerList.Clear();
                      
               foreach (var role in lstRoles)
                {
                    role.currentRound.Clear();
                    role.betOnNumber = 0;
                }
                currentRound.playerFen = (int)config.fold;
                currentRound.Clear();               
            List<UInt32> allCards = RandomCards();
            //bool b = true;
            //int count = 0;
            //每一个人3张牌

            //int i = 0;
            foreach (var role in lstRoles)
            {
                List<UInt32> lstCards = null;

                lstCards = Get3Cards(allCards);

                //if (i == 0)
                //{
                //    lstCards = Get3Cards(allCards);
                //    lstCards.Add(37);
                //    lstCards.Add(24);
                //    lstCards.Add(16);

                //}

                //if (i == 1)
                //{
                //    lstCards = Get3Cards(allCards);
                //    lstCards.Add(31);
                //    lstCards.Add(44);
                //    lstCards.Add(4);

                //}

                //if (i == 2)
                //{
                //    lstCards = Get3Cards(allCards);
                //    lstCards.Add(21);
                //    lstCards.Add(8);
                //    lstCards.Add(29);

                //}

                //if (i == 3)
                //{
                //    lstCards = Get3Cards(allCards);
                //    lstCards.Add(51);
                //    lstCards.Add(23);
                //    lstCards.Add(5);

                //}

                //if (i == 4)
                //{
                //    lstCards = Get3Cards(allCards);
                //    lstCards.Add(34);
                //    lstCards.Add(20);
                //    lstCards.Add(15);

                //}
                //i++;
                
                role.currentRound.AddHandCards(lstCards);
                if (role.roleid== playID)
                {
                    role.currentRound.playState = ERolePlayState.Play;
                }
                else
                {
                    role.currentRound.playState = ERolePlayState.Wait;
                }
                
            }

            // 剩余牌
            currentRound.bottomCards = allCards;
        }


        List<UInt32> Get3Cards(List<UInt32> randomCards)
        {
           List<UInt32> lstCards = randomCards.GetRange(0, 3);

            randomCards.RemoveRange(0, 3);
            //List<UInt32> lstCards = new List<UInt32>();
            return lstCards;
        }

        Random random = new Random();
        List<UInt32> RandomCards()
        {
            List<UInt32> lstCards = new List<UInt32>();

            UInt32 min = (UInt32)ECardId.Black2;
            UInt32 max = (UInt32)ECardId.Max - 1;

            for (UInt32 i = min; i <= max; i++)
            {
                lstCards.Add(i);
            }

            List<UInt32> lstResults = new List<UInt32>();
            while (lstCards.Count != 0)
            {
                int index = random.Next(lstCards.Count);
                UInt32 card = lstCards[index];
                lstResults.Add(card);
                lstCards.RemoveAt(index);
            }

            return lstResults;
        }

        public void SetRoomState(ERoomState state)
        {
            roomState = state;
            if (ERoomState.Play == roomState)
            {
                SetRolesState(ERoomRoleState.Play);
            }
            if (ERoomState.WaitRound == roomState)
            {
                SetRolesState(ERoomRoleState.Settle);
            }
        }

        void SetRolesState(ERoomRoleState roleState)
        {
            foreach (var role in lstRoles)
            {
                role.roleState = roleState;
            }
        }
   }




}
