﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Protocols.MajiangPushDown;
using BattleServer;
using Protocols.Majiang;
using HelpAsset;

namespace MahjongPushDown
{
    [ProtoContract]
    public class stOutCardReadyItem
    {
        /// <summary>
        /// 出牌人
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid { get; set; }

        /// <summary>
        /// 出该牌导致的碰杠胡信息
        /// </summary>
        [ProtoMember(2)]
        public stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();

        /// <summary>
        /// 胡牌类型
        /// </summary>
        [ProtoMember(3)]
        public EHuCardType huType = EHuCardType.Null;

        /// <summary>
        /// 是否自摸
        /// </summary>
        [ProtoMember(4)]
        public bool bSelfTouch = false;

        /// <summary>
        /// 是否是新起牌触发
        /// </summary>
        [ProtoMember(5)]
        public bool bNewCardTrigger = false;

        public bool CanReadyCard(UInt32 cardid)
        {
            foreach(var item in readyInfo.lstReadyCards)
            {
                if(item.cardid == cardid)
                {
                    return true;
                }
            }

            return false;
        }

        public void GetReadyTypes(UInt32 cardid, List<EMJCardType> lstTypes)
        {
            foreach (var card in readyInfo.lstReadyCards)
            {
                if (card.cardid == cardid)
                {
                    foreach(var item in card.lstItems)
                    {
                        lstTypes.Add(item.cardType);
                    }
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 人与牌
    /// </summary>
    [ProtoContract]
    public class stRoleOutCard
    {
        /// <summary>
        /// 出牌人
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid { get; set; }

        /// <summary>
        /// 出的牌
        /// </summary>
        [ProtoMember(2)]
        public UInt32 cardid { get; set; }

        /// <summary>
        /// 碰杠胡列表
        /// </summary>
        [ProtoMember(3)]
        public List<stOutCardReadyItem> lstReadyItems = new List<stOutCardReadyItem>();

        public void RecordHandOutCard(UInt64 roleid, UInt32 cardid)
        {
            this.roleid = roleid;
            this.cardid = cardid;
        }

        public void Clear()
        {
            roleid = 0;
            cardid = 0;
            lstReadyItems.Clear();
        }

        public void AddReadyItem(stOutCardReadyItem item)
        {
            lstReadyItems.Add(item);
        }

        public stOutCardReadyItem GetHuReadyItem(UInt64 roleid)
        {
            foreach(var item in lstReadyItems)
            {
                if(item.roleid == roleid)
                {
                    if(item.readyInfo.bHuPai)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public stOutCardReadyItem GetGangPengReadyItem(UInt64 roleid)
        {
            foreach (var item in lstReadyItems)
            {
                if (item.roleid == roleid)
                {
                    if (!item.readyInfo.bHuPai)
                    {
                        if (item.readyInfo.bPeng || item.readyInfo.gangType != EGangCardType.Null)
                        {
                            return item;
                        }
                    }
                }
            }
            return null;
        }

        bool IsHuNextRoles(UInt64 roleid)
        {
            bool bResult = false;
            bool bStart = false;
            foreach(var item in lstReadyItems)
            {
                if(bStart)
                {
                    if(item.readyInfo.bHuPai)
                    {
                        bResult = true;
                    }
                }

                if(item.roleid == roleid)
                {
                    bStart = true;
                }
            }

            return bResult;
        }

        public void AddReadyItem(UInt64 roleid, stPengGangHuReadyInfo readyInfo)
        {
            stOutCardReadyItem item = new stOutCardReadyItem();
            item.roleid = roleid;
            item.readyInfo = readyInfo;

            stOutCardReadyItem oldHuItem = GetHuReadyItem(item.roleid);
            stOutCardReadyItem oldGangPengItem = GetGangPengReadyItem(item.roleid);

            if (oldHuItem == null && oldGangPengItem == null)
            {
                lstReadyItems.Add(item);
                return;
            }

            if (IsHuNextRoles(roleid))
            {
                if (oldGangPengItem == null && oldHuItem != null)
                {
                    lstReadyItems.Add(item);
                }
                else
                {
                    if (item.readyInfo.bPeng)
                    {
                        oldGangPengItem.readyInfo.bPeng = true;
                    }

                    if (item.readyInfo.gangType != EGangCardType.Null)
                    {
                        oldGangPengItem.readyInfo.gangType = item.readyInfo.gangType;
                    }
                }
                return;
            }

            if (oldHuItem != null && oldGangPengItem == null)
            {
                if (item.readyInfo.bPeng)
                {
                    oldHuItem.readyInfo.bPeng = true;
                }

                if (item.readyInfo.gangType != EGangCardType.Null)
                {
                    oldHuItem.readyInfo.gangType = item.readyInfo.gangType;
                }
            }

            if (oldHuItem == null && oldGangPengItem != null)
            {
                if (item.readyInfo.bPeng)
                {
                    oldGangPengItem.readyInfo.bPeng = true;
                }

                if (item.readyInfo.gangType != EGangCardType.Null)
                {
                    oldGangPengItem.readyInfo.gangType = item.readyInfo.gangType;
                }
            }      
        }

        public bool HasReadyItem()
        {
            return lstReadyItems.Count != 0;
        }

        public stOutCardReadyItem FirstReadyItem
        {
            get
            {
                if(HasReadyItem())
                {
                    stOutCardReadyItem item = lstReadyItems[0];
                    lstReadyItems.RemoveAt(0);
                    return item;
                }
                return null;
            }
        }

        public bool HasHuReadyItem
        {
            get
            {
                foreach (var item in lstReadyItems)
                {
                    if (item.readyInfo.bHuPai)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }

    /// <summary>
    /// 回合信息
    /// </summary>
    [ProtoContract]
    public class RoomRoundInfo
    {
        /// <summary>
        /// 上次出牌信息
        /// </summary>
        [ProtoMember(1)]
        public stRoleOutCard lastHandOutCard = new stRoleOutCard();

        /// <summary>
        /// 胡牌者
        /// </summary>
        [ProtoMember(2)]
        public UInt64 huRoleId = 0;

        /// <summary>
        /// 结算条目
        /// </summary>
        [ProtoMember(3)]
        public List<RoundSettleItem> lstSettleItems = new List<RoundSettleItem>();

        /// <summary>
        /// 出牌开始时间
        /// </summary>
        [ProtoMember(4)]
        public DateTime playStartTime = DateTime.MinValue;

        /// <summary>
        /// 牌桌剩余牌
        /// </summary>
        [ProtoMember(5)]
        public List<UInt32> poolRemainCards = new List<UInt32>();

        /// <summary>
        /// 庄家
        /// </summary>
        [ProtoMember(6)]
        public UInt64 bankerRoleId = 0;

        /// <summary>
        /// 最后起牌角色ID
        /// </summary>
        [ProtoMember(7)]
        public UInt64 lastNewCardRoleId = 0;

        public UInt32 TakeOneNewCard()
        {
            UInt32 cardid = 0;
            if(poolRemainCards.Count != 0)
            {
                cardid = poolRemainCards[0];
                poolRemainCards.RemoveAt(0);
            }
            return cardid;
        }

        public void Clear()
        {
            lastHandOutCard.Clear();
            huRoleId = 0;
            lstSettleItems.Clear();
            playStartTime = DateTime.Now;
            poolRemainCards.Clear();
            bankerRoleId = 0;
            lastNewCardRoleId = 0;
        }

        /// <summary>
        /// 记录结算条款
        /// </summary>
        /// <param name="gangType">杠牌类型</param>
        /// <param name="selfTouch">是否自摸</param>
        /// <param name="roleid">自己</param>
        /// <param name="otherRoleId">点杠人</param>
        /// <param name="cardid">牌ID</param>
        /// <param name="bOtherBaoTing">点杠人是否报听</param>
        public void RecordSettleItem(EGangCardType gangType, bool selfTouch, UInt64 roleid, UInt64 otherRoleId, UInt32 cardid, bool bOtherBaoTing)
        {
            RoundSettleItem item = new RoundSettleItem();

            item.settleType = ERoundSettleType.Gang;
            item.bSelfTouch = selfTouch;
            item.gangType = gangType;
            item.roleid = roleid;
            item.otherRoleId = otherRoleId;
            item.cardid = cardid;
            item.bOtherBaoTing = bOtherBaoTing;
            lstSettleItems.Add(item);
        }

        /// <summary>
        /// 记录结算条款
        /// </summary>
        /// <param name="huType">胡牌类型</param>
        /// <param name="selfTouch">是否自摸</param>
        /// <param name="roleid">自己</param>
        /// <param name="otherRoleId">点炮人</param>
        /// <param name="cardid">牌ID</param>
        /// <param name="bOtherBaoTing">点炮人是否报听</param>
        public void RecordSettleItem(EHuCardType huType, bool selfTouch, HuRoomRole role, UInt64 otherRoleId, UInt32 cardid, bool bOtherBaoTing)
        {
            RoundSettleItem item = new RoundSettleItem();

            item.settleType = ERoundSettleType.Hu;
            item.bSelfTouch = selfTouch;
            item.huType = huType;
            item.roleid = role.roleid;
            item.otherRoleId = otherRoleId;
            item.cardid = cardid;
            item.bOtherBaoTing = bOtherBaoTing;
            item.bRobGangHu = role.bRobGangHu;
            lstSettleItems.Add(item);
        }

        /// <summary>
        /// 获取该类型牌还有多少张
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int GetPoolCardCount(EMJCardType type)
        {
            int count = 0;
            foreach(var cardid in poolRemainCards)
            {
                if(type == MJCardHelp.GetMJType(cardid))
                {
                    count++;
                }
            }

            return count;
        }
    }

    [ProtoContract]
    public class HuRoom
    {
        public HuRoom(UInt64 uuid)
        {
            roomUuid = uuid;
        }

        public void InitRoom(UInt64 ownerid, string ownerName, EGameType game)
        {
            roomOwner.roleid = ownerid;
            roomOwner.nickName = ownerName;
            gameType = game;
            roomState = ERoomState.WaitRoomer;

            lstSeats.Add(EMajongDirection.East);
            lstSeats.Add(EMajongDirection.North);
            lstSeats.Add(EMajongDirection.West);
            lstSeats.Add(EMajongDirection.South);
        }

        [ProtoMember(1)]
        public UInt64 roomUuid { get; set; }

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(2)]
        public EGameType gameType { get; set; }

        /// <summary>
        /// 房间主人
        /// </summary>
        [ProtoMember(3)]
        public stRoomOwner roomOwner = new stRoomOwner();

        /// <summary>
        /// 房间人数
        /// </summary>
        [ProtoMember(4)]
        public List<HuRoomRole> lstRoles = new List<HuRoomRole>();

        /// <summary>
        /// 房间状态
        /// </summary>
        [ProtoMember(5)]
        public ERoomState roomState = ERoomState.WaitRoomer;

        /// <summary>
        /// 第几牌局
        /// </summary>
        [ProtoMember(6)]
        public UInt32 currentCardRound = 0;

        /// <summary>
        /// 房间配置
        /// </summary>
        [ProtoMember(7)]
        public RoomConfig config = new RoomConfig();

        /// <summary>
        /// 回合信息
        /// </summary>
        [ProtoMember(8)]
        public RoomRoundInfo currentRound = new RoomRoundInfo();

        /// <summary>
        /// 最终结算
        /// </summary>
        [ProtoMember(9)]
        public stFinalSettle finalSettle = new stFinalSettle();

        public const int MAX_ROLES_COUNT = 4;

        /// <summary>
        /// 出牌时间10秒
        /// </summary>
        public const int HANDOUT_CARD_SECOND = 10;

        /// <summary>
        /// 解散信息
        /// </summary>
        [ProtoMember(10)]
        public stDismissInfo dismissInfo = new stDismissInfo();

        [ProtoMember(11)]
        public List<EMajongDirection> lstSeats = new List<EMajongDirection>();

        public void SetRoomState(ERoomState state)
        {
            roomState = state;
            if (ERoomState.Play == roomState)
            {
                SetRolesState(HuRoomRoleState.Play);
            }
            if (ERoomState.WaitRound == roomState)
            {
                SetRolesState(HuRoomRoleState.Settle);
            }
        }

        void SetRolesState(HuRoomRoleState roleState)
        {
            foreach(var role in lstRoles)
            {
                role.roleState = roleState;
            }
        }

        public bool IsAllRoleWaitRound()
        {
            foreach(var role in lstRoles)
            {
                if(role.roleState != HuRoomRoleState.WaitRound)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsAllRoleLackingDoorFinish()
        {
            if(roomState != ERoomState.Play)
            {
                return false;
            }

            foreach (var role in lstRoles)
            {
                if (role.roleState != HuRoomRoleState.Play)
                {
                    return false;
                }

                if(role.currentRound.playState == ERolePlayState.LackingDoor)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 是否是缺一门状态
        /// </summary>
        public bool IsLackingDoorState
        {
            get
            {
                if(roomState != ERoomState.Play)
                {
                    return false;
                }

                foreach(var role in lstRoles)
                {
                    if(role.roleState != HuRoomRoleState.Play)
                    {
                        return false;
                    }
                }

                foreach (var role in lstRoles)
                {
                    if(role.currentRound.playState == ERolePlayState.LackingDoor)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// 是否是最后一局
        /// </summary>
        /// <returns></returns>
        public bool IsLastRound()
        {
            if(config.cardRound == ECardRound.OneRound)
            {
                return currentCardRound >= 1;
            }
            else if(config.cardRound == ECardRound.FourRound)
            {
                return currentCardRound >= 4;
            }
            else if(config.cardRound == ECardRound.EightRound)
            {
                return currentCardRound >= 8;
            }

            return true;
        }

        /// <summary>
        /// 推倒胡结算
        /// </summary>
        /// <returns></returns>
        public RoundSettleInfo GetSettleInfo()
        {
            RoundSettleInfo settleInfo = new RoundSettleInfo();
            settleInfo.bankerRoleId = currentRound.bankerRoleId;
            settleInfo.lstSettleItems = currentRound.lstSettleItems;

            foreach(var role in lstRoles)
            {
                RoundSettleRole item = new RoundSettleRole();
                item.roleid = role.roleid;
                item.nickName = role.nickName;
                item.handCards.AddRange(role.currentRound.handCards);
                item.showCards = role.currentRound.showCards;
                item.bRobGangHu = role.bRobGangHu;
                settleInfo.lstSettleRoles.Add(item);
            }

            // 是否有人胡牌
            if(IsSettleHu())
            {
                SettleCommonHu(settleInfo);
            }
           
            foreach(var item in settleInfo.lstSettleRoles)
            {
                HuRoomRole role = GetRole(item.roleid);
                if(null == role)
                {
                    continue;
                }
                role.score += item.score;

                finalSettle.ChangeScore(role.roleid, item.score);
            }

            return settleInfo;
        }

        void SettleCommonHu(RoundSettleInfo settleInfo)
        {
            foreach (var item in currentRound.lstSettleItems)
            {
                if (item.settleType == ERoundSettleType.Gang)
                {
                    if (item.gangType == EGangCardType.AnGang)
                    {
                        settleInfo.AddScore(item.roleid, 6);
                        settleInfo.DelScoreExceptRole(item.roleid, 2);
                        settleInfo.SetMaxScales(item.roleid, 2);
                    }
                    else if (item.gangType == EGangCardType.BuGang)
                    {
                        settleInfo.AddScore(item.roleid, 3);
                        settleInfo.DelScoreExceptRole(item.roleid, 1);
                        settleInfo.SetMaxScales(item.roleid, 1);
                    }
                    else
                    {
                        if (item.bOtherBaoTing)
                        {
                            settleInfo.AddScore(item.roleid, 3);
                            settleInfo.DelScoreExceptRole(item.roleid, 1);
                            settleInfo.SetMaxScales(item.roleid, 1);
                        }
                        else
                        {
                            settleInfo.AddScore(item.roleid, 3);
                            settleInfo.DelScore(item.otherRoleId, 3);
                            settleInfo.SetMaxScales(item.roleid, 1);
                        }
                    }
                }
                else if (item.settleType == ERoundSettleType.Hu)
                {
                    int score = GetHuScales(item.huType);
                    if (item.bSelfTouch)
                    {
                        if(item.huType == EHuCardType.CommonHu)
                        {
                            settleInfo.AddScore(item.roleid, 6);
                            settleInfo.DelScoreExceptRole(item.roleid, 2);
                            settleInfo.SetMaxScales(item.roleid, 2);
                        }
                        else
                        {
                            settleInfo.AddScore(item.roleid, 3 * score);
                            settleInfo.DelScoreExceptRole(item.roleid, score);
                            settleInfo.SetMaxScales(item.roleid, 3 * score);
                        }
                    }
                    else
                    {
                        if (item.bOtherBaoTing)
                        {
                            if (item.bRobGangHu)
                            {
                                //if (item.huType == EHuCardType.CommonHu)
                                //{
                                //    settleInfo.AddScore(item.roleid, 3 * 2);
                                //    settleInfo.DelScore(item.otherRoleId, 3 * 2);
                                //    return;
                                //}

                                settleInfo.AddScore(item.roleid, score);
                                settleInfo.DelScore(item.otherRoleId, score);
                            }
                            else
                            {
                                // 报听点炮3家平分
                                settleInfo.AddScore(item.roleid, score);
                                settleInfo.DelScoreExceptRole(item.roleid, score / 3);
                                settleInfo.SetMaxScales(item.roleid, score);
                            }                    
                        }
                        else
                        {
                            // 不报听点炮
                            if (item.bRobGangHu)
                            {
                                //if (item.huType == EHuCardType.CommonHu)
                                //{
                                //    settleInfo.AddScore(item.roleid, 3 * 2);
                                //    settleInfo.DelScore(item.otherRoleId, 3 * 2);
                                //    return;
                                //}

                                settleInfo.AddScore(item.roleid, score);
                                settleInfo.DelScore(item.otherRoleId, score);
                            }
                            else
                            {
                                settleInfo.AddScore(item.roleid, score);
                                settleInfo.DelScore(item.otherRoleId, score);
                                settleInfo.SetMaxScales(item.roleid, score);
                            }
                        }
                    }
                }
            }
        }

        bool IsSettleHu()
        {
            foreach (var item in currentRound.lstSettleItems)
            {
                if(item.settleType == ERoundSettleType.Hu)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 是否可以解散
        /// </summary>
        public bool IsInCanDismissState
        {
            get
            {
                if (roomState == ERoomState.WaitRound)
                {
                    return true;
                }

                if(roomState == ERoomState.Play)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 获得番数
        /// </summary>
        /// <param name="settleType"></param>
        /// <returns></returns>
        public int GetHuScales(EHuCardType settleType)
        {
            if (EHuCardType.ThirteenOrphans == settleType)
            {
                return 27;
            }

            if (EHuCardType.LuxurySevenPairs == settleType)
            {
                return 18;
            }

            if (EHuCardType.SameColor == settleType)
            {
                return 9;
            }

            if (EHuCardType.SenvenPairs == settleType)
            {
                return 9;
            }

            if (EHuCardType.OneDragon == settleType)
            {
                return 9;
            }

            if (EHuCardType.CommonHu == settleType)
            {
                return 3;
            }

            return 0;
        }

        public HuRoomRole GetRole(UInt64 roleid)
        {
            foreach(var role in lstRoles)
            {
                if(role.roleid == roleid)
                {
                    return role;
                }
            }

            return null;
        }

        public HuRoomRole GetRole(EMajongDirection direction)
        {
            foreach (var role in lstRoles)
            {
                if (role.direction == direction)
                {
                    return role;
                }
            }

            return null;
        }

        public HuRoomRole NextRole(HuRoomRole role)
        {
            EMajongDirection direction = role.direction + 1;
            if(direction == EMajongDirection.Max)
            {
                direction = EMajongDirection.East;
            }

            return GetRole(direction);
        }

        public List<HuRoomRole> NextRoles(HuRoomRole role)
        {
            List<HuRoomRole> tempRoles = new List<HuRoomRole>();
            for (int i=0; i<3; i++)
            {
                role = NextRole(role);
                tempRoles.Add(role);
            }
            return tempRoles;
        }

        /// <summary>
        /// 获得一个座位
        /// </summary>
        /// <returns></returns>
        public EMajongDirection GetOneSeat()
        {
            lstSeats.Sort();
            if(lstSeats.Count > 0)
            {
                EMajongDirection seat = lstSeats[0];
                lstSeats.Remove(seat);
                return seat;
            }
            return EMajongDirection.Null;
        }

        /// <summary>
        /// 归还座位
        /// </summary>
        /// <param name="seat"></param>
        public void ReturnSeat(EMajongDirection seat)
        {
            lstSeats.Add(seat);
        }

        public MahjongRoomInfo GetRoomInfo(UInt64 selfid)
        {
            MahjongRoomInfo info = new MahjongRoomInfo();
            info.roomUuid = roomUuid;
            info.roomState = roomState;
            foreach (var role in lstRoles)
            {
                if (selfid != role.roleid)
                {
                    info.lstRoles.Add(role.remoteInfo);
                }
            }

            info.localRoleInfo = GetRole(selfid).localInfo;
            info.localRoleInfo.remainSecond = dismissRemainSecond;

            //第几牌局
            info.currentCardRound = currentCardRound;

            // 剩余牌数
            info.remainCardCount = currentRound.poolRemainCards.Count;

            // 庄家
            info.bankerRoleId = currentRound.bankerRoleId;

            info.config = config;

            info.roomOwner = roomOwner;

            info.dismissInfo = dismissInfo;

            return info;
        }

        /// <summary>
        /// 解散倒计时
        /// </summary>
        public int dismissRemainSecond
        {
            get
            {
                if (dismissInfo.firstRoleId == 0)
                {
                    return 0;
                }

                if(env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return 0;
                }

                TimeSpan span = dismissInfo.expireTime - env.Timer.CurrentDateTime;
                return (int)span.TotalSeconds;
            }
        }

        /// <summary>
        /// 回合开始通知
        /// </summary>
        void SendRoundStartNtf()
        {
            foreach(var role in lstRoles)
            {
                // 回合开始通知
                MJPushDownRoundStartNtf roundStartNtf = new MJPushDownRoundStartNtf();
                roundStartNtf.roomInfo = GetRoomInfo(role.roleid);
                role.SendMsgToClient(roundStartNtf);
            }
        }

        public void SendRoomMsg(ProtoBody msg)
        {
            foreach(var role in lstRoles)
            {
                role.SendMsgToClient(msg);
            }
        }

        public void SendRoomMsgEx(UInt64 roleid, ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {
                if(role.roleid == roleid)
                {
                    continue;
                }
                role.SendMsgToClient(msg);
            }
        }

        public bool IsRoomFull()
        {
            return lstRoles.Count >= MAX_ROLES_COUNT;
        }

        public void AddRole(HuRoomRole role)
        {
            lstRoles.Add(role);
        }

        public void RemoveRole(UInt64 roleid)
        {
            HuRoomRole role = GetRole(roleid);

            ReturnSeat(role.direction);

            lstRoles.Remove(role);
        }

        public string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.detail_hu_room_, roomUuid.ToString());
                return key;
            }
        }

        public bool SaveDb(bool bAlways = false)
        {
            byte[] data;
            if (!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;
            
            if(bAlways)
            {
                redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);
            }
            else
            {
                redis.db.StringSet(dbKey, data, null, When.Exists, CommandFlags.FireAndForget);
            }

            return true;
        }

        public async Task<bool> LoadFromDbAsync()
        {
            RedisSystem redis = env.Redis;

            RedisValue value = await redis.db.StringGetAsync(dbKey);
            if (value.IsNullOrEmpty)
            {
                return false;
            }

            if (!this.SerializeFromBinary(value))
            {
                return false;
            }

            foreach(var role in lstRoles)
            {
                role.Init(this);
            }

            return true;
        }

        public bool DeleteFromDb()
        {
            RedisSystem redis = env.Redis;

            redis.db.KeyDelete(dbKey, CommandFlags.FireAndForget);

            return true;
        }

        Random random = new Random();

        List<UInt32> RandomCards()
        {
            List<UInt32> lstCards = new List<UInt32>();
            UInt32 min = (UInt32)EMJCard.Null + 1;
            UInt32 max = (UInt32)EMJCard.Max - 1;

            // 不带风
            if(!config.bWindCard)
            {
                max = (UInt32)EMJCard.NineMillion4;
            }

            for (UInt32 i = min; i <= max; i++)
            {
                lstCards.Add(i);
            }

            List<UInt32> lstResults = new List<UInt32>();
            while(lstCards.Count != 0)
            {
                int index = random.Next(lstCards.Count);
                UInt32 card = lstCards[index];
                lstResults.Add(card);
                lstCards.RemoveAt(index);
            }

            return lstResults;
        }

        public void StartNewRound()
        {
            InitNewRound();

            SetRoomState(ERoomState.Play);

            SendRoundStartNtf();

            // 第一个人起牌
            bankerRole.DrawNewCard();
        }

        /// <summary>
        /// 庄家
        /// </summary>
        public HuRoomRole bankerRole
        {
            get
            {
                HuRoomRole role = GetRole(currentRound.bankerRoleId);
                return role;
            }
        }

        /// <summary>
        /// 处理结算
        /// </summary>
        public void Settle()
        {
            MJPushDownRoundSettleNtf ntf = new MJPushDownRoundSettleNtf();
            ntf.settleInfo = GetSettleInfo();
            ntf.IsRoomOver = IsLastRound();
            ntf.finalSettle = finalSettle;
            SendRoomMsg(ntf);

            if (IsLastRound())
            {
                RoomClose();
                return;
            }

            // 切换到结算状态
            SetRoomState(ERoomState.WaitRound);
        }

        /// <summary>
        /// 获取战绩
        /// </summary>
        /// <returns></returns>
        stBattleScoreItem GetBattleScoreItem()
        {
            stBattleScoreItem scoreItem = new stBattleScoreItem();
            scoreItem.dateTime = env.Timer.CurrentDateTime;
            scoreItem.roomUuid = roomUuid;
            scoreItem.gameType = EGameType.MJPushDown;
            scoreItem.roomOwner = roomOwner;
            
            foreach(var item in finalSettle.mapScores)
            {
                HuRoomRole role = GetRole(item.Key);
                if(null == role)
                {
                    continue;
                }
                stBattleScoreRole scoreRole = new stBattleScoreRole();
                scoreRole.roleid = role.roleid;
                scoreRole.nickName = role.nickName;
                scoreRole.score = item.Value;
                scoreItem.lstRoles.Add(scoreRole);
            }
            return scoreItem;
        }

        /// <summary>
        /// 检测解散超时
        /// </summary>
        public bool CheckDismissTimer()
        {
            if(dismissInfo.bStart)
            {
                if(env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return true;
                }
            }

            return false;
        }

        public void NotifyDismissSuccess()
        {
            MJPushDownDismissGameSuccessNtf successNtf = new MJPushDownDismissGameSuccessNtf();
            successNtf.bSuccess = true;
            successNtf.finalSettle = finalSettle;
            SendRoomMsg(successNtf);
        }

        /// <summary>
        /// 关闭房间
        /// </summary>
        public void RoomClose()
        {
            foreach (var role in lstRoles)
            {
                BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                if (null == player)
                {
                    continue;
                }

                // 解绑战斗服
                player.UnBindBattleFromGate();

                g.playerMgr.RemovePlayer(role.roleid);
            }

            // 通知房间关闭
            MJPushDownRoomCloseB2L closeNtf = new MJPushDownRoomCloseB2L();
            closeNtf.roomUuid = roomUuid;
            closeNtf.roomOwnerId = roomOwner.roleid;
            if(roomState == ERoomState.WaitRoomer)
            {
                closeNtf.bReturnRoomCard = true;
            }
            closeNtf.bRecordBattleScore = isRecordBattleScore;
            closeNtf.scoreItem = GetBattleScoreItem();
            SendMsgToLobby(closeNtf);

            g.huRoomMgr.DeleteRoom(roomUuid);
        }

        /// <summary>
        /// 是否记录战绩
        /// </summary>
        public bool isRecordBattleScore
        {
            get
            {
                return roomState != ERoomState.WaitRoomer;
            }
        }

        void SendMsgToLobby(ProtoBody proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }

        /// <summary>
        /// 处理流局
        /// </summary>
        public void OnFlowRound()
        {
            // 处理结算
            Settle();
        }

        public void FirstInit()
        {
            foreach(var role in lstRoles)
            {
                finalSettle.InitScore(role.roleid);
            }
        }

        void InitNewRound()
        {
            currentCardRound++;
     
            if(currentCardRound == 1)
            {
                currentRound.bankerRoleId = roomOwner.roleid;

                foreach (var role in lstRoles)
                {
                    // TODO 临时添加
                    role.currentRound.testRoomUuid = roomUuid;
                    role.currentRound.testRoleId = role.roleid;
                }
            }
            else
            {
                UInt64 bankerId = 0;
                // 上回合有没有流局
                if(currentRound.huRoleId == 0)
                {
                    bankerId = currentRound.lastNewCardRoleId;
                }
                else
                {
                    bankerId = currentRound.huRoleId;
                }

                currentRound.Clear();
                foreach(var role in lstRoles)
                {
                    role.currentRound.Clear();
                }

                currentRound.bankerRoleId = bankerId;
            }

            currentRound.poolRemainCards = RandomCards();

            // 第一个人13张牌
            
            HuRoomRole firstRole = GetRole(currentRound.bankerRoleId);
            firstRole.currentRound.handCards = fetchfirst13Cards(currentRound.poolRemainCards);
            firstRole.currentRound.playState = ERolePlayState.Wait;

            //测试代码
            //firstRole.currentRound.handCards = TestHandCards();
            //firstRole.currentRound.showCards.Add(TestShowCards());
            //firstRole.nextRole.currentRound.handCards = TestHandCards1();
            //firstRole.nextRole.currentRound.playState = ERolePlayState.Wait;

            //currentRound.poolRemainCards.Remove((UInt32)EMJCard.SixBucket4);


            // 其他三个人
            foreach (var role in lstRoles)
            {
                if (role.roleid == currentRound.bankerRoleId)
                {
                    continue;
                }

                ////测试代码
                //if (role.roleid == firstRole.nextRole.roleid)
                //{
                //    continue;
                //}

                HuRoomRole secondRole = GetRole(role.roleid);
                secondRole.currentRound.handCards = fetch13Cards(currentRound.poolRemainCards);
                secondRole.currentRound.playState = ERolePlayState.Wait;
            }

            foreach (var role in lstRoles)
            {
                role.bRobGangHu = false;
            }

            //currentRound.poolRemainCards.Insert(4, (UInt32)EMJCard.SixBucket4);


            //int count = 0;
            //foreach (var role in lstRoles)
            //{
            //    HuRoomRole secondRole = GetRole(role.roleid);
            //    count++;
            //    if (count == 1)
            //    {
            //        secondRole.currentRound.handCards = TestCards1(currentRound.poolRemainCards);
            //    }
            //    else if (count == 2)
            //    {
            //        secondRole.currentRound.handCards = TestCards2(currentRound.poolRemainCards);
            //    }
            //    else if (count == 3)
            //    {
            //        secondRole.currentRound.handCards = TestCards3(currentRound.poolRemainCards);
            //    }
            //    else if (count == 4)
            //    {
            //        secondRole.currentRound.handCards = TestCards4(currentRound.poolRemainCards);
            //    }

            //    secondRole.currentRound.playState = ERolePlayState.Wait;
            //}


            //测试代码
            //currentRound.poolRemainCards.Remove((UInt32)EMJCard.TwoRope4);
            //currentRound.poolRemainCards.Insert(0, (UInt32)EMJCard.TwoRope4);
            //currentRound.poolRemainCards.Remove((UInt32)EMJCard.TwoBucket2);
            //currentRound.poolRemainCards.Insert(1, (UInt32)EMJCard.TwoBucket2);
        }

        public HashSet<UInt32> TestHandCards()
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            lstResults.Add((uint)EMJCard.SevenBucket1);
            lstResults.Add((uint)EMJCard.EightBucket1);
            lstResults.Add((uint)EMJCard.NineBucket1);
            lstResults.Add((uint)EMJCard.FourMillion1);
            lstResults.Add((uint)EMJCard.FourMillion2);
            lstResults.Add((uint)EMJCard.FiveMillion1);
            lstResults.Add((uint)EMJCard.FiveMillion2);
            lstResults.Add((uint)EMJCard.FiveMillion3);
            lstResults.Add((uint)EMJCard.SixMillion1);
            lstResults.Add((uint)EMJCard.SevenMillion1);
            //lstResults.Add((uint)EMJCard.NineRope1);
            //lstResults.Add((uint)EMJCard.NineRope2);
            //lstResults.Add((uint)EMJCard.NineRope3);

            foreach (var item in lstResults)
            {
                if (currentRound.poolRemainCards.Contains(item))
                {
                    currentRound.poolRemainCards.Remove(item);
                }
            }
            return lstResults;
        }

        /// <summary>
        /// 测试:手牌
        /// </summary>
        /// <returns></returns>
        public MingCardItem TestShowCards()
        {
            List<UInt32> lstResults = new List<UInt32>();
            lstResults.Add((uint)EMJCard.TwoRope1);
            lstResults.Add((uint)EMJCard.TwoRope2);
            lstResults.Add((uint)EMJCard.TwoRope3);

            foreach (var item in lstResults)
            {
                if (currentRound.poolRemainCards.Contains(item))
                {
                    currentRound.poolRemainCards.Remove(item);
                }
            }

            MingCardItem card = new MingCardItem();
            card.showCards = lstResults;
            card.mingType = EMingCardType.Peng;
            card.cardType = EMJCardType.TwoRope;

            return card;
        }

        public HashSet<UInt32> TestHandCards1()
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            lstResults.Add((uint)EMJCard.OneRope4);
            lstResults.Add((uint)EMJCard.ThreeRope1);
            lstResults.Add((uint)EMJCard.FourBucket1);
            lstResults.Add((uint)EMJCard.FiveBucket1);
            lstResults.Add((uint)EMJCard.SixBucket1);
            lstResults.Add((uint)EMJCard.FiveMillion1);
            lstResults.Add((uint)EMJCard.SixMillion1);
            lstResults.Add((uint)EMJCard.SevenMillion1);      
            lstResults.Add((uint)EMJCard.NorthWind2);
            lstResults.Add((uint)EMJCard.NorthWind3);
            lstResults.Add((uint)EMJCard.TwoMillion1);
            lstResults.Add((uint)EMJCard.TwoMillion2);
            lstResults.Add((uint)EMJCard.TwoMillion3);

            foreach (var item in lstResults)
            {
                if (currentRound.poolRemainCards.Contains(item))
                {
                    currentRound.poolRemainCards.Remove(item);
                }
            }
            return lstResults;
        }

        public static string ToString(List<UInt32> lstCards)
        {
            string temp = "";
            foreach(var cardid in lstCards)
            {
                temp += cardid.ToString();
                temp += ",";
            }
            return temp;
        }

        public static string ToString(HashSet<UInt32> lstCards)
        {
            string temp = "";
            foreach (var cardid in lstCards)
            {
                temp += cardid.ToString();
                temp += ",";
            }
            return temp;
        }

        HashSet<UInt32> fetch13Cards(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            for(int i=0; i<13; i++)
            {
                UInt32 card = lstCards[0];
                lstResults.Add(card);
                lstCards.RemoveAt(0);
            }
            return lstResults;
        }

        HashSet<UInt32> fetchfirst13Cards(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            for (int i = 0; i < 13; i++)
            {
                UInt32 card = lstCards[0];
                lstResults.Add(card);
                lstCards.RemoveAt(0);
            }
            return lstResults;
        }

        public void OnRoleOnline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = true;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);
        }

        public void OnRoleOffline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = false;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);

            if(IsAllRoleOffline)
            {
                g.huRoomMgr.RemoveRoom(roomUuid);

                MJPushDownAllRoleOfflineB2L offlineNtf = new MJPushDownAllRoleOfflineB2L();
                offlineNtf.roomUuid = roomUuid;
                SendMsgToLobby(ntf);
            }
        }

        public void SendMsgToLobby(Protocol proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }

        /// <summary>
        /// 房间内所有人都下线了
        /// </summary>
        /// <returns></returns>
        bool IsAllRoleOffline
        {
            get
            {
                foreach (var role in lstRoles)
                {
                    BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                    if (player != null)
                    {
                        if (player.isOnline)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }

        HashSet<UInt32> TestCards1(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.OneMillion1);
            lstResults.Add((UInt32)EMJCard.OneMillion2);
            lstResults.Add((UInt32)EMJCard.OneMillion3);

            lstResults.Add((UInt32)EMJCard.ThreeMillion1);
            lstResults.Add((UInt32)EMJCard.ThreeMillion2);
            lstResults.Add((UInt32)EMJCard.ThreeMillion3);

            lstResults.Add((UInt32)EMJCard.FourMillion1);

            lstResults.Add((UInt32)EMJCard.SixMillion1);
            lstResults.Add((UInt32)EMJCard.SixMillion2);
            lstResults.Add((UInt32)EMJCard.SixMillion3);

            lstResults.Add((UInt32)EMJCard.NineMillion1);
            lstResults.Add((UInt32)EMJCard.NineMillion2);
            lstResults.Add((UInt32)EMJCard.NineMillion3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }

        HashSet<UInt32> TestCards2(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.OneRope1);
            lstResults.Add((UInt32)EMJCard.OneRope2);
            lstResults.Add((UInt32)EMJCard.OneRope3);

            lstResults.Add((UInt32)EMJCard.ThreeRope1);
            lstResults.Add((UInt32)EMJCard.ThreeRope2);
            lstResults.Add((UInt32)EMJCard.ThreeRope3);

            lstResults.Add((UInt32)EMJCard.FourRope1);

            lstResults.Add((UInt32)EMJCard.SixRope1);
            lstResults.Add((UInt32)EMJCard.SixRope2);
            lstResults.Add((UInt32)EMJCard.SixRope3);

            lstResults.Add((UInt32)EMJCard.NineRope1);
            lstResults.Add((UInt32)EMJCard.NineRope2);
            lstResults.Add((UInt32)EMJCard.NineRope3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }

        HashSet<UInt32> TestCards3(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.OneBucket1);
            lstResults.Add((UInt32)EMJCard.OneBucket2);
            lstResults.Add((UInt32)EMJCard.OneBucket3);

            lstResults.Add((UInt32)EMJCard.ThreeBucket1);
            lstResults.Add((UInt32)EMJCard.ThreeBucket2);
            lstResults.Add((UInt32)EMJCard.ThreeBucket3);

            lstResults.Add((UInt32)EMJCard.FourBucket1);

            lstResults.Add((UInt32)EMJCard.SixBucket1);
            lstResults.Add((UInt32)EMJCard.SixBucket2);
            lstResults.Add((UInt32)EMJCard.SixBucket3);

            lstResults.Add((UInt32)EMJCard.NineBucket1);
            lstResults.Add((UInt32)EMJCard.NineBucket2);
            lstResults.Add((UInt32)EMJCard.NineBucket3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }

        HashSet<UInt32> TestCards4(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.TwoBucket1);
            lstResults.Add((UInt32)EMJCard.TwoBucket2);
            lstResults.Add((UInt32)EMJCard.TwoBucket3);

            lstResults.Add((UInt32)EMJCard.TwoRope1);
            lstResults.Add((UInt32)EMJCard.TwoRope2);
            lstResults.Add((UInt32)EMJCard.TwoRope3);

            lstResults.Add((UInt32)EMJCard.SevenRope1);

            lstResults.Add((UInt32)EMJCard.TwoMillion1);
            lstResults.Add((UInt32)EMJCard.TwoMillion2);
            lstResults.Add((UInt32)EMJCard.TwoMillion3);

            lstResults.Add((UInt32)EMJCard.EightMillion1);
            lstResults.Add((UInt32)EMJCard.EightMillion2);
            lstResults.Add((UInt32)EMJCard.EightMillion3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }
    }
}
