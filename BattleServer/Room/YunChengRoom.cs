﻿using BattleServer;
using HelpAsset;
using HelpAsset.MahjongYunCheng;
using Log;
using ProtoBuf;
using Protocols;
using Protocols.Majiang;
using Protocols.MajiangYunCheng;
using Redis;
using ServerLib;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahjongYunCheng
{
    [ProtoContract]
    public class stOutCardReadyItem
    {
        /// <summary>
        /// 出牌人
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid { get; set; }

        /// <summary>
        /// 出该牌导致的碰杠胡信息
        /// </summary>
        [ProtoMember(2)]
        public stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();

        /// <summary>
        /// 胡牌类型
        /// </summary>
        [ProtoMember(3)]
        public EHuCardType huType = EHuCardType.Null;

        /// <summary>
        /// 是否自摸
        /// </summary>
        [ProtoMember(4)]
        public bool bSelfTouch = false;

        /// <summary>
        /// 是否是新起牌触发
        /// </summary>
        [ProtoMember(5)]
        public bool bNewCardTrigger = false;

        public bool CanReadyCard(UInt32 cardid)
        {
            foreach (var item in readyInfo.lstReadyCards)
            {
                if (item.cardid == cardid)
                {
                    return true;
                }
            }

            return false;
        }

        public void GetReadyTypes(UInt32 cardid, List<EMJCardType> lstTypes)
        {
            foreach (var card in readyInfo.lstReadyCards)
            {
                if (card.cardid == cardid)
                {
                    foreach (var item in card.lstItems)
                    {
                        lstTypes.Add(item.cardType);
                    }
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 人与牌
    /// </summary>
    [ProtoContract]
    public class stRoleOutCard
    {
        /// <summary>
        /// 出牌人
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid { get; set; }

        /// <summary>
        /// 出的牌
        /// </summary>
        [ProtoMember(2)]
        public UInt32 cardid { get; set; }

        /// <summary>
        /// 碰杠胡列表
        /// </summary>
        [ProtoMember(3)]
        public List<stOutCardReadyItem> lstReadyItems = new List<stOutCardReadyItem>();

        public void RecordHandOutCard(UInt64 roleid, UInt32 cardid)
        {
            this.roleid = roleid;
            this.cardid = cardid;
        }

        public void Clear()
        {
            roleid = 0;
            cardid = 0;
            lstReadyItems.Clear();
        }

        public void AddReadyItem(stOutCardReadyItem item)
        {
            lstReadyItems.Add(item);
        }

        public stOutCardReadyItem GetHuReadyItem(UInt64 roleid)
        {
            foreach (var item in lstReadyItems)
            {
                if (item.roleid == roleid)
                {
                    if (item.readyInfo.bHuPai)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public stOutCardReadyItem GetGangPengReadyItem(UInt64 roleid)
        {
            foreach (var item in lstReadyItems)
            {
                if (item.roleid == roleid)
                {
                    if (!item.readyInfo.bHuPai)
                    {
                        if (item.readyInfo.bPeng || item.readyInfo.gangType != EGangCardType.Null)
                        {
                            return item;
                        }
                    }
                }
            }
            return null;
        }

        bool IsHuNextRoles(UInt64 roleid)
        {
            bool bResult = false;
            bool bStart = false;
            foreach (var item in lstReadyItems)
            {
                if (bStart)
                {
                    if (item.readyInfo.bHuPai)
                    {
                        bResult = true;
                    }
                }

                if (item.roleid == roleid)
                {
                    bStart = true;
                }
            }

            return bResult;
        }

        public void AddReadyItem(UInt64 roleid, stPengGangHuReadyInfo readyInfo)
        {
            stOutCardReadyItem item = new stOutCardReadyItem();
            item.roleid = roleid;
            item.readyInfo = readyInfo;

            stOutCardReadyItem oldHuItem = GetHuReadyItem(item.roleid);
            stOutCardReadyItem oldGangPengItem = GetGangPengReadyItem(item.roleid);

            if (oldHuItem == null && oldGangPengItem == null)
            {
                lstReadyItems.Add(item);
                return;
            }

            if (IsHuNextRoles(roleid))
            {
                if (oldGangPengItem == null && oldHuItem != null)
                {
                    lstReadyItems.Add(item);
                }
                else
                {
                    if (item.readyInfo.bPeng)
                    {
                        oldGangPengItem.readyInfo.bPeng = true;
                    }

                    if (item.readyInfo.gangType != EGangCardType.Null)
                    {
                        oldGangPengItem.readyInfo.gangType = item.readyInfo.gangType;
                    }
                }
                return;
            }

            if (oldHuItem != null && oldGangPengItem == null)
            {
                if (item.readyInfo.bPeng)
                {
                    oldHuItem.readyInfo.bPeng = true;
                }

                if (item.readyInfo.gangType != EGangCardType.Null)
                {
                    oldHuItem.readyInfo.gangType = item.readyInfo.gangType;
                }
            }

            if (oldHuItem == null && oldGangPengItem != null)
            {
                if (item.readyInfo.bPeng)
                {
                    oldGangPengItem.readyInfo.bPeng = true;
                }

                if (item.readyInfo.gangType != EGangCardType.Null)
                {
                    oldGangPengItem.readyInfo.gangType = item.readyInfo.gangType;
                }
            }
        }

        public bool HasReadyItem()
        {
            return lstReadyItems.Count != 0;
        }

        public stOutCardReadyItem FirstReadyItem
        {
            get
            {
                if (HasReadyItem())
                {
                    stOutCardReadyItem item = lstReadyItems[0];
                    lstReadyItems.RemoveAt(0);
                    return item;
                }
                return null;
            }
        }
    }

    /// <summary>
    /// 回合信息
    /// </summary>
    [ProtoContract]
    public class RoomRoundInfo
    {
        /// <summary>
        /// 上次出牌信息
        /// </summary>
        [ProtoMember(1)]
        public stRoleOutCard lastHandOutCard = new stRoleOutCard();

        /// <summary>
        /// 胡牌者
        /// </summary>
        [ProtoMember(2)]
        public UInt64 huRoleId = 0;

        /// <summary>
        /// 结算条目
        /// </summary>
        [ProtoMember(3)]
        public List<RoundSettleItem> lstSettleItems = new List<RoundSettleItem>();

        /// <summary>
        /// 出牌开始时间
        /// </summary>
        [ProtoMember(4)]
        public DateTime playStartTime = DateTime.MinValue;

        /// <summary>
        /// 牌桌剩余牌
        /// </summary>
        [ProtoMember(5)]
        public List<UInt32> poolRemainCards = new List<UInt32>();

        /// <summary>
        /// 庄家
        /// </summary>
        [ProtoMember(6)]
        public UInt64 bankerRoleId = 0;

        /// <summary>
        /// 最后起牌角色ID
        /// </summary>
        [ProtoMember(7)]
        public UInt64 lastNewCardRoleId = 0;

        public UInt32 TakeOneNewCard()
        {
            UInt32 cardid = 0;
            if (poolRemainCards.Count != 0)
            {
                cardid = poolRemainCards[0];
                poolRemainCards.RemoveAt(0);
            }
            return cardid;
        }

        public void Clear()
        {
            lastHandOutCard.Clear();
            huRoleId = 0;
            lstSettleItems.Clear();
            playStartTime = DateTime.Now;
            poolRemainCards.Clear();
            bankerRoleId = 0;
            lastNewCardRoleId = 0;
        }

        /// <summary>
        /// 记录结算条款
        /// </summary>
        /// <param name="gangType">杠牌类型</param>
        /// <param name="selfTouch">是否自摸</param>
        /// <param name="roleid">自己</param>
        /// <param name="otherRoleId">点杠人</param>
        /// <param name="cardid">牌ID</param>
        /// <param name="bOtherBaoTing">其他人是否报听</param>
        public void RecordSettleItem(EGangCardType gangType, bool selfTouch, UInt64 roleid, UInt64 otherRoleId, UInt32 cardid, bool bOtherBaoTing)
        {
            RoundSettleItem item = new RoundSettleItem();

            item.settleType = ERoundSettleType.Gang;
            item.bSelfTouch = selfTouch;
            item.gangType = gangType;
            item.roleid = roleid;
            item.otherRoleId = otherRoleId;
            item.cardid = cardid;
            item.bOtherBaoTing = bOtherBaoTing;
            lstSettleItems.Add(item);
        }

        /// <summary>
        /// 记录结算条款
        /// </summary>
        /// <param name="huType">胡牌类型</param>
        /// <param name="selfTouch">是否自摸</param>
        /// <param name="roleid">自己</param>
        /// <param name="otherRoleId">点炮人</param>
        /// <param name="cardid">牌ID</param>
        /// <param name="bOtherBaoTing">其他人是否报听</param>
        public void RecordSettleItem(EHuCardType huType, bool selfTouch, YunChengRoomRole role, UInt64 otherRoleId, UInt32 cardid, bool bOtherBaoTing)
        {
            RoundSettleItem item = new RoundSettleItem();

            item.settleType = ERoundSettleType.Hu;
            item.bSelfTouch = selfTouch;
            item.huType = huType;
            item.roleid = role.roleid;
            item.otherRoleId = otherRoleId;
            item.cardid = cardid;
            item.bOtherBaoTing = bOtherBaoTing;
            item.bRobGangHu = role.bRobGangHu;
            lstSettleItems.Add(item);
        }

        /// <summary>
        /// 获取该类型牌还有多少张
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public int GetPoolCardCount(EMJCardType type)
        {
            int count = 0;
            foreach (var cardid in poolRemainCards)
            {
                if (type == MJCardHelp.GetMJType(cardid))
                {
                    count++;
                }
            }

            return count;
        }
    }

    [ProtoContract]
    public class YunChengRoom
    {   
        public YunChengRoom(UInt64 uuid)
        {
            //设置房间id
            roomUuid = uuid;
        }

        //初始化房间信息
        public void InitRoom(UInt64 ownerid, string ownerName, EGameType game)
        {
            roomOwner.roleid = ownerid;
            roomOwner.nickName = ownerName;
            gameType = game;
            roomState = ERoomState.WaitRoomer;
            lstSeats.Add(EMajongDirection.East);
            lstSeats.Add(EMajongDirection.North);
            lstSeats.Add(EMajongDirection.West);
            lstSeats.Add(EMajongDirection.South);
        }

        public void InitRoom2(UInt64 accountId, string ownerName, EGameType game)
        {
            roomOwner.falseFangzhu = accountId;

            gameType = game;
            roomState = ERoomState.WaitRoomer;

            lstSeats.Add(EMajongDirection.East);
            lstSeats.Add(EMajongDirection.North);
            lstSeats.Add(EMajongDirection.West);
            lstSeats.Add(EMajongDirection.South);
        }

        [ProtoMember(1)]
        public UInt64 roomUuid { get; set; }

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(2)]
        public EGameType gameType { get; set; }

        /// <summary>
        /// 房间主人
        /// </summary>
        [ProtoMember(3)]
        public stRoomOwner roomOwner = new stRoomOwner();

        /// <summary>
        /// 房间人数
        /// </summary>
        [ProtoMember(4)]
        public List<YunChengRoomRole> lstRoles = new List<YunChengRoomRole>();

        /// <summary>
        /// 房间状态
        /// </summary>
        [ProtoMember(5)]
        public ERoomState roomState = ERoomState.WaitRoomer;

        /// <summary>
        /// 第几牌局
        /// </summary>
        [ProtoMember(6)]
        public UInt32 currentCardRound = 0;

        /// <summary>
        /// 房间配置
        /// </summary>
        [ProtoMember(7)]
        public RoomConfig config = new RoomConfig();

        /// <summary>
        /// 回合信息
        /// </summary>
        [ProtoMember(8)]
        public RoomRoundInfo currentRound = new RoomRoundInfo();

        /// <summary>
        /// 最终结算
        /// </summary>
        [ProtoMember(9)]
        public stFinalSettle finalSettle = new stFinalSettle();

        public const int MAX_ROLES_COUNT = 4;

        /// <summary>
        /// 出牌时间10秒
        /// </summary>
        public const int HANDOUT_CARD_SECOND = 10;

        /// <summary>
        /// 解散信息
        /// </summary>
        [ProtoMember(10)]
        public stDismissInfo dismissInfo = new stDismissInfo();

        [ProtoMember(11)]
        public List<EMajongDirection> lstSeats = new List<EMajongDirection>();

        /// <summary>
        /// 房间金牌类型
        /// </summary>
        [ProtoMember(12)]
        public List<EMJCardType> goldenType = new List<EMJCardType>();

        public ERoomState GetRoomState()
        {
            return roomState;
        }
        
        /// <summary>
        /// 设置房间状态
        /// </summary>
        /// <param name="state"></param>
        public void SetRoomState(ERoomState state)
        {
            roomState = state;
            if (ERoomState.Play == roomState)
            {
                //正在玩
                SetRolesState(ERoomRoleState.Play);
            }
            if (ERoomState.WaitRound == roomState)
            {
                //等待
                SetRolesState(ERoomRoleState.Settle);
            }
        }

        void SetRolesState(ERoomRoleState roleState)
        {
            foreach (var role in lstRoles)
            {
                role.roleState = roleState;
            }
        }

        public bool IsAllRoleWaitRound()
        {
            foreach (var role in lstRoles)
            {
                if (role.roleState != ERoomRoleState.WaitRound)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 是否是最后一局
        /// </summary>
        /// <returns></returns>
        public bool IsLastRound()
        {
            if (config.cardRound == ECardRound.OneRound)
            {
                return currentCardRound >= 1;
            }
            else if (config.cardRound == ECardRound.FourRound)
            {
                return currentCardRound >= 4;
            }
            else if (config.cardRound == ECardRound.EightRound)
            {
                return currentCardRound >= 8;
            }

            return true;
        }

        /// <summary>
        /// 运城贴金结算
        /// </summary>
        /// <returns></returns>
        public RoundSettleInfo GetSettleInfo()
        {
            RoundSettleInfo settleInfo = new RoundSettleInfo();
            settleInfo.bankerRoleId = currentRound.bankerRoleId;
            settleInfo.lstSettleItems = currentRound.lstSettleItems;

            foreach (var role in lstRoles)
            {
                RoundSettleRole item = new RoundSettleRole();
                item.roleid = role.roleid;
                item.nickName = role.nickName;
                item.handCards.AddRange(role.currentRound.handCards);
                item.showCards = role.currentRound.showCards;
                item.bRobGangHu = role.bRobGangHu;
                item.putGolden = role.currentRound.putGoldenList.Count;
                item.goldeType = goldenType;
                settleInfo.lstSettleRoles.Add(item);
            }

            // 是否有人胡牌
            if (IsSettleHu())
            {
                foreach (var item in currentRound.lstSettleItems)
                {
                    if (item.settleType == ERoundSettleType.Gang)
                    {
                        if (item.gangType == EGangCardType.AnGang)
                        {
                            settleInfo.AddScore(item.roleid, 6);
                            settleInfo.DelScoreExceptRole(item.roleid, 2);
                            settleInfo.SetMaxScales(item.roleid, 2);
                        }
                        else if (item.gangType == EGangCardType.BuGang)
                        {
                            settleInfo.AddScore(item.roleid, 3);
                            settleInfo.DelScoreExceptRole(item.roleid, 1);
                            settleInfo.SetMaxScales(item.roleid, 1);
                        }
                        else
                        {
                            if (item.bOtherBaoTing)
                            {
                                settleInfo.AddScore(item.roleid, 3);
                                settleInfo.DelScoreExceptRole(item.roleid, 1);
                                settleInfo.SetMaxScales(item.roleid, 1);
                            }
                            else
                            {
                                settleInfo.AddScore(item.roleid, 3);
                                settleInfo.DelScore(item.otherRoleId, 3);
                                settleInfo.SetMaxScales(item.roleid, 1);
                            }
                        }
                    }
                    else if (item.settleType == ERoundSettleType.Hu)
                    {
                        bool zhuanghu = false;
                        if(item.roleid == bankerRole.roleid)
                        {
                            //如果是庄家胡
                            zhuanghu = true;
                        }
                        int score = GetHuScales(item.huType, item.cardid, item.bSelfTouch,zhuanghu);
                        int huPaifen = 1;
                        if (EHuCardType.SenvenPairs == item.huType)
                        {
                            //七对*2
                            huPaifen *= 2;
                        }
                        else if (EHuCardType.SameColor == item.huType)
                        {
                            //清一色*2
                            huPaifen *= 2;
                        }
                        if (item.bSelfTouch)
                        {
                            huPaifen *= 2;
                        }


                        YunChengRoomRole role = GetRole(item.roleid);
                        int goldenNumber = 0;
                        switch (config.cappingNumber)
                        {
                            case CappingNumber.Two:
                                if (role.currentRound.putGoldenList.Count > 2)
                                {
                                    goldenNumber = 2;
                                }
                                else
                                {
                                    goldenNumber = role.currentRound.putGoldenList.Count;
                                }
                                break;
                            case CappingNumber.Three:
                                if(role.currentRound.putGoldenList.Count > 3)
                                {
                                    goldenNumber = 3;
                                }
                                else
                                {
                                    goldenNumber = role.currentRound.putGoldenList.Count;
                                }
                                break;
                            case CappingNumber.Four:
                                if(role.currentRound.putGoldenList.Count > 4)
                                {
                                    goldenNumber = 4;
                                }
                                else
                                {
                                    goldenNumber  = role.currentRound.putGoldenList.Count;
                                }
                                break;
                        }
                        if(goldenNumber == 1)
                        {
                            score += 4;
                        }
                        else if(goldenNumber == 2)
                        {
                            score += 12;
                        }
                        else if(goldenNumber == 3)
                        {
                            score += 36;
                        }
                        else if(goldenNumber == 3)
                        {
                            score += 108;
                        }

                        if (item.bSelfTouch)
                        {
                            if (zhuanghu)
                            {
                                settleInfo.AddScore(item.roleid, 3 * score);
                                settleInfo.DelScoreExceptRole(item.roleid, score);
                                settleInfo.SetMaxScales(item.roleid, score);
                            }
                            else
                            {
                                settleInfo.AddScore(item.roleid, 3 * score+1* huPaifen);
                                settleInfo.DelScoreExceptRoleBank(item.roleid, score, bankerRole.roleid, huPaifen);
                                settleInfo.SetMaxScales(item.roleid, score);
                            }
                        }
                        else
                        {
                            if (zhuanghu)
                            {
                                settleInfo.AddScore(item.roleid, 3 * score);
                            }
                            else
                            {
                                settleInfo.AddScore(item.roleid, 3 * score+1* huPaifen);
                            }

                            if (item.bOtherBaoTing)
                            {
                                if(zhuanghu)
                                {
                                    settleInfo.DelScoreExceptRole(item.roleid, score);
                                }
                                else
                                {
                                    settleInfo.DelScoreExceptRoleBank(item.roleid, score, bankerRole.roleid, huPaifen);
                                }
                            }
                            else
                            {
                                if (zhuanghu)
                                {
                                    settleInfo.DelScore(item.otherRoleId, score * 3);
                                }
                                else
                                {
                                    settleInfo.DelScore(item.otherRoleId, score * 3+1* huPaifen);
                                }
                            }
                            settleInfo.SetMaxScales(item.roleid, score);
                        }
                    }
                }
            }

            foreach (var item in settleInfo.lstSettleRoles)
            {
                YunChengRoomRole role = GetRole(item.roleid);
                if (null == role)
                {
                    continue;
                }
                role.score += item.score;

                finalSettle.ChangeScore(role.roleid, item.score);
            }

            return settleInfo;
        }

        bool IsSettleHu()
        {
            foreach (var item in currentRound.lstSettleItems)
            {
                if (item.settleType == ERoundSettleType.Hu)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 获得杠牌番数
        /// </summary>
        /// <param name="type"></param>
        /// <param name="cardid"></param>
        /// <returns></returns>
        int GetGangScales(EGangCardType type, UInt32 cardid)
        {
            int point = MJCardHelp.GetCardPoint(cardid);
            if (type == EGangCardType.AnGang)
            {
                point *= 2;
                return point;
            }

            return point;
        }

        /// <summary>
        /// 获得番数
        /// </summary>
        /// <param name="settleType"></param>
        /// <param name="cardid"></param>
        /// <param name="bSelfTouch">是否自摸</param>
        /// <returns></returns>
        public int GetHuScales(EHuCardType settleType, UInt32 cardid, bool bSelfTouch,bool zhuanghu)
        {
            //int point = MJCardHelp.GetCardPoint(cardid);
            //底分1分
            int point = 1;
            if (zhuanghu)
            {
                //庄家胡底分2
                point = 2;
            }
            if (bSelfTouch)
            {
                point *= 2;
            }

            if (EHuCardType.CommonHu == settleType)
            {
                return point;
            }
            else if(EHuCardType.SenvenPairs == settleType)
            {
                //七对*2
                point *= 2;
            }
            else if(EHuCardType.SameColor == settleType)
            {
                //清一色*2
                point *= 2;
            }

            return point;
        }

        /// <summary>
        /// 是否可以解散
        /// </summary>
        public bool IsInCanDismissState
        {
            get
            {
                if (roomState == ERoomState.WaitRound)
                {
                    return true;
                }

                if (roomState == ERoomState.Play)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 获得番数
        /// </summary>
        /// <param name="settleType"></param>
        /// <returns></returns>
        public int GetHuScales(EHuCardType settleType)
        {
            if (EHuCardType.ThirteenOrphans == settleType)
            {
                return 27;
            }

            if (EHuCardType.LuxurySevenPairs == settleType)
            {
                return 18;
            }

            if (EHuCardType.SameColor == settleType)
            {
                return 9;
            }

            if (EHuCardType.SenvenPairs == settleType)
            {
                return 9;
            }

            if (EHuCardType.OneDragon == settleType)
            {
                return 9;
            }

            if (EHuCardType.CommonHu == settleType)
            {
                return 3;
            }

            return 0;
        }

        public YunChengRoomRole GetRole(UInt64 roleid)
        {
            foreach (var role in lstRoles)
            {
                if (role.roleid == roleid)
                {
                    return role;
                }
            }

            return null;
        }

        public YunChengRoomRole GetRole(EMajongDirection direction)
        {
            foreach (var role in lstRoles)
            {
                if (role.direction == direction)
                {
                    return role;
                }
            }

            return null;
        }

        public YunChengRoomRole NextRole(YunChengRoomRole role)
        {
            EMajongDirection direction = role.direction + 1;
            if (direction == EMajongDirection.Max)
            {
                direction = EMajongDirection.East;
            }

            return GetRole(direction);
        }

        /// <summary>
        /// 获取其他用户
        /// </summary>
        /// <param name="role"></param>
        public List<YunChengRoomRole> NextRoles(YunChengRoomRole role)
        {
            List<YunChengRoomRole> tempRoles = new List<YunChengRoomRole>();
            for (int i = 0; i < 3; i++)
            {
                role = NextRole(role);
                tempRoles.Add(role);
            }
            return tempRoles;
        }

        /// <summary>
        /// 获得一个座位
        /// </summary>
        /// <returns></returns>
        public EMajongDirection GetOneSeat()
        {
            lstSeats.Sort();
            if (lstSeats.Count > 0)
            {
                EMajongDirection seat = lstSeats[0];
                lstSeats.Remove(seat);
                return seat;
            }
            return EMajongDirection.Null;
        }

        /// <summary>
        /// 归还座位
        /// </summary>
        /// <param name="seat"></param>
        public void ReturnSeat(EMajongDirection seat)
        {
            lstSeats.Add(seat);
        }

        public MahjongRoomInfo GetRoomInfo(UInt64 selfid)
        {
            MahjongRoomInfo info = new MahjongRoomInfo();
            info.roomUuid = roomUuid;
            info.roomState = roomState;
            foreach (var role in lstRoles)
            {
                if (selfid != role.roleid)
                {
                    info.lstRoles.Add(role.remoteInfo);
                }
            }

            info.localRoleInfo = GetRole(selfid).localInfo;
            info.localRoleInfo.remainSecond = dismissRemainSecond;

            //第几牌局
            info.currentCardRound = currentCardRound;

            // 剩余牌数
            info.remainCardCount = currentRound.poolRemainCards.Count;

            // 庄家
            info.bankerRoleId = currentRound.bankerRoleId;

            info.config = config;

            info.roomOwner = roomOwner;

            info.dismissInfo = dismissInfo;

            //金牌类型
            info.goldenType = goldenType;

            return info;
        }

        /// <summary>
        /// 解散倒计时
        /// </summary>
        public int dismissRemainSecond
        {
            get
            {
                if (dismissInfo.firstRoleId == 0)
                {
                    return 0;
                }

                if (env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return 0;
                }

                TimeSpan span = dismissInfo.expireTime - env.Timer.CurrentDateTime;
                return (int)span.TotalSeconds;
            }
        }

        /// <summary>
        /// 回合开始通知
        /// </summary>
        void SendRoundStartNtf()
        {
            foreach (var role in lstRoles)
            {
                // 回合开始通知
                MJYunChengRoundStartNtf roundStartNtf = new MJYunChengRoundStartNtf();
                roundStartNtf.roomInfo = GetRoomInfo(role.roleid);
                Console.Write("用户ID:"+ role.roleid+"{");
                foreach (UInt32 u in roundStartNtf.roomInfo.localRoleInfo.handCards)
                {
                    Console.Write(MJCardHelp.GetMJType(u).ToString()+",");
                }
                Console.Write("}");
                Console.WriteLine();
                role.SendMsgToClient(roundStartNtf);
            }
        }

        public void SendRoomMsg(ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {
                role.SendMsgToClient(msg);
            }
        }

        public void SendRoomMsgEx(UInt64 roleid, ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {
                if (role.roleid == roleid)
                {
                    continue;
                }
                role.SendMsgToClient(msg);
            }
        }

        public bool IsRoomFull()
        {
            return lstRoles.Count >= MAX_ROLES_COUNT;
        }

        public void AddRole(YunChengRoomRole role)
        {
            lstRoles.Add(role);
        }

        public void RemoveRole(UInt64 roleid)
        {
            YunChengRoomRole role = GetRole(roleid);

            ReturnSeat(role.direction);

            lstRoles.Remove(role);
        }

        public string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.detail_point_room_, roomUuid.ToString());
                return key;
            }
        }

        public bool SaveDb(bool bAlways = false)
        {
            byte[] data;
            if (!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;

            if (bAlways)
            {
                redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);
            }
            else
            {
                redis.db.StringSet(dbKey, data, null, When.Exists, CommandFlags.FireAndForget);
            }

            return true;
        }

        public bool DeleteFromDb()
        {
            RedisSystem redis = env.Redis;

            redis.db.KeyDelete(dbKey, CommandFlags.FireAndForget);

            return true;
        }

        public async Task<bool> LoadFromDbAsync()
        {
            RedisSystem redis = env.Redis;

            RedisValue value = await redis.db.StringGetAsync(dbKey);
            if (value.IsNullOrEmpty)
            {
                return false;
            }

            if (!this.SerializeFromBinary(value))
            {
                return false;
            }

            foreach (var role in lstRoles)
            {
                role.Init(this);
            }

            return true;
        }

        Random random = new Random();

        //初始化牌
        List<UInt32> RandomCards()
        {
            List<UInt32> lstCards = new List<UInt32>();
            UInt32 min = (UInt32)EMJCard.Null + 1;
            UInt32 max = (UInt32)EMJCard.Max - 1;

            for (UInt32 i = min; i <= max; i++)
            {
                lstCards.Add(i);
            }

            List<UInt32> lstResults = new List<UInt32>();
            //洗牌
            while (lstCards.Count != 0)
            {
                //获取一个随机数 在牌的大小范围内
                int index = random.Next(lstCards.Count);
                UInt32 card = lstCards[index];
                lstResults.Add(card);
                lstCards.RemoveAt(index);
            }

            return lstResults;
        }

        //初始化金牌
        List<EMJCardType> RandomGoldenCards(out int index)
        {
            List<UInt32> lstType = new List<UInt32>();
            UInt32 min = (UInt32)EMJCardType.Null + 1;
            UInt32 max = (UInt32)EMJCardType.Max;

            for (UInt32 i = min; i <= max; i++)
            {
                lstType.Add(i);
            }
            List<EMJCardType> lstResults = new List<EMJCardType>();
            index = random.Next(1, lstType.Count);
            EMJCardType card = EMJCardType.Null;
            switch (index)
            {
                case 1:
                    card = EMJCardType.OneRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.NineRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.OneRope);
                        lstResults.Add(EMJCardType.TwoRope);
                    }
                    break;
                case 2:
                    card = EMJCardType.TwoRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.EightRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.OneRope);
                        lstResults.Add(EMJCardType.ThreeRope);
                    }
                    break;
                case 3:
                    card = EMJCardType.ThreeRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.SevenRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.TwoRope);
                        lstResults.Add(EMJCardType.FourRope);
                    }
                    break;
                case 4:
                    card = EMJCardType.FourRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.SixRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.ThreeRope);
                        lstResults.Add(EMJCardType.FiveRope);
                    }
                    break;
                case 5:
                    card = EMJCardType.FiveRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(card);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.FourRope);
                        lstResults.Add(EMJCardType.SixRope);
                    }
                    break;
                case 6:
                    card = EMJCardType.SixRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.FourRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.FiveRope);
                        lstResults.Add(EMJCardType.SevenRope);
                    }
                    break;
                case 7:
                    card = EMJCardType.SevenRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.ThreeRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SixRope);
                        lstResults.Add(EMJCardType.EightRope);
                    }
                    break;
                case 8:
                    card = EMJCardType.EightRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.TwoRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SevenRope);
                        lstResults.Add(EMJCardType.NineRope);
                    }
                    break;
                case 9:
                    card = EMJCardType.NineRope;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.OneRope);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.EightRope);
                        lstResults.Add(EMJCardType.NineRope);
                    }
                    break;
                case 10:
                    card = EMJCardType.OneBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.NineBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.OneBucket);
                        lstResults.Add(EMJCardType.TwoBucket);
                    }
                    break;
                case 11:
                    card = EMJCardType.TwoBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.EightBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.OneBucket);
                        lstResults.Add(EMJCardType.ThreeBucket);
                    }
                    break;
                case 12:
                    card = EMJCardType.ThreeBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.SevenBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.TwoBucket);
                        lstResults.Add(EMJCardType.FourBucket);
                    }
                    break;
                case 13:
                    card = EMJCardType.FourBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.SixBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.ThreeBucket);
                        lstResults.Add(EMJCardType.FiveBucket);
                    }
                    break;
                case 14:
                    card = EMJCardType.FiveBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(card);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.FourBucket);
                        lstResults.Add(EMJCardType.SixBucket);
                    }
                    break;
                case 15:
                    card = EMJCardType.SixBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.FourBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.FiveBucket);
                        lstResults.Add(EMJCardType.SevenBucket);
                    }
                    break;
                case 16:
                    card = EMJCardType.SevenBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.ThreeBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SixBucket);
                        lstResults.Add(EMJCardType.EightBucket);
                    }
                    break;
                case 17:
                    card = EMJCardType.EightBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.TwoBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SevenBucket);
                        lstResults.Add(EMJCardType.NineBucket);
                    }
                    break;
                case 18:
                    card = EMJCardType.NineBucket;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.OneBucket);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.EightBucket);
                        lstResults.Add(EMJCardType.NineBucket);
                    }
                    break;
                case 19:
                    card = EMJCardType.OneMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.NineMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.OneMillion);
                        lstResults.Add(EMJCardType.TwoMillion);
                    }
                    break;
                case 20:
                    card = EMJCardType.TwoMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.EightMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.OneMillion);
                        lstResults.Add(EMJCardType.ThreeMillion);
                    }
                    break;
                case 21:
                    card = EMJCardType.ThreeMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.SevenMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.TwoMillion);
                        lstResults.Add(EMJCardType.FourMillion);
                    }
                    break;
                case 22:
                    card = EMJCardType.FourMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.SixMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.ThreeMillion);
                        lstResults.Add(EMJCardType.FiveMillion);
                    }
                    break;
                case 23:
                    card = EMJCardType.FiveMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(card);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.FourMillion);
                        lstResults.Add(EMJCardType.SixMillion);
                    }
                    break;
                case 24:
                    card = EMJCardType.SixMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.FourMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.FiveMillion);
                        lstResults.Add(EMJCardType.SevenMillion);
                    }
                    break;
                case 25:
                    card = EMJCardType.SevenMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.ThreeMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SixMillion);
                        lstResults.Add(EMJCardType.EightMillion);
                    }
                    break;
                case 26:
                    card = EMJCardType.EightMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.TwoMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SevenMillion);
                        lstResults.Add(EMJCardType.NineMillion);
                    }
                    break;
                case 27:
                    card = EMJCardType.NineMillion;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.OneMillion);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.EightMillion);
                        lstResults.Add(EMJCardType.NineMillion);
                    }
                    break;
                case 28:
                    card = EMJCardType.EastWind;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.WestWind);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SouthWind);
                        lstResults.Add(EMJCardType.NorthWind);
                    }
                    break;
                case 29:
                    card = EMJCardType.SouthWind;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.NorthWind);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.EastWind);
                        lstResults.Add(EMJCardType.WestWind);
                    }
                    break;
                case 30:
                    card = EMJCardType.WestWind;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.EastWind);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.SouthWind);
                        lstResults.Add(EMJCardType.NorthWind);
                    }
                    break;
                case 31:
                    card = EMJCardType.NorthWind;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.SouthWind);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.EastWind);
                        lstResults.Add(EMJCardType.WestWind);
                    }
                    break;
                case 32:
                    card = EMJCardType.RedDragon;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.WhiteFace);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.Fortune);
                        lstResults.Add(EMJCardType.WhiteFace);
                    }
                    break;
                case 33:
                    card = EMJCardType.Fortune;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.Fortune);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.WhiteFace);
                        lstResults.Add(EMJCardType.RedDragon);
                    }
                    break;
                case 34:
                    card = EMJCardType.WhiteFace;
                    if (config.goldens == GoldenNumber.FourGolden)
                    {
                        lstResults.Add(EMJCardType.RedDragon);
                    }
                    else
                    {
                        lstResults.Add(EMJCardType.RedDragon);
                        lstResults.Add(EMJCardType.Fortune);
                    }
                    break;
            }
            

            return lstResults;
        }

        public void FirstInit()
        {
            foreach (var role in lstRoles)
            {
                finalSettle.InitScore(role.roleid);
            }
        }

        public void StartNewRound()
        {
            InitNewRound();

            SetRoomState(ERoomState.Play);

            SendRoundStartNtf();

            bankerRole.DrawNewCard();
        }

        /// <summary>
        /// 庄家
        /// </summary>
        public YunChengRoomRole bankerRole
        {
            get
            {
                YunChengRoomRole role = GetRole(currentRound.bankerRoleId);
                return role;
            }
        }

        /// <summary>
        /// 处理结算
        /// </summary>
        public void Settle()
        {
            MJYunChengRoundSettleNtf ntf = new MJYunChengRoundSettleNtf();
            ntf.settleInfo = GetSettleInfo();
            ntf.IsRoomOver = IsLastRound();
            ntf.finalSettle = finalSettle;
            SendRoomMsg(ntf);

            if (IsLastRound())
            {
                RoomClose();
                return;
            }

            // 切换到结算状态
            SetRoomState(ERoomState.WaitRound);
        }

        /// <summary>
        /// 获取战绩
        /// </summary>
        /// <returns></returns>
        stBattleScoreItem GetBattleScoreItem()
        {
            stBattleScoreItem scoreItem = new stBattleScoreItem();
            scoreItem.dateTime = env.Timer.CurrentDateTime;
            scoreItem.roomUuid = roomUuid;
            scoreItem.gameType = EGameType.MJYunCheng;
            scoreItem.roomOwner = roomOwner;

            foreach (var item in finalSettle.mapScores)
            {
                YunChengRoomRole role = GetRole(item.Key);
                if (null == role)
                {
                    continue;
                }
                stBattleScoreRole scoreRole = new stBattleScoreRole();
                scoreRole.roleid = role.roleid;
                scoreRole.nickName = role.nickName;
                scoreRole.score = item.Value;
                scoreItem.lstRoles.Add(scoreRole);
            }
            return scoreItem;
        }

        /// <summary>
        /// 检测解散超时
        /// </summary>
        public bool CheckDismissTimer()
        {
            if (dismissInfo.bStart)
            {
                if (env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return true;
                }
            }

            return false;
        }

        public void NotifyDismissSuccess()
        {
            MJYunChengDismissGameSuccessNtf successNtf = new MJYunChengDismissGameSuccessNtf();
            successNtf.bSuccess = true;
            successNtf.finalSettle = finalSettle;
            SendRoomMsg(successNtf);
        }

        /// <summary>
        /// 关闭房间
        /// </summary>
        public void RoomClose()
        {
            foreach (var role in lstRoles)
            {
                BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                if (null == player)
                {
                    continue;
                }

                // 解绑战斗服
                player.UnBindBattleFromGate();

                g.playerMgr.RemovePlayer(role.roleid);
            }

            MJYunChengRoomCloseB2L closeNtf = new MJYunChengRoomCloseB2L();
            closeNtf.roomUuid = roomUuid;
            if(roomOwner.roleid != 0)
            {
                closeNtf.roomOwnerId = roomOwner.roleid;
            }
            else
            {
                closeNtf.roomOwnerId = roomOwner.falseFangzhu;
            }
            if (roomState == ERoomState.WaitRoomer)
            {
                closeNtf.bReturnRoomCard = true;
            }
            closeNtf.bRecordBattleScore = isRecordBattleScore;
            closeNtf.scoreItem = GetBattleScoreItem();
            SendMsgToLobby(closeNtf);

            g.yunChengRoomMgr.DeleteRoom(roomUuid);
        }

        /// <summary>
        /// 是否记录战绩
        /// </summary>
        public bool isRecordBattleScore
        {
            get
            {
                return roomState != ERoomState.WaitRoomer;
            }
        }

        void SendMsgToLobby(ProtoBody proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }

        /// <summary>
        /// 处理流局
        /// </summary>
        public void OnFlowRound()
        {
            // 处理结算
            Settle();
        }

        public UInt64 NextRoleId(UInt64 roleid)
        {
            YunChengRoomRole role = GetRole(roleid);
            return NextRole(role).roleid;
        }

        void InitNewRound()
        {
            currentCardRound++;

            if (currentCardRound == 1)
            {
                //第一局的庄家是创建房间的人
                if(config.payMethod == PayMethod.anotherPay)
                {
                    currentRound.bankerRoleId = lstRoles[0].roleid;
                }
                else
                {
                    currentRound.bankerRoleId = roomOwner.roleid;
                }

                foreach (var role in lstRoles)
                {
                    // TODO 临时添加
                    role.currentRound.testRoomUuid = roomUuid;
                    role.currentRound.testRoleId = role.roleid;
                }
            }
            else
            {
                UInt64 bankerId = 0;
                // 上回合有没有流局
                if (currentRound.huRoleId == 0)
                {   
                    //上回合流局 庄家取下一个
                    bankerId = NextRoleId(currentRound.bankerRoleId);
                }
                else
                {
                    if(currentRound.bankerRoleId == currentRound.huRoleId)
                    {
                        //没有流局 庄家是胡家
                        bankerId = currentRound.huRoleId;
                    }
                    else
                    {
                        bankerId = NextRoleId(currentRound.bankerRoleId);
                    }
                }

                currentRound.Clear();
                foreach (var role in lstRoles)
                {
                    role.currentRound.Clear();
                }

                currentRound.bankerRoleId = bankerId;
            }
            
            //初始化牌
            currentRound.poolRemainCards = RandomCards();

            int index;

            //设置金牌类型
            goldenType = RandomGoldenCards(out index);

            //牌堆里删掉一张index
            for(int i = 0; i < currentRound.poolRemainCards.Count;i++)
            {
                if(((int)MJCardHelp.GetMJType(currentRound.poolRemainCards[i])) == index)
                {
                    currentRound.poolRemainCards.RemoveAt(i);
                    break;
                }
            }

            List<UInt32> lstTemps = new List<uint>();
            lstTemps.AddRange(currentRound.poolRemainCards);
            LogSys.Info("[RandomCards],牌池内容1,allcards[{0}]", ToString(lstTemps));
            lstTemps.Sort();
            LogSys.Info("[RandomCards],牌池内容2,allcards[{0}]", ToString(lstTemps));

            // 第一个人14张牌
            YunChengRoomRole firstRole = GetRole(currentRound.bankerRoleId);
            firstRole.currentRound.handCards = fetchfirst13Cards(currentRound.poolRemainCards);
            firstRole.currentRound.handGoldenList = fetchGoldenCards(firstRole.currentRound.handCards);
            firstRole.currentRound.playState = ERolePlayState.Wait;

            //测试代码
            //firstRole.currentRound.handCards = TestHandCards();
            //firstRole.currentRound.showCards.Add(TestShowCards());
            //firstRole.nextRole.currentRound.handCards = TestHandCards1();
            //firstRole.nextRole.currentRound.playState = ERolePlayState.Wait;

            // 其他三个人
            foreach (var role in lstRoles)
            {
                if (role.roleid == currentRound.bankerRoleId)
                {
                    //如果是庄家不发牌
                    continue;
                }

                //测试代码
                //if (role.roleid == firstRole.nextRole.roleid)
                //{
                //    continue;
                //}

                YunChengRoomRole secondRole = GetRole(role.roleid);
                secondRole.currentRound.handCards = fetch13Cards(currentRound.poolRemainCards);
                secondRole.currentRound.handGoldenList = fetchGoldenCards(secondRole.currentRound.handCards);
                secondRole.currentRound.playState = ERolePlayState.Wait;
            }

            foreach (var role in lstRoles)
            {
                role.bRobGangHu = false;
            }

            

            //currentRound.poolRemainCards.Remove((UInt32)EMJCard.NineRope4);
            //currentRound.poolRemainCards.Insert(3, (UInt32)EMJCard.NineRope4);
            //int count = 0;
            //foreach (var role in lstRoles)
            //{
            //    PointRoomRole secondRole = GetRole(role.roleid);
            //    count++;
            //    if (count == 1)
            //    {
            //        secondRole.currentRound.handCards = TestCards1(currentRound.poolRemainCards);
            //    }
            //    else if (count == 2)
            //    {
            //        secondRole.currentRound.handCards = TestCards2(currentRound.poolRemainCards);
            //    }
            //    else if (count == 3)
            //    {
            //        secondRole.currentRound.handCards = TestCards3(currentRound.poolRemainCards);
            //    }
            //    else if (count == 4)
            //    {
            //        secondRole.currentRound.handCards = TestCards4(currentRound.poolRemainCards);
            //    }

            //    secondRole.currentRound.playState = ERolePlayState.Wait;
            //}
        }

        List<UInt32> fetchGoldenCards(HashSet<UInt32> handCards)
        {
            List<UInt32> listResult = new List<UInt32>();
            foreach (UInt32 ui in handCards)
            {
                if (goldenType.Count == 1)
                {
                    if (goldenType[0] == MJCardHelp.GetMJType(ui))
                    {
                        listResult.Add(ui);
                    }
                }
                else if (goldenType.Count == 2)
                {
                    if (goldenType[0] == MJCardHelp.GetMJType(ui)
                        || goldenType[1] == MJCardHelp.GetMJType(ui))
                    {
                        listResult.Add(ui);
                    }
                }
            }
            return listResult;
        }

        public HashSet<UInt32> TestHandCards()
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            lstResults.Add((uint)EMJCard.OneBucket1);
            lstResults.Add((uint)EMJCard.TwoBucket1);
            lstResults.Add((uint)EMJCard.ThreeBucket1);
            lstResults.Add((uint)EMJCard.FiveBucket1);
            lstResults.Add((uint)EMJCard.FourBucket1);
            lstResults.Add((uint)EMJCard.SixBucket1);
            lstResults.Add((uint)EMJCard.SevenRope1);
            lstResults.Add((uint)EMJCard.EightRope1);
            lstResults.Add((uint)EMJCard.FiveBucket3);
            lstResults.Add((uint)EMJCard.FiveBucket2);
            lstResults.Add((uint)EMJCard.NineRope1);
            lstResults.Add((uint)EMJCard.NineRope2);
            lstResults.Add((uint)EMJCard.NineRope3);

            foreach (var item in lstResults)
            {
                if (currentRound.poolRemainCards.Contains(item))
                {
                    currentRound.poolRemainCards.Remove(item);
                }
            }
            return lstResults;
        }

        /// <summary>
        /// 测试:手牌
        /// </summary>
        /// <returns></returns>
        public MingCardItem TestShowCards()
        {
            List<UInt32> lstResults = new List<UInt32>();
            lstResults.Add((uint)EMJCard.SixMillion3);
            lstResults.Add((uint)EMJCard.SixMillion2);
            lstResults.Add((uint)EMJCard.SixMillion1);

            foreach (var item in lstResults)
            {
                if (currentRound.poolRemainCards.Contains(item))
                {
                    currentRound.poolRemainCards.Remove(item);
                }
            }

            MingCardItem card = new MingCardItem();
            card.showCards = lstResults;
            card.mingType = EMingCardType.Peng;
            card.cardType = EMJCardType.SixMillion;

            return card;
        }

        public HashSet<UInt32> TestHandCards1()
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            lstResults.Add((uint)EMJCard.OneMillion1);
            lstResults.Add((uint)EMJCard.TwoMillion1);
            lstResults.Add((uint)EMJCard.ThreeMillion1);
            lstResults.Add((uint)EMJCard.OneMillion2);
            lstResults.Add((uint)EMJCard.TwoMillion2);
            lstResults.Add((uint)EMJCard.ThreeMillion2);
            lstResults.Add((uint)EMJCard.SevenMillion1);
            lstResults.Add((uint)EMJCard.EightMillion1);
            lstResults.Add((uint)EMJCard.NineMillion1);
            lstResults.Add((uint)EMJCard.OneRope1);
            lstResults.Add((uint)EMJCard.OneRope2);
            lstResults.Add((uint)EMJCard.SevenRope4);
            lstResults.Add((uint)EMJCard.EightRope4);

            foreach (var item in lstResults)
            {
                if (currentRound.poolRemainCards.Contains(item))
                {
                    currentRound.poolRemainCards.Remove(item);
                }
            }
            return lstResults;
        }

        public static string ToString(List<UInt32> lstCards)
        {
            string temp = "";
            foreach (var cardid in lstCards)
            {
                temp += cardid.ToString();
                temp += ",";
            }
            return temp;
        }

        public static string ToString(HashSet<UInt32> lstCards)
        {
            string temp = "";
            foreach (var cardid in lstCards)
            {
                temp += cardid.ToString();
                temp += ",";
            }
            return temp;
        }

        HashSet<UInt32> fetch13Cards(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            for (int i = 0; i < 13; i++)
            {
                UInt32 card = lstCards[0];
                lstResults.Add(card);
                lstCards.RemoveAt(0);
            }
            return lstResults;
        }

        HashSet<UInt32> fetchfirst13Cards(List<UInt32> lstCards)
        {
            //return GetSpecialCards(lstCards);

            HashSet<UInt32> lstResults = new HashSet<UInt32>();
            for (int i = 0; i < 13; i++)
            {
                UInt32 card = lstCards[0];
                lstResults.Add(card);
                lstCards.RemoveAt(0);
            }
            return lstResults;
        }

        public void OnRoleOnline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = true;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);
        }

        public void OnRoleOffline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = false;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);

            if (IsAllRoleOffline)
            {
                g.pointRoomMgr.RemoveRoom(roomUuid);

                MJYunChengAllRoleOfflineB2L offlineNtf = new MJYunChengAllRoleOfflineB2L();
                offlineNtf.roomUuid = roomUuid;
                SendMsgToLobby(ntf);
            }
        }

        public void SendMsgToLobby(Protocol proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }

        /// <summary>
        /// 房间内所有人都下线了
        /// </summary>
        /// <returns></returns>
        bool IsAllRoleOffline
        {
            get
            {
                foreach (var role in lstRoles)
                {
                    BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                    if (player != null)
                    {
                        if (player.isOnline)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }

        //HashSet<UInt32> GetSpecialCards(List<UInt32> lstCards)
        //{
        //    HashSet<UInt32> lstResults = new HashSet<UInt32>();
        //    lstResults.Add((UInt32)EMJCard.EightMillion1);
        //    lstResults.Add((UInt32)EMJCard.EightMillion2);
        //    lstResults.Add((UInt32)EMJCard.EightMillion3);
        //    lstResults.Add((UInt32)EMJCard.EightMillion4);

        //    lstResults.Add((UInt32)EMJCard.ThreeMillion2);
        //    lstResults.Add((UInt32)EMJCard.ThreeMillion3);

        //    lstResults.Add((UInt32)EMJCard.SixMillion1);
        //    lstResults.Add((UInt32)EMJCard.SixMillion2);

        //    lstResults.Add((UInt32)EMJCard.EightBucket1);
        //    lstResults.Add((UInt32)EMJCard.EightBucket2);

        //    lstResults.Add((UInt32)EMJCard.OneBucket1);
        //    lstResults.Add((UInt32)EMJCard.TwoBucket1);

        //    lstResults.Add((UInt32)EMJCard.FourBucket1);
        //    lstResults.Add((UInt32)EMJCard.FourBucket2);

        //    foreach (var cardid in lstResults)
        //    {
        //        lstCards.Remove(cardid);
        //    }

        //    return lstResults;
        //}

        HashSet<UInt32> TestCards1(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.OneMillion1);
            lstResults.Add((UInt32)EMJCard.OneMillion2);
            lstResults.Add((UInt32)EMJCard.OneMillion3);

            lstResults.Add((UInt32)EMJCard.ThreeMillion1);
            lstResults.Add((UInt32)EMJCard.ThreeMillion2);
            lstResults.Add((UInt32)EMJCard.ThreeMillion3);

            lstResults.Add((UInt32)EMJCard.FourMillion1);

            lstResults.Add((UInt32)EMJCard.SixMillion1);
            lstResults.Add((UInt32)EMJCard.SixMillion2);
            lstResults.Add((UInt32)EMJCard.SixMillion3);

            lstResults.Add((UInt32)EMJCard.NineMillion1);
            lstResults.Add((UInt32)EMJCard.NineMillion2);
            lstResults.Add((UInt32)EMJCard.NineMillion3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }

        HashSet<UInt32> TestCards2(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.OneRope1);
            lstResults.Add((UInt32)EMJCard.OneRope2);
            lstResults.Add((UInt32)EMJCard.OneRope3);

            lstResults.Add((UInt32)EMJCard.ThreeRope1);
            lstResults.Add((UInt32)EMJCard.ThreeRope2);
            lstResults.Add((UInt32)EMJCard.ThreeRope3);

            lstResults.Add((UInt32)EMJCard.FourRope1);

            lstResults.Add((UInt32)EMJCard.SixRope1);
            lstResults.Add((UInt32)EMJCard.SixRope2);
            lstResults.Add((UInt32)EMJCard.SixRope3);

            lstResults.Add((UInt32)EMJCard.NineRope1);
            lstResults.Add((UInt32)EMJCard.NineRope2);
            lstResults.Add((UInt32)EMJCard.NineRope3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }

        HashSet<UInt32> TestCards3(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.OneBucket1);
            lstResults.Add((UInt32)EMJCard.OneBucket2);
            lstResults.Add((UInt32)EMJCard.OneBucket3);

            lstResults.Add((UInt32)EMJCard.ThreeBucket1);
            lstResults.Add((UInt32)EMJCard.ThreeBucket2);
            lstResults.Add((UInt32)EMJCard.ThreeBucket3);

            lstResults.Add((UInt32)EMJCard.FourBucket1);

            lstResults.Add((UInt32)EMJCard.SixBucket1);
            lstResults.Add((UInt32)EMJCard.SixBucket2);
            lstResults.Add((UInt32)EMJCard.SixBucket3);

            lstResults.Add((UInt32)EMJCard.NineBucket1);
            lstResults.Add((UInt32)EMJCard.NineBucket2);
            lstResults.Add((UInt32)EMJCard.NineBucket3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }

        HashSet<UInt32> TestCards4(List<UInt32> lstCards)
        {
            HashSet<UInt32> lstResults = new HashSet<UInt32>();

            lstResults.Add((UInt32)EMJCard.TwoBucket1);
            lstResults.Add((UInt32)EMJCard.TwoBucket2);
            lstResults.Add((UInt32)EMJCard.TwoBucket3);

            lstResults.Add((UInt32)EMJCard.TwoRope1);
            lstResults.Add((UInt32)EMJCard.TwoRope2);
            lstResults.Add((UInt32)EMJCard.TwoRope3);

            lstResults.Add((UInt32)EMJCard.SevenRope1);

            lstResults.Add((UInt32)EMJCard.TwoMillion1);
            lstResults.Add((UInt32)EMJCard.TwoMillion2);
            lstResults.Add((UInt32)EMJCard.TwoMillion3);

            lstResults.Add((UInt32)EMJCard.EightMillion1);
            lstResults.Add((UInt32)EMJCard.EightMillion2);
            lstResults.Add((UInt32)EMJCard.EightMillion3);

            foreach (var cardid in lstResults)
            {
                lstCards.Remove(cardid);
            }

            return lstResults;
        }

    }
}
