﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using BattleServer;
using Protocols.Landlord;
using HelpAsset;

namespace Landlord
{
    public enum ERobLordResult
    {
        Null =  0, // 默认
        Success = 1, // 成功
        Failed = 2, // 失败
        Continue = 3, // 继续
    }

    /// <summary>
    /// 抢地主条目
    /// </summary>
    [ProtoContract]
    public class stRobLordItem
    {
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 是否抢地主
        /// </summary>
        [ProtoMember(2)]
        public int jiaofen = 0;
    }

    /// <summary>
    /// 回合信息
    /// </summary>
    [ProtoContract]
    public class RoomRoundInfo
    {
        /// <summary>
        /// 底牌
        /// </summary>
        [ProtoMember(1)]
        public List<UInt32> bottomCards = new List<UInt32>();

        /// <summary>
        /// 地主
        /// </summary>
        [ProtoMember(2)]
        public UInt64 lordRoleId = 0;

        /// <summary>
        /// 庄家
        /// </summary>
        [ProtoMember(3)]
        public UInt64 bankerRoleId = 0;

        /// <summary>
        /// 该回合最先出完牌的人
        /// </summary>
        [ProtoMember(4)]
        public UInt64 winRoleId = 0;

        /// <summary>
        /// 公共倍数
        /// </summary>
        [ProtoMember(5)]
        public stPublicScales publicScales = new stPublicScales();

        /// <summary>
        /// 抢地主
        /// </summary>
        [ProtoMember(6)]
        public List<stRobLordItem> lstRobLordRoles = new List<stRobLordItem>();

        /// <summary>
        /// 上次出牌信息
        /// </summary>
        [ProtoMember(7)]
        public stOutCardItem lastOutCardItem = new stOutCardItem();

        /// <summary>
        /// 不叫地主的角色列表
        /// </summary>
        [ProtoMember(8)]
        public HashSet<UInt64> setNotCallLords = new HashSet<ulong>();

        /// <summary>
        /// 当前出牌玩家手中是否只剩下一张癞子 如果这样下家出什么牌都可以
        /// </summary>
        [ProtoMember(10)]
        public bool laiziCard = false;

        /// <summary>
        /// 当前回合所叫的最高分
        /// </summary>
        [ProtoMember(11)]
        public int maxFen = 0;

        public void Clear()
        {
            bottomCards.Clear();
            lordRoleId = 0;
            bankerRoleId = 0;
            winRoleId = 0;
            publicScales.Clear();
            lstRobLordRoles.Clear();
            lastOutCardItem.Clear();
            setNotCallLords.Clear();
            laiziCard = false;
            maxFen = 0;
        }

        public void SetMaxMingCardScales(int scale)
        {
            if (scale > publicScales.mingScales)
            {
                publicScales.mingScales = scale;
            }
        }

        public void AddRobRole(UInt64 roleid, int jiaofen)
        {
            stRobLordItem item = new stRobLordItem();
            item.roleid = roleid;
            item.jiaofen = jiaofen;
            lstRobLordRoles.Add(item);
        }

        /// <summary>
        /// 抢地主倍数
        /// </summary>
        public int CalRobScales()
        {
            /*
             
            if (lstRobLordRoles.Count == 0)
            {
                return 1;
            }

            int scales = 1;

            for (int i = 1; i < lstRobLordRoles.Count; i++)
            {
                stRobLordItem item = lstRobLordRoles[i];
                if (item.bRob)
                {
                    scales *= 2;
                }
            }
            

            return scales;
            */
            return 0;
        }

        /*
        /// <summary>
        /// 抢地主结果
        /// </summary>
        /// <param name="outRoleId"></param>
        /// <returns></returns>
        public ERobLordResult RobLordResult(ref UInt64 outRoleId, ref UInt64 nextRobRoleId)
        {
            if (lstRobLordRoles.Count < 3)
            {
                return ERobLordResult.Continue;
            }

            if(lstRobLordRoles.Count == 3)
            {
                var lstResults = (from item in lstRobLordRoles where item.jiaofen select item).ToList();

                if(lstResults.Count == 0)
                {
                    return ERobLordResult.Failed;
                }
                else if(lstResults.Count == 1)
                {
                    outRoleId = lstResults[0].roleid;
                    return ERobLordResult.Success;
                }
                else
                {
                    nextRobRoleId = lstResults[0].roleid;
                    return ERobLordResult.Continue;
                }
            }
            else
            {
                var lstResults = (from item in lstRobLordRoles where item.bRob select item).ToList();
                outRoleId = lstResults[lstResults.Count - 1].roleid;
                return ERobLordResult.Success;
            }
        }
    */
    }

    [ProtoContract]
    public class LordRoom
    {
        public LordRoom(UInt64 uuid)
        {
            roomUuid = uuid;
        }

        public void InitRoom(UInt64 ownerid, string ownerName, EGameType game)
        {
            roomOwner.roleid = ownerid;
            roomOwner.nickName = ownerName;
            gameType = game;
            roomState = ERoomState.WaitRoomer;

            lstSeats.Add(ESeatPostion.First);
            lstSeats.Add(ESeatPostion.Second);
            lstSeats.Add(ESeatPostion.Third);
        }

        public void InitRoom2(UInt64 ownerid,string ownerName, EGameType game)
        {
            roomOwner.falseFangzhu = ownerid;
            gameType = game;
            roomState = ERoomState.WaitRoomer;

            lstSeats.Add(ESeatPostion.First);
            lstSeats.Add(ESeatPostion.Second);
            lstSeats.Add(ESeatPostion.Third);
        }

        [ProtoMember(1)]
        public UInt64 roomUuid { get; set; }

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(2)]
        public EGameType gameType { get; set; }

        /// <summary>
        /// 房间主人
        /// </summary>
        [ProtoMember(3)]
        public stRoomOwner roomOwner = new stRoomOwner();

        /// <summary>
        /// 房间人数
        /// </summary>
        [ProtoMember(4)]
        public List<LordRoomRole> lstRoles = new List<LordRoomRole>();

        /// <summary>
        /// 房间状态
        /// </summary>
        [ProtoMember(5)]
        public ERoomState roomState = ERoomState.WaitRoomer;

        /// <summary>
        /// 第几牌局
        /// </summary>
        [ProtoMember(6)]
        public UInt32 currentCardRound = 0;

        /// <summary>
        /// 房间配置
        /// </summary>
        [ProtoMember(7)]
        public RoomConfig config = new RoomConfig();

        /// <summary>
        /// 回合信息
        /// </summary>
        [ProtoMember(8)]
        public RoomRoundInfo currentRound = new RoomRoundInfo();

        /// <summary>
        /// 最终结算
        /// </summary>
        [ProtoMember(9)]
        public stFinalSettle finalSettle = new stFinalSettle();

        public const int MAX_ROLES_COUNT = 3;

        /// <summary>
        /// 出牌时间10秒
        /// </summary>
        public const int HANDOUT_CARD_SECOND = 10;

        /// <summary>
        /// 解散信息
        /// </summary>
        [ProtoMember(10)]
        public stDismissInfo dismissInfo = new stDismissInfo();

        /// <summary>
        /// 座位列表
        /// </summary>
        [ProtoMember(11)]
        public List<ESeatPostion> lstSeats = new List<ESeatPostion>();

        /// <summary>
        /// 是否春天
        /// </summary>
        public bool isSpring
        {
            get
            {
                if(lordRole.currentRound.handCards.Count != 0)
                {
                    return false;
                }

                // 农民出过牌
                foreach(var role in lstRoles)
                {
                    if(role.IsFarmer)
                    {
                        if(role.currentRound.handOutCardTimes >= 1)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// 是否是反春天
        /// </summary>
        public bool isRSpring
        {
            get
            {
                // 地主出过2次牌
                if(lordRole.currentRound.handOutCardTimes >= 2)
                {
                    return false;
                }

                foreach(var role in lstRoles)
                {
                    if(role.IsFarmer)
                    {
                        if(role.currentRound.handCards.Count == 0)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        public void Tick()
        {
            foreach (var role in lstRoles)
            {
                role.OnTick();            
            }
        }

        public ERoomState GetRoomState()
        {
            return roomState;
        }

        public void SetRoomState(ERoomState state)
        {
            roomState = state;
            if (ERoomState.Play == roomState)
            {
                SetRolesState(ERoomRoleState.Play);
            }
            if (ERoomState.WaitRound == roomState)
            {
                SetRolesState(ERoomRoleState.Settle);
            }
        }

        void SetRolesState(ERoomRoleState roleState)
        {
            foreach (var role in lstRoles)
            {
                role.roleState = roleState;
            }
        }

        /// <summary>
        /// 是否是最后一局
        /// </summary>
        /// <returns></returns>
        public bool IsLastRound()
        {
            if (config.roomRound == ERoomRound.OneRound)
            {
                return currentCardRound >= 1;
            }
            else if (config.roomRound == ERoomRound.FourRound)
            {
                return currentCardRound >= 4;
            }
            else if (config.roomRound == ERoomRound.EightRound)
            {
                return currentCardRound >= 8;
            }

            return true;
        }

        /// <summary>
        /// 斗地主结算
        /// </summary>
        /// <returns></returns>
        public RoundSettleInfo GetSettleInfo()
        {
            RoundSettleInfo settleInfo = new RoundSettleInfo();
            settleInfo.publicScales = currentRound.publicScales;
            settleInfo.winRoleId = currentRound.winRoleId;

            foreach (var role in lstRoles)
            {
                RoundSettleRole item = new RoundSettleRole();
                item.roleid = role.roleid;
                item.nickName = role.nickName;
                item.scales = role.currentRound.scales;
                item.bLandlord = role.IsLandlord;
                item.roundScore = role.roundScore;
                item.handCards.AddRange(role.currentRound.handCards);

                settleInfo.lstSettleRoles.Add(item);
            }

            foreach (var item in settleInfo.lstSettleRoles)
            {
                LordRoomRole role = GetRole(item.roleid);
                if (null == role)
                {
                    continue;
                }
                role.score += item.roundScore;

                finalSettle.ChangeScore(role.roleid, item.roundScore);
            }

            return settleInfo;
        }

        /// <summary>
        /// 农民倍数
        /// </summary>
        public int peasantScales
        {
            get
            {
                int scales = 0;
                foreach (var role in lstRoles)
                {
                    if (!role.IsLandlord)
                    {
                        scales += role.currentRound.scales;
                    }
                }
                return scales;
            }
        }

        /// <summary>
        /// 地主倍数
        /// </summary>
        public int lordScales
        {
            get
            {
                return lordRole.currentRound.scales;
            }
        }

        /// <summary>
        /// 是否是地主赢
        /// </summary>
        public bool IsLordWin
        {
            get
            {
                return currentRound.winRoleId == currentRound.lordRoleId;
            }
        }

        /// <summary>
        /// 是否可以解散
        /// </summary>
        public bool IsInCanDismissState
        {
            get
            {
                if (roomState == ERoomState.WaitRound)
                {
                    return true;
                }

                if (roomState == ERoomState.Play)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 通知总倍数变化
        /// </summary>
        public void RefreshAllRoleTotalScales()
        {
            foreach (var role in lstRoles)
            {
                role.NotifyTotalScales();
            }
        }

        public LordRoomRole GetRole(UInt64 roleid)
        {
            foreach (var role in lstRoles)
            {
                if (role.roleid == roleid)
                {
                    return role;
                }
            }

            return null;
        }

        public LordRoomRole GetRole(ESeatPostion position)
        {
            foreach (var role in lstRoles)
            {
                if (role.position == position)
                {
                    return role;
                }
            }

            return null;
        }

        public LordRoomRole NextRole(LordRoomRole role)
        {
            ESeatPostion position = role.position + 1;
            if (position == ESeatPostion.Max)
            {
                position = ESeatPostion.First;
            }

            return GetRole(position);
        }

        /// <summary>
        /// 获得一个座位
        /// </summary>
        /// <returns></returns>
        public ESeatPostion GetOneSeat()
        {
            lstSeats.Sort();
            if (lstSeats.Count > 0)
            {
                ESeatPostion seat = lstSeats[0];
                lstSeats.Remove(seat);
                return seat;
            }
            return ESeatPostion.Null;
        }

        /// <summary>
        /// 归还座位
        /// </summary>
        /// <param name="seat"></param>
        public void ReturnSeat(ESeatPostion seat)
        {
            lstSeats.Add(seat);
        }

        public RoomInfo GetRoomInfo(UInt64 selfid)
        {
            RoomInfo info = new RoomInfo();
            info.roomUuid = roomUuid;
            info.roomState = roomState;
            foreach (var role in lstRoles)
            {
                if (selfid != role.roleid)
                {
                    info.lstRoles.Add(role.remoteInfo);
                }
            }

            info.localRoleInfo = GetRole(selfid).localInfo;
            info.localRoleInfo.remainSecond = dismissRemainSecond;

            //第几牌局
            info.currentCardRound = currentCardRound;

            // 庄家
            info.bankerRoleId = currentRound.bankerRoleId;

            // 地主
            info.lordRoleId = currentRound.lordRoleId;

            info.config = config;

            info.roomOwner = roomOwner;

            // 房间底牌
            info.bottomCards = currentRound.bottomCards;

            info.lastOutCardItem = currentRound.lastOutCardItem;

            info.dismissInfo = dismissInfo;

            info.maxFen = currentRound.maxFen;

            return info;
        }

        /// <summary>
        /// 解散倒计时
        /// </summary>
        public int dismissRemainSecond
        {
            get
            {
                if (dismissInfo.firstRoleId == 0)
                {
                    return 0;
                }

                if (env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return 0;
                }

                TimeSpan span = dismissInfo.expireTime - env.Timer.CurrentDateTime;
                return (int)span.TotalSeconds;
            }
        }

        public void SendRoomMsg(ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {
                role.SendMsgToClient(msg);
            }
        }

        public void SendRoomMsgEx(UInt64 roleid, ProtoBody msg)
        {
            foreach (var role in lstRoles)
            {
                if (role.roleid == roleid)
                {
                    continue;
                }
                role.SendMsgToClient(msg);
            }
        }

        public bool IsRoomFull()
        {
            return lstRoles.Count >= MAX_ROLES_COUNT;
        }

        public void AddRole(LordRoomRole role)
        {
            lstRoles.Add(role);
        }

        public void RemoveRole(UInt64 roleid)
        {
            LordRoomRole role = GetRole(roleid);

            ReturnSeat(role.position);

            lstRoles.Remove(role);
        }

        public string dbKey
        {
            get
            {
                string key = env.Redis.MergeKey(RedisName.detail_lord_room_, roomUuid.ToString());
                return key;
            }
        }

        public bool SaveDb(bool bAlways = false)
        {
            byte[] data;
            if (!this.SerializeToBinary(out data))
            {
                return false;
            }

            RedisSystem redis = env.Redis;

            if (bAlways)
            {
                redis.db.StringSet(dbKey, data, null, When.Always, CommandFlags.FireAndForget);
            }
            else
            {
                redis.db.StringSet(dbKey, data, null, When.Exists, CommandFlags.FireAndForget);
            }

            return true;
        }

        public bool DeleteFromDb()
        {
            RedisSystem redis = env.Redis;

            redis.db.KeyDelete(dbKey, CommandFlags.FireAndForget);

            return true;
        }

        public async Task<bool> LoadFromDbAsync()
        {
            RedisSystem redis = env.Redis;

            RedisValue value = await redis.db.StringGetAsync(dbKey);
            if (value.IsNullOrEmpty)
            {
                return false;
            }

            if (!this.SerializeFromBinary(value))
            {
                return false;
            }

            foreach (var role in lstRoles)
            {
                role.Init(this);
            }

            return true;
        }

        Random random = new Random();

        List<UInt32> RandomCards()
        {
            List<UInt32> lstCards = new List<UInt32>();

            UInt32 min = (UInt32)ECardId.Red3;
            UInt32 max = (UInt32)ECardId.Max - 1;

            for (UInt32 i = min; i <= max; i++)
            {
                lstCards.Add(i);
            }

            List<UInt32> lstResults = new List<UInt32>();
            while (lstCards.Count != 0)
            {
                int index = random.Next(lstCards.Count);
                UInt32 card = lstCards[index];
                lstResults.Add(card);
                lstCards.RemoveAt(index);
            }

            return lstResults;
        }

        public void FirstInit()
        {
            foreach (var role in lstRoles)
            {
                finalSettle.InitScore(role.roleid);
            }
        }

        public void StartNewRound()
        {
            InitNewRound();
            SetRoomState(ERoomState.Play);

            // 明牌或正常开始
            foreach (var role in lstRoles)
            {
                role.NotifyRoundConfigNeedSelect();
            }
        }

        public void StartNewRound2()
        {
            InitNewRound2();
            SetRoomState(ERoomState.Play);

            // 明牌或正常开始
            foreach (var role in lstRoles)
            {
                role.NotifyRoundConfigNeedSelect();
            }
        }

        /// <summary>
        /// 庄家
        /// </summary>
        public LordRoomRole bankerRole
        {
            get
            {
                LordRoomRole role = GetRole(currentRound.bankerRoleId);
                return role;
            }
        }

        public LordRoomRole lordRole
        {
            get
            {
                LordRoomRole role = GetRole(currentRound.lordRoleId);
                return role;
            }
        }

        public bool IsAllRoleWaitRound()
        {
            foreach (var role in lstRoles)
            {
                if (role.roleState != ERoomRoleState.WaitRound)
                {
                    return false;
                }
            }

            return true;
        }


        /// <summary>
        /// 处理结算
        /// </summary>
        public void Settle()
        {
            LandlordRoundSettleNtf ntf = new LandlordRoundSettleNtf();
            ntf.settleInfo = GetSettleInfo();
            ntf.IsRoomOver = IsLastRound();
            ntf.finalSettle = finalSettle;
            SendRoomMsg(ntf);

            if (IsLastRound())
            {
                RoomClose();
                return;
            }

            // 切换到结算状态
            SetRoomState(ERoomState.WaitRound);
        }

        /// <summary>
        /// 获取战绩
        /// </summary>
        /// <returns></returns>
        stBattleScoreItem GetBattleScoreItem()
        {
            stBattleScoreItem scoreItem = new stBattleScoreItem();
            scoreItem.dateTime = env.Timer.CurrentDateTime;
            scoreItem.roomUuid = roomUuid;
            scoreItem.gameType = EGameType.Landlord;
            scoreItem.roomOwner = roomOwner;

            foreach (var item in finalSettle.mapScores)
            {
                LordRoomRole role = GetRole(item.Key);
                if (null == role)
                {
                    continue;
                }
                stBattleScoreRole scoreRole = new stBattleScoreRole();
                scoreRole.roleid = role.roleid;
                scoreRole.nickName = role.nickName;
                scoreRole.score = item.Value;
                scoreItem.lstRoles.Add(scoreRole);
            }
            return scoreItem;
        }

        /// <summary>
        /// 检测解散超时
        /// </summary>
        public bool CheckDismissTimer()
        {
            if (dismissInfo.bStart)
            {
                if (env.Timer.CurrentDateTime > dismissInfo.expireTime)
                {
                    return true;
                }
            }

            return false;
        }

        public void NotifyDismissSuccess()
        {
            LandlordDismissGameSuccessNtf successNtf = new LandlordDismissGameSuccessNtf();
            successNtf.bSuccess = true;
            successNtf.finalSettle = finalSettle;
            SendRoomMsg(successNtf);
        }

        /// <summary>
        /// 关闭房间
        /// </summary>
        public void RoomClose()
        {
            foreach (var role in lstRoles)
            {
                BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                if (null == player)
                {
                    continue;
                }

                // 解绑战斗服
                player.UnBindBattleFromGate();

                g.playerMgr.RemovePlayer(role.roleid);
            }

            // 通知lobby房间关闭
            LandlordRoomCloseB2L closeNtf = new LandlordRoomCloseB2L();
            closeNtf.roomUuid = roomUuid;
            if(roomOwner.roleid != 0)
            {
                closeNtf.roomOwnerId = roomOwner.roleid;
            }
            else
            {
                closeNtf.roomOwnerId = roomOwner.falseFangzhu;
            }
            
            if (roomState == ERoomState.WaitRoomer)
            {
                closeNtf.bReturnRoomCard = true;
            }
            closeNtf.bRecordBattleScore = isRecordBattleScore;
            closeNtf.scoreItem = GetBattleScoreItem();
            SendMsgToLobby(closeNtf);

            g.lordRoomMgr.DeleteRoom(roomUuid);
        }

        /// <summary>
        /// 是否记录战绩
        /// </summary>
        public bool isRecordBattleScore
        {
            get
            {
                return roomState != ERoomState.WaitRoomer;
            }
        }

        void SendMsgToLobby(ProtoBody proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }

        void InitNewRound()
        {
            currentCardRound++;

            if (currentCardRound == 1)
            {
                if (config.payMethod == PayMethod.anotherPay)
                {
                    currentRound.bankerRoleId = lstRoles[0].roleid;
                }
                else
                {
                    currentRound.bankerRoleId = roomOwner.roleid;
                }
            }
            else
            {
                UInt64 bankerId = currentRound.bankerRoleId;

                currentRound.Clear();
                foreach (var role in lstRoles)
                {
                    role.currentRound.Clear();
                }

                // 庄家轮流
                currentRound.bankerRoleId = GetRole(bankerId).nextRole.roleid;
            }

            List<UInt32> allCards = RandomCards();

            //int count = 0;
            //每一个人17张牌
            foreach (var role in lstRoles)
            {
                List<UInt32> lstCards = null;
                //if(count == 0)
                //{
                //    count++;
                //    lstCards = GetSpecialCards1(allCards);
                //}
                //else if(count == 1)
                //{
                //    count++;
                //    lstCards = GetSpecialCards2(allCards);
                //}
                //else
                //{
                    lstCards = Get16Cards(allCards);
                //}

                role.currentRound.AddHandCards(lstCards);
                role.currentRound.playState = ERolePlayState.Wait;
            }

            // 剩余5张牌为底牌
            currentRound.bottomCards = allCards;
        }

        void InitNewRound2()
        {
            if (currentCardRound == 1)
            {
                //第一局的庄家是创建房间的人
                if (config.payMethod == PayMethod.anotherPay)
                {
                    currentRound.bankerRoleId = lstRoles[0].roleid;
                }
                else
                {
                    currentRound.bankerRoleId = roomOwner.roleid;
                }
            }
            else
            {
                UInt64 bankerId = currentRound.bankerRoleId;

                currentRound.Clear();
                foreach (var role in lstRoles)
                {
                    role.currentRound.Clear();
                }

                // 庄家轮流
                currentRound.bankerRoleId = GetRole(bankerId).nextRole.roleid;
            }

            List<UInt32> allCards = RandomCards();

            //int count = 0;
            //每一个人17张牌
            foreach (var role in lstRoles)
            {
                List<UInt32> lstCards = null;
                //if(count == 0)
                //{
                //    count++;
                //    lstCards = GetSpecialCards1(allCards);
                //}
                //else if(count == 1)
                //{
                //    count++;
                //    lstCards = GetSpecialCards2(allCards);
                //}
                //else
                //{
                lstCards = Get16Cards(allCards);
                //}

                role.currentRound.AddHandCards(lstCards);
                role.currentRound.playState = ERolePlayState.Wait;
            }

            // 剩余5张牌为底牌
            currentRound.bottomCards = allCards;
        }

        List<UInt32> Get17Cards(List<UInt32> randomCards)
        {
            List<UInt32> lstCards = randomCards.GetRange(0, 17);
            randomCards.RemoveRange(0, 17);

            return lstCards;
        }

        List<UInt32> Get16Cards(List<UInt32> randomCards)
        {
            List<UInt32> lstCards = randomCards.GetRange(0, 16);
            randomCards.RemoveRange(0, 16);
            return lstCards;
        }

        List<UInt32> GetSpecialCards1(List<UInt32> randomCards)
        {
            List<UInt32> lstResults = new List<UInt32>();
            lstResults.Add((UInt32)ECardId.Black3);
            lstResults.Add((UInt32)ECardId.Red3);
            lstResults.Add((UInt32)ECardId.Cube3);

            lstResults.Add((UInt32)ECardId.Black4);
            lstResults.Add((UInt32)ECardId.Red4);
            lstResults.Add((UInt32)ECardId.Cube4);

            lstResults.Add((UInt32)ECardId.Black5);
            lstResults.Add((UInt32)ECardId.Red5);
            lstResults.Add((UInt32)ECardId.Cube5);

            lstResults.Add((UInt32)ECardId.Black6);
            lstResults.Add((UInt32)ECardId.Red6);
            lstResults.Add((UInt32)ECardId.Cube6);

            lstResults.Add((UInt32)ECardId.Flower8);
            lstResults.Add((UInt32)ECardId.Flower9);
            lstResults.Add((UInt32)ECardId.Flower10);

            lstResults.Add((UInt32)ECardId.FlowerJ);
            lstResults.Add((UInt32)ECardId.FlowerQ);

            foreach(var item in lstResults)
            {
                randomCards.Remove(item);
            }

            return lstResults;
        }

        List<UInt32> GetSpecialCards2(List<UInt32> randomCards)
        {
            List<UInt32> lstResults = new List<UInt32>();
            lstResults.Add((UInt32)ECardId.Black8);
            lstResults.Add((UInt32)ECardId.Red8);
            lstResults.Add((UInt32)ECardId.Cube8);

            lstResults.Add((UInt32)ECardId.Black9);
            lstResults.Add((UInt32)ECardId.Red9);
            lstResults.Add((UInt32)ECardId.Cube9);

            lstResults.Add((UInt32)ECardId.Black10);
            lstResults.Add((UInt32)ECardId.Red10);
            lstResults.Add((UInt32)ECardId.Cube10);

            lstResults.Add((UInt32)ECardId.BlackJ);
            lstResults.Add((UInt32)ECardId.RedJ);
            lstResults.Add((UInt32)ECardId.CubeJ);

            lstResults.Add((UInt32)ECardId.Flower3);
            lstResults.Add((UInt32)ECardId.Flower4);
            lstResults.Add((UInt32)ECardId.Flower5);

            lstResults.Add((UInt32)ECardId.Flower6);
            lstResults.Add((UInt32)ECardId.Flower7);

            foreach (var item in lstResults)
            {
                randomCards.Remove(item);
            }

            return lstResults;
        }

        public void RefreshShowCard()
        {
            UInt64 bankerId = currentRound.bankerRoleId;
            currentRound.Clear();
            // 庄家不变
            currentRound.bankerRoleId = bankerId;

            foreach (var role in lstRoles)
            {
                bool bMingCardStart = role.currentRound.bMingCardStart; 
                role.currentRound.Clear();
                role.currentRound.bMingCardStart = bMingCardStart;
                if (bMingCardStart)
                {
                    // 明牌5倍
                    currentRound.SetMaxMingCardScales(5);
                    role.currentRound.bMingPai = true;
                }
            }

            List<UInt32> allCards = RandomCards();

            //每一个人17张牌
            foreach (var role in lstRoles)
            {
                List<UInt32> lstCards = allCards.GetRange(0, 17);
                allCards.RemoveRange(0, 17);
                role.currentRound.AddHandCards(lstCards);
                role.currentRound.playState = ERolePlayState.Wait;
            }

            // 剩余3张牌为底牌
            currentRound.bottomCards = allCards;

            foreach (var role in lstRoles)
            {
                role.NotifyShowNewCards();
            }
        }

        /// <summary>
        /// 回合配置选择结束
        /// </summary>
        /// <returns></returns>
        public bool IsAllRoleRoundConfigFinish()
        {
            if (roomState != ERoomState.Play)
            {
                return false;
            }

            foreach (var role in lstRoles)
            {
                if (role.roleState !=  ERoomRoleState.Play)
                {
                    return false;
                }

                if (role.currentRound.playState == ERolePlayState.MingCardOrNormalStart)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 显示新牌结束
        /// </summary>
        /// <returns></returns>
        public bool IsAllRoleShowNewCardsFinish()
        {
            if (roomState != ERoomState.Play)
            {
                return false;
            }

            foreach (var role in lstRoles)
            {
                if (role.roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (role.currentRound.playState == ERolePlayState.ShowCard)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 加倍结束
        /// </summary>
        /// <returns></returns>
        public bool IsAllRoleDoubleScalesFinish()
        {
            if (roomState != ERoomState.Play)
            {
                return false;
            }

            foreach (var role in lstRoles)
            {
                if (role.roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (role.currentRound.playState == ERolePlayState.DoubleScale)
                {
                    return false;
                }
            }

            return true;
        }

        public void OnRoleOnline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = true;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);
        }

        public void OnRoleOffline(BattlePlayer player)
        {
            RoomRoleOnlineOfflineNtf ntf = new RoomRoleOnlineOfflineNtf();
            ntf.roleid = player.AccountId;
            ntf.bOnline = false;
            ntf.clientIp = player.clientIp;
            SendRoomMsgEx(player.AccountId, ntf);

            if (IsAllRoleOffline)
            {
                g.lordRoomMgr.RemoveRoom(roomUuid);

                LandlordAllRoleOfflineB2L offlineNtf = new LandlordAllRoleOfflineB2L();
                offlineNtf.roomUuid = roomUuid;
                SendMsgToLobby(ntf);
            }
        }

        public void SendMsgToLobby(Protocol proto)
        {
            env.BusService.SendMessage("lobby", proto);
        }

        /// <summary>
        /// 房间内所有人都下线了
        /// </summary>
        /// <returns></returns>
        bool IsAllRoleOffline
        {
            get
            {
                foreach (var role in lstRoles)
                {
                    BattlePlayer player = g.playerMgr.FindPlayer(role.roleid);
                    if (player != null)
                    {
                        if (player.isOnline)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }
    }
}
