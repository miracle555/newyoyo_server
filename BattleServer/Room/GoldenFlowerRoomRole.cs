﻿using BattleServer;
using ProtoBuf;
using Protocols;
using Protocols.GoldenFlower;
using ServerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GoldenFlower
{
    /// <summary>
    /// 玩家回合信息
    /// </summary>
    [ProtoContract]
    public class RoleRoundInfo
    {
        /// <summary>
        /// 手牌
        /// </summary>
        [ProtoMember(1)]
        public HashSet<UInt32> handCards = new HashSet<UInt32>();

        /// <summary>
        /// 池牌
        /// </summary>
        [ProtoMember(2)]
        public HashSet<stOutCardItem> poolCards = new HashSet<stOutCardItem>();

        /// <summary>
        /// 操作状态开始时间
        /// </summary>
        [ProtoMember(3)]
        public DateTime stateStartTime = DateTime.MinValue;

        /// <summary>
        /// 操作状态
        /// </summary>
        [ProtoMember(4)]
        ERolePlayState _playState = ERolePlayState.Wait;

        public ERolePlayState playState
        {
            get
            {
                return _playState;
            }
            set
            {
                _playState = value;               
            }
        }

        /// <summary>
        /// 是否明牌开始
        /// </summary>
        // [ProtoMember(5)]
        // public bool bMingCardStart = false;

        /// <summary>
        ///本房间分数
        /// </summary>      
       [ProtoMember(6)]
        public int scales = 0;

        /// <summary>
        /// 是否看牌
        /// </summary>
       [ProtoMember(7)]
       public bool bKanPai = false;

        /// <summary>
        /// 出牌次数
        /// </summary>
        // [ProtoMember(8)]
        //public int handOutCardTimes = 0;
        ///当前状态
        [ProtoMember(8)]
         ERoomPlayerState _currentState = ERoomPlayerState.NoShowCard ;

        /// <summary>
        /// 当前准备状态
        /// </summary>
        [ProtoMember(9)]
        public bool prepare = false;


        /// <summary>
        /// 上一局结算信息
        /// </summary>
        [ProtoMember(10)]
        public int old = 0;

        public ERoomPlayerState currentState
        {
            get
            {
                return _currentState;
            }
            set
            {
                _currentState = value;
            }
        }



        public RoleRoundInfo()
        {
        }

        public void Clear()
        {
            handCards.Clear();
            poolCards.Clear();
            stateStartTime = DateTime.MinValue;
            _playState = ERolePlayState.Wait;
            //bMingCardStart = false;
          //  scales = 0;
            _currentState = ERoomPlayerState.NoShowCard;
            // bMingPai = false;
            // handOutCardTimes = 0;
            prepare = false;
        }

        public void AddHandCard(UInt32 cardid)
        {
            handCards.Add(cardid);
        }

        public bool HasHandCard(UInt32 carid)
        {
            return handCards.Contains(carid);
        }

        public void AddHandCards(List<UInt32> lstCards)
        {
            foreach (var cardid in lstCards)
            {
                AddHandCard(cardid);
            }
        }

        public void RemoveHandCards(List<UInt32> lstCards)
        {
            foreach (var cardid in lstCards)
            {
                handCards.Remove(cardid);
            }
        }
    };

     [ProtoContract]
    public class GoldenFlowerRoomRole
    {
         public GoldenFlowerRoomRole()
        {
            score = 0;
        }


         public void Init(UInt64 roleid, string nickName, GoldenFlowerRoom room)
         {
             this.roleid = roleid;
             this.nickName = nickName;
             m_room = room;
         }

         public void Init(GoldenFlowerRoom room)
         {
             m_room = room;
         }

         GoldenFlowerRoom m_room;

         [ProtoMember(1)]
         public UInt64 roleid { get; private set; }

         [ProtoMember(2)]
         public string nickName { get; private set; }

         [ProtoMember(3)]
         public int score { get; set; }

         [ProtoMember(4)]
         public ERoleSex sex { get; set; }

        
         [ProtoMember(5)]
         public UInt32 level = 0;

         [ProtoMember(6)]
         public ESeatPostion position = ESeatPostion.First;

        /// <summary>
        /// 玩家状态
        /// </summary>
        [ProtoMember(7)]
         public ERoomRoleState roleState = ERoomRoleState.Null;

         /// <summary>
         /// 玩家当前回合信息
         /// </summary>
         [ProtoMember(8)]
         public RoleRoundInfo currentRound = new RoleRoundInfo();

         /// <summary>
         /// 微信头像url
         /// </summary>
         [ProtoMember(9)]
         public string weixinHeadImgUrl = "";

        ///押注次数
        ///
        [ProtoMember(10)]
        public int betOnNumber = 0;

       
        //  public int totalScales
        // {
        //  get
        //  {
        // 公共倍数
        //    int scales = m_room.currentRound.publicScales.totalScales;
        //// 地主未定
        //if (m_room.currentRound.lordRoleId == 0)
        //{
        //    scales *= currentRound.scales;
        //}
        //else
        //{
        //    // 地主倍数
        //    scales *= m_room.lordScales;
        //    if (IsLandlord)
        //    {
        //        scales *= m_room.peasantScales;
        //    }
        //    else
        //    {
        //        scales *= currentRound.scales;
        //    }
        //}
        //   return scales;
        //  }
        // }
        // public int playerRoomFen
        //  {
        //   get
        //     {

        //         int playRoomFen= m_room.currentRound.playerFen;

        //       return playRoomFen;
        //  }

        //  }

        public RemoteRoleInfo remoteInfo
         {
             get
             {
                 RemoteRoleInfo info = new RemoteRoleInfo();

                 info.baseInfo.roleid = roleid;
                 info.baseInfo.nickName = nickName;
                 info.baseInfo.sex = sex;
                 info.baseInfo.level = level;
                 info.baseInfo.weixinHeadImgUrl = weixinHeadImgUrl;

                 info.otherInfo.score = currentRound.scales;
                 info.otherInfo.postion = position;
                 info.otherInfo.handCards.AddRange(currentRound.handCards);
                 info.otherInfo.isOnline = g.playerMgr.IsOnline(roleid);
                info.otherInfo.prepare = currentRound.prepare; 
                 info.otherInfo.roleState = roleState;
                 info.otherInfo.playState = currentRound.playState;
                 info.otherInfo.currentState= currentRound.currentState; ;
                BattlePlayer player = g.playerMgr.FindPlayer(roleid);
                 if (null != player)
                 {
                     info.baseInfo.clientIp = player.clientIp;
                 }

                 return info;
             }
         }

         public LocalRoleInfo localInfo
         {
             get
             {
                 LocalRoleInfo info = new LocalRoleInfo();     
                 info.position = position;
                 //info.bMingPai = currentRound.bMingPai;
                 info.handCards.AddRange(currentRound.handCards);
                 info.score = currentRound.scales;
                 info.roleState = roleState;
                 info.playState = currentRound.playState;
                 info.prepare = currentRound.prepare;
                 info.currentState = currentRound.currentState;
                 return info;
             }
         }


         /// <summary>
         /// 回合配置选择
         /// </summary>
        // public void NotifyRoundConfigNeedSelect()
        // {
            
           //      GoldenFlowerRoundStartNtf ntf = new GoldenFlowerRoundStartNtf();

          //       //ntf.roomInfo = GetRoomInfo(role.roleid);
           //      SendMsgToClient(ntf);

            //     currentRound.playState = ERolePlayState.CallGoldFlower;
            
        // }

         /// <summary>
         /// 通知总倍数
         /// </summary>
         public void NotifyTotalScales()
         {
             GoldenFlowerTotalScalesChangeNtf ntf = new GoldenFlowerTotalScalesChangeNtf();
            // ntf.totalScales = totalScales;
           //  ntf.roomFen = playerRoomFen;
            //ntf.playerId = roleid;
             SendMsgToClient(ntf);
         }

         public void SendMsgToClient(ProtoBody msg)
         {
             BattlePlayer player = g.playerMgr.FindPlayer(roleid);
             if (null == player)
             {
                 return;
             }
             player.SendMsgToClient(msg);
         }

    }
}
