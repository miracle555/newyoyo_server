﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Protocols.Landlord;
using BattleServer;
using HelpAsset;

namespace Landlord
{
    /// <summary>
    /// 玩家回合信息
    /// </summary>
    [ProtoContract]
    public class RoleRoundInfo
    {
        /// <summary>
        /// 手牌
        /// </summary>
        [ProtoMember(1)]
        public HashSet<UInt32> handCards = new HashSet<UInt32>();

        /// <summary>
        /// 池牌
        /// </summary>
        [ProtoMember(2)]
        public HashSet<stOutCardItem> poolCards = new HashSet<stOutCardItem>();

        /// <summary>
        /// 操作状态开始时间
        /// </summary>
        [ProtoMember(3)]
        public DateTime stateStartTime = DateTime.MinValue;

        /// <summary>
        /// 操作状态
        /// </summary>
        [ProtoMember(4)]
        ERolePlayState _playState = ERolePlayState.Null;

        public ERolePlayState playState
        {
            get
            {
                return _playState;
            }
            set
            {
                _playState = value;
                stateStartTime = env.Timer.CurrentDateTime;
            }
        }

        /// <summary>
        /// 是否明牌开始
        /// </summary>
        [ProtoMember(5)]
        public bool bMingCardStart = false;

        /// <summary>
        /// 倍数
        /// </summary>
        [ProtoMember(6)]
        public int scales = 1;

        /// <summary>
        /// 是否明牌
        /// </summary>
        [ProtoMember(7)]
        public bool bMingPai = false;

        /// <summary>
        /// 出牌次数
        /// </summary>
        [ProtoMember(8)]
        public int handOutCardTimes = 0;

        /// <summary>
        /// 该玩家当前回合叫的分
        /// </summary>
        [ProtoMember(9)]
        public int currentFen = 0;

        /// <summary>
        /// 玩家当前回合是否叫过分
        /// </summary>
        [ProtoMember(10)]
        public bool hasFen = false;

        public RoleRoundInfo()
        {
        }

        public void Clear()
        {
            handCards.Clear();
            poolCards.Clear();
            stateStartTime = DateTime.MinValue;
            _playState = ERolePlayState.Null;
            bMingCardStart = false;
            scales = 1;
            bMingPai = false;
            handOutCardTimes = 0;
            currentFen = 0;
            hasFen = false;
        }

        public void AddHandCard(UInt32 cardid)
        {
            handCards.Add(cardid);
        }

        public bool HasHandCard(UInt32 carid)
        {
            return handCards.Contains(carid);
        }

        public void AddHandCards(List<UInt32> lstCards)
        {
            foreach(var cardid in lstCards)
            {
                AddHandCard(cardid);
            }
        }

        public void RemoveHandCards(List<UInt32> lstCards)
        {
            foreach(var cardid in lstCards)
            {
                handCards.Remove(cardid);
            }
        }
    };

    [ProtoContract]
    public class LordRoomRole
    {
        public LordRoomRole()
        {
            score = 0;
        }

        public void Init(UInt64 roleid, string nickName, LordRoom room)
        {
            this.roleid = roleid;
            this.nickName = nickName;
            m_room = room;
        }

        public void Init(LordRoom room)
        {
            m_room = room;
        }

        LordRoom m_room;

        [ProtoMember(1)]
        public UInt64 roleid { get; private set; }

        [ProtoMember(2)]
        public string nickName { get; private set; }

        [ProtoMember(3)]
        public int score { get; set; }

        [ProtoMember(4)]
        public ERoleSex sex { get; set; }

        [ProtoMember(5)]
        public ESeatPostion position = ESeatPostion.First;

        [ProtoMember(6)]
        public UInt32 level = 0;

        /// <summary>
        /// 玩家状态
        /// </summary>
        [ProtoMember(7)]
        public ERoomRoleState roleState = ERoomRoleState.Null;

        /// <summary>
        /// 玩家当前回合信息
        /// </summary>
        [ProtoMember(8)]
        public RoleRoundInfo currentRound = new RoleRoundInfo();

        /// <summary>
        /// 微信头像url
        /// </summary>
        [ProtoMember(9)]
        public string weixinHeadImgUrl = "";

        /// <summary>
        /// 是否连续出牌
        /// </summary>
        [ProtoMember(10)]
        public bool OnGoing = true;


        public LordRoomRole nextRole
        {
            get
            {
                return m_room.NextRole(this);
            }
        }

        /// <summary>
        /// 自己是不是地主
        /// </summary>
        public bool IsLandlord
        {
            get
            {
                return m_room.currentRound.lordRoleId == roleid;
            }
        }

        /// <summary>
        /// 是否是农名
        /// </summary>
        public bool IsFarmer
        {
            get
            {
                return !IsLandlord;
            }
        }

        /// <summary>
        /// 本回合自己是否赢
        /// </summary>
        bool IsRoundWin
        {
            get
            {
                if(m_room.IsLordWin)
                {
                    if(IsLandlord)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (IsLandlord)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

        /// <summary>
        /// 回合分数
        /// </summary>
        public int roundScore
        {
            get
            {
                // 公共倍数
                int score = m_room.currentRound.publicScales.totalScales;
                //底分
                score *= m_room.currentRound.maxFen;
                // 地主倍数
                score *= m_room.lordScales;
                if(IsLandlord)
                {
                    score *= m_room.peasantScales;
                }
                else
                {
                    score *= currentRound.scales;
                }

                if(!IsRoundWin)
                {
                    score *= (-1);
                }

                return score;
            }
        }

        /// <summary>
        /// 总倍数
        /// </summary>
        public int totalScales
        {
            get
            {
                // 公共倍数
                int scales = m_room.currentRound.publicScales.totalScales;
                // 地主未定
                if(m_room.currentRound.lordRoleId == 0)
                {
                    scales *= currentRound.scales;
                }
                else
                {
                    // 地主倍数
                    scales *= m_room.lordScales;
                    if (IsLandlord)
                    {
                        scales *= m_room.peasantScales;
                    }
                    else
                    {
                        scales *= currentRound.scales;
                    }
                }
                return scales;
            }
        }

        public RemoteRoleInfo remoteInfo
        {
            get
            {
                RemoteRoleInfo info = new RemoteRoleInfo();

                info.baseInfo.roleid = roleid;
                info.baseInfo.nickName = nickName;
                info.baseInfo.sex = sex;
                info.baseInfo.level = level;
                info.baseInfo.weixinHeadImgUrl = weixinHeadImgUrl;

                info.otherInfo.score = score;
                info.otherInfo.postion = position;

                info.otherInfo.isOnline = g.playerMgr.IsOnline(roleid);

                info.otherInfo.bMingPai = currentRound.bMingPai;
                if(info.otherInfo.bMingPai)
                {
                    info.otherInfo.handCards.AddRange(currentRound.handCards);
                }
                info.otherInfo.handCardsCount = currentRound.handCards.Count;

                info.otherInfo.scales = currentRound.scales;
                info.otherInfo.roleState = roleState;
                info.otherInfo.playState = currentRound.playState;

                BattlePlayer player = g.playerMgr.FindPlayer(roleid);
                if(null != player)
                {
                    info.baseInfo.clientIp = player.clientIp;
                }

                return info;
            }
        }

        public LocalRoleInfo localInfo
        {
            get
            {
                LocalRoleInfo info = new LocalRoleInfo();

                info.score = score;
                info.position = position;
                info.bMingPai = currentRound.bMingPai;
                info.handCards.AddRange(currentRound.handCards);
                info.scales = currentRound.scales;
                info.roleState = roleState;
                info.playState = currentRound.playState;

                return info;
            }
        }

        /// <summary>
        /// 回合开始通知
        /// </summary>
        public void NotifyShowNewCards()
        {
            // 回合开始通知
            LandlordNeedShowNewCardsNtf ntf = new LandlordNeedShowNewCardsNtf();
            ntf.roomInfo = m_room.GetRoomInfo(roleid);
            SendMsgToClient(ntf);

            currentRound.playState = ERolePlayState.ShowCard;
        }

        /// <summary>
        /// 通知叫地主
        /// </summary>
        public void NotifyCallLandlord()
        {
            // 通知需要叫地主
            LandlordNeedCallLandlordNtf ntf = new LandlordNeedCallLandlordNtf();
            ntf.roleId = roleid;
            ntf.maxJIao = m_room.currentRound.maxFen;
            SendMsgToClient(ntf);

            currentRound.playState = ERolePlayState.CallLandlord;                         
        }


        /// <summary>
        /// 通知抢地主
        /// </summary>
        public void NotifyRobLandlord()
        {
            // 通知需要抢地主
            LandlordNeedRobLandlordNtf ntf = new LandlordNeedRobLandlordNtf();
            ntf.roleId = roleid;
            SendMsgToClient(ntf);

            currentRound.playState = ERolePlayState.RobLandlord;               
        }

        /// <summary>
        /// 通知加倍
        /// </summary>
        public void NotifyDoubleScales()
        {

            currentRound.playState = ERolePlayState.DoubleScale;
            // 需要显示加倍
            LandlordShowDoubleScalesNtf ntf = new LandlordShowDoubleScalesNtf();
            ntf.remainSecond = remainSecond;
            SendMsgToClient(ntf);
      
        }


        /// <summary>
        /// 通知需要出牌
        /// </summary>
        public void NotifyNeedHandOutCard(bool bichu)
        {
            LandlordNeedHandOutCardNtf ntf = new LandlordNeedHandOutCardNtf();

            if (bichu)
            {
                ntf.bMust = true;
            }
            else
            {
                if (m_room.currentRound.lastOutCardItem.roleid == 0)
                {
                    ntf.bMust = true;
                }
                else
                {
                    if (m_room.currentRound.lastOutCardItem.roleid == roleid)
                    {
                        ntf.bMust = true;
                    }
                }
            }
            
            ntf.roleid = roleid;
            m_room.SendRoomMsg(ntf);

            currentRound.playState = ERolePlayState.HandOutCard;
        }

        /// <summary>
        /// 回合配置选择
        /// </summary>
        public void NotifyRoundConfigNeedSelect()
        {
            // 回合开始通知
            LandlordNeedRoundConfigSelectNtf ntf = new LandlordNeedRoundConfigSelectNtf();
            ntf.roomRound = m_room.currentCardRound;
            SendMsgToClient(ntf);

            currentRound.playState = ERolePlayState.MingCardOrNormalStart;
        }

        /// <summary>
        /// 通知总倍数
        /// </summary>
        public void NotifyTotalScales()
        {
            LandlordTotalScalesChangeNtf ntf = new LandlordTotalScalesChangeNtf();
            ntf.totalScales = totalScales;
            SendMsgToClient(ntf);
        }

        public void SendMsgToClient(ProtoBody msg)
        {
            BattlePlayer player = g.playerMgr.FindPlayer(roleid);
            if (null == player)
            {
                return;
            }
            player.SendMsgToClient(msg);
        }

        /// <summary>
        /// 是否是回合配置选择状态
        /// </summary>
        public bool IsRoundConfigSelectState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.MingCardOrNormalStart)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 是否在显示新牌状态
        /// </summary>
        public bool IsShowNewCardsState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.ShowCard)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 是否是抢地主状态
        /// </summary>
        public bool IsRobLandlordState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.RobLandlord)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 是否叫地主状态
        /// </summary>
        public bool IsCallLandlordState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.CallLandlord)
                {
                    return true;
                }

                return false;
            }
        }

        public bool IsDoubleScaleState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.DoubleScale)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 是否是出牌状态
        /// </summary>
        public bool IsHandOutCardState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != ERoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.HandOutCard)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 剩余时间
        /// </summary>
        public int remainSecond
        {
            get
            {
                long interval = 0;
                if (IsDoubleScaleState)
                {
                    interval = 5;
                }

                DateTime endTime = currentRound.stateStartTime.AddSeconds(interval);
                if (env.Timer.CurrentDateTime < endTime)
                {
                    TimeSpan span = endTime - env.Timer.CurrentDateTime;
                    return span.Seconds;
                }

                return 0;
            }
        }

        /// <summary>
        /// 检测倒计时
        /// </summary>
        public void OnTick()
        {
            if (IsDoubleScaleState)
            {
                CheckTickDoubleScale();
            }
        }

        public void CheckTickDoubleScale()
        {
            if (remainSecond != 0)
            {
                return;
            }

            LandlordTimerDoubleScaleNtf ntfTimer = new LandlordTimerDoubleScaleNtf();
            SendMsgToClient(ntfTimer);
        
            currentRound.scales = 1;          

            // 通知总倍数变化
            //m_room.RefreshAllRoleTotalScales();

            currentRound.playState = ERolePlayState.Wait;

            LandlordDoubleScalesNtf ntf = new LandlordDoubleScalesNtf();
            ntf.roleid = roleid;
            m_room.SendRoomMsgEx(roleid, ntf);

            if (m_room.IsAllRoleDoubleScalesFinish())
            {
                // 地主出牌
                m_room.lordRole.NotifyNeedHandOutCard(false);
            }
        }
    }
}
