﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using Protocols.MajiangPushDown;
using BattleServer;
using Protocols.Majiang;
using HelpAsset.MahjongPushDown;
using HelpAsset;

using System.Diagnostics;
using System.Reflection;  //反射

namespace MahjongPushDown
{
    /// <summary>
    /// 玩家回合信息
    /// </summary>
    [ProtoContract]
    public class RoleRoundInfo
    {
        /// <summary>
        /// 手牌
        /// </summary>
        [ProtoMember(1)]
        public HashSet<UInt32> handCards = new HashSet<UInt32>();

        /// <summary>
        /// 明牌
        /// </summary>
        [ProtoMember(2)]
        public List<MingCardItem> showCards = new List<MingCardItem>();

        /// <summary>
        /// 池牌
        /// </summary>
        [ProtoMember(3)]
        public List<UInt32> poolCards = new List<UInt32>();

        /// <summary>
        /// 是否报停
        /// </summary>
        [ProtoMember(4)]
        public List<EMJCardType> lstReadyTypes = new List<EMJCardType>();

        /// <summary>
        /// 最新起的一张牌
        /// </summary>
        [ProtoMember(5)]
        public UInt32 lastNewCardId = 0;

        /// <summary>
        /// 是否过胡
        /// </summary>
        [ProtoMember(6)]
        public bool bPassHu = false;

        /// <summary>
        /// 缺一门
        /// </summary>
        [ProtoMember(7)]
        public EMahjongCard doorType = EMahjongCard.Null;

        /// <summary>
        /// 操作状态
        /// </summary>
        [ProtoMember(8)]
        ERolePlayState _playState;

        public ERolePlayState playState
        {
            get
            {
                return _playState;
            }
            set
            {
                _playState = value;
                stateStartTime = env.Timer.CurrentDateTime;
            }
        }

        /// <summary>
        /// 碰杠胡听信息
        /// </summary>
        [ProtoMember(9)]
        public stOutCardReadyItem readyItem = new stOutCardReadyItem();

        /// <summary>
        /// 操作状态开始时间
        /// </summary>
        [ProtoMember(10)]
        public DateTime stateStartTime = DateTime.MinValue;

        /// <summary>
        /// 测试房间uuid
        /// </summary>
        [ProtoMember(11)]
        public UInt64 testRoomUuid = 0;

        [ProtoMember(12)]
        public UInt64 testRoleId = 0;

        /// <summary>
        /// 听牌时出的牌
        /// </summary>
        [ProtoMember(13)]
        public UInt32 readyOutCardId = 0;

        /// <summary>
        /// 起牌时检测哪些选项
        /// </summary>
        [ProtoMember(14)]
        public stCheckReadyItem checkItem = new stCheckReadyItem();

        public RoleRoundInfo()
        {
        }

        public int GetHandCardCount(EMJCardType type)
        {
            int count = 0;
            foreach(var cardid in handCards)
            {
                if(type == MJCardHelp.GetMJType(cardid))
                {
                    count++;
                }
            }
            return count;
        }

        public void Clear()
        {
            handCards.Clear();
            showCards.Clear();
            poolCards.Clear();
            lstReadyTypes.Clear();
            lastNewCardId = 0;
            bPassHu = false;
            m_vecHandCards.Clear();
            m_vecHandAndMingCards.Clear();
            doorType = EMahjongCard.Null;
            playState = ERolePlayState.Null;
        }

        public bool IsReadyCard(UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);
            return IsReadyType(type);
        }

        public bool IsReadyType(EMJCardType type)
        {
            return lstReadyTypes.Contains(type);
        }

        public void AddHandCard(UInt32 cardid)
        {
            handCards.Add(cardid);
        }

        public bool HasHandCard(UInt32 cardid)
        {
            return handCards.Contains(cardid);
        }

        public void RemovePoolCard(UInt32 cardid)
        {
            poolCards.Remove(cardid);
        }

        public void AddPoolCard(UInt32 cardid)
        {
            poolCards.Add(cardid);
        }

        public void RemoveHandCard(UInt32 cardid)
        {
            handCards.Remove(cardid);
        }

        public void RemoveHandCards(List<UInt32> lstCards)
        {
            foreach (var cardid in lstCards)
            {
                RemoveHandCard(cardid);
            }
        }

        public UInt32 GetHandCardByType(EMJCardType type)
        {
            foreach(var cardid in handCards)
            {
                if(MJCardHelp.GetMJType(cardid) == type)
                {
                    return cardid;
                }
            }

            return 0;
        }

        /// <summary>
        /// 是否已经报听
        /// </summary>
        public bool AlreadyBaoTing
        {
            get
            {
                return lstReadyTypes.Count != 0;
            }
        }

        VecCards m_vecHandCards = new VecCards();
        VecCards m_vecHandAndMingCards = new VecCards();

        /// <summary>
        /// 手牌
        /// </summary>
        public VecCards vecHandCards
        {
            get
            {
                m_vecHandCards.FillHandCards(handCards);
                return m_vecHandCards;
            }
        }

        /// <summary>
        /// 明牌和手牌
        /// </summary>
        public VecCards vecHandAndMingCards
        {
            get
            {
                m_vecHandAndMingCards.FillMingAndHandCards(handCards, showCards);

                return m_vecHandAndMingCards;
            }
        }
    }

    [ProtoContract]
    public class HuRoomRole
    {
        public HuRoomRole()
        {
            score = 0;
        }

        public void Init(UInt64 roleid, string nickName, HuRoom room)
        {
            this.roleid = roleid;
            this.nickName = nickName;
            m_room = room;
        }

        public void Init(HuRoom room)
        {
            m_room = room;
        }

        HuRoom m_room;

        [ProtoMember(1)]
        public UInt64 roleid { get; private set; }

        [ProtoMember(2)]
        public string nickName { get; private set; }

        [ProtoMember(3)]
        public int score { get; set; }

        [ProtoMember(4)]
        public ERoleSex sex { get; set; }

        [ProtoMember(5)]
        public EMajongDirection direction = EMajongDirection.East;

        [ProtoMember(6)]
        public UInt32 level = 0;

        /// <summary>
        /// 玩家状态
        /// </summary>
        [ProtoMember(7)]
        public HuRoomRoleState roleState = HuRoomRoleState.Null;

        /// <summary>
        /// 玩家当前回合信息
        /// </summary>
        [ProtoMember(8)]
        public RoleRoundInfo currentRound = new RoleRoundInfo();

        /// <summary>
        /// 微信头像url
        /// </summary>
        [ProtoMember(9)]
        public string weixinHeadImgUrl = "";

        /// <summary>
        /// 抢杠胡
        /// </summary>
        [ProtoMember(10)]
        public bool bRobGangHu = false;

        /// <summary>
        /// 玩家起新牌
        /// </summary>
        public void DrawNewCard()
        {
            UInt32 newcard = m_room.currentRound.TakeOneNewCard();

            // 没牌了，流局
            if (newcard == 0)
            {
                m_room.OnFlowRound();
                return;
            }

            currentRound.AddHandCard(newcard);
            currentRound.lastNewCardId = newcard;
            m_room.currentRound.lastNewCardRoleId = roleid;

            MJPushDownGetNewCardNtf ntf = new MJPushDownGetNewCardNtf();
            ntf.cardRoleId = roleid;
            ntf.cardid = (UInt32)newcard;
            SendMsgToClient(ntf);

            // 通知房间其他人
            ntf.cardid = 0;
            m_room.SendRoomMsgEx(roleid, ntf);

            // 需要客户端检测听牌暗杠
            CheckSelfGangHuReady();
        }

        public HuRoomRole nextRole
        {
            get
            {
                return m_room.NextRole(this);
            }
        }


        public void GetCheckRoomRoleInfo(stCheckRoomItem room, stCheckRoleItem role)
        {
            room.config = m_room.config;

            role.handCards = currentRound.handCards.Copy();
            foreach(var item in currentRound.showCards)
            {
                var destitem = item.Copy();
                role.showCards.Add(destitem);
            }
            //role.showCards = currentRound.showCards;
            role.AlreadyBaoTing = currentRound.AlreadyBaoTing;
            role.doorType = currentRound.doorType;
            role.lstReadyTypes = currentRound.lstReadyTypes;
        }

        /// <summary>
        /// 是否可以碰该牌
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public bool CanPeng(UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);

            stCheckRoomItem room = new stCheckRoomItem();
            stCheckRoleItem role = new stCheckRoleItem();
            GetCheckRoomRoleInfo(room, role);

            //if (room.config.bLackingDoor)
            //{
            //    if (role.IsLackingDoorType(type))
            //    {
            //        return false;
            //    }
            //}
            List<UInt32> lstCards = role.GetHandCardsByType(type);
            if (lstCards.Count < 2)
            {
                return false;
            }
            lstCards = lstCards.GetRange(0, 2);

            // 听口变了
            if (PushDownHelp.CheckReadyChange(lstCards, room, role))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 发送碰杠胡听消息
        /// </summary>
        /// <param name="readyInfo"></param>
        public void NotifyPengGangHuReady(stOutCardReadyItem readyItem)
        {
            MJPushDownPengHuCardNtf huNtf = new MJPushDownPengHuCardNtf();
            huNtf.readyInfo = readyItem.readyInfo;
            SendMsgToClient(huNtf);
            currentRound.playState = ERolePlayState.PengGangHuReady;
            currentRound.readyItem = readyItem;
        }

        public bool IsPengGangHuReady(stOutCardReadyItem readyItem)
        {
            if (readyItem.readyInfo.bHuPai || readyItem.readyInfo.bReadyCard || readyItem.readyInfo.gangType != EGangCardType.Null || readyItem.readyInfo.bPeng)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否是碰杠胡听状态
        /// </summary>
        public bool IsPengGangHuReadyState
        {
            get
            {
                if(m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if(roleState != HuRoomRoleState.Play)
                {
                    return false;
                }

                if(currentRound.playState == ERolePlayState.PengGangHuReady)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 是否是在检测碰杠胡听状态
        /// </summary>
        public bool IsNeedCheckSelfPengGangHuReadyState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != HuRoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.NeedCheckSelfPengGangHuReady)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 是否是出牌状态
        /// </summary>
        public bool IsHandingOutState
        {
            get
            {
                if (m_room.roomState != ERoomState.Play)
                {
                    return false;
                }

                if (roleState != HuRoomRoleState.Play)
                {
                    return false;
                }

                if (currentRound.playState == ERolePlayState.NeedHandOut)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// 胡其他人的牌
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public bool CanHuOther(UInt32 cardid, ref EHuCardType huType)
        {
            huType = EHuCardType.Null;

            // 没报停不能胡
            if (m_room.config.bBaoTing)
            {
                if (!currentRound.AlreadyBaoTing)
                {
                    return false;
                }

                if (!currentRound.IsReadyCard(cardid))
                {
                    return false;
                }
            }

            // 只能自摸胡
            if(m_room.config.bOnlyWinSelf)
            {
                return false;
            }

            // 过胡，暂时不能胡牌
            if(currentRound.bPassHu)
            {
                return false;
            }

            VecCards vecHandCards = currentRound.vecHandCards;
            vecHandCards.AddCardToVector(cardid);

            stCheckRoomItem room = new stCheckRoomItem();
            stCheckRoleItem role = new stCheckRoleItem();
            GetCheckRoomRoleInfo(room, role);

            bool bHu = PushDownHelp.CanHuCard(vecHandCards, ref huType, room, role);

            if (bHu)
            {
                if (!m_room.config.IsBigHu)
                {
                    huType = EHuCardType.CommonHu;
                }
            }

            return bHu;
        }

        public List<ReadyInfo> GetReadyInfos(List<UInt32> lstReadyCards)
        {
            stCheckRoomItem room = new stCheckRoomItem();
            stCheckRoleItem role = new stCheckRoleItem();
            GetCheckRoomRoleInfo(room, role);

            List<ReadyInfo> lstResults = new List<ReadyInfo>();

            foreach(var cardid in lstReadyCards)
            {
                ReadyInfo info = new ReadyInfo();
                info.cardid = cardid;
                List<EMJCardType> lstTypes = PushDownHelp.GetReadyTypes(cardid, room, role);
                foreach (var type in lstTypes)
                {
                    ReadyItem item = GetReadyItemCount(type);
                    info.lstItems.Add(item);
                }
                lstResults.Add(info);
            }

            return lstResults;
        }

        /// <summary>
        /// 获取该类型牌还有多少张
        /// </summary>
        /// <param name="cardType"></param>
        /// <param name="lstItems"></param>
        ReadyItem GetReadyItemCount(EMJCardType cardType)
        {
            ReadyItem item = new ReadyItem();
            item.cardType = cardType;

            List<HuRoomRole> lstOthers = m_room.NextRoles(this);
            foreach(var role in lstOthers)
            {
                item.cardCount += role.currentRound.GetHandCardCount(cardType);
            }
            item.cardCount += m_room.currentRound.GetPoolCardCount(cardType);

            return item;
        }

        public RemoteRoleInfo remoteInfo
        {
            get
            {
                RemoteRoleInfo info = new RemoteRoleInfo();

                info.baseInfo.roleid = roleid;
                info.baseInfo.nickName = nickName;
                info.baseInfo.sex = sex;
                info.baseInfo.level = level;
                info.baseInfo.weixinHeadImgUrl = weixinHeadImgUrl;

                info.otherInfo.score = score;
                info.otherInfo.direction = direction;

                info.otherInfo.isOnline = g.playerMgr.IsOnline(roleid);
                info.otherInfo.handCardsCount = currentRound.handCards.Count;
                info.otherInfo.showCards.AddRange(currentRound.showCards);
                info.otherInfo.poolCards = currentRound.poolCards;
                info.otherInfo.bBaoTing = currentRound.AlreadyBaoTing;
                info.otherInfo.roleState = roleState;
                info.otherInfo.doorType = currentRound.doorType;
                info.otherInfo.playState = currentRound.playState;
                info.otherInfo.remainSecond = HuRoom.HANDOUT_CARD_SECOND;
                info.otherInfo.readyOutCardId = currentRound.readyOutCardId;
                info.otherInfo.lastNewCardId = currentRound.lastNewCardId;

                BattlePlayer player = g.playerMgr.FindPlayer(roleid);
                if(null != player)
                {
                    info.baseInfo.clientIp = player.clientIp;
                }

                return info;
            }
        }

        public MahjongLocalRoleInfo localInfo
        {
            get
            {
                MahjongLocalRoleInfo info = new MahjongLocalRoleInfo();

                info.score = score;
                info.direction = direction;

                info.handCards.AddRange(currentRound.handCards);
                info.showCards.AddRange(currentRound.showCards);
                info.poolCards = currentRound.poolCards;
                info.bBaoTing = currentRound.AlreadyBaoTing;
                info.roleState = roleState;
                info.doorType = currentRound.doorType;
                info.playState = currentRound.playState;
                info.readyInfo = currentRound.readyItem.readyInfo;
                info.remainSecond = HuRoom.HANDOUT_CARD_SECOND;
                info.readyOutCardId = currentRound.readyOutCardId;
                info.lastNewCardId = currentRound.lastNewCardId;

                return info;
            }
        }

        public void AddPengCards(UInt32 cardid, List<UInt32> lstCards)
        {
            MingCardItem item = new MingCardItem();
            item.mingType = EMingCardType.Peng;
            item.showCards.Add(cardid);
            item.showCards.AddRange(lstCards);
            item.cardType = MJCardHelp.GetMJType(cardid);
            currentRound.showCards.Add(item);
        }

        public void AddMingGangCards(UInt32 cardid, List<UInt32> lstCards)
        {
            MingCardItem item = new MingCardItem();
            item.mingType = EMingCardType.MingGang;
            item.showCards.Add(cardid);
            item.showCards.AddRange(lstCards);
            item.cardType = MJCardHelp.GetMJType(cardid);
            currentRound.showCards.Add(item);
        }

        public void AddAnGangCards(List<UInt32> lstCards)
        {
            MingCardItem item = new MingCardItem();
            item.mingType = EMingCardType.AnGang;
            item.showCards.AddRange(lstCards);
            item.cardType = MJCardHelp.GetMJType(lstCards[0]);
            currentRound.showCards.Add(item);
        }

        public void AddBuGangCards(List<UInt32> lstCards)
        {
            if(lstCards.Count == 0)
            {
                return;
            }

            EMJCardType type = MJCardHelp.GetMJType(lstCards[0]);
            foreach (var item in currentRound.showCards)
            {
                if(item.cardType == type)
                {
                    item.showCards.Add(lstCards[0]);
                    item.mingType = EMingCardType.MingGang;
                }
            }
        }
         
        public List<UInt32> GetPengCards(EMJCardType type)
        {
            List<UInt32> lstResults = new List<UInt32>();
            foreach (var cardid in currentRound.handCards)
            {
                if(MJCardHelp.GetMJType(cardid) == type)
                {
                    lstResults.Add(cardid);
                    if(lstResults.Count == 2)
                    {
                        return lstResults;
                    }
                }
            }
            lstResults.Clear();
            return lstResults;
        }

        /// <summary>
        /// 需要出新牌
        /// </summary>
        public void NotifyNeedHandOutCard()
        {
            MJPushDownNeedHandCardNtf needNtf = new MJPushDownNeedHandCardNtf();
            needNtf.cardRoleId = roleid;
            needNtf.playRemainSecond = HuRoom.HANDOUT_CARD_SECOND;
            m_room.SendRoomMsg(needNtf);

            currentRound.playState = ERolePlayState.NeedHandOut;
        }

        /// <summary>
        /// 检测自己杠胡听牌
        /// </summary>
        public void NotifyNeedCheckSelfGangHuReady1(bool bPeng = false)
        {
            MJPushDownNeedCheckSelfGangHuReadyNtf needNtf = new MJPushDownNeedCheckSelfGangHuReadyNtf();

            needNtf.checkItem.bAnGang = true;
            needNtf.checkItem.bBuGang = true;
            needNtf.checkItem.bReadyCard = true;

            if (bPeng)
            {
                // 碰牌不检测自摸胡
                needNtf.checkItem.bHuCard = false;
            }
            else
            {
                needNtf.checkItem.bHuCard = true;
            }

            SendMsgToClient(needNtf);

            currentRound.checkItem = needNtf.checkItem;

            currentRound.playState = ERolePlayState.NeedCheckSelfPengGangHuReady;
        }

        /// <summary>
        /// 检测碰杠胡停牌
        /// </summary>
        /// <param name="bPeng"></param>
        public void CheckSelfGangHuReady(bool bPeng = false)
        {
            stOutCardReadyItem readyItem = new stOutCardReadyItem();
            readyItem.roleid = roleid;
            readyItem.bSelfTouch = true;

            stPengGangHuReadyInfo readyInfo = readyItem.readyInfo;
            readyInfo.cardRoleId = roleid;

            stCheckRoomItem roomitem = new stCheckRoomItem();
            stCheckRoleItem roleitem = new stCheckRoleItem();
            GetCheckRoomRoleInfo(roomitem, roleitem);

            List<UInt32> lstReadyCards = new List<UInt32>();

            if (PushDownHelp.CanReadyCard(lstReadyCards, roomitem, roleitem))
            {
                // 是否听牌
                readyInfo.bReadyCard = true;
                readyInfo.lstReadyCards = GetReadyInfos(lstReadyCards);
            }

            if (!bPeng)
            {
                if (PushDownHelp.CanHuSelf(ref readyItem.huType, roomitem, roleitem))
                {
                    readyInfo.bHuPai = true;
                }
            }

            EMJCardType anGangType = EMJCardType.Null;
            if (PushDownHelp.CanAnGang(roomitem, roleitem, ref anGangType))
            {
                UInt32 outcardid = currentRound.GetHandCardByType(anGangType);

                readyInfo.gangType = EGangCardType.AnGang;
                readyInfo.cardid = outcardid;
            }

            UInt32 buGangCardId = 0;
            if (PushDownHelp.CanBuGang(roomitem, roleitem, ref buGangCardId))
            {
                // 判断补杠
                readyInfo.gangType = EGangCardType.BuGang;
                readyInfo.cardid = buGangCardId;
            }

            if (IsPengGangHuReady(readyItem))
            {
                NotifyPengGangHuReady(readyItem);
            }
            else
            {
                NotifyNeedHandOutCard();
            }
        }

        public List<UInt32> GetGangCards(EMJCardType type)
        {
            List<UInt32> lstResults = new List<UInt32>();
            foreach (var cardid in currentRound.handCards)
            {
                if (MJCardHelp.GetMJType(cardid) == type)
                {
                    lstResults.Add(cardid);
                }
            }
            return lstResults;
        }

        /// <summary>
        /// 是否杠其他人出的牌
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public bool CanMingGang(UInt32 cardid)
        {
            EMJCardType type = MJCardHelp.GetMJType(cardid);

            stCheckRoomItem room = new stCheckRoomItem();
            stCheckRoleItem role = new stCheckRoleItem();
            GetCheckRoomRoleInfo(room, role);

            //if (m_room.config.bLackingDoor)
            //{
            //    if (role.IsLackingDoorType(type))
            //    {
            //        return false;
            //    }
            //}

            int count = 0;
            foreach (var handcard in currentRound.handCards)
            {
                if (type == MJCardHelp.GetMJType(handcard))
                {
                    count++;
                }
            }
            if (3 != count)
            {
                return false;
            }

            // 听口变了
            if(PushDownHelp.CheckReadyChange(type, room, role))
            {
                return false;
            }

            return true;
        }

        public void SendMsgToClient(ProtoBody msg)
        {
            BattlePlayer player = g.playerMgr.FindPlayer(roleid);
            if (null == player)
            {
                return;
            }
            player.SendMsgToClient(msg);
        }
    }
}
