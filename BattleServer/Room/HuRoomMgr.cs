﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Debugger;
using Log;
using Protocols;
using ProtoBuf;
using Redis;
using ServerLib;
using StackExchange.Redis;
using BattleServer;

namespace MahjongPushDown
{
    public class HuRoomMgr
    {
        public void AddRoom(HuRoom room)
        {
            if(HasRoom(room.roomUuid))
            {
                return;
            }

            mapRooms.Add(room.roomUuid, room);
        }

        public bool HasRoom(UInt64 roomuuid)
        {
            return mapRooms.ContainsKey(roomuuid);
        }

        public void RemoveRoom(UInt64 roomuuid)
        {
            mapRooms.Remove(roomuuid);
        }

        public void DeleteRoom(UInt64 roomuuid)
        {
            HuRoom room = GetRoom(roomuuid);
            if (null == room)
            {
                return;
            }

            mapRooms.Remove(roomuuid);

            room.DeleteFromDb();
        }

        public HuRoom GetRoom(UInt64 roomuuid)
        {
            if(mapRooms.ContainsKey(roomuuid))
            {
                return mapRooms[roomuuid];
            }

            return null;
        }

        public async Task<HuRoom> FindRoomAsync(UInt64 roomuuid)
        {
            HuRoom room = GetRoom(roomuuid);
            if (null != room)
            {
                return room;
            }

            room = new HuRoom(roomuuid);
            bool bResult = await room.LoadFromDbAsync();
            if (!bResult)
            {
                return null;
            }

            AddRoom(room);

            return room;
        }

        public void OnRoleOnline(BattlePlayer player)
        {
            HuRoom room = GetRoom(player.RoomUuid);
            if (null == room)
            {
                return;
            }
            room.OnRoleOnline(player);
        }

        public void OnRoleOffline(BattlePlayer player)
        {
            HuRoom room = GetRoom(player.RoomUuid);
            if (null == room)
            {
                return;
            }
            room.OnRoleOffline(player);
        }

        public void OnSecond()
        {
            List<HuRoom> lstDels = new List<HuRoom>();
            foreach(var item in mapRooms)
            {
                if(item.Value.CheckDismissTimer())
                {
                    lstDels.Add(item.Value);
                }
            }

            foreach(var room in lstDels)
            {
                room.NotifyDismissSuccess();
                room.RoomClose();
            }
        }

        //////////////////////////////////////////////////////////////////////////

        // 所有房间
        Dictionary<UInt64, HuRoom> mapRooms = new Dictionary<UInt64, HuRoom>();
    }
}
