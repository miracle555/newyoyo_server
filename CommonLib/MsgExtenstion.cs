﻿/* ============================================================
* Author:       刘冰生
* Time:         2017/5/1 12:03:32
* FileName:     MsgExtension.cs
* Purpose:      msg扩展
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Runtime.CompilerServices;
using ServerLib.Bus;

namespace Protocols
{
    public struct stMsgItem
    {
        public UInt64 uuid;
        public TaskCompletionSource<ProtoBody> source;
        public DateTime expireTime;
        public stMsgItem(UInt64 uuid, TaskCompletionSource<ProtoBody> source, DateTime expireTime)
        {
            this.uuid = uuid;
            this.source = source;
            this.expireTime = expireTime;
        }
    }

    public class MsgExtension
    {
        public Task<ProtoBody> BackAsync(UInt64 uuid)
        {
            var tcs = new TaskCompletionSource<ProtoBody>();

            AddTaskSource(uuid, tcs);

            return tcs.Task;
        }

        /// <summary>
        /// 任务列表
        /// </summary>
        Dictionary<UInt64, stMsgItem> mapSources = new Dictionary<UInt64, stMsgItem>();

        void AddTaskSource(UInt64 uuid, TaskCompletionSource<ProtoBody> source)
        {
            if(mapSources.ContainsKey(uuid))
            {
                return;
            }

            stMsgItem item = new stMsgItem(uuid, source, currentTime.AddSeconds(5));

            mapSources.Add(uuid, item);
        }

        public void HandleMsg(UInt64 uuid, ProtoBody msg)
        {
            SetResult(uuid, msg);
        }

        void SetResult(UInt64 uuid, ProtoBody msg)
        {
            if (!mapSources.ContainsKey(uuid))
            {
                return;
            }

            stMsgItem item = mapSources[uuid];
            mapSources.Remove(uuid);

            item.source.TrySetResult(msg);
        }

        DateTime currentTime = DateTime.MinValue;

        public void CheckTimeExpire()
        {
            List<UInt64> lstMsgs = new List<UInt64>();

            foreach(var item in mapSources)
            {
                if(currentTime > item.Value.expireTime)
                {
                    lstMsgs.Add(item.Key);
                }
            }

            foreach(var uuid in lstMsgs)
            {
                SetResult(uuid, null);
            }
        }

        UInt64 _uuid = 0;
        public UInt64 GenerateUuid()
        {
            _uuid++;
            return _uuid;
        }

        public void Init(IBusService serverice)
        {
            serverice.RegisterSpyFunction(SpyFunction);
        }

        public void Update(DateTime dateTime)
        {
            currentTime = dateTime;

            CheckTimeExpire();
        }

        bool SpyFunction(stServerBody serverBody, ProtoBody body)
        {


            return false;
        }
    }
}
