﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocols;
using ServerLib;
using ServerLib.Bus;
using System.Net;
using Event;
using Log;

namespace Module
{
    public abstract class LogicModule : ILogicModule
    {
        public LogicModule()
        {
        }

        public virtual UInt32 Id
        {
            get
            {
                return 0xff;
            }
        }

        public bool Init()
        {
            return OnInit();
        }

        public bool PostInit()
        {
            return OnPostInit();
        }

        public void PreDestroy()
        {
            OnPreDestroy();
        }

        public void Destroy()
        {
            OnDestroy();
        }

        public void Tick()
        {
            OnTick();
        }

        protected virtual bool OnInit()
        {
            return true;
        }

        protected virtual bool OnPostInit()
        {
            return true;
        }

        protected virtual void OnPreDestroy()
        {
        }

        protected virtual void OnDestroy()
        {
        }

        protected virtual void OnTick()
        {
        }

        public void RegisterMsgHandler<T>(MsgHandlerEx<T> msgHandler) where T : ProtoBody
        {
            env.BusService.RegisterMsgHandler(msgHandler);
        }

        public void RegisterMsgHandler<T>(MSGID protoID, MsgHandler<T> msgHandler) where T : ProtoBody
        {
            env.BusService.RegisterMsgHandler(protoID, msgHandler);
        }

        public void RegisterMsgHandler<T>(MSGID protoID, MsgHandlerAll<T> msgHandler) where T : ProtoBody
        {
            env.BusService.RegisterMsgHandler(protoID, msgHandler);
        }
    }
}
