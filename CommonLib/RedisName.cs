﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using ServerLib;
using ServerLib.Bus;
using System.Net;
using Event;
using Log;

namespace Redis
{
    public class RedisName
    {
        public RedisName()
        {
        }

        public const string roomhelp = "roomhelp";
        public const string datacenter = "datacenter";
        public const string rechargedata = "rechargedata";

        // 推倒胡redis命名
        public const string hu_rooms = "hu_rooms";
        public const string basic_hu_room_ = "basic_hu_room_";
        public const string detail_hu_room_ = "detail_hu_room_";

        // 斗地主redis命名
        public const string lord_rooms = "lord_rooms";
        public const string basic_lord_room_ = "basic_lord_room_";
        public const string detail_lord_room_ = "detail_lord_room_";

        // 抠点redis命名
        public const string point_rooms = "point_rooms";
        public const string basic_point_room_ = "basic_point_room_";
        public const string detail_point_room_ = "detail_point_room_";


        // 扎金花redis命名
        public const string flower_rooms = "flower_rooms";
        public const string basic_flower_room_ = "basic_flower_room_";
        public const string detail_flower_room_ = "detail_flower_room_";

        //运城贴金redis命名
        public const string yuncheng_rooms = "yuncheng_rooms";
        public const string basic_yuncheng_room_ = "basic_yuncheng_room_";
        public const string detail_yuncheng_room_ = "detail_yuncheng_room_";


        public static string all_agents = "all_agents";

        /// <summary>
        /// 经验排行
        /// </summary>
        public static string rank_exp = "rank_exp";
    }
}
