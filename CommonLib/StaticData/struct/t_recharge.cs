﻿using System;
using System.Collections.Generic;
using System.Text;
using StaticData;

public partial class t_recharge
{
    public Dictionary<uint, t_recharge> mapItems = new Dictionary<uint, t_recharge>();

    public bool Init()
    {
        mapItems.Clear();

        object o = StaticDataMgr.readObject("t_recharge.dat", typeof(t_recharge));

        t_recharge data = o as t_recharge;
        if (data == null)
        {
            return false;
        }

        foreach (t_recharge config in data.ProtoList)
        {
            mapItems[config.f_id] = config;
        }

        return true;
    }

    public t_recharge GetConfig(uint id)
    {
        if (mapItems.ContainsKey(id))
            return mapItems[id];
        return null;
    }

    public bool HasItem(UInt32 id)
    {
        return mapItems.ContainsKey(id);
    }

    public void Foreach(Action<t_recharge> callback)
    {
        foreach (var item in mapItems)
        {
            callback(item.Value);
        }
    }
}