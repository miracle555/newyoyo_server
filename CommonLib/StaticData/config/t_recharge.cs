using System;
using ProtoBuf;
using System.Collections.Generic;
[ProtoContract]
public partial class t_recharge
{
    [ProtoMember(127)]
    public t_recharge[] ProtoList;
        [ProtoMember(1)]
    public UInt32 f_id;
        [ProtoMember(2)]
    public UInt32 f_itemid;
        [ProtoMember(3)]
    public string f_name;
        [ProtoMember(4)]
    public Int64 f_price;
        [ProtoMember(5)]
    public string f_icon;
        [ProtoMember(6)]
    public Int32 f_count;
    public t_recharge(){}
}
