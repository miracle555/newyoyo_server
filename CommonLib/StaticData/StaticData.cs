﻿/********************************************************************
	purpose:    游戏模块	
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using ServerLib.Bus;
using ServerLib;
using Log;
using Protocols;
using Utility.Debugger;
using System.Collections;
using Utility.Coroutines;
using Event;
using Config;
using System.Runtime.CompilerServices;
using ProtoBuf;
using System.IO;
using Redis;

namespace StaticData
{
    public class StaticDataMgr
    {
        public StaticDataMgr()
            : base()
        {
        }

        public t_recharge rechargeData = new t_recharge();

        public bool Init()
        {
            if (!rechargeData.Init())
            {
                return false;
            }

            return true;
        }

        static object protoDeserialize(Stream source, Type r)
        {
            return Serializer.NonGeneric.Deserialize(r, source);
        }

        public static object readObject(string file, Type t)
        {
            object obj = null;

            try
            {
                string strFile = "..\\StaticData\\" + file;
                using (FileStream fs = new FileStream(strFile, FileMode.Open, FileAccess.Read)) //创建文件流
                {
                    byte[] data = new byte[fs.Length];
                    fs.Read(data, 0, (int)fs.Length);

                    using (MemoryStream ms = new MemoryStream(data))
                    {
                        ms.Position = 0;
                        obj = protoDeserialize(ms, t);
                    }
                }
            }
            catch(Exception ex)
            {
                LogSys.Error("[StaticData][readObject],读取二进制文件出错,exception={0}", ex.Message);
                return null;
            }

            return obj;
        }
    }
}
