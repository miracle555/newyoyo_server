﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Protocols;
using ProtoBuf;

namespace Event
{
	/// <summary>
	/// PlayerLogin
	/// </summary>
	[ProtoContract]
	public class EventPlayerLogin : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.Event_PlayerLogin;
            }
        }
		
		//accountId
		[ProtoMember(1)]
		public ulong accountId
		{
			get;
			set;
		}
		
		//Level
		[ProtoMember(2)]
		public int Level
		{
			get;
			set;
		}

		public EventPlayerLogin()
		{
		}
	}

	/// <summary>
	/// PlayerLogout
	/// </summary>
	[ProtoContract]
	public class EventPlayerLogout : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.Event_PlayerLogout;
            }
        }
		
		//accountId
		[ProtoMember(1)]
		public ulong accountId
		{
			get;
			set;
		}

		public EventPlayerLogout()
		{
		}
	}

	/// <summary>
	/// CreateRole
	/// </summary>
	[ProtoContract]
	public class EventCreateRole : IEventBase
	{
		public int Id
        {
            get
            {
                return EventId.Event_CreateRole;
            }
        }
		
		//accountId
		[ProtoMember(1)]
		public ulong accountId
		{
			get;
			set;
		}
		
		//RoleName
		[ProtoMember(2)]
		public string RoleName
		{
			get;
			set;
		}

		public EventCreateRole()
		{
		}
	}
}
