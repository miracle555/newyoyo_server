﻿using System;
using System.Collections.Generic;
using Protocols.Majiang;
using Protocols.MajiangPushDown;

namespace HelpAsset
{
    namespace MahjongPushDown
    {
        public class VecCards
        {
            public int[][] m_vecCards = new int[4][];


            public VecCards()
            {
                for (int i = 0; i < 4; i++)
                {
                    m_vecCards[i] = new int[10];
                }

                Clear();
            }

            public void Clear()
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        m_vecCards[i][j] = 0;
                    }
                }
            }

            public void FillHandCards(HashSet<UInt32> handCards)
            {
                Clear();
                foreach (var cardid in handCards)
                {
                    EMJCardType type = MJCardHelp.GetMJType(cardid);
                    MJCardHelp.AddCardTypeToVector(m_vecCards, type);
                }
            }

            public void AddCardTypeToVector(EMJCardType type)
            {
                MJCardHelp.AddCardTypeToVector(m_vecCards, type);
            }

            public void AddCardToVector(UInt32 cardid)
            {
                EMJCardType type = MJCardHelp.GetMJType(cardid);
                MJCardHelp.AddCardTypeToVector(m_vecCards, type);
            }

            public void FillMingAndHandCards(HashSet<UInt32> handCards, List<MingCardItem> showCards)
            {
                Clear();
                foreach (var cardid in handCards)
                {
                    EMJCardType type = MJCardHelp.GetMJType(cardid);
                    MJCardHelp.AddCardTypeToVector(m_vecCards, type);
                }
                foreach (var item in showCards)
                {
                    foreach (var cardid in item.showCards)
                    {
                        MJCardHelp.AddCardTypeToVector(m_vecCards, item.cardType);
                    }
                }
            }

            public void FillMingAndHandCards(VecCards handCards, List<MingCardItem> showCards)
            {
                Clear();

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        m_vecCards[i][j] = handCards.m_vecCards[i][j];
                    }
                }

                foreach (var item in showCards)
                {
                    foreach (var cardid in item.showCards)
                    {
                        MJCardHelp.AddCardTypeToVector(m_vecCards, item.cardType);
                    }
                }
            }

            /// <summary>
            /// 是否是清一色
            /// </summary>
            /// <returns></returns>
            public bool IsSameColor
            {
                get
                {
                    return MJCardHelp.IsSameColor(m_vecCards);
                }
            }

            /// <summary>
            /// 是否是一条龙
            /// </summary>
            /// <returns></returns>
            public bool IsOneDragon
            {
                get
                {
                    return MJCardHelp.IsOneDragon(m_vecCards);
                }
            }

            /// <summary>
            /// 是否是7小对
            /// </summary>
            /// <returns></returns>
            public bool IsSevenPairs
            {
                get
                {
                    return MJCardHelp.IsSevenPairs(m_vecCards);
                }
            }

            public bool IsThirteenOrphans
            {
                get
                {
                    return MJCardHelp.IsThirteenOrphans(m_vecCards);
                }
            }

            /// <summary>
            /// 是否是豪华7小对
            /// </summary>
            /// <returns></returns>
            public bool IsLuxurySevenPairs
            {
                get
                {
                    return MJCardHelp.IsLuxurySevenPairs(m_vecCards);
                }
            }

            /// <summary>
            /// 是否缺一门
            /// </summary>
            /// <returns></returns>
            public bool IsLackingDoor(EMahjongCard doorType)
            {
                return MJCardHelp.IsLackingDoor(m_vecCards, doorType);
            }

            public bool CommonHuCard()
            {
                return MJCardHelp.CommonHuCard(m_vecCards);
            }

            /// <summary>
            /// 获得所有暗杠类型
            /// </summary>
            /// <returns></returns>
            public List<EMJCardType> GetAllAnGangTypes()
            {
                List<EMJCardType> lstResults = new List<EMJCardType>();

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 1; j < 10; j++)
                    {
                        if (m_vecCards[i][j] == 4)
                        {
                            EMJCardType type = MJCardHelp.GetVectorCardType(i, j);
                            lstResults.Add(type);
                        }
                    }
                }

                return lstResults;
            }
        }

        /// <summary>
        /// 检测自己碰杠胡信息
        /// </summary>
        public class stCheckRoleItem
        {
            /// <summary>
            /// 手牌
            /// </summary>
            public HashSet<UInt32> handCards = new HashSet<UInt32>();

            /// <summary>
            /// 明牌
            /// </summary>
            public List<MingCardItem> showCards = new List<MingCardItem>();

            /// <summary>
           /// 是否已经报听
           /// </summary>
            public bool AlreadyBaoTing = false;

            /// <summary>
            /// 对于缺一门，玩家选择了缺此门
            /// </summary>
            public EMahjongCard doorType = EMahjongCard.Null;

            /// <summary>
            /// 如果已经听牌，这是听口
            /// </summary>
            public List<EMJCardType> lstReadyTypes = new List<EMJCardType>();

            public UInt32 GetHandCardByType(EMJCardType type)
            {
                foreach (var cardid in handCards)
                {
                    if (MJCardHelp.GetMJType(cardid) == type)
                    {
                        return cardid;
                    }
                }

                return 0;
            }

            /// <summary>
            /// 添加手牌
            /// </summary>
            /// <param name="cardid"></param>
            public void AddHandCard(UInt32 cardid)
            {
                handCards.Add(cardid);
            }

            /// <summary>
            /// 删除手牌
            /// </summary>
            /// <param name="cardid"></param>
            public void RemoveHandCard(UInt32 cardid)
            {
                handCards.Remove(cardid);
            }

            public void RemoveHandCards(List<UInt32> lstCards)
            {
                foreach(var cardid in lstCards)
                {
                    RemoveHandCard(cardid);
                }
            }

            public void AddHandCards(List<UInt32> lstCards)
            {
                foreach (var cardid in lstCards)
                {
                    AddHandCard(cardid);
                }
            }

            /// <summary>
            /// 获得指定类型的手牌
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public List<UInt32> GetHandCardsByType(EMJCardType type)
            {
                List<UInt32> lstResults = new List<UInt32>();

                foreach (var cardid in handCards)
                {
                    if (MJCardHelp.GetMJType(cardid) == type)
                    {
                        lstResults.Add(cardid);
                    }
                }

                return lstResults;
            }

            public List<UInt32> RemoveHandCardType(EMJCardType type)
            {
                List<UInt32> lstRemoveCards = new List<UInt32>();
                foreach (var cardid in handCards)
                {
                    if (MJCardHelp.GetMJType(cardid) == type)
                    {
                        lstRemoveCards.Add(cardid);
                    }
                }

                foreach (var cardid in lstRemoveCards)
                {
                    handCards.Remove(cardid);
                }

                return lstRemoveCards;
            }

            public bool IsLackingDoorType(EMJCardType type)
            {
                EMahjongCard tempType = MJCardHelp.DoorCardType(type);
                return tempType == doorType;
            }
        }

        /// <summary>
        /// 检测自己碰杠胡房间信息
        /// </summary>
        public class stCheckRoomItem
        {
            public RoomConfig config = new RoomConfig();
        }

        public class PushDownHelp
        {
            /// <summary>
            /// 是否是十三幺
            /// </summary>
            /// <param name="setCards">手牌</param>
            /// <param name="showCards">明牌</param>
            /// <param name="roomConfig">房间配置</param>
            /// <returns></returns>
            //public static bool IsThirteenOrphans(stCheckRoomItem room, stCheckRoleItem role)
            //{
            //    // 不带风不可能有十三幺
            //    if (!room.config.bWindCard)
            //    {
            //        return false;
            //    }

            //    if (role.showCards.Count != 0)
            //    {
            //        return false;
            //    }

            //    return MJCardHelp.IsThirteenOrphans(role.handCards);
            //}

            /// <summary>
            /// 是否可以自摸胡
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanHuSelf(stCheckRoomItem room, stCheckRoleItem role)
            {
                EHuCardType huType = EHuCardType.Null;
                return CanHuSelf(ref huType, room, role);
            }

            /// <summary>
            /// 是否可以自摸胡
            /// </summary>
            /// <param name="huType"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanHuSelf(ref EHuCardType huType, stCheckRoomItem room, stCheckRoleItem role)
            {
                huType = EHuCardType.Null;

                // 没报停不能胡
                if (room.config.bBaoTing)
                {
                    if (!role.AlreadyBaoTing)
                    {
                        return false;
                    }
                }

                VecCards vecCards = new VecCards();
                vecCards.FillHandCards(role.handCards);

                bool bHu = PushDownHelp.CanHuCard(vecCards, ref huType, room, role);

                if (bHu)
                {
                    if (!room.config.IsBigHu)
                    {
                        huType = EHuCardType.CommonHu;
                    }
                }

                return bHu;
            }

            /// <summary>
            /// 是否可以胡该类型的牌
            /// </summary>
            /// <param name="type"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            static bool CanHuCardInner(EMJCardType type, stCheckRoomItem room, stCheckRoleItem role)
            {
                VecCards vecHandCards = new VecCards();
                vecHandCards.FillHandCards(role.handCards);
                vecHandCards.AddCardTypeToVector(type);

                EHuCardType settleType = EHuCardType.Null;

                return PushDownHelp.CanHuCard(vecHandCards, ref settleType, room, role);
            }

            /// <summary>
            /// 获取听牌类型
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            static List<EMJCardType> GetReadyCardTypes(stCheckRoomItem room, stCheckRoleItem role)
            {
                List<EMJCardType> lstReadyTypes = new List<EMJCardType>();
                for (EMJCardType type = EMJCardType.OneRope; type <= EMJCardType.WhiteFace; type++)
                {
                    if (CanHuCardInner(type, room, role))
                    {
                        lstReadyTypes.Add(type);
                    }
                }

                return lstReadyTypes;
            }

            /// <summary>
            /// 是否可以听牌
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanReadyCard(stCheckRoomItem room, stCheckRoleItem role)
            {
                List<UInt32> lstReadyCards = new List<UInt32>();
                return CanReadyCard(lstReadyCards, room, role);
            }

            /// <summary>
            /// 是否可以听牌
            /// </summary>
            /// <param name="lstReadyCards"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanReadyCard(List<UInt32> lstReadyCards, stCheckRoomItem room, stCheckRoleItem role)
            {

                if (!room.config.bBaoTing)
                {
                    return false;
                }

                if (role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bReady = false;

                List<UInt32> lstHandCards = new List<UInt32>();
                lstHandCards.AddRange(role.handCards);
                foreach (var cardid in lstHandCards)
                {
                    if (CanReadyCard(cardid, room, role))
                    {
                        bReady = true;
                        lstReadyCards.Add(cardid);
                    }
                }

                return bReady;
            }

            /// <summary>
            /// 听口是否改变
            /// </summary>
            /// <param name="cardid"></param>
            /// <returns></returns>
            public static bool CheckReadyChange(UInt32 cardid, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (!room.config.bBaoTing)
                {
                    return false;
                }

                if (!role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bResult = true;

                // 如果已经报听，听口不能变
                role.RemoveHandCard(cardid);

                // 听口改变
                List<EMJCardType> lstReadyTypes = GetReadyCardTypes(room, role);
                if (MJCardHelp.IsSameTypes(lstReadyTypes, role.lstReadyTypes))
                {
                    bResult = false;
                }

                role.AddHandCard(cardid);

                return bResult;
            }

            /// <summary>
            /// 听口是否改变
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public static bool CheckReadyChange(EMJCardType type, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (!room.config.bBaoTing)
                {
                    return false;
                }

                if (!role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bResult = true;

                // 如果已经报听，听口不能变
                List<UInt32> lstRemoveCards = role.RemoveHandCardType(type);

                // 听口改变
                List<EMJCardType> lstReadyTypes = GetReadyCardTypes(room, role);
                if (MJCardHelp.IsSameTypes(lstReadyTypes, role.lstReadyTypes))
                {
                    bResult = false;
                }

                role.AddHandCards(lstRemoveCards);

                return bResult;
            }

            public static bool CheckReadyChange(List<UInt32> lstRemoveCards, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (!room.config.bBaoTing)
                {
                    return false;
                }

                if (!role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bResult = true;

                // 如果已经报听，听口不能变
                role.RemoveHandCards(lstRemoveCards);

                // 听口改变
                List<EMJCardType> lstReadyTypes = GetReadyCardTypes(room, role);
                if (MJCardHelp.IsSameTypes(lstReadyTypes, role.lstReadyTypes))
                {
                    bResult = false;
                }

                role.AddHandCards(lstRemoveCards);

                return bResult;
            }

            /// <summary>
            /// 移除该牌后听牌类型
            /// </summary>
            /// <param name="cardid"></param>
            /// <returns></returns>
            public static List<EMJCardType> GetReadyTypes(UInt32 cardid, stCheckRoomItem room, stCheckRoleItem role)
            {
                role.RemoveHandCard(cardid);

                List<EMJCardType> lstTypes = GetReadyCardTypes(room, role);

                role.AddHandCard(cardid);

                return lstTypes;
            }

            /// <summary>
            /// 是否可以听牌
            /// </summary>
            /// <param name="cardid"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanReadyCard(UInt32 cardid, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (!room.config.bBaoTing)
                {
                    return false;
                }

                if (role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bReady = false;

                role.RemoveHandCard(cardid);

                List<EMJCardType> lstTypes = GetReadyCardTypes(room, role);
                if (lstTypes.Count != 0)
                {
                    bReady = true;
                }

                role.AddHandCard(cardid);

                return bReady;
            }

            /// <summary>
            /// 是否可以暗杠
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <param name="outGangCardId"></param>
            /// <returns></returns>
            public static bool CanAnGang(stCheckRoomItem room, stCheckRoleItem role, ref EMJCardType outAnGangType)
            {
                outAnGangType = EMJCardType.Null;

                VecCards vecHandcards = new VecCards();
                vecHandcards.FillHandCards(role.handCards);

                List<EMJCardType> lstAnGangTypes = vecHandcards.GetAllAnGangTypes();

                if (lstAnGangTypes.Count == 0)
                {
                    return false;
                }

                //if (room.config.bLackingDoor)
                //{
                //    if (role.IsLackingDoorType(type))
                //    {
                //        return false;
                //    }
                //}

                if (!role.AlreadyBaoTing)
                {
                    outAnGangType = lstAnGangTypes[0];
                    return true;
                }

                // 听口变了
                foreach(var type in lstAnGangTypes)
                {
                    if(!CheckReadyChange(type, room, role ))
                    {
                        outAnGangType = type;
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// 获得手牌中的所有补杠牌
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static List<UInt32> GetAllBuGangCards(stCheckRoomItem room, stCheckRoleItem role)
            {
                List<UInt32> lstResults = new List<UInt32>();

                foreach (var item in role.showCards)
                {
                    if (item.mingType != EMingCardType.Peng)
                    {
                        continue;
                    }

                    UInt32 cardid = role.GetHandCardByType(item.cardType);
                    if (cardid != 0)
                    {
                        lstResults.Add(cardid);
                    }
                }

                return lstResults;
            }

            /// <summary>
            /// 获得补杠牌ID
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanBuGang(stCheckRoomItem room, stCheckRoleItem role, ref UInt32 outBuGangCardId)
            {
                outBuGangCardId = 0;

                List<UInt32> lstCards =  GetAllBuGangCards(room, role);
                if (0 == lstCards.Count)
                {
                    return false;
                }

                //if (room.config.bLackingDoor)
                //{
                //    if (role.IsLackingDoorType(type))
                //    {
                //        return false;
                //    }
                //}

                if (!role.AlreadyBaoTing)
                {
                    outBuGangCardId = lstCards[0];
                    return true;
                }

                // 听口变了
                foreach(var cardid in lstCards)
                {
                    if (!PushDownHelp.CheckReadyChange(cardid, room, role))
                    {
                        outBuGangCardId = cardid;
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// 是否可以胡牌
            /// </summary>
            /// <param name="vecHandCards"></param>
            /// <param name="outHuType"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanHuCard(VecCards vecHandCards, ref EHuCardType outHuType, stCheckRoomItem room, stCheckRoleItem role)
            {
                VecCards vecHandAndMingCards = new VecCards();
                vecHandAndMingCards.FillMingAndHandCards(vecHandCards, role.showCards);

                // 需要缺一门但没有缺一门
                if (room.config.bLackingDoor)
                {
                    if (!vecHandAndMingCards.IsLackingDoor(role.doorType))
                    {
                        return false;
                    }
                }

                // 普通胡牌
                if (vecHandCards.CommonHuCard())
                {
                    outHuType = EHuCardType.CommonHu;

                    // 是否是清一色
                    if (vecHandAndMingCards.IsSameColor)
                    {
                        outHuType = EHuCardType.SameColor;
                    }
                    else if (vecHandCards.IsOneDragon)
                    {
                        outHuType = EHuCardType.OneDragon;
                    }

                    return true;
                }

                if (vecHandCards.IsSevenPairs)
                {
                    // 普通7小对
                    outHuType = EHuCardType.SenvenPairs;
                    if (vecHandCards.IsLuxurySevenPairs)
                    {
                        outHuType = EHuCardType.LuxurySevenPairs;
                    }
                    return true;
                }

                // 十三幺
                if (vecHandCards.IsThirteenOrphans)
                {
                    outHuType = EHuCardType.ThirteenOrphans;
                    return true;
                }

                return false;
            }

        }
    }
}
