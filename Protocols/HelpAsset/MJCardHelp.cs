﻿using System;
using System.Collections.Generic;
using Protocols.Majiang;

namespace HelpAsset
{
    public class MJCardHelp
    {
        public static EMJCardType GetMJType(UInt32 cardid)
        {
            EMJCard card = (EMJCard)cardid;

            // 条
            if (card >= EMJCard.OneRope1 && card <= EMJCard.NineRope4)
            {
                UInt32 temp = cardid - (UInt32)EMJCard.OneRope1;
                temp = temp / 4 + (UInt32)EMJCardType.OneRope;
                return (EMJCardType)temp;
            }

            // 筒
            if (card >= EMJCard.OneBucket1 && card <= EMJCard.NineBucket4)
            {
                UInt32 temp = cardid - (UInt32)EMJCard.OneBucket1;
                temp = temp / 4 + (UInt32)EMJCardType.OneBucket;
                return (EMJCardType)temp;
            }

            // 万
            if (card >= EMJCard.OneMillion1 && card <= EMJCard.NineMillion4)
            {
                UInt32 temp = cardid - (UInt32)EMJCard.OneMillion1;
                temp = temp / 4 + (UInt32)EMJCardType.OneMillion;
                return (EMJCardType)temp;
            }

            if (card >= EMJCard.EastWind1 && card <= EMJCard.EastWind4)
            {
                return EMJCardType.EastWind;
            }

            if (card >= EMJCard.SouthWind1 && card <= EMJCard.SouthWind4)
            {
                return EMJCardType.SouthWind;
            }

            if (card >= EMJCard.WestWind1 && card <= EMJCard.WestWind4)
            {
                return EMJCardType.WestWind;
            }

            if (card >= EMJCard.NorthWind1 && card <= EMJCard.NorthWind4)
            {
                return EMJCardType.NorthWind;
            }

            if (card >= EMJCard.RedDragon1 && card <= EMJCard.RedDragon4)
            {
                return EMJCardType.RedDragon;
            }

            if (card >= EMJCard.Fortune1 && card <= EMJCard.Fortune4)
            {
                return EMJCardType.Fortune;
            }

            if (card >= EMJCard.WhiteFace1 && card <= EMJCard.WhiteFace4)
            {
                return EMJCardType.WhiteFace;
            }

            return EMJCardType.Null;
        }

        /// <summary>
        /// 获得点数
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public static int GetCardPoint(UInt32 cardid)
        {
            EMJCardType type = GetMJType(cardid);

            return GetCardPoint(type);
        }

        public static bool IsRightDoor(EMahjongCard doorType)
        {
            if(doorType == EMahjongCard.RopeCard)
            {
                return true;
            }

            if(doorType == EMahjongCard.BucketCard)
            {
                return true;
            }

            if(doorType == EMahjongCard.MillionCard)
            {
                return true;
            }

            return false;
        } 

        public static int GetCardPoint(EMJCardType type)
        {
            if (IsRope(type))
            {
                return type - EMJCardType.OneRope + 1;
            }

            if (IsBucket(type))
            {
                return type - EMJCardType.OneBucket + 1;
            }

            if (IsMillion(type))
            {
                return type - EMJCardType.OneMillion + 1;
            }
            return 10;
        }

        /// <summary>
        /// 是否是条牌
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public static bool IsRope(UInt32 cardid)
        {
            EMJCardType type = GetMJType(cardid);
            return IsRope(type);
        }

        /// <summary>
        /// 是否是条牌
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsRope(EMJCardType type)
        {
            if (type >= EMJCardType.OneRope && type <= EMJCardType.NineRope)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否是筒牌
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public static bool IsBucket(UInt32 cardid)
        {
            EMJCardType type = GetMJType(cardid);
            return IsBucket(type);
        }

        /// <summary>
        /// 是否是筒牌
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsBucket(EMJCardType type)
        {
            if (type >= EMJCardType.OneBucket && type <= EMJCardType.NineBucket)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否是万牌
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public static bool IsMillion(UInt32 cardid)
        {
            EMJCardType type = GetMJType(cardid);
            return IsMillion(type);
        }

        /// <summary>
        /// 是否是万牌
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsMillion(EMJCardType type)
        {
            if (type >= EMJCardType.OneMillion && type <= EMJCardType.NineMillion)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否是字牌
        /// </summary>
        /// <param name="cardid"></param>
        /// <returns></returns>
        public static bool IsWordCard(UInt32 cardid)
        {
            EMJCardType type = GetMJType(cardid);
            return IsWordCard(type);
        }

        /// <summary>
        /// 是否是字牌
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsWordCard(EMJCardType type)
        {
            if (type >= EMJCardType.EastWind && type <= EMJCardType.WhiteFace)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否是十三幺
        /// </summary>
        /// <returns></returns>
        //public static bool IsThirteenOrphans(HashSet<UInt32> setCards)
        //{
        //    if (setCards.Count != 14)
        //    {
        //        return false;
        //    }

        //    List<EMJCardType> lstTypes = new List<EMJCardType>();
        //    foreach (var cardid in setCards)
        //    {
        //        EMJCardType type = GetMJType(cardid);
        //        lstTypes.Add(type);
        //    }

        //    List<EMJCardType> lstTargets = new List<EMJCardType>();

        //    lstTargets.Add(EMJCardType.EastWind);
        //    lstTargets.Add(EMJCardType.SouthWind);
        //    lstTargets.Add(EMJCardType.WestWind);
        //    lstTargets.Add(EMJCardType.NorthWind);
        //    lstTargets.Add(EMJCardType.RedDragon);
        //    lstTargets.Add(EMJCardType.Fortune);
        //    lstTargets.Add(EMJCardType.WhiteFace);
        //    lstTargets.Add(EMJCardType.OneRope);
        //    lstTargets.Add(EMJCardType.NineRope);
        //    lstTargets.Add(EMJCardType.OneBucket);
        //    lstTargets.Add(EMJCardType.NineBucket);
        //    lstTargets.Add(EMJCardType.OneMillion);
        //    lstTargets.Add(EMJCardType.NineMillion);

        //    foreach(var type in lstTargets)
        //    {
        //        if(!lstTypes.Contains(type))
        //        {
        //            return false;
        //        }
        //        lstTypes.Remove(type);
        //    }

        //    if(lstTypes.Count != 1)
        //    {
        //        return false;
        //    }

        //    EMJCardType lastype = lstTypes[0];
        //    if(!lstTargets.Contains(lastype))
        //    {
        //        return false; 
        //    }

        //    return true;
        //}

        public static bool IsThirteenOrphans(int[][] vecCards)
        {
            List<int> lstValues = new List<int>();
            lstValues.Add(vecCards[0][1]);
            lstValues.Add(vecCards[0][9]);
            lstValues.Add(vecCards[1][1]);
            lstValues.Add(vecCards[1][9]);
            lstValues.Add(vecCards[2][1]);
            lstValues.Add(vecCards[2][9]);

            lstValues.Add(vecCards[3][1]);
            lstValues.Add(vecCards[3][2]);
            lstValues.Add(vecCards[3][3]);
            lstValues.Add(vecCards[3][4]);
            lstValues.Add(vecCards[3][5]);
            lstValues.Add(vecCards[3][6]);
            lstValues.Add(vecCards[3][7]);

            int count = 0;
            foreach(var item in lstValues)
            {
                if(item == 0)
                {
                    return false;
                }

                count += item;
            }

            if(count != 14)
            {
                return false;
            }

            return true;
        }

        public static void DeleteCardTypeToVector(int[][] vecCards, EMJCardType type)
        {
            // 万牌
            if (IsMillion(type))
            {
                int index = (int)type - (int)EMJCardType.OneMillion + 1;
                vecCards[0][0] -= vecCards[0][index];
                vecCards[0][index] = 0;
            }
            else if (IsBucket(type))
            {
                int index = (int)type - (int)EMJCardType.OneBucket + 1;
                vecCards[1][0] -= vecCards[1][index];
                vecCards[1][index] = 0;
            }
            else if (IsRope(type))
            {
                int index = (int)type - (int)EMJCardType.OneRope + 1;
                vecCards[2][0] -= vecCards[2][index];
                vecCards[2][index] = 0;
            }
            else if (IsWordCard(type))
            {
                int index = (int)type - (int)EMJCardType.EastWind + 1;
                vecCards[3][0] -= vecCards[3][index];
                vecCards[3][index] = 0;
            }
        }

        public static void AddCardTypeToVector(int[][] vecCards, EMJCardType type)
        {
            // 万牌
            if (IsMillion(type))
            {
                vecCards[0][0] += 1;
                int index = (int)type - (int)EMJCardType.OneMillion + 1;
                vecCards[0][index] += 1;
            }
            else if (IsBucket(type))
            {
                vecCards[1][0] += 1;
                int index = (int)type - (int)EMJCardType.OneBucket + 1;
                vecCards[1][index] += 1;
            }
            else if (IsRope(type))
            {
                vecCards[2][0] += 1;
                int index = (int)type - (int)EMJCardType.OneRope + 1;
                vecCards[2][index] += 1;
            }
            else if (IsWordCard(type))
            {
                vecCards[3][0] += 1;
                int index = (int)type - (int)EMJCardType.EastWind + 1;
                vecCards[3][index] += 1;
            }
        }
        public static EMahjongCard DoorCardType(EMJCardType type)
        {
            if (type >= EMJCardType.OneMillion && type <= EMJCardType.NineMillion)
            {
                return EMahjongCard.MillionCard;
            }

            if (type >= EMJCardType.OneBucket && type <= EMJCardType.NineBucket)
            {
                return EMahjongCard.BucketCard;
            }

            if (type >= EMJCardType.OneRope && type <= EMJCardType.NineRope)
            {
                return EMahjongCard.RopeCard;
            }

            return EMahjongCard.Null;
        }

        public static EMahjongCard GetLackingDoor(int[][] vecCards)
        {
            int minCount = 100000;
            int index = 0;
            for(int i=0; i<3; i++)
            {
                if(vecCards[i][0] < minCount)
                {
                    minCount = vecCards[i][0];
                    index = i;
                }
            }

            if(index == 0)
            {
                return EMahjongCard.MillionCard;
            }

            if(index == 1)
            {
                return EMahjongCard.BucketCard;
            }

            if(index == 2)
            {
                return EMahjongCard.RopeCard;
            }

            return EMahjongCard.MillionCard;
        }

        public static EMJCardType GetVectorCardType(int i, int j)
        {
            if(i == 0)
            {
                return EMJCardType.OneMillion + (j - 1);
            }

            if(i == 1)
            {
                return EMJCardType.OneBucket + (j - 1);
            }

            if(i == 2)
            {
                return EMJCardType.OneRope + (j - 1);
            }

            if(i == 3)
            {
                return EMJCardType.EastWind + (j - 1);
            }

            return EMJCardType.Null;
        }

        public static bool IsSameTypes(List<EMJCardType> lstType1, List<EMJCardType> lstType2)
        {
            if(lstType1.Count != lstType2.Count)
            {
                return false;
            }

            for(int i=0; i<lstType1.Count; i++)
            {
                if(lstType1[i] != lstType2[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 是否是清一色
        /// </summary>
        /// <param name="vecMingAndHandCards"></param>
        /// <returns></returns>
        public static bool IsSameColor(int[][] vecMingAndHandCards)
        {
            int totalCount = 0;
            for(int i=0; i<4; i++)
            {
                totalCount += vecMingAndHandCards[i][0];
            }

            if(vecMingAndHandCards[0][0] == totalCount)
            {
                return true;
            }

            if (vecMingAndHandCards[1][0] == totalCount)
            {
                return true;
            }

            if (vecMingAndHandCards[2][0] == totalCount)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否是一条龙
        /// </summary>
        /// <returns></returns>
        public static bool IsOneDragon(int[][] vecCards)
        {
            int index = 0;
            for (int i = 0; i < 3; i++)
            {
                if (vecCards[i][0] < 9)
                {
                    if (i == 2)
                    {
                        return false;
                    }
                    continue;
                }

                for (int j = 1; j < 10; j++)
                {
                    if (vecCards[i][j] == 0)
                    {
                        return false;
                    }
                }

                for (int n = 1; n < 10; n++)
                {
                    vecCards[i][n]--;
                    vecCards[i][0]--;
                }
                index = i;
                break;
            }

            if (CommonHuCard(vecCards))
            {
                ReturnVecCard(vecCards, index);
                return true;
            }
            ReturnVecCard(vecCards, index);
            return false;
        }

        public static void ReturnVecCard(int[][] vecCard, int index)
        {
            for (int i = 1; i < 10; i++)
            {
                vecCard[index][i]++;
                vecCard[index][0]++;
            }
        }

        public static bool IsSevenPairsGolden(int[][] vecHandcards, int count)
        {
            int pairCount = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j < 10; j++)
                {
                    int num = vecHandcards[i][j];
                    if (num == 2)
                    {
                        pairCount++;
                    }
                    else if (num == 4)
                    {
                        pairCount += 2;
                    }
                }
            }
            if (pairCount + count != 7)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 是否是7小对
        /// </summary>
        /// <returns></returns>
        public static bool IsSevenPairs(int[][] vecHandcards)
        {
            int pairCount = 0;

            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j < 10; j++)
                {
                    int count = vecHandcards[i][j];
                    int temp = count % 2;
                    if (0 != temp)
                    {
                        return false;
                    }

                    if(count == 2)
                    {
                        pairCount++;
                    }

                    if(count == 4)
                    {
                        pairCount += 2;
                    }
                }
            }

            if(pairCount != 7)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 是否是豪华7小对
        /// </summary>
        /// <returns></returns>
        public static bool IsLuxurySevenPairs(int[][] vecHandcards)
        {
            bool bResult = false;
            int pairCount = 0;

            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j < 10; j++)
                {
                    int count = vecHandcards[i][j];
                    int temp = count % 2;
                    if (0 != temp)
                    {
                        return false;
                    }

                    if (count == 2)
                    {
                        pairCount++;
                    }

                    if (count == 4)
                    {
                        pairCount += 2;
                        bResult = true;
                    }
                }
            }

            if (pairCount != 7)
            {
                return false;
            }

            return bResult;
        }

        /// <summary>
        /// 是否缺一门
        /// </summary>
        /// <param name="vecCards"></param>
        /// <returns></returns>
        public static bool IsLackingDoor(int[][] vecCards, EMahjongCard doorType)
        {
            //if(doorType == EMahjongCard.MillionCard)
            //{
            //    if (vecCards[0][0] == 0)
            //    {
            //        return true;
            //    }
            //}
            //else if(doorType == EMahjongCard.BucketCard)
            //{
            //    if (vecCards[1][0] == 0)
            //    {
            //        return true;
            //    }
            //}
            //else if (doorType == EMahjongCard.RopeCard)
            //{
            //    if (vecCards[2][0] == 0)
            //    {
            //        return true;
            //    }
            //}

            if (vecCards[0][0] == 0)
            {
                return true;
            }

            if (vecCards[1][0] == 0)
            {
                return true;
            }

            if (vecCards[2][0] == 0)
            {
                return true;
            }

            return false;
        }

        public static int one(int n, int need_gui, int max_gui)
        {
            int c1 = n % 10;
            int c2 = (n % 100) / 10;

            if (c1 == 0) ++need_gui;
            else n -= 1;

            if (c2 == 0) ++need_gui;
            else n -= 10;

            if (n == 0) return need_gui;

            if (need_gui > max_gui) return need_gui;

            return next_split(n, need_gui, max_gui);
        }

        public static int two(int n, int need_gui, int max_gui)
        {
            int c1 = n % 10;
            int c2 = (n % 100) / 10;
            int c3 = (n % 1000) / 100;
            int c4 = (n % 10000) / 1000;

            bool choose_ke = true;
            if (c1 == 0)
            {
                // c1 == 0 全拆刻子
            }
            else if (c1 == 1)
            {
                // 刻子
                if (c2 == 0 || c2 == 1)
                {

                }
                else if (c2 == 2)
                {
                    if (c3 == 2)
                    {
                        if (c4 == 2) choose_ke = false;
                    }
                    else if (c3 == 3)
                    {
                        if (c4 != 2) choose_ke = false;
                    }
                    else
                    {
                        choose_ke = false;
                    }
                }
                else if (c2 == 3)
                {
                    if (c3 != 3)
                    {
                        choose_ke = false;
                    }
                }
                else if (c2 == 4)
                {
                    if (c3 == 2)
                    {
                        if (c4 == 2 || c4 == 3 || c4 == 4) choose_ke = false;
                    }
                    if (c3 == 3)
                    {
                        choose_ke = false;
                    }
                }
            }
            else if (c1 == 2)
            {
                choose_ke = false;
            }
            else if (c1 == 3)
            {
                if (c2 == 2)
                {
                    if (c3 == 1 || c3 == 4)
                    {
                        choose_ke = false;
                    }
                    else if (c3 == 2)
                    {
                        if (c4 != 2) choose_ke = false;
                    }
                }

                if (c2 == 3)
                {
                    choose_ke = false;
                }
                else if (c2 == 4)
                {
                    if (c3 == 2)
                    {
                        choose_ke = false;
                    }
                }
            }
            else if (c1 == 4)
            {
                if (c2 == 2 && c3 != 2)
                {
                    choose_ke = false;
                }
                else if (c2 == 3)
                {
                    if (c3 == 0 || c3 == 1 || c3 == 2)
                    {
                        choose_ke = false;
                    }
                }
                else if (c2 == 4)
                {
                    if (c3 == 2) choose_ke = false;
                }
            }

            if (choose_ke)
            {
                need_gui += 1;
            }
            else
            {
                if (c1 < 2)
                {
                    need_gui += (2 - c1);
                    n -= c1;
                }
                else
                {
                    n -= 2;
                }

                if (c2 < 2)
                {
                    need_gui += (2 - c2);
                    n -= c2;
                }
                else
                {
                    n -= 20;
                }
            }

            if (n == 0) return need_gui;

            if (need_gui > max_gui) return need_gui;

            return next_split(n, need_gui, max_gui);
        }

        public static int next_split(int n, int need_gui, int max_gui)
        {
            int c = 0;
            while (true)
            {
                if (n == 0) return need_gui;
                while (n > 0)
                {
                    c = n % 10;
                    n = n / 10;
                    if (c != 0) break;
                }
                if (c == 1 || c == 4)
                {
                    return one(n, need_gui, max_gui);
                }
                else if (c == 2)
                {
                    return two(n, need_gui, max_gui);
                }
            }
        }

        public static int check_normal(int[] cards, int from, int to, int max_gui, int cache_index, int[] cache)
        {
            int n = 0;
            if (cache_index >= 0)
            {
                n = cache[cache_index];
                if (n > 0) return n - 1;
            }
            n = 0;
            for (int i = from; i <= to; i++)
                n = n * 10 + cards[i];

            if (n == 0) return 0;

            bool n3 = false;
            for (int i = 0; i <= max_gui; i++)
            {
                if ((n + i) % 3 == 0)
                {
                    n3 = true;
                    break;
                }
            }

            if (!n3)
            {
                return max_gui + 1;
            }
            return next_split(n, 0, max_gui);
        }

        public static bool Foreach_eye(int[] cards, int gui_num, int max_gui, int eye_color, int[] cache)
        {
            int left_gui = gui_num;
            int cache_index = -1;
            int need_gui = -1;
            for (int i = 0; i < 3; i++)
            {
                cache_index = -1;
                if (eye_color != i)
                    cache_index = i;
                need_gui = check_normal(cards, i * 9, i * 9 + 8, max_gui, cache_index, cache);
                if (cache_index > 0)
                    cache[i] = need_gui + 1;
                left_gui -= need_gui;
                if (left_gui < 0)
                    return false;
            }

            cache_index = -1;
            if (eye_color != 3) cache_index = 3;
            need_gui = check_zi(cards, max_gui, cache_index, cache);
            if (cache_index > 0)
                cache[3] = need_gui + 1;
            return left_gui >= need_gui;
        }

        public static int check_zi(int[] cards, int max_gui, int cache_index, int[] cache)
        {
            if (cache_index >= 0)
            {
                int n = cache[cache_index];
                if (n > 0) return n - 1;
            }

            int need_gui = 0;
            for (int i = 27; i < 34; i++)
            {
                int c = cards[i];
                if (c == 0) continue;
                if (c == 1 || c == 4)
                {
                    need_gui = need_gui + 2;
                }
                else if (c == 2)
                {
                    need_gui = need_gui + 1;
                }
                if (need_gui > max_gui) return need_gui;
            }
            return need_gui;
        }

        public static bool CommonHuCardGolden(int[][] allPai, int count)
        {
            //借鉴文档地址https://github.com/yuanfengyun/qipai/commit/b0c02c3c8e708b7ac8f0bcc962ab03eb60810663
            int[] cards = new int[34];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j < 10; j++)
                {
                    if(i == 0)
                    {
                        cards[j - 1] = allPai[i][j];
                    }
                    else if(i == 1)
                    {
                        cards[8 + j] = allPai[i][j];
                    }
                    else if(i == 2)
                    {
                        cards[17 + j] = allPai[i][j];
                    }
                    else if(i == 3 && j < 8)
                    {
                        cards[26 + j] = allPai[i][j];
                    }
                }
            }
            int[] eye_tbl = new int[34];
            int eye_num = 0;
            int empty = -1;
            for (int i = 0; i < 34; i++)
            {
                // 优化手段，三不靠的牌，必做将
                int min = (i / 9) * 9;
                int max = min + 8;
                if (max == 35) max = 33;
                if (cards[i] == 1 &&
                    (i - 2 < min || cards[i - 2] == 0) &&
                    (i - 1 < min || cards[i - 1] == 0) &&
                    (i + 1 > max || cards[i + 1] == 0) &&
                    (i + 2 > max || cards[i + 2] == 0))
                {
                    if (count < 0)
                        return false;
                    eye_num = 1;
                    eye_tbl[0] = i;
                    empty = -1;
                    break;
                }
                if (empty == -1 && cards[i] == 0)
                    empty = i;
                if (cards[i] > 0 && cards[i] + count >= 2)
                    eye_tbl[eye_num++] = i;
            }
            if (empty > 0)
                eye_tbl[eye_num++] = empty;
            bool hu = false;
            int[] cache = { 0, 0, 0, 0 };
            for (int i = 0; i < eye_num; i++)
            {
                int eye = eye_tbl[i];
                if (eye == empty)
                {
                    hu = Foreach_eye(cards, count - 2, count, 1000, cache);
                }
                else
                {
                    int n = cards[eye];
                    if (n == 1)
                    {
                        cards[eye] = 0;
                        hu = Foreach_eye(cards, count - 1, count, eye / 9, cache);
                    }
                    else
                    {
                        cards[eye] -= 2;
                        hu = Foreach_eye(cards, count, count, eye / 9, cache);
                    }
                    cards[eye] = n;
                }
                if (hu)
                    break;
            }
            return hu;
        }

        /// <summary>
        /// 判断普通胡牌
        /// </summary>
        /// <param name="allPai"></param>
        /// <returns></returns>
        public static bool CommonHuCard(int[][] allPai)
        {
            int jiangPos = 0;//“将”的位置
            int yuShu = 0;//余数
            bool jiangExisted = false;

            //是否满足3，3，3，3，2模型
            for (int i = 0; i < 4; i++)
            {
                yuShu = allPai[i][0] % 3;
                if (yuShu == 1)
                {
                    return false;
                }
                if (yuShu == 2)
                {
                    if (jiangExisted)
                    {
                        return false;
                    }
                    jiangPos = i;
                    jiangExisted = true;
                }
            }

            for (int i = 0; i < 4; i++)
            {
                if (i != jiangPos)
                {
                    if (!Analyze(allPai[i], i == 3))
                    {
                        return false;
                    }
                }
            }

            //该类牌中要包含将，因为要对将进行轮询，效率较低，放在最后
            bool success = false;//指示除掉“将”后能否通过
            for (int j = 1; j < 10; j++)//对列进行操作，用j表示
            {
                if (allPai[jiangPos][j] >= 2)
                {
                    //除去这2张将牌
                    allPai[jiangPos][j] -= 2;
                    allPai[jiangPos][0] -= 2;
                    if (Analyze(allPai[jiangPos], jiangPos == 3))
                    {
                        success = true;
                    }
                    //还原这2张将牌
                    allPai[jiangPos][j] += 2;
                    allPai[jiangPos][0] += 2;
                    if (success) break;
                }
            }
            return success;
        }

        /// <summary>
        /// 分解成“刻”“顺”组合
        /// </summary>
        /// <param name="aKindPai"></param>
        /// <param name="ziPai"></param>
        /// <returns></returns>
        public static bool Analyze(int[] aKindPai, bool bWordCard)
        {
            if (aKindPai[0] == 0)
            {
                return true;
            }
            //寻找第一张牌
            int j = 0;
            for (j = 1; j < 10; j++)
            {
                if (aKindPai[j] != 0)
                {
                    break;
                }
            }
            bool result;
            if (aKindPai[j] >= 3)//作为刻牌
            {
                //除去这3张刻牌
                aKindPai[j] -= 3;
                aKindPai[0] -= 3;
                result = Analyze(aKindPai, bWordCard);
                //还原这3张刻牌
                aKindPai[j] += 3;
                aKindPai[0] += 3;
                return result;
            }
            //作为顺牌
            if ((!bWordCard) && (j < 8)
                && (aKindPai[j + 1] > 0)
                && (aKindPai[j + 2] > 0))
            {
                //除去这3张顺牌
                aKindPai[j]--;
                aKindPai[j + 1]--;
                aKindPai[j + 2]--;
                aKindPai[0] -= 3;
                result = Analyze(aKindPai, bWordCard);
                //还原这3张顺牌
                aKindPai[j]++;
                aKindPai[j + 1]++;
                aKindPai[j + 2]++;
                aKindPai[0] += 3;
                return result;
            }
            return false;
        }
    }
}
