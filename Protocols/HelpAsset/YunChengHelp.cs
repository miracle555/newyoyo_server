﻿using Protocols.Majiang;
using System;
using System.Collections.Generic;
using Protocols.MajiangYunCheng;

namespace HelpAsset
{
    namespace MahjongYunCheng
    {
        public class VecCards
        {
            public int[][] m_vecCards = new int[4][];


            public VecCards()
            {
                for (int i = 0; i < 4; i++)
                {
                    m_vecCards[i] = new int[10];
                }

                Clear();
            }

            public void Clear()
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        m_vecCards[i][j] = 0;
                    }
                }
            }

            public void FillHandCards(HashSet<UInt32> handCards)
            {
                Clear();
                foreach (var cardid in handCards)
                {
                    //获取每张牌的类型
                    EMJCardType type = MJCardHelp.GetMJType(cardid);
                    MJCardHelp.AddCardTypeToVector(m_vecCards, type);
                }
            }

            public void AddCardTypeToVector(EMJCardType type)
            {
                MJCardHelp.AddCardTypeToVector(m_vecCards, type);
            }

            public void AddCardToVector(UInt32 cardid)
            {
                EMJCardType type = MJCardHelp.GetMJType(cardid);
                MJCardHelp.AddCardTypeToVector(m_vecCards, type);
            }

            public void FillMingAndHandCards(HashSet<UInt32> handCards, List<MingCardItem> showCards)
            {
                Clear();
                foreach (var cardid in handCards)
                {
                    EMJCardType type = MJCardHelp.GetMJType(cardid);
                    MJCardHelp.AddCardTypeToVector(m_vecCards, type);
                }
                foreach (var item in showCards)
                {
                    foreach (var cardid in item.showCards)
                    {
                        MJCardHelp.AddCardTypeToVector(m_vecCards, item.cardType);
                    }
                }
            }

            public void deleteGoldenCard(VecCards handCards,EMJCardType cardType)
            {
                MJCardHelp.DeleteCardTypeToVector(handCards.m_vecCards, cardType);
            }

            public void FillMingAndHandCards(VecCards handCards, List<MingCardItem> showCards)
            {
                Clear();

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        m_vecCards[i][j] = handCards.m_vecCards[i][j];
                    }
                }

                foreach (var item in showCards)
                {
                    foreach (var cardid in item.showCards)
                    {
                        MJCardHelp.AddCardTypeToVector(m_vecCards, item.cardType);
                    }
                }
            }

            /// <summary>
            /// 是否是清一色
            /// </summary>
            /// <returns></returns>
            public bool IsSameColor
            {
                get
                {
                    return MJCardHelp.IsSameColor(m_vecCards);
                }
            }

            /// <summary>
            /// 是否是一条龙
            /// </summary>
            /// <returns></returns>
            public bool IsOneDragon
            {
                get
                {
                    return MJCardHelp.IsOneDragon(m_vecCards);
                }
            }

            /// <summary>
            /// 是否是7小对
            /// </summary>
            /// <returns></returns>
            public bool IsSevenPairs
            {
                get
                {
                    return MJCardHelp.IsSevenPairs(m_vecCards);
                }
            }

            public bool IsSevenPairsGolden(int count)
            {
                return MJCardHelp.IsSevenPairsGolden(m_vecCards, count);
            }

            public bool IsThirteenOrphans
            {
                get
                {
                    return MJCardHelp.IsThirteenOrphans(m_vecCards);
                }
            }

            /// <summary>
            /// 是否是豪华7小对
            /// </summary>
            /// <returns></returns>
            public bool IsLuxurySevenPairs
            {
                get
                {
                    return MJCardHelp.IsLuxurySevenPairs(m_vecCards);
                }
            }

            /// <summary>
            /// 是否缺一门
            /// </summary>
            /// <returns></returns>
            public bool IsLackingDoor(EMahjongCard doorType)
            {
                return MJCardHelp.IsLackingDoor(m_vecCards, doorType);
            }

            public bool CommonHuCard()
            {
                return MJCardHelp.CommonHuCard(m_vecCards);
            }

            public bool CommonHuCardGolden(int count)
            {
                return MJCardHelp.CommonHuCardGolden(m_vecCards, count);
            }

            /// <summary>
            /// 获得所有暗杠类型
            /// </summary>
            /// <returns></returns>
            public List<EMJCardType> GetAllAnGangTypes()
            {
                List<EMJCardType> lstResults = new List<EMJCardType>();

                for (int i = 0; i < 4; i++)
                {
                    for (int j = 1; j < 10; j++)
                    {
                        if (m_vecCards[i][j] == 4)
                        {
                            EMJCardType type = MJCardHelp.GetVectorCardType(i, j);
                            lstResults.Add(type);
                        }
                    }
                }

                return lstResults;
            }
        }

        /// <summary>
        /// 检测自己碰杠胡信息
        /// </summary>
        public class stCheckRoleItem
        {
            /// <summary>
            /// 手牌
            /// </summary>
            public HashSet<UInt32> handCards = new HashSet<UInt32>();

            /// <summary>
            /// 明牌
            /// </summary>
            public List<MingCardItem> showCards = new List<MingCardItem>();

            /// <summary>
            /// 是否已经报听
            /// </summary>
            public bool AlreadyBaoTing = false;

            /// <summary>
            /// 如果已经听牌，这是听口
            /// </summary>
            public List<EMJCardType> lstReadyTypes = new List<EMJCardType>();

            /// <summary>
            /// 这是手上明牌中的金牌
            /// </summary>
            public List<UInt32> goldenCards = new List<UInt32>();

            public UInt32 GetHandCardByType(EMJCardType type)
            {
                foreach (var cardid in handCards)
                {
                    if (MJCardHelp.GetMJType(cardid) == type)
                    {
                        return cardid;
                    }
                }

                return 0;
            }

            /// <summary>
            /// 添加手牌
            /// </summary>
            /// <param name="cardid"></param>
            public void AddHandCard(UInt32 cardid)
            {
                handCards.Add(cardid);
            }

            /// <summary>
            /// 获得指定类型的手牌
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public List<UInt32> GetHandCardsByType(EMJCardType type)
            {
                List<UInt32> lstResults = new List<UInt32>();

                foreach (var cardid in handCards)
                {
                    if (MJCardHelp.GetMJType(cardid) == type)
                    {
                        lstResults.Add(cardid);
                    }
                }

                return lstResults;
            }

            /// <summary>
            /// 删除手牌
            /// </summary>
            /// <param name="cardid"></param>
            public void RemoveHandCard(UInt32 cardid)
            {
                handCards.Remove(cardid);
            }

            public void AddHandCards(List<UInt32> lstCards)
            {
                foreach (var cardid in lstCards)
                {
                    AddHandCard(cardid);
                }
            }

            public void RemoveHandCards(List<UInt32> lstCards)
            {
                foreach (var cardid in lstCards)
                {
                    RemoveHandCard(cardid);
                }
            }

            List<UInt32> lstRemoveCards = new List<UInt32>();
            public List<UInt32> RemoveHandCardType(EMJCardType type)
            {
                lstRemoveCards.Clear();
                foreach (var cardid in handCards)
                {
                    if (MJCardHelp.GetMJType(cardid) == type)
                    {
                        lstRemoveCards.Add(cardid);
                    }
                }

                foreach (var cardid in lstRemoveCards)
                {
                    handCards.Remove(cardid);
                }

                return lstRemoveCards;
            }
        }

        /// <summary>
        /// 检测自己碰杠胡房间信息
        /// </summary>
        public class stCheckRoomItem
        {
            public RoomConfig config = new RoomConfig();
        }

        public class YunChengHelp
        {
            /// <summary>
            /// 是否是十三幺
            /// </summary>
            /// <param name="setCards">手牌</param>
            /// <param name="showCards">明牌</param>
            /// <param name="roomConfig">房间配置</param>
            /// <returns></returns>
            //public static bool IsThirteenOrphans(stCheckRoomItem room, stCheckRoleItem role)
            //{
            //    // 不带风不可能有十三幺
            //    if (role.showCards.Count != 0)
            //    {
            //        return false;
            //    }

            //    return MJCardHelp.IsThirteenOrphans(role.handCards);
            //}

            /// <summary>
            /// 是否可以自摸胡
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanHuSelf(uint huCardId, stCheckRoomItem room, stCheckRoleItem role)
            {
                EHuCardType huType = EHuCardType.Null;
                return CanHuSelf(huCardId, ref huType, room, role);
            }

            /// <summary>
            /// 是否可以自摸胡
            /// </summary>
            /// <param name="huType"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanHuSelf(uint huCardId, ref EHuCardType huType, stCheckRoomItem room, stCheckRoleItem role)
            {
                huType = EHuCardType.Null;

                // 没报停不能胡
                //if (!role.AlreadyBaoTing)
                //{
                    //return false;
                //}

                VecCards vecCards = new VecCards();
                vecCards.FillHandCards(role.handCards);

                bool bHu = YunChengHelp.CanHuCard(vecCards, ref huType, room, role);

                // 抠点胡牌规则,12直接就不能胡,345只能自摸,必须带6才能听
                int point = MJCardHelp.GetCardPoint(huCardId);
                //if (point <= 2)
                //{
                    //return false;
                //}

                return bHu;
            }

            /// <summary>
            /// 是否可以胡该类型的牌
            /// </summary>
            /// <param name="type"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanHuCardInner(EMJCardType type, stCheckRoomItem room, stCheckRoleItem role)
            {

                VecCards vecHandCards = new VecCards();
                
                vecHandCards.FillHandCards(role.handCards);
                vecHandCards.AddCardTypeToVector(type);
                EHuCardType settleType = EHuCardType.Null;

                return YunChengHelp.CanHuCard(vecHandCards, ref settleType, room, role);
            }

            /// <summary>
            /// 获取听牌类型
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static List<EMJCardType> GetReadyCardTypes(stCheckRoomItem room, stCheckRoleItem role)
            {
                List<EMJCardType> lstReadyTypes = new List<EMJCardType>();
                for (EMJCardType type = EMJCardType.OneRope; type <= EMJCardType.WhiteFace; type++)
                {
                    if (CanHuCardInner(type, room, role))
                    {
                        lstReadyTypes.Add(type);
                    }
                }

                // 删除所有点数小于等于2的
                //lstReadyTypes.RemoveAll(type => MJCardHelp.GetCardPoint(type) <= 2);

                foreach (var type in lstReadyTypes)
                {
                    // 必须6点或6点以上才可以听牌
                    int point = MJCardHelp.GetCardPoint(type);
                    if (point >= 6)
                    {
                        return lstReadyTypes;
                    }
                }

                lstReadyTypes.Clear();
                return lstReadyTypes;
            }

            /// <summary>
            /// 是否可以听牌
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanReadyCard(stCheckRoomItem room, stCheckRoleItem role)
            {
                List<UInt32> lstReadyCards = new List<UInt32>();
                return CanReadyCard(lstReadyCards, room, role);
            }

            /// <summary>
            /// 是否可以听牌
            /// </summary>
            /// <param name="lstReadyCards"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanReadyCard(List<UInt32> lstReadyCards, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bReady = false;

                List<UInt32> lstHandCards = new List<UInt32>();
                lstHandCards.AddRange(role.handCards);
                foreach (var cardid in lstHandCards)
                {
                    if (CanReadyCard(cardid, room, role))
                    {
                        bReady = true;
                        lstReadyCards.Add(cardid);
                    }
                }

                return bReady;
            }

            /// <summary>
            /// 听口是否改变
            /// </summary>
            /// <param name="cardid"></param>
            /// <returns></returns>
            public static bool CheckReadyChange(UInt32 cardid, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (!role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bResult = true;

                // 如果已经报听，听口不能变
                role.RemoveHandCard(cardid);

                // 听口改变
                List<EMJCardType> lstReadyTypes = GetReadyCardTypes(room, role);
                if (MJCardHelp.IsSameTypes(lstReadyTypes, role.lstReadyTypes))
                {
                    bResult = false;
                }

                role.AddHandCard(cardid);

                return bResult;
            }

            /// <summary>
            /// 听口是否改变
            /// </summary>
            /// <param name="cardid"></param>
            /// <returns></returns>
            public static bool CheckReadyChange(List<UInt32> lstCards, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (!role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bResult = true;

                // 如果已经报听，听口不能变
                role.RemoveHandCards(lstCards);

                // 听口改变
                List<EMJCardType> lstReadyTypes = GetReadyCardTypes(room, role);
                if (MJCardHelp.IsSameTypes(lstReadyTypes, role.lstReadyTypes))
                {
                    bResult = false;
                }

                role.AddHandCards(lstCards);

                return bResult;
            }

            /// <summary>
            /// 听口是否改变
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public static bool CheckReadyChange(EMJCardType type, stCheckRoomItem room, stCheckRoleItem role)
            {
                //if (!role.AlreadyBaoTing)
                //{
                ///return false;
                //}

                //bool bResult = true;

                // 如果已经报听，听口不能变
                ///List<UInt32> lstRemoveCards = role.RemoveHandCardType(type);

                // 听口改变
                //List<EMJCardType> lstReadyTypes = GetReadyCardTypes(room, role);
                //if (MJCardHelp.IsSameTypes(lstReadyTypes, role.lstReadyTypes))
                //{
                //bResult = false;
                //}

                //role.AddHandCards(lstRemoveCards);

                //return bResult;
                return false;
            }

            /// <summary>
            /// 移除该牌后听牌类型
            /// </summary>
            /// <param name="cardid"></param>
            /// <returns></returns>
            public static List<EMJCardType> GetReadyTypes(UInt32 cardid, stCheckRoomItem room, stCheckRoleItem role)
            {
                role.RemoveHandCard(cardid);

                List<EMJCardType> lstTypes = GetReadyCardTypes(room, role);

                role.AddHandCard(cardid);

                return lstTypes;
            }

            /// <summary>
            /// 是否可以听牌
            /// </summary>
            /// <param name="cardid"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanReadyCard(UInt32 cardid, stCheckRoomItem room, stCheckRoleItem role)
            {
                if (role.AlreadyBaoTing)
                {
                    return false;
                }

                bool bReady = false;

                role.RemoveHandCard(cardid);

                List<EMJCardType> lstTypes = GetReadyCardTypes(room, role);
                if (lstTypes.Count != 0)
                {
                    bReady = true;
                }

                role.AddHandCard(cardid);

                return bReady;
            }

            /// <summary>
            /// 是否可以暗杠
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <param name="outAnGangType"></param>
            /// <returns></returns>
            public static bool CanAnGang(stCheckRoomItem room, stCheckRoleItem role, ref EMJCardType outAnGangType)
            {
                outAnGangType = EMJCardType.Null;

                VecCards vecHandcards = new VecCards();
                vecHandcards.FillHandCards(role.handCards);
                List<EMJCardType> lstTypes = vecHandcards.GetAllAnGangTypes();
                if (lstTypes.Count == 0)
                {
                    return false;
                }

                if (!role.AlreadyBaoTing)
                {
                    outAnGangType = lstTypes[0];
                    return true;
                }

                // 找到1个听口未变的
                foreach (var type in lstTypes)
                {
                    if (!CheckReadyChange(type, room, role))
                    {
                        outAnGangType = type;
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// 获得所有的补杠牌
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static List<UInt32> GetAllBuGangCards(stCheckRoomItem room, stCheckRoleItem role)
            {
                List<UInt32> lstResults = new List<UInt32>();

                foreach (var item in role.showCards)
                {
                    if (item.mingType != EMingCardType.Peng)
                    {
                        continue;
                    }

                    UInt32 cardid = role.GetHandCardByType(item.cardType);
                    if (cardid != 0)
                    {
                        lstResults.Add(cardid);
                    }
                }

                return lstResults;
            }

            /// <summary>
            /// 是否可以补杠
            /// </summary>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <param name="outBuGangCardId"></param>
            /// <returns></returns>
            public static bool CanBuGang(stCheckRoomItem room, stCheckRoleItem role, ref UInt32 outBuGangCardId)
            {
                outBuGangCardId = 0;

                List<UInt32> lstCards = GetAllBuGangCards(room, role);
                if (0 == lstCards.Count)
                {
                    return false;
                }

                if (!role.AlreadyBaoTing)
                {
                    outBuGangCardId = lstCards[0];
                    return true;
                }

                // 找到1个听口未改变的
                foreach (var cardid in lstCards)
                {
                    if (!YunChengHelp.CheckReadyChange(cardid, room, role))
                    {
                        outBuGangCardId = cardid;
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// 是否可以胡牌
            /// </summary>
            /// <param name="vecHandCards"></param>
            /// <param name="outHuType"></param>
            /// <param name="room"></param>
            /// <param name="role"></param>
            /// <returns></returns>
            public static bool CanHuCard(VecCards vecHandCards, ref EHuCardType outHuType, stCheckRoomItem room, stCheckRoleItem role)
            {
                VecCards vecHandAndMingCards = new VecCards();
                vecHandAndMingCards.FillMingAndHandCards(vecHandCards, role.showCards);
                foreach(UInt32 u in role.goldenCards)
                {
                    EMJCardType cardType =  MJCardHelp.GetMJType(u);
                    vecHandAndMingCards.deleteGoldenCard(vecHandCards,cardType);
                }
                // 普通胡牌
                if (vecHandCards.CommonHuCardGolden(role.goldenCards.Count))
                {
                    outHuType = EHuCardType.CommonHu;

                    // 是否是清一色
                    if (vecHandAndMingCards.IsSameColor)
                    {
                        outHuType = EHuCardType.SameColor;
                    }
                    return true;
                }

                if (vecHandCards.IsSevenPairsGolden(role.goldenCards.Count))
                {
                    // 普通7小对
                    outHuType = EHuCardType.SenvenPairs;
                    return true;
                }
                return false;
            }

        }
    }
}
