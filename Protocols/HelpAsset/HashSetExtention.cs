﻿/* ============================================================
* Author:       liubingsheng
* Time:         2016/11/29 14:52:25
* FileName:     RedisSystem
* Purpose:      Redis
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Protocols
{
    /// <summary>
    /// HashSet扩展
    /// </summary>
    public static class HashSetExtension
    {
        static Random random = new Random();

        /// <summary>
        /// 克隆1份
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selfList"></param>
        /// <returns></returns>
        public static HashSet<T> Copy<T>(this HashSet<T> selfList)
        {
            HashSet<T> setResults = new HashSet<T>();
            foreach(var item in selfList)
            {
                setResults.Add(item);
            }
            return setResults;
        }
    }
}
