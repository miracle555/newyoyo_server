﻿/* ============================================================
* Author:       liubingsheng
* Time:         2016/11/29 14:52:25
* FileName:     RedisSystem
* Purpose:      Redis
* =============================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Protocols
{
    /// <summary>
    /// List扩展
    /// </summary>
    public static class ListExtension
    {
        static Random random = new Random();

        /// <summary>
        /// 打乱
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lstValues"></param>
        public static List<T> Random<T>(this List<T> selfList)
        {
            List<T> lstRandoms = new List<T>();

            while (selfList.Count != 0)
            {
                int index = random.Next(selfList.Count);
                T item = selfList[index];
                lstRandoms.Add(item);
                selfList.RemoveAt(index);
            }

            selfList.AddRange(lstRandoms);

            return selfList;
        }

        public static List<T> Copy<T>(this List<T> selfList)
        {
            List<T> lstResults = new List<T>();
            foreach (var item in selfList)
            {
                lstResults.Add(item);
            }
            return lstResults;
        }

        /// <summary>
        /// 删除多个元素
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lstValues"></param>
        /// <param name="collection"></param>
        public static List<T> RemoveRange<T>(this List<T> selfList, IEnumerable<T> collection)
        {
            foreach(var item in collection)
            {
                selfList.Remove(item);
            }

            return selfList;
        }

        /// <summary>
        /// 如果条目存在，移动到最后
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selfList"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static List<T> MoveToEnd<T>(this List<T> selfList, T item)
        {
            if(selfList.Contains(item))
            {
                selfList.Remove(item);
                selfList.Add(item);
            }

            return selfList;
        }

        /// <summary>
        /// 找到第一个满足条件的条目
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selfList"></param>
        /// <param name="match"></param>
        /// <returns></returns>
        public static T FindFirst<T>(this List<T> selfList, Predicate<T> match)
        {
            return selfList.Find(match);
        }
    }
}
