﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Protocols;
using ProtoBuf;
using Protocols.Landlord;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace HelpAsset
{
    namespace Landlord
    {
        public class stCardFormInfo
        {
            // 主牌最小的牌类型
            public ECardType minCardType = ECardType.Null;

            // 主牌最大的牌类型
            public ECardType maxCardType = ECardType.Null;

            // 主牌类型数量
            public int cardTypeCount = 0;
        }

        public struct stOneCard
        {
            public UInt32 cardid1;
            public ECardType type;

            public stOneCard(ECardType type, UInt32 cardid1)
            {
                this.cardid1 = cardid1;
                this.type = type;
            }
        }

        public struct stTwoCard
        {
            public UInt32 cardid1;
            public UInt32 cardid2;
            public ECardType type;

            public stTwoCard(ECardType type, UInt32 cardid1, UInt32 cardid2)
            {
                this.cardid1 = cardid1;
                this.cardid2 = cardid2;
                this.type = type;
            }
        }

        public struct stThreeCard
        {
            public UInt32 cardid1;
            public UInt32 cardid2;
            public UInt32 cardid3;
            public ECardType type;

            public stThreeCard(ECardType type, UInt32 cardid1, UInt32 cardid2, UInt32 cardid3)
            {
                this.cardid1 = cardid1;
                this.cardid2 = cardid2;
                this.cardid3 = cardid3;
                this.type = type;
            }
        }

        public struct stFourCard
        {
            public UInt32 cardid1;
            public UInt32 cardid2;
            public UInt32 cardid3;
            public UInt32 cardid4;
            public ECardType type;
            public stFourCard(ECardType type, UInt32 cardid1, UInt32 cardid2, UInt32 cardid3, UInt32 cardid4)
            {
                this.cardid1 = cardid1;
                this.cardid2 = cardid2;
                this.cardid3 = cardid3;
                this.cardid4 = cardid4;
                this.type = type;
            }
        }

        public struct stKingBomb
        {
            public UInt32 cardid1;
            public UInt32 cardid2;
            public stKingBomb(UInt32 cardid1, UInt32 cardid2)
            {
                this.cardid1 = cardid1;
                this.cardid2 = cardid2;
            }
        }

        public class stCardItem
        {
            public ECardType type = ECardType.Null;

            public List<UInt32> lstCards = new List<UInt32>();

            public void AddCard(UInt32 cardid)
            {
                lstCards.Add(cardid);
            }
        }

        public class LordHelp
        {
            /// <summary>
            /// 获得类型列表
            /// </summary>
            /// <param name="lstCards"></param>
            /// <returns></returns>
            static List<ECardType> GetCardTypes(List<UInt32> lstCards)
            {
                List<ECardType> lstResults = new List<ECardType>();
                foreach (var cardid in lstCards)
                {
                    ECardType type = GetCardType(cardid);
                    lstResults.Add(type);
                }
                return lstResults;
            }

            /// <summary>
            /// 获取牌类型
            /// </summary>
            /// <param name="cardid"></param>
            /// <returns></returns>
            public static ECardType GetCardType(UInt32 cardid)
            {
                ECardId card = (ECardId)cardid;

                // 类型
                if (card >= ECardId.Black3 && card <= ECardId.Cube2)
                {
                    UInt32 temp = cardid - (UInt32)ECardId.Black3;
                    temp = temp / 4 + (UInt32)ECardType.Three;
                    return (ECardType)temp;
                }

                if (ECardId.SmallJoker == card)
                {
                    return ECardType.SmallJoker;
                }

                if (ECardId.BigJoker == card)
                {
                    return ECardType.BigJoker;
                }

                //如果是癞子
                if (ECardId.Laizigou == card)
                {
                    return ECardType.Laizigou;
                }

                return ECardType.Null;
            }

            /// <summary>
            /// 是否是癞子
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsLaizigou(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 1)
                {
                    return false;
                }

                if (lstTypes[0] != ECardType.Laizigou)
                {
                    return false;
                }
                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[0];
                info.cardTypeCount = 1;
                return true;
            }

            /// <summary>
            /// 是否是单牌
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsSingle(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 1)
                {
                    return false;
                }
                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[0];
                info.cardTypeCount = 1;

                return true;
            }

            static bool IsThreeBomb(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 2)
                {
                    return false;
                }
                if (lstTypes[0] != ECardType.Three)
                {
                    return false;
                }
                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[0];
                info.cardTypeCount = 1;
                return lstTypes[1] == lstTypes[0];
            }

            static bool IsPair(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 2)
                {
                    return false;

                }

                if (lstTypes[0] == ECardType.Three)
                {
                    return false;
                }

                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[0];
                info.cardTypeCount = 1;

                return lstTypes[1] == lstTypes[0];
            }

            /// <summary>
            /// 是否是王炸
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsKingBomb(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 2)
                {
                    return false;
                }

                lstTypes.Sort();

                ECardType type1 = lstTypes[0];
                ECardType type2 = lstTypes[1];

                if (type1 != ECardType.SmallJoker)
                {
                    return false;
                }

                if (type2 != ECardType.BigJoker)
                {
                    return false;
                }

                info.minCardType = type1;
                info.maxCardType = type2;
                info.cardTypeCount = 2;

                return true;
            }

            /// <summary>
            /// 3不带
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsThree(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 3)
                {
                    return false;
                }
                lstTypes.Sort();

                if (lstTypes[0] != lstTypes[1])
                {
                    return false;
                }

                if (lstTypes[1] != lstTypes[2])
                {
                    return false;
                }

                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[0];
                info.cardTypeCount = 1;

                return true;
            }


            /// <summary> 
            /// 3带2 如33344 33444  
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsThreeTwo(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 5)
                {
                    return false;
                }
                lstTypes.Sort();
                Dictionary<ECardType, int> mapTypes = new Dictionary<ECardType, int>();
                foreach (var type in lstTypes)
                {
                    if (mapTypes.ContainsKey(type))
                    {
                        mapTypes[type] += 1;
                    }
                    else
                    {
                        mapTypes.Add(type, 1);
                    }
                }

                ECardType typeThree = ECardType.Null;
                ECardType typeTwo = ECardType.Null;
                foreach (var item in mapTypes)
                {
                    if (item.Value == 3)
                    {
                        typeThree = item.Key;
                    }

                    if (item.Value == 2)
                    {
                        typeTwo = item.Key;
                    }
                }

                if (typeThree == ECardType.Null)
                {
                    return false;
                }
                if (typeTwo == ECardType.Null || typeTwo == ECardType.Three)
                {
                    return false;
                }
                List<ECardType> list = new List<ECardType>();
                list.Add(typeThree);
                list.Add(typeThree);
                list.Add(typeThree);

                bool bResult = IsThree(list, info);
                return bResult;
            }


            /// <summary>
            /// 3带1
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsThreeOne(List<ECardType> lstTypes, stCardFormInfo info)
            {
                //不能是炸弹
                if (IsBomb(lstTypes, info))
                {
                    return false;
                }

                if (lstTypes.Count != 4)
                {
                    return false;
                }

                lstTypes.Sort();

                ECardType type1 = lstTypes[0];
                ECardType type2 = lstTypes[1];
                ECardType type = ECardType.Null;


                if (type1 != type2)
                {
                    type = type1;
                }
                else
                {

                    type = lstTypes[3];

                }

                lstTypes.Remove(type);
                bool bResult = IsThree(lstTypes, info);
                lstTypes.Add(type);
                return bResult;

            }

            /// <summary>
            /// 是否是炸弹
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsBomb(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 4)
                {
                    return false;
                }
                lstTypes.Sort();

                if (lstTypes[0] != lstTypes[1])
                {
                    return false;
                }

                if (lstTypes[1] != lstTypes[2])
                {
                    return false;
                }

                if (lstTypes[2] != lstTypes[3])
                {
                    return false;
                }

                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[1];
                info.cardTypeCount = 1;

                return true;
            }

            /// <summary>
            /// 是否是顺子
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsStraight(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count < 5)
                {
                    return false;
                }

                lstTypes.Sort();

                if (lstTypes[lstTypes.Count - 1] >= ECardType.Two)
                {
                    return false;
                }

                for (int i = 0; i < lstTypes.Count - 1; i++)
                {
                    ECardType type1 = lstTypes[i];
                    ECardType type2 = lstTypes[i + 1];

                    if ((type1 + 1) != type2)
                    {

                        return false;

                    }
                }

                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[lstTypes.Count - 1];
                info.cardTypeCount = lstTypes.Count;

                return true;
            }

            /// <summary>
            /// 是否是连对
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsStraightPair(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count < 6)
                {
                    return false;
                }

                if (lstTypes.Count % 2 != 0)
                {
                    return false;
                }

                lstTypes.Sort();

                if(lstTypes[0] == ECardType.Three)
                {
                    return false;
                }

                // 大王，小王，2不能在连对中
                if (lstTypes[lstTypes.Count - 1] >= ECardType.Two)
                {
                    return false;
                }

                for (int i = 0; i < (lstTypes.Count - 1); i++)
                {
                    if (i % 2 == 0)
                    {
                        if (lstTypes[i] != lstTypes[i + 1])
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if ((lstTypes[i] + 1) != lstTypes[i + 1])
                        {
                            return false;
                        }
                    }
                }

                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[lstTypes.Count - 1];
                info.cardTypeCount = lstTypes.Count / 2;

                return true;
            }

            /// <summary>
            /// 是否包含炸弹
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <returns></returns>
            static bool IsContainsBomb(List<ECardType> lstTypes)
            {
                if (lstTypes.Count < 4)
                {
                    return false;
                }

                Dictionary<ECardType, int> mapTypes = new Dictionary<ECardType, int>();
                foreach (var type in lstTypes)
                {
                    if (mapTypes.ContainsKey(type))
                    {
                        mapTypes[type] += 1;
                    }
                    else
                    {
                        mapTypes.Add(type, 1);
                    }
                }

                foreach (var item in mapTypes)
                {
                    if (item.Value >= 4)
                    {
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// 是否是飞机
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsAircraft(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count < 6)
                {
                    return false;
                }

                if (lstTypes.Count % 3 != 0)
                {
                    return false;
                }

                // 包含炸弹不行
                if (IsContainsBomb(lstTypes))
                {
                    return false;
                }

                lstTypes.Sort();

                // 大王，小王，2不能在飞机中
                if (lstTypes[lstTypes.Count - 1] >= ECardType.Two)
                {
                    return false;
                }

                HashSet<ECardType> setTypes = new HashSet<ECardType>(lstTypes);
                if (setTypes.Count != (lstTypes.Count / 3))
                {
                    return false;
                }

                for (int i = 0; i < setTypes.Count; i++)
                {
                    if ((lstTypes[i] + 1) != lstTypes[i + 3])
                    {
                        return false;
                    }
                }

                info.minCardType = lstTypes[0];
                info.maxCardType = lstTypes[lstTypes.Count - 1];
                info.cardTypeCount = lstTypes.Count / 3;

                return true;
            }

            /// <summary>
            /// 是否飞机带对子
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsAircraftPair(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count < 10)
                {
                    return false;
                }

                if (lstTypes.Count % 5 != 0)
                {
                    return false;
                }

                // 包含炸弹不行
                if (IsContainsBomb(lstTypes))
                {
                    return false;
                }

                lstTypes.Sort();

                Dictionary<ECardType, int> mapTypes = new Dictionary<ECardType, int>();
                foreach (var type in lstTypes)
                {
                    if(type == ECardType.Three)
                    {
                        return false;
                    }
                    if (mapTypes.ContainsKey(type))
                    {
                        mapTypes[type] += 1;
                    }
                    else
                    {
                        mapTypes.Add(type, 1);
                    }
                }

                // 对子
                List<ECardType> lstTwoTypes = new List<ECardType>();
                // 三牌
                List<ECardType> lstThreeTypes = new List<ECardType>();

                foreach (var item in mapTypes)
                {
                    if (item.Value == 2)
                    {
                        for (int i = 0; i < item.Value; i++)
                        {
                            lstTwoTypes.Add(item.Key);
                        }
                    }
                    else if (item.Value == 3)
                    {
                        for (int i = 0; i < item.Value; i++)
                        {
                            lstThreeTypes.Add(item.Key);
                        }
                    }
                }

                // 单牌数量和三牌数量不一致
                int threeCount = lstThreeTypes.Count / 3;
                if (lstTwoTypes.Count/2 != threeCount)
                {
                    return false;
                }

                return IsAircraft(lstThreeTypes, info);
            }

            /// <summary>
            /// 是否是飞机带翅膀
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsAircraftWing(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count < 8)
                {
                    return false;
                }

                if (lstTypes.Count % 4 != 0)
                {
                    return false;
                }

                // 包含炸弹不行
                if (IsContainsBomb(lstTypes))
                {
                    return false;
                }

                lstTypes.Sort();

                Dictionary<ECardType, int> mapTypes = new Dictionary<ECardType, int>();
                foreach (var type in lstTypes)
                {
                    if (mapTypes.ContainsKey(type))
                    {
                        mapTypes[type] += 1;
                    }
                    else
                    {
                        mapTypes.Add(type, 1);
                    }
                }

                // 单牌
                List<ECardType> lstSingleTypes = new List<ECardType>();
                // 三牌
                List<ECardType> lstThreeTypes = new List<ECardType>();

                foreach (var item in mapTypes)
                {
                    if (item.Value <= 2)
                    {
                        for (int i = 0; i < item.Value; i++)
                        {
                            lstSingleTypes.Add(item.Key);
                        }
                    }
                    else if(item.Value == 3)
                    {
                        for (int i = 0; i < item.Value; i++)
                        {
                            lstThreeTypes.Add(item.Key);
                        }
                    }
                }

                // 单牌数量和三牌数量不一致
                int threeCount = lstThreeTypes.Count / 3;
                if (lstSingleTypes.Count != threeCount)
                {
                    return false;
                }

                return IsAircraft(lstThreeTypes, info);
            }

            /// <summary>
            /// 4带2 
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsFourTwo(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 6)
                {
                    return false;
                }
                lstTypes.Sort();
                Dictionary<ECardType, int> mapTypes = new Dictionary<ECardType, int>();
                foreach (var type in lstTypes)
                {
                    if (mapTypes.ContainsKey(type))
                    {
                        mapTypes[type] += 1;
                    }
                    else
                    {
                        mapTypes.Add(type, 1);
                    }
                }

                ECardType typeFour = ECardType.Null;

                foreach (var item in mapTypes)
                {
                    if (item.Value == 4)
                    {
                        typeFour = item.Key;
                    }
                }

                if (typeFour == ECardType.Null)
                {
                    return false;
                }

                info.minCardType = typeFour;
                info.maxCardType = typeFour;
                info.cardTypeCount = 1;

                return true;
            }

            /// <summary>
            /// 四带两个对
            /// </summary>
            /// <param name="lstTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool IsFourFour(List<ECardType> lstTypes, stCardFormInfo info)
            {
                if (lstTypes.Count != 8)
                {
                    return false;
                }

                lstTypes.Sort();
                Dictionary<ECardType, int> mapTypes = new Dictionary<ECardType, int>();
                foreach (var type in lstTypes)
                {
                    if(type == ECardType.Three)
                    {
                        return false;
                    }
                    if (mapTypes.ContainsKey(type))
                    {
                        mapTypes[type] += 1;
                    }
                    else
                    {
                        mapTypes.Add(type, 1);
                    }
                }

                ECardType typeFour = ECardType.Null;
                ECardType typeTwo1 = ECardType.Null;
                ECardType typeTwo2 = ECardType.Null;
                foreach (var item in mapTypes)
                {
                    if (item.Value == 4)
                    {
                        typeFour = item.Key;
                    }

                    if (item.Value == 2)
                    {
                        if (typeTwo1 == ECardType.Null)
                        {
                            typeTwo1 = item.Key;
                        }
                        else
                        {
                            typeTwo2 = item.Key;
                        }
                    }
                }
                if (typeFour == ECardType.Null)
                {
                    return false;
                }
                if (typeTwo1 == ECardType.Null)
                {
                    return false;
                }
                if (typeTwo2 == ECardType.Null)
                {
                    return false;
                }
                return true;
            }

            /// <summary>
            /// 获得牌形
            /// </summary>
            /// <param name="lstCards"></param>
            /// <returns></returns>
            public static ECardFormType GetCardFormType(List<UInt32> lstCards, UInt32 laiziType)
            {
                stCardFormInfo info = new stCardFormInfo();
                return GetCardFormType(lstCards, info, laiziType);
            }

            /// <summary>
            /// 获得牌形
            /// </summary>
            /// <param name="lstCards"></param>
            /// <returns></returns>
            public static ECardFormType GetCardFormType(List<UInt32> lstCards, stCardFormInfo info, UInt32 laiziType)
            {
                /*
                Single = 1, // 单牌  
                Pair = 2, // 对子  
                Three = 3, // 3不带  
                ThreeOne = 4, // 3带1  
                FourTwo = 5, // 4带2  
                Straight = 6, // 顺子
                StraightPair = 7, // --连对
                Aircraft = 8, // --飞机不带  
                AircraftWing = 9, // --飞机带单牌
                Bomb = 10, // --炸弹  
                KingBomb = 11, // --王炸
                ThreeBomb = 12,//3炸
                LaizigouBomb = 13,//癞子炸弹
                ThreeTwo = 14,//三带二
                Laizigou = 15,//癞子
                FourFour = 16,//四带两个对
                AircraftPair = 17,
                */
                ECardType type = GetCardType(laiziType);
                if (type >= ECardType.Two ||
                    type == ECardType.Three)
                {
                    //错误牌型
                    return ECardFormType.Null;
                }

                List<UInt32> tempcards = new List<UInt32>();
                if (type != ECardType.Null)
                {
                    //利用二进制流的序列化与反序列化实现list的深克隆
                    BinaryFormatter inputFormatter = new BinaryFormatter();
                    MemoryStream inputStream;
                    using (inputStream = new MemoryStream())
                    {
                        inputFormatter.Serialize(inputStream, lstCards);
                    }
                    //将二进制流反序列化为对象  
                    using (MemoryStream outputStream = new MemoryStream(inputStream.ToArray()))
                    {
                        BinaryFormatter outputFormatter = new BinaryFormatter();
                        tempcards = (List<UInt32>)outputFormatter.Deserialize(outputStream);
                    }
                }
                else
                {
                    tempcards.AddRange(lstCards);
                }
                tempcards.Sort();

                List<ECardType> lstTypes = GetCardTypes(tempcards);
                if (type != ECardType.Null)
                {
                    //去掉癞子牌 加入癞子替代的牌
                    lstTypes.Add(type);
                    lstTypes.Remove(ECardType.Laizigou);
                    lstTypes.Sort();
                }

                //癞子
                if (IsLaizigou(lstTypes, info))
                {
                    return ECardFormType.Laizigou;
                }

                //单牌
                if (IsSingle(lstTypes, info))
                {
                    return ECardFormType.Single;
                }

                //对子
                if (IsPair(lstTypes, info))
                {
                    return ECardFormType.Pair;
                }

                //3炸
                if (IsThreeBomb(lstTypes, info))
                {
                    return ECardFormType.ThreeBomb;
                }

                //三不带
                if (IsThree(lstTypes, info))
                {
                    return ECardFormType.Three;
                }

                //三带一
                if (IsThreeOne(lstTypes, info))
                {
                    return ECardFormType.ThreeOne;
                }

                //三带二
                if (IsThreeTwo(lstTypes, info))
                {
                    return ECardFormType.ThreeTwo;
                }

                //四带二
                if (IsFourTwo(lstTypes, info))
                {
                    return ECardFormType.FourTwo;
                }

                //四带两个对
                if (IsFourFour(lstTypes, info))
                {
                    return ECardFormType.FourFour;
                }

                //顺子
                if (IsStraight(lstTypes, info))
                {
                    return ECardFormType.Straight;
                }

                //连对
                if (IsStraightPair(lstTypes, info))
                {
                    return ECardFormType.StraightPair;
                }

                //飞机
                if (IsAircraft(lstTypes, info))
                {
                    return ECardFormType.Aircraft;
                }

                //飞机带翅膀
                if (IsAircraftWing(lstTypes, info))
                {
                    return ECardFormType.AircraftWing;
                }

                //飞机带对子
                if (IsAircraftPair(lstTypes, info))
                {
                    return ECardFormType.AircraftPair;
                }

                //炸弹
                if (IsBomb(lstTypes, info))
                {
                    if (type != ECardType.Null)
                    {
                        return ECardFormType.LaizigouBomb;
                    }
                    return ECardFormType.Bomb;
                }

                //王炸
                if (IsKingBomb(lstTypes, info))
                {
                    return ECardFormType.KingBomb;
                }

                return ECardFormType.Null;
            }

            /// <summary>
            /// 癞子可以顶替的牌有哪些，返回false表示癞子没有可以替代的牌 不是一个正常牌型
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool CanReplaceCards(List<UInt32> selfHandCards, out List<UInt32> outCards)
            {
                outCards = new List<UInt32>();

                List<UInt32> handCards = new List<UInt32>();

                //利用二进制流的序列化与反序列化实现list的深克隆
                BinaryFormatter inputFormatter = new BinaryFormatter();
                MemoryStream inputStream;
                using (inputStream = new MemoryStream())
                {
                    inputFormatter.Serialize(inputStream, selfHandCards);
                }
                //将二进制流反序列化为对象  
                using (MemoryStream outputStream = new MemoryStream(inputStream.ToArray()))
                {
                    BinaryFormatter outputFormatter = new BinaryFormatter();
                    handCards = (List<UInt32>)outputFormatter.Deserialize(outputStream);
                }

                for (UInt32 i = 5; i <= 48; i++)
                {
                    handCards.Add(i);
                    //判断是否是牌类型
                    stCardFormInfo info = new stCardFormInfo();
                    ECardFormType otherForm = GetCardFormType(handCards, info, 0);
                    //如果是
                    if (otherForm == ECardFormType.Single
                        || otherForm == ECardFormType.Laizigou)
                    {
                        handCards.Remove(i);
                        return false;
                    }
                    if (otherForm == ECardFormType.Null)
                    {
                        handCards.Remove(i);
                        continue;
                    }
                    outCards.Add(i);

                    handCards.Remove(i);
                }

                return true;
            }


            public static uint GetDefaultCard(ECardType type)
            {
                switch (type)
                {
                    case ECardType.Null:
                        return 0;
                    case ECardType.Three:
                        return 1;
                    case ECardType.Four:
                        return 5;
                    case ECardType.Five:
                        return 9;
                    case ECardType.Six:
                        return 13;
                    case ECardType.Seven:
                        return 17;
                    case ECardType.Eight:
                        return 21;
                    case ECardType.Nine:
                        return 25;
                    case ECardType.Ten:
                        return 29;
                    case ECardType.J:
                        return 33;
                    case ECardType.Q:
                        return 37;
                    case ECardType.K:
                        return 41;
                    case ECardType.A:
                        return 45;
                    case ECardType.Two:
                        return 49;
                    case ECardType.SmallJoker:
                        return 53;
                    case ECardType.BigJoker:
                        return 54;
                    case ECardType.Laizigou:
                        return 55;
                    default:
                        return 0;
                } 
            }



            /// <summary>
            /// 自己手牌中是否有大于其他人出的牌
            /// </summary>
            /// <param name="selfHandCards">自己手牌</param>
            /// <param name="otherOutCards">其他人出的牌</param>
            /// <param name="outCards">要出的牌</param>
            /// <returns></returns>
            public static bool HasGreaterCards(List<UInt32> selfHandCards, List<UInt32> otherOutCards, UInt32 laiziType, out List<UInt32> outCards)
            {
                outCards = new List<UInt32>();

                List<UInt32> handCards = new List<UInt32>();

                //利用二进制流的序列化与反序列化实现list的深克隆
                BinaryFormatter inputFormatter = new BinaryFormatter();
                MemoryStream inputStream;
                using (inputStream = new MemoryStream())
                {
                    inputFormatter.Serialize(inputStream, selfHandCards);
                }
                //将二进制流反序列化为对象  
                using (MemoryStream outputStream = new MemoryStream(inputStream.ToArray()))
                {
                    BinaryFormatter outputFormatter = new BinaryFormatter();
                    handCards = (List<UInt32>)outputFormatter.Deserialize(outputStream);
                }

                stCardFormInfo otherInfo = new stCardFormInfo();
                ECardFormType otherForm = GetCardFormType(otherOutCards, otherInfo, laiziType);
                // 不是牌形
                if (otherForm == ECardFormType.Null)
                {
                    return false;
                }

                // 单牌提示
                if (otherForm == ECardFormType.Single)
                {
                    if (SingleTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 对子提示
                if (otherForm == ECardFormType.Pair)
                {
                    if (PairTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 3不带
                if (otherForm == ECardFormType.Three)
                {
                    if (ThreeTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 3带1提示
                if (otherForm == ECardFormType.ThreeOne)
                {
                    if (ThreeOneTip(handCards, otherInfo, outCards))
                        return true;
                }

                //4带两对
                if (otherForm == ECardFormType.FourFour)
                {
                    if (FourFourTip(handCards, otherInfo, outCards))
                        return true;
                }

                //3带2
                if (otherForm == ECardFormType.ThreeTwo)
                {
                    if (ThreeTwoTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 4带2提示
                if (otherForm == ECardFormType.FourTwo)
                {
                    if (FourTwoTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 顺子提示
                if (otherForm == ECardFormType.Straight)
                {
                    if (StraightTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 连对提示
                if (otherForm == ECardFormType.StraightPair)
                {
                    if (StraightPairTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 飞机不带提示
                if (otherForm == ECardFormType.Aircraft)
                {
                    if (AircraftTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 飞机带单牌提示
                if (otherForm == ECardFormType.AircraftWing)
                {
                    if (AircraftWingTip(handCards, otherInfo, outCards))
                        return true;
                }

                //飞机带对子提示
                if(otherForm == ECardFormType.AircraftPair)
                {
                    if (AircraftPairTip(handCards, otherInfo, outCards))
                        return true;
                }

                // 炸弹提示
                if (otherForm == ECardFormType.Bomb)
                {
                    if (BombTip(handCards, otherInfo, outCards))
                        return true;
                }

                //三炸提示
                if (otherForm == ECardFormType.ThreeBomb)
                {
                    if (ThreeBombTip(handCards, otherInfo, outCards))
                    {
                        return true;
                    }
                }

                //癞子炸弹提示
                if (otherForm == ECardFormType.LaizigouBomb)
                {
                    if (LaizigouBombTip(handCards, otherInfo, outCards))
                        return true;
                }

                bool hasLaizi = false;
                foreach (UInt32 card in handCards)
                {
                    if (card == 55)
                    {
                        hasLaizi = true;
                        handCards.Remove(card);
                    }
                }
                if (hasLaizi)
                {
                    for (UInt32 i = 5; i <= 48; i++)
                    {
                        // 单牌提示
                        if (otherForm == ECardFormType.Single)
                        {
                            handCards.Add(i);
                            if (SingleTip2(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 对子提示
                        if (otherForm == ECardFormType.Pair)
                        {
                            handCards.Add(i);
                            if (PairTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 3不带
                        if (otherForm == ECardFormType.Three)
                        {
                            handCards.Add(i);
                            if (ThreeTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 3带1提示
                        if (otherForm == ECardFormType.ThreeOne)
                        {
                            handCards.Add(i);
                            if (ThreeOneTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 3带1提示
                        if (otherForm == ECardFormType.ThreeOne)
                        {
                            handCards.Add(i);
                            if (ThreeOneTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        //4带两对
                        if (otherForm == ECardFormType.FourFour)
                        {
                            handCards.Add(i);
                            if (FourFourTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 4带2提示
                        if (otherForm == ECardFormType.FourTwo)
                        {
                            handCards.Add(i);
                            if (FourTwoTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 顺子提示
                        if (otherForm == ECardFormType.Straight)
                        {
                            handCards.Add(i);
                            if (StraightTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 连对提示
                        if (otherForm == ECardFormType.StraightPair)
                        {
                            handCards.Add(i);
                            if (StraightPairTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 飞机不带提示
                        if (otherForm == ECardFormType.Aircraft)
                        {
                            handCards.Add(i);
                            if (AircraftTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        // 飞机带单牌提示
                        if (otherForm == ECardFormType.AircraftWing)
                        {
                            handCards.Add(i);
                            if (AircraftWingTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        //飞机带对子提示
                        if (otherForm == ECardFormType.AircraftPair)
                        {
                            handCards.Add(i);
                            if (AircraftPairTip(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }

                        //癞子炸弹提示
                        if (otherForm == ECardFormType.LaizigouBomb)
                        {
                            handCards.Add(i);
                            if (LaizigouBombTip2(handCards, otherInfo, outCards))
                            {
                                handCards.Remove(i);
                                outCards.Remove(i);
                                outCards.Add(55);
                                return true;
                            }
                            handCards.Remove(i);
                        }
                    }
                }
                if (hasLaizi)
                {
                    handCards.Add(55);
                }
                return false;
            }

            /// <summary>
            /// 单牌提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            static bool SingleTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                selfHandCards.Sort();

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 单牌
                foreach (var item in lstOneCards)
                {
                    if (item.type > otherInfo.maxCardType)
                    {
                        outCards.Add(item.cardid1);
                        return true;
                    }
                }

                // 两牌
                foreach (var item in lstTwoCards)
                {
                    if (item.type > otherInfo.maxCardType && item.type != ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        return true;
                    }
                }

                // 3牌
                foreach (var item in lstThreeCards)
                {
                    if (item.type > otherInfo.maxCardType)
                    {
                        outCards.Add(item.cardid1);
                        return true;
                    }
                }

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 单牌提示带癞子
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            static bool SingleTip2(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                selfHandCards.Sort();

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 对子提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool PairTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                if (selfHandCards.Count < 2)
                {
                    return false;
                }

                selfHandCards.Sort();

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 两牌
                foreach (var item in lstTwoCards)
                {
                    if (item.type > otherInfo.maxCardType && item.type != ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 3牌
                foreach (var item in lstThreeCards)
                {
                    if (item.type > otherInfo.maxCardType)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }
                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }
                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 3不带提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool ThreeTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                selfHandCards.Sort();

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 3牌
                foreach (var item in lstThreeCards)
                {
                    if (item.type > otherInfo.maxCardType)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        outCards.Add(item.cardid3);
                        return true;
                    }
                }

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 三带一对提示 也就是三带二
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool ThreeTwoTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 3牌中有没有
                foreach (var item in lstThreeCards)
                {
                    if (item.type >= otherInfo.minCardType)
                    {
                        foreach (var item2 in lstTwoCards)
                        {
                            outCards.Add(item.cardid1);
                            outCards.Add(item.cardid2);
                            outCards.Add(item.cardid3);
                            outCards.Add(item2.cardid1);
                            outCards.Add(item2.cardid2);
                            return true;
                        }
                    }
                }

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 4牌炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);

                    return true;
                }

                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 3带1提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool ThreeOneTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 3牌中有没有
                foreach (var item in lstThreeCards)
                {
                    if (item.type >= otherInfo.minCardType)
                    {
                        UInt32 outCardId;
                        if (IsOneCardLeft(selfHandCards, item, out outCardId))
                        {
                            outCards.Add(item.cardid1);
                            outCards.Add(item.cardid2);
                            outCards.Add(item.cardid3);
                            outCards.Add(outCardId);

                            return true;
                        }
                    }
                }

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 4牌炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);

                    return true;
                }

                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 四带两对提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool FourFourTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 4牌炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);

                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 4带2提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool FourTwoTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 4牌炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);

                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 顺子提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool StraightTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                Dictionary<ECardType, stCardItem> mapTypes = GetCardItems(handcards);

                List<stCardFormInfo> lstBigInfos = new List<stCardFormInfo>();
                CheckStraightBig(otherInfo, lstBigInfos);

                foreach (var item in lstBigInfos)
                {
                    if (CheckStraightItem(mapTypes, item, outCards))
                    {
                        return true;
                    }
                }

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 连对提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool StraightPairTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                Dictionary<ECardType, stCardItem> mapTypes = GetCardItems(handcards);

                List<stCardFormInfo> lstBigInfos = new List<stCardFormInfo>();
                CheckStraightBig(otherInfo, lstBigInfos);

                foreach (var item in lstBigInfos)
                {
                    if (CheckStraightPairItem(mapTypes, item, outCards))
                    {
                        return true;
                    }
                }

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 飞机不带提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool AircraftTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                Dictionary<ECardType, stCardItem> mapTypes = GetCardItems(handcards);

                List<stCardFormInfo> lstBigInfos = new List<stCardFormInfo>();
                CheckStraightBig(otherInfo, lstBigInfos);

                foreach (var item in lstBigInfos)
                {
                    if (CheckAircraftItem(mapTypes, item, outCards))
                    {
                        return true;
                    }
                }

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 飞机带对子提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool AircraftPairTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                Dictionary<ECardType, stCardItem> mapTypes = GetCardItems(handcards);
                List<stCardFormInfo> lstBigInfos = new List<stCardFormInfo>();
                CheckStraightBig(otherInfo, lstBigInfos);
                foreach (var item in lstBigInfos)
                {
                    if (CheckAircraftPairItem(handcards, mapTypes, item, outCards))
                    {
                        return true;
                    }
                }

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 飞机带单牌提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool AircraftWingTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                Dictionary<ECardType, stCardItem> mapTypes = GetCardItems(handcards);

                List<stCardFormInfo> lstBigInfos = new List<stCardFormInfo>();
                CheckStraightBig(otherInfo, lstBigInfos);

                foreach (var item in lstBigInfos)
                {
                    if (CheckAircraftWingItem(handcards, mapTypes, item, outCards))
                    {
                        return true;
                    }
                }

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 癞子炸弹提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool LaizigouBombTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                //3炸弹
                foreach (var item in lstTwoCards)
                {
                    if (item.type == ECardType.Three)
                    {
                        outCards.Add(item.cardid1);
                        outCards.Add(item.cardid2);
                        return true;
                    }
                }

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    //任何炸弹大于癞子炸弹
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 癞子提示癞子炸弹
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool LaizigouBombTip2(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    if (item.type <= otherInfo.minCardType)
                    {
                        continue;
                    }
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 三炸提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool ThreeBombTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    //任何炸弹大于三炸
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 炸弹提示
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="otherInfo"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            public static bool BombTip(List<UInt32> selfHandCards, stCardFormInfo otherInfo, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                // 检测炸弹
                List<stOneCard> lstOneCards = new List<stOneCard>();
                List<stTwoCard> lstTwoCards = new List<stTwoCard>();
                List<stThreeCard> lstThreeCards = new List<stThreeCard>();
                List<stFourCard> lstFourCards = new List<stFourCard>();
                List<stKingBomb> lstKingBombs = new List<stKingBomb>();

                GetAllKindCards(selfHandCards, lstOneCards, lstTwoCards, lstThreeCards, lstFourCards, lstKingBombs);

                // 炸弹
                foreach (var item in lstFourCards)
                {
                    if (item.type <= otherInfo.minCardType)
                    {
                        continue;
                    }

                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    outCards.Add(item.cardid3);
                    outCards.Add(item.cardid4);
                    return true;
                }

                // 王炸
                foreach (var item in lstKingBombs)
                {
                    outCards.Add(item.cardid1);
                    outCards.Add(item.cardid2);
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 检测是否有该顺子牌组
            /// </summary>
            /// <param name="mapTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool CheckStraightItem(Dictionary<ECardType, stCardItem> mapTypes, stCardFormInfo info, List<UInt32> outCards)
            {
                outCards.Clear();
                for (ECardType type = info.minCardType; type <= info.maxCardType; type++)
                {
                    if (!mapTypes.ContainsKey(type))
                    {
                        outCards.Clear();
                        return false;
                    }

                    stCardItem item = mapTypes[type];

                    if (item.lstCards.Count < 1)
                    {
                        outCards.Clear();
                        return false;
                    }

                    UInt32 cardid = item.lstCards[0];
                    outCards.Add(cardid);
                }

                return true;
            }

            /// <summary>
            /// 检测是否有该连对牌组
            /// </summary>
            /// <param name="mapTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool CheckStraightPairItem(Dictionary<ECardType, stCardItem> mapTypes, stCardFormInfo info, List<UInt32> outCards)
            {
                outCards.Clear();
                for (ECardType type = info.minCardType; type <= info.maxCardType; type++)
                {
                    if(type == ECardType.Three)
                    {
                        return false;
                    }

                    if (!mapTypes.ContainsKey(type))
                    {
                        outCards.Clear();
                        return false;
                    }

                    stCardItem item = mapTypes[type];

                    if (item.lstCards.Count < 2)
                    {
                        outCards.Clear();
                        return false;
                    }

                    UInt32 cardid1 = item.lstCards[0];
                    UInt32 cardid2 = item.lstCards[1];
                    outCards.Add(cardid1);
                    outCards.Add(cardid2);
                }

                return true;
            }

            /// <summary>
            /// 检测是否有该飞机牌组
            /// </summary>
            /// <param name="mapTypes"></param>
            /// <param name="info"></param>
            /// <returns></returns>
            static bool CheckAircraftItem(Dictionary<ECardType, stCardItem> mapTypes, stCardFormInfo info, List<UInt32> outCards)
            {
                outCards.Clear();
                for (ECardType type = info.minCardType; type <= info.maxCardType; type++)
                {
                    if (!mapTypes.ContainsKey(type))
                    {
                        outCards.Clear();
                        return false;
                    }

                    stCardItem item = mapTypes[type];

                    if (item.lstCards.Count < 3)
                    {
                        outCards.Clear();
                        return false;
                    }

                    UInt32 cardid1 = item.lstCards[0];
                    UInt32 cardid2 = item.lstCards[1];
                    UInt32 cardid3 = item.lstCards[2];
                    outCards.Add(cardid1);
                    outCards.Add(cardid2);
                    outCards.Add(cardid3);
                }

                return true;
            }

            /// <summary>
            /// 检测是否有该飞机组牌
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="mapTypes"></param>
            /// <param name="info"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            static bool CheckAircraftPairItem(List<UInt32> selfHandCards, Dictionary<ECardType, stCardItem> mapTypes, stCardFormInfo info, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                outCards.Clear();
                //遍历手牌中所有比要比较的牌最小值大的
                for(ECardType type = info.minCardType; type <= info.maxCardType; type++)
                {
                    if (!mapTypes.ContainsKey(type))
                    {
                        outCards.Clear();
                        return false;
                    }

                    stCardItem item = mapTypes[type];
                    if (item.lstCards.Count < 3)
                    {
                        outCards.Clear();
                        return false;
                    }
                    UInt32 cardid1 = item.lstCards[0];
                    UInt32 cardid2 = item.lstCards[1];
                    UInt32 cardid3 = item.lstCards[2];
                    outCards.Add(cardid1);
                    outCards.Add(cardid2);
                    outCards.Add(cardid3);

                    foreach (var cardid in item.lstCards)
                    {
                        handcards.Remove(cardid);
                    }
                }

                // 需要对子数量
                int needPariCount = outCards.Count / 3;

                if (handcards.Count < needPariCount * 2)
                {
                    outCards.Clear();
                    return false;
                }

                //对剩余的牌进行特殊处理
                Dictionary<ECardType, stCardItem> mapItems = GetCardItems(handcards);
                handcards.Clear();
                foreach (var item in mapItems)
                {
                    if(item.Value.lstCards.Count >= 2)
                    {
                        handcards.Add(item.Value.lstCards[0]);
                        handcards.Add(item.Value.lstCards[1]);
                        item.Value.lstCards.RemoveAt(0);
                        item.Value.lstCards.RemoveAt(0);
                    }
                }

                for (int i = 0; i < handcards.Count - needPariCount*2 + 1; i++)
                {
                    List<UInt32> lstCards1 = new List<UInt32>();
                    for (int j = 0; j < needPariCount; j++)
                    {
                        UInt32 cardid = handcards[i + 2*j];
                        lstCards1.Add(cardid);
                        UInt32 cardid1 = handcards[i + 2 * j+1];
                        lstCards1.Add(cardid1);
                    }
                    List<UInt32> lstCards2 = new List<UInt32>();
                    lstCards2.AddRange(lstCards1);
                    lstCards2.AddRange(outCards);

                    stCardFormInfo tempInfo = new stCardFormInfo();
                    if (GetCardFormType(lstCards2, tempInfo, 0) != ECardFormType.AircraftPair)
                    {
                        continue;
                    }

                    if (tempInfo.cardTypeCount != info.cardTypeCount)
                    {
                        continue;
                    }

                    outCards.AddRange(lstCards1);
                    return true;
                }

                outCards.Clear();
                return false;
            }

            /// <summary>
            /// 检测是否有该飞机牌组
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="mapTypes"></param>
            /// <param name="info"></param>
            /// <param name="outCards"></param>
            /// <returns></returns>
            static bool CheckAircraftWingItem(List<UInt32> selfHandCards, Dictionary<ECardType, stCardItem> mapTypes, stCardFormInfo info, List<UInt32> outCards)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                outCards.Clear();
                for (ECardType type = info.minCardType; type <= info.maxCardType; type++)
                {
                    if (!mapTypes.ContainsKey(type))
                    {
                        outCards.Clear();
                        return false;
                    }

                    stCardItem item = mapTypes[type];

                    if (item.lstCards.Count < 3)
                    {
                        outCards.Clear();
                        return false;
                    }

                    UInt32 cardid1 = item.lstCards[0];
                    UInt32 cardid2 = item.lstCards[1];
                    UInt32 cardid3 = item.lstCards[2];
                    outCards.Add(cardid1);
                    outCards.Add(cardid2);
                    outCards.Add(cardid3);

                    foreach (var cardid in item.lstCards)
                    {
                        handcards.Remove(cardid);
                    }
                }

                // 需要单牌数量
                int needSingleCount = outCards.Count / 3;

                if (handcards.Count < needSingleCount)
                {
                    outCards.Clear();
                    return false;
                }

                // 对剩余的牌做特殊处理，优先单牌，对牌
                Dictionary<ECardType, stCardItem> mapItems = GetCardItems(handcards);
                handcards.Clear();
                foreach (var item in mapItems)
                {
                    if (item.Value.lstCards.Count <= 2)
                    {
                        handcards.AddRange(item.Value.lstCards);
                        item.Value.lstCards.Clear();
                    }
                    else
                    {
                        handcards.Add(item.Value.lstCards[0]);
                        handcards.Add(item.Value.lstCards[1]);
                        item.Value.lstCards.RemoveAt(0);
                        item.Value.lstCards.RemoveAt(0);
                    }
                }

                foreach (var item in mapItems)
                {
                    if (item.Value.lstCards.Count != 0)
                    {
                        handcards.AddRange(item.Value.lstCards);
                    }
                }

                for (int i = 0; i < handcards.Count - needSingleCount + 1; i++)
                {
                    List<UInt32> lstCards1 = new List<UInt32>();
                    for (int j = 0; j < needSingleCount; j++)
                    {
                        UInt32 cardid = handcards[i + j];
                        lstCards1.Add(cardid);
                    }
                    List<UInt32> lstCards2 = new List<UInt32>();
                    lstCards2.AddRange(lstCards1);
                    lstCards2.AddRange(outCards);

                    stCardFormInfo tempInfo = new stCardFormInfo();
                    if (GetCardFormType(lstCards2, tempInfo, 0) != ECardFormType.AircraftWing)
                    {
                        continue;
                    }

                    if (tempInfo.cardTypeCount != info.cardTypeCount)
                    {
                        continue;
                    }

                    outCards.AddRange(lstCards1);
                    return true;
                }

                outCards.Clear();
                return false;
            }

            static Dictionary<ECardType, stCardItem> GetCardItems(List<UInt32> lstCards)
            {
                lstCards.Sort();

                Dictionary<ECardType, stCardItem> mapItems = new Dictionary<ECardType, stCardItem>();

                foreach (var cardid in lstCards)
                {
                    ECardType type = GetCardType(cardid);
                    if (mapItems.ContainsKey(type))
                    {
                        mapItems[type].AddCard(cardid);
                    }
                    else
                    {
                        stCardItem item = new stCardItem();
                        item.type = type;
                        item.AddCard(cardid);
                        mapItems.Add(type, item);
                    }
                }

                return mapItems;
            }

            /// <summary>
            /// 获得所有合适的牌组
            /// </summary>
            /// <param name="otherInfo"></param>
            /// <param name="lstBigInfos"></param>
            public static void CheckStraightBig(stCardFormInfo otherInfo, List<stCardFormInfo> lstBigInfos)
            {
                if (otherInfo.maxCardType >= ECardType.A)
                {
                    return;
                }

                ECardType minType = otherInfo.minCardType + 1;
                ECardType maxType = otherInfo.maxCardType + 1;

                while (true)
                {
                    stCardFormInfo info = new stCardFormInfo();
                    info.minCardType = minType;
                    info.maxCardType = maxType;
                    info.cardTypeCount = otherInfo.cardTypeCount;
                    lstBigInfos.Add(info);

                    if (maxType >= ECardType.A)
                    {
                        break;
                    }

                    minType++;
                    maxType++;
                }
            }

            /// <summary>
            /// 删除3牌后是否有1张牌剩余
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="card"></param>
            /// <param name="outCardId"></param>
            /// <returns></returns>
            static bool IsOneCardLeft(List<UInt32> selfHandCards, stThreeCard card, out UInt32 outCardId)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                outCardId = 0;

                handcards.Sort();

                handcards.Remove(card.cardid1);
                handcards.Remove(card.cardid2);
                handcards.Remove(card.cardid3);

                if (handcards.Count > 0)
                {
                    outCardId = handcards[0];
                    return true;
                }

                return false;
            }

            /// <summary>
            /// 删除3牌后是否有1张牌剩余
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="card"></param>
            /// <param name="outCardId"></param>
            /// <returns></returns>
            static bool IsOneCardLeft(List<UInt32> selfHandCards, stFourCard card, out UInt32 outCardId)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                outCardId = 0;

                handcards.Sort();

                handcards.Remove(card.cardid1);
                handcards.Remove(card.cardid2);
                handcards.Remove(card.cardid3);

                foreach (var cardid in handcards)
                {
                    ECardType type = GetCardType(cardid);
                    if (type != card.type)
                    {
                        outCardId = cardid;
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// 获得所有牌
            /// </summary>
            /// <param name="selfHandCards"></param>
            /// <param name="lstOneCards"></param>
            /// <param name="lstTwoCards"></param>
            /// <param name="lstThreeCards"></param>
            /// <param name="lstFourCards"></param>
            /// <param name="lstKingBombs"></param>
            /// <returns></returns>
            static bool GetAllKindCards(List<UInt32> selfHandCards, List<stOneCard> lstOneCards, List<stTwoCard> lstTwoCards, List<stThreeCard> lstThreeCards, List<stFourCard> lstFourCards, List<stKingBomb> lstKingBombs)
            {
                List<UInt32> handcards = new List<UInt32>();
                handcards.AddRange(selfHandCards);

                handcards.Sort();

                // 检测王炸
                for (int i = 0; i < handcards.Count - 1; i++)
                {
                    UInt32 cardid1 = handcards[i];
                    UInt32 cardid2 = handcards[i + 1];

                    ECardType type1 = GetCardType(cardid1);
                    ECardType type2 = GetCardType(cardid2);

                    if (type1 == ECardType.SmallJoker && type2 == ECardType.BigJoker)
                    {
                        stKingBomb kingBomb = new stKingBomb(cardid1, cardid2);
                        lstKingBombs.Add(kingBomb);
                    }
                }

                foreach (var item in lstKingBombs)
                {
                    handcards.Remove(item.cardid1);
                    handcards.Remove(item.cardid2);
                }

                Dictionary<ECardType, stCardItem> mapTypes = GetCardItems(handcards);
                foreach (var item in mapTypes.Values)
                {
                    if (item.lstCards.Count == 4)
                    {
                        stFourCard card = new stFourCard(item.type, item.lstCards[0], item.lstCards[1], item.lstCards[2], item.lstCards[3]);
                        lstFourCards.Add(card);
                    }
                    else if (item.lstCards.Count == 3)
                    {
                        stThreeCard card = new stThreeCard(item.type, item.lstCards[0], item.lstCards[1], item.lstCards[2]);
                        lstThreeCards.Add(card);
                    }
                    else if (item.lstCards.Count == 2)
                    {
                        stTwoCard card = new stTwoCard(item.type, item.lstCards[0], item.lstCards[1]);
                        lstTwoCards.Add(card);
                    }
                    else if (item.lstCards.Count == 1)
                    {
                        stOneCard card = new stOneCard(item.type, item.lstCards[0]);
                        lstOneCards.Add(card);
                    }
                }

                return true;
            }

            /// <summary>
            /// 是否大于其他人的牌
            /// </summary>
            /// <param name="selfOutCards">自己出的牌</param>
            /// <param name="otherOutCards">其他人出的牌</param>
            /// <returns></returns>
            public static bool IsGreaterCards(List<UInt32> selfOutCards, UInt32 selfType, List<UInt32> otherOutCards,
                 UInt32 otherType, bool onlyLaizi)
            {
                stCardFormInfo selfInfo = new stCardFormInfo();
                stCardFormInfo otherInfo = new stCardFormInfo();

                ECardFormType selfForm = GetCardFormType(selfOutCards, selfInfo, selfType);
                ECardFormType otherForm = GetCardFormType(otherOutCards, otherInfo, otherType);
                // 不是牌形
                if (selfForm == ECardFormType.Null)
                {
                    return false;
                }

                if (otherForm == ECardFormType.Null)
                {
                    return false;
                }

                //如果上家只剩下一张癞子 走什么都大
                if (onlyLaizi)
                {
                    return true;
                }

                // 牌形不一致
                if (selfForm != otherForm)
                {
                    if (otherForm == ECardFormType.KingBomb)
                    {
                        return false;
                    }

                    if (selfForm == ECardFormType.KingBomb)
                    {
                        return true;
                    }

                    if (otherForm == ECardFormType.Bomb)
                    {
                        return false;
                    }

                    if (selfForm == ECardFormType.Bomb)
                    {
                        return true;
                    }

                    if (otherForm == ECardFormType.ThreeBomb)
                    {
                        return false;
                    }

                    if (selfForm == ECardFormType.ThreeBomb)
                    {
                        return true;
                    }

                    if (otherForm == ECardFormType.LaizigouBomb)
                    {
                        return false;
                    }

                    if (selfForm == ECardFormType.LaizigouBomb)
                    {
                        return true;
                    }
                    return false;
                }

                // 牌形一致
                if (selfInfo.cardTypeCount == otherInfo.cardTypeCount)
                {
                    if (selfInfo.minCardType > otherInfo.minCardType)
                    {
                        return true;
                    }
                }

                return false;
            }
        }
    }
}
