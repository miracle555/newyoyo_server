﻿using Protocols.GoldenFlower;
using System;
using System.Collections.Generic;

namespace HelpAsset
{
    namespace FlowerHelp
    {

        ///扑克牌类型
        ///
        public enum ECardType
        {
            Black=1,
            Red=2,
            Flower=3,
            Cube=4,

        }

        //Null = 0, // 错误牌型
        //Hight = 1, //散牌  
        //Pair = 2, // 对子 
        //Straight = 3, // 顺子
        //Golden = 4, // 金花  
        //StraightGolden = 5, // 金花顺 
        //Leopard = 6, //  豹子 
        public class FlowerHelp
        {
            /// <summary>
            /// 得到牌型
            /// </summary>
            ///
            

             public  static ECardFormType Getpaixing(List<UInt32> handCards)
            {
                UInt32 one =+(handCards[0] % 13 - handCards[1] % 13);
                UInt32 two = +(handCards[1] % 13 - handCards[2] % 13);
                UInt32 three = +(handCards[2] % 13- handCards[0] % 13);
                UInt32 onei = handCards[0] % 13;
                UInt32 twoi = handCards[1] % 13;
                UInt32 threei = handCards[2] % 13;
                List<ECardType> typelist= Gettype(handCards);
                List<UInt32> meList = new List<UInt32>();
                meList.Add(onei);
                meList.Add(twoi);
                meList.Add(threei);
                meList.Sort();
                if (one==0&&two==0)
                {//炸弹
                    return ECardFormType.Leopard;
                }
                if (one==0||two==0||three==0)
                {//对子
                    return ECardFormType.Pair;
                }
                if (meList[0]+1==meList[1]&& meList[1] + 1 == meList[2])
                {//顺子
                    if (typelist[0]== typelist[1]&& typelist[0] == typelist[2])
                    {
                        //同花顺
                        return ECardFormType.StraightGolden;
                    }                  
                    //顺子
                    return ECardFormType.Straight;
                }

                if (typelist[0] == typelist[1] && typelist[0] == typelist[2])
                {
                    //同花
                    return ECardFormType.Golden;
                }
                
                if (onei==12||twoi==12||threei==12)
                {
                    if (onei == 0|| twoi == 0 || threei == 0)
                    {
                        if (onei == 1 || twoi == 1 || threei == 1)
                        {
                            //123顺子
                            return ECardFormType.OttStraight;
                        }
                    }
                }

                //特殊牌235
                if (onei == 3 || twoi == 3 || threei == 3)
                {
                    if (onei == 0 || twoi == 0 || threei == 0)
                    {
                        if (onei == 1 || twoi == 1 || threei == 1)
                        {
                            if (typelist[0] == typelist[1] && typelist[0] == typelist[2])
                            {
                                //同花
                                return ECardFormType.Golden;
                            }
                            //235
                            return ECardFormType.Twothreefive;
                        }
                    }
                }

                return ECardFormType.Hight;
            }

            ///得到花色
            ///
            static List<ECardType> Gettype(List<UInt32> handCards)
            {
                List<ECardType> typelist = new List<ECardType>();
                foreach (var i in handCards)
                {
                    if (i>=0&&i<=12)
                    {
                        typelist.Add(ECardType.Black);
                        continue;
                    }
                    if (i >= 13 && i <= 25)
                    {
                        typelist.Add(ECardType.Red);
                        continue;
                    }
                    if (i >= 26 && i <= 38)
                    {
                        typelist.Add(ECardType.Flower);
                        continue;
                    }
                    if (i >= 39 && i <= 51)
                    {
                        typelist.Add(ECardType.Cube);
                        continue;
                    }
                }

                return typelist;
            }

            ///比牌
            ///
          public  static bool pkPai(List<UInt32> handCards, List<UInt32> otherCards)
            {
                bool i = false;
                ECardFormType paixing =  Getpaixing(handCards);

                ECardFormType otherpaixing = Getpaixing(otherCards);
                if(paixing== ECardFormType.Twothreefive || paixing == ECardFormType.Leopard)
                {
                    if (otherpaixing == ECardFormType.Twothreefive || otherpaixing == ECardFormType.Leopard)
                    {                                           
                            if (otherpaixing==paixing)
                            {
                                 if (paixing == ECardFormType.Leopard)
                                 {
                                     if (handCards[0] % 13> otherCards[0] % 13)
                                     {
                                          i = true;
                                          return i;
                                     }
                                     else
                                     {
                                          i = false;
                                          return i;
                                     }
                                 }
                                 else
                                 {
                                      i = false;
                                      return i;
                                 }                                                                 
                            }
                            i = (paixing > otherpaixing) ? true : false;
                            return i;                       
                    }
                }
                if (paixing!=otherpaixing)
                {
                    if (paixing== ECardFormType.Twothreefive)
                    {
                        i = false;
                        return i;
                    }
                    if (otherpaixing == ECardFormType.Twothreefive)
                    {
                        i = true;
                        return i;
                    }
                    i = (paixing > otherpaixing) ? true : false;
                    return i;

                }
                else
                {
                    UInt32 me = handCards[0] % 13 + handCards[1] % 13 + handCards[2] % 13;
                    UInt32 other = otherCards[0] % 13 + otherCards[1] % 13 + otherCards[2] % 13;

                    if (paixing == ECardFormType.Hight || paixing == ECardFormType.Golden)
                    {
                        List<UInt32> melist = new List<UInt32>();
                        List<UInt32> otherlist = new List<UInt32>();
                        foreach (var j in  handCards)
                        {
                            melist.Add(j % 13);
                        }
                        foreach (var j in otherCards)
                        {
                            otherlist.Add(j % 13);
                        }
                        melist.Sort();
                        otherlist.Sort();
                        if (melist[2] >= otherlist[2])
                        {
                            if (melist[2] == otherlist[2])
                            {
                                if (melist[1] >= otherlist[1])
                                {
                                    if (melist[1] == otherlist[1])
                                    {
                                        if (melist[0] >= otherlist[0])
                                        {
                                            if (melist[0] == otherlist[0])
                                            {
                                                i = false;
                                                return i;
                                            }
                                            i = true;
                                            return i;
                                        }
                                        i = false;
                                        return i;
                                    }
                                    i = true;
                                    return i;
                                }
                                i = false;
                                return i;
                            }
                            i = true;
                            return i;
                        }
                        i = false;
                        return i;
                    }
                    if (paixing == ECardFormType.Pair)
                    {
                        UInt32 mePair = 0;
                        UInt32 meOne = 0;
                        UInt32 otherPair = 0;
                        UInt32 otherOne = 0;
                        if (handCards[0] % 13== handCards[1] % 13)
                        {
                             mePair = handCards[0] % 13;
                             meOne = handCards[2] % 13;
                        }
                        if (handCards[1] % 13== handCards[2] % 13)
                        {
                            mePair = handCards[1] % 13;
                            meOne = handCards[0] % 13;
                        }

                        if (handCards[0] % 13 == handCards[2] % 13)
                        {
                            mePair = handCards[0] % 13;
                            meOne = handCards[1] % 13;
                        }

                        if (otherCards[0] % 13 == otherCards[1] % 13)
                        {
                            otherPair = otherCards[0] % 13;
                            otherOne = otherCards[2] % 13;
                        }
                        if (otherCards[1] % 13 == otherCards[2] % 13)
                        {
                            otherPair = otherCards[1] % 13;
                            otherOne = otherCards[0] % 13;
                        }

                        if (otherCards[0] % 13 == otherCards[2] % 13)
                        {
                            otherPair = otherCards[0] % 13;
                            otherOne = otherCards[1] % 13;
                        }


                        if (mePair>=otherPair)
                        {
                            if (mePair == otherPair)
                            {
                                i = (otherOne >= meOne) ? false: true;
                                return i;
                                
                            }

                            i = true;
                            return i;
                        }
                        i = false;
                        return i;
                    }

                    if (me != other)
                    {
                        i = (me > other) ? true : false;
                        return i;
                    }
                    i = false;
                }
                return i;
            }
        }      
    }
}