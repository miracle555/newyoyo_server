﻿/********************************************************************
	created:	2015/01/08
	created:	8:1:2015   17:47
	filename: 	D:\百度云\CommonPlatform\Server\Framework\Utility\Protocols\Proto\ProtoBody.cs
	file path:	D:\百度云\CommonPlatform\Server\Framework\Utility\Protocols\Proto
	file base:	ProtoBody
	file ext:	cs
	author:		刘冰生
	
	purpose:	
*********************************************************************/

using System;
using ProtoBuf;

namespace Protocols
{
    //协议体基类
    [ProtoContract]
    public class ProtoBody
    {
        internal int Serialize(System.IO.Stream stream)
        {
            ProtoBuf.Serializer.NonGeneric.Serialize(stream, this);

            return 0;
        }

        /// <summary>
        /// 使用指定的长度解析流
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="bodyType"></param>
        /// <param name="streamLen"></param>
        /// <returns></returns>
        internal static ProtoBody Deserialize(System.IO.Stream stream, Type bodyType, long len)
        {
            return Serializer.NonGeneric.Deserialize(bodyType, stream, len) as ProtoBody;
        }
    }
}
