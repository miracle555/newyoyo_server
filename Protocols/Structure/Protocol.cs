﻿using System;
using System.IO;
using ProtoBuf;

namespace Protocols
{
    //完整协议体
    public class Protocol
    {
        public MSGID MsgId
        {
            get
            {
                return (MSGID)protoHead.CmdID;
            }
        }

        public UInt64 RoleId
        {
            get
            {
                return serverHead.roleID;
            }
            set
            {
                serverHead.roleID = value;
            }
        }

        public UInt32 SessionId
        {
            get
            {
                return serverHead.sessionID;
            }

            set
            {
                serverHead.sessionID = value;
            }
        }

        public UInt64 uuid
        {
            get
            {
                return serverHead.uuid;
            }

            set
            {
                serverHead.uuid = value;
            }
        }

        public ProtoBody Body
        {
            get
            {
                if (protoBody == null && protoBodyStream != null)
                {
                    protoBodyStream.Position = 0;
                    protoBody = ProtoBody.Deserialize(protoBodyStream, protoHead.BodyType, protoHead.BodyLen);
                    protoBodyStream = null;
                }

                return protoBody;
            }
        }

        /// <summary>
        /// from ProtoBody to Protocol
        /// </summary>
        /// <param name="addr"></param>
        /// <returns></returns>
        public static implicit operator Protocol(ProtoBody bodyIn)
        {
            return new Protocol(bodyIn);
        }

        //服务器间通讯用头部,以后可能会有复数个
        private ServerHead serverHead;

        private ProtoHead protoHead;

        private ProtoBody protoBody;

        private Stream protoBodyStream;

        /// <summary>
        /// 仅生成ProtoHead
        /// </summary>
        public Protocol()
        {
            protoHead = new ProtoHead();
            serverHead = new ServerHead();

            protoBody = null;
        }

        /// <summary>
        /// 生成ProtoHead,ServerHead,ProtoID,并使用传入的ProtoBody赋值
        /// </summary>
        /// <param name="bodyIn"></param>
        public Protocol(ProtoBody bodyIn)
        {
            protoHead = new ProtoHead();
            serverHead = new ServerHead();
            protoBody = bodyIn;

            protoHead.CmdID = ProtoID.GetId(bodyIn.GetType());
        }

        public int Serialize(System.IO.Stream stream, bool isServerSession)
        {
            long basePosition = stream.Position;

            //serialze heads
            if (isServerSession)
            {
                //Utility.Debugger.Debug.Assert(null != serverHead);
                if (null != serverHead && !serverHead.Equals(ServerHead.NULL))
                {
                    serverHead.Serialize(stream);
                }
            }

            long protoHeadPosition = stream.Position;
            protoHead.Serialize(stream);
            long headTotalLen = stream.Position - basePosition;

            //serialize body
            if (protoBodyStream != null)
            {
                if (protoHead.BodyLen > 0)
                {
                    protoBodyStream.Position = 0;

                    byte[] buffer = new byte[protoHead.BodyLen];
                    protoBodyStream.Read(buffer, 0, (int)protoHead.BodyLen);
                    stream.Write(buffer, 0, (int)protoHead.BodyLen);
                }
            }
            else
            {
                protoBody.Serialize(stream);
            }

            //计算包体大小并调整
            protoHead.BodyLen = (UInt32)(stream.Length - headTotalLen);
            stream.Position = protoHeadPosition;
            protoHead.Serialize(stream);
            stream.Position = stream.Length;

            return (int)stream.Length;
        }

        //[12/3/2014 刘冰生] TODO:此处性能需要优化!
        public int Deserialize(System.IO.Stream stream, bool isServerSession)
        {
            protoBodyStream = null;

            if (deserializeHead(stream, isServerSession) != 0)
                return -1;

            Stream bodyStream = stream;
            protoBody = ProtoBody.Deserialize(bodyStream, protoHead.BodyType, protoHead.BodyLen);

            return 0;
        }

        public int DeserializeHead(System.IO.Stream stream, bool isServerSession)
        {
            protoBodyStream = null;

            if (deserializeHead(stream, isServerSession) != 0)
                return -1;

            //int pos = (int)stream.Position;
            protoBodyStream = new MemoryStream();
            if (protoHead.BodyLen > 0)
            {
                byte[] buffer = new byte[protoHead.BodyLen];
                stream.Read(buffer, 0, (int)protoHead.BodyLen);
                protoBodyStream.Write(buffer, 0, (int)protoHead.BodyLen);
                protoBodyStream.Position = 0;
            }
            return 0;
        }

        //重载ToString方法,便于输出
        public override string ToString()
        {
            return "";
            //Type type = ProtoID.GetType(MsgId);
            //if(null == type)
            //{
            //    return "unknown";
            //}

            //return type.Name;
        }

        private int deserializeHead(System.IO.Stream stream, bool isServerSession)
        {
            long initPos = stream.Position;
            //服务器包
            if (isServerSession)
            {
                serverHead = new ServerHead();
                if (serverHead.Deserialize(stream) < 0)
                {
                    stream.Position = initPos;
                    return -1;
                }
            }

            if (protoHead.Deserialize(stream) < 0)
            {
                stream.Position = initPos;
                return -1;
            }

            //[10/24/2015 刘冰生] 调整分包策略 
            //长度不够
            if ((stream.Length - stream.Position) < protoHead.BodyLen)
            {
                stream.Position = initPos;
                return -1;
            }

            return 0;
        }
    }
}
