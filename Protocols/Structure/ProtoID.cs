﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Protocols
{
    //使用枚举类来实现协议号,便于使用者扩展
    public class ProtoID
    {
        //////////////////////////////////////////////////////////////////////////
        // ProtoID static methods

        public static void Init()
        {
            if (null == mapId2Types)
            {
                mapId2Types = new Dictionary<UInt16, Type>();
            }

            if (null == mapType2Ids)
            {
                mapType2Ids = new Dictionary<Type, UInt16>();
            }

            Dictionary<string, UInt16> mapMsgs = new Dictionary<string, UInt16>();
            foreach (UInt16 msgid in Enum.GetValues(typeof(MSGID)))
            {
                string strName = Enum.GetName(typeof(MSGID), msgid);//获取名称
                mapMsgs[strName] = msgid;
            }

            Assembly assem = Assembly.GetExecutingAssembly();
            foreach (Type tp in assem.GetTypes())
            {
                if (tp.IsSubclassOf(typeof(ProtoBody)))
                {
                    string strName = tp.Name;
                    //Debug.Assert(mapMsgs.ContainsKey(strName));
                    if (mapMsgs.ContainsKey(strName))
                    {
                        UInt16 msgid = mapMsgs[strName];
                        mapId2Types[msgid] = tp;
                        mapType2Ids[tp] = msgid;
                    }
                }
            }
        }

        public static Type GetType(UInt16 msgid)
        {
            if (mapId2Types.ContainsKey(msgid))
            {
                return mapId2Types[msgid];
            }

            return null;
        }

        public static Type GetType(MSGID msgid)
        {
            UInt16 id = (UInt16)msgid;
            if (mapId2Types.ContainsKey(id))
            {
                return mapId2Types[id];
            }

            return null;
        }

        public static UInt16 GetId(Type type)
        {
            if (mapType2Ids.ContainsKey(type))
            {
                return mapType2Ids[type];
            }

            return 0;
        }

        /// <summary>
        /// 消息ID到type的映射
        /// </summary>
        private static Dictionary<UInt16, Type> mapId2Types;

        /// <summary>
        /// 消息type到id的映射
        /// </summary>
        private static Dictionary<Type, UInt16> mapType2Ids;

        class IntervalClass
        {
            public IntervalClass()
            {
                ProtoID.Init();
            }
        }

        static IntervalClass obj = new IntervalClass();
    }
}
