﻿using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    namespace MajiangPoint
    {
        /// <summary>
        /// 抠点创建房间
        /// </summary>
        [ProtoContract]
        public class MJPointCreateRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(2)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// 抠点加入房间
        /// </summary>
        [ProtoContract]
        public class MJPointJoinRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// 抠点获取房间信息
        /// </summary>
        [ProtoContract]
        public class MJPointGetRoomInfoL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (抠点)离开房间
        /// </summary>
        [ProtoContract]
        public class MJPointLeaveRoomB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (抠点)房间结束
        /// </summary>
        [ProtoContract]
        public class MJPointRoomCloseB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 是否归还房卡
            /// </summary>
            [ProtoMember(2)]
            public bool bReturnRoomCard = false;

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(3)]
            public UInt64 roomOwnerId = 0;

            /// <summary>
            /// 战绩
            /// </summary>
            [ProtoMember(4)]
            public stBattleScoreItem scoreItem = new stBattleScoreItem();

            /// <summary>
            /// 是否记录战绩
            /// </summary>
            [ProtoMember(5)]
            public bool bRecordBattleScore = false;

        };

        /// <summary>
        /// (抠点)玩家赢了一局
        /// </summary>
        [ProtoContract]
        public class MJPointWinOneRoundB2L : ProtoBody
        {
        };

        /// <summary>
        /// (抠点)房间所有人都下线了
        /// </summary>
        [ProtoContract]
        public class MJPointAllRoleOfflineB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };
    }
}