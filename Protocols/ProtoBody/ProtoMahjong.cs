﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Protocols
{
    // 抠点
    namespace Majiang
    {
        public enum EMJCard
        {
            Null = 0,
            OneRope1 = 1, // 一条
            OneRope2 = 2, // 一条
            OneRope3 = 3, // 一条
            OneRope4 = 4, // 一条
            TwoRope1 = 5, // 两条
            TwoRope2 = 6, // 两条
            TwoRope3 = 7, // 两条
            TwoRope4 = 8, // 两条
            ThreeRope1 = 9, // 三条
            ThreeRope2 = 10, // 三条
            ThreeRope3 = 11, // 三条
            ThreeRope4 = 12, // 三条
            FourRope1 = 13, // 四条
            FourRope2 = 14, // 四条
            FourRope3 = 15, // 四条
            FourRope4 = 16, // 四条
            FiveRope1 = 17, // 五条
            FiveRope2 = 18, // 五条
            FiveRope3 = 19, // 五条
            FiveRope4 = 20, // 五条
            SixRope1 = 21, // 六条
            SixRope2 = 22, // 六条
            SixRope3 = 23, // 六条
            SixRope4 = 24, // 六条
            SevenRope1 = 25, // 七条
            SevenRope2 = 26, // 七条
            SevenRope3 = 27, // 七条
            SevenRope4 = 28, // 七条
            EightRope1 = 29, // 八条
            EightRope2 = 30, // 八条
            EightRope3 = 31, // 八条
            EightRope4 = 32, // 八条
            NineRope1 = 33, // 九条
            NineRope2 = 34, // 九条
            NineRope3 = 35, // 九条
            NineRope4= 36, // 九条

            OneBucket1 = 37, // 一筒
            OneBucket2 = 38, // 一筒
            OneBucket3 = 39, // 一筒
            OneBucket4 = 40, // 一筒
            TwoBucket1 = 41, // 两筒
            TwoBucket2 = 42, // 两筒
            TwoBucket3 = 43, // 两筒
            TwoBucket4 = 44, // 两筒
            ThreeBucket1 = 45, // 三筒
            ThreeBucket2 = 46, // 三筒
            ThreeBucket3 = 47, // 三筒
            ThreeBucket4 = 48, // 三筒
            FourBucket1 = 49, // 四筒
            FourBucket2 = 50, // 四筒
            FourBucket3 = 51, // 四筒
            FourBucket4 = 52, // 四筒
            FiveBucket1 = 53, // 五筒
            FiveBucket2 = 54, // 五筒
            FiveBucket3 = 55, // 五筒
            FiveBucket4 = 56, // 五筒
            SixBucket1 = 57, // 六筒
            SixBucket2 = 58, // 六筒
            SixBucket3 = 59, // 六筒
            SixBucket4 = 60, // 六筒
            SevenBucket1 = 61, // 七筒
            SevenBucket2 = 62, // 七筒
            SevenBucket3 = 63, // 七筒
            SevenBucket4 = 64, // 七筒
            EightBucket1 = 65, // 八筒
            EightBucket2 = 66, // 八筒
            EightBucket3 = 67, // 八筒
            EightBucket4 = 68, // 八筒
            NineBucket1 = 69, // 九筒
            NineBucket2 = 70, // 九筒
            NineBucket3 = 71, // 九筒
            NineBucket4 = 72, // 九筒

            OneMillion1 = 73, // 一万
            OneMillion2 = 74, // 一万
            OneMillion3 = 75, // 一万
            OneMillion4 = 76, // 一万
            TwoMillion1 = 77, // 两万
            TwoMillion2 = 78, // 两万
            TwoMillion3 = 79, // 两万
            TwoMillion4 = 80, // 两万
            ThreeMillion1 = 81, // 三万
            ThreeMillion2 = 82, // 三万
            ThreeMillion3 = 83, // 三万
            ThreeMillion4 = 84, // 三万
            FourMillion1 = 85, // 四万
            FourMillion2 = 86, // 四万
            FourMillion3 = 87, // 四万
            FourMillion4 = 88, // 四万
            FiveMillion1 = 89, // 五万
            FiveMillion2 = 90, // 五万
            FiveMillion3 = 91, // 五万
            FiveMillion4 = 92, // 五万
            SixMillion1 = 93, // 六万
            SixMillion2 = 94, // 六万
            SixMillion3 = 95, // 六万
            SixMillion4 = 96, // 六万
            SevenMillion1 = 97, // 七万
            SevenMillion2 = 98, // 七万
            SevenMillion3 = 99, // 七万
            SevenMillion4 = 100, // 七万
            EightMillion1 = 101, // 八万
            EightMillion2 = 102, // 八万
            EightMillion3 = 103, // 八万
            EightMillion4 = 104, // 八万
            NineMillion1 = 105, // 九万
            NineMillion2 = 106, // 九万
            NineMillion3 = 107, // 九万
            NineMillion4 = 108, // 九万

            EastWind1 = 109, // 东风
            EastWind2 = 110, // 东风
            EastWind3 = 111, // 东风
            EastWind4 = 112, // 东风
            SouthWind1 = 113 , // 南风
            SouthWind2 = 114, // 南风
            SouthWind3 = 115, // 南风
            SouthWind4 = 116, // 南风
            WestWind1 = 117, // 西风
            WestWind2 = 118, // 西风
            WestWind3 = 119, // 西风
            WestWind4 = 120, // 西风
            NorthWind1 = 121, // 北风
            NorthWind2 = 122, // 北风
            NorthWind3 = 123, // 北风
            NorthWind4 = 124, // 北风

            RedDragon1 = 125, // 红中
            RedDragon2 = 126, // 红中
            RedDragon3 = 127, // 红中
            RedDragon4 = 128, // 红中
            Fortune1 = 129, // 发财
            Fortune2 = 130, // 发财
            Fortune3 = 131, // 发财
            Fortune4 = 132, // 发财
            WhiteFace1 = 133, // 白脸
            WhiteFace2 = 134, // 白脸
            WhiteFace3 = 135, // 白脸
            WhiteFace4 = 136, // 白脸
            Max = 137,
        }

        public enum EMajongDirection : int
        {
            Null = 0, // 默认 
            East = 1, // 东
            North = 2, // 北
            West = 3, // 西
            South = 4, // 南
            Max = 5, // 最大
        }

        /// <summary>
        /// 牌类型
        /// </summary>
        public enum EMahjongCard
        {
            Null = 0,
            WindCard = 1, // 风牌
            ArrowCard = 2, // 箭牌
            MillionCard = 3, // 万字牌
            BucketCard = 4, // 筒字牌
            RopeCard = 5, // 索字牌
        }

        public enum EMJCardType
        {
            Null = 0,
            OneRope = 1, // 一条
            TwoRope = 2, // 两条
            ThreeRope = 3, // 三条
            FourRope = 4, // 四条
            FiveRope = 5, // 五条
            SixRope = 6, // 六条
            SevenRope = 7, // 七条
            EightRope = 8, // 八条
            NineRope = 9, // 九条
            OneBucket = 10, // 一筒
            TwoBucket = 11, // 两筒
            ThreeBucket = 12, // 三筒
            FourBucket = 13, // 四筒
            FiveBucket = 14, // 五筒
            SixBucket = 15, // 六筒
            SevenBucket = 16, // 七筒
            EightBucket = 17, // 八筒
            NineBucket = 18, // 九筒
            OneMillion = 19, // 一万
            TwoMillion = 20, // 两万
            ThreeMillion = 21, // 三万
            FourMillion = 22, // 四万
            FiveMillion = 23, // 五万
            SixMillion = 24, // 六万
            SevenMillion = 25, // 七万
            EightMillion = 26, // 八万
            NineMillion = 27, // 九万
            EastWind = 28, // 东风
            SouthWind = 29, // 南风
            WestWind = 30, // 西风
            NorthWind = 31, // 北风
            RedDragon = 32, // 红中
            Fortune = 33, // 发财
            WhiteFace = 34, // 白脸
            Max = 35,
        }

        [ProtoContract]
        public class ReadyItem
        {
            /// <summary>
            /// 牌类型
            /// </summary>
            [ProtoMember(1)]
            public EMJCardType cardType = EMJCardType.Null;

            /// <summary>
            /// 剩余张数
            /// </summary>
            [ProtoMember(2)]
            public int cardCount = 0;
        }

        [ProtoContract]
        public class ReadyInfo
        {
            /// <summary>
            /// 可以出的牌
            /// </summary>
            [ProtoMember(1)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 听牌剩余张数
            /// </summary>
            [ProtoMember(2)]
            public List<ReadyItem> lstItems = new List<ReadyItem>();
        }
    }
}