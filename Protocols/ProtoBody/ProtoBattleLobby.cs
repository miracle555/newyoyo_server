﻿using System;
using ProtoBuf;
using System.Collections.Generic;
using Protocols.MajiangPoint;

namespace Protocols
{
    [ProtoContract]
    public class RoleInfo
    {
        [ProtoMember(1)]
        public UInt64 roleid { get; set; }

        [ProtoMember(2)]
        public string nickName { get; set; }

        [ProtoMember(3)]
        public ERoleSex sex { get; set; }

        [ProtoMember(4)]
        public UInt32 level = 0;

        [ProtoMember(5)]
        public UInt32 gateAddr = 0;

        [ProtoMember(6)]
        public UInt32 sessionId = 0;

        [ProtoMember(7)]
        public string clientIp = "";

        /// <summary>
        /// 微信头像url
        /// </summary>
        [ProtoMember(8)]
        public string weixinHeadImgUrl = "";

        [ProtoMember(9)]
        public UInt64 roomUuid = 0;
    }

    /// <summary>
    /// 创建角色在战斗服上
    /// </summary>
    [ProtoContract]
    public class CreatePlayerOnBattleL2B : ProtoBody
    {
        [ProtoMember(1)]
        public RoleInfo roleInfo = new RoleInfo();
    };

    /// <summary>
    /// 清理房间信息
    /// </summary>
    [ProtoContract]
    public class GmClearRoomL2B : ProtoBody
    {
        [ProtoMember(1)]
        public UInt64 roomUuid = 0;
    };
}