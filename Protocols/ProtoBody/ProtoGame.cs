﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Protocols
{
    /// <summary>
    /// 游戏类型
    /// </summary>
    public enum EGameType
    {
        Null = 0,
        MJPushDown = 1, // 推倒胡
        MJPoint = 2, // 抠点
        Landlord = 3,  // 斗地主

        GoldenFlower=4,//扎金花

        MJYunCheng = 5,//运城贴金

    }

    public enum ERoomCardEventType
    {
        Null = 0,
        Add = 1, // 运营商充卡
        Del = 2, // 转出
        Consume = 3, // 消耗
        Return = 4,//运营商退卡
        recharge = 5,//充值购买
    }

    public enum ERebateEventTyp
    {
        Null = 0,
        /// <summary>
        /// 返利
        /// </summary>
        Rebate = 1,

        /// <summary>
        /// 提现
        /// </summary>
        WithDraw = 2,
    }

    /// <summary>
    /// 房卡事件条目
    /// </summary>
    [ProtoContract]
    public class stRCEventItem
    {
        /// <summary>
        /// 日期时间
        /// </summary>
        [ProtoMember(1)]
        public DateTime dateTime = DateTime.MinValue;

        /// <summary>
        /// 玩家uuid
        /// </summary>
        [ProtoMember(2)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 事件类型
        /// </summary>
        [ProtoMember(3)]
        public ERoomCardEventType eventType = ERoomCardEventType.Null;

        /// <summary>
        /// 游戏类型
        /// </summary>
        [ProtoMember(4)]
        public EGameType gameType = EGameType.Null;

        /// <summary>
        /// 房间局数
        /// </summary>
        [ProtoMember(5)]
        public int roomRound = 0;

        /// <summary>
        /// 房卡数量
        /// </summary>
        [ProtoMember(6)]
        public UInt32 roomCardCount = 0;

        /// <summary>
        /// 名称
        /// </summary>
        [ProtoMember(7)]
        public string name = "";

        /// <summary>
        /// 代理商id
        /// </summary>
        [ProtoMember(8)]
        public UInt64 agentId = 0;
    }

    /// <summary>
    /// 1个玩家的事件
    /// </summary>
    [ProtoContract]
    public class stRCEventRoleItem
    {
        /// <summary>
        /// 玩家uuid
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 该玩家当日事件
        /// </summary>
        [ProtoMember(2)]
        public List<stRCEventItem> lstEvents = new List<stRCEventItem>();
    }

    /// <summary>
    /// 1个代理的事件
    /// </summary>
    [ProtoContract]
    public class stRCEventAgentItem
    {
        /// <summary>
        /// 日期
        /// </summary>
        [ProtoMember(1)]
        public DateTime date = DateTime.MinValue.Date;

        /// <summary>
        /// 代理uuid
        /// </summary>
        [ProtoMember(2)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 该代理当日事件
        /// </summary>
        [ProtoMember(3)]
        public List<stRCEventItem> lstEvents = new List<stRCEventItem>();

        /// <summary>
        /// 代理昵称
        /// </summary>
        [ProtoMember(4)]
        public string nickName = "";

        /// <summary>
        /// 该代理手下的所有角色
        /// </summary>
        [ProtoMember(5)]
        public Dictionary<UInt64, stRCEventRoleItem> mapRoles = new Dictionary<ulong, stRCEventRoleItem>();
    }

    /// <summary>
    /// 返利流水
    /// </summary>
    [ProtoContract]
    public class stRCEventRebateItem
    {
        /// <summary>
        /// 日期时间
        /// </summary>
        [ProtoMember(1)]
        public DateTime dateTime = DateTime.MinValue;

        /// <summary>
        /// 玩家的id
        /// </summary>
        [ProtoMember(2)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 事件类型
        /// </summary>
        [ProtoMember(3)]
        public ERebateEventTyp eventType = ERebateEventTyp.Null;

        /// <summary>
        /// 金额
        /// </summary>
        [ProtoMember(4)]
        public double money = 0;

        /// <summary>
        /// 名字
        /// </summary>
        [ProtoMember(5)]
        public string name = "";

    }
    /// <summary>
    /// 一个玩家的事件
    /// </summary>
    [ProtoContract]
    public class stRCEventRoleRebateItem
    {
        /// <summary>
        /// 玩家uuid
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 该玩家当日事件
        /// </summary>
        [ProtoMember(2)]
        public List<stRCEventRebateItem> lstEvents = new List<stRCEventRebateItem>();
    }


    /// <summary>
    /// 一个代理的事件
    /// </summary>
    [ProtoContract]
    public class stRCEventAgentRebateItem
    {
        /// <summary>
        /// 日期
        /// </summary>
        [ProtoMember(1)]
        public DateTime date = DateTime.MinValue.Date;

        /// <summary>
        /// 代理uuid
        /// </summary>
        [ProtoMember(2)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 该代理当日事件
        /// </summary>
        [ProtoMember(3)]
        public List<stRCEventRebateItem> lstEvents = new List<stRCEventRebateItem>();

        /// <summary>
        /// 代理昵称
        /// </summary>
        [ProtoMember(4)]
        public string nickName = "";

        /// <summary>
        /// 该代理手下的所有角色
        /// </summary>
        [ProtoMember(5)]
        public Dictionary<UInt64, stRCEventRoleRebateItem> mapRoles = new Dictionary<ulong, stRCEventRoleRebateItem>();
    }

    /// <summary>
    /// 房卡来源类型
    /// </summary>
    public enum ERCSourceType
    {
        Null = 0,

        /// <summary>
        /// 系统
        /// </summary>
        System = 1,

        /// <summary>
        /// 运营商
        /// </summary>
        Gm = 2,

        /// <summary>
        /// 代理转移
        /// </summary>
        Agent = 3, 

        /// <summary>
        /// 商城
        /// </summary>
        Mall = 4,
    }

    /// <summary>
    /// 房卡条目
    /// </summary>
    [ProtoContract]
    public class stRoomCardItem
    {
        /// <summary>
        /// 房卡来源类型
        /// </summary>
        [ProtoMember(1)]
        public ERCSourceType sourceType = ERCSourceType.Null;

        /// <summary>
        /// 代理ID或运营商ID
        /// </summary>
        [ProtoMember(2)]
        public UInt64 srcRoleId = 0;

        /// <summary>
        /// 房卡数量
        /// </summary>
        [ProtoMember(3)]
        public UInt32 roomCardCount = 0;

        /// <summary>
        /// 扣除房卡用户的ID
        /// </summary>
        [ProtoMember(4)]
        public UInt64 roleid = 0;
    }

    /// <summary>
    /// 房主
    /// </summary>
    [ProtoContract]
    public class stRoomOwner
    {
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        [ProtoMember(2)]
        public string nickName = "";

        [ProtoMember(3)]
        public UInt64 falseFangzhu = 0;
    }

    /// <summary>
    /// 战绩玩家信息
    /// </summary>
    [ProtoContract]
    public class stBattleScoreRole
    {
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        [ProtoMember(2)]
        public string nickName = "";

        [ProtoMember(3)]
        public int score = 0;
    }

    /// <summary>
    /// 解散房间信息
    /// </summary>
    [ProtoContract]
    public class stDismissInfo
    {
        // 解散发起者
        [ProtoMember(1)]
        public UInt64 firstRoleId = 0;

        // 同意解散人员
        [ProtoMember(2)]
        public HashSet<UInt64> setAgreeRoles = new HashSet<UInt64>();

        /// <summary>
        /// 解散拒绝人员
        /// </summary>
        [ProtoMember(3)]
        public HashSet<UInt64> setNotRoles = new HashSet<UInt64>();

        [ProtoMember(4)]
        public bool bStart = false;

        [ProtoMember(5)]
        public DateTime expireTime = DateTime.MinValue;

        public void Clear()
        {
            firstRoleId = 0;
            setAgreeRoles.Clear();
            setNotRoles.Clear();
            bStart = false;
        }
    }

    /// <summary>
    /// 战绩条目
    /// </summary>
    [ProtoContract]
    public class stBattleScoreItem
    {
        [ProtoMember(1)]
        public DateTime dateTime = DateTime.MinValue;

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(2)]
        public EGameType gameType = EGameType.Null;

        /// <summary>
        /// 房间uuid
        /// </summary>
        [ProtoMember(3)]
        public UInt64 roomUuid = 0;

        /// <summary>
        /// 房主
        /// </summary>
        [ProtoMember(4)]
        public stRoomOwner roomOwner = new stRoomOwner();

        /// <summary>
        /// 每个玩家的信息
        /// </summary>
        [ProtoMember(5)]
        public List<stBattleScoreRole> lstRoles = new List<stBattleScoreRole>();
    }

    /// <summary>
    /// 获得房间游戏类型
    /// </summary>
    [ProtoContract]
    public class GetRoomGameTypeReq : ProtoBody
    {
        /// <summary>
        /// 房间uuid
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roomUuid = 0;
    };

    /// <summary>
    /// 获得房间游戏类型返回
    /// </summary>
    [ProtoContract]
    public class GetRoomGameTypeRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 房间不存在
            /// </summary>
            NotExist = 2,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(2)]
        public EGameType gameType = EGameType.Null;
    };

    /// <summary>
    /// 玩家上下线
    /// </summary>
    [ProtoContract]
    public class RoomRoleOnlineOfflineNtf : ProtoBody
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 上线还是离线
        /// </summary>
        [ProtoMember(2)]
        public bool bOnline = false;

        /// <summary>
        /// 玩家ip
        /// </summary>
        [ProtoMember(3)]
        public string clientIp = "";
    };

    /// <summary>
    /// 经验排行
    /// </summary>
    [ProtoContract]
    public class stExpRankItem
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        /// <summary>
        /// 昵称
        /// </summary>
        [ProtoMember(2)]
        public string nickName = "";

        [ProtoMember(3)]
        public UInt32 level = 0; // 称号等级

        [ProtoMember(4)]
        public string title = ""; // 称号

        [ProtoMember(5)]
        public UInt32 curExp = 0; // 当前经验
        /// <summary>
        /// 微信头像url
        /// </summary>
        [ProtoMember(6)]
        public string weixinHeadImgUrl = "";

        /// <summary>
        /// 角色性别
        /// </summary>
        [ProtoMember(7)]
        public ERoleSex sex = ERoleSex.Boy;

        /// <summary>
        /// 排行
        /// </summary>
        [ProtoMember(8)]
        public int rank = 0;
    }

    /// <summary>
    /// 获取经验排行榜
    /// </summary>
    [ProtoContract]
    public class GetExpRankReq : ProtoBody
    {
    };

    /// <summary>
    /// 获取经验排行返回
    /// </summary>
    [ProtoContract]
    public class GetExpRankRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 经验排行
        /// </summary>
        [ProtoMember(2)]
        public List<stExpRankItem> lstRankItems = new List<stExpRankItem>();

        /// <summary>
        /// 自己的排行
        /// </summary>
        [ProtoMember(3)]
        public int selfRank = 0;

        /// <summary>
        /// 自己称号等级
        /// </summary>
        [ProtoMember(4)]
        public UInt32 selfLevel = 0;

        /// <summary>
        /// 自己称号
        /// </summary>
        [ProtoMember(5)]
        public string selfTitle = ""; 
    };

    /// <summary>
    /// 获取战绩列表
    /// </summary>
    [ProtoContract]
    public class GetBattleScoreItemsReq : ProtoBody
    {
    };

    /// <summary>
    /// 获取战绩列表返回
    /// </summary>
    [ProtoContract]
    public class GetBattleScoreItemsRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 战绩列表
        /// </summary>
        [ProtoMember(2)]
        public List<stBattleScoreItem> lstScoreItems = new List<stBattleScoreItem>();
    };

    /// <summary>
    /// 新战绩通知
    /// </summary>
    [ProtoContract]
    public class BattleScoreItemNtf : ProtoBody
    {
        /// <summary>
        /// 战绩
        /// </summary>
        [ProtoMember(1)]
        public stBattleScoreItem scoreItem = new stBattleScoreItem();
    };

    /// <summary>
    /// 玩家信息
    /// </summary>
    [ProtoContract]
    public class stRoleItem
    {
        [ProtoMember(1)]
        public UInt64 roleid = 0;

        [ProtoMember(2)]
        public string nickName = "";

        [ProtoMember(3)]
        public ERoleSex sex = ERoleSex.Boy;

        [ProtoMember(4)]
        public UInt32 level = 0;

        [ProtoMember(5)]
        public string clientIp = "";

        /// <summary>
        /// 微信头像url
        /// </summary>
        [ProtoMember(6)]
        public string weixinHeadImgUrl = "";

        /// <summary>
        /// 是否在线
        /// </summary>
        [ProtoMember(7)]
        public bool isOnline = false;
    };

    /// <summary>
    /// 获取房间其他玩家信息
    /// </summary>
    [ProtoContract]
    public class GetRoomRolesInfoReq : ProtoBody
    {
        [ProtoMember(1)]
        public UInt64 roomuuid = 0;
    };

    /// <summary>
    /// 获取房间其他玩家信息
    /// </summary>
    [ProtoContract]
    public class GetRoomRolesInfoRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 房间不存在
            /// </summary>
            RoomNotExist = 2,

            /// <summary>
            /// 房间已满
            /// </summary>
            RoomFull = 3,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        [ProtoMember(2)]
        public List<stRoleItem> lstRoles = new List<stRoleItem>();
    };

    [ProtoContract]
    public class GetPayOrderReq : ProtoBody
    {
        /// <summary>
        /// 商品id
        /// </summary>
        [ProtoMember(1)]
        public uint recharge_id;
    }

    [ProtoContract]
    public class GetPayOrderRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 商品不存在
            /// </summary>
            ItemNotExist = 2,

            /// <summary>
            /// 未填写邀请者
            /// </summary>
            NoInviter = 3,

            /// <summary>
            /// 微信支付下单失败
            /// </summary>
            WeiXinPrePayFailed = 4,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 商户号
        /// </summary>
        [ProtoMember(2)]
        public string partnerid;

        /// <summary>
        /// 预支付id
        /// </summary>
        [ProtoMember(3)]
        public string prepayid;

        /// <summary>
        /// 微信appid
        /// </summary>
        [ProtoMember(4)]
        public string appid;

        /// <summary>
        /// 微信商户Key
        /// </summary>
        [ProtoMember(5)]
        public string MchKey;
    }

    public enum Pay
    {
        masterPay = 1,
        AAPay = 2,
        anthorPay = 3,
    }

    /// <summary>
    /// 当前房主建房req
    /// </summary>
    [ProtoContract]
    public class CurrentAnthorCreatRoomReq : ProtoBody
    {
        
    }

    /// <summary>
    /// 当前房主建房rsp
    /// </summary>
    [ProtoContract]
    public class CurrentAnthorCreatRoomRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 房主建房列表
        /// </summary>
        [ProtoMember(2)]
        public Dictionary<UInt64, AnthorRoom> roomList;
    }

    /// <summary>
    /// 历史房主建房req
    /// </summary>
    [ProtoContract]
    public class HistoryAnthorCreatRoomReq : ProtoBody
    {

    }

    /// <summary>
    /// 历史房主建房rsp
    /// </summary>
    [ProtoContract]
    public class HistoryAnthorCreatRoomRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 房主建房列表
        /// </summary>
        [ProtoMember(2)]
        public Dictionary<UInt64, AnthorRoom> roomList;
    }

    /// <summary>
    /// 房主建房
    /// </summary>
    [ProtoContract]
    public class AnthorRoom : ProtoBody
    {
        public AnthorRoom()
        {
        }

        /// <summary>
        /// 房间id
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roomUuid;

        /// <summary>
        /// 创建时间
        /// </summary>
        [ProtoMember(2)]
        public DateTime creatTime;

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(3)]
        public EGameType gameType;

        /// <summary>
        /// 房间是否结束
        /// </summary>
        [ProtoMember(4)]
        public bool isEnd;

        /// <summary>
        /// 游戏房间总人数
        /// </summary>
        [ProtoMember(5)]
        public int totalNumber;

        /// <summary>
        /// 游戏房间当前人数
        /// </summary>
        [ProtoMember(6)]
        public int currentNumber;

        /// <summary>
        /// 是否是单金 true是单金 false是双金
        /// </summary>
        [ProtoMember(7)]
        public bool danjin = false;

        /// <summary>
        /// 0位标题 1位内容
        /// </summary>
        [ProtoMember(8)]
        public string[] shareTitle = new string[2];
    }

    /// <summary>
    /// 中奖列表
    /// </summary>
    [ProtoContract]
    public class Winning : ProtoBody
    {
        /// <summary>
        /// 中奖时间
        /// </summary>
        [ProtoMember(1)]
        public DateTime winTime;

        /// <summary>
        /// 中奖类型
        /// </summary>
        [ProtoMember(2)]
        public WinType winType;
    }

    /// <summary>
    /// 中奖类型
    /// </summary>
    public enum WinType
    {
        RoomCardOne,//1张房卡
        RoomCardThree,//3张房卡
        OneHundredAndEightyEight,//188
        EighteenPointEight,//18.8红包
        RoomCardSix,//6张房卡
        ZeroPointOneEight,//0.18
        RoomCardEight,//8张房卡
        OnePointEightEight,//1.88
    }

    /// <summary>
    /// 获取其他玩家位置信息req
    /// </summary>
    [ProtoContract]
    public class OtherLocationReq : ProtoBody
    {
        /// <summary>
        /// 玩家id
        /// </summary>
        [ProtoMember(1)]
        public UInt64 otherId;
    }

    /// <summary>
    /// 获取其他玩家位置信息返回rsp
    /// </summary>
    [ProtoContract]
    public class OtherLocationRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;
        /// <summary>
        /// 玩家位置信息
        /// </summary>
        [ProtoMember(2)]
        public String address;

        /// <summary>
        /// 玩家距离信息
        /// </summary>
        [ProtoMember(3)]
        public String distance;
    }

    /// <summary>
    /// 红包请求
    /// </summary>
    [ProtoContract]
    public class RedPackageReq : ProtoBody
    {

    }

    /// <summary>
    /// 红包返回
    /// </summary>
    [ProtoContract]
    public class RedPackageRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 红包累计金额
        /// </summary>
        [ProtoMember(2)]
        public decimal redPacketMoney;

        /// <summary>
        /// 每日对局次数
        /// </summary>
        [ProtoMember(3)]
        public int playRoundCount;

        /// <summary>
        /// 已经进行的抽奖次数
        /// </summary>
        [ProtoMember(4)]
        public int readyNumber;

        /// <summary>
        /// 每日抽奖次数
        /// </summary>
        [ProtoMember(5)]
        public int luckDrawCount;

        [ProtoMember(6)]
        public bool hasShare = false;
    }

    /// <summary>
    /// 玩家转盘请求
    /// </summary>
    [ProtoContract]
    public class TurntableReq : ProtoBody
    {
        
    }

    /// <summary>
    /// 玩家转盘返回
    /// </summary>
    [ProtoContract]
    public class TurntableRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 转盘抽奖返回
        /// </summary>
        [ProtoMember(2)]
        public WinType winType;
    }

    /// <summary>
    /// 房主建房解散房间
    /// </summary>
    [ProtoContract]
    public class DissolveTheRoomReq : ProtoBody
    {
        /// <summary>
        /// 房间id
        /// </summary>
        [ProtoMember(1)]
        public UInt64 roomId;
    }

    /// <summary>
    /// 房主建房解散房间返回
    /// </summary>
    [ProtoContract]
    public class DissolveTheRoomRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;
    }

    /// <summary>
    /// 微信分享请求
    /// </summary>
    [ProtoContract]
    public class WeChatShareReq : ProtoBody
    {

    }

    /// <summary>
    /// 微信分享返回
    /// </summary>
    [ProtoContract]
    public class WeChatShareRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 已经进行分享
            /// </summary>
            HaveShare = 2,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

    }

}