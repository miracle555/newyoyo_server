﻿using System;

namespace Protocols
{
    public enum MSGID : ushort
    {
        /////////////////////////////////////////////////////////////////////////
        // 特殊消息
        ClientConnectNtf = 1,
        ClientDisconnectNtf = 2,
        /////////////////////////////////////////////////////////////////////////
        ServerRegisterReq = 1000,
        ServerRegisterRsp = 1001,
        ServerListNtf = 1003,
        ServerRegisterSelfReq = 1004,

        KickAccountReq = 1005,
        KickAccountRsp = 1006,
        GateCommonBroadcast = 1007,
        KickNtf = 1008,
        HeartbeatReq = 1009,
        HeartbeatRsp = 1010,

        TestAddExpReq = 1101, // 测试添加经验
        TestAddExpRsp = 1102, // 测试添加经验返回
        TestRechargeReq = 1103,//测试充值时返利
        TestRechargeRsp = 1104,//测试充值时返利返回

        /////////////////////////////////////////////////////////////////////////
        GmModifySelfPasswordReq = 2001, // GM修改自己密码
        GmModifySelfPasswordRsp = 2002, // gm修改自己密码
        GmGetServerCurrentStatusReq = 2003, // GM获取服务器当前状态
        GmGetServerCurrentStatusRsp = 2004, // gm获取服务器当前状态
        GmCreateAgentAccountReq = 2005, // GM创建代理账号
        GmCreateAgentAccountRsp = 2006, // gm创建代理账号
        GmStopAgentAccountReq = 2007, // GM停用代理账号
        GmStopAgentAccountRsp = 2008, // GM停用代理账号
        GmModifyAgentAccountReq = 2009,// GM修改代理账号密码
        GmModifyAgentAccountRsp = 2010, // GM修改代理账号密码
        GmAddRoomCardReq = 2011, // GM增加房卡
        GmAddRoomCardRsp = 2012, // gm增加房卡返回
        GmDecRoomCardReq = 2013, // GM扣除房卡
        GmDecRoomCardRsp = 2014, // gm减少房卡返回
        GmTransferRoomCardReq = 2015, // 代理转移房卡
        GmTransferRoomCardRsp = 2016, // 代理转移房卡返回
        GmPublicNoticeReq = 2017, // gm发公告
        GmPublicNoticeRsp = 2018, // gm发公告返回
        GmQueryAgentRoomCardEventsReq = 2019, // gm查询代理房卡流水
        GmQueryAgentRoomCardEventsRsp = 2020, // gm查询代理房卡流水返回
        AgemtQueryRoomCardEventsReq = 2021, // 代理查询流水事件
        AgemtQueryRoomCardEventsRsp = 2022, // 代理查询流水事件返回
        GmGetPlayerInfoReq = 2023, // gm获取玩家信息
        GmGetPlayerInfoRsp = 2024, // gm获取玩家信息
        GmAddAgentRoomCardReq = 2025,//gm增加代理商房卡
        GmAddAgentRoomCardRsp = 2026,//gm增加代理商房卡返回
        GmDecAgentRoomCardReq = 2027,//gm减少代理商房卡
        GmDecAgentRoomCardRsp = 2028,//gm减少代理商房卡返回
        GmAgentImpowerReq = 2029,//代理授权
        GmAgentImpowerRsp = 2030,//代理授权返回
        GmAgentStopPowerReq = 2031,//代理停权
        GmAgentStopPowerRsp = 2032,//代理停权返回
        GmGetAgentListReq = 2033,//代理列表
        GmGetAgentListRsp = 2034,//代理列表返回
        AgentDataReq = 2035,//代理数据
        AgentDataRsp = 2036,//代理数据返回
        GmClearRoomReq = 2037, // 清理房间数据
        GmClearRoomRsp = 2038, // 清理房间数据返回
        GmClearPlayerRoomReq = 2039, // 清理玩家房间数据
        GmClearPlayerRoomRsp = 2040, // 清理玩家房间数据
        GmQueryAgentRebateEventsReq = 2041,//Gm查询代理返利
        GmQueryAgentRebateEventsRsp = 2042,//Gm查询代理返利返回
        AgentQueryRebateEventsReq = 2043,//代理查询返利
        AgentQueryRebateEventsRsp = 2044,//代理查询返利返回
        GmBettleMentReq = 2045,//gm结算返利
        GmBettleMentRsp = 2046,//gm结算返利返回
        GmQuerylowerLevelUserListReq = 2047,//gm查询代理下级用户列表请求
        GmQuerylowerLevelUserListRsp = 2048,//gm查询代理下级用户列表返回
        GmQueryUserRechargeEventReq = 2049,//gm查询用户充值请求
        GmQueryUserRechargeEventRsp = 2050,//gm查询用户充值返回
        UserInfoListReq = 2051,//用户列表请求
        UserInfoListRsp = 2052,//用户列表返回
        AgentUserListReq = 2053,//代理查询用户列表请求
        AgentUserListRsp = 2054,//代理查询用户列表返回
        AgentModifyPasswordReq = 2055,//代理修改密码请求
        AgentModifyPasswordRsp = 2056,//代理修改密码返回
        DissolveTheRoomReq = 2057,//房主建房 解散房间req
        DissolveTheRoomRsp = 2058,//房主建房 解散房间rsp
        DeductionsMoneyReq = 2059,//扣除玩家中奖金额req
        DeductionsMoneyRsp = 2060,//扣除玩家中间金额rsp
        GmQueryTurnRecordReq=2061,//GM查询代理转让记录
        GmQueryTurnRecordRsp = 2062,//GM查询代理转让记录
        GmQueryDeductRecordReq =2063,//GM查询代理扣除记录
        GmQueryDeductRecordRsp = 2064,//GM查询代理扣除记录
        GmResetServicePasswordReq =2065,//GM重置客服的密码
        GmResetServicePasswordRsp = 2066,//GM重置客服的密码     
        GmQueryTixianRecordReq = 2067,//GM查询代理扣除记录
        GmQueryTixianRecordRsp = 2068,//GM查询代理扣除记录
        


        /////////////////////////////////////////////////////////////////////////
        //Login功能用
        UserLoginReq = 3000, // 玩家登陆
        UserLoginRsp = 3001, //玩家登陆
        ReloginReq = 3002, //断线重连
        ReloginRsp = 3003, //断线重连
        GetServerTimeReq = 3004, //获取服务器时间
        GetServerTimeRsp = 3005, //获取服务器时间
        PlayerOnlineNtf = 3006, //玩家上线
        PlayerOfflineNtf = 3007, //玩家下线
        PlayerDataReq = 3008, //玩家数据请求
        PlayerDataRsp = 3009, //玩家数据请求
        GetPlayerStateReq = 3010, //获取玩家状态
        GetPlayerStateRsp = 3012, //获取玩家状态
        RoleExpChangeNtf = 3013, // 经验变化通知
        RoleLevelChangeNtf = 3014, // 等级变化通知
        RoomCardChangeNtf = 3015, // 房卡数量变化通知
        InputInviteSelfRoleReq = 3016, // 填写邀请自己的人
        InputInviteSelfRoleRsp = 3017, // 填写邀请自己的人
        GetServerConfigReq = 3018, // 获取服务器配置
        GetServerConfigRsp = 3019, // 获取服务器配置
        PublicNoticeNtf = 3020, // 公告通知
        InviteRoleCountChangeNtf = 3021, // 邀请人数量变化通知
        UserRegisterReq = 3022, // 玩家注册
        UserRegisterRsp = 3023, // 玩家注册返回
        UserLocationInfoReq = 3024,//玩家定位信息req
        UserLocationInfoRsp = 3025,//玩家定位信息rsp
        CurrentAnthorCreatRoomReq = 3026,//当前房主建房Req
        CurrentAnthorCreatRoomRsp = 3027,//当前房主建房Rsp
        HistoryAnthorCreatRoomReq = 3028,//历史房主建房Req
        HistoryAnthorCreatRoomRsp = 3029,//历史房主建房Rsp
        RedPackageReq = 3032,//红包请求req
        RedPackageRsp = 3033,//红包请求rsp
        TurntableReq = 3034,//转盘req
        TurntableRsp = 3035,//转盘rsp
        WeChatShareReq = 3036,//微信分享req
        WeChatShareRsp = 3037,//微信分享rsp

        /////////////////////////////////////////////////////////////////////////
        //战斗和网关间的消息
        BattleServerBindReq = 4000, //绑定战斗服
        BattleServerUnbindReq = 4001, //解绑战斗服

        /////////////////////////////////////////////////////////////////////////
        //lobby和battle间消息
        CreatePlayerOnBattleL2B = 5000,
        GmClearRoomL2B = 5001, //gm清理房间信息

        // 推倒胡消息
        MJPushDownCreateRoomL2B = 5100, // 创建玩家在战斗服上
        MJPushDownJoinRoomL2B = 5101, //(对倒胡)加入房间
        MJPushDownLeaveRoomB2L = 5102, // 推倒胡离开房间
        MJPushDownGetRoomInfoL2B = 5103, // 吹倒胡获取房间信息
        MJPushDownWinOneRoundB2L = 5104, // 推倒胡赢了一局
        MJPushDownAllRoleOfflineB2L = 5105, // (推倒胡）房间所有人都下线
        MJPushDownRoomCloseB2L = 5106, // 房间结束

        // 抠点消息
        MJPointCreateRoomL2B = 5200, // 创建玩家在战斗服上
        MJPointJoinRoomL2B = 5201, // （抠点）加入房间
        MJPointLeaveRoomB2L = 5202, // 抠点离开房间
        MJPointGetRoomInfoL2B = 5203, // 抠点获取房间信息
        MJPointWinOneRoundB2L = 5204, // 抠点赢了一局
        MJPointAllRoleOfflineB2L = 5205, // (抠点）房间所有人都下线
        MJPointRoomCloseB2L = 5206, // 房间结束

        // 斗地主消息
        LandlordCreateRoomL2B = 5300, // (斗地主）创建房间
        LandlordJoinRoomL2B = 5301, // (斗地主）加入房间
        LandlordGetRoomInfoL2B = 5302, // (斗地主）获取房间信息
        LandlordLeaveRoomB2L = 5303, // (斗地主)离开房间
        LandlordWinOneRoundB2L = 5304, // （斗地主）玩家赢了一局
        LandlordAllRoleOfflineB2L = 5305, // (斗地主）房间所有人都下线
        LandlordRoomCloseB2L = 5306, // 房间结束

      

        /////////////////////////////////////////////////////////////////////////

        // 所有游戏共用消息
        GetRoomGameTypeReq = 6013, // 获得房间游戏类型
        GetRoomGameTypeRsp = 6014, // 获得房间游戏类型返回
        RoomRoleOnlineOfflineNtf = 6015,// 上下线通知客户端
        GetBattleScoreItemsReq = 6016, // 战绩列表请求
        GetBattleScoreItemsRsp = 6017, // 战绩列表返回
        BattleScoreItemNtf = 6018, // 新战绩通知
        GetExpRankReq = 6019, // 获取经验排行
        GetExpRankRsp = 6020, // 获取经验排行返回
        GetRoomRolesInfoReq = 6021, // 获取房间其他玩家信息
        GetRoomRolesInfoRsp = 6022, // 获得房间其他玩家信息
        GetPayOrderReq = 6023,  //获取订单信息
        GetPayOrderRsp = 6024,  //订单信息返回

        //////////////////////////////////////////////////////////////////////////

        // 推到胡麻将消息
        MJPushDownCreateRoomReq = 7000, // (推倒胡麻将)创建房间
        MJPushDownCreateRoomRsp = 7001, // (推倒胡麻将)创建房间返回
        MJPushDownJoinRoomReq = 7002, // (推倒胡麻将)加入房间
        MJPushDownJoinRoomRsp = 7003, // (推倒胡麻将)加入房间返回
        MJPushDownJoinRoomNtf = 7004, // (推倒胡麻将)加入房间通知
        MJPushDownRoundStartNtf = 7005, // (推倒胡麻将)回合开始通知
        MJPushDownHandOutCardReq = 7006, //(推倒胡麻将)出牌请求
        MJPushDownHandOutCardRsp = 7007, // (推倒胡麻将)出牌请求返回
        MJPushDownHandOutCardNtf = 7008, // (推倒胡麻将)出牌通知
        MJPushDownGetNewCardNtf = 7009, // (推倒胡麻将)起到新牌
        MJPushDownPengHuCardNtf = 7010, // (推倒胡麻将)通知碰牌，杠牌，胡牌
        MJPushDownPengCardReq = 7011, // (推倒胡麻将)碰牌请求
        MJPushDownPengCardRsp = 7012, // (推倒胡麻将)碰牌请求返回
        MJPushDownPengCardNtf = 7013, // (推倒胡麻将)碰牌通知
        MJPushDownHuCardReq = 7014, // (推倒胡麻将)胡牌请求
        MJPushDownHuCardRsp = 7015, // (推倒胡麻将)胡牌返回
        MJPushDownHuCardNtf = 7016, // (推倒胡麻将)胡牌通知
        MJPushDownFlowRoundNtf = 7017, // (推倒胡麻将)流局通知
        MJPushDownGangCardReq = 7018, // (推倒胡麻将)杠牌请求
        MJPushDownGangCardRsp = 7019, // (推倒胡麻将)杠牌请求返回
        MJPushDownGangCardNtf = 7020, // (推倒胡麻将)杠牌通知
        MJPushDownReadyCardReq = 7021, // (推倒胡麻将)听牌请求
        MJPushDownReadyCardRsp = 7022, // (推倒胡麻将)听牌请求返回
        MJPushDownReadyCardNtf = 7023, // (推倒胡麻将)听牌通知
        MJPushDownPassReq = 7024, // (推倒胡麻将)过
        MJPushDownPassRsp = 7025, // (推倒胡麻将)过
        MJPushDownGetRoomInfoReq = 7026, // (推倒胡麻将)获取房间信息
        MJPushDownGetRoomInfoRsp = 7027, // (推倒胡麻将)获取房间信息返回
        MJPushDownNeedHandCardNtf = 7028, // (推倒胡麻将)轮到你出牌了
        MJPushDownNextStepReq = 7030, // (推倒胡麻将)下一步
        MJPushDownNextStepRsp = 7031, // (推倒胡麻将)下一步返回
        MJPushDownDismissGameReq = 7032, // (推倒胡麻将)解散游戏
        MJPushDownDismissGameRsp = 7033, // (推倒胡麻将)解散游戏返回 
        MJPushDownDismissGameNtf = 7034, // (推倒胡麻将)解散通知
        MJPushDownDismissGameSuccessNtf = 7035, // (推倒胡麻将)解散通过通知
        MJPushDownRoundSettleNtf = 7036, // (推倒胡麻将)结算通知
        MJPushDownNextStepNtf = 7037, // (推倒胡麻将)下一步通知
        MJPushDownLackingOneDoorReq = 7038, // (推倒胡麻将)缺一门选择
        MJPushDownLackingOneDoorRsp = 7039, // (推倒胡麻将)缺一门选择返回
        MJPushDownLackingDoorFinishNtf = 7040, // (推倒胡麻将)缺一门选择结束
        MJPushDownLeaveRoomReq = 7041, // 房间未开始时离开房间请求
        MJPushDownLeaveRoomRsp = 7042, // 房间未开始时离开房间返回
        MJPushDownLeaveRoomNtf = 7043, // 房间未开始时离开房间通知
        MJPushDownDismissAgreeReq = 7044, // 解散是否同意请求
        MJPushDownDismissAgreeRsp = 7045, // 解散是否同意返回
        MJPushDownDismissAgreeNtf = 7046, // 解散是否同意通知
        MJPushDownNeedCheckSelfGangHuReadyNtf = 7047, // (推倒胡麻将)请检测暗杠，补杠，自摸胡，听牌
        MJPushDownCheckSelfGangHuReadyReq = 7048, // (推倒胡麻将)请检测暗杠，补杠，自摸胡，听牌
        MJPushDownCheckSelfGangHuReadyRsp = 7059, // (推倒胡麻将)请检测暗杠，补杠，自摸胡，听牌返回消息
        MJPushDownChatReq = 7050, // (推倒胡麻将)聊天请求
        MJPushDownChatRsp = 7051, // (推倒胡麻将)聊天请求返回
        MJPushDownChatNtf = 7052, // (推倒胡麻将)聊天请求通知
        MJPushDownChatWordReq = 7053,//(推倒胡麻将)文字聊天请求
        MJPushDownChatWordRsp = 7054,//(推倒胡麻将)文字聊天返回
        MJPushDownChatWordNtf = 7055,//(推倒胡麻将)文字聊天通知

        // 抠点麻将消息
        MJPointCreateRoomReq = 8000, // (抠点)创建房间
        MJPointCreateRoomRsp = 8001, // (抠点)创建房间返回
        MJPointJoinRoomReq = 8002, // (抠点)加入房间
        MJPointJoinRoomRsp = 8003, // (抠点)加入房间返回
        MJPointJoinRoomNtf = 8004, // (抠点)加入房间通知
        MJPointRoundStartNtf = 8005, // (抠点)回合开始通知
        MJPointHandOutCardReq = 8006, //(抠点)出牌请求
        MJPointHandOutCardRsp = 8007, // (抠点)出牌请求返回
        MJPointHandOutCardNtf = 8008, // (抠点)出牌通知
        MJPointGetNewCardNtf = 8009, // (抠点)起到新牌
        MJPointPengHuCardNtf = 8010, // (抠点)通知碰牌，杠牌，胡牌
        MJPointPengCardReq = 8011, // (抠点)碰牌请求
        MJPointPengCardRsp = 8012, // (抠点)碰牌请求返回
        MJPointPengCardNtf = 8013, // (抠点)碰牌通知
        MJPointHuCardReq = 8014, // (抠点)胡牌请求
        MJPointHuCardRsp = 8015, // (抠点)胡牌返回
        MJPointHuCardNtf = 8016, // (抠点)胡牌通知
        MJPointFlowRoundNtf = 8017, // (抠点)流局通知
        MJPointGangCardReq = 8018, // (抠点)杠牌请求
        MJPointGangCardRsp = 8019, // (抠点)杠牌请求返回
        MJPointGangCardNtf = 8020, // (抠点)杠牌通知
        MJPointReadyCardReq = 8021, // (抠点)听牌请求
        MJPointReadyCardRsp = 8022, // (抠点)听牌请求返回
        MJPointReadyCardNtf = 8023, // (抠点)听牌通知
        MJPointPassReq = 8024, // (抠点)过
        MJPointPassRsp = 8025, // (抠点)过
        MJPointGetRoomInfoReq = 8026, // (抠点)获取房间信息
        MJPointGetRoomInfoRsp = 8027, // (抠点)获取房间信息返回
        MJPointNeedHandCardNtf = 8028, // (抠点)轮到你出牌了
        MJPointNextStepReq = 8030, // (抠点)下一步
        MJPointNextStepRsp = 8031, // (抠点)下一步返回
        MJPointDismissGameReq = 8032, // (抠点)解散游戏
        MJPointDismissGameRsp = 8033, // (抠点)解散游戏返回 
        MJPointDismissGameNtf = 8034, // (抠点)解散通知
        MJPointDismissGameSuccessNtf = 8035, // (抠点)解散通过通知
        MJPointRoundSettleNtf = 8036, // (抠点)结算通知
        MJPointNextStepNtf = 8037, // (抠点)下一步通知
        MJPointLeaveRoomReq = 8038, // 房间未开始时离开房间请求
        MJPointLeaveRoomRsp = 8039, // 房间未开始时离开房间返回
        MJPointLeaveRoomNtf = 8040, // 房间未开始时离开房间通知
        MJPointDismissAgreeReq = 8041, // 解散是否同意请求
        MJPointDismissAgreeRsp = 8042, // 解散是否同意返回
        MJPointDismissAgreeNtf = 8043, // 解散是否同意通知
        MJPointNeedCheckSelfGangHuReadyNtf = 8044, // (抠点)请检测暗杠，补杠，自摸胡，听牌
        MJPointCheckSelfGangHuReadyReq = 8045, // (抠点)请检测暗杠，补杠，自摸胡，听牌
        MJPointCheckSelfGangHuReadyRsp = 8056, // (抠点)请检测暗杠，补杠，自摸胡，听牌返回消息
        MJPointChatReq = 8047, // (抠点)聊天请求
        MJPointChatRsp = 8048, // (抠点)聊天请求返回
        MJPointChatNtf = 8049, // (抠点)聊天请求通知
        MJPointChatWordReq = 8050,//(抠点)文字聊天请求
        MJPointChatWordRsp = 8051,//(抠点)文字聊天返回
        MJPointChatWordNtf = 8052,//(抠点)文字聊天通知

        // 斗地主消息
        LandlordCreateRoomReq = 9000, // (斗地主)创建房间
        LandlordCreateRoomRsp = 9001, // (斗地主)创建房间返回
        LandlordJoinRoomReq = 9002, // (斗地主)斗地主加入房间
        LandlordJoinRoomRsp = 9003, // (斗地主)加入房间返回
        LandlordJoinRoomNtf = 9004, //(斗地主)加入房间通知

        LandlordNeedRoundConfigSelectNtf = 9005, // (斗地主)回合配置需要选择通知
        LandlordRoundConfigSelectReq = 9006, // (斗地主)回合配置选择
        LandlordRoundConfigSelectRsp = 9007, // (斗地主)回合配置选择返回
        LandlordRoundConfigSelectNtf = 9008, // (斗地主)回合配置选择通知

        LandlordNeedShowNewCardsNtf = 9009, // (斗地主)开始显示新牌通知
        LandlordMingCardScalesReq = 9010, // (斗地主)明牌倍数
        LandlordMingCardScalesRsp = 9011, //(斗地主)明牌倍数请求返回
        LandlordMingCardScalesNtf = 9012, // (斗地主)明牌倍数请求通知

        LandlordNeedRobLandlordNtf = 9013, // 通知客户端需要抢地主
        LandlordRobLandlordReq = 9014, //(斗地主)抢地主
        LandlordRobLandlordRsp = 9015, // (斗地主)抢地主返回
        LandlordRobLandlordNtf = 9016, // (斗地主)抢地主通知
        LandlordRobLandlordResultNtf = 9017, // (斗地主)抢地主最终确定通知

        LandlordShowDoubleScalesNtf = 9018, // 通知显示加倍
        LandlordDoubleScalesReq = 9019, // (斗地主)加倍请求
        LandlordDoubleScalesRsp = 9020, // (斗地主)加倍请求返回
        LandlordDoubleScalesNtf = 9021, // (斗地主)加倍请求通知

        LandlordNeedHandOutCardNtf = 9022, // (斗地主)轮到你出牌了
        LandlordHandOutCardReq = 9023, // (斗地主)出牌请求
        LandlordHandOutCardRsp = 9024, // (斗地主)出牌请求返回
        LandlordHandOutCardNtf = 9025, // (斗地主)出牌通知

        LandlordRoundSettleNtf = 9026, // (斗地主)结算通知
        LandlordGetRoomInfoReq = 9027, // (斗地主)获取房间信息
        LandlordGetRoomInfoRsp = 9028, // (斗地主)获取房间信息返回

        LandlordNextStepReq = 9029, //(斗地主)下一步
        LandlordNextStepRsp = 9030, // (斗地主)下一步消息返回
        LandlordNextStepNtf = 9031,

        LandlordDismissGameReq = 9032, // (斗地主)解散游戏
        LandlordDismissGameRsp = 9033, // (斗地主)解散游戏返回
        LandlordDismissGameNtf = 9034, // (斗地主)解散通知
        LandlordDismissGameSuccessNtf = 9035, // (斗地主)解散通过通知
        LandlordDismissAgreeReq = 9036, // (斗地主)解散是否同意
        LandlordDismissAgreeRsp = 9037, // (斗地主)解散是否同意返回
        LandlordDismissAgreeNtf = 9038, // (推倒胡麻将)解散是否同意通知
        LandlordLeaveRoomReq = 9039, // (斗地主)未开始时退出房间
        LandlordLeaveRoomRsp = 9040, // (斗地主)裂开房间返回
        LandlordLeaveRoomNtf = 9041, // (斗地主)离开房间通知

        LandlordTotalScalesChangeNtf = 9042, // (斗地主）总倍数变化通知

        LandlordChatReq = 9043, // (斗地主)聊天请求
        LandlordChatRsp = 9044, // (斗地主)聊天请求返回
        LandlordChatNtf = 9045, // (斗地主)聊天请求通知

        LandlordNeedCallLandlordNtf = 9046, // 通知客户端需要叫地主
        LandlordCallLandlordReq = 9047, //(叫地主)抢地主
        LandlordCallLandlordRsp = 9048, // (叫地主)抢地主返回
        LandlordCallLandlordNtf = 9049, // (叫地主)抢地主通知

        LandlordChatWordReq = 9050,//(斗地主)文字聊天请求
        LandlordChatWordRsp = 9051,//(斗地主)文字聊天返回
        LandlordChatWordNtf = 9052,//(斗地主)文字聊天通知
        LandlordTimerDoubleScaleNtf = 9053,//加倍倒计时通知
        LandlordDistanceReq = 9054,//获取与另一个玩家之间的距离请求
        LandlordDistanceRsp = 9055,//获取与另一个玩家之间的距离返回
        LandlordEggReq = 9056,//运城扔鸡蛋请求
        LandlordEggRsp = 9057,//运城扔鸡蛋返回
        LandlordEggNtf = 9058,//运城扔鸡蛋通知
        LandlordOtherCloseRoomL2B = 9059,//运城麻将房主代开房解散房间


        // 扎金花消息定义
        GoldenFlowerCreateRoomL2B = 5400, // (扎金花）创建房间
        GoldenFlowerJoinRoomL2B = 5401, // (扎金花）加入房间
        GoldenFlowerGetRoomInfoL2B = 5402, // (扎金花）获取房间信息
        GoldenFlowerLeaveRoomB2L = 5403, // (扎金花)离开房间
        GoldenFlowerWinOneRoundB2L = 5404, // （扎金花）玩家赢了一局
        GoldenFlowerAllRoleOfflineB2L = 5405, // (扎金花）房间所有人都下线
        GoldenFlowerRoomCloseB2L = 5406, //  (扎金花）房间结束

        // 扎金花消息定义
        GoldenFlowerCreateRoomReq = 20000, // (扎金花)创建房间
        GoldenFlowerCreateRoomRsp = 20001,//(扎金花)创建房间返回
        GoldenFlowerJoinRoomReq = 20002,//(扎金花)加入房间
        GoldenFlowerJoinRoomRsp = 20003,//(扎金花)加入房间返回
        GoldenFlowerJoinRoomNtf = 20005,//(扎金花)加入房间通知
        GoldenFlowerRoundStartNtf = 20006,// (扎金花)回合开始通知
        GoldenFlowerGetRoomInfoReq = 20007,//(扎金花)获取房间信息
        GoldenFlowerGetRoomInfoRsp = 20008,//(扎金花)获取房间信息返回
        GoldenFlowerShowCardReq =20009,//(扎金花)看牌请求
        GoldenFlowerShowCardRsp =20010,//扎金花看牌请求返回
        GoldenFlowerRaiseCardReq = 20011,//扎金花)加注请求
        GoldenFlowerRaiseCardRsp = 20012,//扎金花 加注请求返回
        
        GoldenFlowerDarkCardReq = 20013,//扎金花)跟注请求
        GoldenFlowerDarkCardRsp =20014,//扎金花跟注请求返回
        
        GoldenFlowerPariMutuelNtf =20015,//扎金花彩分通知
        GoldenFlowerCallCardReq = 20016, //(扎金花)弃牌通知
        GoldenFlowerCallCardRsp = 20017,//(扎金花)弃牌通知返回
       
        GoldenFlowerHandOutCardReq = 20018,// (扎金花)比牌请求
        GoldenFlowerHandOutCardRsp = 20019,//(扎金花)比牌请求返回

        GoldenFlowerDismissGameReq = 20020, //(扎金花)解散游戏
        GoldenFlowerDismissGameRsp = 20021,//(扎金花)解散游戏返回
        GoldenFlowerDismissGameNtf = 20022,// (扎金花)解散通知
        GoldenFlowerDismissGameSuccessNtf = 20023,//(扎金花)解散通过通知
        GoldenFlowerDismissAgreeReq = 20024,//(扎金花)解散是否同意
        GoldenFlowerDismissAgreeRsp = 20025,// (扎金花)解散是否同意返回
        GoldenFlowerDismissAgreeNtf = 20026,//(扎金花)解散是否同意通知
        GoldenFlowerLeaveRoomReq = 20027,// (扎金花)未开始时退出房间
        GoldenFlowerLeaveRoomRsp = 20028,// (扎金花)离开房间返回
        GoldenFlowerLeaveRoomNtf = 20029, // (扎金花)离开房间通知
        GoldenFlowerTotalScalesChangeNtf = 20030,//  (扎金花)锅底分变化通知
        GoldenFlowerChatReq = 20031,//  (扎金花)聊天请求
        GoldenFlowerChatRsp = 20032,//  (扎金花)聊天请求返回
        GoldenFlowerChatNtf = 20033,//  (扎金花)聊天请求通知
        GoldenFlowerChatWordReq = 20034,//  用户聊天输入文字请求
        GoldenFlowerChatWordRsp = 20035,// 用户聊天输入文字返回
        GoldenFlowerChatWordNtf=20036,// 用户聊天输入文字通知
        GoldenFlowerNextStepReq = 20037, //下一步
        GoldenFlowerNextStepRsp = 20038, // 下一步消息返回
        GoldenFlowerNextStepNtf = 20039, //下一步广播
        GoldenFlowerRoundSettleNtf=20040,//结算通知
        GoldenFlowerWinNtf = 20041,//检测最后一位玩家赢
        GoldenFlowerStartReq= 20042,//开始游戏请求
        GoldenFlowerStartRsp= 20043,//开始游戏返回
        GoldenFlowerShowCardNtf=20044,//看牌通知
        GoldenFlowerRaiseCardNtf = 20045,//扎金花加注通知
        GoldenFlowerDarkCardNtf = 20046,//扎金花跟注通知
        GoldenFlowerCallCardNtf = 20047,//扎金花弃牌通知
        GoldenFlowerHandOutCardNtf = 20048,//(扎金花)比牌通知
        GoldenFlowerStartFenReq=20049,//(扎金花)开始底分请求
        GoldenFlowerStartFenRsp = 20050,//(扎金花)开始底分返回
        GoldenFlowerStartFenNtf = 20051,//(扎金花)开始底分通知
        GoldenFlowerNeedHandOutCardNtf=20052,//(扎金花)该下位玩家操作
        GoldenFlowerDistanceReq = 22053,//获取与另一个玩家之间的距离请求
        GoldenFlowerDistanceRsp = 22054,//获取与另一个玩家之间的距离返回
        GoldenFlowerEggReq = 22055,//炸金花扔鸡蛋请求
        GoldenFlowerEggRsp = 22056,//炸金花扔鸡蛋返回
        GoldenFlowerEggNtf = 22057,//炸金花扔鸡蛋通知
        GoldenFlowerOtherCloseRoomL2B = 22058,//炸金花房主代开房解散房间
        GoldenFlowerStartB2L = 22059,//开始游戏战斗服转发大厅服

        //运城贴金消息
        MJYunChengCreateRoomL2B = 12200, // 创建玩家在战斗服上
        MJYunChengJoinRoomL2B = 12201, // （运城贴金）加入房间
        MJYunChengLeaveRoomB2L = 12202, // 运城贴金离开房间
        MJYunChengGetRoomInfoL2B = 12203, // 运城贴金获取房间信息
        MJYunChengWinOneRoundB2L = 12204, // 运城贴金赢了一局
        MJYunChengAllRoleOfflineB2L = 12205, // (运城贴金）房间所有人都下线
        MJYunChengRoomCloseB2L = 12206, // 房间结束
        //运城贴金
        MJYunChengCreateRoomReq = 12000, // (运城贴金)创建房间
        MJYunChengCreateRoomRsp = 12001, // (运城贴金)创建房间返回
        MJYunChengJoinRoomReq = 12002, // (运城贴金)加入房间
        MJYunChengJoinRoomRsp = 12003, // (运城贴金)加入房间返回
        MJYunChengJoinRoomNtf = 12004, // (运城贴金)加入房间通知
        MJYunChengRoundStartNtf = 12005, // (运城贴金)回合开始通知
        MJYunChengHandOutCardReq = 12006, //(运城贴金)出牌请求
        MJYunChengHandOutCardRsp = 12007, // (运城贴金)出牌请求返回
        MJYunChengHandOutCardNtf = 12008, // (运城贴金)出牌通知
        MJYunChengGetNewCardNtf = 12009, // (运城贴金)起到新牌
        MJYunChengPengHuCardNtf = 12010, // (运城贴金)通知碰牌，杠牌，胡牌
        MJYunChengPengCardReq = 12011, // (运城贴金)碰牌请求
        MJYunChengPengCardRsp = 12012, // (运城贴金)碰牌请求返回
        MJYunChengPengCardNtf = 12013, // (运城贴金)碰牌通知
        MJYunChengHuCardReq = 12014, // (运城贴金)胡牌请求
        MJYunChengHuCardRsp = 12015, // (运城贴金)胡牌返回
        MJYunChengHuCardNtf = 12016, // (运城贴金)胡牌通知
        MJYunChengFlowRoundNtf = 12017, // (运城贴金)流局通知
        MJYunChengGangCardReq = 12018, // (运城贴金)杠牌请求
        MJYunChengGangCardRsp = 12019, // (运城贴金)杠牌请求返回
        MJYunChengGangCardNtf = 12020, // (运城贴金)杠牌通知
        MJYunChengReadyCardReq = 12021, // (运城贴金)听牌请求
        MJYunChengReadyCardRsp = 12022, // (运城贴金)听牌请求返回
        MJYunChengReadyCardNtf = 12023, // (运城贴金)听牌通知
        MJYunChengPassReq = 12024, // (运城贴金)过
        MJYunChengPassRsp = 12025, // (运城贴金)过
        MJYunChengGetRoomInfoReq = 12026, // (运城贴金)获取房间信息
        MJYunChengGetRoomInfoRsp = 12027, // (运城贴金)获取房间信息返回
        MJYunChengNeedHandCardNtf = 12028, // (运城贴金)轮到你出牌了
        MJYunChengNextStepReq = 12030, // (运城贴金)下一步
        MJYunChengNextStepRsp = 12031, // (运城贴金)下一步返回
        MJYunChengDismissGameReq = 12032, // (运城贴金)解散游戏
        MJYunChengDismissGameRsp = 12033, // (运城贴金)解散游戏返回 
        MJYunChengDismissGameNtf = 12034, // (运城贴金)解散通知
        MJYunChengDismissGameSuccessNtf = 12035, // (运城贴金)解散通过通知
        MJYunChengRoundSettleNtf = 12036, // (运城贴金)结算通知
        MJYunChengNextStepNtf = 12037, // (运城贴金)下一步通知
        MJYunChengLeaveRoomReq = 12038, // 房间未开始时离开房间请求
        MJYunChengLeaveRoomRsp = 12039, // 房间未开始时离开房间返回
        MJYunChengLeaveRoomNtf = 12040, // 房间未开始时离开房间通知
        MJYunChengDismissAgreeReq = 12041, // 解散是否同意请求
        MJYunChengDismissAgreeRsp = 12042, // 解散是否同意返回
        MJYunChengDismissAgreeNtf = 12043, // 解散是否同意通知
        MJYunChengNeedCheckSelfGangHuReadyNtf = 12044, // (运城贴金)请检测暗杠，补杠，自摸胡，听牌
        //MJYunChengCheckSelfGangHuReadyReq = 12045, // (运城贴金)请检测暗杠，补杠，自摸胡，听牌
        MJYunChengCheckSelfGangHuReadyRsp = 12056, // (运城贴金)请检测暗杠，补杠，自摸胡，听牌返回消息
        MJYunChengChatReq = 12047, // (运城贴金)聊天请求
        MJYunChengChatRsp = 12048, // (运城贴金)聊天请求返回
        MJYunChengChatNtf = 12049, // (运城贴金)聊天请求通知
        MJYunChengChatWordReq = 12050,//(运城贴金)文字聊天请求
        MJYunChengChatWordRsp = 12051,//(运城贴金)文字聊天返回
        MJYunChengChatWordNtf = 12052,//(运城贴金)文字聊天通知
        MJYunChengDistanceReq = 12053,//获取与另一个玩家之间的距离请求
        MJYunChengDistanceRsp = 12054,//获取与另一个玩家之间的距离返回
        MJYunChengEggReq = 12055,//运城扔鸡蛋请求
        MJYunChengEggRsp = 12060,//运城扔鸡蛋返回
        MJYunChengEggNtf = 12057,//运城扔鸡蛋通知
        MJYunChengOtherCloseRoomL2B = 12058,//运城麻将房主代开房解散房间

    }
}