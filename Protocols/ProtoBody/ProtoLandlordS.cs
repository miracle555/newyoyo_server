﻿using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    namespace Landlord
    {
        /// <summary>
        /// (斗地主）创建房间
        /// </summary>
        [ProtoContract]
        public class LandlordCreateRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(2)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// (斗地主）加入房间
        /// </summary>
        [ProtoContract]
        public class LandlordJoinRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (斗地主）加入房间
        /// </summary>
        [ProtoContract]
        public class LandlordGetRoomInfoL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (斗地主）离开房间
        /// </summary>
        [ProtoContract]
        public class LandlordLeaveRoomB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (斗地主）房间结束
        /// </summary>
        [ProtoContract]
        public class LandlordRoomCloseB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 是否归还房卡
            /// </summary>
            [ProtoMember(2)]
            public bool bReturnRoomCard = false;

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(3)]
            public UInt64 roomOwnerId = 0;

            /// <summary>
            /// 战绩
            /// </summary>
            [ProtoMember(4)]
            public stBattleScoreItem scoreItem = new stBattleScoreItem();

            /// <summary>
            /// 是否记录战绩
            /// </summary>
            [ProtoMember(5)]
            public bool bRecordBattleScore = false;
        };

        /// <summary>
        /// (斗地主)玩家赢了一局
        /// </summary>
        [ProtoContract]
        public class LandlordWinOneRoundB2L : ProtoBody
        {
        };

        /// <summary>
        /// (斗地主)房间所有人都下线了
        /// </summary>
        [ProtoContract]
        public class LandlordAllRoleOfflineB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// 房主建房解散房间
        /// </summary>
        [ProtoContract]
        public class LandlordOtherCloseRoomL2B : ProtoBody
        {
            /// <summary>
            /// 房间ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roomId;
        }
    }
}