﻿/********************************************************************
	created:	2014/12/11
	created:	11:12:2014   19:24
	filename: 	CommonPlatform\Server\ServerInstance\CommonLib\Protocols\ProtoScene.cs
	file path:	CommonPlatform\Server\ServerInstance\CommonLib\Protocols
	file base:	ProtoScene
	file ext:	cs
	author:		刘冰生
	
	purpose:    Protocol for Login Module 
*********************************************************************/
using System;
using System.Collections.Generic;
using ProtoBuf;
using playerid = System.UInt64;

namespace Protocols
{
    /// <summary>
    /// 平台id定义
    /// </summary>
    public enum EPID : ushort
    {
        Internal = 0,        //内部平台   
        Weixin = 1,          //微信
    }

    /// <summary>
    /// 操作系统类别
    /// </summary>
    public enum EDeviceOS
    {
        None,
        PC,
        MAC,
        Android,
        IOS,
        WP
    }

    /// <summary>
    /// 玩家注册
    /// </summary>
    [ProtoContract]
    public class UserRegisterReq : ProtoBody
    {
        [ProtoMember(1)]
        public string account = "";

        [ProtoMember(2)]
        public string password = "";
    }

    /// <summary>
    /// 玩家注册返回
    /// </summary>
    [ProtoContract]
    public class UserRegisterRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 该账号已经存在
            /// </summary>
            AlreadyExist = 2,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;
    }

    /// <summary>
    /// 玩家登陆
    /// </summary>
    [ProtoContract]
    public class UserLoginReq : ProtoBody
    {
        [ProtoMember(1)]
        public EPID platformType = EPID.Internal;

        [ProtoMember(2)]
        public string account = "";// 自有平台账号

        [ProtoMember(3)]
        public string password = "";// 自有平台密码

        [ProtoMember(4)]
        public string weixinCode = ""; // 微信临时票据code

        [ProtoMember(5)]
        public string weixinInnerToken = ""; // 微信登录临时token

        /// <summary>
        /// 使用accountId登录
        /// </summary>
        [ProtoMember(6)]
        public UInt64 accountId = 0;
    }

    /// <summary>
    /// 玩家登陆
    /// </summary>
    [ProtoContract]
    public class UserLoginRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 密码错误
            /// </summary>
            PassWord_Wrong = 2,

            /// <summary>
            /// 有相同账号在登录，该账号正在其他设备上登录
            /// </summary>
            AccountDuplicateLogin = 3,

            /// <summary>
            /// 传上来的微信临时票据code错误
            /// </summary>
            weixinCodeWrong = 4,

            /// <summary>
            /// 微信token登录
            /// </summary>
            weixinInnerTokenWrong = 5,

            /// <summary>
            /// 账号不存在
            /// </summary>
            AccountNotExist = 6,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        [ProtoMember(2)]
        public UInt64 accountid = 0;

        /// <summary>
        /// gate重连时的内部token
        /// </summary>
        [ProtoMember(3)]
        public string gateInnerToken = "";

        /// <summary>
        /// 微信登录临时token
        /// </summary>
        [ProtoMember(4)]
        public string weixinInnerToken = "";
    }

    /// <summary>
    /// 断线重连
    /// </summary>
    [ProtoContract]
    public class ReloginReq : ProtoBody
    {
        [ProtoMember(1)]
        public UInt64 accountid = 0;

        [ProtoMember(2)]
        public string innerToken = "";
    }

    /// <summary>
    /// 断线重连
    /// </summary>
    [ProtoContract]
    public class ReloginRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 内部token过期，重登录时使用
            /// </summary>
            InnerToken_Expire = 2,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;
    }

    [ProtoContract]
    public class KickAccountReq : ProtoBody
    {
        [ProtoMember(1)]
        public EPID platform;

        [ProtoMember(2)]
        public string account;

        [ProtoMember(3)]
        public KickReason reason;
    }

    [ProtoContract]
    public class KickAccountRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;
    }

    /// <summary>
    /// 获取服务器时间
    /// </summary>
    [ProtoContract]
    public class GetServerTimeReq : ProtoBody
    {
        [ProtoMember(1)]
        public double clientTime = 0;
    }

    /// <summary>
    /// 获取服务器时间
    /// </summary>
    [ProtoContract]
    public class GetServerTimeRsp : ProtoBody
    {
        [ProtoMember(1)]
        public double serverTime = 0;
    }

    /// <summary>
    /// 获取服务器配置消息
    /// </summary>
    [ProtoContract]
    public class GetServerConfigReq : ProtoBody
    {
    }

    /// <summary>
    /// 获取服务器配置消息
    /// </summary>
    [ProtoContract]
    public class GetServerConfigRsp : ProtoBody
    {
        /// <summary>
        /// 心跳间隔毫秒数
        /// </summary>
        [ProtoMember(1)]
        public int heartMillisecond = 0;

        /// <summary>
        /// 创建房间消耗房卡，顺序，推倒胡，扣点，斗地主
        /// </summary>
        [ProtoMember(2)]
        public List<UInt32> lstCreateRoomCards = new List<UInt32>();
    }

    /// <summary>
    /// 获取玩家经纬度
    /// </summary>
    [ProtoContract]
    public class UserLocationInfoReq : ProtoBody
    {
        /// <summary>
        /// 经度
        /// </summary>
        [ProtoMember(1)]
        public float longitude;

        /// <summary>
        /// 纬度
        /// </summary>
        [ProtoMember(2)]
        public float latitude;

        /// <summary>
        /// 是否开启GPS
        /// </summary>
        [ProtoMember(3)]
        public bool isOpenGps;
    }

    /// <summary>
    /// 获取玩家经纬度返回
    /// </summary>
    [ProtoContract]
    public class UserLocationInfoRsp:ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 玩家经纬度所定位的地址
        /// </summary>
        [ProtoMember(2)]
        public String adress;
    }
}