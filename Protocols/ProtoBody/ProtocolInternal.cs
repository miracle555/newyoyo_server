﻿/********************************************************************
	created:	2014/12/01
	created:	1:12:2014   15:37
	author:		刘冰生
	
	purpose:    服务器框架用协议体定义	
*********************************************************************/
using System;
using System.Collections.Generic;
using ProtoBuf;
using playerid = System.UInt64;

namespace Protocols
{
    //////////////////////////////////////////////////////////////////////////

    /// <summary>
    /// 服务器注册
    /// </summary>
    [ProtoContract]
    public class ServerRegisterReq : ProtoBody
    {
    }

    /// <summary>
    /// 服务器注册
    /// </summary>
    [ProtoContract]
    public class ServerRegisterRsp : ProtoBody
    {
        /// <summary>
        /// 服务器ID
        /// </summary>
        [ProtoMember(1)]
        public UInt32 serverID;
    }

    [ProtoContract]
    public class ServerProtoItem
    {
        /// <summary>
        /// 服务器ID
        /// </summary>
        [ProtoMember(1)]
        public string addr = "";

        [ProtoMember(2)]
        public string ip = "";

        [ProtoMember(3)]
        public string port = "";
    }

    /// <summary>
    /// 服务器列表
    /// </summary>
    [ProtoContract]
    public class ServerListNtf : ProtoBody
    {
        /// <summary>
        /// 服务器ID
        /// </summary>
        [ProtoMember(1)]
        public List<ServerProtoItem> lstItems = new List<ServerProtoItem>();
    }

    /// <summary>
    /// 服务器列表
    /// </summary>
    [ProtoContract]
    public class ServerRegisterSelfReq : ProtoBody
    {
        /// <summary>
        /// 服务器ID
        /// </summary>
        [ProtoMember(1)]
        public UInt32 serverID;
    }

    //////////////////////////////////////////////////////////////////////////
    // 0x0101 ~ 0x01FF Player管理器用 

    /// <summary>
    /// 玩家上线通知
    /// </summary>
    [ProtoContract]
    public class PlayerOnlineNtf : ProtoBody
    {
        [ProtoMember(1)]
        public string clientIp = "";
    }

    /// <summary>
    /// 玩家下线通知
    /// </summary>
    [ProtoContract]
    public class PlayerOfflineNtf : ProtoBody
    {
    }

    /// <summary>
    /// 
    /// </summary>
    [ProtoContract]
    public class PlayerHoldingOrStandByNtf : ProtoBody
    {
        /// <summary>
        /// 账号
        /// </summary>
        [ProtoMember(1)]
        public string account;

        /// <summary>
        /// 
        /// </summary>
        [ProtoMember(2)]
        public EPID platform;

        /// <summary>
        /// 玩家角色ID
        /// </summary>
        [ProtoMember(3)]
        public UInt64 accountId;

        /// <summary>
        /// 玩家登录Gate地址
        /// </summary>
        [ProtoMember(4)]
        public UInt32 gateAddress;

        /// <summary>
        /// 玩家Gate上的SessionID
        /// </summary>
        [ProtoMember(5)]
        public UInt32 sessionId;
    }

    /// <summary>
    /// 
    /// </summary>
    [ProtoContract]
    public class PlayerHoldingToGamingNtf : ProtoBody
    {
        /// <summary>
        /// 账号
        /// </summary>
        [ProtoMember(1)]
        public string account;

        /// <summary>
        /// 
        /// </summary>
        [ProtoMember(2)]
        public EPID platform;

        /// <summary>
        /// 玩家角色ID
        /// </summary>
        [ProtoMember(3)]
        public UInt64 accountId;

        /// <summary>
        /// 玩家登录Gate地址
        /// </summary>
        [ProtoMember(4)]
        public UInt32 gateAddress;

        /// <summary>
        /// 玩家Gate上的SessionID
        /// </summary>
        [ProtoMember(5)]
        public UInt32 sessionId;
    }

    /// <summary>
    /// 玩家名字改变
    /// </summary>
    [ProtoContract]
    public class PlayerNameUpdateNtf : ProtoBody
    {
        /// <summary>
        /// 玩家角色ID
        /// </summary>
        [ProtoMember(1)]
        public UInt64 accountId;

        /// <summary>
        /// 玩家名称
        /// </summary>
        [ProtoMember(2)]
        public string name;
    }

    //////////////////////////////////////////////////////////////////////////
    // 0x0401 ~ 0x04FF PING模块使用 

    /// <summary>
    /// server ping
    /// </summary>
    [ProtoContract]
    public class ServerPing : ProtoBody
    {
        /// <summary>
        /// ping mode 
        /// </summary>
        [ProtoMember(1)]
        public byte mode;

        /// <summary>
        /// ping message 
        /// </summary>
        [ProtoMember(2)]
        public string echo;

    }

    /// <summary>
    /// server pong
    /// </summary>
    [ProtoContract]
    public class ServerPong : ProtoBody
    {
        /// <summary>
        /// identify whitch server pong 
        /// </summary>
        [ProtoMember(1)]
        public UInt32 from;

        /// <summary>
        /// pong message 
        /// </summary>
        [ProtoMember(2)]
        public string echo;
    }

    [ProtoContract]
    public class ServerLog : ProtoBody
    {
        [ProtoMember(1)]
        public int gameId;

        [ProtoMember(2)]
        public ushort zoneId;

        [ProtoMember(3)]
        public ushort logicDbType;

        [ProtoMember(4)]
        public int level;

        [ProtoMember(5)]
        public string module;

        [ProtoMember(6)]
        public string message;

        [ProtoMember(7)]
        public DateTime t;
    }

    [ProtoContract]
    public class ServerPerformance : ProtoBody
    {
        [ProtoMember(1)]
        public double cpuUseRate;
        [ProtoMember(2)]
        public long memory;
        [ProtoMember(3)]
        public long serverSumSendPacket;
        [ProtoMember(4)]
        public long serverSumSendBytes;
        [ProtoMember(5)]
        public long serverSumReceivePacket;
        [ProtoMember(6)]
        public long serverSumReceiveBytes;
        [ProtoMember(7)]
        public long serverSecondsSendPacket;
        [ProtoMember(8)]
        public long serverSecondsSendBytes;
        [ProtoMember(9)]
        public long serverSecondsReceivePacket;
        [ProtoMember(10)]
        public long serverSecondsReceiveBytes;
        [ProtoMember(11)]
        public Dictionary<string, Int64> serverSendPacketSortMap = new Dictionary<string, long>();
        [ProtoMember(12)]
        public Dictionary<string, Int64> serverSendBytesSortMap = new Dictionary<string, long>();
        [ProtoMember(13)]
        public Dictionary<string, Int64> serverReceivePacketSortMap = new Dictionary<string, long>();
        [ProtoMember(14)]
        public Dictionary<string, Int64> serverReceiveBytesSortMap = new Dictionary<string, long>();
        [ProtoMember(15)]
        public Dictionary<string, Int64> scriptMap = new Dictionary<string, Int64>();
    }
}
