﻿using ProtoBuf;
using Protocols.GoldenFlower;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Protocols
{
    namespace GoldenFlower
    {
        /// <summary>
        /// 房间状态
        /// </summary>
        public enum ERoomState
        {
            Null = 0,
            WaitRoomer = 1, // 等待其他人
            Play = 2, // 正在玩
            WaitRound = 3, // 等待下一局
        }

        /// <summary>
        /// 玩家状态
        /// </summary>
        public enum ERoomRoleState
        {
            Null = 0,
            WaitRoomer = 1, // 等待其他人
            Play = 2, // 正在玩
            Settle = 3, // 结算状态
            WaitRound = 4, // 等待下一局
            Dismiss = 5, // 解散状态
        }
    
        /// <summary>
        /// 操作状态
        /// </summary>
        public enum ERolePlayState
        {
            Null = 0,
            Wait = 1, // 等待其他人操作
            Play = 2,  //自己操作
            NextGame = 3,   //下局游戏
        }

        ///玩家在房间内的状态
        ///
        public enum ERoomPlayerState
        {
            Null = 0,
            NoShowCard = 1,//不看牌 
            ShowCard = 2, // 看牌
            Defeated = 3,//比牌失败
            CallGoldFlower = 4,//弃牌
        }

        /// <summary>
        /// 扑克牌ID
        /// </summary>
        public enum ECardId
        {


            Null = -1,

            Black2 = 0, // 黑桃2
            Black3 = 1, // 黑桃3
            Black4 = 2, // 黑桃4
            Black5 = 3, // 黑桃5
            Black6 = 4, // 黑桃6
            Black7 = 5, // 黑桃7
            Black8 = 6, // 黑桃8
            Black9 = 7, // 黑桃9
            Black10 = 8, // 黑桃10
            BlackJ = 9, // 黑桃11
            BlackQ = 10, // 黑桃12
            BlackK = 11, // 黑桃13 
            BlackA = 12, // 黑桃A
            Red2 = 13, // 红桃2
            Red3 = 14, // 红桃3
            Red4 = 15, // 红桃4
            Red5 = 16, // 红桃5
            Red6 = 17, // 红桃6
            Red7 = 18, // 红桃7
            Red8 = 19, // 红桃8
            Red9 = 20, // 红桃9
            Red10 = 21, // 红桃10
            RedJ = 22, // 红桃11
            RedQ = 23, // 红桃12
            RedK = 24, // 红桃13 
            RedA = 25, // 红桃A
            Flower2 = 26, // 梅花2
            Flower3 = 27, // 梅花3
            Flower4 = 28, // 梅花4
            Flower5 = 29, // 梅花5
            Flower6 = 30, // 梅花6
            Flower7 = 31, // 梅花7
            Flower8 = 32, // 梅花8
            Flower9 = 33, // 梅花9
            Flower10 = 34, // 梅花10
            FlowerJ = 35, // 梅花11
            FlowerQ = 36, // 梅花12
            FlowerK = 37, // 梅花13 
            FlowerA = 38, // 梅花A
            Cube2 = 39, // 方片2
            Cube3 = 40, // 方片3
            Cube4 = 41, // 方片4
            Cube5 = 42, // 方片5
            Cube6 = 43, // 方片6
            Cube7 = 44, // 方片7
            Cube8 = 45, // 方片8
            Cube9 = 46, // 方片9
            Cube10 = 47, // 方片10
            CubeJ = 48, // 方片11
            CubeQ = 49, // 方片12
            CubeK = 50, // 方片13 
            CubeA = 51, // 方片A         

            Max = 52,
        }

        public enum ESeatPostion
        {
            Null = 0, // 默认 
            First = 1, // 第一位
            Second = 2, // 第二位
            Third = 3, // 第三位
            Fourth = 4,//第四位
            Fifth = 5,//第五位
        }

        /// <summary>
        /// 牌形
        /// </summary>
        public enum ECardFormType
        {
            Null = 0, // 错误牌型
            Hight = 1, // 高牌  
            Pair = 2, // 对子 
            OttStraight=3,//123顺子
            Straight = 4, // 顺子
            Golden = 5, // 金花  
            StraightGolden = 6, // 金花顺 
            Leopard = 7, //  豹子 
            Twothreefive=8,//特殊牌型235
        }

        /// <summary>
        /// 角色基本信息
        /// </summary>
        [ProtoContract]
        public class RemoteRoleBaseInfo
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 角色名
            /// </summary>
            [ProtoMember(2)]
            public string nickName = "";

            /// <summary>
            /// 性别
            /// </summary>
            [ProtoMember(3)]
            public ERoleSex sex = ERoleSex.Boy;

            /// <summary>
            /// 等级
            /// </summary>
            [ProtoMember(4)]
            public UInt32 level = 0;

            /// <summary>
            /// 该角色的ip
            /// </summary>
            [ProtoMember(5)]
            public string clientIp = "";

            /// <summary>
            /// 微信头像url
            /// </summary>
            [ProtoMember(6)]
            public string weixinHeadImgUrl = "";
        }

        /// <summary>
        /// 扎金花其他角色
        /// </summary>
        [ProtoContract]
        public class RemoteRoleOtherInfo
        {

            /// <summary>
            /// 是否在线
            /// </summary>
            [ProtoMember(1)]
            public bool isOnline = false;

            // 本房间分数
            [ProtoMember(2)]
            public int score = 0;


            /// <summary>
            /// 玩家状态
            /// </summary>
            [ProtoMember(3)]
            public ERoomRoleState roleState = ERoomRoleState.Null;

            /// <summary>
            /// 操作状态
            /// </summary>
            [ProtoMember(4)]
            public ERolePlayState playState = ERolePlayState.Null;

            /// <summary>
            /// 牌型
            /// </summary>
            [ProtoMember(5)]
            public ECardFormType cardType = ECardFormType.Null;

            /// <summary>
            /// 手牌
            /// </summary>
            [ProtoMember(8)]
            public List<UInt32> handCards = new List<UInt32>();

            /// <summary>
            /// 方位
            /// </summary>
            [ProtoMember(9)]
            public ESeatPostion postion = ESeatPostion.Null;

            ///押注
            ///
            [ProtoMember(10)]
            public int betOn = 0;

            /// <summary>
            /// 当前状态
            /// </summary>
            [ProtoMember(11)]
            public ERoomPlayerState currentState= ERoomPlayerState.Null;

            /// <summary>
            /// 当前准备状态
            /// </summary>
            [ProtoMember(12)]
            public bool prepare = false;

        }

        /// 其他角色信息
        /// </summary>
        [ProtoContract]
        public class RemoteRoleInfo
        {
            /// <summary>
            /// 基础信息
            /// </summary>
            [ProtoMember(1)]
            public RemoteRoleBaseInfo baseInfo = new RemoteRoleBaseInfo();

            /// <summary>
            /// 其他信息
            /// </summary>
            [ProtoMember(2)]
            public RemoteRoleOtherInfo otherInfo = new RemoteRoleOtherInfo();


        }




        /// <summary>
        /// 扎金花本人角色信息
        /// </summary>
        [ProtoContract]
        public class LocalRoleInfo
        {

            // 本房间分数
            [ProtoMember(1)]
            public int score = 0;

            /// <summary>
            /// 玩家状态
            /// </summary>
            [ProtoMember(2)]
            public ERoomRoleState roleState = ERoomRoleState.Null;

            /// <summary>
            /// 操作状态
            /// </summary>
            [ProtoMember(3)]
            public ERolePlayState playState = ERolePlayState.Null;

            /// <summary>
            /// 牌型
            /// </summary>
            [ProtoMember(4)]
            public ECardFormType cardType = ECardFormType.Null;

            ///押注
            ///
            [ProtoMember(5)]
            public int betOn =0;
            /// <summary>
            /// 手牌
            /// </summary>
            [ProtoMember(8)]
            public List<UInt32> handCards = new List<UInt32>();
            /// <summary>
            /// 方位
            /// </summary>
            [ProtoMember(9)]
            public ESeatPostion position = ESeatPostion.Null;

            /// <summary>
            /// 剩余时间
            /// </summary>
            [ProtoMember(10)]
            public int remainSecond = 0;

            ///押注次数
            ///
            [ProtoMember(11)]
            public int betOnNumber = 0;

            /// <summary>
            /// 当前状态
            /// </summary>
            [ProtoMember(12)]
            public ERoomPlayerState currentState = ERoomPlayerState.Null;

            /// <summary>
            /// 当前准备状态
            /// </summary>
            [ProtoMember(13)]
            public bool prepare = false;


        }

        /// <summary>
        /// 房间局数
        /// </summary>
        public enum ERoomRound
        {
            Null = 0,
            EightRound = 3, // 8局 3
            SixteenRound = 5,// 16局 5
            TwentyfourRound = 7, // 24局 7
        }

        /// <summary>
        /// 彩分
        /// </summary>
        public enum Pari
        {
            Null = 0,
            Five=5,//5分
            Ten=10,//10分
        }

        /// <summary>
        /// 下注轮数限制
        /// </summary>
        /// 
        public enum Rounds
        {
            Null=0,
            Twenty=20,//20轮
            forty=40,//40轮
        }

        /// <summary>
        /// 倍数选择
        /// </summary>
        public enum Multiple
        {
            Null=0,
            One=1,//1分
            Two=2,//2分
            Three=3,//3分
            Four=4,//4分
        }



        [ProtoContract]
        public class RoomConfig
        {
            /// <summary>
            /// 选择局数
            /// </summary>
            [ProtoMember(1)]
            public ERoomRound roomRound = ERoomRound.Null;

            ///彩分
            ///
            [ProtoMember(2)]
            public Pari pariMutuel = Pari.Null;

            ///下注次数限制
            ///
            [ProtoMember(3)]
            public Rounds roundNumber  = Rounds.Null;

            ///选择倍数
            [ProtoMember(4)]
            public Multiple fold = Multiple.Null;
            /// <summary>
            /// 扎金花扣1张房卡
            /// </summary>
            public UInt32 roomCardCount = 1;

            /// <summary>
            /// 付款方式
            /// </summary>
            [ProtoMember(5)]
            public PayMethod payMethod = PayMethod.masterPay;


            /// <summary>
            /// 局数
            /// </summary>
            public int roomRoundCount
            {
                get
                {
                    if (roomRound == ERoomRound.EightRound)
                    {
                        return 8;
                    }
                    else if (roomRound == ERoomRound.SixteenRound)
                    {
                        return 16;
                    }
                    else if (roomRound == ERoomRound.TwentyfourRound)
                    {
                        return 24;
                    }
                    else
                    {
                        return 8;
                    }
                }
            }
        }

        /// <summary>
        /// 付款方式
        /// </summary>
        [ProtoContract]
        public enum PayMethod
        {
            masterPay = 1,//房主付
            AAPay = 2,//AA制
            anotherPay = 3,//房主代开房
        }

        /// <summary>
        /// 出牌信息
        /// </summary>
        [ProtoContract]
        public class stOutCardItem
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            [ProtoMember(2)]
            public List<UInt32> lstCards = new List<UInt32>();

            public void Clear()
            {
                roleid = 0;
                lstCards.Clear();
            }

            public void RecordOutCard(UInt64 roleid, List<UInt32> lstCards)
            {
                this.roleid = roleid;
                this.lstCards = lstCards;
            }
        }


        /// <summary>
        /// 房间信息
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerRoomInfo
        {
            /// <summary>
            /// 房间唯一ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间状态
            /// </summary>
            [ProtoMember(2)]
            public ERoomState roomState = ERoomState.WaitRoomer;

            /// <summary>
            /// 本人信息
            /// </summary>
            [ProtoMember(3)]
            public LocalRoleInfo localRoleInfo = new LocalRoleInfo();

            /// <summary>
            /// 房间其他玩家信息
            /// </summary>
            [ProtoMember(4)]
            public List<RemoteRoleInfo> lstRoles = new List<RemoteRoleInfo>();

            /// <summary>
            /// 第几牌局
            /// </summary>
            [ProtoMember(5)]
            public UInt32 currentCardRound = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(6)]
            public RoomConfig config = new RoomConfig();

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(7)]
            public stRoomOwner roomOwner = new stRoomOwner();

            /// <summary>
            /// 上次出牌信息
            /// </summary>
            [ProtoMember(8)]
            public stOutCardItem lastOutCardItem = new stOutCardItem();

            /// <summary>
            /// 解散信息
            /// </summary>
            [ProtoMember(9)]
            public stDismissInfo dismissInfo = new stDismissInfo();
          
            /// <summary>
            /// 筹码列表
            /// </summary>
            [ProtoMember(10)]
            public List<int> counterList = new List<int>();

            ///房间总分
            ///
            [ProtoMember(11)]
            public int roomFen = 0;

            ///伪房主
            ///
            [ProtoMember(12)]
            public UInt64 roomPlayID = 0;
        };

        /// <summary>
        /// (扎金花)创建房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerCreateRoomReq : ProtoBody
        {
            [ProtoMember(1)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// (扎金花)创建房间返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerCreateRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 已经创建房间
                /// </summary>
                AlreadyCreateRoom = 2,

                /// <summary>
                /// 已经在其他房间里
                /// </summary>
                AlreadInRoom = 3,

                /// <summary>
                /// 房卡数量不足
                /// </summary>
                RoomCardNotEnough = 4,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public GoldenFlowerRoomInfo roomInfo = new GoldenFlowerRoomInfo();
        };


        /// <summary>
        /// (扎金花)加入房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerJoinRoomReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (扎金花)加入房间返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerJoinRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 房间不存在
                /// </summary>
                RoomNotExist = 2,

                /// <summary>
                /// 房间已满
                /// </summary>

                RoomFull = 3,

                /// <summary>
                /// 房卡数量不足
                /// </summary>
                RoomCardNotEnough = 4,

                /// <summary>
                /// 游戏已经开始
                /// </summary>
                HasStart = 5,

                /// <summary>
                /// 
                /// </summary>
                AnthorNotJoin = 6,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public GoldenFlowerRoomInfo roomInfo = new GoldenFlowerRoomInfo();
        };

        /// <summary>
        /// (扎金花)加入房间通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerJoinRoomNtf : ProtoBody
        {
            /// <summary>
            /// 人物信息
            /// </summary>
            [ProtoMember(1)]
            public RemoteRoleInfo remoteRoleInfo = new RemoteRoleInfo();

            ///伪房主
            ///
            [ProtoMember(2)]
            public UInt64 roomPlayerId = 0;


        };

        /// <summary>
        /// (扎金花)回合开始通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerRoundStartNtf : ProtoBody
        {
            /// <summary>
            /// 可以操作的玩家id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 playerId = 0;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public GoldenFlowerRoomInfo roomInfo = new GoldenFlowerRoomInfo();
        };



        /// <summary>
        /// (扎金花)获取房间信息
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerGetRoomInfoReq : ProtoBody
        {
        };

        /// <summary>
        /// (扎金花)获取房间信息返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerGetRoomInfoRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public GoldenFlowerRoomInfo roomInfo = new GoldenFlowerRoomInfo();
        };

        /// <summary>
        /// (扎金花)看牌请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerShowCardReq : ProtoBody
        {
            ///玩家id
            ///
            [ProtoMember(1)]
            public UInt64 playId;
        };

        /// <summary>
        /// (扎金花)看牌请求返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerShowCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
          
        };

        ///扎金花 看牌通知
        ///
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerShowCardNtf : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            ///玩家id
            ///
            [ProtoMember(2)]
            public UInt64 playId;
        };



        /// <summary>
        /// (扎金花)加注请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerRaiseCardReq : ProtoBody
        {

            ///加注分数
            ///
            [ProtoMember(3)]
            public int fen = 0;
        };

        ///加注请求返回
        ///
        [ProtoContract]
        public class GoldenFlowerRaiseCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
            /// <summary>
            ///加注分数
            /// </summary>
            [ProtoMember(2)]
            public int fen=0;
       
        };

        ///加注通知
        ///

        [ProtoContract]
        public class GoldenFlowerRaiseCardNtf : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            ///玩家id
            ///
            [ProtoMember(2)]
            public UInt64 playId;

            /// <summary>
            ///加注分数
            /// </summary>
            [ProtoMember(3)]
            public int fen = 0;

        };





        /// <summary>
        /// (扎金花)弃牌请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerCallCardReq: ProtoBody
        {
            ///玩家id
            ///
            [ProtoMember(1)]
            public UInt64 playId;


        };
        ///扎金花弃牌请求返回
        ///       
        [ProtoContract]
        public class GoldenFlowerCallCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        ///扎金花 弃牌通知
        ///
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerCallCardNtf : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            ///玩家id
            ///
            [ProtoMember(2)]
            public UInt64 playId;
            
        };




        /// <summary>
        /// (扎金花)比牌请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerHandOutCardReq : ProtoBody
        {
           
            /// <summary>
            ///目标玩家ID
            /// </summary>
            [ProtoMember(2)]
            public UInt64 otherId = 0;


        };

        /// <summary>
        /// (扎金花)比牌请求返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerHandOutCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;


            [ProtoMember(2)]
            public bool iswin;

            [ProtoMember(3)]

            public UInt64 otherId;   


        };


        ///扎金花 比牌通知
        ///
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerHandOutCardNtf : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            ///玩家id
            ///
            [ProtoMember(2)]
            public UInt64 playId;

            ///其他玩家
            ///
            [ProtoMember(3)]
            public UInt64 otherId;


            ///WIn玩家
            ///
            [ProtoMember(4)]
            public UInt64 winId;

        };

        /// <summary>
        /// (扎金花)解散游戏
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDismissGameReq : ProtoBody
        {
        };

        /// <summary>
        /// (扎金花)解散游戏返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDismissGameRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 解散倒计时
            /// </summary>
            [ProtoMember(2)]
            public int remainSecond = 0;

        };

        /// <summary>
        /// (扎金花)解散通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDismissGameNtf : ProtoBody
        {
            /// <summary>
            /// 点击解散游戏的人
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 解散倒计时
            /// </summary>
            [ProtoMember(2)]
            public int remainSecond = 0;
        };

        /// <summary>
        /// (扎金花)解散通过通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDismissGameSuccessNtf : ProtoBody
        {
            [ProtoMember(1)]
            public bool bSuccess = false;

            /// <summary>
            /// 最终结算信息
            /// </summary>
            [ProtoMember(2)]
            public stFinalSettle finalSettle = new stFinalSettle();
        };

        /// <summary>
        /// (扎金花)解散是否同意
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDismissAgreeReq : ProtoBody
        {
            [ProtoMember(1)]
            public bool bAgree = false;
        };

        /// <summary>
        /// (扎金花)解散是否同意返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDismissAgreeRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (扎金花)解散是否同意通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDismissAgreeNtf : ProtoBody
        {
            /// <summary>
            /// 角色id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 是否同意解散房间
            /// </summary>
            [ProtoMember(2)]
            public bool bAgree = false;
        };

        /// <summary>
        /// (扎金花)未开始时退出房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerLeaveRoomReq : ProtoBody
        {
        };

        /// <summary>
        /// (扎金花)离开房间返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerLeaveRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (扎金花)离开房间通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerLeaveRoomNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            [ProtoMember(2)]
            public bool bRoomOwner = false;

            [ProtoMember(3)]
            public UInt64 roomPlayID = 0;
        };

        /// <summary>
        /// (扎金花)锅底变化通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerTotalScalesChangeNtf : ProtoBody
        {
            /// <summary>
            /// 锅底分
            /// </summary>
            [ProtoMember(1)]
            public int totalScales = 0;
            ///玩家id
            [ProtoMember(2)]
            public UInt64 playerId;
            ///玩家本房间分数
            ///
            [ProtoMember(3)]
            public int roomFen=0 ;


        };

        /// <summary>
        /// 锅底分
        /// </summary>
        [ProtoContract]
        public class stPublicScales
        {
            /// <summary>
            /// 锅底分
            /// </summary>
            [ProtoMember(1)]
            public int Scales = 0;
           

            public void Clear()
            {
                Scales = 0;
               
            }

            public int totalScales
            {
                get
                {
                    return Scales;
                }
            }           
        }

        /// <summary>
        /// 最终结算
        /// </summary>
        [ProtoContract]
        public class stFinalSettle
        {
            /// <summary>
            /// 各个玩家的最终结算分数
            /// </summary>
            [ProtoMember(1)]
            public Dictionary<UInt64, int> mapScores = new Dictionary<UInt64, int>();

            public void ChangeScore(UInt64 roleid, int score)
            {
                if (mapScores.ContainsKey(roleid))
                {
                    mapScores[roleid] += score;
                }
                else
                {
                    mapScores.Add(roleid, score);
                }
            }

            public void InitScore(UInt64 roleid)
            {
                if (!mapScores.ContainsKey(roleid))
                {
                    mapScores.Add(roleid, 0);
                }
            }
        }

        /// <summary>
        /// 结算角色信息
        /// </summary>
        [ProtoContract]
        public class RoundSettleRole
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 昵称
            /// </summary>
            [ProtoMember(2)]
            public string nickName = "";
          
            /// <summary>
            /// 本局最终得分
            /// </summary>
            [ProtoMember(4)]
            public int roundScore = 0;

          
        }

        /// <summary>
        /// 结算信息
        /// </summary>
        [ProtoContract]
        public class RoundSettleInfo
        {
            /// <summary>
            /// 结算角色信息列表
            /// </summary>
            [ProtoMember(1)]
            public List<RoundSettleRole> lstSettleRoles = new List<RoundSettleRole>();

            /// <summary>
            /// 公共倍数
            /// </summary>
            [ProtoMember(2)]
            public stPublicScales publicScales = new stPublicScales();

            /// <summary>
            /// 赢牌角色ID
            /// </summary>
            [ProtoMember(3)]
            public UInt64 winRoleId = 0;
        }

        /// <summary>
        /// (扎金花)聊天请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerChatReq : ProtoBody
        {
            /// <summary>
            /// 聊天ID
            /// </summary>
            [ProtoMember(1)]
            public UInt32 chatid = 0;
        };

        /// <summary>
        /// (扎金花)聊天请求返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerChatRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (扎金花)聊天请求通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerChatNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 聊天ID
            /// </summary>
            [ProtoMember(2)]
            public UInt32 chatid = 0;
        };

        /// <summary>
        /// 用户聊天输入文字请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerChatWordReq : ProtoBody
        {
            [ProtoMember(1)]
            public string chatWord = "";
        }

        /// <summary>
        /// 用户聊天输入文字返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerChatWordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 用户聊天输入文字通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerChatWordNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleId = 0;

            /// <summary>
            /// 用户输入的文字
            /// </summary>
            [ProtoMember(2)]
            public string chatWord = "";
        }

        ///下一步
        ///
        [ProtoContract]
        public class GoldenFlowerNextStepReq : ProtoBody
        {
            
        }
        ///下一步返回
        ///
        [ProtoContract]
        public class GoldenFlowerNextStepRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }
        ///下一步广播
        [ProtoContract]
        public class GoldenFlowerNextStepNtf : ProtoBody
        {

            [ProtoMember(1)]
            public UInt64 playID = 0;

        }




        ///彩分通知
        ///
        [ProtoContract]
        public class GoldenFlowerPariMutuelNtf : ProtoBody
        {
            [ProtoMember(1)]
            public bool zha = false;
            //输家玩家分
            [ProtoMember(2)]
            public List<int> fen = new List<int>();
            //输家玩家ID
            [ProtoMember(3)]
            public List<UInt64> playerID = new List<UInt64>();


        }

        ///开始游戏通知
        ///      
        [ProtoContract]
        public class GoldenFlowerStartReq : ProtoBody
        {

        }

        ///开始游戏通知返回
        [ProtoContract]
        public class GoldenFlowerStartRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// (扎金花)跟注请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDarkCardReq : ProtoBody
        {
        };

        ///跟注请求返回
        ///
        [ProtoContract]
        public class GoldenFlowerDarkCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            ///加注分数
            /// </summary>
            [ProtoMember(2)]
            public int fen = 0;

        };

        ///扎金花 跟注通知
        ///
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDarkCardNtf : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            ///玩家id
            ///
            [ProtoMember(2)]
            public UInt64 playId;


            /// <summary>
            ///加注分数
            /// </summary>
            [ProtoMember(3)]
            public int fen = 0;


        };

        /// <summary>
        /// (扎金花)开始底分请求
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerStartFenReq : ProtoBody
        {
        };
        /// <summary>
        /// (扎金花)开始底分返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerStartFenRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            ///底分数
            /// </summary>
            [ProtoMember(2)]
            public int fen = 0;


        };
        /// <summary>
        /// (扎金花)开始底分通知
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerStartFenNtf : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            ///玩家id
            ///
            [ProtoMember(2)]
            public UInt64 playId;


            /// <summary>
            ///玩家底分数
            /// </summary>
            [ProtoMember(3)]
            public int fen = 0;

            /// <summary>
            //房间总分数
            /// </summary>
            [ProtoMember(4)]
            public int zFen = 0;

        };





        //检测最后一位玩家赢      
        [ProtoContract]
        public class GoldenFlowerWinNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64  playID = 0;

            [ProtoMember(2)]
            public bool finallyInning = false;
        }

        //结算广播
        [ProtoContract]
        public class GoldenFlowerRoundSettleNtf :ProtoBody
        {
            [ProtoMember(1)]
            public List<RemoteRoleBaseInfo>  player=new List<RemoteRoleBaseInfo>();

            [ProtoMember(2)]
            public List<int>  fen = new List<int>();

            [ProtoMember(3)]
            public stFinalSettle settle = new stFinalSettle();
        }

       
        //下一位玩家操作广播
        [ProtoContract]
        public class GoldenFlowerNeedHandOutCardNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 nextPlayerID;    
        }

        /// <summary>
        /// 炸金花获取两个玩家之间的距离
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerDistanceReq : ProtoBody
        {
            /// <summary>
            /// 被看距离玩家id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid;
        }

        [ProtoContract]
        public class GoldenFlowerDistanceRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 两个玩家之间的距离
            /// </summary>
            [ProtoMember(2)]
            public string distance = "";

            [ProtoMember(3)]
            public string address = "";
        }

        /// <summary>
        /// 扔鸡蛋
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerEggReq : ProtoBody
        {
            /// <summary>
            /// 被扔鸡蛋玩家id
            /// </summary>            
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 扔鸡蛋动画类型
            /// </summary>
            [ProtoMember(2)]
            public EggType eggType = EggType.egg;
        }

        /// <summary>
        /// 扔鸡蛋动画返回
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerEggRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 扔鸡蛋动画广播
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerEggNtf : ProtoBody
        {
            /// <summary>
            /// 扔鸡蛋玩家id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 throwRoleid;

            /// <summary>
            /// 被扔鸡蛋玩家id
            /// </summary>
            [ProtoMember(2)]
            public UInt64 beRoleid;

            /// <summary>
            /// 扔鸡蛋动画枚举
            /// </summary>
            [ProtoMember(3)]
            public EggType eggType;
        }

        /// <summary>
        /// 扔鸡蛋动画枚举
        /// </summary>
        public enum EggType : uint
        {
            cheer = 0,
            flower = 1,
            tomato = 2,
            shoe = 3,
            egg = 4
        }

    }
}
