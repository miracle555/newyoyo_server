﻿using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    namespace MajiangYunCheng
    {
        /// <summary>
        /// 运城贴金创建房间
        /// </summary>
        [ProtoContract]
        public class MJYunChengCreateRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(2)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// 运城贴金加入房间
        /// </summary>
        [ProtoContract]
        public class MJYunChengJoinRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// 运城贴金获取房间信息
        /// </summary>
        [ProtoContract]
        public class MJYunChengGetRoomInfoL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (运城贴金)离开房间
        /// </summary>
        [ProtoContract]
        public class MJYunChengLeaveRoomB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (运城贴金)房间结束
        /// </summary>
        [ProtoContract]
        public class MJYunChengRoomCloseB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 是否归还房卡
            /// </summary>
            [ProtoMember(2)]
            public bool bReturnRoomCard = false;

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(3)]
            public UInt64 roomOwnerId = 0;

            /// <summary>
            /// 战绩
            /// </summary>
            [ProtoMember(4)]
            public stBattleScoreItem scoreItem = new stBattleScoreItem();

            /// <summary>
            /// 是否记录战绩
            /// </summary>
            [ProtoMember(5)]
            public bool bRecordBattleScore = false;

        };

        /// <summary>
        /// (运城贴金)玩家赢了一局
        /// </summary>
        [ProtoContract]
        public class MJYunChengWinOneRoundB2L : ProtoBody
        {
        };

        /// <summary>
        /// (抠点)房间所有人都下线了
        /// </summary>
        [ProtoContract]
        public class MJYunChengAllRoleOfflineB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// 房主建房解散房间
        /// </summary>
        [ProtoContract]
        public class MJYunChengOtherCloseRoomL2B : ProtoBody
        {
            /// <summary>
            /// 房间ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roomId;
        }
    }
}
