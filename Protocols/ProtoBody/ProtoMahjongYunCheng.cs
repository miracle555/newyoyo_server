﻿using ProtoBuf;
using Protocols.Majiang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Protocols
{
    //运城贴金
    namespace MajiangYunCheng
    {
        /// <summary>
        /// 房间状态
        /// </summary>
        public enum ERoomState
        {
            Null = 0,
            WaitRoomer = 1, // 等待其他人
            Play = 2, // 正在玩
            WaitRound = 3, // 等待下一局
        }

        /// <summary>
        /// 玩家状态
        /// </summary>
        public enum ERoomRoleState
        {
            Null = 0,
            WaitRoomer = 1, // 等待其他人
            Play = 2, // 正在玩
            Settle = 3, // 结算状态
            WaitRound = 4, // 等待下一局
            Dismiss = 5, // 解散状态
        }

        /// <summary>
        /// 操作状态
        /// </summary>
        public enum ERolePlayState
        {
            Null = 0,
            Wait = 1, // 等待其他人操作
            PengGangHuReady = 2, // 碰杠胡听选择状态
            NeedHandOut = 3, // 需要出牌状态
            NeedCheckSelfPengGangHuReady = 4,// 检测自己碰杠胡听
        }

        [ProtoContract]
        public class RemoteRoleBaseInfo
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 角色名
            /// </summary>
            [ProtoMember(2)]
            public string nickName = "";

            /// <summary>
            /// 性别
            /// </summary>
            [ProtoMember(3)]
            public ERoleSex sex = ERoleSex.Boy;

            /// <summary>
            /// 等级
            /// </summary>
            [ProtoMember(4)]
            public UInt32 level = 0;

            /// <summary>
            /// 该角色的ip
            /// </summary>
            [ProtoMember(5)]
            public string clientIp = "";

            /// <summary>
            /// 微信头像url
            /// </summary>
            [ProtoMember(6)]
            public string weixinHeadImgUrl = "";

            /// <summary>
            /// 位置
            /// </summary>
            [ProtoMember(7)]
            public string address = "";
        }

        public enum EMingCardType
        {
            Null = 0,
            MingGang = 1,//明杠
            AnGang = 2,//暗杠
            Peng = 3,//碰
        }

        /// <summary>
        /// 明牌条目
        /// </summary>
        [ProtoContract]
        public class MingCardItem
        {
            /// <summary>
            /// 明牌类型
            /// </summary>
            [ProtoMember(1)]
            public EMingCardType mingType = EMingCardType.Null;

            /// <summary>
            /// 明牌
            /// </summary>
            [ProtoMember(2)]
            public List<UInt32> showCards = new List<UInt32>();

            /// <summary>
            /// 牌类型
            /// </summary>
            [ProtoMember(3)]
            public EMJCardType cardType = EMJCardType.Null;

            public MingCardItem Copy()
            {
                MingCardItem item = new MingCardItem();
                item.mingType = mingType;
                item.showCards = showCards.Copy();
                item.cardType = cardType;
                return item;
            }
        }

        /// <summary>
        /// 麻将其他角色信息
        /// </summary>
        [ProtoContract]
        public class RemoteRoleOtherInfo
        {
            /// <summary>
            /// 方位
            /// </summary>
            [ProtoMember(1)]
            public EMajongDirection direction = EMajongDirection.Null;

            /// <summary>
            /// 是否在线
            /// </summary>
            [ProtoMember(2)]
            public bool isOnline = false;

            /// <summary>
            /// 手牌
            /// </summary>
            [ProtoMember(3)]
            public int handCardsCount = 0;

            /// <summary>
            /// 明牌
            /// </summary>
            [ProtoMember(4)]
            public List<MingCardItem> showCards = new List<MingCardItem>();

            /// <summary>
            /// 池牌
            /// </summary>
            [ProtoMember(5)]
            public List<UInt32> poolCards = new List<UInt32>();

            [ProtoMember(6)]
            public int score = 0;

            /// <summary>
            /// 是否报听
            /// </summary>
            [ProtoMember(7)]
            public bool bBaoTing = false;

            /// <summary>
            /// 房间角色状态
            /// </summary>
            [ProtoMember(8)]
            public ERoomRoleState roleState = ERoomRoleState.Null;

            /// <summary>
            /// 操作状态
            /// </summary>
            [ProtoMember(9)]
            public ERolePlayState playState = ERolePlayState.Null;

            /// <summary>
            /// 剩余时间
            /// </summary>
            [ProtoMember(10)]
            public int remainSecond = 0;

            /// <summary>
            /// 听牌时出的那张牌
            /// </summary>
            [ProtoMember(11)]
            public UInt32 readyOutCardId = 0;

            /// <summary>
            /// 最新起的那张牌
            /// </summary>
            [ProtoMember(12)]
            public UInt32 lastNewCardId = 0;

            /// <summary>
            /// 其他玩家上金的牌
            /// </summary>
            [ProtoMember(13)]
            public List<UInt32> putGoldenList = new List<UInt32>();
        }

        /// <summary>
        /// 麻将其他角色信息
        /// </summary>
        [ProtoContract]
        public class RemoteRoleInfo
        {
            /// <summary>
            /// 基础信息
            /// </summary>
            [ProtoMember(1)]
            public RemoteRoleBaseInfo baseInfo = new RemoteRoleBaseInfo();

            /// <summary>
            /// 其他信息
            /// </summary>
            [ProtoMember(2)]
            public RemoteRoleOtherInfo otherInfo = new RemoteRoleOtherInfo();
        }

        /// <summary>
        /// 麻将本人角色信息
        /// </summary>
        [ProtoContract]
        public class MahjongLocalRoleInfo
        {
            /// <summary>
            /// 方位
            /// </summary>
            [ProtoMember(1)]
            public EMajongDirection direction = EMajongDirection.Null;

            /// <summary>
            /// 分数
            /// </summary>
            [ProtoMember(2)]
            public int score = 0;

            /// <summary>
            /// 手牌
            /// </summary>
            [ProtoMember(3)]
            public List<UInt32> handCards = new List<UInt32>();

            /// <summary>
            /// 明牌
            /// </summary>
            [ProtoMember(4)]
            public List<MingCardItem> showCards = new List<MingCardItem>();

            /// <summary>
            /// 池牌
            /// </summary>
            [ProtoMember(5)]
            public List<UInt32> poolCards = new List<UInt32>();

            /// <summary>
            /// 是否报听
            /// </summary>
            [ProtoMember(6)]
            public bool bBaoTing = false;

            /// <summary>
            /// 玩家状态
            /// </summary>
            [ProtoMember(7)]
            public ERoomRoleState roleState = ERoomRoleState.Null;

            /// <summary>
            /// 操作状态
            /// </summary>
            [ProtoMember(8)]
            public ERolePlayState playState = ERolePlayState.Null;

            /// <summary>
            /// 碰杠胡听信息
            /// </summary>
            [ProtoMember(9)]
            public stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();

            /// <summary>
            /// 剩余时间
            /// </summary>
            [ProtoMember(10)]
            public int remainSecond = 0;

            /// <summary>
            /// 听牌时出的那张牌
            /// </summary>
            [ProtoMember(11)]
            public UInt32 readyOutCardId = 0;

            /// <summary>
            /// 检测听牌
            /// </summary>
            [ProtoMember(12)]
            public stCheckReadyItem checkItem = new stCheckReadyItem();

            /// <summary>
            /// 最新起的那张牌
            /// </summary>
            [ProtoMember(13)]
            public UInt32 lastNewCardId = 0;

            /// <summary>
            /// 当前回合的金数
            /// </summary>
            [ProtoMember(14)]
            public UInt32 goldenCount = 0;

            /// <summary>
            /// 上金的牌列表
            /// </summary>
            [ProtoMember(15)]
            public List<UInt32> putGoldenList = new List<UInt32>();

            /// <summary>
            /// 手牌中金牌的列表
            /// </summary>
            [ProtoMember(16)]
            public List<UInt32> handGoldenList = new List<UInt32>();
        }

        /// <summary>
        /// 麻将房间信息
        /// </summary>
        [ProtoContract]
        public class MahjongRoomInfo
        {
            /// <summary>
            /// 房间唯一ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间状态
            /// </summary>
            [ProtoMember(2)]
            public ERoomState roomState = ERoomState.WaitRoomer;

            /// <summary>
            /// 本人信息
            /// </summary>
            [ProtoMember(3)]
            public MahjongLocalRoleInfo localRoleInfo = new MahjongLocalRoleInfo();

            /// <summary>
            /// 房间其他玩家信息
            /// </summary>
            [ProtoMember(4)]
            public List<RemoteRoleInfo> lstRoles = new List<RemoteRoleInfo>();

            /// <summary>
            /// 第几牌局
            /// </summary>
            [ProtoMember(5)]
            public UInt32 currentCardRound = 0;

            /// <summary>
            /// 剩余牌数
            /// </summary>
            [ProtoMember(6)]
            public int remainCardCount = 0;

            /// <summary>
            /// 庄家
            /// </summary>
            [ProtoMember(7)]
            public UInt64 bankerRoleId = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(8)]
            public RoomConfig config = new RoomConfig();

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(9)]
            public stRoomOwner roomOwner = new stRoomOwner();

            /// <summary>
            /// 这局的金牌的类型
            /// </summary>
            [ProtoMember(10)]
            public List<EMJCardType> goldenType = new List<EMJCardType>();
                
            /// <summary>
            /// 解散信息
            /// </summary>
            [ProtoMember(11)]
            public stDismissInfo dismissInfo = new stDismissInfo();
        };

        /// <summary>
        /// 房间局数
        /// </summary>
        public enum ECardRound
        {
            Null = 0,
            OneRound = 1, // 1局
            FourRound = 2,// 4局
            EightRound = 3, // 8局
        }

        /// <summary>
        /// 房间金数
        /// </summary>
        [ProtoContract]
        public enum GoldenNumber
        {
            Null = 0,
            FourGolden = 4,//4金
            EightGolden = 8,//8金
        }

        /// <summary>
        /// 封顶金数量
        /// </summary>
        [ProtoContract]
        public enum CappingNumber
        {
            Two = 2,//2金封顶
            Three = 3,//3金封顶
            Four = 4,//4金封顶
        }

        

        /// <summary>
        /// 房间配置信息
        /// </summary>
        [ProtoContract]
        public class RoomConfig
        {
            /// <summary>
            /// 选择局数
            /// </summary>
            [ProtoMember(1)]
            public ECardRound cardRound = ECardRound.OneRound;

            /// <summary>
            /// 只能自摸
            /// </summary>
            [ProtoMember(2)]
            public bool bOnlyWinSelfWhenGoldenLess = false;

            /// <summary>
            /// 选择金牌数量
            /// </summary>
            [ProtoMember(3)]
            public GoldenNumber goldens = GoldenNumber.FourGolden;

            /// <summary>
            /// 单局金封顶数量
            /// </summary>
            [ProtoMember(4)]
            public CappingNumber cappingNumber = CappingNumber.Three;

            /// <summary>
            /// 是否捆金
            /// </summary>
            [ProtoMember(5)]
            public bool isKunJin = false;

            /// <summary>
            /// 付款方式
            /// </summary>
            [ProtoMember(6)]
            public PayMethod payMethod = PayMethod.masterPay;

            /// <summary>
            /// 金数
            /// </summary>
            public UInt32 roomGoldens
            {
                get
                {
                    if (goldens == GoldenNumber.FourGolden)
                    {
                        return 4;
                    }
                    else if (goldens == GoldenNumber.EightGolden)
                    {
                        return 8;
                    }
                    else
                    {
                        return 4;
                    }
                }
            }

            /// <summary>
            /// 麻将房卡一律扣3张
            /// </summary>
            public UInt32 roomCardCount
            {
                get
                {
                    if (payMethod.Equals(PayMethod.AAPay))
                    {
                        return 1;
                    }
                    else
                    {
                        return 4;
                    }
                }
            }
            /// <summary>
            /// 局数
            /// </summary>
            public int roomRound
            {
                get
                {
                    if (cardRound == ECardRound.OneRound)
                    {
                        return 1;
                    }
                    else if (cardRound == ECardRound.FourRound)
                    {
                        return 4;
                    }
                    else if (cardRound == ECardRound.EightRound)
                    {
                        return 8;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
        }

        /// <summary>
        /// (运城贴金)创建房间
        /// </summary>
        [ProtoContract]
        public class MJYunChengCreateRoomReq : ProtoBody
        {
            [ProtoMember(1)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// (运城贴金)创建房间返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengCreateRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 已经创建房间
                /// </summary>
                AlreadyCreateRoom = 2,

                /// <summary>
                /// 已经在其他房间里
                /// </summary>
                AlreadInRoom = 3,

                /// <summary>
                /// 房卡数量不足
                /// </summary>
                RoomCardNotEnough = 4,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public MahjongRoomInfo roomInfo = new MahjongRoomInfo();
        };

        /// <summary>
        /// (运城贴金)加入房间
        /// </summary>
        [ProtoContract]
        public class MJYunChengJoinRoomReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (运城麻将)加入房间返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengJoinRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 房间不存在
                /// </summary>
                RoomNotExist = 2,

                /// <summary>
                /// 房间已满
                /// </summary>
                RoomFull = 3,

                /// <summary>
                /// 房卡数量不足
                /// </summary>
                RoomCardNotEnough = 4,

                /// <summary>
                /// 房主不允许加入自己的房间
                /// </summary>
                AnthorNotJoin = 5,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public MahjongRoomInfo roomInfo = new MahjongRoomInfo();
        };

        /// <summary>
        /// (运城贴金)加入房间通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengJoinRoomNtf : ProtoBody
        {
            /// <summary>
            /// 人物信息
            /// </summary>
            [ProtoMember(1)]
            public RemoteRoleInfo remoteRoleInfo = new RemoteRoleInfo();
        };

        /// <summary>
        /// (运城贴金)回合开始通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengRoundStartNtf : ProtoBody
        {
            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(1)]
            public MahjongRoomInfo roomInfo = new MahjongRoomInfo();
        };

        /// <summary>
        /// (运城贴金)出牌请求
        /// </summary>
        [ProtoContract]
        public class MJYunChengHandOutCardReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt32 cardid = 0;
        };

        /// <summary>
        /// (运城贴金)出牌请求返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengHandOutCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
            
            //true 表示是金牌 false 表示不是金牌
            [ProtoMember(2)]
            public bool isGolden = false; 
        };

        /// <summary>
        /// (运城贴金)出牌通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengHandOutCardNtf : ProtoBody
        {
            /// <summary>
            /// 出牌角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 cardRoleId = 0;

            [ProtoMember(2)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 是否是听牌出的牌
            /// </summary>
            [ProtoMember(3)]
            public bool bReadyOut = false;

            [ProtoMember(4)]
            public bool bIsGolden = false;
        };

        /// <summary>
        /// (运城贴金)某人起到新牌
        /// </summary>
        [ProtoContract]
        public class MJYunChengGetNewCardNtf : ProtoBody
        {
            /// <summary>
            /// 出牌角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 cardRoleId = 0;

            /// <summary>
            /// 出的牌ID
            /// </summary>
            [ProtoMember(2)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 是否是金牌
            /// </summary>
            [ProtoMember(3)]
            public bool bIsGolden = false;
        };

        /// <summary>
        /// (运城贴金)轮到你出牌了
        /// </summary>
        [ProtoContract]
        public class MJYunChengNeedHandCardNtf : ProtoBody
        {
            /// <summary>
            /// 出牌角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 cardRoleId = 0;

            /// <summary>
            /// 剩余出牌时间
            /// </summary>
            [ProtoMember(2)]
            public int playRemainSecond = 0;
        };

        /// <summary>
        /// 杠牌类型
        /// </summary>
        public enum EGangCardType
        {
            Null = 0,
            MingGang = 1, // 明杠
            AnGang = 2, // 暗杠
            BuGang = 3, // 补杠
        }

        /// <summary>
        /// 碰杠胡听信息
        /// </summary>
        [ProtoContract]
        public class stPengGangHuReadyInfo
        {
            /// <summary>
            /// 出牌角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 cardRoleId = 0;

            /// <summary>
            /// 牌ID 
            /// </summary>
            [ProtoMember(2)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 可以碰牌 
            /// </summary>
            [ProtoMember(3)]
            public bool bPeng = false;

            /// <summary>
            /// 杠牌类型
            /// </summary>
            [ProtoMember(4)]
            public EGangCardType gangType = EGangCardType.Null;

            /// <summary>
            /// 可以胡牌
            /// </summary>
            [ProtoMember(5)]
            public bool bHuPai = false;

            /// <summary>
            /// 可以听牌
            /// </summary>
            [ProtoMember(6)]
            public bool bReadyCard = false;

            /// <summary>
            /// 出完该牌可以听
            /// </summary>
            [ProtoMember(7)]
            public List<ReadyInfo> lstReadyCards = new List<ReadyInfo>();

            /// <summary>
            /// 上金
            /// </summary>
            [ProtoMember(8)]
            public bool isGolden = false;

            public void Clear()
            {
                cardRoleId = 0;
                cardid = 0;
                bPeng = false;
                gangType = EGangCardType.Null;
                bHuPai = false;
                bReadyCard = false;
                lstReadyCards.Clear();
            }
        }

        /// <summary>
        /// (运城贴金)通知碰牌，杠牌，胡牌
        /// </summary>
        [ProtoContract]
        public class MJYunChengPengHuCardNtf : ProtoBody
        {
            [ProtoMember(1)]
            public stPengGangHuReadyInfo readyInfo = new stPengGangHuReadyInfo();
        };

        /// <summary>
        /// (运城贴金)碰牌请求
        /// </summary>
        [ProtoContract]
        public class MJYunChengPengCardReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)碰牌请求返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengPengCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 出牌人 
            /// </summary>
            [ProtoMember(2)]
            public UInt64 cardRoleId = 0;

            /// <summary>
            /// 出牌牌ID 
            /// </summary>
            [ProtoMember(3)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 碰牌，从手牌中删除2张牌
            /// </summary>
            [ProtoMember(4)]
            public List<UInt32> lstPengCards = new List<UInt32>();
        };

        /// <summary>
        /// (运城贴金)碰牌通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengPengCardNtf : ProtoBody
        {
            /// <summary>
            /// 出牌人 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 cardRoleId = 0;

            /// <summary>
            /// 出牌牌ID 
            /// </summary>
            [ProtoMember(2)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 碰牌人
            /// </summary>
            [ProtoMember(3)]
            public UInt64 pengRoleId = 0;

            /// <summary>
            /// 碰牌ID 
            /// </summary>
            [ProtoMember(4)]
            public List<UInt32> lstCards = new List<UInt32>();
        };

        /// <summary>
        /// 胡牌类型
        /// </summary>
        public enum EHuCardType
        {
            Null = 0,
            CommonHu = 1, // 平胡
            SenvenPairs = 2, // 七小对
            OneDragon = 3, // 一条龙
            SameColor = 4, // 清一色
            LuxurySevenPairs = 5, // 豪华七小对
            ThirteenOrphans = 6, // 十三幺
        }

        /// <summary>
        /// 胡牌类型
        /// </summary>
        public enum ERoundSettleType
        {
            Null = 0,
            Gang = 1,
            Hu = 2,
        }

        /// <summary>
        /// 结算角色信息
        /// </summary>

        [ProtoContract]
        public class RoundSettleRole
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 昵称
            /// </summary>
            [ProtoMember(2)]
            public string nickName = "";

            /// <summary>
            /// 扣分加分
            /// </summary>
            [ProtoMember(3)]
            public int score = 0;

            /// <summary>
            /// 倍数
            /// </summary>
            [ProtoMember(4)]
            public int multiTimes = 0;

            /// <summary>
            /// 手牌
            /// </summary>
            [ProtoMember(5)]
            public List<UInt32> handCards = new List<UInt32>();

            /// <summary>
            /// 明牌
            /// </summary>
            [ProtoMember(6)]
            public List<MingCardItem> showCards = new List<MingCardItem>();

            /// <summary>
            /// 是否抢杠胡
            /// </summary>
            [ProtoMember(7)]
            public bool bRobGangHu = false;

            /// <summary>
            /// 玩家上金的数量
            /// </summary>
            [ProtoMember(8)]
            public int putGolden;

            /// <summary>
            /// 金牌类型
            /// </summary>
            [ProtoMember(9)]
            public List<EMJCardType> goldeType;
        }

        [ProtoContract]
        public class stFinalSettle
        {
            /// <summary>
            /// 各个玩家的最终结算分数
            /// </summary>
            [ProtoMember(1)]
            public Dictionary<UInt64, int> mapScores = new Dictionary<UInt64, int>();

            public void ChangeScore(UInt64 roleid, int score)
            {
                if (mapScores.ContainsKey(roleid))
                {
                    mapScores[roleid] += score;
                }
                else
                {
                    mapScores.Add(roleid, score);
                }
            }

            public void InitScore(UInt64 roleid)
            {
                if (!mapScores.ContainsKey(roleid))
                {
                    mapScores.Add(roleid, 0);
                }
            }
        }

        [ProtoContract]
        public class stCheckReadyItem
        {
            /// <summary>
            /// 是否暗杠
            /// </summary>
            [ProtoMember(1)]
            public bool bAnGang = false;

            /// <summary>
            /// 是否补杠
            /// </summary>
            [ProtoMember(2)]
            public bool bBuGang = false;

            /// <summary>
            /// 是否胡牌
            /// </summary>
            [ProtoMember(3)]
            public bool bHuCard = false;

            /// <summary>
            /// 是否听牌
            /// </summary>
            [ProtoMember(4)]
            public bool bReadyCard = false;

        }

        /// <summary>
        /// 结算信息
        /// </summary>

        [ProtoContract]
        public class RoundSettleInfo
        {
            /// <summary>
            /// 庄家ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 bankerRoleId = 0;

            /// <summary>
            /// 结算角色信息列表
            /// </summary>
            [ProtoMember(2)]
            public List<RoundSettleRole> lstSettleRoles = new List<RoundSettleRole>();

            /// <summary>
            /// 结算事件列表
            /// </summary>
            [ProtoMember(3)]
            public List<RoundSettleItem> lstSettleItems = new List<RoundSettleItem>();

            public void AddScore(UInt64 roleid, int score)
            {
                foreach (var role in lstSettleRoles)
                {
                    if (role.roleid == roleid)
                    {
                        role.score += score;
                    }
                }
            }

            public void DelScore(UInt64 roleid, int score)
            {
                foreach (var role in lstSettleRoles)
                {
                    if (role.roleid == roleid)
                    {
                        role.score -= score;
                    }
                }
            }

            public void DelScore2(UInt64 roleid, int score,UInt64 bankId)
            {
                foreach (var role in lstSettleRoles)
                {
                    if (role.roleid == roleid)
                    {
                        role.score -= score;
                        if(role.roleid == bankId)
                        {
                            role.score -= 1;
                        }
                    }
                }
            }

            public void DelScoreExceptRole(UInt64 roleid, int score)
            {
                foreach (var role in lstSettleRoles)
                {
                    if (role.roleid != roleid)
                    {
                        role.score -= score;
                    }
                }
            }

            public void DelScoreExceptRoleBank(UInt64 roleid, int score,UInt64 bankId,int huPaifen)
            {
                foreach (var role in lstSettleRoles)
                {
                    if (role.roleid != roleid)
                    {
                        role.score -= score;
                        if(role.roleid == bankId)
                        {
                            role.score -= (1* huPaifen);
                        }
                    }
                }
            }

            public void SetMaxScales(UInt64 roleid, int multiTimes)
            {
                foreach (var role in lstSettleRoles)
                {
                    if (role.roleid == roleid)
                    {
                        if (role.multiTimes < multiTimes)
                        {
                            role.multiTimes = multiTimes;
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 结算条目
        /// </summary>
        [ProtoContract]
        public class RoundSettleItem
        {
            /// <summary>
            /// 事件类型
            /// </summary>
            [ProtoMember(1)]
            public ERoundSettleType settleType = ERoundSettleType.Null;

            /// <summary>
            /// 是否自摸
            /// </summary>
            [ProtoMember(2)]
            public bool bSelfTouch = false;

            /// <summary>
            /// 杠牌类型
            /// </summary>
            [ProtoMember(3)]
            public EGangCardType gangType = EGangCardType.Null;

            /// <summary>
            /// 胡牌类型
            /// </summary>
            [ProtoMember(4)]
            public EHuCardType huType = EHuCardType.Null;

            /// <summary>
            /// 角色id
            /// </summary>
            [ProtoMember(5)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 点炮人或点杠人
            /// </summary>
            [ProtoMember(6)]
            public UInt64 otherRoleId = 0;

            /// <summary>
            /// 牌ID
            /// </summary>
            [ProtoMember(7)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 点炮人或点杠人是否报听
            /// </summary>
            [ProtoMember(8)]
            public bool bOtherBaoTing = false;

            /// <summary>
            /// 抢杠胡
            /// </summary>
            [ProtoMember(9)]
            public bool bRobGangHu = false;
        }

        /// <summary>
        /// (运城贴金)获取房间信息
        /// </summary>
        [ProtoContract]
        public class MJYunChengGetRoomInfoReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)获取房间信息返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengGetRoomInfoRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public MahjongRoomInfo roomInfo = new MahjongRoomInfo();
        };

        /// <summary>
        /// (运城贴金)胡牌请求
        /// </summary>
        [ProtoContract]
        public class MJYunChengHuCardReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)胡牌返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengHuCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

        };

        /// <summary>
        /// (运城贴金)胡牌通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengHuCardNtf : ProtoBody
        {
            /// <summary>
            /// 点炮还是自摸
            /// </summary>
            [ProtoMember(1)]
            public bool bSelfTouch = false;

            /// <summary>
            /// 点炮人
            /// </summary>
            [ProtoMember(2)]
            public UInt64 cardRoleId = 0;

            /// <summary>
            /// 胡牌人
            /// </summary>
            [ProtoMember(3)]
            public UInt64 huRoleId = 0;
        };

        /// <summary>
        /// (运城贴金)杠牌请求
        /// </summary>
        [ProtoContract]
        public class MJYunChengGangCardReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)杠牌请求返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengGangCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 杠牌类型
            /// </summary>
            [ProtoMember(2)]
            public EGangCardType gangType = EGangCardType.Null;

            /// <summary>
            /// 出牌角色ID 
            /// </summary>
            [ProtoMember(3)]
            public UInt64 cardRoleId = 0;

            /// <summary>
            /// 牌ID 
            /// </summary>
            [ProtoMember(4)]
            public UInt32 cardid = 0;

            /// <summary>
            /// z自己的牌
            /// </summary>
            [ProtoMember(5)]
            public List<UInt32> lstCards = new List<UInt32>();
        };

        /// <summary>
        /// (运城贴金)杠牌通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengGangCardNtf : ProtoBody
        {
            /// <summary>
            /// 杠牌类型
            /// </summary>
            [ProtoMember(1)]
            public EGangCardType gangType = EGangCardType.Null;

            /// <summary>
            /// 点杠人
            /// </summary>
            [ProtoMember(2)]
            public UInt64 loseRoleId = 0;

            /// <summary>
            /// 开杠人
            /// </summary>
            [ProtoMember(3)]
            public UInt64 winRoleId = 0;

            /// <summary>
            /// 牌id
            /// </summary>
            [ProtoMember(4)]
            public UInt32 cardid = 0;

            /// <summary>
            /// 开杠人的牌
            /// </summary>
            [ProtoMember(5)]
            public List<UInt32> lstCards = new List<UInt32>();
        };

        /// <summary>
        /// (运城贴金)听牌请求
        /// </summary>
        [ProtoContract]
        public class MJYunChengReadyCardReq : ProtoBody
        {
            /// <summary>
            /// 听牌时出的牌
            /// </summary>
            [ProtoMember(1)]
            public UInt32 cardid = 0;
        };

        /// <summary>
        /// (运城贴金)听牌请求返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengReadyCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (运城贴金)听牌通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengReadyCardNtf : ProtoBody
        {
            /// <summary>
            /// 听牌人
            /// </summary>
            [ProtoMember(1)]
            public UInt64 readyRoleId = 0;
        };

        /// <summary>
        /// (运城贴金)过
        /// </summary>
        [ProtoContract]
        public class MJYunChengPassReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)过
        /// </summary>
        [ProtoContract]
        public class MJYunChengPassRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (运城贴金)下一步
        /// </summary>
        [ProtoContract]
        public class MJYunChengNextStepReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)下一步消息返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengNextStepRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (运城贴金)下一步
        /// </summary>
        [ProtoContract]
        public class MJYunChengNextStepNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;
        };

        /// <summary>
        /// (运城贴金)解散游戏
        /// </summary>
        [ProtoContract]
        public class MJYunChengDismissGameReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)解散游戏返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengDismissGameRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 解散倒计时
            /// </summary>
            [ProtoMember(2)]
            public int remainSecond = 0;
        };

        /// <summary>
        /// (运城贴金)解散通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengDismissGameNtf : ProtoBody
        {
            /// <summary>
            /// 点击解散游戏的人
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 解散倒计时
            /// </summary>
            [ProtoMember(2)]
            public int remainSecond = 0;
        };

        /// <summary>
        /// (运城贴金)解散是否同意
        /// </summary>
        [ProtoContract]
        public class MJYunChengDismissAgreeReq : ProtoBody
        {
            [ProtoMember(1)]
            public bool bAgree = false;
        };

        /// <summary>
        /// (运城贴金)解散是否同意返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengDismissAgreeRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (运城贴金)解散是否同意通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengDismissAgreeNtf : ProtoBody
        {
            /// <summary>
            /// 角色id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 是否同意解散房间
            /// </summary>
            [ProtoMember(2)]
            public bool bAgree = false;
        };

        /// <summary>
        /// (运城贴金)解散通过通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengDismissGameSuccessNtf : ProtoBody
        {
            [ProtoMember(1)]
            public bool bSuccess = false;

            /// <summary>
            /// 最终结算信息
            /// </summary>
            [ProtoMember(2)]
            public stFinalSettle finalSettle = new stFinalSettle();
        };

        /// <summary>
        /// (运城贴金)未开始时退出房间
        /// </summary>
        [ProtoContract]
        public class MJYunChengLeaveRoomReq : ProtoBody
        {
        };

        /// <summary>
        /// (运城贴金)裂开房间返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengLeaveRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (运城贴金)离开房间通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengLeaveRoomNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            [ProtoMember(2)]
            public bool bRoomOwner = false;
        };

        /// <summary>
        /// (运城贴金)离开房间
        /// </summary>
        [ProtoContract]
        public class MJPointLeaveRoomB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (运城贴金)聊天请求
        /// </summary>
        [ProtoContract]
        public class MJYunChengChatReq : ProtoBody
        {
            /// <summary>
            /// 聊天ID
            /// </summary>
            [ProtoMember(1)]
            public UInt32 chatid = 0;
        };

        /// <summary>
        /// (运城贴金)聊天请求返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengChatRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (运城贴金)聊天请求通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengChatNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 聊天ID
            /// </summary>
            [ProtoMember(2)]
            public UInt32 chatid = 0;
        };

        /// <summary>
        /// 扔鸡蛋动画枚举
        /// </summary>
        public enum EggType : uint
        {
            cheer = 0,
            flower = 1,
            tomato = 2,
            shoe = 3,
            egg = 4
        }


        /// <summary>
        /// 扔鸡蛋
        /// </summary>
        [ProtoContract]
        public class MJYunChengEggReq:ProtoBody
        {
            /// <summary>
            /// 被扔鸡蛋玩家id
            /// </summary>            
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 扔鸡蛋动画类型
            /// </summary>
            [ProtoMember(2)]
            public EggType eggType = EggType.egg;
        }

        /// <summary>
        /// 扔鸡蛋动画返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengEggRsp: ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 扔鸡蛋动画广播
        /// </summary>
        [ProtoContract]
        public class MJYunChengEggNtf: ProtoBody
        {
            /// <summary>
            /// 扔鸡蛋玩家id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 throwRoleid;

            /// <summary>
            /// 被扔鸡蛋玩家id
            /// </summary>
            [ProtoMember(2)]
            public UInt64 beRoleid;

            /// <summary>
            /// 扔鸡蛋动画枚举
            /// </summary>
            [ProtoMember(3)]
            public EggType eggType;
        }

        /// <summary>
        /// 用户聊天输入文字请求
        /// </summary>
        [ProtoContract]
        public class MJYunChengChatWordReq : ProtoBody
        {
            [ProtoMember(1)]
            public string chatWord = "";
        }

        /// <summary>
        /// 用户聊天输入文字返回
        /// </summary>
        [ProtoContract]
        public class MJYunChengChatWordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 用户聊天输入文字通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengChatWordNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleId = 0;

            /// <summary>
            /// 用户输入的文字
            /// </summary>
            [ProtoMember(2)]
            public string chatWord = "";
        }

        /// <summary>
        /// 运城贴金获取两个玩家之间的距离
        /// </summary>
        [ProtoContract]
        public class MJYunChengDistanceReq:ProtoBody
        {
            /// <summary>
            /// 被看距离玩家id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid;
        }

        [ProtoContract]
        public class MJYunChengDistanceRsp:ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 两个玩家之间的距离
            /// </summary>
            [ProtoMember(2)]
            public string distance = "";

            [ProtoMember(3)]
            public string address = "";
        }

        /// <summary>
        /// (运城贴金)结算通知
        /// </summary>
        [ProtoContract]
        public class MJYunChengRoundSettleNtf : ProtoBody
        {
            /// <summary>
            /// 结算信息
            /// </summary>
            [ProtoMember(1)]
            public RoundSettleInfo settleInfo = new RoundSettleInfo();

            /// <summary>
            /// 房间是否结束
            /// </summary>
            [ProtoMember(2)]
            public bool IsRoomOver = false;

            /// <summary>
            /// 最终结算信息
            /// </summary>
            [ProtoMember(3)]
            public stFinalSettle finalSettle = new stFinalSettle();
        };

        /// <summary>
        /// 付款方式
        /// </summary>
        [ProtoContract]
        public enum PayMethod
        {
            masterPay = 1,//房主付
            AAPay = 2,//AA制
            anotherPay = 3,//房主代开房
        }
    }
}
