﻿using System;
using ProtoBuf;

namespace Protocols
{
    [ProtoContract]
    public class StaticDataCheckReload : ProtoBody
    {
        [ProtoMember(1)]
        public DateTime t = DateTime.MinValue;
    }
}