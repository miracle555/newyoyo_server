﻿using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    namespace MajiangPushDown
    {
        /// <summary>
        /// 推倒胡创建房间
        /// </summary>
        [ProtoContract]
        public class MJPushDownCreateRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(2)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// 推倒胡加入房间
        /// </summary>
        [ProtoContract]
        public class MJPushDownJoinRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// 推倒胡获取房间信息
        /// </summary>
        [ProtoContract]
        public class MJPushDownGetRoomInfoL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (推倒胡)离开房间
        /// </summary>
        [ProtoContract]
        public class MJPushDownLeaveRoomB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (推倒胡)房间结束
        /// </summary>
        [ProtoContract]
        public class MJPushDownRoomCloseB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 是否归还房卡
            /// </summary>
            [ProtoMember(2)]
            public bool bReturnRoomCard = false;

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(3)]
            public UInt64 roomOwnerId = 0;

            /// <summary>
            /// 战绩
            /// </summary>
            [ProtoMember(4)]
            public stBattleScoreItem scoreItem = new stBattleScoreItem();

            /// <summary>
            /// 是否记录战绩
            /// </summary>
            [ProtoMember(5)]
            public bool bRecordBattleScore = false;
        };

        /// <summary>
        /// (推倒胡)玩家赢了一局
        /// </summary>
        [ProtoContract]
        public class MJPushDownWinOneRoundB2L : ProtoBody
        {
        };

        /// <summary>
        /// (推倒胡)房间所有人都下线了
        /// </summary>
        [ProtoContract]
        public class MJPushDownAllRoleOfflineB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };
    }
}