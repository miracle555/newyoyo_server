﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Protocols
{
    namespace GoldenFlower
    {
        /// <summary>
        /// (扎金花）创建房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerCreateRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(2)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// (扎金花）加入房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerJoinRoomL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (扎金花）取得房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerGetRoomInfoL2B : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (扎金花）离开房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerLeaveRoomB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

        };

        /// <summary>
        /// (扎金花）房间结束
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerRoomCloseB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 是否归还房卡
            /// </summary>
            [ProtoMember(2)]
            public bool bReturnRoomCard = false;

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(3)]
            public UInt64 roomOwnerId = 0;

            /// <summary>
            /// 战绩
            /// </summary>
            [ProtoMember(4)]
            public stBattleScoreItem scoreItem = new stBattleScoreItem();

            /// <summary>
            /// 是否记录战绩
            /// </summary>
            [ProtoMember(5)]
            public bool bRecordBattleScore = false;
        };

        /// <summary>
        /// (扎金花)玩家赢了一局
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerWinOneRoundB2L : ProtoBody
        {
        };

        /// <summary>
        /// (扎金花)房间所有人都下线了
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerAllRoleOfflineB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// 房主建房解散房间
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerOtherCloseRoomL2B : ProtoBody
        {
            /// <summary>
            /// 房间ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roomId;
        }

        /// <summary>
        /// 开始游戏战斗服转大厅服
        /// </summary>
        [ProtoContract]
        public class GoldenFlowerStartB2L : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomId;
        }
    }
}