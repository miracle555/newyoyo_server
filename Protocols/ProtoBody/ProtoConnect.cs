﻿using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    public enum DisconnReason
    {
        Active,             //客户端主动断开
        Passive,            //服务器断开
        Exception,          //异常断开
    }

    [ProtoContract]
    public class ClientConnectNtf : ProtoBody
    {
        public System.Net.IPEndPoint endpoint;
        public bool success;
    }

    [ProtoContract]
    public class ClientDisconnectNtf : ProtoBody
    {
        public DisconnReason reason;
    }
}