﻿/********************************************************************
	created:	2015/01/09
	created:	9:1:2015   15:19
	filename: 	C:\Data\Repos\CommonPlatform\Server\ServerInstance\CommonLib\Protocols\ProtoGate.cs
	file path:	C:\Data\Repos\CommonPlatform\Server\ServerInstance\CommonLib\Protocols
	file base:	ProtoGate
	file ext:	cs
	author:		刘冰生
	
	purpose:	
*********************************************************************/
using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    public enum KickReason
    {
        /// <summary>
        /// 同名账号登录
        /// </summary>
        AccountDuplicate = 1,

        /// <summary>
        /// 清理房间数据
        /// </summary>
        ClearRoom = 2,
    }

    /// <summary>
    /// 协议广播
    /// </summary>
    [ProtoContract]
    public class GateCommonBroadcast : ProtoBody
    {
        /// <summary>
        /// role列表
        /// </summary>
        [ProtoMember(1)]
        public List<UInt64> roles = new List<ulong>();

        /// <summary>
        /// 需要广播的协议
        /// </summary>
        [ProtoMember(2, DynamicType = true)]
        public ProtoBody proto;
    }

    [ProtoContract]
    public class HeartbeatReq : ProtoBody
    {
    }

    [ProtoContract]
    public class HeartbeatRsp : ProtoBody
    {
    }

    [ProtoContract]
    public class KickNtf : ProtoBody
    {
        [ProtoMember(1)]
        public KickReason reason;
    }
}