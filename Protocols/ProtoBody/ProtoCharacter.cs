﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Protocols
{
    public enum ERoleSex
    {
        Boy = 0, // 男
        Girl = 1, // 女
    }

    /// <summary>
    /// 玩家数据
    /// </summary>
    [ProtoContract]
    public class PlayerData
    {
        [ProtoMember(1)]
        public UInt64 accountId = 0; // 账号id

        [ProtoMember(2)]
        public string nickName = "";

        [ProtoMember(3)]
        public UInt32 diamond = 0;

        /// <summary>
        /// 角色性别
        /// </summary>
        [ProtoMember(4)]
        public ERoleSex sex = ERoleSex.Boy;

        [ProtoMember(5)]
        public UInt32 level = 0; // 称号等级

        [ProtoMember(6)]
        public string title = ""; // 称号

        [ProtoMember(7)]
        public UInt32 levelupExp = 0; // 升到下一级需要经验

        [ProtoMember(8)]
        public UInt32 curExp = 0; // 当前经验

        /// <summary>
        /// 邀请自己的人
        /// </summary>
        [ProtoMember(9)]
        public UInt64 inviteSelfRoleId = 0;

        /// <summary>
        /// 自己的外网IP
        /// </summary>
        [ProtoMember(10)]
        public string selfIp = "";

        /// <summary>
        /// 微信头像url
        /// </summary>
        [ProtoMember(11)]
        public string weixinHeadImgUrl = "";

        /// <summary>
        /// 当前已经邀请的人数
        /// </summary>
        [ProtoMember(12)]
        public UInt32 inviteRoleCount = 0;

        /// <summary>
        /// 下一级要达到的邀请人数
        /// </summary>
        [ProtoMember(13)]
        public UInt32 nextInviteRoleCount = 0;

        /// <summary>
        /// 达到下一级邀请人数奖励房卡数量
        /// </summary>
        [ProtoMember(14)]
        public UInt32 nextInviteAwardRoomCard = 0;
    }

    /// <summary>
    /// 请求玩家信息
    /// </summary>
    [ProtoContract]
    public class PlayerDataReq : ProtoBody
    {
    }

    /// <summary>
    /// 返回玩家信息
    /// </summary>
    [ProtoContract]
    public class PlayerDataRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 名字
        /// </summary>
        [ProtoMember(2)]
        public PlayerData playerData = new PlayerData();
    }

    /// <summary>
    /// 玩家状态
    /// </summary>
    public enum EPlayerState : uint
    {
        /// <summary>
        /// 正常
        /// </summary>
        Play = 0,

        /// <summary>
        /// 战斗
        /// </summary>
        Battle = 1,
    }

    /// <summary>
    /// 获取玩家状态
    /// </summary>
    [ProtoContract]
    public class GetPlayerStateReq : ProtoBody
    {
    }

    /// <summary>
    /// 返回玩家状态
    /// </summary>
    [ProtoContract]
    public class GetPlayerStateRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;

        /// <summary>
        /// 名字
        /// </summary>
        [ProtoMember(2)]
        public EPlayerState playerState = EPlayerState.Play;

        /// <summary>
        /// 房间ID
        /// </summary>
        [ProtoMember(3)]
        public UInt64 roomUuid = 0;

        /// <summary>
        /// 房间类型
        /// </summary>
        [ProtoMember(4)]
        public EGameType gameType = EGameType.Null;
    }

    /// <summary>
    /// 经验变化通知
    /// </summary>
    [ProtoContract]
    public class RoleExpChangeNtf : ProtoBody
    {
        /// <summary>
        /// 当前经验
        /// </summary>
        [ProtoMember(1)]
        public UInt32 curExp = 0;
    }

    /// <summary>
    /// 测试添加经验
    /// </summary>
    [ProtoContract]
    public class TestAddExpReq : ProtoBody
    {
        /// <summary>
        /// 增加经验数量
        /// </summary>
        [ProtoMember(1)]
        public UInt32 exp = 0;
    }

    /// <summary>
    /// 测试添加经验返回
    /// </summary>
    [ProtoContract]
    public class TestAddExpRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;
    }

    /// <summary>
    /// 等级变化通知
    /// </summary>
    [ProtoContract]
    public class RoleLevelChangeNtf : ProtoBody
    {
        [ProtoMember(1)]
        public UInt32 level = 0; // 等级

        [ProtoMember(2)]
        public string title = ""; // 称号

        [ProtoMember(3)]
        public UInt32 levelupExp = 0; // 升到下一级需要经验
    }

    /// <summary>
    /// 房卡变化通知
    /// </summary>
    [ProtoContract]
    public class RoomCardChangeNtf : ProtoBody
    {
        /// <summary>
        /// 当前房卡数
        /// </summary>
        [ProtoMember(1)]
        public UInt32 diamond = 0;
    }

    /// <summary>
    /// 公告通知
    /// </summary>
    [ProtoContract]
    public class PublicNoticeNtf : ProtoBody
    {
        /// <summary>
        /// 公告通知
        /// </summary>
        [ProtoMember(1)]
        public string notice = "";
    }

    /// <summary>
    /// 邀请人数量变化通知
    /// </summary>
    [ProtoContract]
    public class InviteRoleCountChangeNtf : ProtoBody
    {
        /// <summary>
        /// 当前已经邀请的人数
        /// </summary>
        [ProtoMember(1)]
        public UInt32 inviteRoleCount = 0;

        /// <summary>
        /// 下一级要达到的邀请人数
        /// </summary>
        [ProtoMember(2)]
        public UInt32 nextInviteRoleCount = 0;

        /// <summary>
        /// 达到下一级邀请人数奖励房卡数量
        /// </summary>
        [ProtoMember(3)]
        public UInt32 nextInviteAwardRoomCard = 0;
    }

    /// <summary>
    /// 填写邀请自己的人
    /// </summary>
    [ProtoContract]
    public class InputInviteSelfRoleReq : ProtoBody
    {
        [ProtoMember(1)]
        public UInt64 roleid = 0;
    }

    /// <summary>
    /// 填写邀请自己的人
    /// </summary>
    [ProtoContract]
    public class InputInviteSelfRoleRsp : ProtoBody
    {
        public enum ErrorID : uint
        {
            /// <summary>
            /// 成功
            /// </summary>
            Success = 0,

            /// <summary>
            /// 失败
            /// </summary>
            Failed = 1,

            /// <summary>
            /// 相互为邀请人错误
            /// </summary>
            MutualInvit = 2,

            /// <summary>
            /// 不能邀请自己
            /// </summary>
            NotSelf = 3,
        }

        [ProtoMember(1)]
        public ErrorID errorCode;
    }
}
