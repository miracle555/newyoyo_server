﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Protocols
{
    // 斗地主
    namespace Landlord
    {
        /// <summary>
        /// 房间状态
        /// </summary>
        public enum ERoomState
        {
            Null = 0,
            WaitRoomer = 1, // 等待其他人
            Play = 2, // 正在玩
            WaitRound = 3, // 等待下一局
        }

        /// <summary>
        /// 玩家状态
        /// </summary>
        public enum ERoomRoleState
        {
            Null = 0,
            WaitRoomer = 1, // 等待其他人
            Play = 2, // 正在玩
            Settle = 3, // 结算状态
            WaitRound = 4, // 等待下一局
            Dismiss = 5, // 解散状态
        }

        /// <summary>
        /// 操作状态
        /// </summary>
        public enum ERolePlayState
        {
            Null = 0,
            MingCardOrNormalStart = 1, // 明牌开始或正常开始
            ShowCard = 2, // 显示牌，可以发明牌消息
            RobLandlord = 3, // 抢地主
            DoubleScale = 4, // 加倍
            Wait = 5, // 等待其他人操作
            HandOutCard = 6, // 出牌或不出，出牌状态
            CallLandlord = 7, // 叫地主
        }

        public enum ESeatPostion
        {
            Null = 0, // 默认 
            First = 1, // 第一位
            Second = 2, // 第二位
            Third = 3, // 第三位
            Max = 4,
        }

        /// <summary>
        /// 扑克牌ID
        /// </summary>
        public enum ECardId
        {
            Null = 0,

            Black3 = 1, // 黑桃3
            Flower3 = 2, // 梅花3
            Red3 = 3, // 红桃3
            Cube3 = 4, // 方块3

            Black4 = 5, // 黑桃4
            Red4 = 6, // 红桃4
            Flower4 = 7, // 梅花4
            Cube4 = 8, // 方块4

            Black5 = 9, // 黑桃5
            Red5 = 10, // 红桃5
            Flower5 = 11, // 梅花5
            Cube5 = 12, // 方块5

            Black6 = 13, // 黑桃6
            Red6 = 14, // 红桃6
            Flower6 = 15, // 梅花6
            Cube6= 16, // 方块6

            Black7 = 17, // 黑桃7
            Red7 = 18, // 红桃7
            Flower7 = 19, // 梅花7
            Cube7 = 20, // 方块7

            Black8 = 21, // 黑桃8
            Red8 = 22, // 红桃8
            Flower8 = 23, // 梅花8
            Cube8 = 24, // 方块8

            Black9 = 25, // 黑桃9
            Red9 = 26, // 红桃9
            Flower9 = 27, // 梅花9
            Cube9= 28, // 方块9

            Black10 = 29, // 黑桃10
            Red10 = 30, // 红桃10
            Flower10 = 31, // 梅花10
            Cube10 = 32, // 方块10

            BlackJ = 33, // 黑桃J
            RedJ =34, // 红桃J
            FlowerJ = 35, // 梅花J
            CubeJ = 36, // 方块J

            BlackQ = 37, // 黑桃Q
            RedQ = 38, // 红桃Q
            FlowerQ = 39, // 梅花Q
            CubeQ = 40, // 方块Q

            BlackK = 41, // 黑桃K
            RedK = 42, // 红桃K
            FlowerK = 43, // 梅花K
            CubeK = 44, // 方块K

            BlackA = 45, // 黑桃A
            RedA = 46, // 红桃A
            FlowerA = 47, // 梅花A
            CubeA = 48, // 方块A

            Black2 = 49, // 黑桃2
            Red2 = 50, // 红桃2
            Flower2 = 51, // 梅花2
            Cube2 = 52, // 方块2

            SmallJoker = 53, // 小王
            BigJoker = 54, // 大王

            Laizigou = 55,//癞子（花牌）

            Max = 56,
        }

        /// <summary>
        /// 卡牌类型
        /// </summary>
        public enum ECardType
        {
            Null = 0,
            Three = 1, // 3
            Four = 2, // 4
            Five = 3, // 5
            Six = 4, // 6
            Seven = 5, // 7
            Eight = 6, // 8
            Nine = 7, // 9
            Ten = 8, // 10
            J = 9, // J
            Q = 10, // Q
            K = 11, // K
            A = 12, // A
            Two = 13, // 2
            SmallJoker = 14, // 小王
            BigJoker = 15, // 大王
            Laizigou = 16,//癞子
        }

        /// <summary>
        /// 牌形
        /// </summary>
        public enum ECardFormType
        {
            Null = 0, // 错误牌型
            Single = 1, // 单牌  
            Pair = 2, // 对子  
            Three = 3, // 3不带  
            ThreeOne = 4, // 3带1  
            FourTwo = 5, // 4带2  
            Straight = 6, // 顺子
            StraightPair = 7, // --连对
            Aircraft = 8, // --飞机不带  
            AircraftWing = 9, // --飞机带单牌
            Bomb = 10, // --炸弹  
            KingBomb = 11, // --王炸
            ThreeBomb = 12,//3炸
            LaizigouBomb = 13,//癞子炸弹
            ThreeTwo = 14,//三带二
            Laizigou = 15,//癞子
            FourFour = 16,//四带两个对子
            AircraftPair = 17,
        }

        [ProtoContract]
        public class RemoteRoleBaseInfo
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 角色名
            /// </summary>
            [ProtoMember(2)]
            public string nickName = "";

            /// <summary>
            /// 性别
            /// </summary>
            [ProtoMember(3)]
            public ERoleSex sex = ERoleSex.Boy;

            /// <summary>
            /// 等级
            /// </summary>
            [ProtoMember(4)]
            public UInt32 level = 0;

            /// <summary>
            /// 该角色的ip
            /// </summary>
            [ProtoMember(5)]
            public string clientIp = "";

            /// <summary>
            /// 微信头像url
            /// </summary>
            [ProtoMember(6)]
            public string weixinHeadImgUrl = "";
        }

        /// <summary>
        /// 斗地主其他角色
        /// </summary>
        [ProtoContract]
        public class RemoteRoleOtherInfo
        {
            /// <summary>
            /// 方位
            /// </summary>
            [ProtoMember(1)]
            public ESeatPostion postion = ESeatPostion.Null;

            /// <summary>
            /// 是否在线
            /// </summary>
            [ProtoMember(2)]
            public bool isOnline = false;

            // 本房间分数
            [ProtoMember(3)]
            public int score = 0;

            /// <summary>
            /// 加倍倍数
            /// </summary>
            [ProtoMember(4)]
            public int scales = 0;

            /// <summary>
            /// 玩家状态
            /// </summary>
            [ProtoMember(5)]
            public ERoomRoleState roleState = ERoomRoleState.Null;

            /// <summary>
            /// 操作状态
            /// </summary>
            [ProtoMember(6)]
            public ERolePlayState playState = ERolePlayState.Null;

            /// <summary>
            /// 是否明牌
            /// </summary>
            [ProtoMember(7)]
            public bool bMingPai = false;

            /// <summary>
            /// 手牌，明牌时该字段才有效
            /// </summary>
            [ProtoMember(8)]
            public List<UInt32> handCards = new List<UInt32>();

            /// <summary>
            /// 手牌数量
            /// </summary>
            [ProtoMember(9)]
            public int handCardsCount = 0;

            /// <summary>
            /// 总倍数
            /// </summary>
            [ProtoMember(10)]
            public int totalScales = 0;
        }
        
        /// <summary>
        /// 其他角色信息
        /// </summary>
        [ProtoContract]
        public class RemoteRoleInfo
        {
            /// <summary>
            /// 基础信息
            /// </summary>
            [ProtoMember(1)]
            public RemoteRoleBaseInfo baseInfo = new RemoteRoleBaseInfo();

            /// <summary>
            /// 其他信息
            /// </summary>
            [ProtoMember(2)]
            public RemoteRoleOtherInfo otherInfo = new RemoteRoleOtherInfo();
        }

        /// <summary>
        /// 麻将本人角色信息
        /// </summary>
        [ProtoContract]
        public class LocalRoleInfo
        {
             /// <summary>
            /// 方位
            /// </summary>
            [ProtoMember(1)]
            public ESeatPostion position = ESeatPostion.Null;

            /// <summary>
            /// 分数
            /// </summary>
            [ProtoMember(2)]
            public int score = 0;

            /// <summary>
            /// 手牌
            /// </summary>
            [ProtoMember(3)]
            public List<UInt32> handCards = new List<UInt32>();

            /// <summary>
            /// 加倍倍数
            /// </summary>
            [ProtoMember(4)]
            public int scales = 0;

            /// <summary>
            /// 玩家状态
            /// </summary>
            [ProtoMember(5)]
            public ERoomRoleState roleState = ERoomRoleState.Null;

            /// <summary>
            /// 操作状态
            /// </summary>
            [ProtoMember(6)]
            public ERolePlayState playState = ERolePlayState.Null;

            /// <summary>
            /// 是否明牌
            /// </summary>
            [ProtoMember(7)]
            public bool bMingPai = false;

            /// <summary>
            /// 总倍数
            /// </summary>
            [ProtoMember(8)]
            public int totalScales = 0;

            /// <summary>
            /// 剩余时间
            /// </summary>
            [ProtoMember(9)]
            public int remainSecond = 0;
        }

        /// <summary>
        /// 出牌信息
        /// </summary>
        [ProtoContract]
        public class stOutCardItem
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            [ProtoMember(2)]
            public List<UInt32> lstCards = new List<UInt32>();

            /// <summary>
            /// 上回合出牌的癞子代替的牌
            /// </summary>
            [ProtoMember(3)]
            public UInt32 laiziType = 0;

            public void Clear()
            {
                roleid = 0;
                lstCards.Clear();
            }

            public void RecordOutCard(UInt64 roleid, List<UInt32> lstCards,UInt32 laiziType)
            {
                this.roleid = roleid;
                this.lstCards = lstCards;
                this.laiziType = laiziType;
            }
        }

        /// <summary>
        /// 房间信息
        /// </summary>
        [ProtoContract]
        public class RoomInfo
        {
            /// <summary>
            /// 房间唯一ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;

            /// <summary>
            /// 房间状态
            /// </summary>
            [ProtoMember(2)]
            public ERoomState roomState = ERoomState.WaitRoomer;

            /// <summary>
            /// 本人信息
            /// </summary>
            [ProtoMember(3)]
            public LocalRoleInfo localRoleInfo = new LocalRoleInfo();

            /// <summary>
            /// 房间其他玩家信息
            /// </summary>
            [ProtoMember(4)]
            public List<RemoteRoleInfo> lstRoles = new List<RemoteRoleInfo>();

            /// <summary>
            /// 第几牌局
            /// </summary>
            [ProtoMember(5)]
            public UInt32 currentCardRound = 0;

            /// <summary>
            /// 庄家
            /// </summary>
            [ProtoMember(6)]
            public UInt64 bankerRoleId = 0;

            /// <summary>
            /// 房间配置
            /// </summary>
            [ProtoMember(7)]
            public RoomConfig config = new RoomConfig();

            /// <summary>
            /// 房主
            /// </summary>
            [ProtoMember(8)]
            public stRoomOwner roomOwner = new stRoomOwner();

            /// <summary>
            /// 地主
            /// </summary>
            [ProtoMember(9)]
            public UInt64 lordRoleId = 0;

            /// <summary>
            /// 房间底牌
            /// </summary>
            [ProtoMember(10)]
            public List<UInt32> bottomCards = new List<UInt32>();

            /// <summary>
            /// 上次出牌信息
            /// </summary>
            [ProtoMember(11)]
            public stOutCardItem lastOutCardItem = new stOutCardItem();

            /// <summary>
            /// 解散信息
            /// </summary>
            [ProtoMember(12)]
            public stDismissInfo dismissInfo = new stDismissInfo();

            /// <summary>
            /// 当前最大叫分
            /// </summary>
            [ProtoMember(13)]
            public int maxFen = 0;
        };

        /// <summary>
        /// 房间局数
        /// </summary>
        public enum ERoomRound
        {
            Null = 0,
            OneRound = 1, // 1局
            FourRound = 2,// 4局
            EightRound = 3, // 8局
        }

        /// <summary>
        /// 付款方式
        /// </summary>
        [ProtoContract]
        public enum PayMethod
        {
            masterPay = 1,//房主付
            AAPay = 2,//AA制
            anotherPay = 3,//房主代开房
        }

        [ProtoContract]
        public class RoomConfig
        {
            /// <summary>
            /// 选择局数
            /// </summary>
            [ProtoMember(1)]
            public ERoomRound roomRound = ERoomRound.Null;

            /// <summary>
            /// 付款方式
            /// </summary>
            [ProtoMember(2)]
            public PayMethod payMethod = PayMethod.masterPay;

            /// <summary>
            /// 斗地主扣2张房卡
            /// </summary>
            public UInt32 roomCardCount
            {
                get
                {
                    if (payMethod.Equals(PayMethod.AAPay))
                    {
                        return 1;
                    }
                    return 3;
                }
            }

            /// <summary>
            /// 局数
            /// </summary>
            public int roomRoundCount
            {
                get
                {
                    if (roomRound == ERoomRound.OneRound)
                    {
                        return 1;
                    }
                    else if (roomRound == ERoomRound.FourRound)
                    {
                        return 4;
                    }
                    else if (roomRound == ERoomRound.EightRound)
                    {
                        return 8;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
        }

        /// <summary>
        /// (斗地主)创建房间
        /// </summary>
        [ProtoContract]
        public class LandlordCreateRoomReq : ProtoBody
        {
            [ProtoMember(1)]
            public RoomConfig config = new RoomConfig();
        };

        /// <summary>
        /// (斗地主)创建房间返回
        /// </summary>
        [ProtoContract]
        public class LandlordCreateRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 已经创建房间
                /// </summary>
                AlreadyCreateRoom = 2,

                /// <summary>
                /// 已经在其他房间里
                /// </summary>
                AlreadInRoom = 3,

                /// <summary>
                /// 房卡数量不足
                /// </summary>
                RoomCardNotEnough = 4,
            }

	        [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public RoomInfo roomInfo = new RoomInfo();
        };

        /// <summary>
        /// (斗地主)加入房间
        /// </summary>
        [ProtoContract]
        public class LandlordJoinRoomReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        };

        /// <summary>
        /// (斗地主)加入房间返回
        /// </summary>
        [ProtoContract]
        public class LandlordJoinRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 房间不存在
                /// </summary>
                RoomNotExist = 2,

                /// <summary>
                /// 房间已满
                /// </summary>
                RoomFull = 3,

                /// <summary>
                /// 房卡数量不足
                /// </summary>
                RoomCardNotEnough = 4,

                /// <summary>
                /// 房主不允许加入自己的房间
                /// </summary>
                AnthorNotJoin = 5,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public RoomInfo roomInfo = new RoomInfo();
        };

        /// <summary>
        /// (斗地主)加入房间通知
        /// </summary>
        [ProtoContract]
        public class LandlordJoinRoomNtf : ProtoBody
        {
            /// <summary>
            /// 人物信息
            /// </summary>
            [ProtoMember(1)]
            public RemoteRoleInfo remoteRoleInfo = new RemoteRoleInfo();
        };

        /// <summary>
        /// (斗地主)回合配置需要选择通知
        /// </summary>
        [ProtoContract]
        public class LandlordNeedRoundConfigSelectNtf : ProtoBody
        {
            /// <summary>
            /// 当前局数
            /// </summary>
            [ProtoMember(1)]
            public UInt32 roomRound = 0;
        };

        /// <summary>
        /// (斗地主)回合配置选择
        /// </summary>
        [ProtoContract]
        public class LandlordRoundConfigSelectReq : ProtoBody
        {
            /// <summary>
            /// 是否明牌开始
            /// </summary>
            [ProtoMember(1)]
            public bool bMingCardStart = false;
        };

        /// <summary>
        /// (斗地主)回合配置选择返回
        /// </summary>
        [ProtoContract]
        public class LandlordRoundConfigSelectRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)回合配置选择通知
        /// </summary>
        [ProtoContract]
        public class LandlordRoundConfigSelectNtf : ProtoBody
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 是否明牌开始
            /// </summary>
            [ProtoMember(2)]
            public bool bMingCardStart = false;
        };

        /// <summary>
        /// (斗地主)开始显示新牌通知
        /// </summary>
        [ProtoContract]
        public class LandlordNeedShowNewCardsNtf : ProtoBody
        {
            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(1)]
            public RoomInfo roomInfo = new RoomInfo();
        };

        /// <summary>
        /// (斗地主)明牌倍数
        /// </summary>
        [ProtoContract]
        public class LandlordMingCardScalesReq : ProtoBody
        {
            /// <summary>
            /// 明牌倍数
            /// </summary>
            [ProtoMember(1)]
            public int scales = 0;
        };

        /// <summary>
        /// (斗地主)明牌倍数请求返回
        /// </summary>
        [ProtoContract]
        public class LandlordMingCardScalesRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)明牌倍数请求通知
        /// </summary>
        [ProtoContract]
        public class LandlordMingCardScalesNtf : ProtoBody
        {
            /// <summary>
            /// 出牌角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 明牌倍数
            /// </summary>
            [ProtoMember(2)]
            public int scales = 1;

            /// <summary>
            /// 明牌时的手牌
            /// </summary>
            [ProtoMember(3)]
            public List<UInt32> handcards = new List<UInt32>();
        };


         /// <summary>
        /// 需要叫地主通知
        /// </summary>
         [ProtoContract]
        public class LandlordNeedCallLandlordNtf : ProtoBody
        {
            /// <summary>
            /// 叫地主玩家Id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleId = 0;

            /// <summary>
            /// 当前回合所叫的最高分
            /// </summary>
            [ProtoMember(2)]
            public int maxJIao = 0;
        }

        /// <summary>
        /// (斗地主)叫地主
        /// </summary>
        [ProtoContract]
        public class LandlordCallLandlordReq : ProtoBody
        {
            /// <summary>
            /// 是否叫地主 0表示不叫 1表示叫1分 2表示叫2分 3表示叫3分
            /// </summary>
            [ProtoMember(1)]
            public int jiaofen = 0;
        };

        /// <summary>
        /// (斗地主)叫地主返回
        /// </summary>
        [ProtoContract]
        public class LandlordCallLandlordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)叫地主通知
        /// </summary>
        [ProtoContract]
        public class LandlordCallLandlordNtf : ProtoBody
        {
            /// <summary>
            /// 角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 叫地主  0表示不叫 1表示1分 2表示2分 3表示3分
            /// </summary>
            [ProtoMember(2)]
            public int jiaofen = 0;
        };
 
        /// <summary>
        /// 需要抢地主通知
        /// </summary>
         [ProtoContract]
        public class LandlordNeedRobLandlordNtf : ProtoBody
        {
            /// <summary>
            /// 需要抢地主玩家Id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleId = 0;
        }

        /// <summary>
        /// (斗地主)抢地主
        /// </summary>
        [ProtoContract]
        public class LandlordRobLandlordReq : ProtoBody
        {
            /// <summary>
            /// 是否抢地主
            /// </summary>
            [ProtoMember(1)]
            public bool bRob = false;
        };

        /// <summary>
        /// (斗地主)抢地主返回
        /// </summary>
        [ProtoContract]
        public class LandlordRobLandlordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)抢地主通知
        /// </summary>
        [ProtoContract]
        public class LandlordRobLandlordNtf : ProtoBody
        {
            /// <summary>
            /// 角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 抢地主
            /// </summary>
            [ProtoMember(2)]
            public bool bRob = false;
        };

        /// <summary>
        /// (斗地主)抢地主最终确定通知
        /// </summary>
        [ProtoContract]
        public class LandlordRobLandlordResultNtf : ProtoBody
        {
            /// <summary>
            /// 地主角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 lordRoleId = 0;

            /// <summary>
            /// 地主的分
            /// </summary>
            [ProtoMember(2)]
            public int fen = 0;
        };

        /// <summary>
        /// (斗地主)显示加倍通知
        /// </summary>
        [ProtoContract]
        public class LandlordShowDoubleScalesNtf : ProtoBody
        {
            /// <summary>
            /// 剩余时间
            /// </summary>
            [ProtoMember(1)]
            public long remainSecond = 0;
        };

        /// <summary>
        /// (斗地主)加倍请求
        /// </summary>
        [ProtoContract]
        public class LandlordDoubleScalesReq : ProtoBody
        {
            /// <summary>
            /// 是否加倍
            /// </summary>
            [ProtoMember(1)]
            public bool bDoubleScales = false;
        };

        /// <summary>
        /// (斗地主)加倍请求返回
        /// </summary>
        [ProtoContract]
        public class LandlordDoubleScalesRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)加倍请求通知
        /// </summary>
        [ProtoContract]
        public class LandlordDoubleScalesNtf : ProtoBody
        {
            /// <summary>
            /// 角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 是否加倍
            /// </summary>
            [ProtoMember(2)]
            public bool bDoubleScales = false;
        };

        /// <summary>
        /// (斗地主)轮到你出牌了
        /// </summary>
        [ProtoContract]
        public class LandlordNeedHandOutCardNtf : ProtoBody
        {
            /// <summary>
            /// 是否必须出牌
            /// </summary>
            [ProtoMember(1)]
            public bool bMust = false;

            /// <summary>
            /// 需要出牌的角色ID
            /// </summary>
            [ProtoMember(2)]
            public UInt64 roleid = 0;
        };

        /// <summary>
        /// (斗地主)出牌请求
        /// </summary>
        [ProtoContract]
        public class LandlordHandOutCardReq : ProtoBody
        {
            /// <summary>
            /// 是否出牌
            /// </summary>
            [ProtoMember(1)]
            public bool bHandOut = false;

            /// <summary>
            /// 牌形
            /// </summary>
            [ProtoMember(2)]
            public ECardFormType cardForm = ECardFormType.Null;

            /// <summary>
            /// 牌列表
            /// </summary>
            [ProtoMember(3)]
            public List<UInt32> lstCards = new List<UInt32>();

            /// <summary>
            /// 癞子代替的牌的类型
            /// </summary>
            [ProtoMember(4)]
            public UInt32 laiziType = 0;
        };

        /// <summary>
        /// (斗地主)出牌请求返回
        /// </summary>
        [ProtoContract]
        public class LandlordHandOutCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)出牌通知
        /// </summary>
        [ProtoContract]
        public class LandlordHandOutCardNtf : ProtoBody
        {
            /// <summary>
            /// 出牌角色ID 
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 出牌或要不起
            /// </summary>
            [ProtoMember(2)]
            public bool bHandOut = false;

            /// <summary>
            /// 牌形
            /// </summary>
            [ProtoMember(3)]
            public ECardFormType cardForm = ECardFormType.Null;

            /// <summary>
            /// 牌列表
            /// </summary>
            [ProtoMember(4)]
            public List<UInt32> lstCards = new List<UInt32>();

            /// <summary>
            /// 癞子牌所替代的牌
            /// </summary>
            [ProtoMember(5)]
            public UInt32 laiziType = 0;
        };

        /// <summary>
        /// 结算角色信息
        /// </summary>

        [ProtoContract]
        public class RoundSettleRole
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 昵称
            /// </summary>
            [ProtoMember(2)]
            public string nickName = "";

            /// <summary>
            /// 倍数
            /// </summary>
            [ProtoMember(3)]
            public int scales = 0;

            /// <summary>
            /// 本局得分扣分
            /// </summary>
            [ProtoMember(4)]
            public int roundScore = 0;

            /// <summary>
            /// 是否是地主
            /// </summary>
            [ProtoMember(5)]
            public bool bLandlord = false;

            /// <summary>
            /// 结算牌
            /// </summary>
            [ProtoMember(6)]
            public List<UInt32> handCards = new List<UInt32>();

        }

        /// <summary>
        /// 公共倍数
        /// </summary>

        [ProtoContract]
        public class stPublicScales
        {
            /// <summary>
            /// 初始倍数
            /// </summary>
            [ProtoMember(1)]
            public int initScales = 1;

            /// <summary>
            /// 底牌倍数
            /// </summary>
            [ProtoMember(2)]
            public int bottomScales = 1;

            /// <summary>
            /// 明牌倍数
            /// </summary>
            [ProtoMember(3)]
            public int mingScales = 1;

            /// <summary>
            /// 炸弹倍数
            /// </summary>
            [ProtoMember(4)]
            public int bombScales = 1;

            /// <summary>
            /// 抢地主倍数
            /// </summary>
            [ProtoMember(5)]
            public int robScales = 1;

            /// <summary>
            /// 春天
            /// </summary>
            [ProtoMember(6)]
            public int springScales = 1;

            public void Clear()
            {
                initScales = 1;
                bombScales = 1;
                mingScales = 1;
                bombScales = 1;
                robScales = 1;
                springScales = 1;
            }

            public int totalScales
            {
                get
                {
                    return initScales * bottomScales * mingScales * bombScales * robScales * springScales;
                }
            }
        }

        /// <summary>
        /// 最终结算
        /// </summary>

        [ProtoContract]
        public class stFinalSettle
        {
            /// <summary>
            /// 各个玩家的最终结算分数
            /// </summary>
            [ProtoMember(1)]
            public Dictionary<UInt64, int> mapScores = new Dictionary<UInt64, int>();

            public void ChangeScore(UInt64 roleid, int score)
            {
                if (mapScores.ContainsKey(roleid))
                {
                    mapScores[roleid] += score;
                }
                else
                {
                    mapScores.Add(roleid, score);
                }
            }

            public void InitScore(UInt64 roleid)
            {
                if (!mapScores.ContainsKey(roleid))
                {
                    mapScores.Add(roleid, 0);
                }
            }
        }

        /// <summary>
        /// 结算信息
        /// </summary>

        [ProtoContract]
        public class RoundSettleInfo
        {
            /// <summary>
            /// 结算角色信息列表
            /// </summary>
            [ProtoMember(1)]
            public List<RoundSettleRole> lstSettleRoles = new List<RoundSettleRole>();

            /// <summary>
            /// 公共倍数
            /// </summary>
            [ProtoMember(2)]
            public stPublicScales publicScales = new stPublicScales();

            /// <summary>
            /// 赢牌角色ID
            /// </summary>
            [ProtoMember(3)]
            public UInt64 winRoleId = 0;
        }

        /// <summary>
        /// (斗地主)结算通知
        /// </summary>
        [ProtoContract]
        public class LandlordRoundSettleNtf : ProtoBody
        {
            /// <summary>
            /// 结算信息
            /// </summary>
            [ProtoMember(1)]
            public RoundSettleInfo settleInfo = new RoundSettleInfo();

            /// <summary>
            /// 房间是否结束
            /// </summary>
            [ProtoMember(2)]
            public bool IsRoomOver = false;

            /// <summary>
            /// 最终结算信息
            /// </summary>
            [ProtoMember(3)]
            public stFinalSettle finalSettle = new stFinalSettle();
        };

        /// <summary>
        /// (斗地主)获取房间信息
        /// </summary>
        [ProtoContract]
        public class LandlordGetRoomInfoReq : ProtoBody
        {
        };

        /// <summary>
        /// (斗地主)获取房间信息返回
        /// </summary>
        [ProtoContract]
        public class LandlordGetRoomInfoRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 房间信息
            /// </summary>
            [ProtoMember(2)]
            public RoomInfo roomInfo = new RoomInfo();
        };

        /// <summary>
        /// (斗地主)下一步
        /// </summary>
        [ProtoContract]
        public class LandlordNextStepReq : ProtoBody
        {
        };

        /// <summary>
        /// (斗地主)下一步消息返回
        /// </summary>
        [ProtoContract]
        public class LandlordNextStepRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)下一步
        /// </summary>
        [ProtoContract]
        public class LandlordNextStepNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;
        };

        /// <summary>
        /// (斗地主)解散游戏
        /// </summary>
        [ProtoContract]
        public class LandlordDismissGameReq : ProtoBody
        {
        };

        /// <summary>
        /// (斗地主)解散游戏返回
        /// </summary>
        [ProtoContract]
        public class LandlordDismissGameRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 解散倒计时
            /// </summary>
            [ProtoMember(2)]
            public int remainSecond = 0;

        };

        /// <summary>
        /// (斗地主)解散通知
        /// </summary>
        [ProtoContract]
        public class LandlordDismissGameNtf : ProtoBody
        {
            /// <summary>
            /// 点击解散游戏的人
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 解散倒计时
            /// </summary>
            [ProtoMember(2)]
            public int remainSecond = 0;
        };

        /// <summary>
        /// (斗地主)解散通过通知
        /// </summary>
        [ProtoContract]
        public class LandlordDismissGameSuccessNtf : ProtoBody
        {
            [ProtoMember(1)]
            public bool bSuccess = false;

            /// <summary>
            /// 最终结算信息
            /// </summary>
            [ProtoMember(2)]
            public stFinalSettle finalSettle = new stFinalSettle();
        };

        /// <summary>
        /// (斗地主)解散是否同意
        /// </summary>
        [ProtoContract]
        public class LandlordDismissAgreeReq : ProtoBody
        {
            [ProtoMember(1)]
            public bool bAgree = false;
        };

        /// <summary>
        /// (斗地主)解散是否同意返回
        /// </summary>
        [ProtoContract]
        public class LandlordDismissAgreeRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (推倒胡麻将)解散是否同意通知
        /// </summary>
        [ProtoContract]
        public class LandlordDismissAgreeNtf : ProtoBody
        {
            /// <summary>
            /// 角色id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 是否同意解散房间
            /// </summary>
            [ProtoMember(2)]
            public bool bAgree = false;
        };

        /// <summary>
        /// (斗地主)未开始时退出房间
        /// </summary>
        [ProtoContract]
        public class LandlordLeaveRoomReq : ProtoBody
        {
        };

        /// <summary>
        /// (斗地主)裂开房间返回
        /// </summary>
        [ProtoContract]
        public class LandlordLeaveRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)离开房间通知
        /// </summary>
        [ProtoContract]
        public class LandlordLeaveRoomNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            [ProtoMember(2)]
            public bool bRoomOwner = false;
        };

        /// <summary>
        /// (斗地主)倍数变化通知
        /// </summary>
        [ProtoContract]
        public class LandlordTotalScalesChangeNtf : ProtoBody
        {
            /// <summary>
            /// 自己总倍数
            /// </summary>
            [ProtoMember(1)]
            public int totalScales = 1;
        };

        /// <summary>
        /// (斗地主)聊天请求
        /// </summary>
        [ProtoContract]
        public class LandlordChatReq : ProtoBody
        {
            /// <summary>
            /// 聊天ID
            /// </summary>
            [ProtoMember(1)]
            public UInt32 chatid = 0;
        };

        /// <summary>
        /// (斗地主)聊天请求返回
        /// </summary>
        [ProtoContract]
        public class LandlordChatRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        };

        /// <summary>
        /// (斗地主)聊天请求通知
        /// </summary>
        [ProtoContract]
        public class LandlordChatNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 聊天ID
            /// </summary>
            [ProtoMember(2)]
            public UInt32 chatid = 0;
        };

        /// <summary>
        /// 用户聊天输入文字请求
        /// </summary>
        [ProtoContract]
        public class LandlordChatWordReq : ProtoBody
        {
            [ProtoMember(1)]
            public string chatWord = "";
        }

        /// <summary>
        /// 用户聊天输入文字返回
        /// </summary>
        [ProtoContract]
        public class LandlordChatWordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 用户聊天输入文字通知
        /// </summary>
        [ProtoContract]
        public class LandlordChatWordNtf : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roleId = 0;

            /// <summary>
            /// 用户输入的文字
            /// </summary>
            [ProtoMember(2)]
            public string chatWord = "";
        }

        [ProtoContract]
        public class LandlordTimerDoubleScaleNtf : ProtoBody
        {
        }

        /// <summary>
        /// 运城贴金获取两个玩家之间的距离
        /// </summary>
        [ProtoContract]
        public class LandlordDistanceReq : ProtoBody
        {
            /// <summary>
            /// 被看距离玩家id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid;
        }

        [ProtoContract]
        public class LandlordDistanceRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 两个玩家之间的距离
            /// </summary>
            [ProtoMember(2)]
            public string distance = "";

            [ProtoMember(3)]
            public string address = "";
        }

        /// <summary>
        /// 扔鸡蛋
        /// </summary>
        [ProtoContract]
        public class LandlordEggReq : ProtoBody
        {
            /// <summary>
            /// 被扔鸡蛋玩家id
            /// </summary>            
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 扔鸡蛋动画类型
            /// </summary>
            [ProtoMember(2)]
            public EggType eggType = EggType.egg;
        }

        /// <summary>
        /// 扔鸡蛋动画枚举
        /// </summary>
        public enum EggType : uint
        {
            cheer = 0,
            flower = 1,
            tomato = 2,
            shoe = 3,
            egg = 4
        }


        /// <summary>
        /// 扔鸡蛋动画返回
        /// </summary>
        [ProtoContract]
        public class LandlordEggRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 扔鸡蛋动画广播
        /// </summary>
        [ProtoContract]
        public class LandlordEggNtf : ProtoBody
        {
            /// <summary>
            /// 扔鸡蛋玩家id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 throwRoleid;

            /// <summary>
            /// 被扔鸡蛋玩家id
            /// </summary>
            [ProtoMember(2)]
            public UInt64 beRoleid;

            /// <summary>
            /// 扔鸡蛋动画枚举
            /// </summary>
            [ProtoMember(3)]
            public EggType eggType;
        }
    }
}