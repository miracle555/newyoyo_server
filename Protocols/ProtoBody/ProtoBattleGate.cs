﻿using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    /// <summary>
    /// 绑定战斗服务器
    /// </summary>
    [ProtoContract]
    public class BattleServerBindReq : ProtoBody
    {
    }

    /// <summary>
    /// 解除对战斗服务器的绑定
    /// </summary>
    [ProtoContract]
    public class BattleServerUnbindReq : ProtoBody
    {
    }
}