﻿/********************************************************************
	created:	2014/12/11
	created:	11:12:2014   19:24
	filename: 	CommonPlatform\Server\ServerInstance\CommonLib\Protocols\ProtoScene.cs
	file path:	CommonPlatform\Server\ServerInstance\CommonLib\Protocols
	file base:	ProtoScene
	file ext:	cs
	author:		刘冰生
	
	purpose:    Protocol for Login Module 
*********************************************************************/
using System;
using System.Collections.Generic;
using ProtoBuf;
using playerid = System.UInt64;

namespace Protocols
{
    namespace gm
    {
        /// <summary>
        /// GM修改自己密码
        /// </summary>
        [ProtoContract]
        public class GmModifySelfPasswordReq : ProtoBody
        {
            [ProtoMember(1)]
            public string account = "";//gm账号

            /// <summary>
            /// 新密码1
            /// </summary>
            [ProtoMember(2)]
            public string newPassword1 = "";

            /// <summary>
            /// 新密码2
            /// </summary>
            [ProtoMember(3)]
            public string newPassword2 = "";
        }

        /// <summary>
        /// gm修改自己密码
        /// </summary>
        [ProtoContract]
        public class GmModifySelfPasswordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 老密码错误
                /// </summary>
                OldPasswordWrong = 2,

                /// <summary>
                /// 新密码不一致
                /// </summary>
                NewPasswordNotSame = 3,

                /// <summary>
                /// 不是运营商账号，无修改密码权限
                /// </summary>
                NotGm = 4,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        [ProtoContract]
        public class stServerStatusInfo
        {
            /// <summary>
            /// 当前在线人数
            /// </summary>
            [ProtoMember(1)]
            public int roleCount = 0;

            /// <summary>
            /// 当前在线房间数
            /// </summary>
            [ProtoMember(2)]
            public int roomCount = 0;

            /// <summary>
            /// 推倒胡当前在线房间数
            /// </summary>
            [ProtoMember(3)]
            public int pushdownRoomCount = 0;

            /// <summary>
            /// 抠点当前在线房间数
            /// </summary>
            [ProtoMember(4)]
            public int pointRoomCount = 0;

            /// <summary>
            /// 斗地主当前在线房间数
            /// </summary>
            [ProtoMember(5)]
            public int lordRoomCount = 0;

            

        }

        /// <summary>
        /// 当日数据
        /// </summary>
        [ProtoContract]
        public class stOneDayData
        {
            /// <summary>
            /// 当日活跃人数
            /// </summary>
            [ProtoMember(1)]
            public int dayActiveRoleCount = 0;

            /// <summary>
            /// 当日充值金额
            /// </summary>
            [ProtoMember(2)]
            public int dayRecharge = 0;

            /// <summary>
            /// 当日房卡消耗数量
            /// </summary>
            [ProtoMember(3)]
            public int dayRoomCardConsume = 0;

            /// <summary>
            /// 当日平均付费金额
            /// </summary>
            [ProtoMember(4)]
            public double dayAveragePay = 0;

            /// <summary>
            /// 当日新增玩家数
            /// </summary>
            [ProtoMember(5)]
            public int dayNewAddRoleCount = 0;

            /// <summary>
            /// 当日最高同时在线
            /// </summary>
            [ProtoMember(6)]
            public int dayMaxRoleOnlineCount = 0;
        }

        /// <summary>
        /// 历史数据
        /// </summary>
        [ProtoContract]
        public class stHistoryData
        {
            /// <summary>
            /// 历史充值金额
            /// </summary>
            [ProtoMember(1)]
            public int totalRecharge = 0;

            /// <summary>
            /// 历史房卡消耗数量
            /// </summary>
            [ProtoMember(2)]
            public int totalRoomCardConsume = 0;

            /// <summary>
            /// 注册玩家总数
            /// </summary>
            [ProtoMember(3)]
            public int totalRole = 0;

            /// <summary>
            /// 历史微信充值房卡总数
            /// </summary>
            [ProtoMember(4)]
            public int totalWeiXinRechargeRoomCardCount = 0;

            /// <summary>
            /// 历史房间创建总数
            /// </summary>
            [ProtoMember(5)]
            public int totalCreateRoom = 0;
        }


        /// <summary>
        /// GM获取服务器当前状态
        /// </summary>
        [ProtoContract]
        public class GmGetServerCurrentStatusReq : ProtoBody
        {
        }

        /// <summary>
        /// gm获取服务器当前状态
        /// </summary>
        [ProtoContract]
        public class GmGetServerCurrentStatusRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是运营商账号，不能获取服务器状态
                /// </summary>
                NotGm = 2,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 服务器状态信息
            /// </summary>
            [ProtoMember(2)]
            public stServerStatusInfo serverInfo = new stServerStatusInfo();

            /// <summary>
            /// 当日数据
            /// </summary>
            [ProtoMember(3)]
            public stOneDayData dayInfo = new stOneDayData();

            /// <summary>
            /// 历史数据
            /// </summary>
            [ProtoMember(4)]
            public stHistoryData historyData = new stHistoryData();

        }

        /// <summary>
        /// GM创建授权账号
        /// </summary>
        [ProtoContract]
        public class GmCreateAgentAccountReq : ProtoBody
        {
            /// <summary>
            /// 账号
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 密码
            /// </summary>
            [ProtoMember(2)]
            public string password1 = "";

            /// <summary>
            /// 密码
            /// </summary>
            [ProtoMember(3)]
            public string password2 = "";

            /// <summary>
            /// 电话
            /// </summary>
            [ProtoMember(4)]
            public string phone = "";

            /// <summary>
            /// 姓名
            /// </summary>
            [ProtoMember(5)]
            public string name = "";

            /// <summary>
            /// 地区
            /// </summary>
            [ProtoMember(6)]
            public string area = "";

            /// <summary>
            /// 一级比例分成
            /// </summary>
            [ProtoMember(7)]
            public double oneLevelProportion = 0;

            /// <summary>
            /// 二级比例分成
            /// </summary>
            [ProtoMember(8)]
            public double twoLevelProportion = 0;
            
        }

        /// <summary>
        /// gm创建代理账号
        /// </summary>
        [ProtoContract]
        public class GmCreateAgentAccountRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是运营商账号，不能创建代理
                /// </summary>
                NotGm = 2,

                /// <summary>
                /// 输入的代理密码不一致
                /// </summary>
                PasswordNotSame = 3,

                /// <summary>
                /// 重复创建
                /// </summary>
                RepeatedCreate = 4,

                /// <summary>
                /// 没有找到代理账号
                /// </summary>
                NotFound = 5,

                /// <summary>
                /// 姓名不能为空
                /// </summary>
                NameNotNull = 6,

                /// <summary>
                /// 电话不能为空
                /// </summary>
                PhoneNotNull = 7,

                /// <summary>
                /// 没有修改gm账号权限
                /// </summary>
                NotQuanXian = 8,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// gm修改代理账号
        /// </summary>
        [ProtoContract]
        public class GmModifyAgentAccountReq : ProtoBody
        {
            /// <summary>
            /// 账号
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 密码
            /// </summary>
            [ProtoMember(2)]
            public string password1 = "";

            /// <summary>
            /// 密码
            /// </summary>
            [ProtoMember(3)]
            public string password2 = "";

            /// <summary>
            /// 电话
            /// </summary>
            [ProtoMember(4)]
            public string phone = "";

            /// <summary>
            /// 姓名
            /// </summary>
            [ProtoMember(5)]
            public string name = "";

            /// <summary>
            /// 地区
            /// </summary>
            [ProtoMember(6)]
            public string area = "";

            /// <summary>
            /// 一级比例分成
            /// </summary>
            [ProtoMember(7)]
            public double oneLevelProportion = 0;

            /// <summary>
            /// 二级比例分成
            /// </summary>
            [ProtoMember(8)]
            public double twoLevelProportion = 0;
        }

        [ProtoContract]
        public class GmModifyAgentAccountRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是运营商账号，不能修改代理密码
                /// </summary>
                NotGm = 2,

                /// <summary>
                /// 代理密码两次不一致
                /// </summary>
                PasswordNotSame = 3,

                /// <summary>
                /// 输入的账号ID不是代理
                /// </summary>
                InputAccountNotAgent = 3,

                /// <summary>
                /// 输入的账号ID不存在
                /// </summary>
                InputAccoutNotExist = 4,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// GM停用代理账号
        /// </summary>
        [ProtoContract]
        public class GmStopAgentAccountReq : ProtoBody
        {
            /// <summary>
            /// 账号ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountid = 0;
        }

        /// <summary>
        /// GM停用代理账号
        /// </summary>
        [ProtoContract]
        public class GmStopAgentAccountRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是运营商账号，不能停用代理
                /// </summary>
                NotGm = 2,

                /// <summary>
                /// 输入的账号ID不是代理
                /// </summary>
                InputAccountNotAgent = 3,

                /// <summary>
                /// 输入的账号ID不存在
                /// </summary>
                InputAccountNotExist = 4,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// GM增加房卡
        /// </summary>
        [ProtoContract]
        public class GmAddRoomCardReq : ProtoBody
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 房卡数量
            /// </summary>
            [ProtoMember(2)]
            public UInt32 roomcardCount = 0;
        }

        /// <summary>
        /// gm增加房卡返回
        /// </summary>
        [ProtoContract]
        public class GmAddRoomCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// GM扣除房卡
        /// </summary>
        [ProtoContract]
        public class GmDecRoomCardReq : ProtoBody
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 房卡数量
            /// </summary>
            [ProtoMember(2)]
            public UInt32 roomcardCount = 0;
        }

        /// <summary>
        /// gm减少房卡返回
        /// </summary>
        [ProtoContract]
        public class GmDecRoomCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 代理转移房卡
        /// </summary>
        [ProtoContract]
        public class GmTransferRoomCardReq : ProtoBody
        {
            /// <summary>
            /// 角色ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 房卡数量
            /// </summary>
            [ProtoMember(2)]
            public UInt32 roomcardCount = 0;

            
        }

        /// <summary>
        /// 代理转移房卡返回
        /// </summary>
        [ProtoContract]
        public class GmTransferRoomCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是代理，没有转移房卡的权限
                /// </summary>
                NotAgent = 2,

                /// <summary>
                /// 代理（自己）房卡数量不足
                /// </summary>
                RoomCardNotEnough = 3,

                /// <summary>
                /// 输入的账号ID不存在
                /// </summary>
                InputAccountNotExist = 4,

                /// <summary>
                /// 账号已停权
                /// </summary>
                AccountStopPower = 5,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 转移成功后自己剩余的房卡数量，只有错误码为Success时才有用
            /// </summary>
            [ProtoMember(2)]
            public UInt32 diamond = 0;
        }

        /// <summary>
        /// gm发公告
        /// </summary>
        [ProtoContract]
        public class GmPublicNoticeReq : ProtoBody
        {
            /// <summary>
            /// 公告
            /// </summary>
            [ProtoMember(1)]
            public string notice = "";
        }

        /// <summary>
        /// gm发公告返回
        /// </summary>
        [ProtoContract]
        public class GmPublicNoticeRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// gm查询代理房卡流水
        /// </summary>
        [ProtoContract]
        public class GmQueryAgentRoomCardEventsReq : ProtoBody
        {
            /// <summary>
            /// 查询日期
            /// </summary>
            [ProtoMember(1)]
            public string strDate = "";

            /// <summary>
            /// 代理roleid
            /// </summary>
            [ProtoMember(2)]
            public UInt64 roleid = 0;
        }

        /// <summary>
        /// gm查询代理房卡流水返回
        /// </summary>
        [ProtoContract]
        public class GmQueryAgentRoomCardEventsRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 代理流水
            /// </summary>
            [ProtoMember(2)]
            public List<stRCEventItem> agentEvents = new List<stRCEventItem>();
        }

        /// <summary>
        /// 代理查询流水事件
        /// </summary>
        [ProtoContract]
        public class AgemtQueryRoomCardEventsReq : ProtoBody
        {
            /// <summary>
            /// 查询日期
            /// </summary>
            [ProtoMember(1)]
            public string strDate = "";
        }

        /// <summary>
        /// 代理查询流水事件返回
        /// </summary>
        [ProtoContract]
        public class AgemtQueryRoomCardEventsRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 代理流水
            /// </summary>
            [ProtoMember(2)]
            public List<stRCEventItem> agentEvents = new List<stRCEventItem>();
        }

        /// <summary>
        /// gm获取玩家信息
        /// </summary>
        [ProtoContract]
        public class GmGetPlayerInfoReq : ProtoBody
        {
            /// <summary>
            /// 账号id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;
        }

        /// <summary>
        /// gm获取玩家返回
        /// </summary>
        [ProtoContract]
        public class GmGetPlayerInfoRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 钻石数，房卡数
            /// </summary>
            [ProtoMember(2)]
            public UInt32 diamond = 0;

            /// <summary>
            /// 玩家累计金额数
            /// </summary>
            [ProtoMember(3)]
            public decimal redPacketMoney = 0;
        }

        /// <summary>
        /// gm给代理账号冲房卡
        /// </summary>
        [ProtoContract]
        public class GmAddAgentRoomCardReq : ProtoBody
        {
            /// <summary>
            /// 代理账号
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 房卡
            /// </summary>
            [ProtoMember(2)]
            public UInt32 roomcardCount = 0;

            /// <summary>
            /// 代理名字
            /// </summary>
            [ProtoMember(3)]
            public string name = "";

        }

        [ProtoContract]
        public class GmAddAgentRoomCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 不是Gm账号
                /// </summary>
                NotIsGm = 2,

                /// <summary>
                /// 不是代理账号
                /// </summary>
                NotIsAgent = 3,

                /// <summary>
                /// 账号不存在
                /// </summary>
                NotFound = 4,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 钻石数，房卡数
            /// </summary>
            [ProtoMember(2)]
            public UInt32 diamond = 0;
        }

        /// <summary>
        /// gm给代理账号减房卡
        /// </summary>
        [ProtoContract]
        public class GmDecAgentRoomCardReq : ProtoBody
        {
            /// <summary>
            /// 代理账号
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 房卡
            /// </summary>
            [ProtoMember(2)]
            public UInt32 roomcardCount = 0;

           

        }

        [ProtoContract]
        public class GmDecAgentRoomCardRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 不是Gm
                /// </summary>
                NotIsGm = 2,

                /// <summary>
                /// 不是代理商
                /// </summary>
                NotIsAgent = 3,

            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 钻石数，房卡数
            /// </summary>
            [ProtoMember(2)]
            public UInt32 diamond = 0;
        }

        /// <summary>
        /// 代理授权
        /// </summary>
        [ProtoContract]
        public class GmAgentImpowerReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 accountId = 0;
        }

        [ProtoContract]
        public class GmAgentImpowerRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 没有找到该代理商
                /// </summary>
                NotFound = 2,

                /// <summary>
                /// 不是代理商账号
                /// </summary>
                NotIsAgent = 3,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 代理停权
        /// </summary>
        [ProtoContract]
        public class GmAgentStopPowerReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 accountId = 0;
        }

        [ProtoContract]
        public class GmAgentStopPowerRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 没有找到该代理商
                /// </summary>
                NotFound = 2,

                /// <summary>
                /// 不是代理商账号
                /// </summary>
                NotIsAgent = 3,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 清理房间
        /// </summary>
        [ProtoContract]
        public class GmClearRoomReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 roomUuid = 0;
        }

        [ProtoContract]
        public class GmClearRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 清理玩家房间数据
        /// </summary>
        [ProtoContract]
        public class GmClearPlayerRoomReq : ProtoBody
        {
            /// <summary>
            /// 要被清理房间数据的玩家
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;
        }

        /// <summary>
        /// 清理玩家房间数据
        /// </summary>
        [ProtoContract]
        public class GmClearPlayerRoomRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        public enum eAgencyState
        {
            /// <summary>
            /// 授权
            /// </summary>
            imPower,

            /// <summary>
            /// 停权
            /// </summary>
            rightToStop,
        }

        /// <summary>
        /// 代理商
        /// </summary>
        [ProtoContract]
        public class stAgentData : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 房卡余额
            /// </summary>
            [ProtoMember(2)]
            public int roomCard = 0;

            /// <summary>
            /// 代理累计房卡充值
            /// </summary>
            [ProtoMember(3)]
            public int roomCardRecharge = 0;

            /// <summary>
            /// 代理累计房卡消耗
            /// </summary>
            [ProtoMember(4)]
            public int roomCardConsume = 0;

            /// <summary>
            /// 代理状态(授权/停权)
            /// </summary>
            [ProtoMember(5)]
            public eAgencyState agencyState;

            /// <summary>
            /// 返利金额
            /// </summary>
            [ProtoMember(6)]
            public double money = 0;

            /// <summary>
            /// 名字
            /// </summary>
            [ProtoMember(7)]
            public string name = "";

            /// <summary>
            /// 电话
            /// </summary>
            [ProtoMember(8)]
            public string phone = "";

            /// <summary>
            /// 地区
            /// </summary>
            [ProtoMember(9)]
            public string area = "";

            /// <summary>
            /// 下级用户数
            /// </summary>
            [ProtoMember(10)]
            public int lowerLevelUsersCount = 0;

            /// <summary>
            /// 一级分成比例
            /// </summary>
            [ProtoMember(11)]
            public double oneLevelProportion = 0;

            /// <summary>
            /// 二级比例分成
            /// </summary>
            [ProtoMember(12)]
            public double twoLevelProportion = 0;

            /// <summary>
            /// 代理密码
            /// </summary>
            [ProtoMember(13)]
            public string password = "";
        }

        /// <summary>
        /// 获取代理商列表
        /// </summary>
        [ProtoContract]
        public class GmGetAgentListReq : ProtoBody
        {

        }

        [ProtoContract]
        public class GmGetAgentListRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 不是GM
                /// </summary>
                NotIsGM = 2,

                /// <summary>
                /// 没有代理商
                /// </summary>
                NoAgent = 3,

            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 代理商列表
            /// </summary>
            [ProtoMember(2)]
            public List<stAgentData> agentData = new List<stAgentData>();

        }

        /// <summary>
        /// 用户信息
        /// </summary>
        [ProtoContract]
        public class stUserData
        {
            /// <summary>
            /// 账号id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 昵称
            /// </summary>
            [ProtoMember(2)]
            public string name = "";

            /// <summary>
            /// 剩余房卡数
            /// </summary>
            [ProtoMember(3)]
            public uint roomCard = 0;
        }
        /// <summary>
        /// gm查看代理下级用户列表请求
        /// </summary>
        [ProtoContract]
        public class GmQuerylowerLevelUserListReq : ProtoBody
        {
            /// <summary>
            /// 代理id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;
        }

        [ProtoContract]
        public class GmQuerylowerLevelUserListRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 不是代理商
                /// </summary>
                NotIsAgent = 2,

                /// <summary>
                /// 没有gm权限
                /// </summary>
                NotIsGm = 3

            }
            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 下级用户列表
            /// </summary>
            [ProtoMember(2)]
            public List<stUserData> lstUserData = new List<stUserData>(); 
        }

        /// <summary>
        /// 获取代理数据
        /// </summary>
        [ProtoContract]
        public class AgentDataReq : ProtoBody
        {
            /// <summary>
            /// 代理商id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;
        }

        [ProtoContract]
        public class AgentDataRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 不是代理商
                /// </summary>
                NotIsAgent = 2,

            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 代理累计房卡充值
            /// </summary>
            [ProtoMember(2)]
            public int roomCardRecharge = 0;

            /// <summary>
            /// 代理累计房卡消耗
            /// </summary>
            [ProtoMember(3)]
            public int roomCardConsume = 0;

            /// <summary>
            /// 代理状态(授权/停权)
            /// </summary>
            [ProtoMember(4)]
            public eAgencyState agencyState;

            /// <summary>
            /// 返利累计金额
            /// </summary>
            [ProtoMember(5)]
            public decimal money = 0;

            /// <summary>
            /// 历史返利金额
            /// </summary>
            [ProtoMember(6)]
            public double totalMoney = 0;

            /// <summary>
            /// 推广用户数
            /// </summary>
            [ProtoMember(7)]
            public uint userCount = 0;

            /// <summary>
            /// 代理商id
            /// </summary>
            [ProtoMember(8)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 代理商名字
            /// </summary>
            [ProtoMember(9)]
            public string name = "";

            /// <summary>
            /// 代理商房卡
            /// </summary>
            [ProtoMember(10)]
            public UInt32 roomCard = 0;

        }

        /// <summary>
        /// 提现
        /// </summary>
        [ProtoContract] 
        public class GmBettleMentReq : ProtoBody
        {
            /// <summary>
            /// 代理商id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 提现金额
            /// </summary>
            [ProtoMember(2)]
            public double money = 0;
        }

        /// <summary>
        /// 提现返回
        /// </summary>
        [ProtoContract]
        public class GmBettleMentRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 代理商未找到
                /// </summary>
                NotFound = 2,

                /// <summary>
                /// 不是代理商
                /// </summary>
                NotIsAgent = 3,

                /// <summary>
                /// Gm账号为找到
                /// </summary>
                NotIsGm = 4,

                /// <summary>
                /// 提现金额不足
                /// </summary>
                MoneyNotEnough = 5,

            }

            [ProtoMember(1)]
            public ErrorID errorCode;

        }
        /// <summary>
        /// gm查询代理返利流水
        /// </summary>
        [ProtoContract]
        public class GmQueryAgentRebateEventsReq : ProtoBody
        {
            /// <summary>
            /// 代理Id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 日期
            /// </summary>
            [ProtoMember(2)]
            public string strDate = "";
        }

        /// <summary>
        /// gm查询代理返利流水返回
        /// </summary>
        [ProtoContract]
        public class GmQueryAgentRebateEventsRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是gm
                /// </summary>
                NotIsGm = 2,
            }
            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            ///代理Id
            /// </summary>
            [ProtoMember(2)]
            public UInt64 id = 0;
            /// <summary>
            ///代理姓名
            /// </summary>
            /// 
            [ProtoMember(3)]
            public string name = "";

            /// <summary>
            ///提现金额
            /// </summary>
            /// 
            [ProtoMember(3)]
            public decimal money = 0;
        }

        [ProtoContract]
        public class stUserRechargeData
        {
            /// <summary>
            /// 日期时间
            /// </summary>
            [ProtoMember(1)]
            public DateTime dateTime = DateTime.MinValue;

            /// <summary>
            /// 用户ID
            /// </summary>
            [ProtoMember(2)]
            public UInt64 roleId = 0;

            /// <summary>
            /// 用户名字
            /// </summary>
            [ProtoMember(3)]
            public string name = "";

            /// <summary>
            /// 充值金额
            /// </summary>
            [ProtoMember(4)]
            public Int64 money = 0;       
        }

        /// <summary>
        /// gm查询用户充值流水请求
        /// </summary>
        [ProtoContract]
        public class GmQueryUserRechargeEventReq : ProtoBody
        {
            /// <summary>
            /// 代理Id
            /// </summary>
            [ProtoMember(1)]
            public UInt64 accountId = 0;

            /// <summary>
            /// 日期
            /// </summary>
            [ProtoMember(2)]
            public string strDate = "";

            /// <summary>
            /// 是否是默认查询
            /// </summary>
            [ProtoMember(3)]
            public bool isCheckAll = false;

            /// <summary>
            /// 页数
            /// </summary>
            [ProtoMember(4)]
            public int page = 0;
        }

        /// <summary>
        /// gm查询用户充值返回
        /// </summary>
        [ProtoContract]
        public class GmQueryUserRechargeEventRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是gm
                /// </summary>
                NotIsGm = 2,

                /// <summary>
                /// 未找到该玩家
                /// </summary>
                NotFound = 3
            }
            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 总共的页数
            /// </summary>
            [ProtoMember(2)]
            public int totalPage = 0;

            /// <summary>
            /// 用户充值列表
            /// </summary>
            [ProtoMember(3)]
            public List<stUserRechargeData> userRechargeDate= new List<stUserRechargeData>();
        }


        /// <summary>
        /// 代理查询返利流水事件
        /// </summary>
        [ProtoContract]
        public class AgentQueryRebateEventsReq : ProtoBody
        {
            /// <summary>
            /// 日期
            /// </summary>
            [ProtoMember(1)]
            public string strDate = "";
        }

        /// <summary>
        /// 代理查询返利流水事件返回
        /// </summary>
        [ProtoContract]
        public class AgentQueryRebateEventsRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                /// <summary>
                /// 成功
                /// </summary>
                Success = 0,

                /// <summary>
                /// 失败
                /// </summary>
                Failed = 1,

                /// <summary>
                /// 不是代理商
                /// </summary>
                NotIsAgent = 2,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 返利流水
            /// </summary>
            [ProtoMember(2)]
            public List<stRCEventRebateItem> rebateEvent = new List<stRCEventRebateItem>();

        }

        /// <summary>
        /// 测试充值时返利
        /// </summary>
        [ProtoContract]
        public class TestRechargeReq : ProtoBody
        {
            /// <summary>
            /// 谁充值
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 充值表ID
            /// </summary>
            [ProtoMember(2)]
            public UInt32 rechargeid = 0;
        }

        [ProtoContract]
        public class TestRechargeRsp : ProtoBody
        {
            public enum ErrorID
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 不是GM账号
                /// </summary>
                NotIsGm = 2,

                /// <summary>
                /// 目标玩家不存在
                /// </summary>
                TargetNotExist = 3,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// GM用户列表请求
        /// </summary>
        [ProtoContract]
        public class UserInfoListReq : ProtoBody
        {
            /// <summary>
            /// 页数
            /// </summary>
            [ProtoMember(1)]
            public int page = 0;

            /// <summary>
            /// 用户id
            /// </summary>
            [ProtoMember(2)]
            public UInt64 accountId = 0;
        }

        /// <summary>
        /// GM用户列表返回
        /// </summary>
        [ProtoContract]
        public class UserInfoListRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 总共的页数
            /// </summary>
            [ProtoMember(2)]
            public int totalPage = 0;

            /// <summary>
            /// 用户列表
            /// </summary>
            [ProtoMember(3)]
            public List<stUserData> lstUserData = new List<stUserData>();
        }

        /// <summary>
        /// 代理查询用户列表请求
        /// </summary>
        [ProtoContract]
        public class AgentUserListReq : ProtoBody
        {

        }

        /// <summary>
        /// 代理查询用户列表返回
        /// </summary>
        [ProtoContract]
        public class AgentUserListRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            /// <summary>
            /// 用户列表
            /// </summary>
            [ProtoMember(3)]
            public List<stUserData> lstUserData = new List<stUserData>();
        }

        /// <summary>
        /// 代理修改密码
        /// </summary>
        [ProtoContract]
        public class AgentModifyPasswordReq : ProtoBody
        {
            /// <summary>
            /// 密码
            /// </summary>
            [ProtoMember(1)]
            public string password = "";

            [ProtoMember(2)]
            public string password1 = "";
        }

        [ProtoContract]
        public class AgentModifyPasswordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 密码不一致
                /// </summary>
                NotEqual = 2,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }

        /// <summary>
        /// 提现申请
        /// </summary>
        [ProtoContract]
        public class DeductionsMoneyReq : ProtoBody
        {
            /// <summary>
            /// 提现申请人ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 提现金额
            /// </summary>
            [ProtoMember(2)]
            public decimal money = 0;
          
        }

        /// <summary>
        /// 申请提现返回
        /// </summary>
        [ProtoContract]
        public class DeductionsMoneyRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,

                /// <summary>
                /// 余额不足
                /// </summary>
                NotSufficientFunds = 2,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;

            [ProtoMember(2)]
            public decimal redPacketMoney = 0;

            [ProtoMember(3)]
            public UInt64 playerID = 0;
        }

        ///转让记录请求
        [ProtoContract]
        public class GmQueryTurnRecordReq : ProtoBody
        {
            /// <summary>
            /// 转让记录人ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;

            /// <summary>
            /// 查询是不是自己的记录
            /// </summary>
            [ProtoMember(2)]
            public bool isSelf;
        }

        ///记录数据
        ///
        [ProtoContract]
        public class GmQueryTurnRecord
        {
            //ID
            [ProtoMember(1)]
            public UInt64 service = 0;
            //昵称
            [ProtoMember(2)]
            public string name = "";
            //记录数据
            [ProtoMember(3)]
            public decimal number = 0;
            //时间
            [ProtoMember(4)]
            public DateTime time = new DateTime();

        }

        ///转让记录返回
        [ProtoContract]
        public class GmQueryTurnRecordRsp : ProtoBody
        {

            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
            /// <summary>
            /// 转让数据
            /// </summary>
            [ProtoMember(2)]
            public List<GmQueryTurnRecord> list = new List<GmQueryTurnRecord>();   
        }

        ///扣除记录请求
        [ProtoContract]
        public class GmQueryDeductRecordReq : ProtoBody
        {

            /// <summary>
            /// 扣除记录人ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;
            /// <summary>
            /// 查询是不是自己的记录
            /// </summary>
            [ProtoMember(2)]
            public bool isSelf = true;
        }

        ///扣除记录返回
        [ProtoContract]
        public class GmQueryDeductRecordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
            /// <summary>
            /// 扣除数据
            /// </summary>
            [ProtoMember(2)]
            public List<GmQueryTurnRecord> list = new List<GmQueryTurnRecord>();
        }

        ///提现记录请求
        [ProtoContract]
        public class GmQueryTixianRecordReq : ProtoBody
        {
            // <summary>
            /// 扣除记录人ID
            /// </summary>
            [ProtoMember(1)]
            public UInt64 roleid = 0;
            /// <summary>
            /// 查询是不是自己的记录
            /// </summary>
            [ProtoMember(2)]
            public bool isSelf = true;
        }

        ///提现记录返回
        [ProtoContract]
        public class GmQueryTixianRecordRsp : ProtoBody
        {

            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,
            }


            [ProtoMember(1)]
            public ErrorID errorCode;
            /// <summary>
            /// 提现数据
            /// </summary>
            [ProtoMember(2)]
            public List<GmQueryTurnRecord> list = new List<GmQueryTurnRecord>();
        }

        ///重置密码请求
        [ProtoContract]
        public class GmResetServicePasswordReq : ProtoBody
        {
            [ProtoMember(1)]
            public UInt64 i=0;
        }

        ///重置密码返回
        [ProtoContract]
        public class GmResetServicePasswordRsp : ProtoBody
        {
            public enum ErrorID : uint
            {
                Success = 0,

                Failed = 1,           
            }

            [ProtoMember(1)]
            public ErrorID errorCode;
        }


    }
}