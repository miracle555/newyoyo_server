﻿using System;
using ProtoBuf;
using System.Collections.Generic;

namespace Protocols
{
    [ProtoContract]
    public class VersionProto
    {
        [ProtoMember(1)]
        public string version;

        [ProtoMember(2)]
        public VersionDataInfo data;
    }

    [ProtoContract]
    public class GetVersionDataReq : ProtoBody
    {
        [ProtoMember(1)]
        public string tag;

        [ProtoMember(2)]
        public string version;
    }

    [ProtoContract]
    public class VersionDataInfo : ProtoBody
    {
    }

    [ProtoContract]
    public class GetVersionDataRsp : ProtoBody
    {
        [ProtoMember(1)]
        public string tag;

        [ProtoMember(2)]
        public string version;

        [ProtoMember(3)]
        public VersionProto data;
    }
}