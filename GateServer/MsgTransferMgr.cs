﻿/********************************************************************
	created:	2017/5/22
	purpose:    消息转发管理器	
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Log;
using Utility.Debugger;
using Protocols;

namespace GateServer
{
    public class MsgTransferMgr
    {
        public MsgTransferMgr()
        {

        }

        public bool Init()
        {
            InitBattleMsgs();
            return true;
        }

        public bool IsTrans2Battle(MSGID id)
        {
            return setBattleMsgs.Contains(id);
        }

        /// <summary>
        /// 转发战斗服的消息
        /// </summary>
        HashSet<MSGID> setBattleMsgs = new HashSet<MSGID>();
        void InitBattleMsgs()
        {
            // 推到胡麻将转发Battle的消息
            setBattleMsgs.Add(MSGID.MJPushDownHandOutCardReq); //(推倒胡麻将)出牌请求
            setBattleMsgs.Add(MSGID.MJPushDownPengCardReq);// (推倒胡麻将)碰牌请求
            setBattleMsgs.Add(MSGID.MJPushDownHuCardReq); // (推倒胡麻将)胡牌请求
            setBattleMsgs.Add(MSGID.MJPushDownGangCardReq); // (推倒胡麻将)杠牌请求
            setBattleMsgs.Add(MSGID.MJPushDownReadyCardReq); // (推倒胡麻将)听牌请求
            setBattleMsgs.Add(MSGID.MJPushDownPassReq); // (推倒胡麻将)过
            setBattleMsgs.Add(MSGID.MJPushDownNextStepReq); // (推倒胡麻将)下一步
            setBattleMsgs.Add(MSGID.MJPushDownDismissGameReq); // (推倒胡麻将)解散游戏
            setBattleMsgs.Add(MSGID.MJPushDownLackingOneDoorReq); // (推倒胡麻将)缺一门选择
            setBattleMsgs.Add(MSGID.MJPushDownLeaveRoomReq); // 离开房间，房间未开始时
            setBattleMsgs.Add(MSGID.MJPushDownDismissAgreeReq); // 解散是否同意
            setBattleMsgs.Add(MSGID.MJPushDownCheckSelfGangHuReadyReq); // 检测自己碰杠胡听
            setBattleMsgs.Add(MSGID.MJPushDownChatReq); // 聊天
            setBattleMsgs.Add(MSGID.MJPushDownChatWordReq);//文字聊天

            // 抠点麻将转发Battle的消息
            setBattleMsgs.Add(MSGID.MJPointHandOutCardReq); //(抠点)出牌请求
            setBattleMsgs.Add(MSGID.MJPointPengCardReq);// (抠点)碰牌请求
            setBattleMsgs.Add(MSGID.MJPointHuCardReq); // (抠点)胡牌请求
            setBattleMsgs.Add(MSGID.MJPointGangCardReq); // (抠点)杠牌请求
            setBattleMsgs.Add(MSGID.MJPointReadyCardReq); // (抠点)听牌请求
            setBattleMsgs.Add(MSGID.MJPointPassReq); // (抠点)过
            setBattleMsgs.Add(MSGID.MJPointNextStepReq); // (抠点)下一步
            setBattleMsgs.Add(MSGID.MJPointDismissGameReq); // (抠点)解散游戏
            setBattleMsgs.Add(MSGID.MJPointLeaveRoomReq); // 离开房间，房间未开始时
            setBattleMsgs.Add(MSGID.MJPointDismissAgreeReq); // 解散是否同意
            setBattleMsgs.Add(MSGID.MJPointCheckSelfGangHuReadyReq); // 检测自己碰杠胡听
            setBattleMsgs.Add(MSGID.MJPointChatReq); // 聊天
            setBattleMsgs.Add(MSGID.MJPointChatWordReq);//文字聊天
            // 斗地主消息
            setBattleMsgs.Add(MSGID.LandlordRoundConfigSelectReq); // (斗地主)回合配置需要选择
            setBattleMsgs.Add(MSGID.LandlordMingCardScalesReq); // (斗地主)明牌倍数
            setBattleMsgs.Add(MSGID.LandlordRobLandlordReq); // (斗地主)抢地主
            setBattleMsgs.Add(MSGID.LandlordCallLandlordReq); // (斗地主)叫地主
            setBattleMsgs.Add(MSGID.LandlordDoubleScalesReq); // (斗地主)加倍请求
            setBattleMsgs.Add(MSGID.LandlordHandOutCardReq); // (斗地主)出牌请求
            setBattleMsgs.Add(MSGID.LandlordNextStepReq); //(斗地主)下一步
            setBattleMsgs.Add(MSGID.LandlordDismissGameReq); // (斗地主)解散游戏
            setBattleMsgs.Add(MSGID.LandlordDismissAgreeReq); // 解散是否同意
            setBattleMsgs.Add(MSGID.LandlordLeaveRoomReq); // (斗地主)人数未满时离开房间\
            setBattleMsgs.Add(MSGID.LandlordChatReq); // (斗地主)聊天
            setBattleMsgs.Add(MSGID.LandlordChatWordReq);//文字聊天
            setBattleMsgs.Add(MSGID.LandlordEggReq);

            // 扎金花消息
            setBattleMsgs.Add(MSGID.GoldenFlowerShowCardReq);//看牌通知
            setBattleMsgs.Add(MSGID.GoldenFlowerDarkCardReq);//跟注通知
            setBattleMsgs.Add(MSGID.GoldenFlowerRaiseCardReq);//押注通知
            setBattleMsgs.Add(MSGID.GoldenFlowerHandOutCardReq);//比牌请求            
            setBattleMsgs.Add(MSGID.GoldenFlowerCallCardReq);//弃牌通知
            setBattleMsgs.Add(MSGID.GoldenFlowerDismissAgreeReq);// 解散是否同意
            setBattleMsgs.Add(MSGID.GoldenFlowerDismissGameReq);// (扎金花)解散游戏
            setBattleMsgs.Add(MSGID.GoldenFlowerLeaveRoomReq);// (扎金花)离开房间，房间未开始时
            setBattleMsgs.Add(MSGID.GoldenFlowerChatReq);// (扎金花)聊天
            setBattleMsgs.Add(MSGID.GoldenFlowerChatWordReq); // (扎金花)文字聊天
            setBattleMsgs.Add(MSGID.GoldenFlowerStartReq);//开始游戏
            setBattleMsgs.Add(MSGID.GoldenFlowerNextStepReq);//下一步请求          
            setBattleMsgs.Add(MSGID.GoldenFlowerStartFenReq);//开始底分请求
            setBattleMsgs.Add(MSGID.GoldenFlowerEggReq);



            //运城贴金消息转发
            setBattleMsgs.Add(MSGID.MJYunChengHandOutCardReq);
            setBattleMsgs.Add(MSGID.MJYunChengPengCardReq);
            setBattleMsgs.Add(MSGID.MJYunChengHuCardReq);
            setBattleMsgs.Add(MSGID.MJYunChengGangCardReq);
            setBattleMsgs.Add(MSGID.MJYunChengReadyCardReq);
            setBattleMsgs.Add(MSGID.MJYunChengPassReq);
            setBattleMsgs.Add(MSGID.MJYunChengNextStepReq);
            setBattleMsgs.Add(MSGID.MJYunChengDismissGameReq);
            setBattleMsgs.Add(MSGID.MJYunChengDismissAgreeReq);
            setBattleMsgs.Add(MSGID.MJYunChengLeaveRoomReq);
            setBattleMsgs.Add(MSGID.MJYunChengChatReq);
            setBattleMsgs.Add(MSGID.MJYunChengChatWordReq);
            setBattleMsgs.Add(MSGID.MJYunChengEggReq);

        }
    }
}
