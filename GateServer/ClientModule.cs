﻿/********************************************************************
	created:	2014/12/11
	created:	11:12:2014   9:42
	purpose:    此模块用于C/S间报文与S/S间报文的转换转发
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using ServerLib.Bus;
using Protocols;
using Log;
using Utility.Debugger;
using ServerLib.Session;
using Config;
using ServerLib;
using GateServer;
using Event;

namespace Module
{
    public class ClientModule : LogicModule
    {
        public ClientModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Interface
        protected override bool OnInit()
        {
            base.OnInit();

            ServerItems serverItems = new ServerItems();
            env.Redis.getObject("iptable", serverItems);

            VirtualAddress localAddr = env.Config.BusCfg.LocalAddress;
            IPEndPoint listenEndPoint = serverItems.GetEndPoint(localAddr);
            if (listenEndPoint == null)
            {
                LogSys.Error("[ClientModule.Init]No LocalAddress");
                Debug.Assert(false, "[ClientModule.Init]No LocalAddress");
                return false;
            }

            SessionConfig sessionConfig = new SessionConfig();
            sessionConfig.ProtoBodyDelayParse = 1;

            g.sessionMgr.OnConnected += this.HandleConnected;
            g.sessionMgr.OnConnectFailed += this.HandleConnectFailed;
            g.sessionMgr.OnMessage += this.HandleMessage;
            g.sessionMgr.OnDisconnected += this.HandleDisconnected;

            g.sessionMgr.Init(sessionConfig, listenEndPoint);

            g.transferMgr.Init();

            RecordMsgHandler<UserRegisterReq>(MSGID.UserRegisterReq, HandleUserRegisterReq);
            RecordMsgHandler<UserLoginReq>(MSGID.UserLoginReq, HandleUserLoginReq);
            RecordMsgHandler<ReloginReq>(MSGID.ReloginReq, HandleReloginReq);
            RecordMsgHandler<GetServerTimeReq>(MSGID.GetServerTimeReq, HandleGetServerTimeReq);
            RecordMsgHandler<HeartbeatReq>(MSGID.HeartbeatReq, HandleHeartbeatReq);
            RecordMsgHandler<GetServerConfigReq>(MSGID.GetServerConfigReq, HandleGetServerConfigReq);

            return true;
        }

        protected override void OnDestroy()
        {
            g.sessionMgr.Stop();

            base.OnDestroy();
        }

        protected override void OnTick()
        {
            g.sessionMgr.Update();

            List<UInt32> lstSessionIds = g.playerMgr.GetConditionSessionIds(CheckHeartExpire);
            foreach (var sessionid in lstSessionIds)
            {
                GatePlayer player = g.playerMgr.FindSession(sessionid);
                if (null != player)
                {
                    LogSys.Warn("[LOGIN] 心跳超时, Session: {0}, Plantform: {1}, Account: {2}", sessionid, player.platformType, player.account);
                }
                else
                {
                    LogSys.Warn("[LOGIN] 心跳超时, Session: {0}", sessionid);
                }

                g.sessionMgr.Disconnect(sessionid, DisconnReason.Active);
            }

            lstSessionIds = g.playerMgr.GetConditionSessionIds(CheckVerifyExpire);
            foreach (var sessionid in lstSessionIds)
            {
                GatePlayer player = g.playerMgr.FindSession(sessionid);
                if (null != player)
                {
                    LogSys.Warn("[LOGIN] 验证超时, Session: {0}, Plantform: {1}, Account: {2}", sessionid, player.platformType, player.account);
                }
                else
                {
                    LogSys.Warn("[LOGIN] 验证超时, Session: {0}", sessionid);
                }

                g.sessionMgr.Disconnect(sessionid, DisconnReason.Active);
            }
        }

        static bool CheckHeartExpire(GatePlayer player)
        {
            return player.IsHeartExpire();
        }

        static bool CheckVerifyExpire(GatePlayer player)
        {
            return player.IsVerifyExpire();
        }

        //建立连接
        public void HandleConnected(IPEndPoint endpoint, UInt32 sessionId)
        {
            LogSys.Info("[client] endpoint {0} session {1} connected!", endpoint.ToString(), sessionId);

            GatePlayer player = new GatePlayer(sessionId, g.playerMgr, g.sessionMgr);
            player.ClientIp = endpoint.Address.ToString();

            bool bResult = g.playerMgr.AddSession(player);
            if (!bResult)
            {
                LogSys.Warn("[SESSION] add sesion failed, reject session {1} connect!", sessionId);

                return;
            }
        }

        Dictionary<MSGID, Action<UInt32, Protocol>> mapFunctions = new Dictionary<MSGID, Action<uint, Protocol>>();

        public void RecordMsgHandler<T>(MSGID protoID, Action<UInt32, T> msgHandler) where T : ProtoBody
        {
            Action<UInt32, Protocol> common_handler = delegate(UInt32 sessionId, Protocol protocol)
            {
                T body = protocol.Body as T;

                if (null != body)
                {
                    msgHandler(sessionId, body);
                }
            };

            mapFunctions.Add(protoID, common_handler);
        }

        /// <summary>
        /// 是否是登录消息
        /// </summary>
        /// <param name="msgid"></param>
        /// <returns></returns>
        bool IsLoginMsg(MSGID msgid)
        {
            return msgid == MSGID.UserLoginReq || msgid == MSGID.ReloginReq;
        }

        /// <summary>
        /// 是否是注册消息
        /// </summary>
        /// <param name="msgid"></param>
        /// <returns></returns>
        bool IsRegisterMsg(MSGID msgid)
        {
            return msgid == MSGID.UserRegisterReq;
        }

        //客户端消息回调
        public void HandleMessage(UInt32 sessionId, Protocol proto)
        {
            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null == player)
            {
                LogSys.Warn("[ClientModule][HandleMessage] 找不到玩家连接, Session {0}, ignore protocol {1} ", sessionId, proto.MsgId);
                return;
            }

            // 连接状态
            if(player.SessionState == SessionStateID.Connected)
            {
                if(!IsLoginMsg(proto.MsgId) && !IsRegisterMsg(proto.MsgId))
                {
                    LogSys.Warn("[ClientModule][HandleMessage] 第一个消息不是登录消息也不是注册消息, Session {0}, ignore protocol {1} ", sessionId, proto.MsgId);
                    g.sessionMgr.Disconnect(sessionId, DisconnReason.Active);
                    return;
                }

                if(mapFunctions.ContainsKey(proto.MsgId))
                {
                    Action<UInt32, Protocol> handler = mapFunctions[proto.MsgId];
                    handler(sessionId, proto);
                }
                return;
            }

            if(player.SessionState == SessionStateID.Register)
            {
                LogSys.Warn("[ClientModule][HandleMessage] 注册状态发消息, Session {0}, ignore protocol {1} ", sessionId, proto.MsgId);
                g.sessionMgr.Disconnect(sessionId, DisconnReason.Active);
                return;
            }

            if(player.SessionState == SessionStateID.RegisterFinish)
            {
                if (!IsLoginMsg(proto.MsgId))
                {
                    LogSys.Warn("[ClientModule][HandleMessage] 不是登录消息, Session {0}, ignore protocol {1} ", sessionId, proto.MsgId);
                    g.sessionMgr.Disconnect(sessionId, DisconnReason.Active);
                    return;
                }

                if (mapFunctions.ContainsKey(proto.MsgId))
                {
                    Action<UInt32, Protocol> handler = mapFunctions[proto.MsgId];
                    handler(sessionId, proto);
                }
                return;
            }

            if (player.SessionState == SessionStateID.Authorizing)
            {
                LogSys.Warn("[ClientModule][HandleMessage] 验证状态发消息, Session {0}, ignore protocol {1} ", sessionId, proto.MsgId);
                g.sessionMgr.Disconnect(sessionId, DisconnReason.Active);
                return;
            }

            if(player.SessionState == SessionStateID.Gaming)
            {
                if(IsLoginMsg(proto.MsgId) || IsRegisterMsg(proto.MsgId))
                {
                    LogSys.Warn("[ClientModule][HandleMessage] Gaming状态，发注册消息或登录消息, Session {0}, ignore protocol {1} ", sessionId, proto.MsgId);
                    g.sessionMgr.Disconnect(sessionId, DisconnReason.Active);
                    return;
                }

                if (mapFunctions.ContainsKey(proto.MsgId))
                {
                    Action<UInt32, Protocol> handler = mapFunctions[proto.MsgId];
                    handler(sessionId, proto);
                    return;
                }
                
                // 是否是转发到战斗服的消息
                if (g.transferMgr.IsTrans2Battle(proto.MsgId))
                {
                    player.SendMsgToBattle(proto);
                }
                else
                {
                    player.SendMsgToLobby(proto);
                }
                return;
            }

            LogSys.Warn("[ClientModule][HandleMessage] 其他状态，断开连接状态, Session {0}, ignore protocol {1} ", sessionId, proto.MsgId);
            g.sessionMgr.Disconnect(sessionId, DisconnReason.Active);
        }

        /// <summary>
        /// 登录消息
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="req"></param>
        void HandleUserLoginReq(UInt32 sessionId, UserLoginReq req)
        {
            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null == player)
            {
                LogSys.Warn("[ClientModule][HandleUserLoginReq] 找不到玩家连接: Plantform={0}, Account={1}, AccountId={2}, Session={3}", req.platformType, req.account,req.accountId, sessionId);
                return;
            }

            LogSys.Info("[ClientModule][HandleUserLoginReq] UserLoginReq : Plantform={0}, Account={1}, AccountId={2}, Session={3}",  req.platformType, req.account,req.accountId, sessionId);

            player.SessionState = SessionStateID.Authorizing;
            player.account = req.account;
            player.platformType = req.platformType;
            player.AccountId = req.accountId;

            player.SendMsgToLobby(req);
        }

        /// <summary>
        /// 注册消息
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="req"></param>
        void HandleUserRegisterReq(UInt32 sessionId, UserRegisterReq req)
        {
            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null == player)
            {
                LogSys.Warn("[ClientModule][HandleUserRegisterReq] 找不到玩家连接: Account {0} Session {1} ", req.account, sessionId);
                return;
            }

            LogSys.Info("[ClientModule][HandleUserLoginReq] UserLoginReq : Account {0} Session {1} ", req.account, sessionId);

            player.SessionState = SessionStateID.Register;
  
            player.SendMsgToLobby(req);
        }

        /// <summary>
        /// 断线重连
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="req"></param>
        void HandleReloginReq(UInt32 sessionId, ReloginReq req)
        {
            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null == player)
            {
                LogSys.Warn("[ClientModule][HandleReloginReq] 找不到玩家连接: AccountId {0} Session {1} ", req.accountid, sessionId);
                return;
            }

            LogSys.Info("[ClientModule][HandleReloginReq] ReloginReq : AccountId {0} Session {1} ", req.accountid, sessionId);

            player.SessionState = SessionStateID.Authorizing;
            player.AccountId = req.accountid;
            g.playerMgr.BindSessionWithPlayer(sessionId, req.accountid);

            player.SendMsgToLobby(req);
        }

        Protocols.MajiangPushDown.RoomConfig pushDownConfig = new Protocols.MajiangPushDown.RoomConfig();
        Protocols.MajiangPoint.RoomConfig pointConfig = new Protocols.MajiangPoint.RoomConfig();
        Protocols.Landlord.RoomConfig lordConfig = new Protocols.Landlord.RoomConfig();

        Protocols.GoldenFlower.RoomConfig flowerConfig = new Protocols.GoldenFlower.RoomConfig();

        Protocols.MajiangYunCheng.RoomConfig yunChengConfig = new Protocols.MajiangYunCheng.RoomConfig();//运城麻将


        /// <summary>
        /// 获取服务器配置
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="?"></param>
        void HandleGetServerConfigReq(UInt32 sessionId, GetServerConfigReq req)
        {
            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null == player)
            {
                LogSys.Warn("[ClientModule][HandleGetServerConfigReq] 找不到玩家连接: Session {0} ", sessionId);
                return;
            }

            GetServerConfigRsp rsp = new GetServerConfigRsp();
            rsp.heartMillisecond = env.Config.heartMillisecond;

            pushDownConfig.cardRound = Protocols.MajiangPushDown.ECardRound.OneRound;
            rsp.lstCreateRoomCards.Add(pushDownConfig.roomCardCount);
            pushDownConfig.cardRound = Protocols.MajiangPushDown.ECardRound.FourRound;
            rsp.lstCreateRoomCards.Add(pushDownConfig.roomCardCount);
            pushDownConfig.cardRound = Protocols.MajiangPushDown.ECardRound.EightRound;
            rsp.lstCreateRoomCards.Add(pushDownConfig.roomCardCount);

            pointConfig.cardRound = Protocols.MajiangPoint.ECardRound.OneRound;
            rsp.lstCreateRoomCards.Add(pointConfig.roomCardCount);
            pointConfig.cardRound = Protocols.MajiangPoint.ECardRound.FourRound;
            rsp.lstCreateRoomCards.Add(pointConfig.roomCardCount);
            pointConfig.cardRound = Protocols.MajiangPoint.ECardRound.EightRound;
            rsp.lstCreateRoomCards.Add(pointConfig.roomCardCount);

            lordConfig.roomRound = Protocols.Landlord.ERoomRound.OneRound;
            rsp.lstCreateRoomCards.Add(lordConfig.roomCardCount);
            lordConfig.roomRound = Protocols.Landlord.ERoomRound.FourRound;
            rsp.lstCreateRoomCards.Add(lordConfig.roomCardCount);
            lordConfig.roomRound = Protocols.Landlord.ERoomRound.EightRound;
            rsp.lstCreateRoomCards.Add(lordConfig.roomCardCount);


            flowerConfig.roomRound = Protocols.GoldenFlower.ERoomRound.EightRound;
            rsp.lstCreateRoomCards.Add(flowerConfig.roomCardCount);
            flowerConfig.roomRound = Protocols.GoldenFlower.ERoomRound.SixteenRound;
            rsp.lstCreateRoomCards.Add(flowerConfig.roomCardCount);
            flowerConfig.roomRound = Protocols.GoldenFlower.ERoomRound.TwentyfourRound;
            rsp.lstCreateRoomCards.Add(flowerConfig.roomCardCount);


            //运城贴金
            yunChengConfig.cardRound = Protocols.MajiangYunCheng.ECardRound.OneRound;
            rsp.lstCreateRoomCards.Add(yunChengConfig.roomCardCount);
            yunChengConfig.cardRound = Protocols.MajiangYunCheng.ECardRound.FourRound;
            rsp.lstCreateRoomCards.Add(yunChengConfig.roomCardCount);
            yunChengConfig.cardRound = Protocols.MajiangYunCheng.ECardRound.EightRound;
            rsp.lstCreateRoomCards.Add(yunChengConfig.roomCardCount);

            player.SendMsgToClient(rsp);
        }

        /// <summary>
        /// 获取服务器时间
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="req"></param>
        void HandleGetServerTimeReq(UInt32 sessionId, GetServerTimeReq req)
        {
            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null == player)
            {
                LogSys.Warn("[ClientModule][HandleGetServerTimeReq] 找不到玩家连接: Session {0} ", sessionId);
                return;
            }

            GetServerTimeRsp rsp = new GetServerTimeRsp();
            rsp.serverTime = (DateTime.Now - DateTime.MinValue).TotalSeconds;
            player.SendMsgToClient(rsp);
        }

        HeartbeatRsp rsp = new HeartbeatRsp();

        /// <summary>
        /// 心跳消息
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="req"></param>
        void HandleHeartbeatReq(UInt32 sessionId, HeartbeatReq req)
        {
            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null == player)
            {
                LogSys.Warn("[ClientModule][HandleHeartbeatReq] 找不到玩家连接: Session {0} ", sessionId);
                return;
            }

            player.SendMsgToClient(rsp);

            player.UpdateHeartBeat();
        }

        /// <summary>
        /// 断开连接，玩家下线
        /// </summary>
        /// <param name="sessionId"></param>
        /// <param name="reason"></param>
        public void HandleDisconnected(UInt32 sessionId, DisconnReason reason)
        {
            LogSys.Info("[client][login] session {0} disconnected, reason = {1}", sessionId, reason.ToString());

            GatePlayer player = g.playerMgr.FindSession(sessionId);
            if (null != player)
            {
                LogSys.Info("[client][login] player [accountid:{0},account:{1},sessionid:{2}] disconnected, reason = {3}",player.AccountId, player.account, sessionId, reason.ToString());

                player.SessionState = SessionStateID.Disconnected;

                // 下线通知
                PlayerOfflineNtf ntf = new PlayerOfflineNtf();
                player.SendMsgToLobby(ntf);
            }
            else
            {
                LogSys.Warn("[SESSION] Net disconnected for session {0}, but cannot find corresponding gatePlayer!", sessionId);
            }

            g.playerMgr.RemoveSession(sessionId);
        }

        /// <summary>
        /// 建立连接失败
        /// </summary>
        /// <param name="remoteEndPoint">endpoint</param>
        /// <param name="sessionId">sessionId</param>
        /// <param name="bActive">bActive</param>
        /// <param name="serverID">serverID</param>
        public void HandleConnectFailed(IPEndPoint remoteEndPoint)
        {

        }
    }
}
