﻿/********************************************************************
	created:	2014/12/11
	created:	11:12:2014   9:42
	filename: 	CommonPlatform\Server\ServerInstance\GateServer\Modules\GateModule.cs
	file path:	CommonPlatform\Server\ServerInstance\GateServer\Modules
	file base:	GateModule
	file ext:	cs
	author:		刘冰生
	
	purpose:    此模块用于C/S间报文与S/S间报文的转换转发
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using ServerLib.Bus;
using Protocols;
using Log;
using Utility.Debugger;
using ServerLib.Session;
using Config;
using ServerLib;
using GateServer;
using Event;

namespace Module
{
    public class GateModule : LogicModule
    {
        public GateModule()
            : base()
        {
        }

        //////////////////////////////////////////////////////////////////////////
        // Module Interface
        protected override bool OnInit()
        {
            base.OnInit();

            //登录
            RecordMsgHandler<UserLoginRsp>(MSGID.UserLoginRsp, OnUserLoginRsp);
            RecordMsgHandler<ReloginRsp>(MSGID.ReloginRsp, OnReloginRsp);
            RecordMsgHandler<KickAccountReq>(MSGID.KickAccountReq, OnKickAccountReq);

            RecordMsgHandler<UserRegisterRsp>(MSGID.UserRegisterRsp, OnUserRegisterRsp);

            //绑定战斗服
            RecordMsgHandler<BattleServerBindReq>(MSGID.BattleServerBindReq, OnBattleServerBind);
            RecordMsgHandler<BattleServerUnbindReq>(MSGID.BattleServerUnbindReq, OnBattleServerUnbind);

            //接收消息事件
            env.eventSystem.AddListener<EventMsgReceived>(HandleEventMsgReceived);

            return true;
        }

        public void RecordMsgHandler<T>(MSGID protoID, MsgHandlerAll<T> msgHandler) where T : ProtoBody
        {
            AddProtoId(protoID);
            RegisterMsgHandler(protoID, msgHandler);
        }

        protected HashSet<MSGID> setProtoIds = new HashSet<MSGID>();
        protected void AddProtoId(MSGID id)
        {
            setProtoIds.Add(id);
        }
        protected bool HasProtoId(MSGID id)
        {
            return setProtoIds.Contains(id);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        /// <summary>
        /// 消息事件
        /// </summary>
        /// <param name="ev"></param>
        void HandleEventMsgReceived(EventMsgReceived ev)
        {
            if (HasProtoId(ev.proto.MsgId))
            {
                return;
            }

            GatePlayer player = g.playerMgr.FindPlayer(ev.proto.RoleId);
            if (null == player)
            {
                return;
            }

            player.SendMsgToClient(ev.proto);
        }

        /// <summary>
        /// 注册返回
        /// </summary>
        /// <param name="rsp"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnUserRegisterRsp(UserRegisterRsp rsp, UInt32 from, stServerBody serverBody)
        {
            GatePlayer player = g.playerMgr.FindSession(serverBody.sessionId);
            if (null != player)
            {
                if (rsp.errorCode != UserRegisterRsp.ErrorID.Success)
                {
                    SendClientAndClose(player, rsp);
                    return;
                }

                player.SessionState = SessionStateID.RegisterFinish;
                player.LobbyAddress = from;

                player.SendMsgToClient(rsp);
            }
        }

        // Message Handlers
        /// <summary>
        /// 登录返回
        /// </summary>
        /// <param name="rsp"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnUserLoginRsp(UserLoginRsp rsp, UInt32 from, stServerBody serverBody)
        {
            GatePlayer player = g.playerMgr.FindSession(serverBody.sessionId);
            if (null != player)
            {
                if (rsp.errorCode != UserLoginRsp.ErrorID.Success)
                {
                    SendClientAndClose(player, rsp);
                    return;
                }

                player.AccountId = rsp.accountid;
                g.playerMgr.BindSessionWithPlayer(serverBody.sessionId, rsp.accountid);
                player.SessionState = SessionStateID.Gaming;
                player.LobbyAddress = from;

                player.SendMsgToClient(rsp);

                // 上线通知
                PlayerOnlineNtf ntf = new PlayerOnlineNtf();
                ntf.clientIp = player.ClientIp;
                player.SendMsgToLobby(ntf);

                player.StartCheckHeartBeat();
            }
        }

        /// <summary>
        /// 重新登录返回
        /// </summary>
        /// <param name="rsp"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnReloginRsp(ReloginRsp rsp, UInt32 from, stServerBody serverBody)
        {
            GatePlayer player = g.playerMgr.FindSession(serverBody.sessionId);
            if (null != player)
            {
                if (rsp.errorCode != ReloginRsp.ErrorID.Success)
                {
                    SendClientAndClose(player, rsp);
                    return;
                }

                player.SessionState = SessionStateID.Gaming;
                player.LobbyAddress = from;

                player.SendMsgToClient(rsp);

                // 上线通知
                PlayerOnlineNtf ntf = new PlayerOnlineNtf();
                ntf.clientIp = player.ClientIp;
                player.SendMsgToLobby(ntf);

                player.StartCheckHeartBeat();
            }
        }

        void SendClientAndClose(GatePlayer player, ProtoBody msg)
        {
            player.SendMsgToClient(msg, delegate()
            {
                g.sessionMgr.Disconnect(player.SessionID, DisconnReason.Active);
            });
        }

        /// <summary>
        /// 踢玩家下线
        /// </summary>
        /// <param name="rsp"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnKickAccountReq(KickAccountReq req, UInt32 from, stServerBody serverBody)
        {
            GatePlayer player = g.playerMgr.FindSession(serverBody.sessionId);
            if (null != player)
            {
                KickNtf ntf = new KickNtf();
                ntf.reason = req.reason;
                SendClientAndClose(player, ntf);
            }

            KickAccountRsp rsp = new KickAccountRsp();
            rsp.errorCode = KickAccountRsp.ErrorID.Success;
            player.SendMsgToLobby(rsp, serverBody.uuid);
        }

        /// <summary>
        /// 绑定战斗服
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnBattleServerBind(BattleServerBindReq req, UInt32 from, stServerBody serverBody)
        {
            GatePlayer player = g.playerMgr.FindPlayer(serverBody.accountId);
            if(null == player)
            {
                return;
            }

            player.BattleAddress = from;
        }

        /// <summary>
        /// 解绑战斗服
        /// </summary>
        /// <param name="req"></param>
        /// <param name="from"></param>
        /// <param name="serverBody"></param>
        void OnBattleServerUnbind(BattleServerUnbindReq req, UInt32 from, stServerBody serverBody)
        {
            GatePlayer player = g.playerMgr.FindPlayer(serverBody.accountId);
            if (null == player)
            {
                return;
            }

            player.BattleAddress = 0;
        }
    }
}
