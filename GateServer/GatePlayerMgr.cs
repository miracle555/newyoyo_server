﻿/********************************************************************
	created:	2014/12/22
	created:	22:12:2014   14:25
	filename: 	CommonPlatform\Server\ServerInstance\GateServer\Session\g.playerMgr.cs
	file path:	CommonPlatform\Server\ServerInstance\GateServer\Session
	file base:	g.playerMgr
	file ext:	cs
	author:		刘冰生
	
	purpose:    Session管理器	
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Log;
using Utility.Debugger;
using Protocols;

namespace GateServer
{
    public class GatePlayerMgr
    {
        public GatePlayerMgr()
        {

        }

        public GatePlayer FindSession(UInt32 sessionID)
        {
            if (!mapSessions.ContainsKey(sessionID))
            {
                return null;
            }

            return mapSessions[sessionID];
        }

        public GatePlayer FindPlayer(UInt64 accountId)
        {
            if (!mapPlayers.ContainsKey(accountId))
            {
                return null;
            }

            return mapPlayers[accountId];
        }

        public bool AddSession(GatePlayer session)
        {
            if (null != FindSession(session.SessionID))
            {
                return false;
            }

            mapSessions.Add(session.SessionID, session);

            return true;
        }

        public bool BindSessionWithPlayer(UInt32 sessionId, UInt64 accountId)
        {
            RemovePlayer(accountId);

            GatePlayer session = FindSession(sessionId);
            if (null == session)
            {
                return false;
            }

            session.AccountId = accountId;
            mapPlayers.Add(accountId, session);

            return true;
        }

        public bool RemoveSession(UInt32 sessionId)
        {
            GatePlayer session = FindSession(sessionId);
            if (null == session)
            {
                return false;
            }
            
            mapPlayers.Remove(session.AccountId);
            mapSessions.Remove(sessionId);

            session.OnDestroy();

            return true;
        }

        public bool RemovePlayer(UInt64 accountId)
        {
            GatePlayer session = FindPlayer(accountId);
            if (null == session)
            {
                return false;
            }

            mapPlayers.Remove(accountId);
            mapSessions.Remove(session.SessionID);

            session.OnDestroy();
            return true;
        }

        public void CheckAllPlayer(Action<GatePlayer> function)
        {
            foreach (var item in mapPlayers.Values)
            {
                function(item);
            }
        }

        /// <summary>
        /// 获得符合条件的玩家
        /// </summary>
        /// <param name="function"></param>
        /// <returns></returns>
        public List<UInt64> GetConditionRoleIds(Func<GatePlayer, bool> function)
        {
            List<UInt64> lstResults = new List<UInt64>();
            foreach(var item in mapPlayers)
            {
                if(function(item.Value))
                {
                    lstResults.Add(item.Key);
                }
            }
            return lstResults;
        }

        public List<UInt32> GetConditionSessionIds(Func<GatePlayer, bool> function)
        {
            List<UInt32> lstResults = new List<UInt32>();
            foreach (var item in mapSessions)
            {
                if (function(item.Value))
                {
                    lstResults.Add(item.Key);
                }
            }
            return lstResults;
        }

        //////////////////////////////////////////////////////////////////////////
        // Data Member

        private Dictionary<UInt32, GatePlayer> mapSessions = new Dictionary<UInt32, GatePlayer>();

        private Dictionary<UInt64, GatePlayer> mapPlayers = new Dictionary<UInt64, GatePlayer>();
    }
}
