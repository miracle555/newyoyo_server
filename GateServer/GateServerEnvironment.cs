﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerLib;
using Config;
using Module;
using ServerLib.Session;
using Protocols;

namespace GateServer
{
    class GateServerenvironment
    {
        public GateServerenvironment(ServerConfig config)
        {
            env.Init(config);
            env.ModuleManager.RegisterModule(new GateModule());
            env.ModuleManager.RegisterModule(new ClientModule());
        }

        public bool Start()
        {
            return env.Start();
        }
    }

    class g
    {
        /// <summary>
        /// 玩家管理器
        /// </summary>
        public static GatePlayerMgr playerMgr = new GatePlayerMgr();

        /// <summary>
        /// 消息转发管理器
        /// </summary>
        public static MsgTransferMgr transferMgr = new MsgTransferMgr();

        /// <summary>
        /// 客户端连接管理器
        /// </summary>
        public static SessionManager sessionMgr = new SessionManager(false);
    }
}
