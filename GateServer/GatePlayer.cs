﻿/********************************************************************
	created:	2014/12/22
	created:	22:12:2014   14:20
	filename: 	CommonPlatform\Server\ServerInstance\GateServer\Session\GatePlayer.cs
	file path:	CommonPlatform\Server\ServerInstance\GateServer\Session
	file base:	GatePlayer
	file ext:	cs
	author:		刘冰生
	
	purpose:    Gate管理的Session对象	,期望由ServerLib的session派生出来,现在暂时独立使用
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Protocols;
using ServerLib;
using ServerLib.Session;
using ServerLib.Bus;

namespace GateServer
{
    public enum SessionStateID : int
    {
        Invalid = 0,
        Connected = 1,
        Register = 2, // 注册
        RegisterFinish = 3, // 注册完成
        Authorizing = 4,
        Gaming = 5,
        Disconnected = 6, 
        Count = 7,
    }

    public class GatePlayer
    {
        public GatePlayer(UInt32 sessionIDIn, GatePlayerMgr mgr, SessionManager sessionMgr)
        {
            SessionID = sessionIDIn;
            this.mgr = mgr;
            this.sessionMgr = sessionMgr;

            SessionState = SessionStateID.Connected;
        }

        public void OnDestroy()
        {
        }

        public void SendMsgToLobby(Protocol proto, UInt64 uuid = 0)
        {
            proto.SessionId = SessionID;
            proto.RoleId = AccountId;
            proto.uuid = 0;

            if (LobbyAddress != 0)
            {
                env.BusService.SendMessageToServer(LobbyAddress, proto);
                return;
            }

            env.BusService.SendMessage("lobby", proto);
        }

        public void SendMsgToBattle(Protocol proto, UInt64 uuid = 0)
        {
            proto.SessionId = SessionID;
            proto.RoleId = AccountId;
            proto.uuid = uuid;

            if (BattleAddress != 0)
            {
                env.BusService.SendMessageToServer(BattleAddress, proto);
            }
        }

        public void UpdateHeartBeat()
        {
            heartExpireTime = env.Timer.CurrentDateTime.AddMilliseconds(env.Config.heartMillisecond * 60);
        }

        /// <summary>
        /// 开始检测心跳
        /// </summary>
        public void StartCheckHeartBeat()
        {
            bCheckHeart = true;
            heartExpireTime = env.Timer.CurrentDateTime.AddMilliseconds(env.Config.heartMillisecond * 60);
            bCheckVerifyExpire = false;
        }

        /// <summary>
        /// 检测心跳超时
        /// </summary>
        /// <returns></returns>
        public bool IsHeartExpire()
        {
            if (!bCheckHeart)
            {
                return false;
            }

            if (env.Timer.CurrentDateTime > heartExpireTime)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 登录超时
        /// </summary>
        /// <returns></returns>
        public bool IsVerifyExpire()
        {
            if(!bCheckVerifyExpire)
            {
                return false;
            }

            if(env.Timer.CurrentDateTime > startTime)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否检测心跳
        /// </summary>
        bool bCheckHeart = false;
        DateTime heartExpireTime = DateTime.MinValue;

        public void SendMsgToClient(Protocol proto, Action callback = null)
        {
            sessionMgr.SendMessage(SessionID, proto, callback);
        }

        //////////////////////////////////////////////////////////////////////////
        public UInt32 SessionID
        {
            get;
            set;
        }

        public UInt64 AccountId
        {
            get;
            set;
        }

        public String account;

        public EPID platformType;

        public UInt32 LobbyAddress
        {
            get;
            set;
        }

        public UInt32 BattleAddress
        {
            get;
            set;
        }

        public string ClientIp = "";

        protected GatePlayerMgr mgr;
        protected SessionManager sessionMgr;

        bool bCheckVerifyExpire = true;
        public DateTime startTime = env.Timer.CurrentDateTime.AddSeconds(3);

        public SessionStateID SessionState
        {
            set;
            get;
        }
    }
}
